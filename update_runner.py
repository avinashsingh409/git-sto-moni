import re
import sys

# Define the new version string
new_version_string = sys.argv[1] 

# Read the input file
input_file_path = "./windows/runner/Runner.rc"
with open(input_file_path, "r", encoding="utf-8") as file:
    file_contents = file.read()

# Use regular expressions to find and replace VERSION_AS_STRING
pattern = r'#define VERSION_AS_STRING\s+".*?"'
replacement = f'#define VERSION_AS_STRING "{new_version_string}"'
new_contents = re.sub(pattern, replacement, file_contents)

# Write the modified content back to the file
with open(input_file_path, "w", encoding="utf-8") as file:
    file.write(new_contents)

print(f"Updated VERSION_AS_STRING to {new_version_string}")
