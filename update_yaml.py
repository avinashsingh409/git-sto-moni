import sys
import ruamel.yaml

# Retrieve the new version from a GitLab CI variable
new_version = sys.argv[1]  # You can pass this value from the pipeline

# Path to the YAML file
yaml_file_path = "pubspec.yaml"  # Customize the path as needed

# Load the YAML file
yaml = ruamel.yaml.YAML()

with open(yaml_file_path, "r") as yaml_file:
    data = yaml.load(yaml_file)

# Update the msix_version variable
if "msix_version" in data:
    data["msix_version"] = new_version
else:
    data["msix_version"] = new_version

# Write the updated data back to the YAML file
with open(yaml_file_path, "w") as yaml_file:
    yaml.dump(data, yaml_file)

print(f"Updated msix_version to {new_version}")
