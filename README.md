# STO Mobile Execution Application


## Code Architecture

### Backbone of the Project:

Starting the code architecture with the backbone of the project primarily focusing on
building blocks and the skeleton that is holding down the project in one piece. Here are
the list of most important bones to hold down and build the skeleton:

#### * `Dio`
This is used for the entire application’s api call package and handles network calls/Interceptor.

#### * `Build_Runner`
The build_runner package provides a concrete way of generating files using Dart code,
outside of tools like pub. This package is the bread and butter of the part file
generation, freezed files generation, de/serialization and injectable generation. To
generate the listed use cases all you have to do is run:

*** flutter pub run build_runner build --delete-conflicting-outputs

This will take care of all the file generation and part file generation on its own. If
the generation of freezed files and injectable files does not make sense at the
moment please refer to the clarifications down below for better understanding.

#### * `Dartz`
Starting up with Dartz. It is a functional programming concept primarily built for
dart. The code highly depends on dart on the repository layer where we would
usually return Either an exception or the data from the api.

#### * `Equatable`
Being able to compare objects in Dart often involves having to override the ==
operator as well as hashCode. Not only is it verbose and tedious, but failure to do
so can lead to inefficient code which does not behave as we expect. By default,
== returns true if two objects are the same instance.

#### * `Freezed Data Class`
Unlike other kotlin/java, flutter projects or JS projects where we would utilize the
native classes the libraries provide, we are using Freezed for Classes/Pojo
Classes equality, factory classes as equality in flutter is pretty debatable and
freezed works wonder for flutter. You will be very familiar with Freezed if you’re
using TypeScript on top of JS projects. The cherry on the top is that it also
handles de/serialization, define constructor+ properties, overrides toString,
equality and copyWith method itself. Implementing all of this can take hundreds
of lines, which are error-prone and affect the readability of your model
significantly. Freezed fixes that by implementing most of this for you, allowing you
to focus on the definition of your model. Freezed classes are everywhere in the
application where classes are required.

#### * `Json Serializable`
JSON serializer converts an object to its string. The reverse is also possible which is
known as Deserialization. JSON is a format that encodes an object to a string. On the
transmission of data or storing is a file, data need to be in byte strings, but as complex
objects are in JSON format. Serialization converts these objects into byte strings which
is JSON serialization. After these byte strings are transmitted, the receiver recovers the
original object from the string known as Deserialization. This is highly used for
de/serializing the data from the backend. We use the Json Serializable package in this
project for that specific reason.

#### * `Get It`
This is a simple Service Locator for Dart and Flutter projects with some additional
goodies highly inspired by splat. As your App grows, at some point you will need to put
your app's logic in classes that are separated from your Widgets. Keeping your widgets
from having direct dependencies makes your code better organized and easier to test
and maintain. But now you need a way to access these objects from your UI code. We
primarily use getIt to get the instance of injected dependencies into our application. Its
initial instance is created in the injection.dart file.

#### * `Injectable`
Finally getting into injectable, we generally use it to register factories, singleton,
LazySingleton, injectable and other annotations that make DI possible. Its initial
instance is initialized in injection.dart.

After this it's just a matter of fact of your preference whether you want to declare,
inject, or provide a singleton to a class or as injectable. We use this everywhere
where we’re making use of dependency injection.

----------------------------


