part of 'field_definition_bloc.dart';

@freezed
class FieldDefinitionState with _$FieldDefinitionState {
  const factory FieldDefinitionState.initial() = _Initial;
  const factory FieldDefinitionState.loading() = _Loading;
  const factory FieldDefinitionState.valueLoaded({
    required List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
    required List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData,
  }) = _ValueLoaded;
  const factory FieldDefinitionState.empty() = _Empty;
}
