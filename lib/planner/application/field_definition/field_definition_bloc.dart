import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/translation_key_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';

part 'field_definition_event.dart';
part 'field_definition_state.dart';
part 'field_definition_bloc.freezed.dart';

@injectable
class FieldDefinitionBloc extends Bloc<FieldDefinitionEvent, FieldDefinitionState> {
  final IFieldDefinitionRepository fieldDefinitionRepository;
  FieldDefinitionBloc(this.fieldDefinitionRepository) : super(const _Initial()) {
    on<FieldDefinitionEvent>((event, emit) async {
      await event.when(getFieldDefinitionWithValue: (subgroupId) async {
        emit(const FieldDefinitionState.loading());
        final dynamicFieldDefs = await fieldDefinitionRepository.getDynamicFieldDefinitionsWithValues(
          subGroupId: subgroupId,
        );
        final staticFieldDefs = await fieldDefinitionRepository.getStaticFieldDefinitionsWithValues(
          subGroupId: subgroupId,
        );
        if (dynamicFieldDefs.isEmpty && staticFieldDefs.isEmpty) {
          emit(const FieldDefinitionState.empty());
        } else {
          emit(FieldDefinitionState.valueLoaded(
              dynamicFieldDefinitionData: dynamicFieldDefs, staticFieldDefinitionData: staticFieldDefs));
        }
      }, changeDynamicValue: (guid, newVal) async {
        await fieldDefinitionRepository.updateDynamicFieldDefinitionValue(guid: guid, newVal: newVal);
      }, changeStaticValue: (translationKey, newVal, plannerName) async {
        await fieldDefinitionRepository.updateStaticFieldDefinitionValue(
            translationKey: translationKey, newVal: newVal, plannerName: plannerName);
      });
    });
  }
}
