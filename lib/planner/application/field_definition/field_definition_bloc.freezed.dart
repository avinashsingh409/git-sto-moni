// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_definition_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldDefinitionEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int subGroupId) getFieldDefinitionWithValue,
    required TResult Function(String guid, String newVal) changeDynamicValue,
    required TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)
        changeStaticValue,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult? Function(String guid, String newVal)? changeDynamicValue,
    TResult? Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult Function(String guid, String newVal)? changeDynamicValue,
    TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetFieldDefinitionWithValue value)
        getFieldDefinitionWithValue,
    required TResult Function(_ChangeDynamicValue value) changeDynamicValue,
    required TResult Function(_ChangeStaticValue value) changeStaticValue,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult? Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult? Function(_ChangeStaticValue value)? changeStaticValue,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult Function(_ChangeStaticValue value)? changeStaticValue,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldDefinitionEventCopyWith<$Res> {
  factory $FieldDefinitionEventCopyWith(FieldDefinitionEvent value,
          $Res Function(FieldDefinitionEvent) then) =
      _$FieldDefinitionEventCopyWithImpl<$Res, FieldDefinitionEvent>;
}

/// @nodoc
class _$FieldDefinitionEventCopyWithImpl<$Res,
        $Val extends FieldDefinitionEvent>
    implements $FieldDefinitionEventCopyWith<$Res> {
  _$FieldDefinitionEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetFieldDefinitionWithValueCopyWith<$Res> {
  factory _$$_GetFieldDefinitionWithValueCopyWith(
          _$_GetFieldDefinitionWithValue value,
          $Res Function(_$_GetFieldDefinitionWithValue) then) =
      __$$_GetFieldDefinitionWithValueCopyWithImpl<$Res>;
  @useResult
  $Res call({int subGroupId});
}

/// @nodoc
class __$$_GetFieldDefinitionWithValueCopyWithImpl<$Res>
    extends _$FieldDefinitionEventCopyWithImpl<$Res,
        _$_GetFieldDefinitionWithValue>
    implements _$$_GetFieldDefinitionWithValueCopyWith<$Res> {
  __$$_GetFieldDefinitionWithValueCopyWithImpl(
      _$_GetFieldDefinitionWithValue _value,
      $Res Function(_$_GetFieldDefinitionWithValue) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subGroupId = null,
  }) {
    return _then(_$_GetFieldDefinitionWithValue(
      subGroupId: null == subGroupId
          ? _value.subGroupId
          : subGroupId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_GetFieldDefinitionWithValue implements _GetFieldDefinitionWithValue {
  const _$_GetFieldDefinitionWithValue({required this.subGroupId});

  @override
  final int subGroupId;

  @override
  String toString() {
    return 'FieldDefinitionEvent.getFieldDefinitionWithValue(subGroupId: $subGroupId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetFieldDefinitionWithValue &&
            (identical(other.subGroupId, subGroupId) ||
                other.subGroupId == subGroupId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, subGroupId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetFieldDefinitionWithValueCopyWith<_$_GetFieldDefinitionWithValue>
      get copyWith => __$$_GetFieldDefinitionWithValueCopyWithImpl<
          _$_GetFieldDefinitionWithValue>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int subGroupId) getFieldDefinitionWithValue,
    required TResult Function(String guid, String newVal) changeDynamicValue,
    required TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)
        changeStaticValue,
  }) {
    return getFieldDefinitionWithValue(subGroupId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult? Function(String guid, String newVal)? changeDynamicValue,
    TResult? Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
  }) {
    return getFieldDefinitionWithValue?.call(subGroupId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult Function(String guid, String newVal)? changeDynamicValue,
    TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
    required TResult orElse(),
  }) {
    if (getFieldDefinitionWithValue != null) {
      return getFieldDefinitionWithValue(subGroupId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetFieldDefinitionWithValue value)
        getFieldDefinitionWithValue,
    required TResult Function(_ChangeDynamicValue value) changeDynamicValue,
    required TResult Function(_ChangeStaticValue value) changeStaticValue,
  }) {
    return getFieldDefinitionWithValue(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult? Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult? Function(_ChangeStaticValue value)? changeStaticValue,
  }) {
    return getFieldDefinitionWithValue?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult Function(_ChangeStaticValue value)? changeStaticValue,
    required TResult orElse(),
  }) {
    if (getFieldDefinitionWithValue != null) {
      return getFieldDefinitionWithValue(this);
    }
    return orElse();
  }
}

abstract class _GetFieldDefinitionWithValue implements FieldDefinitionEvent {
  const factory _GetFieldDefinitionWithValue({required final int subGroupId}) =
      _$_GetFieldDefinitionWithValue;

  int get subGroupId;
  @JsonKey(ignore: true)
  _$$_GetFieldDefinitionWithValueCopyWith<_$_GetFieldDefinitionWithValue>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ChangeDynamicValueCopyWith<$Res> {
  factory _$$_ChangeDynamicValueCopyWith(_$_ChangeDynamicValue value,
          $Res Function(_$_ChangeDynamicValue) then) =
      __$$_ChangeDynamicValueCopyWithImpl<$Res>;
  @useResult
  $Res call({String guid, String newVal});
}

/// @nodoc
class __$$_ChangeDynamicValueCopyWithImpl<$Res>
    extends _$FieldDefinitionEventCopyWithImpl<$Res, _$_ChangeDynamicValue>
    implements _$$_ChangeDynamicValueCopyWith<$Res> {
  __$$_ChangeDynamicValueCopyWithImpl(
      _$_ChangeDynamicValue _value, $Res Function(_$_ChangeDynamicValue) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? guid = null,
    Object? newVal = null,
  }) {
    return _then(_$_ChangeDynamicValue(
      guid: null == guid
          ? _value.guid
          : guid // ignore: cast_nullable_to_non_nullable
              as String,
      newVal: null == newVal
          ? _value.newVal
          : newVal // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ChangeDynamicValue implements _ChangeDynamicValue {
  const _$_ChangeDynamicValue({required this.guid, required this.newVal});

  @override
  final String guid;
  @override
  final String newVal;

  @override
  String toString() {
    return 'FieldDefinitionEvent.changeDynamicValue(guid: $guid, newVal: $newVal)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeDynamicValue &&
            (identical(other.guid, guid) || other.guid == guid) &&
            (identical(other.newVal, newVal) || other.newVal == newVal));
  }

  @override
  int get hashCode => Object.hash(runtimeType, guid, newVal);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChangeDynamicValueCopyWith<_$_ChangeDynamicValue> get copyWith =>
      __$$_ChangeDynamicValueCopyWithImpl<_$_ChangeDynamicValue>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int subGroupId) getFieldDefinitionWithValue,
    required TResult Function(String guid, String newVal) changeDynamicValue,
    required TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)
        changeStaticValue,
  }) {
    return changeDynamicValue(guid, newVal);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult? Function(String guid, String newVal)? changeDynamicValue,
    TResult? Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
  }) {
    return changeDynamicValue?.call(guid, newVal);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult Function(String guid, String newVal)? changeDynamicValue,
    TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
    required TResult orElse(),
  }) {
    if (changeDynamicValue != null) {
      return changeDynamicValue(guid, newVal);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetFieldDefinitionWithValue value)
        getFieldDefinitionWithValue,
    required TResult Function(_ChangeDynamicValue value) changeDynamicValue,
    required TResult Function(_ChangeStaticValue value) changeStaticValue,
  }) {
    return changeDynamicValue(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult? Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult? Function(_ChangeStaticValue value)? changeStaticValue,
  }) {
    return changeDynamicValue?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult Function(_ChangeStaticValue value)? changeStaticValue,
    required TResult orElse(),
  }) {
    if (changeDynamicValue != null) {
      return changeDynamicValue(this);
    }
    return orElse();
  }
}

abstract class _ChangeDynamicValue implements FieldDefinitionEvent {
  const factory _ChangeDynamicValue(
      {required final String guid,
      required final String newVal}) = _$_ChangeDynamicValue;

  String get guid;
  String get newVal;
  @JsonKey(ignore: true)
  _$$_ChangeDynamicValueCopyWith<_$_ChangeDynamicValue> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ChangeStaticValueCopyWith<$Res> {
  factory _$$_ChangeStaticValueCopyWith(_$_ChangeStaticValue value,
          $Res Function(_$_ChangeStaticValue) then) =
      __$$_ChangeStaticValueCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {TranslationKey translationKey, String newVal, String? plannerName});
}

/// @nodoc
class __$$_ChangeStaticValueCopyWithImpl<$Res>
    extends _$FieldDefinitionEventCopyWithImpl<$Res, _$_ChangeStaticValue>
    implements _$$_ChangeStaticValueCopyWith<$Res> {
  __$$_ChangeStaticValueCopyWithImpl(
      _$_ChangeStaticValue _value, $Res Function(_$_ChangeStaticValue) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? translationKey = null,
    Object? newVal = null,
    Object? plannerName = freezed,
  }) {
    return _then(_$_ChangeStaticValue(
      translationKey: null == translationKey
          ? _value.translationKey
          : translationKey // ignore: cast_nullable_to_non_nullable
              as TranslationKey,
      newVal: null == newVal
          ? _value.newVal
          : newVal // ignore: cast_nullable_to_non_nullable
              as String,
      plannerName: freezed == plannerName
          ? _value.plannerName
          : plannerName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ChangeStaticValue implements _ChangeStaticValue {
  const _$_ChangeStaticValue(
      {required this.translationKey,
      required this.newVal,
      required this.plannerName});

  @override
  final TranslationKey translationKey;
  @override
  final String newVal;
  @override
  final String? plannerName;

  @override
  String toString() {
    return 'FieldDefinitionEvent.changeStaticValue(translationKey: $translationKey, newVal: $newVal, plannerName: $plannerName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeStaticValue &&
            (identical(other.translationKey, translationKey) ||
                other.translationKey == translationKey) &&
            (identical(other.newVal, newVal) || other.newVal == newVal) &&
            (identical(other.plannerName, plannerName) ||
                other.plannerName == plannerName));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, translationKey, newVal, plannerName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChangeStaticValueCopyWith<_$_ChangeStaticValue> get copyWith =>
      __$$_ChangeStaticValueCopyWithImpl<_$_ChangeStaticValue>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int subGroupId) getFieldDefinitionWithValue,
    required TResult Function(String guid, String newVal) changeDynamicValue,
    required TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)
        changeStaticValue,
  }) {
    return changeStaticValue(translationKey, newVal, plannerName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult? Function(String guid, String newVal)? changeDynamicValue,
    TResult? Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
  }) {
    return changeStaticValue?.call(translationKey, newVal, plannerName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int subGroupId)? getFieldDefinitionWithValue,
    TResult Function(String guid, String newVal)? changeDynamicValue,
    TResult Function(
            TranslationKey translationKey, String newVal, String? plannerName)?
        changeStaticValue,
    required TResult orElse(),
  }) {
    if (changeStaticValue != null) {
      return changeStaticValue(translationKey, newVal, plannerName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetFieldDefinitionWithValue value)
        getFieldDefinitionWithValue,
    required TResult Function(_ChangeDynamicValue value) changeDynamicValue,
    required TResult Function(_ChangeStaticValue value) changeStaticValue,
  }) {
    return changeStaticValue(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult? Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult? Function(_ChangeStaticValue value)? changeStaticValue,
  }) {
    return changeStaticValue?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetFieldDefinitionWithValue value)?
        getFieldDefinitionWithValue,
    TResult Function(_ChangeDynamicValue value)? changeDynamicValue,
    TResult Function(_ChangeStaticValue value)? changeStaticValue,
    required TResult orElse(),
  }) {
    if (changeStaticValue != null) {
      return changeStaticValue(this);
    }
    return orElse();
  }
}

abstract class _ChangeStaticValue implements FieldDefinitionEvent {
  const factory _ChangeStaticValue(
      {required final TranslationKey translationKey,
      required final String newVal,
      required final String? plannerName}) = _$_ChangeStaticValue;

  TranslationKey get translationKey;
  String get newVal;
  String? get plannerName;
  @JsonKey(ignore: true)
  _$$_ChangeStaticValueCopyWith<_$_ChangeStaticValue> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FieldDefinitionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)
        valueLoaded,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult? Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ValueLoaded value) valueLoaded,
    required TResult Function(_Empty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ValueLoaded value)? valueLoaded,
    TResult? Function(_Empty value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ValueLoaded value)? valueLoaded,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldDefinitionStateCopyWith<$Res> {
  factory $FieldDefinitionStateCopyWith(FieldDefinitionState value,
          $Res Function(FieldDefinitionState) then) =
      _$FieldDefinitionStateCopyWithImpl<$Res, FieldDefinitionState>;
}

/// @nodoc
class _$FieldDefinitionStateCopyWithImpl<$Res,
        $Val extends FieldDefinitionState>
    implements $FieldDefinitionStateCopyWith<$Res> {
  _$FieldDefinitionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldDefinitionStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldDefinitionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)
        valueLoaded,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult? Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ValueLoaded value) valueLoaded,
    required TResult Function(_Empty value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ValueLoaded value)? valueLoaded,
    TResult? Function(_Empty value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ValueLoaded value)? valueLoaded,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldDefinitionState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$FieldDefinitionStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'FieldDefinitionState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)
        valueLoaded,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult? Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ValueLoaded value) valueLoaded,
    required TResult Function(_Empty value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ValueLoaded value)? valueLoaded,
    TResult? Function(_Empty value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ValueLoaded value)? valueLoaded,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements FieldDefinitionState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_ValueLoadedCopyWith<$Res> {
  factory _$$_ValueLoadedCopyWith(
          _$_ValueLoaded value, $Res Function(_$_ValueLoaded) then) =
      __$$_ValueLoadedCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
      List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData});
}

/// @nodoc
class __$$_ValueLoadedCopyWithImpl<$Res>
    extends _$FieldDefinitionStateCopyWithImpl<$Res, _$_ValueLoaded>
    implements _$$_ValueLoadedCopyWith<$Res> {
  __$$_ValueLoadedCopyWithImpl(
      _$_ValueLoaded _value, $Res Function(_$_ValueLoaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? staticFieldDefinitionData = freezed,
    Object? dynamicFieldDefinitionData = freezed,
  }) {
    return _then(_$_ValueLoaded(
      staticFieldDefinitionData: freezed == staticFieldDefinitionData
          ? _value._staticFieldDefinitionData
          : staticFieldDefinitionData // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageFieldDefinitionDTO>?,
      dynamicFieldDefinitionData: freezed == dynamicFieldDefinitionData
          ? _value._dynamicFieldDefinitionData
          : dynamicFieldDefinitionData // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageFieldDefinitionDTO>?,
    ));
  }
}

/// @nodoc

class _$_ValueLoaded implements _ValueLoaded {
  const _$_ValueLoaded(
      {required final List<WorkpackageFieldDefinitionDTO>?
          staticFieldDefinitionData,
      required final List<WorkpackageFieldDefinitionDTO>?
          dynamicFieldDefinitionData})
      : _staticFieldDefinitionData = staticFieldDefinitionData,
        _dynamicFieldDefinitionData = dynamicFieldDefinitionData;

  final List<WorkpackageFieldDefinitionDTO>? _staticFieldDefinitionData;
  @override
  List<WorkpackageFieldDefinitionDTO>? get staticFieldDefinitionData {
    final value = _staticFieldDefinitionData;
    if (value == null) return null;
    if (_staticFieldDefinitionData is EqualUnmodifiableListView)
      return _staticFieldDefinitionData;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<WorkpackageFieldDefinitionDTO>? _dynamicFieldDefinitionData;
  @override
  List<WorkpackageFieldDefinitionDTO>? get dynamicFieldDefinitionData {
    final value = _dynamicFieldDefinitionData;
    if (value == null) return null;
    if (_dynamicFieldDefinitionData is EqualUnmodifiableListView)
      return _dynamicFieldDefinitionData;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'FieldDefinitionState.valueLoaded(staticFieldDefinitionData: $staticFieldDefinitionData, dynamicFieldDefinitionData: $dynamicFieldDefinitionData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ValueLoaded &&
            const DeepCollectionEquality().equals(
                other._staticFieldDefinitionData, _staticFieldDefinitionData) &&
            const DeepCollectionEquality().equals(
                other._dynamicFieldDefinitionData,
                _dynamicFieldDefinitionData));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_staticFieldDefinitionData),
      const DeepCollectionEquality().hash(_dynamicFieldDefinitionData));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ValueLoadedCopyWith<_$_ValueLoaded> get copyWith =>
      __$$_ValueLoadedCopyWithImpl<_$_ValueLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)
        valueLoaded,
    required TResult Function() empty,
  }) {
    return valueLoaded(staticFieldDefinitionData, dynamicFieldDefinitionData);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult? Function()? empty,
  }) {
    return valueLoaded?.call(
        staticFieldDefinitionData, dynamicFieldDefinitionData);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (valueLoaded != null) {
      return valueLoaded(staticFieldDefinitionData, dynamicFieldDefinitionData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ValueLoaded value) valueLoaded,
    required TResult Function(_Empty value) empty,
  }) {
    return valueLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ValueLoaded value)? valueLoaded,
    TResult? Function(_Empty value)? empty,
  }) {
    return valueLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ValueLoaded value)? valueLoaded,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (valueLoaded != null) {
      return valueLoaded(this);
    }
    return orElse();
  }
}

abstract class _ValueLoaded implements FieldDefinitionState {
  const factory _ValueLoaded(
      {required final List<WorkpackageFieldDefinitionDTO>?
          staticFieldDefinitionData,
      required final List<WorkpackageFieldDefinitionDTO>?
          dynamicFieldDefinitionData}) = _$_ValueLoaded;

  List<WorkpackageFieldDefinitionDTO>? get staticFieldDefinitionData;
  List<WorkpackageFieldDefinitionDTO>? get dynamicFieldDefinitionData;
  @JsonKey(ignore: true)
  _$$_ValueLoadedCopyWith<_$_ValueLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmptyCopyWith<$Res> {
  factory _$$_EmptyCopyWith(_$_Empty value, $Res Function(_$_Empty) then) =
      __$$_EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyCopyWithImpl<$Res>
    extends _$FieldDefinitionStateCopyWithImpl<$Res, _$_Empty>
    implements _$$_EmptyCopyWith<$Res> {
  __$$_EmptyCopyWithImpl(_$_Empty _value, $Res Function(_$_Empty) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'FieldDefinitionState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)
        valueLoaded,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult? Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<WorkpackageFieldDefinitionDTO>? staticFieldDefinitionData,
            List<WorkpackageFieldDefinitionDTO>? dynamicFieldDefinitionData)?
        valueLoaded,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ValueLoaded value) valueLoaded,
    required TResult Function(_Empty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ValueLoaded value)? valueLoaded,
    TResult? Function(_Empty value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ValueLoaded value)? valueLoaded,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements FieldDefinitionState {
  const factory _Empty() = _$_Empty;
}
