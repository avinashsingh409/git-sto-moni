part of 'field_definition_bloc.dart';

@freezed
class FieldDefinitionEvent with _$FieldDefinitionEvent {
  const factory FieldDefinitionEvent.getFieldDefinitionWithValue({required int subGroupId}) = _GetFieldDefinitionWithValue;
  const factory FieldDefinitionEvent.changeDynamicValue({required String guid, required String newVal}) = _ChangeDynamicValue;
  const factory FieldDefinitionEvent.changeStaticValue({
    required TranslationKey translationKey,
    required String newVal,
    required String? plannerName,
  }) = _ChangeStaticValue;
}
