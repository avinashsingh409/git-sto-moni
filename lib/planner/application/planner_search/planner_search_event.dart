part of 'planner_search_bloc.dart';

@freezed
class PlannerSearchEvent with _$PlannerSearchEvent {
  const factory PlannerSearchEvent.search({required String keyword}) = _Search;
  const factory PlannerSearchEvent.changeSearchField({required WorkpackageSearchTypeEnum searchField}) = _ChangeSearchField;
}