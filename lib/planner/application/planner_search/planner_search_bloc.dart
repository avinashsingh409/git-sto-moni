import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/domain/planner_search/repository/planner_search_repository.dart';

part 'planner_search_event.dart';
part 'planner_search_state.dart';
part 'planner_search_bloc.freezed.dart';

@injectable
class PlannerSearchBloc extends Bloc<PlannerSearchEvent, PlannerSearchState> {
  WorkpackageSearchTypeEnum? searchField;
  final IPlannerSearchRepository searchRepository;

  PlannerSearchBloc(this.searchRepository) : super(const _Initial()) {
    on<PlannerSearchEvent>((event, emit) async {
      await event.when(
        search: (keyword) async {
          if (searchField != null) {
            emit(const PlannerSearchState.loading());
            final searchActivitiesStream = searchRepository.getSearchedWorkPackages(field: searchField!, keyword: keyword);
            await emit.onEach(
              searchActivitiesStream.stream, 
              onData: (results) {
                List<WorkpackageDTO> sortedResults = List<WorkpackageDTO>.from(results);
                sortedResults.sort((a, b) => (a.description ?? "").toLowerCase().compareTo((b.description ?? "").toLowerCase()));
                emit(PlannerSearchState.success(results: sortedResults));
              }
            ).catchError((error) {
              emit(const PlannerSearchState.error(errorMsg: "Unexpected Error"));
            });
          }
        },
        changeSearchField: (field) {
          searchField = field;
        }
      );
    });
  }
}
