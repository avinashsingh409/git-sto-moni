part of 'planner_search_bloc.dart';

@freezed
class PlannerSearchState with _$PlannerSearchState {
  const factory PlannerSearchState.initial() = _Initial;
  const factory PlannerSearchState.loading() = _Loading;
  const factory PlannerSearchState.success({required List<WorkpackageDTO> results}) = _Success;
  const factory PlannerSearchState.error({required String errorMsg}) = _Error;
}
