import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/repository/workpackage_grouping_repository.dart';

part 'workpackage_grouping_view_event.dart';
part 'workpackage_grouping_view_state.dart';
part 'workpackage_grouping_view_bloc.freezed.dart';

@injectable
class WorkpackageGroupingViewBloc extends Bloc<WorkpackageGroupingViewEvent, WorkpackageGroupingViewState> {
  final IWorkpackageGroupingRepository workpackageGroupingRepository;
  WorkpackageGroupingViewBloc(this.workpackageGroupingRepository) : super(const _Initial()) {
    on<WorkpackageGroupingViewEvent>((event, emit) async {
      await event.when(getAllGroupingData: () async {
        emit(const WorkpackageGroupingViewState.loading());
        final workpackageGroupings = await workpackageGroupingRepository.getAllWorkPackageGroupings();
        if (workpackageGroupings.isNotEmpty) {
          emit(WorkpackageGroupingViewState.success(workpackageGroupings: workpackageGroupings));
        } else {
          emit(const WorkpackageGroupingViewState.empty());
        }
      });
    });
  }
}
