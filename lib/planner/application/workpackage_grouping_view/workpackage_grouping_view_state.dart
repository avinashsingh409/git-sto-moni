part of 'workpackage_grouping_view_bloc.dart';

@freezed
class WorkpackageGroupingViewState with _$WorkpackageGroupingViewState {
  const factory WorkpackageGroupingViewState.initial() = _Initial;
  const factory WorkpackageGroupingViewState.loading() = _Loading;
  const factory WorkpackageGroupingViewState.success({required List<WorkpackageGroupingDTO> workpackageGroupings}) = _Success;
  const factory WorkpackageGroupingViewState.empty() = _Empty;
}
