part of 'workpackage_grouping_view_bloc.dart';

@freezed
class WorkpackageGroupingViewEvent with _$WorkpackageGroupingViewEvent {
  const factory WorkpackageGroupingViewEvent.getAllGroupingData() = _GetAllGroupingData;
}
