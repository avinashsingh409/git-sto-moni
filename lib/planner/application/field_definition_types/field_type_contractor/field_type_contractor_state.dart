part of 'field_type_contractor_bloc.dart';

@freezed
class FieldTypeContractorState with _$FieldTypeContractorState {
  const factory FieldTypeContractorState.initial() = _Initial;
}
