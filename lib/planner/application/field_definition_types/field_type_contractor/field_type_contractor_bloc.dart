import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';

part 'field_type_contractor_event.dart';
part 'field_type_contractor_state.dart';
part 'field_type_contractor_bloc.freezed.dart';

@injectable
class FieldTypeContractorBloc extends Bloc<FieldTypeContractorEvent, FieldTypeContractorState> {
  final IFieldDefinitionRepository fieldDefinitionRepository;
  FieldTypeContractorBloc(this.fieldDefinitionRepository) : super(const _Initial()) {
    on<FieldTypeContractorEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
