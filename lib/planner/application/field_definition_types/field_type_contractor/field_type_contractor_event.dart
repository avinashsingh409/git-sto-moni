part of 'field_type_contractor_bloc.dart';

@freezed
class FieldTypeContractorEvent with _$FieldTypeContractorEvent {
  const factory FieldTypeContractorEvent.getAllContractorsOptions() = _GetAllContractorsOptions;
}
