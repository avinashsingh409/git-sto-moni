// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_type_contractor_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldTypeContractorEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllContractorsOptions,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllContractorsOptions,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllContractorsOptions,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllContractorsOptions value)
        getAllContractorsOptions,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllContractorsOptions value)?
        getAllContractorsOptions,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllContractorsOptions value)? getAllContractorsOptions,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypeContractorEventCopyWith<$Res> {
  factory $FieldTypeContractorEventCopyWith(FieldTypeContractorEvent value,
          $Res Function(FieldTypeContractorEvent) then) =
      _$FieldTypeContractorEventCopyWithImpl<$Res, FieldTypeContractorEvent>;
}

/// @nodoc
class _$FieldTypeContractorEventCopyWithImpl<$Res,
        $Val extends FieldTypeContractorEvent>
    implements $FieldTypeContractorEventCopyWith<$Res> {
  _$FieldTypeContractorEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetAllContractorsOptionsCopyWith<$Res> {
  factory _$$_GetAllContractorsOptionsCopyWith(
          _$_GetAllContractorsOptions value,
          $Res Function(_$_GetAllContractorsOptions) then) =
      __$$_GetAllContractorsOptionsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetAllContractorsOptionsCopyWithImpl<$Res>
    extends _$FieldTypeContractorEventCopyWithImpl<$Res,
        _$_GetAllContractorsOptions>
    implements _$$_GetAllContractorsOptionsCopyWith<$Res> {
  __$$_GetAllContractorsOptionsCopyWithImpl(_$_GetAllContractorsOptions _value,
      $Res Function(_$_GetAllContractorsOptions) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetAllContractorsOptions implements _GetAllContractorsOptions {
  const _$_GetAllContractorsOptions();

  @override
  String toString() {
    return 'FieldTypeContractorEvent.getAllContractorsOptions()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetAllContractorsOptions);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllContractorsOptions,
  }) {
    return getAllContractorsOptions();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllContractorsOptions,
  }) {
    return getAllContractorsOptions?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllContractorsOptions,
    required TResult orElse(),
  }) {
    if (getAllContractorsOptions != null) {
      return getAllContractorsOptions();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllContractorsOptions value)
        getAllContractorsOptions,
  }) {
    return getAllContractorsOptions(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllContractorsOptions value)?
        getAllContractorsOptions,
  }) {
    return getAllContractorsOptions?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllContractorsOptions value)? getAllContractorsOptions,
    required TResult orElse(),
  }) {
    if (getAllContractorsOptions != null) {
      return getAllContractorsOptions(this);
    }
    return orElse();
  }
}

abstract class _GetAllContractorsOptions implements FieldTypeContractorEvent {
  const factory _GetAllContractorsOptions() = _$_GetAllContractorsOptions;
}

/// @nodoc
mixin _$FieldTypeContractorState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypeContractorStateCopyWith<$Res> {
  factory $FieldTypeContractorStateCopyWith(FieldTypeContractorState value,
          $Res Function(FieldTypeContractorState) then) =
      _$FieldTypeContractorStateCopyWithImpl<$Res, FieldTypeContractorState>;
}

/// @nodoc
class _$FieldTypeContractorStateCopyWithImpl<$Res,
        $Val extends FieldTypeContractorState>
    implements $FieldTypeContractorStateCopyWith<$Res> {
  _$FieldTypeContractorStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldTypeContractorStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldTypeContractorState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldTypeContractorState {
  const factory _Initial() = _$_Initial;
}
