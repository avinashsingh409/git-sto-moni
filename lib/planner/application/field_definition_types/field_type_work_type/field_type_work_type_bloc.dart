import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';

part 'field_type_work_type_event.dart';
part 'field_type_work_type_state.dart';
part 'field_type_work_type_bloc.freezed.dart';

@injectable
class FieldTypeWorkTypeBloc extends Bloc<FieldTypeWorkTypeEvent, FieldTypeWorkTypeState> {
  final IFieldDefinitionRepository fieldDefinitionRepository;
  List<WorkTypeOptionsDTO>? workTypes = [];
  FieldTypeWorkTypeBloc(this.fieldDefinitionRepository) : super(const _Initial()) {
    on<FieldTypeWorkTypeEvent>((event, emit) async {
      await event.when(initializeWorkTypes: () async {
        emit(const FieldTypeWorkTypeState.loading());
        workTypes = await fieldDefinitionRepository.getAllWorkTypeOptions();
        final currentWorkType = await fieldDefinitionRepository.getCurrentWorkType();
        if (workTypes != null) {
          emit(FieldTypeWorkTypeState.success(workTypes: workTypes ?? [], currentWorkType: currentWorkType));
        } else {
          emit(const FieldTypeWorkTypeState.empty());
        }
      }, updateInternalState: (WorkTypeOptionsDTO? selectedWorkType) {
        if (selectedWorkType != null) {
          emit(const FieldTypeWorkTypeState.loading());
          emit(FieldTypeWorkTypeState.success(workTypes: workTypes ?? [], currentWorkType: selectedWorkType));
        }
      });
    });
  }
}
