part of 'field_type_work_type_bloc.dart';

@freezed
class FieldTypeWorkTypeEvent with _$FieldTypeWorkTypeEvent {
  const factory FieldTypeWorkTypeEvent.initializeWorkTypes() = _InitializeWorkTypes;
  const factory FieldTypeWorkTypeEvent.updateInternalState({required WorkTypeOptionsDTO? selectedWorkType}) =
      _UpdateInternalState;
}
