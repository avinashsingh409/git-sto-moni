part of 'field_type_work_type_bloc.dart';

@freezed
class FieldTypeWorkTypeState with _$FieldTypeWorkTypeState {
  const factory FieldTypeWorkTypeState.initial() = _Initial;
  const factory FieldTypeWorkTypeState.loading() = _Loading;
  const factory FieldTypeWorkTypeState.success(
      {required List<WorkTypeOptionsDTO> workTypes, required WorkTypeOptionsDTO? currentWorkType}) = _Success;
  const factory FieldTypeWorkTypeState.empty() = _Empty;
}
