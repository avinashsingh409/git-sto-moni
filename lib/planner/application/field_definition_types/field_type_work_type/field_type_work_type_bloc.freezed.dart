// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_type_work_type_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldTypeWorkTypeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializeWorkTypes,
    required TResult Function(WorkTypeOptionsDTO? selectedWorkType)
        updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializeWorkTypes,
    TResult? Function(WorkTypeOptionsDTO? selectedWorkType)?
        updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializeWorkTypes,
    TResult Function(WorkTypeOptionsDTO? selectedWorkType)? updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializeWorkTypes value) initializeWorkTypes,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypeWorkTypeEventCopyWith<$Res> {
  factory $FieldTypeWorkTypeEventCopyWith(FieldTypeWorkTypeEvent value,
          $Res Function(FieldTypeWorkTypeEvent) then) =
      _$FieldTypeWorkTypeEventCopyWithImpl<$Res, FieldTypeWorkTypeEvent>;
}

/// @nodoc
class _$FieldTypeWorkTypeEventCopyWithImpl<$Res,
        $Val extends FieldTypeWorkTypeEvent>
    implements $FieldTypeWorkTypeEventCopyWith<$Res> {
  _$FieldTypeWorkTypeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitializeWorkTypesCopyWith<$Res> {
  factory _$$_InitializeWorkTypesCopyWith(_$_InitializeWorkTypes value,
          $Res Function(_$_InitializeWorkTypes) then) =
      __$$_InitializeWorkTypesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitializeWorkTypesCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeEventCopyWithImpl<$Res, _$_InitializeWorkTypes>
    implements _$$_InitializeWorkTypesCopyWith<$Res> {
  __$$_InitializeWorkTypesCopyWithImpl(_$_InitializeWorkTypes _value,
      $Res Function(_$_InitializeWorkTypes) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_InitializeWorkTypes implements _InitializeWorkTypes {
  const _$_InitializeWorkTypes();

  @override
  String toString() {
    return 'FieldTypeWorkTypeEvent.initializeWorkTypes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_InitializeWorkTypes);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializeWorkTypes,
    required TResult Function(WorkTypeOptionsDTO? selectedWorkType)
        updateInternalState,
  }) {
    return initializeWorkTypes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializeWorkTypes,
    TResult? Function(WorkTypeOptionsDTO? selectedWorkType)?
        updateInternalState,
  }) {
    return initializeWorkTypes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializeWorkTypes,
    TResult Function(WorkTypeOptionsDTO? selectedWorkType)? updateInternalState,
    required TResult orElse(),
  }) {
    if (initializeWorkTypes != null) {
      return initializeWorkTypes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializeWorkTypes value) initializeWorkTypes,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return initializeWorkTypes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return initializeWorkTypes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (initializeWorkTypes != null) {
      return initializeWorkTypes(this);
    }
    return orElse();
  }
}

abstract class _InitializeWorkTypes implements FieldTypeWorkTypeEvent {
  const factory _InitializeWorkTypes() = _$_InitializeWorkTypes;
}

/// @nodoc
abstract class _$$_UpdateInternalStateCopyWith<$Res> {
  factory _$$_UpdateInternalStateCopyWith(_$_UpdateInternalState value,
          $Res Function(_$_UpdateInternalState) then) =
      __$$_UpdateInternalStateCopyWithImpl<$Res>;
  @useResult
  $Res call({WorkTypeOptionsDTO? selectedWorkType});
}

/// @nodoc
class __$$_UpdateInternalStateCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeEventCopyWithImpl<$Res, _$_UpdateInternalState>
    implements _$$_UpdateInternalStateCopyWith<$Res> {
  __$$_UpdateInternalStateCopyWithImpl(_$_UpdateInternalState _value,
      $Res Function(_$_UpdateInternalState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedWorkType = freezed,
  }) {
    return _then(_$_UpdateInternalState(
      selectedWorkType: freezed == selectedWorkType
          ? _value.selectedWorkType
          : selectedWorkType // ignore: cast_nullable_to_non_nullable
              as WorkTypeOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_UpdateInternalState implements _UpdateInternalState {
  const _$_UpdateInternalState({required this.selectedWorkType});

  @override
  final WorkTypeOptionsDTO? selectedWorkType;

  @override
  String toString() {
    return 'FieldTypeWorkTypeEvent.updateInternalState(selectedWorkType: $selectedWorkType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateInternalState &&
            (identical(other.selectedWorkType, selectedWorkType) ||
                other.selectedWorkType == selectedWorkType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, selectedWorkType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      __$$_UpdateInternalStateCopyWithImpl<_$_UpdateInternalState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializeWorkTypes,
    required TResult Function(WorkTypeOptionsDTO? selectedWorkType)
        updateInternalState,
  }) {
    return updateInternalState(selectedWorkType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializeWorkTypes,
    TResult? Function(WorkTypeOptionsDTO? selectedWorkType)?
        updateInternalState,
  }) {
    return updateInternalState?.call(selectedWorkType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializeWorkTypes,
    TResult Function(WorkTypeOptionsDTO? selectedWorkType)? updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(selectedWorkType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializeWorkTypes value) initializeWorkTypes,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return updateInternalState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return updateInternalState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializeWorkTypes value)? initializeWorkTypes,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(this);
    }
    return orElse();
  }
}

abstract class _UpdateInternalState implements FieldTypeWorkTypeEvent {
  const factory _UpdateInternalState(
          {required final WorkTypeOptionsDTO? selectedWorkType}) =
      _$_UpdateInternalState;

  WorkTypeOptionsDTO? get selectedWorkType;
  @JsonKey(ignore: true)
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FieldTypeWorkTypeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)
        success,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult? Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypeWorkTypeStateCopyWith<$Res> {
  factory $FieldTypeWorkTypeStateCopyWith(FieldTypeWorkTypeState value,
          $Res Function(FieldTypeWorkTypeState) then) =
      _$FieldTypeWorkTypeStateCopyWithImpl<$Res, FieldTypeWorkTypeState>;
}

/// @nodoc
class _$FieldTypeWorkTypeStateCopyWithImpl<$Res,
        $Val extends FieldTypeWorkTypeState>
    implements $FieldTypeWorkTypeStateCopyWith<$Res> {
  _$FieldTypeWorkTypeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldTypeWorkTypeState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)
        success,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult? Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldTypeWorkTypeState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'FieldTypeWorkTypeState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)
        success,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult? Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements FieldTypeWorkTypeState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<WorkTypeOptionsDTO> workTypes,
      WorkTypeOptionsDTO? currentWorkType});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? workTypes = null,
    Object? currentWorkType = freezed,
  }) {
    return _then(_$_Success(
      workTypes: null == workTypes
          ? _value._workTypes
          : workTypes // ignore: cast_nullable_to_non_nullable
              as List<WorkTypeOptionsDTO>,
      currentWorkType: freezed == currentWorkType
          ? _value.currentWorkType
          : currentWorkType // ignore: cast_nullable_to_non_nullable
              as WorkTypeOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(
      {required final List<WorkTypeOptionsDTO> workTypes,
      required this.currentWorkType})
      : _workTypes = workTypes;

  final List<WorkTypeOptionsDTO> _workTypes;
  @override
  List<WorkTypeOptionsDTO> get workTypes {
    if (_workTypes is EqualUnmodifiableListView) return _workTypes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_workTypes);
  }

  @override
  final WorkTypeOptionsDTO? currentWorkType;

  @override
  String toString() {
    return 'FieldTypeWorkTypeState.success(workTypes: $workTypes, currentWorkType: $currentWorkType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._workTypes, _workTypes) &&
            (identical(other.currentWorkType, currentWorkType) ||
                other.currentWorkType == currentWorkType));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_workTypes), currentWorkType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)
        success,
    required TResult Function() empty,
  }) {
    return success(workTypes, currentWorkType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult? Function()? empty,
  }) {
    return success?.call(workTypes, currentWorkType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(workTypes, currentWorkType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements FieldTypeWorkTypeState {
  const factory _Success(
      {required final List<WorkTypeOptionsDTO> workTypes,
      required final WorkTypeOptionsDTO? currentWorkType}) = _$_Success;

  List<WorkTypeOptionsDTO> get workTypes;
  WorkTypeOptionsDTO? get currentWorkType;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmptyCopyWith<$Res> {
  factory _$$_EmptyCopyWith(_$_Empty value, $Res Function(_$_Empty) then) =
      __$$_EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyCopyWithImpl<$Res>
    extends _$FieldTypeWorkTypeStateCopyWithImpl<$Res, _$_Empty>
    implements _$$_EmptyCopyWith<$Res> {
  __$$_EmptyCopyWithImpl(_$_Empty _value, $Res Function(_$_Empty) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'FieldTypeWorkTypeState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)
        success,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult? Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkTypeOptionsDTO> workTypes,
            WorkTypeOptionsDTO? currentWorkType)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements FieldTypeWorkTypeState {
  const factory _Empty() = _$_Empty;
}
