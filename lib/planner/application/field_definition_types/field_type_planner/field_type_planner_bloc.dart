import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';

part 'field_type_planner_event.dart';
part 'field_type_planner_state.dart';
part 'field_type_planner_bloc.freezed.dart';

@injectable
class FieldTypePlannerBloc extends Bloc<FieldTypePlannerEvent, FieldTypePlannerState> {
  final IFieldDefinitionRepository fieldDefinitionRepository;
  List<UserOptionsDTO>? planners = [];
  FieldTypePlannerBloc(this.fieldDefinitionRepository) : super(const _Initial()) {
    on<FieldTypePlannerEvent>((event, emit) async {
      await event.when(getAllPlannerOptions: () async {
        emit(const FieldTypePlannerState.loading());
        planners = await fieldDefinitionRepository.getAllPlannerOptions();
        final currentPlanner = await fieldDefinitionRepository.getCurrentPlanner();
        if (planners != null) {
          emit(FieldTypePlannerState.success(planners: planners ?? [], currentPlanner: currentPlanner));
        } else {
          emit(const FieldTypePlannerState.empty());
        }
      }, updateInternalState: (selectedPlanner) {
        if (selectedPlanner != null) {
          emit(const FieldTypePlannerState.loading());
          emit(FieldTypePlannerState.success(planners: planners ?? [], currentPlanner: selectedPlanner));
        }
      });
    });
  }
}
