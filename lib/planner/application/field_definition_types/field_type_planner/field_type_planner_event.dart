part of 'field_type_planner_bloc.dart';

@freezed
class FieldTypePlannerEvent with _$FieldTypePlannerEvent {
  const factory FieldTypePlannerEvent.getAllPlannerOptions() = _GetAllPlannerOptions;
  const factory FieldTypePlannerEvent.updateInternalState({required UserOptionsDTO? selectedPlanner}) = _UpdateInternalState;
}
