// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_type_planner_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldTypePlannerEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllPlannerOptions,
    required TResult Function(UserOptionsDTO? selectedPlanner)
        updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllPlannerOptions,
    TResult? Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllPlannerOptions,
    TResult Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllPlannerOptions value) getAllPlannerOptions,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypePlannerEventCopyWith<$Res> {
  factory $FieldTypePlannerEventCopyWith(FieldTypePlannerEvent value,
          $Res Function(FieldTypePlannerEvent) then) =
      _$FieldTypePlannerEventCopyWithImpl<$Res, FieldTypePlannerEvent>;
}

/// @nodoc
class _$FieldTypePlannerEventCopyWithImpl<$Res,
        $Val extends FieldTypePlannerEvent>
    implements $FieldTypePlannerEventCopyWith<$Res> {
  _$FieldTypePlannerEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetAllPlannerOptionsCopyWith<$Res> {
  factory _$$_GetAllPlannerOptionsCopyWith(_$_GetAllPlannerOptions value,
          $Res Function(_$_GetAllPlannerOptions) then) =
      __$$_GetAllPlannerOptionsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetAllPlannerOptionsCopyWithImpl<$Res>
    extends _$FieldTypePlannerEventCopyWithImpl<$Res, _$_GetAllPlannerOptions>
    implements _$$_GetAllPlannerOptionsCopyWith<$Res> {
  __$$_GetAllPlannerOptionsCopyWithImpl(_$_GetAllPlannerOptions _value,
      $Res Function(_$_GetAllPlannerOptions) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetAllPlannerOptions implements _GetAllPlannerOptions {
  const _$_GetAllPlannerOptions();

  @override
  String toString() {
    return 'FieldTypePlannerEvent.getAllPlannerOptions()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetAllPlannerOptions);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllPlannerOptions,
    required TResult Function(UserOptionsDTO? selectedPlanner)
        updateInternalState,
  }) {
    return getAllPlannerOptions();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllPlannerOptions,
    TResult? Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
  }) {
    return getAllPlannerOptions?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllPlannerOptions,
    TResult Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
    required TResult orElse(),
  }) {
    if (getAllPlannerOptions != null) {
      return getAllPlannerOptions();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllPlannerOptions value) getAllPlannerOptions,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return getAllPlannerOptions(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return getAllPlannerOptions?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (getAllPlannerOptions != null) {
      return getAllPlannerOptions(this);
    }
    return orElse();
  }
}

abstract class _GetAllPlannerOptions implements FieldTypePlannerEvent {
  const factory _GetAllPlannerOptions() = _$_GetAllPlannerOptions;
}

/// @nodoc
abstract class _$$_UpdateInternalStateCopyWith<$Res> {
  factory _$$_UpdateInternalStateCopyWith(_$_UpdateInternalState value,
          $Res Function(_$_UpdateInternalState) then) =
      __$$_UpdateInternalStateCopyWithImpl<$Res>;
  @useResult
  $Res call({UserOptionsDTO? selectedPlanner});
}

/// @nodoc
class __$$_UpdateInternalStateCopyWithImpl<$Res>
    extends _$FieldTypePlannerEventCopyWithImpl<$Res, _$_UpdateInternalState>
    implements _$$_UpdateInternalStateCopyWith<$Res> {
  __$$_UpdateInternalStateCopyWithImpl(_$_UpdateInternalState _value,
      $Res Function(_$_UpdateInternalState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedPlanner = freezed,
  }) {
    return _then(_$_UpdateInternalState(
      selectedPlanner: freezed == selectedPlanner
          ? _value.selectedPlanner
          : selectedPlanner // ignore: cast_nullable_to_non_nullable
              as UserOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_UpdateInternalState implements _UpdateInternalState {
  const _$_UpdateInternalState({required this.selectedPlanner});

  @override
  final UserOptionsDTO? selectedPlanner;

  @override
  String toString() {
    return 'FieldTypePlannerEvent.updateInternalState(selectedPlanner: $selectedPlanner)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateInternalState &&
            (identical(other.selectedPlanner, selectedPlanner) ||
                other.selectedPlanner == selectedPlanner));
  }

  @override
  int get hashCode => Object.hash(runtimeType, selectedPlanner);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      __$$_UpdateInternalStateCopyWithImpl<_$_UpdateInternalState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllPlannerOptions,
    required TResult Function(UserOptionsDTO? selectedPlanner)
        updateInternalState,
  }) {
    return updateInternalState(selectedPlanner);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllPlannerOptions,
    TResult? Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
  }) {
    return updateInternalState?.call(selectedPlanner);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllPlannerOptions,
    TResult Function(UserOptionsDTO? selectedPlanner)? updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(selectedPlanner);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllPlannerOptions value) getAllPlannerOptions,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return updateInternalState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return updateInternalState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllPlannerOptions value)? getAllPlannerOptions,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(this);
    }
    return orElse();
  }
}

abstract class _UpdateInternalState implements FieldTypePlannerEvent {
  const factory _UpdateInternalState(
          {required final UserOptionsDTO? selectedPlanner}) =
      _$_UpdateInternalState;

  UserOptionsDTO? get selectedPlanner;
  @JsonKey(ignore: true)
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FieldTypePlannerState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)
        success,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult? Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypePlannerStateCopyWith<$Res> {
  factory $FieldTypePlannerStateCopyWith(FieldTypePlannerState value,
          $Res Function(FieldTypePlannerState) then) =
      _$FieldTypePlannerStateCopyWithImpl<$Res, FieldTypePlannerState>;
}

/// @nodoc
class _$FieldTypePlannerStateCopyWithImpl<$Res,
        $Val extends FieldTypePlannerState>
    implements $FieldTypePlannerStateCopyWith<$Res> {
  _$FieldTypePlannerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldTypePlannerStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldTypePlannerState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)
        success,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult? Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldTypePlannerState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$FieldTypePlannerStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'FieldTypePlannerState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)
        success,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult? Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements FieldTypePlannerState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$FieldTypePlannerStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? planners = null,
    Object? currentPlanner = freezed,
  }) {
    return _then(_$_Success(
      planners: null == planners
          ? _value._planners
          : planners // ignore: cast_nullable_to_non_nullable
              as List<UserOptionsDTO>,
      currentPlanner: freezed == currentPlanner
          ? _value.currentPlanner
          : currentPlanner // ignore: cast_nullable_to_non_nullable
              as UserOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(
      {required final List<UserOptionsDTO> planners,
      required this.currentPlanner})
      : _planners = planners;

  final List<UserOptionsDTO> _planners;
  @override
  List<UserOptionsDTO> get planners {
    if (_planners is EqualUnmodifiableListView) return _planners;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_planners);
  }

  @override
  final UserOptionsDTO? currentPlanner;

  @override
  String toString() {
    return 'FieldTypePlannerState.success(planners: $planners, currentPlanner: $currentPlanner)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._planners, _planners) &&
            (identical(other.currentPlanner, currentPlanner) ||
                other.currentPlanner == currentPlanner));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_planners), currentPlanner);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)
        success,
    required TResult Function() empty,
  }) {
    return success(planners, currentPlanner);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult? Function()? empty,
  }) {
    return success?.call(planners, currentPlanner);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(planners, currentPlanner);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements FieldTypePlannerState {
  const factory _Success(
      {required final List<UserOptionsDTO> planners,
      required final UserOptionsDTO? currentPlanner}) = _$_Success;

  List<UserOptionsDTO> get planners;
  UserOptionsDTO? get currentPlanner;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmptyCopyWith<$Res> {
  factory _$$_EmptyCopyWith(_$_Empty value, $Res Function(_$_Empty) then) =
      __$$_EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyCopyWithImpl<$Res>
    extends _$FieldTypePlannerStateCopyWithImpl<$Res, _$_Empty>
    implements _$$_EmptyCopyWith<$Res> {
  __$$_EmptyCopyWithImpl(_$_Empty _value, $Res Function(_$_Empty) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'FieldTypePlannerState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)
        success,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult? Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(
            List<UserOptionsDTO> planners, UserOptionsDTO? currentPlanner)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements FieldTypePlannerState {
  const factory _Empty() = _$_Empty;
}
