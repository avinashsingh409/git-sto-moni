part of 'field_type_planner_bloc.dart';

@freezed
class FieldTypePlannerState with _$FieldTypePlannerState {
  const factory FieldTypePlannerState.initial() = _Initial;
  const factory FieldTypePlannerState.loading() = _Loading;
  const factory FieldTypePlannerState.success({required List<UserOptionsDTO> planners, required UserOptionsDTO? currentPlanner}) =
      _Success;
  const factory FieldTypePlannerState.empty() = _Empty;
}
