part of 'field_type_lookup_cubit.dart';

@freezed
class FieldTypeLookupState with _$FieldTypeLookupState {
  const factory FieldTypeLookupState.initial() = _Initial;
  const factory FieldTypeLookupState.dropdownCreated({
    required List<LookupValueDTO> options,
    required String? currentSelected,
  }) = _DropdownCreated;
}
