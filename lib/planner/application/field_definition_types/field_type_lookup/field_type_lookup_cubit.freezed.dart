// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_type_lookup_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldTypeLookupState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(
            List<LookupValueDTO> options, String? currentSelected)
        dropdownCreated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_DropdownCreated value) dropdownCreated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_DropdownCreated value)? dropdownCreated,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_DropdownCreated value)? dropdownCreated,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypeLookupStateCopyWith<$Res> {
  factory $FieldTypeLookupStateCopyWith(FieldTypeLookupState value,
          $Res Function(FieldTypeLookupState) then) =
      _$FieldTypeLookupStateCopyWithImpl<$Res, FieldTypeLookupState>;
}

/// @nodoc
class _$FieldTypeLookupStateCopyWithImpl<$Res,
        $Val extends FieldTypeLookupState>
    implements $FieldTypeLookupStateCopyWith<$Res> {
  _$FieldTypeLookupStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldTypeLookupStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldTypeLookupState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(
            List<LookupValueDTO> options, String? currentSelected)
        dropdownCreated,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_DropdownCreated value) dropdownCreated,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_DropdownCreated value)? dropdownCreated,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_DropdownCreated value)? dropdownCreated,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldTypeLookupState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_DropdownCreatedCopyWith<$Res> {
  factory _$$_DropdownCreatedCopyWith(
          _$_DropdownCreated value, $Res Function(_$_DropdownCreated) then) =
      __$$_DropdownCreatedCopyWithImpl<$Res>;
  @useResult
  $Res call({List<LookupValueDTO> options, String? currentSelected});
}

/// @nodoc
class __$$_DropdownCreatedCopyWithImpl<$Res>
    extends _$FieldTypeLookupStateCopyWithImpl<$Res, _$_DropdownCreated>
    implements _$$_DropdownCreatedCopyWith<$Res> {
  __$$_DropdownCreatedCopyWithImpl(
      _$_DropdownCreated _value, $Res Function(_$_DropdownCreated) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? options = null,
    Object? currentSelected = freezed,
  }) {
    return _then(_$_DropdownCreated(
      options: null == options
          ? _value._options
          : options // ignore: cast_nullable_to_non_nullable
              as List<LookupValueDTO>,
      currentSelected: freezed == currentSelected
          ? _value.currentSelected
          : currentSelected // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_DropdownCreated implements _DropdownCreated {
  const _$_DropdownCreated(
      {required final List<LookupValueDTO> options,
      required this.currentSelected})
      : _options = options;

  final List<LookupValueDTO> _options;
  @override
  List<LookupValueDTO> get options {
    if (_options is EqualUnmodifiableListView) return _options;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_options);
  }

  @override
  final String? currentSelected;

  @override
  String toString() {
    return 'FieldTypeLookupState.dropdownCreated(options: $options, currentSelected: $currentSelected)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DropdownCreated &&
            const DeepCollectionEquality().equals(other._options, _options) &&
            (identical(other.currentSelected, currentSelected) ||
                other.currentSelected == currentSelected));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_options), currentSelected);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DropdownCreatedCopyWith<_$_DropdownCreated> get copyWith =>
      __$$_DropdownCreatedCopyWithImpl<_$_DropdownCreated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(
            List<LookupValueDTO> options, String? currentSelected)
        dropdownCreated,
  }) {
    return dropdownCreated(options, currentSelected);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
  }) {
    return dropdownCreated?.call(options, currentSelected);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<LookupValueDTO> options, String? currentSelected)?
        dropdownCreated,
    required TResult orElse(),
  }) {
    if (dropdownCreated != null) {
      return dropdownCreated(options, currentSelected);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_DropdownCreated value) dropdownCreated,
  }) {
    return dropdownCreated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_DropdownCreated value)? dropdownCreated,
  }) {
    return dropdownCreated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_DropdownCreated value)? dropdownCreated,
    required TResult orElse(),
  }) {
    if (dropdownCreated != null) {
      return dropdownCreated(this);
    }
    return orElse();
  }
}

abstract class _DropdownCreated implements FieldTypeLookupState {
  const factory _DropdownCreated(
      {required final List<LookupValueDTO> options,
      required final String? currentSelected}) = _$_DropdownCreated;

  List<LookupValueDTO> get options;
  String? get currentSelected;
  @JsonKey(ignore: true)
  _$$_DropdownCreatedCopyWith<_$_DropdownCreated> get copyWith =>
      throw _privateConstructorUsedError;
}
