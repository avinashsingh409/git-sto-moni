import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

part 'field_type_lookup_state.dart';
part 'field_type_lookup_cubit.freezed.dart';

@injectable
class FieldTypeLookupCubit extends Cubit<FieldTypeLookupState> {
  final List<LookupValueDTO> _dropdownMenuItems = [];
  FieldTypeLookupCubit() : super(const FieldTypeLookupState.initial());

  void initializeDropdownItems({required WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition}) {
    final listOfOptions = unparsedWorkpackageFieldDefinition.definition?.lookup?.values;
    _dropdownMenuItems.add(LookupValueDTO(caption: '', value: 999223999999));
    if (listOfOptions != null) {
      for (LookupValueDTO listItem in listOfOptions) {
        _dropdownMenuItems.add(listItem);
      }
      emit(FieldTypeLookupState.dropdownCreated(
          options: _dropdownMenuItems, currentSelected: unparsedWorkpackageFieldDefinition.value));
    }
  }

  void changeDropDownItem({required String? newOption}) {
    if (newOption != null) {
      emit(const FieldTypeLookupState.initial());
      emit(FieldTypeLookupState.dropdownCreated(options: _dropdownMenuItems, currentSelected: newOption));
    }
  }
}
