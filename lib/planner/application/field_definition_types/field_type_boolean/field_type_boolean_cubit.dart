import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/mappers/field_definition_value_mapper.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

part 'field_type_boolean_state.dart';
part 'field_type_boolean_cubit.freezed.dart';

@injectable
class FieldTypeBooleanCubit extends Cubit<FieldTypeBooleanState> {
  final FieldDefinitionValueMapper fieldDefinitionValueMapper;
  bool? boolVal;
  FieldTypeBooleanCubit(this.fieldDefinitionValueMapper) : super(const FieldTypeBooleanState.initial());

  void intializeBoolValue({required WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition}) {
    final unparsedWorkpackageFieldDefinitionValue = unparsedWorkpackageFieldDefinition.value;
    if (unparsedWorkpackageFieldDefinitionValue != null) {
      boolVal = fieldDefinitionValueMapper.mapAndReturnBoolVal(boolVal: unparsedWorkpackageFieldDefinitionValue);
      emit(FieldTypeBooleanState.valueChanged(changedValue: boolVal));
    }
    emit(FieldTypeBooleanState.valueChanged(changedValue: boolVal));
  }

  void changeBoolVal({required bool changedValue}) {
    emit(const FieldTypeBooleanState.initial());
    boolVal = changedValue;
    emit(FieldTypeBooleanState.valueChanged(changedValue: boolVal));
  }
}
