part of 'field_type_boolean_cubit.dart';

@freezed
class FieldTypeBooleanState with _$FieldTypeBooleanState {
  const factory FieldTypeBooleanState.initial() = _Initial;
  const factory FieldTypeBooleanState.valueChanged({required bool? changedValue}) = _ValueChanged;
}
