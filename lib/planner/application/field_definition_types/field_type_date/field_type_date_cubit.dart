import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/mappers/field_definition_value_mapper.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/enums/field_type.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

part 'field_type_date_state.dart';
part 'field_type_date_cubit.freezed.dart';

@injectable
class FieldTypeDateCubit extends Cubit<FieldTypeDateState> {
  final FieldDefinitionValueMapper fieldDefinitionValueMapper;
  DateTime? dateVal;
  FieldTypeDateCubit(this.fieldDefinitionValueMapper) : super(const FieldTypeDateState.initial());

  void intializedDateValue({required WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition}) {
    final unparsedWorkpackageFieldDefinitionValue = unparsedWorkpackageFieldDefinition.value;
    if (unparsedWorkpackageFieldDefinitionValue != null) {
      _emitInitialDateState(
          unparsedWorkpackageFieldDefinition: unparsedWorkpackageFieldDefinition,
          unparsedWorkpackageFieldDefinitionValue: unparsedWorkpackageFieldDefinitionValue);
    } else {
      emit(const FieldTypeDateState.dateChanged(
        dateVal: null,
      ));
    }
  }

  /*
    These are the method blocks where states are emitted. To make the code for business logic more
    clearer and removing nested if else for readability.
  */

  void _emitInitialDateState(
      {required WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition,
      required String unparsedWorkpackageFieldDefinitionValue}) {
    dateVal = FieldDefinitionValueMapper().mapAndReturnDateTime(dateTimeString: unparsedWorkpackageFieldDefinitionValue);
    if (fieldDefinitionIsDateTime(unparsedWorkpackageFieldDefinition)) {
      _emitFreshDateState();
    } else {
      emit(FieldTypeDateState.dateChanged(
        dateVal: dateVal,
      ));
    }
  }

  void _emitFreshDateState() {
    emit(const FieldTypeDateState.initial());
    emit(FieldTypeDateState.dateChanged(
      dateVal: dateVal,
    ));
  }

  /*
    Date and time selector methods. These are the date and time selector methods that calls up
    flutter method to invoke the date time pickers
  */
  Future<String?> selectDate({required WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition}) async {
    final currentDate = DateTime.now();
    final DateTime? picked = await showDatePicker(
      context: GlobalNavigationKey().context!,
      initialDate: dateVal ?? DateTime.now(),
      firstDate:
          dateVal != null ? DateTime(dateVal!.year, dateVal!.month - 3) : DateTime(currentDate.year, currentDate.month - 3),
      lastDate: dateVal != null ? DateTime(dateVal!.year + 10) : DateTime(currentDate.year + 10),
    );
    if (picked != null) {
      dateVal = picked;
      _emitFreshDateState();
    }
    if (fieldDefinitionIsDateTime(unparsedWorkpackageFieldDefinition)) {
      await selectTime();
    }
    if (dateVal != null) {
      try {
        final iso8601String = dateVal!.toIso8601String();
        return iso8601String;
      } catch (_) {
        //
      }
      _emitFreshDateState();
    }
    return null;
  }

  Future<void> selectTime() async {
    final TimeOfDay? picked = await showTimePicker(
      context: GlobalNavigationKey().context!,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null) {
      updateTime(newTime: picked);
      _emitFreshDateState();
    }
  }

  void updateTime({required TimeOfDay newTime}) {
    if (dateVal != null) {
      dateVal = dateVal.copyWith(
          year: dateVal!.year, month: dateVal!.month, day: dateVal!.day, hour: newTime.hour, minute: newTime.minute);
    }
  }
}

//Method to identify if the dateField is dateTime instead of Date
bool fieldDefinitionIsDateTime(WorkpackageFieldDefinitionDTO unparsedWorkpackageFieldDefinition) =>
    unparsedWorkpackageFieldDefinition.definition?.fieldType == FieldType.datetime.name.toString();

//Extension for Changing Date
extension MyDateUtils on DateTime? {
  DateTime copyWith({
    int? year,
    int? month,
    int? day,
    int? hour,
    int? minute,
    int? second,
    int? millisecond,
    int? microsecond,
  }) {
    return DateTime(
      year ?? this!.year,
      month ?? this!.month,
      day ?? this!.day,
      hour ?? this!.hour,
      minute ?? this!.minute,
      second ?? this!.second,
      millisecond ?? this!.millisecond,
      microsecond ?? this!.microsecond,
    );
  }
}
