part of 'field_type_date_cubit.dart';

@freezed
class FieldTypeDateState with _$FieldTypeDateState {
  const factory FieldTypeDateState.initial() = _Initial;
  const factory FieldTypeDateState.dateChanged({
    required DateTime? dateVal,
  }) = _DateChanged;
}
