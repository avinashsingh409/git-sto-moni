part of 'field_type_primary_contractor_bloc.dart';

@freezed
class FieldTypePrimaryContractorEvent with _$FieldTypePrimaryContractorEvent {
  const factory FieldTypePrimaryContractorEvent.initializePrimaryContractors() = _InitializePrimaryContractors;
  const factory FieldTypePrimaryContractorEvent.updateInternalState({required ContractorOptionsDTO? selectedContractor}) =
      _UpdateInternalState;
}
