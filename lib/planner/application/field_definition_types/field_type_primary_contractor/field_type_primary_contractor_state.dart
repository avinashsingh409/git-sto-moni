part of 'field_type_primary_contractor_bloc.dart';

@freezed
class FieldTypePrimaryContractorState with _$FieldTypePrimaryContractorState {
  const factory FieldTypePrimaryContractorState.initial() = _Initial;
  const factory FieldTypePrimaryContractorState.loading() = _Loading;
  const factory FieldTypePrimaryContractorState.success(
      {required List<ContractorOptionsDTO> contractors, required ContractorOptionsDTO? currentContractor}) = _Success;
  const factory FieldTypePrimaryContractorState.empty() = _Empty;
}
