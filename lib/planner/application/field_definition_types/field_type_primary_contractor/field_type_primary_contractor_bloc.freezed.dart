// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_type_primary_contractor_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FieldTypePrimaryContractorEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializePrimaryContractors,
    required TResult Function(ContractorOptionsDTO? selectedContractor)
        updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializePrimaryContractors,
    TResult? Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializePrimaryContractors,
    TResult Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializePrimaryContractors value)
        initializePrimaryContractors,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypePrimaryContractorEventCopyWith<$Res> {
  factory $FieldTypePrimaryContractorEventCopyWith(
          FieldTypePrimaryContractorEvent value,
          $Res Function(FieldTypePrimaryContractorEvent) then) =
      _$FieldTypePrimaryContractorEventCopyWithImpl<$Res,
          FieldTypePrimaryContractorEvent>;
}

/// @nodoc
class _$FieldTypePrimaryContractorEventCopyWithImpl<$Res,
        $Val extends FieldTypePrimaryContractorEvent>
    implements $FieldTypePrimaryContractorEventCopyWith<$Res> {
  _$FieldTypePrimaryContractorEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitializePrimaryContractorsCopyWith<$Res> {
  factory _$$_InitializePrimaryContractorsCopyWith(
          _$_InitializePrimaryContractors value,
          $Res Function(_$_InitializePrimaryContractors) then) =
      __$$_InitializePrimaryContractorsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitializePrimaryContractorsCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorEventCopyWithImpl<$Res,
        _$_InitializePrimaryContractors>
    implements _$$_InitializePrimaryContractorsCopyWith<$Res> {
  __$$_InitializePrimaryContractorsCopyWithImpl(
      _$_InitializePrimaryContractors _value,
      $Res Function(_$_InitializePrimaryContractors) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_InitializePrimaryContractors implements _InitializePrimaryContractors {
  const _$_InitializePrimaryContractors();

  @override
  String toString() {
    return 'FieldTypePrimaryContractorEvent.initializePrimaryContractors()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InitializePrimaryContractors);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializePrimaryContractors,
    required TResult Function(ContractorOptionsDTO? selectedContractor)
        updateInternalState,
  }) {
    return initializePrimaryContractors();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializePrimaryContractors,
    TResult? Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
  }) {
    return initializePrimaryContractors?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializePrimaryContractors,
    TResult Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
    required TResult orElse(),
  }) {
    if (initializePrimaryContractors != null) {
      return initializePrimaryContractors();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializePrimaryContractors value)
        initializePrimaryContractors,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return initializePrimaryContractors(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return initializePrimaryContractors?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (initializePrimaryContractors != null) {
      return initializePrimaryContractors(this);
    }
    return orElse();
  }
}

abstract class _InitializePrimaryContractors
    implements FieldTypePrimaryContractorEvent {
  const factory _InitializePrimaryContractors() =
      _$_InitializePrimaryContractors;
}

/// @nodoc
abstract class _$$_UpdateInternalStateCopyWith<$Res> {
  factory _$$_UpdateInternalStateCopyWith(_$_UpdateInternalState value,
          $Res Function(_$_UpdateInternalState) then) =
      __$$_UpdateInternalStateCopyWithImpl<$Res>;
  @useResult
  $Res call({ContractorOptionsDTO? selectedContractor});
}

/// @nodoc
class __$$_UpdateInternalStateCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorEventCopyWithImpl<$Res,
        _$_UpdateInternalState>
    implements _$$_UpdateInternalStateCopyWith<$Res> {
  __$$_UpdateInternalStateCopyWithImpl(_$_UpdateInternalState _value,
      $Res Function(_$_UpdateInternalState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedContractor = freezed,
  }) {
    return _then(_$_UpdateInternalState(
      selectedContractor: freezed == selectedContractor
          ? _value.selectedContractor
          : selectedContractor // ignore: cast_nullable_to_non_nullable
              as ContractorOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_UpdateInternalState implements _UpdateInternalState {
  const _$_UpdateInternalState({required this.selectedContractor});

  @override
  final ContractorOptionsDTO? selectedContractor;

  @override
  String toString() {
    return 'FieldTypePrimaryContractorEvent.updateInternalState(selectedContractor: $selectedContractor)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateInternalState &&
            (identical(other.selectedContractor, selectedContractor) ||
                other.selectedContractor == selectedContractor));
  }

  @override
  int get hashCode => Object.hash(runtimeType, selectedContractor);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      __$$_UpdateInternalStateCopyWithImpl<_$_UpdateInternalState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initializePrimaryContractors,
    required TResult Function(ContractorOptionsDTO? selectedContractor)
        updateInternalState,
  }) {
    return updateInternalState(selectedContractor);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initializePrimaryContractors,
    TResult? Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
  }) {
    return updateInternalState?.call(selectedContractor);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initializePrimaryContractors,
    TResult Function(ContractorOptionsDTO? selectedContractor)?
        updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(selectedContractor);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitializePrimaryContractors value)
        initializePrimaryContractors,
    required TResult Function(_UpdateInternalState value) updateInternalState,
  }) {
    return updateInternalState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult? Function(_UpdateInternalState value)? updateInternalState,
  }) {
    return updateInternalState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitializePrimaryContractors value)?
        initializePrimaryContractors,
    TResult Function(_UpdateInternalState value)? updateInternalState,
    required TResult orElse(),
  }) {
    if (updateInternalState != null) {
      return updateInternalState(this);
    }
    return orElse();
  }
}

abstract class _UpdateInternalState implements FieldTypePrimaryContractorEvent {
  const factory _UpdateInternalState(
          {required final ContractorOptionsDTO? selectedContractor}) =
      _$_UpdateInternalState;

  ContractorOptionsDTO? get selectedContractor;
  @JsonKey(ignore: true)
  _$$_UpdateInternalStateCopyWith<_$_UpdateInternalState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FieldTypePrimaryContractorState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)
        success,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult? Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldTypePrimaryContractorStateCopyWith<$Res> {
  factory $FieldTypePrimaryContractorStateCopyWith(
          FieldTypePrimaryContractorState value,
          $Res Function(FieldTypePrimaryContractorState) then) =
      _$FieldTypePrimaryContractorStateCopyWithImpl<$Res,
          FieldTypePrimaryContractorState>;
}

/// @nodoc
class _$FieldTypePrimaryContractorStateCopyWithImpl<$Res,
        $Val extends FieldTypePrimaryContractorState>
    implements $FieldTypePrimaryContractorStateCopyWith<$Res> {
  _$FieldTypePrimaryContractorStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FieldTypePrimaryContractorState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)
        success,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult? Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FieldTypePrimaryContractorState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'FieldTypePrimaryContractorState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)
        success,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult? Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements FieldTypePrimaryContractorState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<ContractorOptionsDTO> contractors,
      ContractorOptionsDTO? currentContractor});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? contractors = null,
    Object? currentContractor = freezed,
  }) {
    return _then(_$_Success(
      contractors: null == contractors
          ? _value._contractors
          : contractors // ignore: cast_nullable_to_non_nullable
              as List<ContractorOptionsDTO>,
      currentContractor: freezed == currentContractor
          ? _value.currentContractor
          : currentContractor // ignore: cast_nullable_to_non_nullable
              as ContractorOptionsDTO?,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success(
      {required final List<ContractorOptionsDTO> contractors,
      required this.currentContractor})
      : _contractors = contractors;

  final List<ContractorOptionsDTO> _contractors;
  @override
  List<ContractorOptionsDTO> get contractors {
    if (_contractors is EqualUnmodifiableListView) return _contractors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_contractors);
  }

  @override
  final ContractorOptionsDTO? currentContractor;

  @override
  String toString() {
    return 'FieldTypePrimaryContractorState.success(contractors: $contractors, currentContractor: $currentContractor)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._contractors, _contractors) &&
            (identical(other.currentContractor, currentContractor) ||
                other.currentContractor == currentContractor));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_contractors), currentContractor);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)
        success,
    required TResult Function() empty,
  }) {
    return success(contractors, currentContractor);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult? Function()? empty,
  }) {
    return success?.call(contractors, currentContractor);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(contractors, currentContractor);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements FieldTypePrimaryContractorState {
  const factory _Success(
      {required final List<ContractorOptionsDTO> contractors,
      required final ContractorOptionsDTO? currentContractor}) = _$_Success;

  List<ContractorOptionsDTO> get contractors;
  ContractorOptionsDTO? get currentContractor;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmptyCopyWith<$Res> {
  factory _$$_EmptyCopyWith(_$_Empty value, $Res Function(_$_Empty) then) =
      __$$_EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyCopyWithImpl<$Res>
    extends _$FieldTypePrimaryContractorStateCopyWithImpl<$Res, _$_Empty>
    implements _$$_EmptyCopyWith<$Res> {
  __$$_EmptyCopyWithImpl(_$_Empty _value, $Res Function(_$_Empty) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'FieldTypePrimaryContractorState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)
        success,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult? Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ContractorOptionsDTO> contractors,
            ContractorOptionsDTO? currentContractor)?
        success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements FieldTypePrimaryContractorState {
  const factory _Empty() = _$_Empty;
}
