import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';

part 'field_type_primary_contractor_event.dart';
part 'field_type_primary_contractor_state.dart';
part 'field_type_primary_contractor_bloc.freezed.dart';

@injectable
class FieldTypePrimaryContractorBloc extends Bloc<FieldTypePrimaryContractorEvent, FieldTypePrimaryContractorState> {
  final IFieldDefinitionRepository fieldDefinitionRepository;
  List<ContractorOptionsDTO>? contractors = [];
  FieldTypePrimaryContractorBloc(this.fieldDefinitionRepository) : super(const _Initial()) {
    on<FieldTypePrimaryContractorEvent>((event, emit) async {
      await event.when(initializePrimaryContractors: () async {
        emit(const FieldTypePrimaryContractorState.loading());
        contractors = await fieldDefinitionRepository.getAllContractorsOptions();
        final currentContractor = await fieldDefinitionRepository.getCurrentContractor();
        if (contractors != null) {
          emit(FieldTypePrimaryContractorState.success(contractors: contractors ?? [], currentContractor: currentContractor));
        } else {
          emit(const FieldTypePrimaryContractorState.empty());
        }
      }, updateInternalState: (selectedContractor) {
        if (selectedContractor != null) {
          emit(const FieldTypePrimaryContractorState.loading());
          emit(FieldTypePrimaryContractorState.success(contractors: contractors ?? [], currentContractor: selectedContractor));
        }
      });
    });
  }
}
