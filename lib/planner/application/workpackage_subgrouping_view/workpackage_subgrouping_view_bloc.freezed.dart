// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'workpackage_subgrouping_view_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$WorkpackageSubgroupingViewEvent {
  List<WorkpackageSubgroupDTO> get subgroups =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups)
        getSubgroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? getSubgroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? getSubgroups,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSubgroups value) getSubgroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSubgroups value)? getSubgroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSubgroups value)? getSubgroups,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WorkpackageSubgroupingViewEventCopyWith<WorkpackageSubgroupingViewEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkpackageSubgroupingViewEventCopyWith<$Res> {
  factory $WorkpackageSubgroupingViewEventCopyWith(
          WorkpackageSubgroupingViewEvent value,
          $Res Function(WorkpackageSubgroupingViewEvent) then) =
      _$WorkpackageSubgroupingViewEventCopyWithImpl<$Res,
          WorkpackageSubgroupingViewEvent>;
  @useResult
  $Res call({List<WorkpackageSubgroupDTO> subgroups});
}

/// @nodoc
class _$WorkpackageSubgroupingViewEventCopyWithImpl<$Res,
        $Val extends WorkpackageSubgroupingViewEvent>
    implements $WorkpackageSubgroupingViewEventCopyWith<$Res> {
  _$WorkpackageSubgroupingViewEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subgroups = null,
  }) {
    return _then(_value.copyWith(
      subgroups: null == subgroups
          ? _value.subgroups
          : subgroups // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageSubgroupDTO>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GetSubgroupsCopyWith<$Res>
    implements $WorkpackageSubgroupingViewEventCopyWith<$Res> {
  factory _$$_GetSubgroupsCopyWith(
          _$_GetSubgroups value, $Res Function(_$_GetSubgroups) then) =
      __$$_GetSubgroupsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<WorkpackageSubgroupDTO> subgroups});
}

/// @nodoc
class __$$_GetSubgroupsCopyWithImpl<$Res>
    extends _$WorkpackageSubgroupingViewEventCopyWithImpl<$Res, _$_GetSubgroups>
    implements _$$_GetSubgroupsCopyWith<$Res> {
  __$$_GetSubgroupsCopyWithImpl(
      _$_GetSubgroups _value, $Res Function(_$_GetSubgroups) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subgroups = null,
  }) {
    return _then(_$_GetSubgroups(
      subgroups: null == subgroups
          ? _value._subgroups
          : subgroups // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageSubgroupDTO>,
    ));
  }
}

/// @nodoc

class _$_GetSubgroups implements _GetSubgroups {
  const _$_GetSubgroups({required final List<WorkpackageSubgroupDTO> subgroups})
      : _subgroups = subgroups;

  final List<WorkpackageSubgroupDTO> _subgroups;
  @override
  List<WorkpackageSubgroupDTO> get subgroups {
    if (_subgroups is EqualUnmodifiableListView) return _subgroups;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_subgroups);
  }

  @override
  String toString() {
    return 'WorkpackageSubgroupingViewEvent.getSubgroups(subgroups: $subgroups)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetSubgroups &&
            const DeepCollectionEquality()
                .equals(other._subgroups, _subgroups));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_subgroups));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetSubgroupsCopyWith<_$_GetSubgroups> get copyWith =>
      __$$_GetSubgroupsCopyWithImpl<_$_GetSubgroups>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups)
        getSubgroups,
  }) {
    return getSubgroups(subgroups);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? getSubgroups,
  }) {
    return getSubgroups?.call(subgroups);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? getSubgroups,
    required TResult orElse(),
  }) {
    if (getSubgroups != null) {
      return getSubgroups(subgroups);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSubgroups value) getSubgroups,
  }) {
    return getSubgroups(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSubgroups value)? getSubgroups,
  }) {
    return getSubgroups?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSubgroups value)? getSubgroups,
    required TResult orElse(),
  }) {
    if (getSubgroups != null) {
      return getSubgroups(this);
    }
    return orElse();
  }
}

abstract class _GetSubgroups implements WorkpackageSubgroupingViewEvent {
  const factory _GetSubgroups(
          {required final List<WorkpackageSubgroupDTO> subgroups}) =
      _$_GetSubgroups;

  @override
  List<WorkpackageSubgroupDTO> get subgroups;
  @override
  @JsonKey(ignore: true)
  _$$_GetSubgroupsCopyWith<_$_GetSubgroups> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$WorkpackageSubgroupingViewState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups) success,
    required TResult Function() noSubGroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult? Function()? noSubGroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult Function()? noSubGroups,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_NoSubGroups value) noSubGroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_NoSubGroups value)? noSubGroups,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_NoSubGroups value)? noSubGroups,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkpackageSubgroupingViewStateCopyWith<$Res> {
  factory $WorkpackageSubgroupingViewStateCopyWith(
          WorkpackageSubgroupingViewState value,
          $Res Function(WorkpackageSubgroupingViewState) then) =
      _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res,
          WorkpackageSubgroupingViewState>;
}

/// @nodoc
class _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res,
        $Val extends WorkpackageSubgroupingViewState>
    implements $WorkpackageSubgroupingViewStateCopyWith<$Res> {
  _$WorkpackageSubgroupingViewStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'WorkpackageSubgroupingViewState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups) success,
    required TResult Function() noSubGroups,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult? Function()? noSubGroups,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult Function()? noSubGroups,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_NoSubGroups value) noSubGroups,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_NoSubGroups value)? noSubGroups,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_NoSubGroups value)? noSubGroups,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements WorkpackageSubgroupingViewState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'WorkpackageSubgroupingViewState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups) success,
    required TResult Function() noSubGroups,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult? Function()? noSubGroups,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult Function()? noSubGroups,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_NoSubGroups value) noSubGroups,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_NoSubGroups value)? noSubGroups,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_NoSubGroups value)? noSubGroups,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements WorkpackageSubgroupingViewState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<WorkpackageSubgroupDTO> subgroups});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subgroups = null,
  }) {
    return _then(_$_Success(
      subgroups: null == subgroups
          ? _value._subgroups
          : subgroups // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageSubgroupDTO>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success({required final List<WorkpackageSubgroupDTO> subgroups})
      : _subgroups = subgroups;

  final List<WorkpackageSubgroupDTO> _subgroups;
  @override
  List<WorkpackageSubgroupDTO> get subgroups {
    if (_subgroups is EqualUnmodifiableListView) return _subgroups;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_subgroups);
  }

  @override
  String toString() {
    return 'WorkpackageSubgroupingViewState.success(subgroups: $subgroups)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._subgroups, _subgroups));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_subgroups));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups) success,
    required TResult Function() noSubGroups,
  }) {
    return success(subgroups);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult? Function()? noSubGroups,
  }) {
    return success?.call(subgroups);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult Function()? noSubGroups,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(subgroups);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_NoSubGroups value) noSubGroups,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_NoSubGroups value)? noSubGroups,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_NoSubGroups value)? noSubGroups,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements WorkpackageSubgroupingViewState {
  const factory _Success(
      {required final List<WorkpackageSubgroupDTO> subgroups}) = _$_Success;

  List<WorkpackageSubgroupDTO> get subgroups;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_NoSubGroupsCopyWith<$Res> {
  factory _$$_NoSubGroupsCopyWith(
          _$_NoSubGroups value, $Res Function(_$_NoSubGroups) then) =
      __$$_NoSubGroupsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoSubGroupsCopyWithImpl<$Res>
    extends _$WorkpackageSubgroupingViewStateCopyWithImpl<$Res, _$_NoSubGroups>
    implements _$$_NoSubGroupsCopyWith<$Res> {
  __$$_NoSubGroupsCopyWithImpl(
      _$_NoSubGroups _value, $Res Function(_$_NoSubGroups) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoSubGroups implements _NoSubGroups {
  const _$_NoSubGroups();

  @override
  String toString() {
    return 'WorkpackageSubgroupingViewState.noSubGroups()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoSubGroups);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<WorkpackageSubgroupDTO> subgroups) success,
    required TResult Function() noSubGroups,
  }) {
    return noSubGroups();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult? Function()? noSubGroups,
  }) {
    return noSubGroups?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<WorkpackageSubgroupDTO> subgroups)? success,
    TResult Function()? noSubGroups,
    required TResult orElse(),
  }) {
    if (noSubGroups != null) {
      return noSubGroups();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_NoSubGroups value) noSubGroups,
  }) {
    return noSubGroups(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_NoSubGroups value)? noSubGroups,
  }) {
    return noSubGroups?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_NoSubGroups value)? noSubGroups,
    required TResult orElse(),
  }) {
    if (noSubGroups != null) {
      return noSubGroups(this);
    }
    return orElse();
  }
}

abstract class _NoSubGroups implements WorkpackageSubgroupingViewState {
  const factory _NoSubGroups() = _$_NoSubGroups;
}
