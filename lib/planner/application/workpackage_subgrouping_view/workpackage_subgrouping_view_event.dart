part of 'workpackage_subgrouping_view_bloc.dart';

@freezed
class WorkpackageSubgroupingViewEvent with _$WorkpackageSubgroupingViewEvent {
  const factory WorkpackageSubgroupingViewEvent.getSubgroups({required List<WorkpackageSubgroupDTO> subgroups}) = _GetSubgroups;
}
