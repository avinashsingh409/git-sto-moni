import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_subgroup_dto.dart';

part 'workpackage_subgrouping_view_event.dart';
part 'workpackage_subgrouping_view_state.dart';
part 'workpackage_subgrouping_view_bloc.freezed.dart';

@injectable
class WorkpackageSubgroupingViewBloc extends Bloc<WorkpackageSubgroupingViewEvent, WorkpackageSubgroupingViewState> {
  WorkpackageSubgroupingViewBloc() : super(const _Initial()) {
    on<WorkpackageSubgroupingViewEvent>((event, emit) async {
      await event.when(getSubgroups: (subgroups) async {
        emit(const WorkpackageSubgroupingViewState.loading());
        if (subgroups.isEmpty) {
          emit(const WorkpackageSubgroupingViewState.noSubGroups());
        } else {
          //Adding future.delayed to make a loadng mask
          await Future.delayed(const Duration(milliseconds: 500))
              .then((value) => emit(WorkpackageSubgroupingViewState.success(subgroups: subgroups)));
        }
      });
    });
  }
}
