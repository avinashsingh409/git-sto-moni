part of 'workpackage_subgrouping_view_bloc.dart';

@freezed
class WorkpackageSubgroupingViewState with _$WorkpackageSubgroupingViewState {
  const factory WorkpackageSubgroupingViewState.initial() = _Initial;
  const factory WorkpackageSubgroupingViewState.loading() = _Loading;
  const factory WorkpackageSubgroupingViewState.success({required List<WorkpackageSubgroupDTO> subgroups}) = _Success;
  const factory WorkpackageSubgroupingViewState.noSubGroups() = _NoSubGroups;
}
