// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'work_package_details_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$WorkPackageDetailsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getWorkpackageDetails,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getWorkpackageDetails,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getWorkpackageDetails,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetWorkpackageDetails value)
        getWorkpackageDetails,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetWorkpackageDetails value)? getWorkpackageDetails,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetWorkpackageDetails value)? getWorkpackageDetails,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkPackageDetailsEventCopyWith<$Res> {
  factory $WorkPackageDetailsEventCopyWith(WorkPackageDetailsEvent value,
          $Res Function(WorkPackageDetailsEvent) then) =
      _$WorkPackageDetailsEventCopyWithImpl<$Res, WorkPackageDetailsEvent>;
}

/// @nodoc
class _$WorkPackageDetailsEventCopyWithImpl<$Res,
        $Val extends WorkPackageDetailsEvent>
    implements $WorkPackageDetailsEventCopyWith<$Res> {
  _$WorkPackageDetailsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetWorkpackageDetailsCopyWith<$Res> {
  factory _$$_GetWorkpackageDetailsCopyWith(_$_GetWorkpackageDetails value,
          $Res Function(_$_GetWorkpackageDetails) then) =
      __$$_GetWorkpackageDetailsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetWorkpackageDetailsCopyWithImpl<$Res>
    extends _$WorkPackageDetailsEventCopyWithImpl<$Res,
        _$_GetWorkpackageDetails>
    implements _$$_GetWorkpackageDetailsCopyWith<$Res> {
  __$$_GetWorkpackageDetailsCopyWithImpl(_$_GetWorkpackageDetails _value,
      $Res Function(_$_GetWorkpackageDetails) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetWorkpackageDetails implements _GetWorkpackageDetails {
  const _$_GetWorkpackageDetails();

  @override
  String toString() {
    return 'WorkPackageDetailsEvent.getWorkpackageDetails()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetWorkpackageDetails);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getWorkpackageDetails,
  }) {
    return getWorkpackageDetails();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getWorkpackageDetails,
  }) {
    return getWorkpackageDetails?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getWorkpackageDetails,
    required TResult orElse(),
  }) {
    if (getWorkpackageDetails != null) {
      return getWorkpackageDetails();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetWorkpackageDetails value)
        getWorkpackageDetails,
  }) {
    return getWorkpackageDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetWorkpackageDetails value)? getWorkpackageDetails,
  }) {
    return getWorkpackageDetails?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetWorkpackageDetails value)? getWorkpackageDetails,
    required TResult orElse(),
  }) {
    if (getWorkpackageDetails != null) {
      return getWorkpackageDetails(this);
    }
    return orElse();
  }
}

abstract class _GetWorkpackageDetails implements WorkPackageDetailsEvent {
  const factory _GetWorkpackageDetails() = _$_GetWorkpackageDetails;
}

/// @nodoc
mixin _$WorkPackageDetailsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkPackageDetailsStateCopyWith<$Res> {
  factory $WorkPackageDetailsStateCopyWith(WorkPackageDetailsState value,
          $Res Function(WorkPackageDetailsState) then) =
      _$WorkPackageDetailsStateCopyWithImpl<$Res, WorkPackageDetailsState>;
}

/// @nodoc
class _$WorkPackageDetailsStateCopyWithImpl<$Res,
        $Val extends WorkPackageDetailsState>
    implements $WorkPackageDetailsStateCopyWith<$Res> {
  _$WorkPackageDetailsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$WorkPackageDetailsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'WorkPackageDetailsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements WorkPackageDetailsState {
  const factory _Initial() = _$_Initial;
}
