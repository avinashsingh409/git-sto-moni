import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/repository/workpackage_repository.dart';

part 'work_package_details_event.dart';
part 'work_package_details_state.dart';
part 'work_package_details_bloc.freezed.dart';

@injectable
class WorkPackageDetailsBloc extends Bloc<WorkPackageDetailsEvent, WorkPackageDetailsState> {
  final IWorkpackageRepository workpackageRepository;
  final _workPackageBehaviorSubject = BehaviorSubject<WorkpackageDTO>();
  Stream<WorkpackageDTO> getWorkpackageDetails() => _workPackageBehaviorSubject.stream;
  WorkPackageDetailsBloc(this.workpackageRepository) : super(const _Initial()) {
    on<WorkPackageDetailsEvent>((event, emit) {
      event.when(getWorkpackageDetails: () {
        final workpackageStream = workpackageRepository.getWorkpackagedById();
        workpackageStream.listen((workpackage) {
          if (workpackage != null) {
            _workPackageBehaviorSubject.add(workpackage);
          }
        });
      });
    });
  }
}
