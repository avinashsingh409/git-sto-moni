part of 'work_package_details_bloc.dart';

@freezed
class WorkPackageDetailsState with _$WorkPackageDetailsState {
  const factory WorkPackageDetailsState.initial() = _Initial;
}
