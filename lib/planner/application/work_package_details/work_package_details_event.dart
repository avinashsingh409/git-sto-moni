part of 'work_package_details_bloc.dart';

@freezed
class WorkPackageDetailsEvent with _$WorkPackageDetailsEvent {
  const factory WorkPackageDetailsEvent.getWorkpackageDetails() = _GetWorkpackageDetails;
}
