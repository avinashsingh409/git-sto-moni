// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'attachments_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AttachmentsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllAttachmentsInWorkpackage,
    required TResult Function(String newImageString, String name)
        cacheNewAttachment,
    required TResult Function(AttachmentDTO attachmentDTO) deleteAttachment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllAttachmentsInWorkpackage,
    TResult? Function(String newImageString, String name)? cacheNewAttachment,
    TResult? Function(AttachmentDTO attachmentDTO)? deleteAttachment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllAttachmentsInWorkpackage,
    TResult Function(String newImageString, String name)? cacheNewAttachment,
    TResult Function(AttachmentDTO attachmentDTO)? deleteAttachment,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllAttachmentsInWorkpackage value)
        getAllAttachmentsInWorkpackage,
    required TResult Function(_CacheNewAttachment value) cacheNewAttachment,
    required TResult Function(_DeleteAttachment value) deleteAttachment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult? Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult? Function(_DeleteAttachment value)? deleteAttachment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult Function(_DeleteAttachment value)? deleteAttachment,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AttachmentsEventCopyWith<$Res> {
  factory $AttachmentsEventCopyWith(
          AttachmentsEvent value, $Res Function(AttachmentsEvent) then) =
      _$AttachmentsEventCopyWithImpl<$Res, AttachmentsEvent>;
}

/// @nodoc
class _$AttachmentsEventCopyWithImpl<$Res, $Val extends AttachmentsEvent>
    implements $AttachmentsEventCopyWith<$Res> {
  _$AttachmentsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetAllAttachmentsInWorkpackageCopyWith<$Res> {
  factory _$$_GetAllAttachmentsInWorkpackageCopyWith(
          _$_GetAllAttachmentsInWorkpackage value,
          $Res Function(_$_GetAllAttachmentsInWorkpackage) then) =
      __$$_GetAllAttachmentsInWorkpackageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetAllAttachmentsInWorkpackageCopyWithImpl<$Res>
    extends _$AttachmentsEventCopyWithImpl<$Res,
        _$_GetAllAttachmentsInWorkpackage>
    implements _$$_GetAllAttachmentsInWorkpackageCopyWith<$Res> {
  __$$_GetAllAttachmentsInWorkpackageCopyWithImpl(
      _$_GetAllAttachmentsInWorkpackage _value,
      $Res Function(_$_GetAllAttachmentsInWorkpackage) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetAllAttachmentsInWorkpackage
    implements _GetAllAttachmentsInWorkpackage {
  const _$_GetAllAttachmentsInWorkpackage();

  @override
  String toString() {
    return 'AttachmentsEvent.getAllAttachmentsInWorkpackage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetAllAttachmentsInWorkpackage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllAttachmentsInWorkpackage,
    required TResult Function(String newImageString, String name)
        cacheNewAttachment,
    required TResult Function(AttachmentDTO attachmentDTO) deleteAttachment,
  }) {
    return getAllAttachmentsInWorkpackage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllAttachmentsInWorkpackage,
    TResult? Function(String newImageString, String name)? cacheNewAttachment,
    TResult? Function(AttachmentDTO attachmentDTO)? deleteAttachment,
  }) {
    return getAllAttachmentsInWorkpackage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllAttachmentsInWorkpackage,
    TResult Function(String newImageString, String name)? cacheNewAttachment,
    TResult Function(AttachmentDTO attachmentDTO)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (getAllAttachmentsInWorkpackage != null) {
      return getAllAttachmentsInWorkpackage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllAttachmentsInWorkpackage value)
        getAllAttachmentsInWorkpackage,
    required TResult Function(_CacheNewAttachment value) cacheNewAttachment,
    required TResult Function(_DeleteAttachment value) deleteAttachment,
  }) {
    return getAllAttachmentsInWorkpackage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult? Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult? Function(_DeleteAttachment value)? deleteAttachment,
  }) {
    return getAllAttachmentsInWorkpackage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult Function(_DeleteAttachment value)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (getAllAttachmentsInWorkpackage != null) {
      return getAllAttachmentsInWorkpackage(this);
    }
    return orElse();
  }
}

abstract class _GetAllAttachmentsInWorkpackage implements AttachmentsEvent {
  const factory _GetAllAttachmentsInWorkpackage() =
      _$_GetAllAttachmentsInWorkpackage;
}

/// @nodoc
abstract class _$$_CacheNewAttachmentCopyWith<$Res> {
  factory _$$_CacheNewAttachmentCopyWith(_$_CacheNewAttachment value,
          $Res Function(_$_CacheNewAttachment) then) =
      __$$_CacheNewAttachmentCopyWithImpl<$Res>;
  @useResult
  $Res call({String newImageString, String name});
}

/// @nodoc
class __$$_CacheNewAttachmentCopyWithImpl<$Res>
    extends _$AttachmentsEventCopyWithImpl<$Res, _$_CacheNewAttachment>
    implements _$$_CacheNewAttachmentCopyWith<$Res> {
  __$$_CacheNewAttachmentCopyWithImpl(
      _$_CacheNewAttachment _value, $Res Function(_$_CacheNewAttachment) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? newImageString = null,
    Object? name = null,
  }) {
    return _then(_$_CacheNewAttachment(
      newImageString: null == newImageString
          ? _value.newImageString
          : newImageString // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_CacheNewAttachment implements _CacheNewAttachment {
  const _$_CacheNewAttachment(
      {required this.newImageString, required this.name});

  @override
  final String newImageString;
  @override
  final String name;

  @override
  String toString() {
    return 'AttachmentsEvent.cacheNewAttachment(newImageString: $newImageString, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CacheNewAttachment &&
            (identical(other.newImageString, newImageString) ||
                other.newImageString == newImageString) &&
            (identical(other.name, name) || other.name == name));
  }

  @override
  int get hashCode => Object.hash(runtimeType, newImageString, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CacheNewAttachmentCopyWith<_$_CacheNewAttachment> get copyWith =>
      __$$_CacheNewAttachmentCopyWithImpl<_$_CacheNewAttachment>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllAttachmentsInWorkpackage,
    required TResult Function(String newImageString, String name)
        cacheNewAttachment,
    required TResult Function(AttachmentDTO attachmentDTO) deleteAttachment,
  }) {
    return cacheNewAttachment(newImageString, name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllAttachmentsInWorkpackage,
    TResult? Function(String newImageString, String name)? cacheNewAttachment,
    TResult? Function(AttachmentDTO attachmentDTO)? deleteAttachment,
  }) {
    return cacheNewAttachment?.call(newImageString, name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllAttachmentsInWorkpackage,
    TResult Function(String newImageString, String name)? cacheNewAttachment,
    TResult Function(AttachmentDTO attachmentDTO)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (cacheNewAttachment != null) {
      return cacheNewAttachment(newImageString, name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllAttachmentsInWorkpackage value)
        getAllAttachmentsInWorkpackage,
    required TResult Function(_CacheNewAttachment value) cacheNewAttachment,
    required TResult Function(_DeleteAttachment value) deleteAttachment,
  }) {
    return cacheNewAttachment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult? Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult? Function(_DeleteAttachment value)? deleteAttachment,
  }) {
    return cacheNewAttachment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult Function(_DeleteAttachment value)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (cacheNewAttachment != null) {
      return cacheNewAttachment(this);
    }
    return orElse();
  }
}

abstract class _CacheNewAttachment implements AttachmentsEvent {
  const factory _CacheNewAttachment(
      {required final String newImageString,
      required final String name}) = _$_CacheNewAttachment;

  String get newImageString;
  String get name;
  @JsonKey(ignore: true)
  _$$_CacheNewAttachmentCopyWith<_$_CacheNewAttachment> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DeleteAttachmentCopyWith<$Res> {
  factory _$$_DeleteAttachmentCopyWith(
          _$_DeleteAttachment value, $Res Function(_$_DeleteAttachment) then) =
      __$$_DeleteAttachmentCopyWithImpl<$Res>;
  @useResult
  $Res call({AttachmentDTO attachmentDTO});
}

/// @nodoc
class __$$_DeleteAttachmentCopyWithImpl<$Res>
    extends _$AttachmentsEventCopyWithImpl<$Res, _$_DeleteAttachment>
    implements _$$_DeleteAttachmentCopyWith<$Res> {
  __$$_DeleteAttachmentCopyWithImpl(
      _$_DeleteAttachment _value, $Res Function(_$_DeleteAttachment) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attachmentDTO = null,
  }) {
    return _then(_$_DeleteAttachment(
      attachmentDTO: null == attachmentDTO
          ? _value.attachmentDTO
          : attachmentDTO // ignore: cast_nullable_to_non_nullable
              as AttachmentDTO,
    ));
  }
}

/// @nodoc

class _$_DeleteAttachment implements _DeleteAttachment {
  const _$_DeleteAttachment({required this.attachmentDTO});

  @override
  final AttachmentDTO attachmentDTO;

  @override
  String toString() {
    return 'AttachmentsEvent.deleteAttachment(attachmentDTO: $attachmentDTO)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeleteAttachment &&
            (identical(other.attachmentDTO, attachmentDTO) ||
                other.attachmentDTO == attachmentDTO));
  }

  @override
  int get hashCode => Object.hash(runtimeType, attachmentDTO);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DeleteAttachmentCopyWith<_$_DeleteAttachment> get copyWith =>
      __$$_DeleteAttachmentCopyWithImpl<_$_DeleteAttachment>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllAttachmentsInWorkpackage,
    required TResult Function(String newImageString, String name)
        cacheNewAttachment,
    required TResult Function(AttachmentDTO attachmentDTO) deleteAttachment,
  }) {
    return deleteAttachment(attachmentDTO);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllAttachmentsInWorkpackage,
    TResult? Function(String newImageString, String name)? cacheNewAttachment,
    TResult? Function(AttachmentDTO attachmentDTO)? deleteAttachment,
  }) {
    return deleteAttachment?.call(attachmentDTO);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllAttachmentsInWorkpackage,
    TResult Function(String newImageString, String name)? cacheNewAttachment,
    TResult Function(AttachmentDTO attachmentDTO)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (deleteAttachment != null) {
      return deleteAttachment(attachmentDTO);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllAttachmentsInWorkpackage value)
        getAllAttachmentsInWorkpackage,
    required TResult Function(_CacheNewAttachment value) cacheNewAttachment,
    required TResult Function(_DeleteAttachment value) deleteAttachment,
  }) {
    return deleteAttachment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult? Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult? Function(_DeleteAttachment value)? deleteAttachment,
  }) {
    return deleteAttachment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllAttachmentsInWorkpackage value)?
        getAllAttachmentsInWorkpackage,
    TResult Function(_CacheNewAttachment value)? cacheNewAttachment,
    TResult Function(_DeleteAttachment value)? deleteAttachment,
    required TResult orElse(),
  }) {
    if (deleteAttachment != null) {
      return deleteAttachment(this);
    }
    return orElse();
  }
}

abstract class _DeleteAttachment implements AttachmentsEvent {
  const factory _DeleteAttachment(
      {required final AttachmentDTO attachmentDTO}) = _$_DeleteAttachment;

  AttachmentDTO get attachmentDTO;
  @JsonKey(ignore: true)
  _$$_DeleteAttachmentCopyWith<_$_DeleteAttachment> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AttachmentsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Unauthorized value) unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Unauthorized value)? unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Unauthorized value)? unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AttachmentsStateCopyWith<$Res> {
  factory $AttachmentsStateCopyWith(
          AttachmentsState value, $Res Function(AttachmentsState) then) =
      _$AttachmentsStateCopyWithImpl<$Res, AttachmentsState>;
}

/// @nodoc
class _$AttachmentsStateCopyWithImpl<$Res, $Val extends AttachmentsState>
    implements $AttachmentsStateCopyWith<$Res> {
  _$AttachmentsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$AttachmentsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'AttachmentsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() unauthorized,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? unauthorized,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Unauthorized value) unauthorized,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Unauthorized value)? unauthorized,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Unauthorized value)? unauthorized,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements AttachmentsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_UnauthorizedCopyWith<$Res> {
  factory _$$_UnauthorizedCopyWith(
          _$_Unauthorized value, $Res Function(_$_Unauthorized) then) =
      __$$_UnauthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnauthorizedCopyWithImpl<$Res>
    extends _$AttachmentsStateCopyWithImpl<$Res, _$_Unauthorized>
    implements _$$_UnauthorizedCopyWith<$Res> {
  __$$_UnauthorizedCopyWithImpl(
      _$_Unauthorized _value, $Res Function(_$_Unauthorized) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Unauthorized implements _Unauthorized {
  const _$_Unauthorized();

  @override
  String toString() {
    return 'AttachmentsState.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Unauthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() unauthorized,
  }) {
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? unauthorized,
  }) {
    return unauthorized?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Unauthorized value) unauthorized,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Unauthorized value)? unauthorized,
  }) {
    return unauthorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Unauthorized value)? unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class _Unauthorized implements AttachmentsState {
  const factory _Unauthorized() = _$_Unauthorized;
}
