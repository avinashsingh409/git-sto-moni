part of 'attachments_bloc.dart';

@freezed
class AttachmentsState with _$AttachmentsState {
  const factory AttachmentsState.initial() = _Initial;
  const factory AttachmentsState.unauthorized() = _Unauthorized;
}
