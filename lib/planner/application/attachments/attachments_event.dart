part of 'attachments_bloc.dart';

@freezed
class AttachmentsEvent with _$AttachmentsEvent {
  const factory AttachmentsEvent.getAllAttachmentsInWorkpackage() = _GetAllAttachmentsInWorkpackage;
  const factory AttachmentsEvent.cacheNewAttachment({required String newImageString, required String name}) = _CacheNewAttachment;
  const factory AttachmentsEvent.deleteAttachment({required AttachmentDTO attachmentDTO}) = _DeleteAttachment;
}
