import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/repository/attachments_repository.dart';

part 'attachments_event.dart';
part 'attachments_state.dart';
part 'attachments_bloc.freezed.dart';

@injectable
class AttachmentsBloc extends Bloc<AttachmentsEvent, AttachmentsState> {
  final IAttachmentsRepository attachmentsRepository;
  //Stream for all attachments
  final _attachmentsBehaviorSubject = BehaviorSubject<List<AttachmentDTO>>();
  Stream<List<AttachmentDTO>> getAllAttachments() => _attachmentsBehaviorSubject.stream;
  AttachmentsBloc(this.attachmentsRepository) : super(const _Initial()) {
    on<AttachmentsEvent>((event, emit) async {
      await event.when(getAllAttachmentsInWorkpackage: () async {
        final attachmentsSubject = attachmentsRepository.getCachedAttachmentByWorklistId();
        attachmentsSubject.listen((event) {
          _attachmentsBehaviorSubject.add(event);
        });
      }, cacheNewAttachment: (newImageString, name) async {
        await attachmentsRepository.insertNewImage(newImage64String: newImageString, name: name);
      }, deleteAttachment: (attachmentDTO) async {
        emit(const AttachmentsState.initial());
        final attachmentDeleteStatus = await attachmentsRepository.markAttachmentAsDelete(attachmentDTO: attachmentDTO);
        attachmentDeleteStatus.fold(
          (_) => add(const AttachmentsEvent.getAllAttachmentsInWorkpackage()),
          (exception) => emit(
            const AttachmentsState.unauthorized(),
          ),
        );
      });
    });
  }
}
