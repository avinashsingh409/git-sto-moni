import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/repository/attachments_repository.dart';

part 'image_annotation_event.dart';
part 'image_annotation_state.dart';
part 'image_annotation_bloc.freezed.dart';

@injectable
class ImageAnnotationBloc extends Bloc<ImageAnnotationEvent, ImageAnnotationState> {
  final IAttachmentsRepository attachmentsRepository;
  late Uint8List imageUnit8list;
  ImageAnnotationBloc(this.attachmentsRepository) : super(const _Initial()) {
    on<ImageAnnotationEvent>((event, emit) async {
      await event.when(
        parseImage: (base64ImageString) async {
          emit(const ImageAnnotationState.loading());
          try {
            final decodedImageUnit8list = base64Decode(base64ImageString);
            imageUnit8list = decodedImageUnit8list;
            emit(ImageAnnotationState.imageParsed(imageUnit8list: imageUnit8list));
          } on Exception catch (_) {
            emit(const ImageAnnotationState.parseError());
          }
        },
        updateImage: (updatedImageUnit8list, currentAttachment) async {
          emit(const ImageAnnotationState.initial());
          try {
            final imageThumbnailBytes = base64.encode(updatedImageUnit8list);
            emit(ImageAnnotationState.imageUpdated(newImageUnit8list: updatedImageUnit8list));
            await attachmentsRepository.updateImage(
                updatedImageThumbnailBytes: imageThumbnailBytes, currentAttachment: currentAttachment);
          } on Exception catch (_) {
            emit(const ImageAnnotationState.updateError());
          }
        },
      );
    });
  }
}
