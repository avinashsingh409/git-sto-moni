part of 'image_annotation_bloc.dart';

@freezed
class ImageAnnotationEvent with _$ImageAnnotationEvent {
  const factory ImageAnnotationEvent.parseImage({required String base64ImageString}) = _ParseImage;
  const factory ImageAnnotationEvent.updateImage(
      {required Uint8List updatedImageUnit8list, required AttachmentDTO currentAttachment}) = _UpdateImage;
}
