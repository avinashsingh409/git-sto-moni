part of 'image_annotation_bloc.dart';

@freezed
class ImageAnnotationState with _$ImageAnnotationState {
  const factory ImageAnnotationState.initial() = _Initial;
  const factory ImageAnnotationState.loading() = _Loading;
  const factory ImageAnnotationState.imageParsed({required Uint8List imageUnit8list}) = _ImageParsed;
  const factory ImageAnnotationState.parseError() = _ParseError;
  const factory ImageAnnotationState.imageUpdated({required Uint8List newImageUnit8list}) = _ImageUpdated;
  const factory ImageAnnotationState.updateError() = _UpdateError;
}
