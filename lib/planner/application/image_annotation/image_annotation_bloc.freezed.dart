// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'image_annotation_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ImageAnnotationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String base64ImageString) parseImage,
    required TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)
        updateImage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String base64ImageString)? parseImage,
    TResult? Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String base64ImageString)? parseImage,
    TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ParseImage value) parseImage,
    required TResult Function(_UpdateImage value) updateImage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ParseImage value)? parseImage,
    TResult? Function(_UpdateImage value)? updateImage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ParseImage value)? parseImage,
    TResult Function(_UpdateImage value)? updateImage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImageAnnotationEventCopyWith<$Res> {
  factory $ImageAnnotationEventCopyWith(ImageAnnotationEvent value,
          $Res Function(ImageAnnotationEvent) then) =
      _$ImageAnnotationEventCopyWithImpl<$Res, ImageAnnotationEvent>;
}

/// @nodoc
class _$ImageAnnotationEventCopyWithImpl<$Res,
        $Val extends ImageAnnotationEvent>
    implements $ImageAnnotationEventCopyWith<$Res> {
  _$ImageAnnotationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_ParseImageCopyWith<$Res> {
  factory _$$_ParseImageCopyWith(
          _$_ParseImage value, $Res Function(_$_ParseImage) then) =
      __$$_ParseImageCopyWithImpl<$Res>;
  @useResult
  $Res call({String base64ImageString});
}

/// @nodoc
class __$$_ParseImageCopyWithImpl<$Res>
    extends _$ImageAnnotationEventCopyWithImpl<$Res, _$_ParseImage>
    implements _$$_ParseImageCopyWith<$Res> {
  __$$_ParseImageCopyWithImpl(
      _$_ParseImage _value, $Res Function(_$_ParseImage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? base64ImageString = null,
  }) {
    return _then(_$_ParseImage(
      base64ImageString: null == base64ImageString
          ? _value.base64ImageString
          : base64ImageString // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ParseImage with DiagnosticableTreeMixin implements _ParseImage {
  const _$_ParseImage({required this.base64ImageString});

  @override
  final String base64ImageString;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationEvent.parseImage(base64ImageString: $base64ImageString)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ImageAnnotationEvent.parseImage'))
      ..add(DiagnosticsProperty('base64ImageString', base64ImageString));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ParseImage &&
            (identical(other.base64ImageString, base64ImageString) ||
                other.base64ImageString == base64ImageString));
  }

  @override
  int get hashCode => Object.hash(runtimeType, base64ImageString);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ParseImageCopyWith<_$_ParseImage> get copyWith =>
      __$$_ParseImageCopyWithImpl<_$_ParseImage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String base64ImageString) parseImage,
    required TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)
        updateImage,
  }) {
    return parseImage(base64ImageString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String base64ImageString)? parseImage,
    TResult? Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
  }) {
    return parseImage?.call(base64ImageString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String base64ImageString)? parseImage,
    TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
    required TResult orElse(),
  }) {
    if (parseImage != null) {
      return parseImage(base64ImageString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ParseImage value) parseImage,
    required TResult Function(_UpdateImage value) updateImage,
  }) {
    return parseImage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ParseImage value)? parseImage,
    TResult? Function(_UpdateImage value)? updateImage,
  }) {
    return parseImage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ParseImage value)? parseImage,
    TResult Function(_UpdateImage value)? updateImage,
    required TResult orElse(),
  }) {
    if (parseImage != null) {
      return parseImage(this);
    }
    return orElse();
  }
}

abstract class _ParseImage implements ImageAnnotationEvent {
  const factory _ParseImage({required final String base64ImageString}) =
      _$_ParseImage;

  String get base64ImageString;
  @JsonKey(ignore: true)
  _$$_ParseImageCopyWith<_$_ParseImage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateImageCopyWith<$Res> {
  factory _$$_UpdateImageCopyWith(
          _$_UpdateImage value, $Res Function(_$_UpdateImage) then) =
      __$$_UpdateImageCopyWithImpl<$Res>;
  @useResult
  $Res call({Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment});
}

/// @nodoc
class __$$_UpdateImageCopyWithImpl<$Res>
    extends _$ImageAnnotationEventCopyWithImpl<$Res, _$_UpdateImage>
    implements _$$_UpdateImageCopyWith<$Res> {
  __$$_UpdateImageCopyWithImpl(
      _$_UpdateImage _value, $Res Function(_$_UpdateImage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updatedImageUnit8list = null,
    Object? currentAttachment = null,
  }) {
    return _then(_$_UpdateImage(
      updatedImageUnit8list: null == updatedImageUnit8list
          ? _value.updatedImageUnit8list
          : updatedImageUnit8list // ignore: cast_nullable_to_non_nullable
              as Uint8List,
      currentAttachment: null == currentAttachment
          ? _value.currentAttachment
          : currentAttachment // ignore: cast_nullable_to_non_nullable
              as AttachmentDTO,
    ));
  }
}

/// @nodoc

class _$_UpdateImage with DiagnosticableTreeMixin implements _UpdateImage {
  const _$_UpdateImage(
      {required this.updatedImageUnit8list, required this.currentAttachment});

  @override
  final Uint8List updatedImageUnit8list;
  @override
  final AttachmentDTO currentAttachment;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationEvent.updateImage(updatedImageUnit8list: $updatedImageUnit8list, currentAttachment: $currentAttachment)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ImageAnnotationEvent.updateImage'))
      ..add(DiagnosticsProperty('updatedImageUnit8list', updatedImageUnit8list))
      ..add(DiagnosticsProperty('currentAttachment', currentAttachment));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateImage &&
            const DeepCollectionEquality()
                .equals(other.updatedImageUnit8list, updatedImageUnit8list) &&
            (identical(other.currentAttachment, currentAttachment) ||
                other.currentAttachment == currentAttachment));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(updatedImageUnit8list),
      currentAttachment);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateImageCopyWith<_$_UpdateImage> get copyWith =>
      __$$_UpdateImageCopyWithImpl<_$_UpdateImage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String base64ImageString) parseImage,
    required TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)
        updateImage,
  }) {
    return updateImage(updatedImageUnit8list, currentAttachment);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String base64ImageString)? parseImage,
    TResult? Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
  }) {
    return updateImage?.call(updatedImageUnit8list, currentAttachment);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String base64ImageString)? parseImage,
    TResult Function(
            Uint8List updatedImageUnit8list, AttachmentDTO currentAttachment)?
        updateImage,
    required TResult orElse(),
  }) {
    if (updateImage != null) {
      return updateImage(updatedImageUnit8list, currentAttachment);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ParseImage value) parseImage,
    required TResult Function(_UpdateImage value) updateImage,
  }) {
    return updateImage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ParseImage value)? parseImage,
    TResult? Function(_UpdateImage value)? updateImage,
  }) {
    return updateImage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ParseImage value)? parseImage,
    TResult Function(_UpdateImage value)? updateImage,
    required TResult orElse(),
  }) {
    if (updateImage != null) {
      return updateImage(this);
    }
    return orElse();
  }
}

abstract class _UpdateImage implements ImageAnnotationEvent {
  const factory _UpdateImage(
      {required final Uint8List updatedImageUnit8list,
      required final AttachmentDTO currentAttachment}) = _$_UpdateImage;

  Uint8List get updatedImageUnit8list;
  AttachmentDTO get currentAttachment;
  @JsonKey(ignore: true)
  _$$_UpdateImageCopyWith<_$_UpdateImage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ImageAnnotationState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImageAnnotationStateCopyWith<$Res> {
  factory $ImageAnnotationStateCopyWith(ImageAnnotationState value,
          $Res Function(ImageAnnotationState) then) =
      _$ImageAnnotationStateCopyWithImpl<$Res, ImageAnnotationState>;
}

/// @nodoc
class _$ImageAnnotationStateCopyWithImpl<$Res,
        $Val extends ImageAnnotationState>
    implements $ImageAnnotationStateCopyWith<$Res> {
  _$ImageAnnotationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial with DiagnosticableTreeMixin implements _Initial {
  const _$_Initial();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ImageAnnotationState.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ImageAnnotationState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading with DiagnosticableTreeMixin implements _Loading {
  const _$_Loading();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ImageAnnotationState.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements ImageAnnotationState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_ImageParsedCopyWith<$Res> {
  factory _$$_ImageParsedCopyWith(
          _$_ImageParsed value, $Res Function(_$_ImageParsed) then) =
      __$$_ImageParsedCopyWithImpl<$Res>;
  @useResult
  $Res call({Uint8List imageUnit8list});
}

/// @nodoc
class __$$_ImageParsedCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_ImageParsed>
    implements _$$_ImageParsedCopyWith<$Res> {
  __$$_ImageParsedCopyWithImpl(
      _$_ImageParsed _value, $Res Function(_$_ImageParsed) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? imageUnit8list = null,
  }) {
    return _then(_$_ImageParsed(
      imageUnit8list: null == imageUnit8list
          ? _value.imageUnit8list
          : imageUnit8list // ignore: cast_nullable_to_non_nullable
              as Uint8List,
    ));
  }
}

/// @nodoc

class _$_ImageParsed with DiagnosticableTreeMixin implements _ImageParsed {
  const _$_ImageParsed({required this.imageUnit8list});

  @override
  final Uint8List imageUnit8list;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.imageParsed(imageUnit8list: $imageUnit8list)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ImageAnnotationState.imageParsed'))
      ..add(DiagnosticsProperty('imageUnit8list', imageUnit8list));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ImageParsed &&
            const DeepCollectionEquality()
                .equals(other.imageUnit8list, imageUnit8list));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(imageUnit8list));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ImageParsedCopyWith<_$_ImageParsed> get copyWith =>
      __$$_ImageParsedCopyWithImpl<_$_ImageParsed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return imageParsed(imageUnit8list);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return imageParsed?.call(imageUnit8list);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (imageParsed != null) {
      return imageParsed(imageUnit8list);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return imageParsed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return imageParsed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (imageParsed != null) {
      return imageParsed(this);
    }
    return orElse();
  }
}

abstract class _ImageParsed implements ImageAnnotationState {
  const factory _ImageParsed({required final Uint8List imageUnit8list}) =
      _$_ImageParsed;

  Uint8List get imageUnit8list;
  @JsonKey(ignore: true)
  _$$_ImageParsedCopyWith<_$_ImageParsed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ParseErrorCopyWith<$Res> {
  factory _$$_ParseErrorCopyWith(
          _$_ParseError value, $Res Function(_$_ParseError) then) =
      __$$_ParseErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ParseErrorCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_ParseError>
    implements _$$_ParseErrorCopyWith<$Res> {
  __$$_ParseErrorCopyWithImpl(
      _$_ParseError _value, $Res Function(_$_ParseError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ParseError with DiagnosticableTreeMixin implements _ParseError {
  const _$_ParseError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.parseError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'ImageAnnotationState.parseError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ParseError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return parseError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return parseError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (parseError != null) {
      return parseError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return parseError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return parseError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (parseError != null) {
      return parseError(this);
    }
    return orElse();
  }
}

abstract class _ParseError implements ImageAnnotationState {
  const factory _ParseError() = _$_ParseError;
}

/// @nodoc
abstract class _$$_ImageUpdatedCopyWith<$Res> {
  factory _$$_ImageUpdatedCopyWith(
          _$_ImageUpdated value, $Res Function(_$_ImageUpdated) then) =
      __$$_ImageUpdatedCopyWithImpl<$Res>;
  @useResult
  $Res call({Uint8List newImageUnit8list});
}

/// @nodoc
class __$$_ImageUpdatedCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_ImageUpdated>
    implements _$$_ImageUpdatedCopyWith<$Res> {
  __$$_ImageUpdatedCopyWithImpl(
      _$_ImageUpdated _value, $Res Function(_$_ImageUpdated) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? newImageUnit8list = null,
  }) {
    return _then(_$_ImageUpdated(
      newImageUnit8list: null == newImageUnit8list
          ? _value.newImageUnit8list
          : newImageUnit8list // ignore: cast_nullable_to_non_nullable
              as Uint8List,
    ));
  }
}

/// @nodoc

class _$_ImageUpdated with DiagnosticableTreeMixin implements _ImageUpdated {
  const _$_ImageUpdated({required this.newImageUnit8list});

  @override
  final Uint8List newImageUnit8list;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.imageUpdated(newImageUnit8list: $newImageUnit8list)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ImageAnnotationState.imageUpdated'))
      ..add(DiagnosticsProperty('newImageUnit8list', newImageUnit8list));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ImageUpdated &&
            const DeepCollectionEquality()
                .equals(other.newImageUnit8list, newImageUnit8list));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(newImageUnit8list));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ImageUpdatedCopyWith<_$_ImageUpdated> get copyWith =>
      __$$_ImageUpdatedCopyWithImpl<_$_ImageUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return imageUpdated(newImageUnit8list);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return imageUpdated?.call(newImageUnit8list);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (imageUpdated != null) {
      return imageUpdated(newImageUnit8list);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return imageUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return imageUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (imageUpdated != null) {
      return imageUpdated(this);
    }
    return orElse();
  }
}

abstract class _ImageUpdated implements ImageAnnotationState {
  const factory _ImageUpdated({required final Uint8List newImageUnit8list}) =
      _$_ImageUpdated;

  Uint8List get newImageUnit8list;
  @JsonKey(ignore: true)
  _$$_ImageUpdatedCopyWith<_$_ImageUpdated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateErrorCopyWith<$Res> {
  factory _$$_UpdateErrorCopyWith(
          _$_UpdateError value, $Res Function(_$_UpdateError) then) =
      __$$_UpdateErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UpdateErrorCopyWithImpl<$Res>
    extends _$ImageAnnotationStateCopyWithImpl<$Res, _$_UpdateError>
    implements _$$_UpdateErrorCopyWith<$Res> {
  __$$_UpdateErrorCopyWithImpl(
      _$_UpdateError _value, $Res Function(_$_UpdateError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UpdateError with DiagnosticableTreeMixin implements _UpdateError {
  const _$_UpdateError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ImageAnnotationState.updateError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'ImageAnnotationState.updateError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UpdateError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Uint8List imageUnit8list) imageParsed,
    required TResult Function() parseError,
    required TResult Function(Uint8List newImageUnit8list) imageUpdated,
    required TResult Function() updateError,
  }) {
    return updateError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(Uint8List imageUnit8list)? imageParsed,
    TResult? Function()? parseError,
    TResult? Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult? Function()? updateError,
  }) {
    return updateError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Uint8List imageUnit8list)? imageParsed,
    TResult Function()? parseError,
    TResult Function(Uint8List newImageUnit8list)? imageUpdated,
    TResult Function()? updateError,
    required TResult orElse(),
  }) {
    if (updateError != null) {
      return updateError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_ImageParsed value) imageParsed,
    required TResult Function(_ParseError value) parseError,
    required TResult Function(_ImageUpdated value) imageUpdated,
    required TResult Function(_UpdateError value) updateError,
  }) {
    return updateError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_ImageParsed value)? imageParsed,
    TResult? Function(_ParseError value)? parseError,
    TResult? Function(_ImageUpdated value)? imageUpdated,
    TResult? Function(_UpdateError value)? updateError,
  }) {
    return updateError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_ImageParsed value)? imageParsed,
    TResult Function(_ParseError value)? parseError,
    TResult Function(_ImageUpdated value)? imageUpdated,
    TResult Function(_UpdateError value)? updateError,
    required TResult orElse(),
  }) {
    if (updateError != null) {
      return updateError(this);
    }
    return orElse();
  }
}

abstract class _UpdateError implements ImageAnnotationState {
  const factory _UpdateError() = _$_UpdateError;
}
