import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_filter_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/repository/workpackage_repository.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/widgets/workpackage_filter_pane.dart';

part 'workpackage_list_screen_event.dart';
part 'workpackage_list_screen_state.dart';
part 'workpackage_list_screen_bloc.freezed.dart';

@injectable
class WorkpackageListScreenBloc extends Bloc<WorkpackageListScreenEvent, WorkpackageListScreenState> {
  final IWorkpackageRepository workpackageRepository;

  BehaviorSubject workpackageStream = BehaviorSubject<List<WorkpackageDTO>>();
  WorkpackageListScreenBloc(this.workpackageRepository) : super(const _Initial()) {
    on<WorkpackageListScreenEvent>((event, emit) async {
      await event.when(getWorkPackages: () async {
        emit(const WorkpackageListScreenState.loading());
        switch (WorkpackageSelectedFilterOption.selectedOption) {
          case WorkpackageFilterTypeEnum.plannedByUser:
            workpackageStream = workpackageRepository.getWorkpackagesPlannedByUser();
            break;
          case WorkpackageFilterTypeEnum.alphabetic:
            workpackageStream = workpackageRepository.getWorkpackagesAlphabetically();
            break;
          case WorkpackageFilterTypeEnum.clearFilters:
            workpackageStream = workpackageRepository.getAllCachedWorkPackages();
            break;
        }
        await emit.onEach(
          workpackageStream.stream,
          onData: (workpackages) {
            List<WorkpackageDTO> workPackagesList = [];
            for (var workpackage in workpackages) {
              workPackagesList.add(workpackage);
            }
            emit(WorkpackageListScreenState.success(
                workpackages: workPackagesList, filterTypeEnum: WorkpackageSelectedFilterOption.selectedOption));
          },
        ).catchError((error) {
          emit(const WorkpackageListScreenState.error(errorMsg: "Unexpected Error"));
        });
      });
    });
  }
}
