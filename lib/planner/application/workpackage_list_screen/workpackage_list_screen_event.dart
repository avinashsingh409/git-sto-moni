part of 'workpackage_list_screen_bloc.dart';

@freezed
class WorkpackageListScreenEvent with _$WorkpackageListScreenEvent {
  const factory WorkpackageListScreenEvent.getWorkPackages() = _GetWorkPackages;
}
