part of 'workpackage_list_screen_bloc.dart';

@freezed
class WorkpackageListScreenState with _$WorkpackageListScreenState {
  const factory WorkpackageListScreenState.initial() = _Initial;
  const factory WorkpackageListScreenState.loading() = _Loading;
  const factory WorkpackageListScreenState.success({
    required List<WorkpackageDTO> workpackages,
    required WorkpackageFilterTypeEnum filterTypeEnum,
  }) = _Success;
  const factory WorkpackageListScreenState.error({required String errorMsg}) = _Error;
}
