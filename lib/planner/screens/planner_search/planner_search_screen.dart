import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/planner_search/planner_search_bloc.dart';
import 'package:sto_mobile_v2/planner/screens/planner_search/widgets/planner_search_pane.dart';
import 'package:sto_mobile_v2/planner/screens/planner_search/widgets/planner_search_result_pane.dart';

class PlannerSearchScreen extends StatefulWidget {
  const PlannerSearchScreen({super.key});

  @override
  State<PlannerSearchScreen> createState() => _PlannerSearchScreenState();
}

class _PlannerSearchScreenState extends State<PlannerSearchScreen> {
  final TextEditingController searchController = TextEditingController();
  final bloc = getIt<PlannerSearchBloc>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const SizedBox(height: 12),
        PlannerSearchPane(
          searchController: searchController,
          bloc: bloc
        ),
        BlocBuilder<PlannerSearchBloc, PlannerSearchState>(
          builder: (context, state) {
            return state.when(
              initial: () => const Expanded(child: Center(child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Icon(Icons.search, size: 50),
                  SizedBox(height: 10),
                  Text("Select a Search Field and enter a Search Term")
                ])
              )),
              loading: () => const Expanded(flex: 3, child: LoadingIndicator()),
              success: (results) => PlannerSearchResultPane(results: results),
              error: (errorMessage) => Flexible(
                flex: 3,
                child: Center(child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Icon(Icons.error, color: Colors.red, size: 40),
                  const SizedBox(height: 12),
                  Text(errorMessage, style: const TextStyle(fontSize: 20))
                ])),
              )
            );
          }
        )
      ])
    );
  }
}