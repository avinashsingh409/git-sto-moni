import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/planner/application/planner_search/planner_search_bloc.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';

class PlannerSearchPane extends StatelessWidget {
  final TextEditingController searchController;
  final PlannerSearchBloc bloc;
  const PlannerSearchPane({Key? key, required this.searchController, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.symmetric(horizontal: 12), child: 
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Text("Search Work Packages", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        const SizedBox(height: 12),
        DropdownSearch(
          items: WorkpackageSearchTypeEnum.values,
          onChanged: (field) => bloc.add(PlannerSearchEvent.changeSearchField(searchField: field ?? WorkpackageSearchTypeEnum.number)),
          itemAsString: (field) => translateSearchField(field)
        ),
        const SizedBox(height: 12),
        Row(children: [
          Expanded(child: TextFormField(
            controller: searchController,
            decoration: const InputDecoration(border: UnderlineInputBorder(), labelText: "Enter Search Term"),
            onFieldSubmitted: (input) => bloc.add(PlannerSearchEvent.search(keyword: input.trim())),
          )),
          CircleAvatar(radius: 25, backgroundColor: AppTheme.pgPrimaryRed, child:
            IconButton(
              icon: const Icon(Icons.search, color: Colors.white),
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                bloc.add(PlannerSearchEvent.search(keyword: searchController.text));
              },
            )
          )
        ])
      ])
    );
  }

  String translateSearchField(WorkpackageSearchTypeEnum field) {
    switch (field) {
      case WorkpackageSearchTypeEnum.number:
        return "Work Package Number";
      case WorkpackageSearchTypeEnum.description:
        return "Work Package Description";
      case WorkpackageSearchTypeEnum.plannerName:
        return "Planner Name";
      case WorkpackageSearchTypeEnum.functionalLocation:
        return "Functional Location";
      case WorkpackageSearchTypeEnum.equipmentNumber:
        return "Equipment Number";
      case WorkpackageSearchTypeEnum.equipmentType:
        return "Equipment Type";
      case WorkpackageSearchTypeEnum.equipmentSubtype:
        return "Equipment Subtype";
      case WorkpackageSearchTypeEnum.status:
        return "Work Package Status";
      case WorkpackageSearchTypeEnum.equipmentTagNumber:
        return "Equipment Tag Number";
    }
  }
}