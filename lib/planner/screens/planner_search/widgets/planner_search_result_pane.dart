import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/widgets/workpackage_item.dart';

class PlannerSearchResultPane extends StatelessWidget {
  final List<WorkpackageDTO> results;
  const PlannerSearchResultPane({Key? key, required this.results}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(flex: 3, child:
      Padding(padding: const EdgeInsets.only(top: 12), child:
        results.isNotEmpty ? ListView.builder(
          itemCount: results.length,
          itemBuilder: (context, index) {
            return WorkpackageItem(
              workpackageDTO: results[index],
            );
          }
        ) : const Center(child: Text("No Work Packages"))
      )
    );
  }
}