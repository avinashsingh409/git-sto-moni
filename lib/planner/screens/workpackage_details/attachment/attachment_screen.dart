import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/image_selector.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/attachments/attachments_bloc.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_details/attachment/attachment_zoom_pane.dart';

class AttachmentScreen extends StatefulWidget {
  const AttachmentScreen({super.key});

  @override
  State<AttachmentScreen> createState() => _AttachmentScreenState();
}

class _AttachmentScreenState extends State<AttachmentScreen> {
  final TextEditingController _nameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final bloc = getIt<AttachmentsBloc>();
    bloc.add(const AttachmentsEvent.getAllAttachmentsInWorkpackage());
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.8,
      child: Stack(
        children: [
          BlocProvider.value(
              value: bloc,
              child: BlocListener<AttachmentsBloc, AttachmentsState>(
                listener: (context, state) {
                  state.when(
                    initial: () {},
                    unauthorized: () => _showErrorToast(context: context),
                  );
                },
                child: StreamBuilder(
                    stream: bloc.getAllAttachments(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const LoadingIndicator();
                      } else if (snapshot.connectionState == ConnectionState.active ||
                          snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}');
                        } else if (snapshot.hasData) {
                          if (snapshot.data != null && snapshot.data!.isNotEmpty) {
                            return Container(
                              padding: const EdgeInsets.all(12),
                              child: SingleChildScrollView(
                                child: GridView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: snapshot.data?.length ?? 0,
                                  itemBuilder: ((context, index) {
                                    final currentImage = Image.memory(
                                      base64Decode(snapshot.data?[index].imageBytes ?? ''),
                                    );
                                    return GestureDetector(
                                      onLongPress: () => _showAdditionalDialog(
                                          context: context, attachmentDTO: snapshot.data?[index] ?? AttachmentDTO(), bloc: bloc),
                                      onTap: () => showZoomPane(context: context, image: currentImage),
                                      child: SizedBox(
                                        height: 400,
                                        width: 400,
                                        child: Card(
                                          semanticContainer: true,
                                          clipBehavior: Clip.antiAliasWithSaveLayer,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(10.0),
                                          ),
                                          elevation: 3,
                                          margin: const EdgeInsets.all(10),
                                          child: currentImage,
                                        ),
                                      ),
                                    );
                                  }),
                                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                                ),
                              ),
                            );
                          }
                        }
                      }
                      return const SizedBox();
                    }),
              )),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                child: const Icon(Icons.camera_alt),
                onPressed: () {
                  showImageSelection(
                      context: context,
                      onSelection: (imageString) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return Center(
                              child: Container(
                                margin: const EdgeInsets.all(20),
                                height: 200,
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 16,
                                    ),
                                    const Text(
                                      'Please provide a name',
                                      style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        child: TextField(
                                          enabled: true,
                                          decoration: const InputDecoration(
                                            hintText: 'Image name',
                                            hintStyle: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.italic,
                                            ),
                                          ),
                                          controller: _nameController,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        ElevatedButton(
                                          onPressed: () async {
                                            if (_nameController.text.isNotEmpty) {
                                              bloc.add(
                                                AttachmentsEvent.cacheNewAttachment(
                                                    newImageString: imageString, name: _nameController.text.trim()),
                                              );
                                              _nameController.clear();
                                              Navigator.of(context).pop();
                                            }
                                          },
                                          child: const Text('Add Image'),
                                        ),
                                        ElevatedButton(
                                          onPressed: () {
                                            _nameController.clear();
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text('Cancel'),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showAdditionalDialog(
      {required BuildContext context, required AttachmentDTO attachmentDTO, required AttachmentsBloc bloc}) {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              decoration: const BoxDecoration(
                  color: AppTheme.pgPrimaryRed,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  )),
              height: 80,
              width: 250,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop();
                      if (attachmentDTO.isNewlyAdded != null && attachmentDTO.isNewlyAdded!) {
                        jumpToImageAnnotationScreen(context: context, attachmentDTO: attachmentDTO);
                      } else {
                        _showErrorToast(context: context);
                      }
                    },
                    icon: const Icon(
                      Icons.edit,
                      color: Colors.white,
                      size: 20,
                    ),
                    label: const Text(
                      'Annotate',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  VerticalDivider(
                    thickness: 1,
                    color: Colors.white.withOpacity(0.5),
                  ),
                  TextButton.icon(
                    onPressed: () {
                      bloc.add(AttachmentsEvent.deleteAttachment(attachmentDTO: attachmentDTO));
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.delete,
                      color: Colors.white,
                      size: 20,
                    ),
                    label: const Text(
                      'Delete\t\t',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _showErrorToast({required BuildContext context}) {
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        const SnackBar(
          backgroundColor: AppTheme.pgPrimaryRed,
          content: Center(
            child: Text(
              'Feature only available for new attachments',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
          duration: Duration(seconds: 2),
        ),
      );
  }
}
