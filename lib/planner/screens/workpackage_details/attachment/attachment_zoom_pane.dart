import 'package:flutter/material.dart';

void showZoomPane({required BuildContext context, required Image image}) {
  showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.5,
            child: image,
          ),
        );
      });
}
