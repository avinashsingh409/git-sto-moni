import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

const TextStyle workpackageDetailsHeader = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w600,
  fontSize: 14,
  decoration: TextDecoration.underline,
);

BoxDecoration workpackageDecorationBubble = BoxDecoration(
    color: const Color.fromRGBO(93, 145, 223, 0.85),
    borderRadius: BorderRadius.circular(20),
    border: Border.all(color: const Color.fromRGBO(93, 145, 223, 1)));

var workpackageDetailsNavItems = [
  const BottomNavigationBarItem(
    icon: Icon(
      Icons.details,
    ),
    label: 'Details',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
  const BottomNavigationBarItem(
    icon: Icon(
      Icons.attach_file,
    ),
    label: 'Attachments',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
];
