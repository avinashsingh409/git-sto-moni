import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_linker_exception.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/utils/app_linker.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/work_package_details/work_package_details_bloc.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_details/attachment/attachment_screen.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_details/widgets/workpackage_group_view.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_details/workpackage_details_style.dart';

class WorkpackageDetailsScreen extends StatefulWidget {
  final WorkpackageDTO workpackage;
  const WorkpackageDetailsScreen({super.key, required this.workpackage});

  @override
  State<WorkpackageDetailsScreen> createState() => _WorkpackageDetailsScreenState();
}

class _WorkpackageDetailsScreenState extends State<WorkpackageDetailsScreen> {
  final bloc = getIt<WorkPackageDetailsBloc>();

  @override
  void initState() {
    bloc.add(const WorkPackageDetailsEvent.getWorkpackageDetails());
    super.initState();
  }

  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
        value: bloc,
        child: StreamBuilder(
          stream: bloc.getWorkpackageDetails(),
          builder: ((context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const LoadingIndicator();
            } else if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else if (snapshot.hasData) {
                if (snapshot.data != null) {
                  final workpackage = snapshot.data!;
                  return Scaffold(
                    appBar: AppBar(
                      title: Text(workpackage.description ?? "N/A"),
                    ),
                    body: IndexedStack(
                      index: currentIndex,
                      children: [
                        SizedBox(
                          child: Column(
                            children: [
                              WorkpackageInformation(
                                workpackage: workpackage,
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Padding(padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5), child: 
                                ElevatedButton(
                                  onPressed: () async {
                                    final wbsNode = await AppLinker().getLinkedWBS(workpackage.id ?? 0);
                                    wbsNode.fold(
                                      (wbsNode) {
                                        jumpToWbsScreen(context: context, wbsNode: wbsNode);
                                      },
                                      (exception) {
                                        showDialog(
                                          context: context, 
                                          builder: (context) => AlertDialog(
                                            title: Text(AppLinkerException.getErrorTitle(exception)),
                                            content: Text(AppLinkerException.getErrorMessage(exception)),
                                            actions: [
                                              TextButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop(false);
                                                },
                                                style: TextButton.styleFrom(
                                                  foregroundColor: AppTheme.pgPrimaryRed,
                                                  textStyle: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                  )
                                                ),
                                                child: Text(S.of(context).coreWarningDialogOkay.toUpperCase()),
                                              )
                                            ],
                                          )
                                        );
                                      }
                                    );
                                  },
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: AppTheme.pgPrimaryRed,
                                    minimumSize: const Size.fromHeight(42)
                                  ),
                                  child: const Text("View Work Package in STO Execution", style: TextStyle(fontWeight: FontWeight.w700)),
                                ),
                              ),
                              const WorkpackageGroupingView(),
                            ],
                          ),
                        ),
                        const AttachmentScreen(),
                      ],
                    ),
                    bottomNavigationBar: BottomAppBar(
                        height: MediaQuery.of(context).size.height * 0.07,
                        color: AppTheme.pgPrimaryRed,
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          height: 40,
                          child: Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                            TextButton(
                              onPressed: () {
                                setState(() {
                                  currentIndex = 0;
                                });
                              },
                              child: const Column(
                                children: <Widget>[
                                  Icon(
                                    size: 20,
                                    Icons.details,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    'Details',
                                    style: TextStyle(color: Colors.white, fontSize: 10),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              color: Colors.white38,
                              width: 1,
                            ),
                            TextButton(
                              onPressed: () {
                                setState(() {
                                  currentIndex = 1;
                                });
                              },
                              child: const Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.attach_file,
                                    size: 20,
                                    color: Colors.white,
                                  ),
                                  Text(
                                    'Attachments',
                                    style: TextStyle(color: Colors.white, fontSize: 10),
                                  )
                                ],
                              ),
                            ),
                          ]),
                        )),
                  );
                } else {
                  return const Text("Error parsing Activity");
                }
              }
            }
            return const SizedBox();
          }),
        ));
  }
}

class WorkpackageInformation extends StatelessWidget {
  final WorkpackageDTO workpackage;
  const WorkpackageInformation({super.key, required this.workpackage});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          width: double.maxFinite,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.vertical(
              bottom: Radius.circular(10.0),
            ),
            border: Border.all(color: Colors.black.withOpacity(0.2)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(
                height: 8,
              ),
              const Center(
                child: Text(
                  "Work Package Information",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              const Text(
                "Workpackage ID",
                style: workpackageDetailsHeader,
              ),
              const SizedBox(
                height: 4,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                decoration: workpackageDecorationBubble,
                child: Text(
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  workpackage.id.toString(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              const Text(
                "Description",
                style: workpackageDetailsHeader,
              ),
              const SizedBox(
                height: 4,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                decoration: workpackageDecorationBubble,
                child: Text(
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  workpackage.description?.trim() ?? "N/A",
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              const Text(
                "Planner Name",
                style: workpackageDetailsHeader,
              ),
              const SizedBox(
                height: 4,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                decoration: workpackageDecorationBubble,
                child: Text(
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  workpackage.plannerName ?? "N/A",
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
