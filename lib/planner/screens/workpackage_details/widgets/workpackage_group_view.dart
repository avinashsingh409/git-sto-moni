import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/config/navigation/models/field_definition_args.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/workpackage_grouping_view/workpackage_grouping_view_bloc.dart';
import 'package:sto_mobile_v2/planner/application/workpackage_subgrouping_view/workpackage_subgrouping_view_bloc.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_subgroup_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';

class WorkpackageGroupingView extends StatelessWidget {
  final String? dynamicColumns;
  const WorkpackageGroupingView({super.key, this.dynamicColumns});

  @override
  Widget build(BuildContext context) {
    final maingroupBloc = getIt<WorkpackageGroupingViewBloc>();
    final subgroupBloc = getIt<WorkpackageSubgroupingViewBloc>();
    maingroupBloc.add(const WorkpackageGroupingViewEvent.getAllGroupingData());
    return BlocProvider.value(
      value: maingroupBloc,
      child: BlocBuilder<WorkpackageGroupingViewBloc, WorkpackageGroupingViewState>(
        builder: (context, state) {
          return state.when(
            initial: () => const SizedBox(),
            loading: () => const LinearProgressIndicator(),
            success: (workpackageGroupings) {
              return Expanded(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 4,
                    ),
                    Expanded(child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 12),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.black.withOpacity(0.2),
                          ),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 1.0,
                              spreadRadius: 0.0,
                              offset: Offset(1.0, 1.0), // shadow direction: bottom right
                            )
                          ],
                          borderRadius: const BorderRadius.all(Radius.circular(12))),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 8,
                          ),
                          Card(
                            semanticContainer: true,
                            child: DropdownSearch<WorkpackageGroupingDTO>(
                              items: workpackageGroupings,
                              dropdownDecoratorProps: const DropDownDecoratorProps(
                                dropdownSearchDecoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                ),
                                baseStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                              selectedItem: workpackageGroupings[0],
                              itemAsString: (item) => item.name ?? "",
                              popupProps: PopupProps.menu(
                                showSearchBox: true,
                                searchFieldProps: TextFieldProps(
                                  autofocus: false,
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.search),
                                    hintText: S.current.stoEventDropDownSearchBoxHintText,
                                    border: const OutlineInputBorder(),
                                  ),
                                ),
                              ),
                              onChanged: (changedWorkpackageGrouping) {
                                subgroupBloc.add(WorkpackageSubgroupingViewEvent.getSubgroups(
                                    subgroups: changedWorkpackageGrouping?.subgroups ?? []));
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          WorkpackageSubgroupingView(
                            workpackageSubgroupingViewBloc: subgroupBloc,
                            initialSubGroups: workpackageGroupings[0].subgroups ?? [],
                          ),
                        ],
                      ),
                    )),
                    const SizedBox(height: 12)
                  ],
                ),
              );
            },
            empty: () => const Center(
              child: Text('No workpackagegroupings founnd'),
            ),
          );
        },
      ),
    );
  }
}

class WorkpackageSubgroupingView extends StatefulWidget {
  final WorkpackageSubgroupingViewBloc workpackageSubgroupingViewBloc;
  final List<WorkpackageSubgroupDTO>? initialSubGroups;
  final String? dynamicColumns;
  const WorkpackageSubgroupingView({
    super.key,
    required this.workpackageSubgroupingViewBloc,
    required this.initialSubGroups,
    this.dynamicColumns,
  });

  @override
  State<WorkpackageSubgroupingView> createState() => _WorkpackageSubgroupingViewState();
}

class _WorkpackageSubgroupingViewState extends State<WorkpackageSubgroupingView> {
  ScrollController scollBarController = ScrollController();

  @override
  void initState() {
    widget.workpackageSubgroupingViewBloc
        .add(WorkpackageSubgroupingViewEvent.getSubgroups(subgroups: widget.initialSubGroups ?? []));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: widget.workpackageSubgroupingViewBloc,
      child: BlocBuilder<WorkpackageSubgroupingViewBloc, WorkpackageSubgroupingViewState>(
        builder: (context, state) {
          return Expanded(
            child: state.when(
                initial: () => const SizedBox(),
                loading: () => const Center(
                      child: LoadingIndicator(),
                    ),
                success: (subgroups) {
                  return ListView.separated(
                      physics: const AlwaysScrollableScrollPhysics(), //
                      controller: scollBarController,
                      separatorBuilder: (BuildContext context, int index) {
                        return const Divider(
                          color: Colors.black26,
                          indent: 24,
                        );
                      },
                      shrinkWrap: true,
                      itemCount: subgroups.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () => jumpToFieldDefinitionScreen(
                              context: context,
                              fieldDefinitionArgs: FieldDefinitionScreenArguments(
                                workpackageSubgroupDTO: subgroups[index],
                                dynamicColumns: widget.dynamicColumns,
                              )),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 28),
                                child: Text(
                                  subgroups[index].name ?? "",
                                  style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                                ),
                              ),
                              const Icon(
                                Icons.chevron_right,
                                size: 40,
                                color: AppTheme.pgPrimaryBlack,
                              ),
                            ],
                          ),
                        );
                      });
                },
                noSubGroups: () => const Center(
                      child: Text('No subgroups'),
                    )),
          );
        },
      ),
    );
  }
}
