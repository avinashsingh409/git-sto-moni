import 'package:flutter/material.dart';

List fieldDefinitionColors = [
  const Color.fromRGBO(93, 145, 223, 0.85).withOpacity(0.4),
  Colors.greenAccent.withOpacity(0.5),
  Colors.amber.withOpacity(0.5),
];
