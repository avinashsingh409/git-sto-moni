import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition/field_definition_bloc.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/translation_key_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/enums/field_type.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_subgroup_dto.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_boolean.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_date.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_lookup.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_number.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_planner.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_primary_contractor.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_text.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/widgets/field_type_work_type.dart';

class FieldDefinitionScreen extends StatelessWidget {
  final WorkpackageSubgroupDTO subgroupDTO;
  final String? dynamicColumns;
  const FieldDefinitionScreen({super.key, required this.subgroupDTO, required this.dynamicColumns});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<FieldDefinitionBloc>();
    bloc.add(FieldDefinitionEvent.getFieldDefinitionWithValue(subGroupId: subgroupDTO.id ?? 0));
    return BlocProvider.value(
      value: bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(subgroupDTO.name ?? 'N/A'),
        ),
        body: BlocBuilder<FieldDefinitionBloc, FieldDefinitionState>(
          builder: (context, state) {
            return state.when(
                initial: () => const SizedBox(),
                loading: () => const LoadingIndicator(),
                valueLoaded: (staticFieldDefs, dynamicFieldDefs) {
                  return SizedBox(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 4,
                          ),
                          if (staticFieldDefs != null)
                            StaticFieldDefitions(
                              staticFieldDefs: staticFieldDefs,
                              bloc: bloc,
                            ),
                          const SizedBox(
                            height: 4,
                          ),
                          if (dynamicFieldDefs != null)
                            DynamicFieldDefitions(
                              bloc: bloc,
                              dynamicFieldDefs:
                                  dynamicFieldDefs.where((element) => element.definition?.fieldType != "contractor").toList(),
                            ),
                        ],
                      ),
                    ),
                  );
                },
                empty: () {
                  return const Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.dashboard_customize_rounded,
                          size: 80,
                          color: Colors.red,
                        ),
                        SizedBox(
                          height: 14,
                        ),
                        Text(
                          'No Fields Configured',
                          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                      ],
                    ),
                  );
                });
          },
        ),
      ),
    );
  }
}

class StaticFieldDefitions extends StatelessWidget {
  final List<WorkpackageFieldDefinitionDTO> staticFieldDefs;
  final FieldDefinitionBloc bloc;
  const StaticFieldDefitions({super.key, required this.staticFieldDefs, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ListView.separated(
        padding: EdgeInsets.zero,
        itemCount: staticFieldDefs.length,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: ((context, index) {
          final itrFielDefinition = staticFieldDefs[index];
          final translationKeyString = itrFielDefinition.definition?.translationKey?.toLowerCase();
          final translationEnum = TranslationKey.values.firstWhere(
            (element) => element.name.toString() == translationKeyString,
            orElse: () => TranslationKey.none,
          );
          switch (translationEnum) {
            case TranslationKey.primarycontractor:
              return FieldTypePrimaryContractor(onValueChanged: (newVal) {
                bloc.add(FieldDefinitionEvent.changeStaticValue(
                    translationKey: TranslationKey.primarycontractor, newVal: newVal, plannerName: null));
              });
            case TranslationKey.worktype:
              return FieldTypeWorkType(
                onValueChanged: (newValue) {
                  bloc.add(FieldDefinitionEvent.changeStaticValue(
                      translationKey: TranslationKey.worktype, newVal: newValue, plannerName: null));
                },
              );
            case TranslationKey.workscopedescription:
              return FieldTypeText(
                definition: itrFielDefinition,
                onValueChanged: (newValue, _) {
                  bloc.add(FieldDefinitionEvent.changeStaticValue(
                      translationKey: TranslationKey.workscopedescription, newVal: newValue, plannerName: null));
                },
                edit: true,
              );
            case TranslationKey.plannername:
              return FieldTypePlanner(
                onValueChanged: (newPlanner) {
                  bloc.add(
                    FieldDefinitionEvent.changeStaticValue(
                        translationKey: TranslationKey.plannername,
                        newVal: newPlanner.id.toString(),
                        plannerName: newPlanner.userDetails),
                  );
                },
              );
            case TranslationKey.worklistid:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.worklistitemstatus:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.islocked:
              return FieldTypeBoolean(
                definition: itrFielDefinition,
                onPressed: (newValue, guid) {
                  //
                },
                edit: false,
              );
            case TranslationKey.scoperequestnumber:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.sapworkorder:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.scopeapprovalcode:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.unitnumber:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.processsystems:
              return FieldTypeText(
                edit: false,
                definition: itrFielDefinition,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.equipmenttype:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.stosubtype:
              return FieldTypeText(
                edit: false,
                definition: itrFielDefinition,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.expensetype:
              return FieldTypeText(
                definition: itrFielDefinition,
                edit: false,
                onValueChanged: (newValue, guid) {
                  //
                },
              );
            case TranslationKey.none:
              return const SizedBox();
          }
        }),
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: 4,
          );
        },
      ),
    );
  }
}

class DynamicFieldDefitions extends StatelessWidget {
  final List<WorkpackageFieldDefinitionDTO> dynamicFieldDefs;
  final FieldDefinitionBloc bloc;
  const DynamicFieldDefitions({super.key, required this.dynamicFieldDefs, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          final itrFielDefinition = dynamicFieldDefs[index];
          final fieldType = itrFielDefinition.definition?.fieldType;
          final fieldTypeEnum = FieldType.values.firstWhere(
            (element) => element.name.toString() == fieldType,
            orElse: () => FieldType.none,
          );
          switch (fieldTypeEnum) {
            case FieldType.lookup:
              return FieldTypeLookUp(
                definition: itrFielDefinition,
                onValueChanged: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                edit: itrFielDefinition.edit,
              );
            case FieldType.boolean:
              return FieldTypeBoolean(
                definition: itrFielDefinition,
                onPressed: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                edit: itrFielDefinition.edit,
              );
            case FieldType.text:
              return FieldTypeText(
                definition: itrFielDefinition,
                onValueChanged: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                edit: itrFielDefinition.edit,
              );
            case FieldType.date:
              return FieldTypeDate(
                definition: itrFielDefinition,
                onPressed: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                isDateTime: false,
                edit: itrFielDefinition.edit,
              );
            case FieldType.number:
              return FieldTypeNumber(
                definition: itrFielDefinition,
                onValueChanged: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                edit: itrFielDefinition.edit,
              );
            case FieldType.datetime:
              return FieldTypeDate(
                definition: itrFielDefinition,
                onPressed: (newValue, guid) {
                  bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
                },
                isDateTime: true,
                edit: itrFielDefinition.edit,
              );
            // case FieldType.user:
            //   return FieldTypeLookUp(
            //     definition: itrFielDefinition,
            //     onValueChanged: (newValue, guid) {
            //       bloc.add(FieldDefinitionEvent.changeDynamicValue(guid: guid, newVal: newValue));
            //     },
            //   );
            default:
              return const SizedBox();
          }
        },
        itemCount: dynamicFieldDefs.length,
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: 4,
          );
        },
      ),
    );
  }
}
