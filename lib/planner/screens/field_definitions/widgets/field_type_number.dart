import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/common/screens/widgets/string_field.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

class FieldTypeNumber extends StatefulWidget {
  final WorkpackageFieldDefinitionDTO? definition;
  final Function(String newVal, String guid) onValueChanged;
  final bool? edit;
  const FieldTypeNumber({
    super.key,
    required this.definition,
    required this.onValueChanged,
    required this.edit,
  });

  @override
  State<FieldTypeNumber> createState() => _FieldTypeNumberState();
}

class _FieldTypeNumberState extends State<FieldTypeNumber> {
  final TextEditingController _fieldTypeTextController = TextEditingController();

  @override
  void initState() {
    // bloc.add(FieldDefinitionEvent.getFieldDefinitionValue(guid: widget.definition?.guid ?? ''));
    final initialVal = widget.definition?.value;
    if (initialVal != null) {
      _fieldTypeTextController.text = initialVal;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: const EdgeInsets.symmetric(horizontal: 8),
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: Colors.black12),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 1.0,
            spreadRadius: 0.0,
            offset: Offset(1.0, 1.0),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.definition?.caption ?? 'N/A',
            style: const TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            width: double.infinity,
            child: FieldTypeNumberField(
              hintText: widget.definition?.definition?.toolTip ?? "",
              controller: _fieldTypeTextController,
              valueChangedCallBack: (String value) {
                widget.onValueChanged(value, widget.definition?.guid ?? '');
              },
              allowDecimals: widget.definition?.definition?.allowDecimals ?? false,
              isReadOnly: (widget.edit != null) ? !widget.edit! : false,
            ),
          ),
        ],
      ),
    );
  }
}
