import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_planner/field_type_planner_bloc.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';

class FieldTypePlanner extends StatelessWidget {
  final Function(UserOptionsDTO newPlanner) onValueChanged;
  const FieldTypePlanner({super.key, required this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<FieldTypePlannerBloc>();
    bloc.add(const FieldTypePlannerEvent.getAllPlannerOptions());
    return BlocProvider.value(
      value: bloc,
      child: Container(
        height: 110,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: Colors.black12),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 0.0,
              offset: Offset(1.0, 1.0), // shadow direction: bottom right
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Planner Name",
              style: TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 8,
            ),
            BlocBuilder<FieldTypePlannerBloc, FieldTypePlannerState>(
              builder: (context, state) {
                return state.maybeWhen(
                  orElse: () => const SizedBox(),
                  success: (_, currentPlanner) => Text(currentPlanner?.userDetails ?? ''),
                );
              },
            ),
            const SizedBox(
              height: 4,
            ),
            Center(
              child: SizedBox(
                height: 32,
                child: ElevatedButton(
                  onPressed: () => showPlannerOptions(
                      context: context,
                      bloc: bloc,
                      onNewValueSelected: (newPlannerDTO) {
                        onValueChanged(newPlannerDTO);
                      }),
                  child: const Text('Select Planner'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

showPlannerOptions(
    {required BuildContext context,
    required FieldTypePlannerBloc bloc,
    required final Function(UserOptionsDTO newVal) onNewValueSelected}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return BlocProvider.value(
        value: bloc,
        child: Center(
          child: Container(
            color: Colors.white,
            height: 120,
            child: BlocBuilder<FieldTypePlannerBloc, FieldTypePlannerState>(
              builder: (context, state) {
                return state.when(
                    initial: () => const SizedBox(),
                    loading: () => const LoadingIndicator(),
                    success: (planners, initial) => Container(
                          padding: const EdgeInsets.all(8),
                          height: 80,
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 12,
                              ),
                              const Text(
                                'Please select a Planner',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Card(
                                child: DropdownSearch<UserOptionsDTO>(
                                  // enabled: isConnected,
                                  selectedItem: initial,
                                  items: planners,
                                  itemAsString: (item) => item.userDetails ?? '',
                                  popupProps: PopupProps.menu(
                                    showSearchBox: true,
                                    searchFieldProps: TextFieldProps(
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        prefixIcon: const Icon(Icons.search),
                                        hintText: S.current.stoEventDropDownSearchBoxHintText,
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                  onChanged: (changedPlanner) {
                                    if (changedPlanner != null) {
                                      onNewValueSelected(changedPlanner);
                                    }
                                    bloc.add(FieldTypePlannerEvent.updateInternalState(selectedPlanner: changedPlanner));
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                    empty: () => const Center(
                          child: Text('No Planner Data Found'),
                        ));
              },
            ),
          ),
        ),
      );
    },
  );
}
