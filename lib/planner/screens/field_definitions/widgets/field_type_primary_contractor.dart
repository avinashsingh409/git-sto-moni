import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_primary_contractor/field_type_primary_contractor_bloc.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';

class FieldTypePrimaryContractor extends StatelessWidget {
  final Function(String newVal) onValueChanged;
  const FieldTypePrimaryContractor({super.key, required this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<FieldTypePrimaryContractorBloc>();
    bloc.add(const FieldTypePrimaryContractorEvent.initializePrimaryContractors());
    return BlocProvider.value(
      value: bloc,
      child: Container(
        height: 110,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: Colors.black12),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 0.0,
              offset: Offset(1.0, 1.0),
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Contractor",
              style: TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 8,
            ),
            BlocBuilder<FieldTypePrimaryContractorBloc, FieldTypePrimaryContractorState>(
              builder: (context, state) {
                return state.maybeWhen(
                  orElse: () => const SizedBox(),
                  success: (_, currentContractor) => Text(currentContractor?.name ?? ''),
                );
              },
            ),
            const SizedBox(
              height: 4,
            ),
            Center(
              child: SizedBox(
                height: 32,
                child: ElevatedButton(
                  onPressed: () => showPrimaryContractorOptions(
                    context: context,
                    bloc: bloc,
                    onNewValueSelected: (String newVal) {
                      onValueChanged(newVal);
                    },
                  ),
                  child: const Text('Select Contractor'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

showPrimaryContractorOptions(
    {required BuildContext context,
    required FieldTypePrimaryContractorBloc bloc,
    required final Function(String newVal) onNewValueSelected}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return BlocProvider.value(
        value: bloc,
        child: Center(
          child: Container(
            color: Colors.white,
            height: 120,
            child: BlocBuilder<FieldTypePrimaryContractorBloc, FieldTypePrimaryContractorState>(
              builder: (context, state) {
                return state.when(
                    initial: () => const SizedBox(),
                    loading: () => const LoadingIndicator(),
                    success: (contractors, initial) => Container(
                          padding: const EdgeInsets.all(8),
                          height: 80,
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 12,
                              ),
                              const Text(
                                'Please select a Contractor',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Card(
                                child: DropdownSearch<ContractorOptionsDTO>(
                                  // enabled: isConnected,
                                  selectedItem: initial,
                                  items: contractors,
                                  itemAsString: (item) => item.name ?? '',
                                  popupProps: PopupProps.menu(
                                    showSearchBox: true,
                                    searchFieldProps: TextFieldProps(
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        prefixIcon: const Icon(Icons.search),
                                        hintText: S.current.stoEventDropDownSearchBoxHintText,
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                  onChanged: (changedPrimayContractor) {
                                    onNewValueSelected(changedPrimayContractor?.id.toString() ?? '');
                                    bloc.add(FieldTypePrimaryContractorEvent.updateInternalState(
                                        selectedContractor: changedPrimayContractor));
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                    empty: () => const Center(
                          child: Text('No Contractor Data Found'),
                        ));
              },
            ),
          ),
        ),
      );
    },
  );
}
