import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_lookup/field_type_lookup_cubit.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

class FieldTypeLookUp extends StatelessWidget {
  final WorkpackageFieldDefinitionDTO? definition;
  final Function(String newVal, String guid) onValueChanged;
  final bool? edit;
  const FieldTypeLookUp({
    super.key,
    required this.definition,
    required this.onValueChanged,
    required this.edit,
  });

  @override
  Widget build(BuildContext context) {
    final cubit = getIt<FieldTypeLookupCubit>();
    cubit.initializeDropdownItems(unparsedWorkpackageFieldDefinition: definition ?? WorkpackageFieldDefinitionDTO());
    return BlocProvider.value(
      value: cubit,
      child: Container(
        height: 100,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: Colors.black12),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 0.0,
              offset: Offset(1.0, 1.0), // shadow direction: bottom right
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              definition?.caption ?? 'N/A',
              style: const TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IgnorePointer(
                  ignoring: (edit != null) ? !edit! : false,
                  child: BlocBuilder<FieldTypeLookupCubit, FieldTypeLookupState>(
                    builder: (context, state) {
                      return state.when(
                        initial: () => const SizedBox(),
                        dropdownCreated: (options, currentSelected) {
                          return LookupDropDownButton(
                            options: options,
                            currentSelected: currentSelected,
                            cubit: cubit,
                            onChanged: (newVal) {
                              if (newVal != null) {
                                cubit.changeDropDownItem(newOption: newVal);
                                onValueChanged(newVal, definition?.guid ?? "");
                              }
                            },
                          );
                        },
                      );
                    },
                  ),
                ),
                if (edit != null && !edit!)
                  const Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.lock,
                      color: Colors.black54,
                    ),
                  )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class LookupDropDownButton extends StatelessWidget {
  final List<LookupValueDTO> options;
  final String? currentSelected;
  final FieldTypeLookupCubit cubit;
  final ValueChanged<String?>? onChanged;
  const LookupDropDownButton({
    super.key,
    required this.options,
    required this.currentSelected,
    required this.cubit,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(4),
            child: DropdownButton<String?>(
                underline: Container(
                  height: 1,
                  color: Colors.black38,
                ),
                value: currentSelected,
                items: options
                    .map<DropdownMenuItem<String?>>((LookupValueDTO item) => DropdownMenuItem<String?>(
                        value: item.caption, // add this property an pass the _value to it
                        child: Text(
                          item.caption ?? '',
                        )))
                    .toList(),
                onChanged: onChanged),
          )),
    );
  }
}
