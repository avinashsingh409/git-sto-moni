import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_boolean/field_type_boolean_cubit.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

class FieldTypeBoolean extends StatelessWidget {
  final WorkpackageFieldDefinitionDTO? definition;
  final Function(String newVal, String guid) onPressed;
  final bool? edit;
  const FieldTypeBoolean({
    super.key,
    required this.definition,
    required this.onPressed,
    required this.edit,
  });

  @override
  Widget build(BuildContext context) {
    final cubit = getIt<FieldTypeBooleanCubit>();
    cubit.intializeBoolValue(unparsedWorkpackageFieldDefinition: definition ?? WorkpackageFieldDefinitionDTO());
    return BlocProvider.value(
      value: cubit,
      child: Container(
        height: 100,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,
          border: Border.all(color: Colors.black12),
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 0.0,
              offset: Offset(1.0, 1.0), // shadow direction: bottom right
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              definition?.caption ?? 'N/A',
              style: const TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            IgnorePointer(
              ignoring: (edit != null) ? !edit! : false,
              child: BlocBuilder<FieldTypeBooleanCubit, FieldTypeBooleanState>(
                builder: (context, state) {
                  return state.when(
                      initial: () => const SizedBox(),
                      valueChanged: (changedVal) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    cubit.changeBoolVal(changedValue: true);
                                    onPressed("true", definition?.guid ?? "");
                                  },
                                  icon: Icon(
                                    changedVal != null && changedVal ? Icons.radio_button_on : Icons.radio_button_off,
                                    size: 20,
                                  ),
                                ),
                                const Text("True")
                              ],
                            ),
                            Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    cubit.changeBoolVal(changedValue: false);
                                    onPressed("false", definition?.guid ?? "");
                                  },
                                  icon: Icon(
                                    changedVal != null && !changedVal ? Icons.radio_button_on : Icons.radio_button_off,
                                    size: 20,
                                  ),
                                ),
                                const Text("False")
                              ],
                            ),
                            (edit != null && !edit!)
                                ? const Padding(
                                    padding: EdgeInsets.only(right: 10),
                                    child: Icon(
                                      Icons.lock,
                                      color: Colors.black54,
                                    ),
                                  )
                                : const SizedBox(
                                    width: 38,
                                  ),
                          ],
                        );
                      });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
