import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_work_type/field_type_work_type_bloc.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';

class FieldTypeWorkType extends StatelessWidget {
  final Function(String newVal) onValueChanged;
  const FieldTypeWorkType({super.key, required this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<FieldTypeWorkTypeBloc>();
    bloc.add(const FieldTypeWorkTypeEvent.initializeWorkTypes());
    return BlocProvider.value(
      value: bloc,
      child: Container(
        height: 110,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: Colors.black12),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 1.0,
              spreadRadius: 0.0,
              offset: Offset(1.0, 1.0),
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Work Type",
              style: TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 8,
            ),
            BlocBuilder<FieldTypeWorkTypeBloc, FieldTypeWorkTypeState>(
              builder: (context, state) {
                return state.maybeWhen(
                  orElse: () => const SizedBox(),
                  success: (_, currentWorkType) => Text(
                    currentWorkType?.description ?? '',
                  ),
                );
              },
            ),
            const SizedBox(
              height: 4,
            ),
            Center(
              child: SizedBox(
                height: 32,
                child: ElevatedButton(
                  onPressed: () => showWorkTypeOptions(
                      context: context,
                      bloc: bloc,
                      onNewValueSelected: (String newVal) {
                        onValueChanged(newVal);
                      }),
                  child: const Text('Select Work Type'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

showWorkTypeOptions(
    {required BuildContext context,
    required FieldTypeWorkTypeBloc bloc,
    required final Function(String newVal) onNewValueSelected}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return BlocProvider.value(
        value: bloc,
        child: Center(
          child: Container(
            color: Colors.white,
            height: 120,
            child: BlocBuilder<FieldTypeWorkTypeBloc, FieldTypeWorkTypeState>(
              builder: (context, state) {
                return state.when(
                    initial: () => const SizedBox(),
                    loading: () => const LoadingIndicator(),
                    success: (workTypes, initial) => Container(
                          padding: const EdgeInsets.all(8),
                          height: 80,
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 12,
                              ),
                              const Text(
                                'Please select a Work Type',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(
                                height: 4,
                              ),
                              Card(
                                child: DropdownSearch<WorkTypeOptionsDTO>(
                                  selectedItem: initial,
                                  items: workTypes,
                                  itemAsString: (item) => item.description ?? '',
                                  popupProps: PopupProps.menu(
                                    showSearchBox: true,
                                    searchFieldProps: TextFieldProps(
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        prefixIcon: const Icon(Icons.search),
                                        hintText: S.current.stoEventDropDownSearchBoxHintText,
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                  onChanged: (changedWorkType) {
                                    onNewValueSelected(changedWorkType?.id.toString() ?? '');
                                    bloc.add(FieldTypeWorkTypeEvent.updateInternalState(selectedWorkType: changedWorkType));
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                    empty: () => const Center(
                          child: Text('No Work Type Data Found'),
                        ));
              },
            ),
          ),
        ),
      );
    },
  );
}
