import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/field_definition_types/field_type_date/field_type_date_cubit.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

class FieldTypeDate extends StatelessWidget {
  final WorkpackageFieldDefinitionDTO? definition;
  final Function(String newVal, String guid) onPressed;
  final bool isDateTime;
  final bool? edit;
  const FieldTypeDate({
    super.key,
    required this.definition,
    required this.onPressed,
    required this.isDateTime,
    required this.edit,
  });

  @override
  Widget build(BuildContext context) {
    final cubit = getIt<FieldTypeDateCubit>();
    cubit.intializedDateValue(unparsedWorkpackageFieldDefinition: definition ?? WorkpackageFieldDefinitionDTO());
    return BlocProvider.value(
      value: cubit,
      child: BlocBuilder<FieldTypeDateCubit, FieldTypeDateState>(
        builder: (context, state) {
          return state.when(
              initial: () => const SizedBox(),
              dateChanged: (dateVal) {
                return Container(
                  height: 140,
                  margin: const EdgeInsets.symmetric(horizontal: 8),
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.black12),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black12,
                        blurRadius: 1.0,
                        spreadRadius: 0.0,
                        offset: Offset(1.0, 1.0), // shadow direction: bottom right
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        definition?.caption ?? 'N/A',
                        style:
                            const TextStyle(overflow: TextOverflow.ellipsis, color: Colors.black87, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Row(
                        children: [
                          Text(
                            dateVal != null ? "${dateVal.month}/${dateVal.day}/${dateVal.year}" : '',
                            style: const TextStyle(fontWeight: FontWeight.w500),
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          isDateTime
                              ? Text(
                                  dateVal != null ? "${dateVal.hour}:${dateVal.minute.toString().padLeft(2, "0")}" : '',
                                  style: const TextStyle(fontWeight: FontWeight.w500),
                                )
                              : const SizedBox(),
                        ],
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      IgnorePointer(
                        ignoring: (edit != null) ? !edit! : false,
                        child: Center(
                          child: SizedBox(
                            height: 32,
                            child: ElevatedButton(
                              onPressed: () async {
                                await cubit
                                    .selectDate(unparsedWorkpackageFieldDefinition: definition ?? WorkpackageFieldDefinitionDTO())
                                    .then((newValue) {
                                  if (newValue != null) {
                                    onPressed(newValue, definition?.guid ?? "");
                                  }
                                });
                              },
                              child: const Text('Select date'),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SizedBox(
                            height: 12,
                          ),
                          if (edit != null && !edit!)
                            const Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Icon(
                                Icons.lock,
                                color: Colors.black54,
                              ),
                            )
                        ],
                      ),
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
