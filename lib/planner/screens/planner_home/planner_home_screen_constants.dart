import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/sync_ticker/sync_ticker_bloc.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/screens/sync/sync_screen.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/screens/planner_search/planner_search_screen.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/workpackage_list_screen.dart';

final plannerScreens = [
  const WorkpackageListScreen(),
  const PlannerSearchScreen(),
  const SyncScreen(),
];

var navItems = [
  const BottomNavigationBarItem(
    icon: Icon(
      Icons.home,
    ),
    label: 'Home',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
  const BottomNavigationBarItem(
    icon: Icon(Icons.search),
    label: 'Search',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
  BottomNavigationBarItem(
    icon: BlocProvider(
      create: (context) => getIt<SyncTickerBloc>(),
      child: BlocBuilder<SyncTickerBloc, SyncTickerState>(
        builder: (context, state) {
          return state.maybeWhen(
              orElse: () => const SizedBox(),
              noChanges: () => const Icon(
                    Icons.sync,
                  ),
              syncPending: () => Stack(
                    children: [
                      Positioned(
                        right: 0,
                        child: Container(
                          padding: const EdgeInsets.all(1),
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: const BoxConstraints(
                            minWidth: 12,
                            minHeight: 12,
                          ),
                        ),
                      ),
                      const Icon(
                        Icons.sync,
                      ),
                    ],
                  ));
        },
      ),
    ),
    label: 'Sync',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
];

const TextStyle drawerNameStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
