import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/logout/logout_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_drawer.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_switcher.dart';
import 'package:sto_mobile_v2/common/screens/widgets/data_loss_dialog.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/app_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/screens/planner_home/planner_home_screen_constants.dart';

class PlannerHomeScreen extends StatefulWidget {
  const PlannerHomeScreen({super.key});

  @override
  State<PlannerHomeScreen> createState() => _PlannerHomeScreenState();
}

class _PlannerHomeScreenState extends State<PlannerHomeScreen> {
  final logoutBloc = getIt<LogoutBloc>();
  int currentIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: logoutBloc,
      child: BlocListener<LogoutBloc, LogoutState>(
        listener: (context, state) {
          state.maybeWhen(
            orElse: () {},
            cacheNotEmpty: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DataLossDialog(
                    onConfirm: () async {
                      logoutBloc.add(const LogoutEvent.forceLogout());
                    }
                  );
                }
              );
            },
            error: (errMsg) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                      backgroundColor: AppTheme.pgPrimaryRed,
                      content: Center(
                        child: Text(
                          errMsg,
                        ),
                      ),
                      duration: const Duration(seconds: 4)),
                );
            },
            success: () {
              jumpToLoginScreen(context: context, isSessionRestoration: false);
            },
          );
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          key: _scaffoldKey,
          appBar: AppBar(
            title: const Text('STO $planner'),
            leading: IconButton(
              iconSize: 32,
              onPressed: () {
                _scaffoldKey.currentState?.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
            actions: const [
              AppSwitcherCog(),
            ],
          ),
          body: IndexedStack(index: currentIndex, children: plannerScreens),
          drawer: AppDrawer(
            logoutBloc: logoutBloc,
          ),
          bottomNavigationBar: SizedBox(
            height: MediaQuery.of(context).size.height * .106,
            child: BottomNavigationBar(
              showSelectedLabels: true,
              showUnselectedLabels: true,
              type: BottomNavigationBarType.shifting,
              currentIndex: currentIndex,
              unselectedItemColor: Colors.white60,
              elevation: 0,
              items: navItems,
              onTap: (index) {
                setState(() {
                  currentIndex = index;
                });
              },
            ),
          ),
        ),
      ),
    );
  }
}
