import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_painter/image_painter.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/image_annotation/image_annotation_bloc.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';

class ImageAnnotationScreen extends StatefulWidget {
  final AttachmentDTO attachmentDTO;
  const ImageAnnotationScreen({super.key, required this.attachmentDTO});

  @override
  State<ImageAnnotationScreen> createState() => _ImageAnnotationScreenState();
}

class _ImageAnnotationScreenState extends State<ImageAnnotationScreen> {
  final _imageKey = GlobalKey<ImagePainterState>();

  final bloc = getIt<ImageAnnotationBloc>();

  @override
  void initState() {
    bloc.add(ImageAnnotationEvent.parseImage(base64ImageString: widget.attachmentDTO.imageBytes ?? ''));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: Scaffold(
        appBar: AppBar(),
        body: BlocConsumer<ImageAnnotationBloc, ImageAnnotationState>(
          listener: (context, state) {
            state.maybeWhen(
              orElse: () {},
              imageUpdated: (_) => ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(const SnackBar(
                    backgroundColor: AppTheme.pgPrimaryRed,
                    content: Center(
                      child: Text(
                        'Image Updated',
                      ),
                    ),
                    duration: Duration(seconds: 2))),
            );
          },
          builder: (context, state) {
            return state.maybeWhen(
                orElse: () => const SizedBox(),
                loading: () => const Center(child: LoadingIndicator()),
                imageParsed: (imageUnit8list) {
                  return ImageEditorFrame(
                    imageKey: _imageKey,
                    imageUnit8list: imageUnit8list,
                  );
                },
                parseError: () => const Center(
                      child: Text('Image could not be Formatted'),
                    ),
                imageUpdated: (updatedImageUnit8list) {
                  return ImageEditorFrame(
                    imageKey: _imageKey,
                    imageUnit8list: updatedImageUnit8list,
                  );
                });
          },
        ),
        floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.save),
            onPressed: () async {
              final updatedImageUnit8list = await _imageKey.currentState?.exportImage();
              if (updatedImageUnit8list != null) {
                bloc.add(ImageAnnotationEvent.updateImage(
                    updatedImageUnit8list: updatedImageUnit8list, currentAttachment: widget.attachmentDTO));
              }
            }),
      ),
    );
  }
}

class ImageEditorFrame extends StatelessWidget {
  final Uint8List imageUnit8list;
  const ImageEditorFrame({
    super.key,
    required GlobalKey<ImagePainterState> imageKey,
    required this.imageUnit8list,
  }) : _imageKey = imageKey;

  final GlobalKey<ImagePainterState> _imageKey;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.8,
      child: ImagePainter.memory(
        imageUnit8list,
        key: _imageKey,
        scalable: true,
        initialStrokeWidth: 2,
        initialColor: Colors.green,
        initialPaintMode: PaintMode.line,
      ),
    );
  }
}
