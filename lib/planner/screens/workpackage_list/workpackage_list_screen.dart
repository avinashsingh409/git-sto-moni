import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/application/workpackage_list_screen/workpackage_list_screen_bloc.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_filter_type_enum.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/widgets/workpackage_filter_pane.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/widgets/workpackage_item.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_list/workpackage_list_style.dart';

class WorkpackageListScreen extends StatelessWidget {
  const WorkpackageListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<WorkpackageListScreenBloc>();
    bloc.add(const WorkpackageListScreenEvent.getWorkPackages());
    return BlocProvider(
      create: (context) => bloc,
      child: Column(
        children: [
          const SizedBox(
            height: 12,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BlocBuilder<WorkpackageListScreenBloc, WorkpackageListScreenState>(
                  builder: (context, state) {
                    return state.maybeWhen(
                        orElse: () => const SizedBox(),
                        success: (_, filterType) {
                          switch (filterType) {
                            case WorkpackageFilterTypeEnum.plannedByUser:
                              return const Text(
                                'Filter Status : Planned by User',
                                style: workpackageFilterTitleStyle,
                              );

                            case WorkpackageFilterTypeEnum.alphabetic:
                              return const Text(
                                'Filter Status : Alphabetically',
                                style: workpackageFilterTitleStyle,
                              );

                            case WorkpackageFilterTypeEnum.clearFilters:
                              return const Text(
                                'Filter Status : No Filters',
                                style: workpackageFilterTitleStyle,
                              );
                          }
                        });
                  },
                ),
                GestureDetector(
                  onTap: () {
                    showWorkpackageFilterDialogue(bloc: bloc, context: context);
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(right: 12),
                    child: Icon(
                      size: 32,
                      Icons.format_indent_decrease_sharp,
                      color: AppTheme.pgPrimaryRed,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          BlocBuilder<WorkpackageListScreenBloc, WorkpackageListScreenState>(
            builder: (context, state) {
              return state.when(
                initial: () => const SizedBox(),
                loading: () => const LoadingIndicator(),
                success: (workpackages, _) {
                  if (workpackages.isNotEmpty) {
                    return Expanded(
                      child: ListView.builder(
                          itemCount: workpackages.length,
                          itemBuilder: (context, index) {
                            return WorkpackageItem(
                              workpackageDTO: workpackages[index],
                            );
                          }),
                    );
                  } else {
                    return const Center(
                      child: Text("No Workpackages"),
                    );
                  }
                },
                error: ((errorMsg) {
                  return Center(
                    child: Text(errorMsg),
                  );
                }),
              );
            },
          ),
        ],
      ),
    );
  }
}
