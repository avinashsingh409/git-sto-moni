import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/planner/application/workpackage_list_screen/workpackage_list_screen_bloc.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_filter_type_enum.dart';

Future<void> showWorkpackageFilterDialogue({required BuildContext context, required WorkpackageListScreenBloc bloc}) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      var selectedOption = WorkpackageSelectedFilterOption.selectedOption;
      return AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Filter By',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 2,
              ),
              child: SizedBox(
                height: 30,
                width: 100,
                child: ElevatedButton(
                  onPressed: () {
                    selectedOption = WorkpackageFilterTypeEnum.clearFilters;
                    WorkpackageSelectedFilterOption().changeFilter(changedFilter: selectedOption);
                    bloc.add(const WorkpackageListScreenEvent.getWorkPackages());
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.grey[500]),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: Text('X   Clear'),
                  ),
                ),
              ),
            ),
          ],
        ),
        content: StatefulBuilder(
          builder: (context, setState) {
            return Theme(
              data: Theme.of(context).copyWith(unselectedWidgetColor: AppTheme.pgPrimaryBlack),
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Divider(
                      color: AppTheme.pgPrimaryBlack,
                    ),
                    RadioListTile<WorkpackageFilterTypeEnum>(
                        title: const Text('Planned by User'),
                        contentPadding: EdgeInsets.zero,
                        value: WorkpackageFilterTypeEnum.plannedByUser,
                        groupValue: selectedOption,
                        onChanged: (val) {
                          if (val != null) {
                            setState(() {
                              selectedOption = val;
                            });
                          }
                        }),
                    RadioListTile<WorkpackageFilterTypeEnum>(
                        title: const Text('Alphabetically'),
                        contentPadding: EdgeInsets.zero,
                        value: WorkpackageFilterTypeEnum.alphabetic,
                        groupValue: selectedOption,
                        onChanged: (val) {
                          if (val != null) {
                            setState(() {
                              selectedOption = val;
                            });
                          }
                        }),
                  ],
                ),
              ),
            );
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    WorkpackageSelectedFilterOption().changeFilter(changedFilter: selectedOption);
                    bloc.add(const WorkpackageListScreenEvent.getWorkPackages());
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: Text('Filter'),
                  ),
                ),
                ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 5,
                    ),
                    child: Text('Cancel'),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    },
  );
}

@lazySingleton
class WorkpackageSelectedFilterOption {
  static WorkpackageFilterTypeEnum selectedOption = WorkpackageFilterTypeEnum.clearFilters;

  void changeFilter({required WorkpackageFilterTypeEnum changedFilter}) {
    selectedOption = changedFilter;
  }
}
