import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

class WorkpackageItem extends StatelessWidget {
  final WorkpackageDTO workpackageDTO;
  const WorkpackageItem({super.key, required this.workpackageDTO});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        SelectedWorkPackage().changeSelectedPackage(
            newDynamicColumns: workpackageDTO.dynamicColumns,
            newWorkpackageId: workpackageDTO.id,
            selectedWorkpackage: workpackageDTO);
        jumpToWorkpackagesDetailsScreen(context: context, workpackageDTO: workpackageDTO);
      },
      child: Card(
        elevation: 5,
        color: Colors.white,
        margin: const EdgeInsets.all(5),
        borderOnForeground: true,
        child: Row(
          children: [
            const SizedBox(
              height: 75,
              width: 17,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      getWorkpackageTitle() ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            "Planner Name",
                            style: TextStyle(
                              fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                              color: Colors.grey.shade700,
                            ),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "Equipment Type",
                            style: TextStyle(
                              fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                              color: Colors.grey.shade700,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            "${workpackageDTO.plannerName ?? 'N/A'} ",
                            style: TextStyle(
                              fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                              color: Colors.grey.shade700,
                            ),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            workpackageDTO.equipmentType?.name ?? 'N/A',
                            style: TextStyle(
                              fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                              color: Colors.grey.shade700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String? getWorkpackageTitle() {
    if (workpackageDTO.scopeIdentifier != null) {
      return "${workpackageDTO.scopeIdentifier ?? ''} - ${workpackageDTO.description ?? ''}";
    } else {
      return "${workpackageDTO.id ?? 0} - ${workpackageDTO.description ?? ''}";
    }
  }
}
