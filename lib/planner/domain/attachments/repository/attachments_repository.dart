import 'package:dartz/dartz.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/attachment_exception.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';

abstract class IAttachmentsRepository {
  Future<List<AttachmentDTO>?> getAttachments();
  BehaviorSubject<List<AttachmentDTO>> getCachedAttachmentByWorklistId();
  Future<Either<Unit, AttachmentException>> markAttachmentAsDelete({required AttachmentDTO attachmentDTO});
  Future<void> insertAllAttachments({required List<AttachmentDTO> attachmentDTOs});
  Future<void> deleteAllAttachments();
  Future<List<AttachmentDTO>?> getAllDeletedAttachments();
  Future<int> updateImage({required String updatedImageThumbnailBytes, required AttachmentDTO currentAttachment});
  Future<void> insertNewImage({required String newImage64String, required String name});
  Future<List<AttachmentDTO>?> getAttachmentsToUpload();
}
