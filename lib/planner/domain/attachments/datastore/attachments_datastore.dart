import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';

abstract class IAttachmentsDataStore {
  Future<List<AttachmentDTO>?> getAttachments();
  BehaviorSubject<List<AttachmentDTO>> getCachedAttachmentByWorklistId({required int workpackageId});
  Future<void> insertAllAttachments({required List<AttachmentDTO> attachmentDTOs});
  Future<void> markAttachmentAsDelete({required AttachmentDTO attachmentDTO});
  Future<List<AttachmentDTO>?> getAllDeletedAttachments();
  Future<void> deleteAllAttachments();
  Future<int> updateImage({required AttachmentDTO updatedAttachment});
  Future<void> insertNewImage({required AttachmentDTO newAttachment});
  Future<List<AttachmentDTO>?> getAttachmentsToUpload();
}
