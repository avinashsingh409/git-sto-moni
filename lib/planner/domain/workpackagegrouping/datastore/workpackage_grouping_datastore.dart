import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/workpackage_grouping_response.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';

abstract class IWorkpackageGroupingDataStore {
  Future<WorkpackageGroupingResponse?> getAllGroupings({required int turnaroundEventId});
  Future<void> insertAllWorkpackageGroupings({required List<WorkpackageGroupingDTO> workpackageGroupingDTOs});
  Future<void> deleteAllWorkpackageGroupings();
  Future<List<WorkpackageGroupingDTO>> getAllWorkPackageGroupings();
  Future<List<WorkpackageFieldDefinitionDTO>> getAllFieldDefinitionsBySubgroupId({required int subGroupId});
}
