import 'package:dartz/dartz.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/workpackage_exception.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

abstract class IWorkpackageRepository {
  Future<Either<Unit, WorkpackagesException>> getAndCachePlannerData({required int turnaroundEventId});
  BehaviorSubject<WorkpackageDTO?> getWorkpackagedById();
  Future<WorkpackageDTO?> getSingleWorkPackage({required int workPackageId});
  BehaviorSubject<List<WorkpackageDTO>> getAllCachedWorkPackages();
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesPlannedByUser();
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesAlphabetically();
}
