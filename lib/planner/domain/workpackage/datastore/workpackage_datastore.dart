import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_response.dart';

abstract class IWorkpackageDataStore {
  Future<List<WorkpackageResponse>?> getOnlineWorkPackages({required int turnaroundEventId});
  Future<void> insertAllWorkpackages({required List<WorkpackageDTO> workpackageDTOs});
  BehaviorSubject<List<WorkpackageDTO>> getAllCachedWorkPackages();
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesPlannedByUser({required String preferredUsername});
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesAlphabetically();
  Future<void> deleteAllWorkpackages();
  BehaviorSubject<WorkpackageDTO?> getWorkpackagedById({required int workpackageId});
  Future<WorkpackageDTO?> getSingleWorkPackage({required int workPackageId});
  Future<void> updateStaticFields({required WorkpackageDTO newWorkpackageDTO});
}
