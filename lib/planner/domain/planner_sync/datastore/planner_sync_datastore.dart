import 'dart:io';

import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_model.dart';

abstract class IPlannerSyncDataStore {
  Future<List<FieldDefinitionValueDTO>?> getAllChangedFieldDefinitions();
  Future<List<StaticFieldDefinitionValueDTO>?> getAllChangedStaticFieldDefinitions();
  BehaviorSubject<bool> isSyncPending();
  Future<void> pushDataToServer({required List<PlannerSyncModel> changedData, required int turnAroundEventId});
  Future<void> uploadAttachments({required List<File> attachments});
}
