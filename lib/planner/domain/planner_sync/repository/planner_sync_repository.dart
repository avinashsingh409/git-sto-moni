import 'package:dartz/dartz.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/attachment_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/sync_exception.dart';

abstract class IPlannerSyncRepository {
  Future<Either<Unit, SyncException>> pushDataToServer({required int turnAroundEventId});
  Future<Either<Unit, AttachmentException>> uploadAttachments();
  BehaviorSubject<bool> isSyncPending();
}
