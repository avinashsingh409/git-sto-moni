import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

abstract class IFieldDefinitionDataStore {
  Future<void> insertAllFieldDefinitionValues({required List<FieldDefinitionValueDTO> fieldDefinitionValues});
  Future<void> cacheAllStaticFieldsOptions({
    required List<UserOptionsDTO>? userOptions,
    required List<WorkTypeOptionsDTO>? workTypeOptions,
    required List<ContractorOptionsDTO>? contractorOptions,
  });
  Future<List<FieldDefinitionValueDTO>> getAllFieldDefinitionValuesFromWorkpackageId({required int workpackageId});
  Future<List<FieldDefinitionValueDTO>?> getAllChangedFieldDefinitions();
  Future<List<StaticFieldDefinitionValueDTO>?> getAllChangedStaticFielddefinitions();
  Future<void> updateDynamicFieldDefinitionValue({required String guid, required String newVal});
  Future<void> updateStaticFields({required WorkpackageDTO newWorkpackageDTO});
  Future<List<UserOptionsDTO>?> getAllPlannerOptions();
  Future<UserOptionsDTO?> getCurrentPlanner();
  Future<List<ContractorOptionsDTO>?> getAllContractorsOptions();
  Future<ContractorOptionsDTO?> getCurrentContractor();
  Future<List<WorkTypeOptionsDTO>?> getAllWorkTypeOptions();
  Future<WorkTypeOptionsDTO?> getCurrentWorkType();
  Future<void> cacheStaticFieldChanges({required String translationKey, required String newValue});
  Future<void> clearWorkpackageCache();
  Future<void> clearAllStaticFieldOptions();
}
