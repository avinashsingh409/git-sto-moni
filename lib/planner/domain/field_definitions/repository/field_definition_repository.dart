import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/translation_key_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

abstract class IFieldDefinitionRepository {
  Future<void> insertAllFieldDefinitionValues({required List<FieldDefinitionValueDTO> fieldDefinitionValues});
  Future<void> cacheAllStaticFieldsOptions({
    required List<UserOptionsDTO>? userOptions,
    required List<WorkTypeOptionsDTO>? workTypeOptions,
    required List<ContractorOptionsDTO>? contractorOptions,
  });
  Future<void> clearWorkpackageValues();
  Future<List<WorkpackageFieldDefinitionDTO>> getDynamicFieldDefinitionsWithValues({
    required int subGroupId,
  });
  Future<List<WorkpackageFieldDefinitionDTO>> getStaticFieldDefinitionsWithValues({
    required int subGroupId,
  });

  Future<List<UserOptionsDTO>?> getAllPlannerOptions();
  Future<UserOptionsDTO?> getCurrentPlanner();
  Future<ContractorOptionsDTO?> getCurrentContractor();
  Future<WorkTypeOptionsDTO?> getCurrentWorkType();
  Future<List<ContractorOptionsDTO>?> getAllContractorsOptions();
  Future<List<WorkTypeOptionsDTO>?> getAllWorkTypeOptions();

  Future<void> updateDynamicFieldDefinitionValue({required String guid, required String newVal});
  Future<void> updateStaticFieldDefinitionValue(
      {required TranslationKey translationKey, required String newVal, required String? plannerName});
}
