import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

abstract class IPlannerSearchRepository {
  BehaviorSubject<List<WorkpackageDTO>> getSearchedWorkPackages({required WorkpackageSearchTypeEnum field, required String keyword});
}