import 'dart:io';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';

@injectable
class AttachmentSyncApiService {
  final Dio dio;

  AttachmentSyncApiService(this.dio);

  Future<void> uploadAttachments({required List<File> attachments}) async {
    List<MultipartFile> files = [];
    for (var item in attachments) {
      MultipartFile file = await MultipartFile.fromFile(
        item.path,
        filename: item.toString(),
      );
      files.add(file);
    }
    var formData = FormData.fromMap({
      'files': files,
    });
    await dio.post(ApiEndPoints.attachmentSync, data: formData);
  }
}
