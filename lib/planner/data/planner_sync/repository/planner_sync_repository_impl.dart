import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/attachment_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/sync_exception.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/repository/sync_ticker_repository.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_model.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/repository/attachments_repository.dart';
import 'package:sto_mobile_v2/planner/domain/planner_sync/datastore/planner_sync_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/planner_sync/repository/planner_sync_repository.dart';

@LazySingleton(as: IPlannerSyncRepository)
class PlannerSyncRepositoryImpl extends IPlannerSyncRepository {
  final IPlannerSyncDataStore plannerSyncDataStore;
  final IAttachmentsRepository attachmentsRepository;
  final ISyncTickerRepository syncTickerRepository;
  PlannerSyncRepositoryImpl(this.plannerSyncDataStore, this.attachmentsRepository, this.syncTickerRepository);

  @override
  Future<Either<Unit, SyncException>> pushDataToServer({required int turnAroundEventId}) async {
    final changedDynamicData = await plannerSyncDataStore.getAllChangedFieldDefinitions();
    final changedStaticData = await plannerSyncDataStore.getAllChangedStaticFieldDefinitions();
    final deletedAttachments = await attachmentsRepository.getAllDeletedAttachments();
    try {
      final syncList = PlannerSyncModel.getPlannerSyncList(
        fieldDefDtoList: changedDynamicData,
        staticFieldValues: changedStaticData,
        deletedAttachments: deletedAttachments,
      );
      if (syncList != null && syncList.isNotEmpty) {
        await plannerSyncDataStore.pushDataToServer(changedData: syncList, turnAroundEventId: turnAroundEventId);
      }
      await syncTickerRepository.clearStatus();
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(SyncException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(SyncException.socketException());
        }
        return const Right(SyncException.unexpectedError());
      }
    } on ConnectionException catch (_) {
      return const Right(SyncException.socketException());
    }
    return const Left(unit);
  }

  @override
  Future<Either<Unit, AttachmentException>> uploadAttachments() async {
    final attachmentsToUpload = await attachmentsRepository.getAttachmentsToUpload();
    if (attachmentsToUpload != null && attachmentsToUpload.isNotEmpty) {
      final List<File> attachments = [];
      for (var element in attachmentsToUpload) {
        var file = await _createFileFromString(base64String: element.imageBytes ?? '', fileName: element.name ?? '');
        attachments.add(file);
      }
      try {
        await plannerSyncDataStore.uploadAttachments(attachments: attachments);
      } on DioError catch (error) {
        if (DioErrorType.connectTimeout == error.type) {
          return const Right(AttachmentException.connectTimeOut());
        } else if (DioErrorType.other == error.type) {
          if (error.message.contains('SocketException')) {
            return const Right(AttachmentException.socketException());
          }
          return const Right(AttachmentException.unexpectedError());
        }
      } on ConnectionException catch (_) {
        return const Right(AttachmentException.socketException());
      }
    }
    return const Left(unit);
  }

  Future<File> _createFileFromString({required String base64String, required String fileName}) async {
    Uint8List bytes = base64.decode(base64String);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File("$dir/\$\$$fileName");
    await file.writeAsBytes(bytes);
    return file;
  }

  @override
  BehaviorSubject<bool> isSyncPending() {
    // TODO: implement isSyncPending
    throw UnimplementedError();
  }
}
