import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_model.dart';

@injectable
class PlannerSyncApiService {
  final Dio dio;

  PlannerSyncApiService(this.dio);

  Future<void> pushDataToServer({required List<PlannerSyncModel> changedData, required int turnAroundEventId}) async {
    await dio.post(ApiEndPoints.pushDataToServer(eventId: turnAroundEventId), data: changedData);
  }
}
