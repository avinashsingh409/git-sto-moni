// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'planner_sync_static_field_definition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlannerSyncStaticFieldDefinition
    _$$_PlannerSyncStaticFieldDefinitionFromJson(Map<String, dynamic> json) =>
        _$_PlannerSyncStaticFieldDefinition(
          fieldTranslationKey: json['fieldTranslationKey'] as String,
          fieldTranslationValue: json['fieldTranslationValue'] as String,
        );

Map<String, dynamic> _$$_PlannerSyncStaticFieldDefinitionToJson(
        _$_PlannerSyncStaticFieldDefinition instance) =>
    <String, dynamic>{
      'fieldTranslationKey': instance.fieldTranslationKey,
      'fieldTranslationValue': instance.fieldTranslationValue,
    };
