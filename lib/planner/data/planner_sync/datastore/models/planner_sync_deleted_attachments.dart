import 'package:freezed_annotation/freezed_annotation.dart';
part 'planner_sync_deleted_attachments.freezed.dart';
part 'planner_sync_deleted_attachments.g.dart';

@freezed
abstract class PlannerSyncDeletedAttachments with _$PlannerSyncDeletedAttachments {
  const factory PlannerSyncDeletedAttachments({
    required int attachmentId,
    required DateTime dateDeleted,
  }) = _PlannerSyncDeletedAttachments;

  factory PlannerSyncDeletedAttachments.fromJson(Map<String, dynamic> json) => _$PlannerSyncDeletedAttachmentsFromJson(json);
}
