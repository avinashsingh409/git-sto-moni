import 'package:freezed_annotation/freezed_annotation.dart';
part 'planner_sync_field_definition.g.dart';
part 'planner_sync_field_definition.freezed.dart';

@freezed
abstract class PlannerSyncFieldDefinition with _$PlannerSyncFieldDefinition {
  const factory PlannerSyncFieldDefinition({
    required String? guid,
    required String? value,
  }) = _PlannerSyncFieldDefinition;

  factory PlannerSyncFieldDefinition.fromJson(Map<String, dynamic> json) => _$PlannerSyncFieldDefinitionFromJson(json);
}
