// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'planner_sync_static_field_definition.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PlannerSyncStaticFieldDefinition _$PlannerSyncStaticFieldDefinitionFromJson(
    Map<String, dynamic> json) {
  return _PlannerSyncStaticFieldDefinition.fromJson(json);
}

/// @nodoc
mixin _$PlannerSyncStaticFieldDefinition {
  String get fieldTranslationKey => throw _privateConstructorUsedError;
  String get fieldTranslationValue => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlannerSyncStaticFieldDefinitionCopyWith<PlannerSyncStaticFieldDefinition>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlannerSyncStaticFieldDefinitionCopyWith<$Res> {
  factory $PlannerSyncStaticFieldDefinitionCopyWith(
          PlannerSyncStaticFieldDefinition value,
          $Res Function(PlannerSyncStaticFieldDefinition) then) =
      _$PlannerSyncStaticFieldDefinitionCopyWithImpl<$Res,
          PlannerSyncStaticFieldDefinition>;
  @useResult
  $Res call({String fieldTranslationKey, String fieldTranslationValue});
}

/// @nodoc
class _$PlannerSyncStaticFieldDefinitionCopyWithImpl<$Res,
        $Val extends PlannerSyncStaticFieldDefinition>
    implements $PlannerSyncStaticFieldDefinitionCopyWith<$Res> {
  _$PlannerSyncStaticFieldDefinitionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fieldTranslationKey = null,
    Object? fieldTranslationValue = null,
  }) {
    return _then(_value.copyWith(
      fieldTranslationKey: null == fieldTranslationKey
          ? _value.fieldTranslationKey
          : fieldTranslationKey // ignore: cast_nullable_to_non_nullable
              as String,
      fieldTranslationValue: null == fieldTranslationValue
          ? _value.fieldTranslationValue
          : fieldTranslationValue // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlannerSyncStaticFieldDefinitionCopyWith<$Res>
    implements $PlannerSyncStaticFieldDefinitionCopyWith<$Res> {
  factory _$$_PlannerSyncStaticFieldDefinitionCopyWith(
          _$_PlannerSyncStaticFieldDefinition value,
          $Res Function(_$_PlannerSyncStaticFieldDefinition) then) =
      __$$_PlannerSyncStaticFieldDefinitionCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String fieldTranslationKey, String fieldTranslationValue});
}

/// @nodoc
class __$$_PlannerSyncStaticFieldDefinitionCopyWithImpl<$Res>
    extends _$PlannerSyncStaticFieldDefinitionCopyWithImpl<$Res,
        _$_PlannerSyncStaticFieldDefinition>
    implements _$$_PlannerSyncStaticFieldDefinitionCopyWith<$Res> {
  __$$_PlannerSyncStaticFieldDefinitionCopyWithImpl(
      _$_PlannerSyncStaticFieldDefinition _value,
      $Res Function(_$_PlannerSyncStaticFieldDefinition) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fieldTranslationKey = null,
    Object? fieldTranslationValue = null,
  }) {
    return _then(_$_PlannerSyncStaticFieldDefinition(
      fieldTranslationKey: null == fieldTranslationKey
          ? _value.fieldTranslationKey
          : fieldTranslationKey // ignore: cast_nullable_to_non_nullable
              as String,
      fieldTranslationValue: null == fieldTranslationValue
          ? _value.fieldTranslationValue
          : fieldTranslationValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PlannerSyncStaticFieldDefinition
    implements _PlannerSyncStaticFieldDefinition {
  const _$_PlannerSyncStaticFieldDefinition(
      {required this.fieldTranslationKey, required this.fieldTranslationValue});

  factory _$_PlannerSyncStaticFieldDefinition.fromJson(
          Map<String, dynamic> json) =>
      _$$_PlannerSyncStaticFieldDefinitionFromJson(json);

  @override
  final String fieldTranslationKey;
  @override
  final String fieldTranslationValue;

  @override
  String toString() {
    return 'PlannerSyncStaticFieldDefinition(fieldTranslationKey: $fieldTranslationKey, fieldTranslationValue: $fieldTranslationValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlannerSyncStaticFieldDefinition &&
            (identical(other.fieldTranslationKey, fieldTranslationKey) ||
                other.fieldTranslationKey == fieldTranslationKey) &&
            (identical(other.fieldTranslationValue, fieldTranslationValue) ||
                other.fieldTranslationValue == fieldTranslationValue));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, fieldTranslationKey, fieldTranslationValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlannerSyncStaticFieldDefinitionCopyWith<
          _$_PlannerSyncStaticFieldDefinition>
      get copyWith => __$$_PlannerSyncStaticFieldDefinitionCopyWithImpl<
          _$_PlannerSyncStaticFieldDefinition>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlannerSyncStaticFieldDefinitionToJson(
      this,
    );
  }
}

abstract class _PlannerSyncStaticFieldDefinition
    implements PlannerSyncStaticFieldDefinition {
  const factory _PlannerSyncStaticFieldDefinition(
          {required final String fieldTranslationKey,
          required final String fieldTranslationValue}) =
      _$_PlannerSyncStaticFieldDefinition;

  factory _PlannerSyncStaticFieldDefinition.fromJson(
      Map<String, dynamic> json) = _$_PlannerSyncStaticFieldDefinition.fromJson;

  @override
  String get fieldTranslationKey;
  @override
  String get fieldTranslationValue;
  @override
  @JsonKey(ignore: true)
  _$$_PlannerSyncStaticFieldDefinitionCopyWith<
          _$_PlannerSyncStaticFieldDefinition>
      get copyWith => throw _privateConstructorUsedError;
}
