// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'planner_sync_field_definition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlannerSyncFieldDefinition _$$_PlannerSyncFieldDefinitionFromJson(
        Map<String, dynamic> json) =>
    _$_PlannerSyncFieldDefinition(
      guid: json['guid'] as String?,
      value: json['value'] as String?,
    );

Map<String, dynamic> _$$_PlannerSyncFieldDefinitionToJson(
        _$_PlannerSyncFieldDefinition instance) =>
    <String, dynamic>{
      'guid': instance.guid,
      'value': instance.value,
    };
