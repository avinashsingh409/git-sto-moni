import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_deleted_attachments.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_field_definition.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_static_field_definition.dart';
part 'planner_sync_model.freezed.dart';
part 'planner_sync_model.g.dart';

@freezed
abstract class PlannerSyncModel with _$PlannerSyncModel {
  const factory PlannerSyncModel({
    required int worklistItemId,
    required List<PlannerSyncFieldDefinition>? updatedFieldDefinitions,
    required List<PlannerSyncStaticFieldDefinition>? updatedStaticField,
    required List<PlannerSyncDeletedAttachments>? deletedAttachments,
  }) = _PlannerSyncModel;

  factory PlannerSyncModel.fromJson(Map<String, dynamic> json) => _$PlannerSyncModelFromJson(json);

  static List<PlannerSyncModel>? getPlannerSyncList({
    required List<FieldDefinitionValueDTO>? fieldDefDtoList,
    required List<StaticFieldDefinitionValueDTO>? staticFieldValues,
    required List<AttachmentDTO>? deletedAttachments,
  }) {
    Set<int> workPackageIdSet = {};
    final Map<int, List<PlannerSyncFieldDefinition>?> tempDynamicMap = {};
    final Map<int, List<PlannerSyncStaticFieldDefinition>?> tempStaticMap = {};
    final Map<int, List<PlannerSyncDeletedAttachments>?> tempDeletedAttachmentMap = {};
    final Map<int, PlannerSyncModel> tempSyncModels = {};

    //Adding dynamic fields to the object
    if (fieldDefDtoList != null) {
      for (var element in fieldDefDtoList) {
        workPackageIdSet.add(element.workpackageId ?? 0);
      }
      for (var workpackageId in workPackageIdSet) {
        for (var element in fieldDefDtoList) {
          if (workpackageId == element.workpackageId) {
            List<PlannerSyncFieldDefinition>? tempData = [];
            final previousList = tempDynamicMap[element.workpackageId ?? 0];
            tempData = List.from(previousList ?? []);
            tempData.add(PlannerSyncFieldDefinition(guid: element.guid, value: element.value));
            tempDynamicMap[element.workpackageId ?? 0] = tempData;
          }
        }
      }
    }

    //Adding static fields to the object
    if (staticFieldValues != null) {
      //iterating through set of workpackage id
      for (var element in staticFieldValues) {
        workPackageIdSet.add(element.workpackageId ?? 0);
      }

      //looping throught the set of workpackageid and staticfields and putting it into map
      for (var workpackageId in workPackageIdSet) {
        for (var element in staticFieldValues) {
          if (workpackageId == element.workpackageId) {
            List<PlannerSyncStaticFieldDefinition>? tempStaticData = [];
            final previousList = tempStaticMap[element.workpackageId ?? 0];
            tempStaticData = List.from(previousList ?? []);
            tempStaticData.add(PlannerSyncStaticFieldDefinition(
              fieldTranslationKey: element.translationKey ?? '',
              fieldTranslationValue: element.value ?? '',
            ));
            tempStaticMap[element.workpackageId ?? 0] = tempStaticData;
          }
        }
      }
    }

    //Adding deleted attachments to the object
    if (deletedAttachments != null) {
      for (var element in deletedAttachments) {
        workPackageIdSet.add(element.objectId ?? 0);
      }
      for (var workpackageId in workPackageIdSet) {
        for (var element in deletedAttachments) {
          if (workpackageId == element.objectId) {
            List<PlannerSyncDeletedAttachments>? tempData = [];
            var previousList = tempDeletedAttachmentMap[element.objectId ?? 0];
            tempData = List.from(previousList ?? []);
            tempData.add(PlannerSyncDeletedAttachments(attachmentId: element.id ?? 0, dateDeleted: DateTime.now()));
            tempDeletedAttachmentMap[element.objectId ?? 0] = tempData;
          }
        }
      }
    }

    //Assigning dynamic fields data into the map
    tempDynamicMap.forEach((key, value) {
      tempSyncModels[key] = PlannerSyncModel(
        worklistItemId: key,
        updatedFieldDefinitions: value,
        updatedStaticField: [],
        deletedAttachments: [],
      );
    });

    //Assigning static fields data into the map
    tempStaticMap.forEach((key, value) {
      if (tempSyncModels.containsKey(key)) {
        final pointerVar = tempSyncModels[key];
        tempSyncModels[key] = PlannerSyncModel(
            worklistItemId: pointerVar?.worklistItemId ?? 0,
            updatedFieldDefinitions: pointerVar?.updatedFieldDefinitions ?? [],
            updatedStaticField: value,
            deletedAttachments: []);
      } else {
        tempSyncModels[key] = PlannerSyncModel(
          worklistItemId: key,
          updatedFieldDefinitions: [],
          updatedStaticField: value,
          deletedAttachments: [],
        );
      }
    });

    //Assigning deletedAttachment data into the map
    tempDeletedAttachmentMap.forEach((key, value) {
      if (tempSyncModels.containsKey(key)) {
        final pointerVar = tempSyncModels[key];
        tempSyncModels[key] = PlannerSyncModel(
          worklistItemId: pointerVar?.worklistItemId ?? 0,
          updatedFieldDefinitions: pointerVar?.updatedFieldDefinitions ?? [],
          updatedStaticField: pointerVar?.updatedStaticField ?? [],
          deletedAttachments: value,
        );
      } else {
        tempSyncModels[key] = PlannerSyncModel(
          worklistItemId: key,
          updatedFieldDefinitions: [],
          updatedStaticField: [],
          deletedAttachments: value,
        );
      }
    });

    //
    final plannerSyncList = tempSyncModels.entries.map((entry) => entry.value).toList();
    if (plannerSyncList.isNotEmpty) {
      return plannerSyncList;
    }
    return [];
  }
}
