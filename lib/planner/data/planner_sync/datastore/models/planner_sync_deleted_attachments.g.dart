// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'planner_sync_deleted_attachments.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlannerSyncDeletedAttachments _$$_PlannerSyncDeletedAttachmentsFromJson(
        Map<String, dynamic> json) =>
    _$_PlannerSyncDeletedAttachments(
      attachmentId: json['attachmentId'] as int,
      dateDeleted: DateTime.parse(json['dateDeleted'] as String),
    );

Map<String, dynamic> _$$_PlannerSyncDeletedAttachmentsToJson(
        _$_PlannerSyncDeletedAttachments instance) =>
    <String, dynamic>{
      'attachmentId': instance.attachmentId,
      'dateDeleted': instance.dateDeleted.toIso8601String(),
    };
