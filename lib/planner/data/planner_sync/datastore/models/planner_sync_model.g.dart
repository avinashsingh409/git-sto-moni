// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'planner_sync_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlannerSyncModel _$$_PlannerSyncModelFromJson(Map<String, dynamic> json) =>
    _$_PlannerSyncModel(
      worklistItemId: json['worklistItemId'] as int,
      updatedFieldDefinitions: (json['updatedFieldDefinitions']
              as List<dynamic>?)
          ?.map((e) =>
              PlannerSyncFieldDefinition.fromJson(e as Map<String, dynamic>))
          .toList(),
      updatedStaticField: (json['updatedStaticField'] as List<dynamic>?)
          ?.map((e) => PlannerSyncStaticFieldDefinition.fromJson(
              e as Map<String, dynamic>))
          .toList(),
      deletedAttachments: (json['deletedAttachments'] as List<dynamic>?)
          ?.map((e) =>
              PlannerSyncDeletedAttachments.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_PlannerSyncModelToJson(_$_PlannerSyncModel instance) =>
    <String, dynamic>{
      'worklistItemId': instance.worklistItemId,
      'updatedFieldDefinitions': instance.updatedFieldDefinitions,
      'updatedStaticField': instance.updatedStaticField,
      'deletedAttachments': instance.deletedAttachments,
    };
