// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'planner_sync_deleted_attachments.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PlannerSyncDeletedAttachments _$PlannerSyncDeletedAttachmentsFromJson(
    Map<String, dynamic> json) {
  return _PlannerSyncDeletedAttachments.fromJson(json);
}

/// @nodoc
mixin _$PlannerSyncDeletedAttachments {
  int get attachmentId => throw _privateConstructorUsedError;
  DateTime get dateDeleted => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlannerSyncDeletedAttachmentsCopyWith<PlannerSyncDeletedAttachments>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlannerSyncDeletedAttachmentsCopyWith<$Res> {
  factory $PlannerSyncDeletedAttachmentsCopyWith(
          PlannerSyncDeletedAttachments value,
          $Res Function(PlannerSyncDeletedAttachments) then) =
      _$PlannerSyncDeletedAttachmentsCopyWithImpl<$Res,
          PlannerSyncDeletedAttachments>;
  @useResult
  $Res call({int attachmentId, DateTime dateDeleted});
}

/// @nodoc
class _$PlannerSyncDeletedAttachmentsCopyWithImpl<$Res,
        $Val extends PlannerSyncDeletedAttachments>
    implements $PlannerSyncDeletedAttachmentsCopyWith<$Res> {
  _$PlannerSyncDeletedAttachmentsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attachmentId = null,
    Object? dateDeleted = null,
  }) {
    return _then(_value.copyWith(
      attachmentId: null == attachmentId
          ? _value.attachmentId
          : attachmentId // ignore: cast_nullable_to_non_nullable
              as int,
      dateDeleted: null == dateDeleted
          ? _value.dateDeleted
          : dateDeleted // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlannerSyncDeletedAttachmentsCopyWith<$Res>
    implements $PlannerSyncDeletedAttachmentsCopyWith<$Res> {
  factory _$$_PlannerSyncDeletedAttachmentsCopyWith(
          _$_PlannerSyncDeletedAttachments value,
          $Res Function(_$_PlannerSyncDeletedAttachments) then) =
      __$$_PlannerSyncDeletedAttachmentsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int attachmentId, DateTime dateDeleted});
}

/// @nodoc
class __$$_PlannerSyncDeletedAttachmentsCopyWithImpl<$Res>
    extends _$PlannerSyncDeletedAttachmentsCopyWithImpl<$Res,
        _$_PlannerSyncDeletedAttachments>
    implements _$$_PlannerSyncDeletedAttachmentsCopyWith<$Res> {
  __$$_PlannerSyncDeletedAttachmentsCopyWithImpl(
      _$_PlannerSyncDeletedAttachments _value,
      $Res Function(_$_PlannerSyncDeletedAttachments) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attachmentId = null,
    Object? dateDeleted = null,
  }) {
    return _then(_$_PlannerSyncDeletedAttachments(
      attachmentId: null == attachmentId
          ? _value.attachmentId
          : attachmentId // ignore: cast_nullable_to_non_nullable
              as int,
      dateDeleted: null == dateDeleted
          ? _value.dateDeleted
          : dateDeleted // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PlannerSyncDeletedAttachments
    implements _PlannerSyncDeletedAttachments {
  const _$_PlannerSyncDeletedAttachments(
      {required this.attachmentId, required this.dateDeleted});

  factory _$_PlannerSyncDeletedAttachments.fromJson(
          Map<String, dynamic> json) =>
      _$$_PlannerSyncDeletedAttachmentsFromJson(json);

  @override
  final int attachmentId;
  @override
  final DateTime dateDeleted;

  @override
  String toString() {
    return 'PlannerSyncDeletedAttachments(attachmentId: $attachmentId, dateDeleted: $dateDeleted)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlannerSyncDeletedAttachments &&
            (identical(other.attachmentId, attachmentId) ||
                other.attachmentId == attachmentId) &&
            (identical(other.dateDeleted, dateDeleted) ||
                other.dateDeleted == dateDeleted));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, attachmentId, dateDeleted);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlannerSyncDeletedAttachmentsCopyWith<_$_PlannerSyncDeletedAttachments>
      get copyWith => __$$_PlannerSyncDeletedAttachmentsCopyWithImpl<
          _$_PlannerSyncDeletedAttachments>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlannerSyncDeletedAttachmentsToJson(
      this,
    );
  }
}

abstract class _PlannerSyncDeletedAttachments
    implements PlannerSyncDeletedAttachments {
  const factory _PlannerSyncDeletedAttachments(
      {required final int attachmentId,
      required final DateTime dateDeleted}) = _$_PlannerSyncDeletedAttachments;

  factory _PlannerSyncDeletedAttachments.fromJson(Map<String, dynamic> json) =
      _$_PlannerSyncDeletedAttachments.fromJson;

  @override
  int get attachmentId;
  @override
  DateTime get dateDeleted;
  @override
  @JsonKey(ignore: true)
  _$$_PlannerSyncDeletedAttachmentsCopyWith<_$_PlannerSyncDeletedAttachments>
      get copyWith => throw _privateConstructorUsedError;
}
