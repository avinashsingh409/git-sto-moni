import 'package:freezed_annotation/freezed_annotation.dart';
part 'planner_sync_static_field_definition.freezed.dart';
part 'planner_sync_static_field_definition.g.dart';

@freezed
abstract class PlannerSyncStaticFieldDefinition with _$PlannerSyncStaticFieldDefinition {
  const factory PlannerSyncStaticFieldDefinition({
    required String fieldTranslationKey,
    required String fieldTranslationValue,
  }) = _PlannerSyncStaticFieldDefinition;

  factory PlannerSyncStaticFieldDefinition.fromJson(Map<String, dynamic> json) =>
      _$PlannerSyncStaticFieldDefinitionFromJson(json);
}
