// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'planner_sync_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PlannerSyncModel _$PlannerSyncModelFromJson(Map<String, dynamic> json) {
  return _PlannerSyncModel.fromJson(json);
}

/// @nodoc
mixin _$PlannerSyncModel {
  int get worklistItemId => throw _privateConstructorUsedError;
  List<PlannerSyncFieldDefinition>? get updatedFieldDefinitions =>
      throw _privateConstructorUsedError;
  List<PlannerSyncStaticFieldDefinition>? get updatedStaticField =>
      throw _privateConstructorUsedError;
  List<PlannerSyncDeletedAttachments>? get deletedAttachments =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlannerSyncModelCopyWith<PlannerSyncModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlannerSyncModelCopyWith<$Res> {
  factory $PlannerSyncModelCopyWith(
          PlannerSyncModel value, $Res Function(PlannerSyncModel) then) =
      _$PlannerSyncModelCopyWithImpl<$Res, PlannerSyncModel>;
  @useResult
  $Res call(
      {int worklistItemId,
      List<PlannerSyncFieldDefinition>? updatedFieldDefinitions,
      List<PlannerSyncStaticFieldDefinition>? updatedStaticField,
      List<PlannerSyncDeletedAttachments>? deletedAttachments});
}

/// @nodoc
class _$PlannerSyncModelCopyWithImpl<$Res, $Val extends PlannerSyncModel>
    implements $PlannerSyncModelCopyWith<$Res> {
  _$PlannerSyncModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? worklistItemId = null,
    Object? updatedFieldDefinitions = freezed,
    Object? updatedStaticField = freezed,
    Object? deletedAttachments = freezed,
  }) {
    return _then(_value.copyWith(
      worklistItemId: null == worklistItemId
          ? _value.worklistItemId
          : worklistItemId // ignore: cast_nullable_to_non_nullable
              as int,
      updatedFieldDefinitions: freezed == updatedFieldDefinitions
          ? _value.updatedFieldDefinitions
          : updatedFieldDefinitions // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncFieldDefinition>?,
      updatedStaticField: freezed == updatedStaticField
          ? _value.updatedStaticField
          : updatedStaticField // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncStaticFieldDefinition>?,
      deletedAttachments: freezed == deletedAttachments
          ? _value.deletedAttachments
          : deletedAttachments // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncDeletedAttachments>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlannerSyncModelCopyWith<$Res>
    implements $PlannerSyncModelCopyWith<$Res> {
  factory _$$_PlannerSyncModelCopyWith(
          _$_PlannerSyncModel value, $Res Function(_$_PlannerSyncModel) then) =
      __$$_PlannerSyncModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int worklistItemId,
      List<PlannerSyncFieldDefinition>? updatedFieldDefinitions,
      List<PlannerSyncStaticFieldDefinition>? updatedStaticField,
      List<PlannerSyncDeletedAttachments>? deletedAttachments});
}

/// @nodoc
class __$$_PlannerSyncModelCopyWithImpl<$Res>
    extends _$PlannerSyncModelCopyWithImpl<$Res, _$_PlannerSyncModel>
    implements _$$_PlannerSyncModelCopyWith<$Res> {
  __$$_PlannerSyncModelCopyWithImpl(
      _$_PlannerSyncModel _value, $Res Function(_$_PlannerSyncModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? worklistItemId = null,
    Object? updatedFieldDefinitions = freezed,
    Object? updatedStaticField = freezed,
    Object? deletedAttachments = freezed,
  }) {
    return _then(_$_PlannerSyncModel(
      worklistItemId: null == worklistItemId
          ? _value.worklistItemId
          : worklistItemId // ignore: cast_nullable_to_non_nullable
              as int,
      updatedFieldDefinitions: freezed == updatedFieldDefinitions
          ? _value._updatedFieldDefinitions
          : updatedFieldDefinitions // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncFieldDefinition>?,
      updatedStaticField: freezed == updatedStaticField
          ? _value._updatedStaticField
          : updatedStaticField // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncStaticFieldDefinition>?,
      deletedAttachments: freezed == deletedAttachments
          ? _value._deletedAttachments
          : deletedAttachments // ignore: cast_nullable_to_non_nullable
              as List<PlannerSyncDeletedAttachments>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PlannerSyncModel implements _PlannerSyncModel {
  const _$_PlannerSyncModel(
      {required this.worklistItemId,
      required final List<PlannerSyncFieldDefinition>? updatedFieldDefinitions,
      required final List<PlannerSyncStaticFieldDefinition>? updatedStaticField,
      required final List<PlannerSyncDeletedAttachments>? deletedAttachments})
      : _updatedFieldDefinitions = updatedFieldDefinitions,
        _updatedStaticField = updatedStaticField,
        _deletedAttachments = deletedAttachments;

  factory _$_PlannerSyncModel.fromJson(Map<String, dynamic> json) =>
      _$$_PlannerSyncModelFromJson(json);

  @override
  final int worklistItemId;
  final List<PlannerSyncFieldDefinition>? _updatedFieldDefinitions;
  @override
  List<PlannerSyncFieldDefinition>? get updatedFieldDefinitions {
    final value = _updatedFieldDefinitions;
    if (value == null) return null;
    if (_updatedFieldDefinitions is EqualUnmodifiableListView)
      return _updatedFieldDefinitions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<PlannerSyncStaticFieldDefinition>? _updatedStaticField;
  @override
  List<PlannerSyncStaticFieldDefinition>? get updatedStaticField {
    final value = _updatedStaticField;
    if (value == null) return null;
    if (_updatedStaticField is EqualUnmodifiableListView)
      return _updatedStaticField;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<PlannerSyncDeletedAttachments>? _deletedAttachments;
  @override
  List<PlannerSyncDeletedAttachments>? get deletedAttachments {
    final value = _deletedAttachments;
    if (value == null) return null;
    if (_deletedAttachments is EqualUnmodifiableListView)
      return _deletedAttachments;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PlannerSyncModel(worklistItemId: $worklistItemId, updatedFieldDefinitions: $updatedFieldDefinitions, updatedStaticField: $updatedStaticField, deletedAttachments: $deletedAttachments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlannerSyncModel &&
            (identical(other.worklistItemId, worklistItemId) ||
                other.worklistItemId == worklistItemId) &&
            const DeepCollectionEquality().equals(
                other._updatedFieldDefinitions, _updatedFieldDefinitions) &&
            const DeepCollectionEquality()
                .equals(other._updatedStaticField, _updatedStaticField) &&
            const DeepCollectionEquality()
                .equals(other._deletedAttachments, _deletedAttachments));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      worklistItemId,
      const DeepCollectionEquality().hash(_updatedFieldDefinitions),
      const DeepCollectionEquality().hash(_updatedStaticField),
      const DeepCollectionEquality().hash(_deletedAttachments));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlannerSyncModelCopyWith<_$_PlannerSyncModel> get copyWith =>
      __$$_PlannerSyncModelCopyWithImpl<_$_PlannerSyncModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlannerSyncModelToJson(
      this,
    );
  }
}

abstract class _PlannerSyncModel implements PlannerSyncModel {
  const factory _PlannerSyncModel(
      {required final int worklistItemId,
      required final List<PlannerSyncFieldDefinition>? updatedFieldDefinitions,
      required final List<PlannerSyncStaticFieldDefinition>? updatedStaticField,
      required final List<PlannerSyncDeletedAttachments>?
          deletedAttachments}) = _$_PlannerSyncModel;

  factory _PlannerSyncModel.fromJson(Map<String, dynamic> json) =
      _$_PlannerSyncModel.fromJson;

  @override
  int get worklistItemId;
  @override
  List<PlannerSyncFieldDefinition>? get updatedFieldDefinitions;
  @override
  List<PlannerSyncStaticFieldDefinition>? get updatedStaticField;
  @override
  List<PlannerSyncDeletedAttachments>? get deletedAttachments;
  @override
  @JsonKey(ignore: true)
  _$$_PlannerSyncModelCopyWith<_$_PlannerSyncModel> get copyWith =>
      throw _privateConstructorUsedError;
}
