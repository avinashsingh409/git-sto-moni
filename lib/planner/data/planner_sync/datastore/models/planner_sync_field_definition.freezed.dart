// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'planner_sync_field_definition.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PlannerSyncFieldDefinition _$PlannerSyncFieldDefinitionFromJson(
    Map<String, dynamic> json) {
  return _PlannerSyncFieldDefinition.fromJson(json);
}

/// @nodoc
mixin _$PlannerSyncFieldDefinition {
  String? get guid => throw _privateConstructorUsedError;
  String? get value => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlannerSyncFieldDefinitionCopyWith<PlannerSyncFieldDefinition>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlannerSyncFieldDefinitionCopyWith<$Res> {
  factory $PlannerSyncFieldDefinitionCopyWith(PlannerSyncFieldDefinition value,
          $Res Function(PlannerSyncFieldDefinition) then) =
      _$PlannerSyncFieldDefinitionCopyWithImpl<$Res,
          PlannerSyncFieldDefinition>;
  @useResult
  $Res call({String? guid, String? value});
}

/// @nodoc
class _$PlannerSyncFieldDefinitionCopyWithImpl<$Res,
        $Val extends PlannerSyncFieldDefinition>
    implements $PlannerSyncFieldDefinitionCopyWith<$Res> {
  _$PlannerSyncFieldDefinitionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? guid = freezed,
    Object? value = freezed,
  }) {
    return _then(_value.copyWith(
      guid: freezed == guid
          ? _value.guid
          : guid // ignore: cast_nullable_to_non_nullable
              as String?,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlannerSyncFieldDefinitionCopyWith<$Res>
    implements $PlannerSyncFieldDefinitionCopyWith<$Res> {
  factory _$$_PlannerSyncFieldDefinitionCopyWith(
          _$_PlannerSyncFieldDefinition value,
          $Res Function(_$_PlannerSyncFieldDefinition) then) =
      __$$_PlannerSyncFieldDefinitionCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? guid, String? value});
}

/// @nodoc
class __$$_PlannerSyncFieldDefinitionCopyWithImpl<$Res>
    extends _$PlannerSyncFieldDefinitionCopyWithImpl<$Res,
        _$_PlannerSyncFieldDefinition>
    implements _$$_PlannerSyncFieldDefinitionCopyWith<$Res> {
  __$$_PlannerSyncFieldDefinitionCopyWithImpl(
      _$_PlannerSyncFieldDefinition _value,
      $Res Function(_$_PlannerSyncFieldDefinition) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? guid = freezed,
    Object? value = freezed,
  }) {
    return _then(_$_PlannerSyncFieldDefinition(
      guid: freezed == guid
          ? _value.guid
          : guid // ignore: cast_nullable_to_non_nullable
              as String?,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PlannerSyncFieldDefinition implements _PlannerSyncFieldDefinition {
  const _$_PlannerSyncFieldDefinition(
      {required this.guid, required this.value});

  factory _$_PlannerSyncFieldDefinition.fromJson(Map<String, dynamic> json) =>
      _$$_PlannerSyncFieldDefinitionFromJson(json);

  @override
  final String? guid;
  @override
  final String? value;

  @override
  String toString() {
    return 'PlannerSyncFieldDefinition(guid: $guid, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlannerSyncFieldDefinition &&
            (identical(other.guid, guid) || other.guid == guid) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, guid, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlannerSyncFieldDefinitionCopyWith<_$_PlannerSyncFieldDefinition>
      get copyWith => __$$_PlannerSyncFieldDefinitionCopyWithImpl<
          _$_PlannerSyncFieldDefinition>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlannerSyncFieldDefinitionToJson(
      this,
    );
  }
}

abstract class _PlannerSyncFieldDefinition
    implements PlannerSyncFieldDefinition {
  const factory _PlannerSyncFieldDefinition(
      {required final String? guid,
      required final String? value}) = _$_PlannerSyncFieldDefinition;

  factory _PlannerSyncFieldDefinition.fromJson(Map<String, dynamic> json) =
      _$_PlannerSyncFieldDefinition.fromJson;

  @override
  String? get guid;
  @override
  String? get value;
  @override
  @JsonKey(ignore: true)
  _$$_PlannerSyncFieldDefinitionCopyWith<_$_PlannerSyncFieldDefinition>
      get copyWith => throw _privateConstructorUsedError;
}
