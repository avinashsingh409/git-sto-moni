import 'dart:io';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/attachment_sync_api_service.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/datastore/models/planner_sync_model.dart';
import 'package:sto_mobile_v2/planner/data/planner_sync/planner_sync_api_service.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/datastore/field_definitions_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/planner_sync/datastore/planner_sync_datastore.dart';

@LazySingleton(as: IPlannerSyncDataStore)
class PlannerSyncDataStoreImpl extends IPlannerSyncDataStore {
  final IFieldDefinitionDataStore fieldDefinitionDataStore;
  final PlannerSyncApiService apiService;
  final AttachmentSyncApiService attachmentSyncApiService;

  PlannerSyncDataStoreImpl(
    this.apiService,
    this.fieldDefinitionDataStore,
    this.attachmentSyncApiService,
  );

  @override
  Future<void> pushDataToServer({required List<PlannerSyncModel> changedData, required int turnAroundEventId}) {
    return apiService.pushDataToServer(changedData: changedData, turnAroundEventId: turnAroundEventId).catchDioException();
  }

  @override
  Future<List<FieldDefinitionValueDTO>?> getAllChangedFieldDefinitions() {
    return fieldDefinitionDataStore.getAllChangedFieldDefinitions();
  }

  @override
  Future<void> uploadAttachments({required List<File> attachments}) {
    return attachmentSyncApiService.uploadAttachments(attachments: attachments);
  }

  @override
  BehaviorSubject<bool> isSyncPending() {
    // TODO: implement isSyncPending
    throw UnimplementedError();
  }

  @override
  Future<List<StaticFieldDefinitionValueDTO>?> getAllChangedStaticFieldDefinitions() {
    return fieldDefinitionDataStore.getAllChangedStaticFielddefinitions();
  }
}
