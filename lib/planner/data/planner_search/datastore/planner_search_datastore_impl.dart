import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/workpackage_dao.dart';
import 'package:sto_mobile_v2/planner/domain/planner_search/datastore/planner_search_datastore.dart';

@LazySingleton(as: IPlannerSearchDataStore)
class PlannerSearchDataStoreImpl extends IPlannerSearchDataStore {
  final WorkpackageDAO workpackageDAO;
  PlannerSearchDataStoreImpl(this.workpackageDAO);

  @override
  BehaviorSubject<List<WorkpackageDTO>> getSearchedWorkPackages({required WorkpackageSearchTypeEnum field, required String keyword}) {
    return workpackageDAO.getSearchedWorkPackages(field: field, keyword: keyword);
  }
}
