import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/domain/planner_search/datastore/planner_search_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/planner_search/repository/planner_search_repository.dart';

@LazySingleton(as: IPlannerSearchRepository)
class PlannerSearchRepositoryImpl extends IPlannerSearchRepository {
  final IPlannerSearchDataStore dataStore;
  PlannerSearchRepositoryImpl(this.dataStore);

  @override
  BehaviorSubject<List<WorkpackageDTO>> getSearchedWorkPackages({required WorkpackageSearchTypeEnum field, required String keyword}) {
    return dataStore.getSearchedWorkPackages(field: field, keyword: keyword);
  }
}