import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_subgroup_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';

@injectable
class WorkpackageGroupingDAO {
  final isar = PersistenceModule.isar;

  WorkpackageGroupingDAO();

  /// Isar Collection of WorkPackageGroupings
  late IsarCollection<WorkpackageGroupingDTO> collection = isar.workpackageGroupingDTOs;

  /// Delete All WorkPackageGroupings
  Future<void> deleteAllWorkPackageGroupings() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All WorkpackageGroupings
  Future<List<WorkpackageGroupingDTO>> getAllWorkPackageGroupings() async {
    final workPackageGroupings = collection.where().findAll();
    return workPackageGroupings;
  }

  Future<List<WorkpackageFieldDefinitionDTO>> getAllFieldDefinitions({required int subGroupId}) async {
    List<WorkpackageFieldDefinitionDTO> fieldDefitions = [];
    final mainGroups = await collection.where().findAll();
    for (final element in mainGroups) {
      final subGroups = element.subgroups;
      if (subGroups != null) {
        final selectedSubGroup = subGroups.firstWhere(
          (element) => element.id == subGroupId,
          orElse: () => WorkpackageSubgroupDTO(),
        );
        final tempFieldDefinitions = selectedSubGroup.fieldDefinitionData;
        if (tempFieldDefinitions != null) {
          for (var element in tempFieldDefinitions) {
            fieldDefitions.add(element);
          }
        }
      }
    }
    return fieldDefitions;
  }

  /// Watch All WorkPackageGroupings
  BehaviorSubject<List<WorkpackageGroupingDTO>> watchAllWorkPackageGroupings() {
    final bstream = BehaviorSubject<List<WorkpackageGroupingDTO>>();
    final query = collection.where().build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  /// Upsert All WorkPackageGroupings
  Future<List<int>> upsertAllWorkPackageGroupings({
    required List<WorkpackageGroupingDTO> workPackageGroupings,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(workPackageGroupings);
    });
  }
}
