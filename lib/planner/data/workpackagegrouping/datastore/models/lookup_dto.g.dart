// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lookup_dto.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const LookupDTOSchema = Schema(
  name: r'LookupDTO',
  id: 7990361751830208664,
  properties: {
    r'multiselect': PropertySchema(
      id: 0,
      name: r'multiselect',
      type: IsarType.bool,
    ),
    r'name': PropertySchema(
      id: 1,
      name: r'name',
      type: IsarType.string,
    ),
    r'values': PropertySchema(
      id: 2,
      name: r'values',
      type: IsarType.objectList,
      target: r'LookupValueDTO',
    )
  },
  estimateSize: _lookupDTOEstimateSize,
  serialize: _lookupDTOSerialize,
  deserialize: _lookupDTODeserialize,
  deserializeProp: _lookupDTODeserializeProp,
);

int _lookupDTOEstimateSize(
  LookupDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.values;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        final offsets = allOffsets[LookupValueDTO]!;
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount +=
              LookupValueDTOSchema.estimateSize(value, offsets, allOffsets);
        }
      }
    }
  }
  return bytesCount;
}

void _lookupDTOSerialize(
  LookupDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.multiselect);
  writer.writeString(offsets[1], object.name);
  writer.writeObjectList<LookupValueDTO>(
    offsets[2],
    allOffsets,
    LookupValueDTOSchema.serialize,
    object.values,
  );
}

LookupDTO _lookupDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = LookupDTO(
    multiselect: reader.readBoolOrNull(offsets[0]),
    name: reader.readStringOrNull(offsets[1]),
    values: reader.readObjectList<LookupValueDTO>(
      offsets[2],
      LookupValueDTOSchema.deserialize,
      allOffsets,
      LookupValueDTO(),
    ),
  );
  return object;
}

P _lookupDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readObjectList<LookupValueDTO>(
        offset,
        LookupValueDTOSchema.deserialize,
        allOffsets,
        LookupValueDTO(),
      )) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension LookupDTOQueryFilter
    on QueryBuilder<LookupDTO, LookupDTO, QFilterCondition> {
  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition>
      multiselectIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'multiselect',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition>
      multiselectIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'multiselect',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> multiselectEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'multiselect',
        value: value,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'values',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'values',
      ));
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesLengthEqualTo(
      int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition>
      valuesLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition>
      valuesLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'values',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }
}

extension LookupDTOQueryObject
    on QueryBuilder<LookupDTO, LookupDTO, QFilterCondition> {
  QueryBuilder<LookupDTO, LookupDTO, QAfterFilterCondition> valuesElement(
      FilterQuery<LookupValueDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'values');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LookupDTO _$LookupDTOFromJson(Map<String, dynamic> json) => LookupDTO(
      name: json['name'] as String?,
      values: (json['values'] as List<dynamic>?)
          ?.map((e) => LookupValueDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      multiselect: json['multiselect'] as bool?,
    );

Map<String, dynamic> _$LookupDTOToJson(LookupDTO instance) => <String, dynamic>{
      'name': instance.name,
      'multiselect': instance.multiselect,
      'values': instance.values,
    };
