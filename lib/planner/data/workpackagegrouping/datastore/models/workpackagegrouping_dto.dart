import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/definition_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';

import 'workpackage_subgroup_dto.dart';
part 'workpackagegrouping_dto.g.dart';

@Collection()
@JsonSerializable()
class WorkpackageGroupingDTO {
  Id? id;
  String? name;
  int? order;
  List<WorkpackageSubgroupDTO>? subgroups;

  /// WorkpackageGrouping DTO
  WorkpackageGroupingDTO({
    this.id,
    this.name,
    this.order,
  });

  factory WorkpackageGroupingDTO.fromJson(Map<String, dynamic> json) => _$WorkpackageGroupingDTOFromJson(json);
}
