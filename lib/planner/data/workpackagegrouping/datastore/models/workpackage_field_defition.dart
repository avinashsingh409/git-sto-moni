import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/definition_dto.dart';
part 'workpackage_field_defition.g.dart';

@embedded
@JsonSerializable()
class WorkpackageFieldDefinitionDTO {
  String? guid;
  String? caption;
  bool? view;
  bool? edit;
  DefinitionDTO? definition;
  @JsonKey(includeFromJson: false, includeToJson: false)

  //Used for storing state value in FieldDefs
  String? value;

  /// WorkpackageFieldDefinition DTO
  WorkpackageFieldDefinitionDTO({
    this.guid,
    this.caption,
    this.view,
    this.edit,
    this.definition,
    this.value,
  });

  factory WorkpackageFieldDefinitionDTO.fromJson(Map<String, dynamic> json) => _$WorkpackageFieldDefinitionDTOFromJson(json);
}
