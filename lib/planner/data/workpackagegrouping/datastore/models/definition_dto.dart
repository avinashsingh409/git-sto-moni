import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_dto.dart';
part 'definition_dto.g.dart';

@embedded
@JsonSerializable()
class DefinitionDTO {
  int? order;
  bool? static;
  String? toolTip;
  String? fieldType;
  int? maxLength;
  bool? sharedValue;
  String? textBoxSize;
  String? translationKey;
  LookupDTO? lookup;
  bool? allowDecimals;

  /// Definition DTO
  DefinitionDTO(
      {this.order,
      this.static,
      this.toolTip,
      this.fieldType,
      this.maxLength,
      this.sharedValue,
      this.textBoxSize,
      this.translationKey,
      this.lookup,
      this.allowDecimals});

  factory DefinitionDTO.fromJson(Map<String, dynamic> json) => _$DefinitionDTOFromJson(json);
}
