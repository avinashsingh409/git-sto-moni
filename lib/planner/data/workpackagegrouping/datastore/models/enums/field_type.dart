//All the supported enums for field definition type
enum FieldType {
  boolean,
  text,
  date,
  number,
  lookup,
  datetime,
  user,
  //orElse condition
  none,
}
