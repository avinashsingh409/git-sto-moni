// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'workpackage_grouping_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WorkpackageGroupingResponse _$WorkpackageGroupingResponseFromJson(
    Map<String, dynamic> json) {
  return _WorkpackageGroupingResponse.fromJson(json);
}

/// @nodoc
mixin _$WorkpackageGroupingResponse {
  WorklistFieldOptions? get worklistFieldOptions =>
      throw _privateConstructorUsedError;
  List<WorkpackageGroupingDTO>? get worklistGroups =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WorkpackageGroupingResponseCopyWith<WorkpackageGroupingResponse>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkpackageGroupingResponseCopyWith<$Res> {
  factory $WorkpackageGroupingResponseCopyWith(
          WorkpackageGroupingResponse value,
          $Res Function(WorkpackageGroupingResponse) then) =
      _$WorkpackageGroupingResponseCopyWithImpl<$Res,
          WorkpackageGroupingResponse>;
  @useResult
  $Res call(
      {WorklistFieldOptions? worklistFieldOptions,
      List<WorkpackageGroupingDTO>? worklistGroups});

  $WorklistFieldOptionsCopyWith<$Res>? get worklistFieldOptions;
}

/// @nodoc
class _$WorkpackageGroupingResponseCopyWithImpl<$Res,
        $Val extends WorkpackageGroupingResponse>
    implements $WorkpackageGroupingResponseCopyWith<$Res> {
  _$WorkpackageGroupingResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? worklistFieldOptions = freezed,
    Object? worklistGroups = freezed,
  }) {
    return _then(_value.copyWith(
      worklistFieldOptions: freezed == worklistFieldOptions
          ? _value.worklistFieldOptions
          : worklistFieldOptions // ignore: cast_nullable_to_non_nullable
              as WorklistFieldOptions?,
      worklistGroups: freezed == worklistGroups
          ? _value.worklistGroups
          : worklistGroups // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageGroupingDTO>?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $WorklistFieldOptionsCopyWith<$Res>? get worklistFieldOptions {
    if (_value.worklistFieldOptions == null) {
      return null;
    }

    return $WorklistFieldOptionsCopyWith<$Res>(_value.worklistFieldOptions!,
        (value) {
      return _then(_value.copyWith(worklistFieldOptions: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_WorkpackageGroupingResponseCopyWith<$Res>
    implements $WorkpackageGroupingResponseCopyWith<$Res> {
  factory _$$_WorkpackageGroupingResponseCopyWith(
          _$_WorkpackageGroupingResponse value,
          $Res Function(_$_WorkpackageGroupingResponse) then) =
      __$$_WorkpackageGroupingResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {WorklistFieldOptions? worklistFieldOptions,
      List<WorkpackageGroupingDTO>? worklistGroups});

  @override
  $WorklistFieldOptionsCopyWith<$Res>? get worklistFieldOptions;
}

/// @nodoc
class __$$_WorkpackageGroupingResponseCopyWithImpl<$Res>
    extends _$WorkpackageGroupingResponseCopyWithImpl<$Res,
        _$_WorkpackageGroupingResponse>
    implements _$$_WorkpackageGroupingResponseCopyWith<$Res> {
  __$$_WorkpackageGroupingResponseCopyWithImpl(
      _$_WorkpackageGroupingResponse _value,
      $Res Function(_$_WorkpackageGroupingResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? worklistFieldOptions = freezed,
    Object? worklistGroups = freezed,
  }) {
    return _then(_$_WorkpackageGroupingResponse(
      worklistFieldOptions: freezed == worklistFieldOptions
          ? _value.worklistFieldOptions
          : worklistFieldOptions // ignore: cast_nullable_to_non_nullable
              as WorklistFieldOptions?,
      worklistGroups: freezed == worklistGroups
          ? _value._worklistGroups
          : worklistGroups // ignore: cast_nullable_to_non_nullable
              as List<WorkpackageGroupingDTO>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WorkpackageGroupingResponse implements _WorkpackageGroupingResponse {
  const _$_WorkpackageGroupingResponse(
      {required this.worklistFieldOptions,
      required final List<WorkpackageGroupingDTO>? worklistGroups})
      : _worklistGroups = worklistGroups;

  factory _$_WorkpackageGroupingResponse.fromJson(Map<String, dynamic> json) =>
      _$$_WorkpackageGroupingResponseFromJson(json);

  @override
  final WorklistFieldOptions? worklistFieldOptions;
  final List<WorkpackageGroupingDTO>? _worklistGroups;
  @override
  List<WorkpackageGroupingDTO>? get worklistGroups {
    final value = _worklistGroups;
    if (value == null) return null;
    if (_worklistGroups is EqualUnmodifiableListView) return _worklistGroups;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'WorkpackageGroupingResponse(worklistFieldOptions: $worklistFieldOptions, worklistGroups: $worklistGroups)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WorkpackageGroupingResponse &&
            (identical(other.worklistFieldOptions, worklistFieldOptions) ||
                other.worklistFieldOptions == worklistFieldOptions) &&
            const DeepCollectionEquality()
                .equals(other._worklistGroups, _worklistGroups));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, worklistFieldOptions,
      const DeepCollectionEquality().hash(_worklistGroups));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WorkpackageGroupingResponseCopyWith<_$_WorkpackageGroupingResponse>
      get copyWith => __$$_WorkpackageGroupingResponseCopyWithImpl<
          _$_WorkpackageGroupingResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WorkpackageGroupingResponseToJson(
      this,
    );
  }
}

abstract class _WorkpackageGroupingResponse
    implements WorkpackageGroupingResponse {
  const factory _WorkpackageGroupingResponse(
          {required final WorklistFieldOptions? worklistFieldOptions,
          required final List<WorkpackageGroupingDTO>? worklistGroups}) =
      _$_WorkpackageGroupingResponse;

  factory _WorkpackageGroupingResponse.fromJson(Map<String, dynamic> json) =
      _$_WorkpackageGroupingResponse.fromJson;

  @override
  WorklistFieldOptions? get worklistFieldOptions;
  @override
  List<WorkpackageGroupingDTO>? get worklistGroups;
  @override
  @JsonKey(ignore: true)
  _$$_WorkpackageGroupingResponseCopyWith<_$_WorkpackageGroupingResponse>
      get copyWith => throw _privateConstructorUsedError;
}
