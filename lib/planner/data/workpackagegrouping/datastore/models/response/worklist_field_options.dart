import 'package:freezed_annotation/freezed_annotation.dart';
part 'worklist_field_options.freezed.dart';
part 'worklist_field_options.g.dart';

@freezed
abstract class WorklistFieldOptions with _$WorklistFieldOptions {
  const factory WorklistFieldOptions({
    required Map<int, String?>? userOptions,
    required Map<int, String?>? workTypeOptions,
    required Map<int, String?>? contractorOptions,
  }) = _WorklistFieldOptions;

  factory WorklistFieldOptions.fromJson(Map<String, dynamic> json) => _$WorklistFieldOptionsFromJson(json);
}
