// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'worklist_field_options.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WorklistFieldOptions _$WorklistFieldOptionsFromJson(Map<String, dynamic> json) {
  return _WorklistFieldOptions.fromJson(json);
}

/// @nodoc
mixin _$WorklistFieldOptions {
  Map<int, String?>? get userOptions => throw _privateConstructorUsedError;
  Map<int, String?>? get workTypeOptions => throw _privateConstructorUsedError;
  Map<int, String?>? get contractorOptions =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WorklistFieldOptionsCopyWith<WorklistFieldOptions> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorklistFieldOptionsCopyWith<$Res> {
  factory $WorklistFieldOptionsCopyWith(WorklistFieldOptions value,
          $Res Function(WorklistFieldOptions) then) =
      _$WorklistFieldOptionsCopyWithImpl<$Res, WorklistFieldOptions>;
  @useResult
  $Res call(
      {Map<int, String?>? userOptions,
      Map<int, String?>? workTypeOptions,
      Map<int, String?>? contractorOptions});
}

/// @nodoc
class _$WorklistFieldOptionsCopyWithImpl<$Res,
        $Val extends WorklistFieldOptions>
    implements $WorklistFieldOptionsCopyWith<$Res> {
  _$WorklistFieldOptionsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userOptions = freezed,
    Object? workTypeOptions = freezed,
    Object? contractorOptions = freezed,
  }) {
    return _then(_value.copyWith(
      userOptions: freezed == userOptions
          ? _value.userOptions
          : userOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
      workTypeOptions: freezed == workTypeOptions
          ? _value.workTypeOptions
          : workTypeOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
      contractorOptions: freezed == contractorOptions
          ? _value.contractorOptions
          : contractorOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WorklistFieldOptionsCopyWith<$Res>
    implements $WorklistFieldOptionsCopyWith<$Res> {
  factory _$$_WorklistFieldOptionsCopyWith(_$_WorklistFieldOptions value,
          $Res Function(_$_WorklistFieldOptions) then) =
      __$$_WorklistFieldOptionsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Map<int, String?>? userOptions,
      Map<int, String?>? workTypeOptions,
      Map<int, String?>? contractorOptions});
}

/// @nodoc
class __$$_WorklistFieldOptionsCopyWithImpl<$Res>
    extends _$WorklistFieldOptionsCopyWithImpl<$Res, _$_WorklistFieldOptions>
    implements _$$_WorklistFieldOptionsCopyWith<$Res> {
  __$$_WorklistFieldOptionsCopyWithImpl(_$_WorklistFieldOptions _value,
      $Res Function(_$_WorklistFieldOptions) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userOptions = freezed,
    Object? workTypeOptions = freezed,
    Object? contractorOptions = freezed,
  }) {
    return _then(_$_WorklistFieldOptions(
      userOptions: freezed == userOptions
          ? _value._userOptions
          : userOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
      workTypeOptions: freezed == workTypeOptions
          ? _value._workTypeOptions
          : workTypeOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
      contractorOptions: freezed == contractorOptions
          ? _value._contractorOptions
          : contractorOptions // ignore: cast_nullable_to_non_nullable
              as Map<int, String?>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WorklistFieldOptions implements _WorklistFieldOptions {
  const _$_WorklistFieldOptions(
      {required final Map<int, String?>? userOptions,
      required final Map<int, String?>? workTypeOptions,
      required final Map<int, String?>? contractorOptions})
      : _userOptions = userOptions,
        _workTypeOptions = workTypeOptions,
        _contractorOptions = contractorOptions;

  factory _$_WorklistFieldOptions.fromJson(Map<String, dynamic> json) =>
      _$$_WorklistFieldOptionsFromJson(json);

  final Map<int, String?>? _userOptions;
  @override
  Map<int, String?>? get userOptions {
    final value = _userOptions;
    if (value == null) return null;
    if (_userOptions is EqualUnmodifiableMapView) return _userOptions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Map<int, String?>? _workTypeOptions;
  @override
  Map<int, String?>? get workTypeOptions {
    final value = _workTypeOptions;
    if (value == null) return null;
    if (_workTypeOptions is EqualUnmodifiableMapView) return _workTypeOptions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Map<int, String?>? _contractorOptions;
  @override
  Map<int, String?>? get contractorOptions {
    final value = _contractorOptions;
    if (value == null) return null;
    if (_contractorOptions is EqualUnmodifiableMapView)
      return _contractorOptions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  String toString() {
    return 'WorklistFieldOptions(userOptions: $userOptions, workTypeOptions: $workTypeOptions, contractorOptions: $contractorOptions)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WorklistFieldOptions &&
            const DeepCollectionEquality()
                .equals(other._userOptions, _userOptions) &&
            const DeepCollectionEquality()
                .equals(other._workTypeOptions, _workTypeOptions) &&
            const DeepCollectionEquality()
                .equals(other._contractorOptions, _contractorOptions));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_userOptions),
      const DeepCollectionEquality().hash(_workTypeOptions),
      const DeepCollectionEquality().hash(_contractorOptions));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WorklistFieldOptionsCopyWith<_$_WorklistFieldOptions> get copyWith =>
      __$$_WorklistFieldOptionsCopyWithImpl<_$_WorklistFieldOptions>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WorklistFieldOptionsToJson(
      this,
    );
  }
}

abstract class _WorklistFieldOptions implements WorklistFieldOptions {
  const factory _WorklistFieldOptions(
          {required final Map<int, String?>? userOptions,
          required final Map<int, String?>? workTypeOptions,
          required final Map<int, String?>? contractorOptions}) =
      _$_WorklistFieldOptions;

  factory _WorklistFieldOptions.fromJson(Map<String, dynamic> json) =
      _$_WorklistFieldOptions.fromJson;

  @override
  Map<int, String?>? get userOptions;
  @override
  Map<int, String?>? get workTypeOptions;
  @override
  Map<int, String?>? get contractorOptions;
  @override
  @JsonKey(ignore: true)
  _$$_WorklistFieldOptionsCopyWith<_$_WorklistFieldOptions> get copyWith =>
      throw _privateConstructorUsedError;
}
