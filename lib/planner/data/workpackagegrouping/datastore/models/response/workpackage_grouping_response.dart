import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/worklist_field_options.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';
part 'workpackage_grouping_response.freezed.dart';
part 'workpackage_grouping_response.g.dart';

@freezed
abstract class WorkpackageGroupingResponse with _$WorkpackageGroupingResponse {
  const factory WorkpackageGroupingResponse({
    required WorklistFieldOptions? worklistFieldOptions,
    required List<WorkpackageGroupingDTO>? worklistGroups,
  }) = _WorkpackageGroupingResponse;

  factory WorkpackageGroupingResponse.fromJson(Map<String, dynamic> json) => _$WorkpackageGroupingResponseFromJson(json);
}
