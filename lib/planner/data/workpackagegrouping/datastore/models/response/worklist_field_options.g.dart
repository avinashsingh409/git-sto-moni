// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'worklist_field_options.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WorklistFieldOptions _$$_WorklistFieldOptionsFromJson(
        Map<String, dynamic> json) =>
    _$_WorklistFieldOptions(
      userOptions: (json['userOptions'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(int.parse(k), e as String?),
      ),
      workTypeOptions: (json['workTypeOptions'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(int.parse(k), e as String?),
      ),
      contractorOptions:
          (json['contractorOptions'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(int.parse(k), e as String?),
      ),
    );

Map<String, dynamic> _$$_WorklistFieldOptionsToJson(
        _$_WorklistFieldOptions instance) =>
    <String, dynamic>{
      'userOptions':
          instance.userOptions?.map((k, e) => MapEntry(k.toString(), e)),
      'workTypeOptions':
          instance.workTypeOptions?.map((k, e) => MapEntry(k.toString(), e)),
      'contractorOptions':
          instance.contractorOptions?.map((k, e) => MapEntry(k.toString(), e)),
    };
