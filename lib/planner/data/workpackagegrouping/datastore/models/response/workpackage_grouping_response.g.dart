// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackage_grouping_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WorkpackageGroupingResponse _$$_WorkpackageGroupingResponseFromJson(
        Map<String, dynamic> json) =>
    _$_WorkpackageGroupingResponse(
      worklistFieldOptions: json['worklistFieldOptions'] == null
          ? null
          : WorklistFieldOptions.fromJson(
              json['worklistFieldOptions'] as Map<String, dynamic>),
      worklistGroups: (json['worklistGroups'] as List<dynamic>?)
          ?.map(
              (e) => WorkpackageGroupingDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_WorkpackageGroupingResponseToJson(
        _$_WorkpackageGroupingResponse instance) =>
    <String, dynamic>{
      'worklistFieldOptions': instance.worklistFieldOptions,
      'worklistGroups': instance.worklistGroups,
    };
