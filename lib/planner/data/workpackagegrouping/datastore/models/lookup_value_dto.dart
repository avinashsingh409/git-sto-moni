//Inner class of Lookup Value
import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'lookup_value_dto.g.dart';

@embedded
@JsonSerializable()
class LookupValueDTO {
  String? caption;
  double? value;

  LookupValueDTO({this.value, this.caption});

  factory LookupValueDTO.fromJson(Map<String, dynamic> json) => _$LookupValueDTOFromJson(json);
}
