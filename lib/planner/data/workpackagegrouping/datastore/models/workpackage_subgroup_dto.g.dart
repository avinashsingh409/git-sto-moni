// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackage_subgroup_dto.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const WorkpackageSubgroupDTOSchema = Schema(
  name: r'WorkpackageSubgroupDTO',
  id: 4650845141919354137,
  properties: {
    r'fieldDefinitionData': PropertySchema(
      id: 0,
      name: r'fieldDefinitionData',
      type: IsarType.objectList,
      target: r'WorkpackageFieldDefinitionDTO',
    ),
    r'id': PropertySchema(
      id: 1,
      name: r'id',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 2,
      name: r'name',
      type: IsarType.string,
    ),
    r'order': PropertySchema(
      id: 3,
      name: r'order',
      type: IsarType.long,
    ),
    r'worklistFieldGroupId': PropertySchema(
      id: 4,
      name: r'worklistFieldGroupId',
      type: IsarType.long,
    )
  },
  estimateSize: _workpackageSubgroupDTOEstimateSize,
  serialize: _workpackageSubgroupDTOSerialize,
  deserialize: _workpackageSubgroupDTODeserialize,
  deserializeProp: _workpackageSubgroupDTODeserializeProp,
);

int _workpackageSubgroupDTOEstimateSize(
  WorkpackageSubgroupDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final list = object.fieldDefinitionData;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        final offsets = allOffsets[WorkpackageFieldDefinitionDTO]!;
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += WorkpackageFieldDefinitionDTOSchema.estimateSize(
              value, offsets, allOffsets);
        }
      }
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _workpackageSubgroupDTOSerialize(
  WorkpackageSubgroupDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeObjectList<WorkpackageFieldDefinitionDTO>(
    offsets[0],
    allOffsets,
    WorkpackageFieldDefinitionDTOSchema.serialize,
    object.fieldDefinitionData,
  );
  writer.writeLong(offsets[1], object.id);
  writer.writeString(offsets[2], object.name);
  writer.writeLong(offsets[3], object.order);
  writer.writeLong(offsets[4], object.worklistFieldGroupId);
}

WorkpackageSubgroupDTO _workpackageSubgroupDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WorkpackageSubgroupDTO(
    fieldDefinitionData: reader.readObjectList<WorkpackageFieldDefinitionDTO>(
      offsets[0],
      WorkpackageFieldDefinitionDTOSchema.deserialize,
      allOffsets,
      WorkpackageFieldDefinitionDTO(),
    ),
    id: reader.readLongOrNull(offsets[1]),
    name: reader.readStringOrNull(offsets[2]),
    order: reader.readLongOrNull(offsets[3]),
    worklistFieldGroupId: reader.readLongOrNull(offsets[4]),
  );
  return object;
}

P _workpackageSubgroupDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readObjectList<WorkpackageFieldDefinitionDTO>(
        offset,
        WorkpackageFieldDefinitionDTOSchema.deserialize,
        allOffsets,
        WorkpackageFieldDefinitionDTO(),
      )) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension WorkpackageSubgroupDTOQueryFilter on QueryBuilder<
    WorkpackageSubgroupDTO, WorkpackageSubgroupDTO, QFilterCondition> {
  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'fieldDefinitionData',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'fieldDefinitionData',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> fieldDefinitionDataLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'fieldDefinitionData',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> idBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
          QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
          QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> orderBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'order',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'worklistFieldGroupId',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'worklistFieldGroupId',
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'worklistFieldGroupId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'worklistFieldGroupId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'worklistFieldGroupId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
      QAfterFilterCondition> worklistFieldGroupIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'worklistFieldGroupId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkpackageSubgroupDTOQueryObject on QueryBuilder<
    WorkpackageSubgroupDTO, WorkpackageSubgroupDTO, QFilterCondition> {
  QueryBuilder<WorkpackageSubgroupDTO, WorkpackageSubgroupDTO,
          QAfterFilterCondition>
      fieldDefinitionDataElement(FilterQuery<WorkpackageFieldDefinitionDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'fieldDefinitionData');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkpackageSubgroupDTO _$WorkpackageSubgroupDTOFromJson(
        Map<String, dynamic> json) =>
    WorkpackageSubgroupDTO(
      id: json['id'] as int?,
      name: json['name'] as String?,
      order: json['order'] as int?,
      worklistFieldGroupId: json['worklistFieldGroupId'] as int?,
      fieldDefinitionData: (json['fieldDefinitionData'] as List<dynamic>?)
          ?.map((e) =>
              WorkpackageFieldDefinitionDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WorkpackageSubgroupDTOToJson(
        WorkpackageSubgroupDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'order': instance.order,
      'worklistFieldGroupId': instance.worklistFieldGroupId,
      'fieldDefinitionData': instance.fieldDefinitionData,
    };
