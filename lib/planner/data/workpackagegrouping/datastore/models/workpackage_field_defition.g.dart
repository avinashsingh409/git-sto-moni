// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackage_field_defition.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const WorkpackageFieldDefinitionDTOSchema = Schema(
  name: r'WorkpackageFieldDefinitionDTO',
  id: -1187984719381408233,
  properties: {
    r'caption': PropertySchema(
      id: 0,
      name: r'caption',
      type: IsarType.string,
    ),
    r'definition': PropertySchema(
      id: 1,
      name: r'definition',
      type: IsarType.object,
      target: r'DefinitionDTO',
    ),
    r'edit': PropertySchema(
      id: 2,
      name: r'edit',
      type: IsarType.bool,
    ),
    r'guid': PropertySchema(
      id: 3,
      name: r'guid',
      type: IsarType.string,
    ),
    r'value': PropertySchema(
      id: 4,
      name: r'value',
      type: IsarType.string,
    ),
    r'view': PropertySchema(
      id: 5,
      name: r'view',
      type: IsarType.bool,
    )
  },
  estimateSize: _workpackageFieldDefinitionDTOEstimateSize,
  serialize: _workpackageFieldDefinitionDTOSerialize,
  deserialize: _workpackageFieldDefinitionDTODeserialize,
  deserializeProp: _workpackageFieldDefinitionDTODeserializeProp,
);

int _workpackageFieldDefinitionDTOEstimateSize(
  WorkpackageFieldDefinitionDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.caption;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.definition;
    if (value != null) {
      bytesCount += 3 +
          DefinitionDTOSchema.estimateSize(
              value, allOffsets[DefinitionDTO]!, allOffsets);
    }
  }
  {
    final value = object.guid;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.value;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _workpackageFieldDefinitionDTOSerialize(
  WorkpackageFieldDefinitionDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.caption);
  writer.writeObject<DefinitionDTO>(
    offsets[1],
    allOffsets,
    DefinitionDTOSchema.serialize,
    object.definition,
  );
  writer.writeBool(offsets[2], object.edit);
  writer.writeString(offsets[3], object.guid);
  writer.writeString(offsets[4], object.value);
  writer.writeBool(offsets[5], object.view);
}

WorkpackageFieldDefinitionDTO _workpackageFieldDefinitionDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WorkpackageFieldDefinitionDTO(
    caption: reader.readStringOrNull(offsets[0]),
    definition: reader.readObjectOrNull<DefinitionDTO>(
      offsets[1],
      DefinitionDTOSchema.deserialize,
      allOffsets,
    ),
    edit: reader.readBoolOrNull(offsets[2]),
    guid: reader.readStringOrNull(offsets[3]),
    value: reader.readStringOrNull(offsets[4]),
    view: reader.readBoolOrNull(offsets[5]),
  );
  return object;
}

P _workpackageFieldDefinitionDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readObjectOrNull<DefinitionDTO>(
        offset,
        DefinitionDTOSchema.deserialize,
        allOffsets,
      )) as P;
    case 2:
      return (reader.readBoolOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension WorkpackageFieldDefinitionDTOQueryFilter on QueryBuilder<
    WorkpackageFieldDefinitionDTO,
    WorkpackageFieldDefinitionDTO,
    QFilterCondition> {
  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'caption',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'caption',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'caption',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      captionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'caption',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      captionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'caption',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'caption',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> captionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'caption',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> definitionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'definition',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> definitionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'definition',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> editIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'edit',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> editIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'edit',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> editEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'edit',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'guid',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      guidContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      guidMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'guid',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> guidIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      valueContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
          QAfterFilterCondition>
      valueMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'value',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> valueIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> viewIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'view',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> viewIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'view',
      ));
    });
  }

  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> viewEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'view',
        value: value,
      ));
    });
  }
}

extension WorkpackageFieldDefinitionDTOQueryObject on QueryBuilder<
    WorkpackageFieldDefinitionDTO,
    WorkpackageFieldDefinitionDTO,
    QFilterCondition> {
  QueryBuilder<WorkpackageFieldDefinitionDTO, WorkpackageFieldDefinitionDTO,
      QAfterFilterCondition> definition(FilterQuery<DefinitionDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'definition');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkpackageFieldDefinitionDTO _$WorkpackageFieldDefinitionDTOFromJson(
        Map<String, dynamic> json) =>
    WorkpackageFieldDefinitionDTO(
      guid: json['guid'] as String?,
      caption: json['caption'] as String?,
      view: json['view'] as bool?,
      edit: json['edit'] as bool?,
      definition: json['definition'] == null
          ? null
          : DefinitionDTO.fromJson(json['definition'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WorkpackageFieldDefinitionDTOToJson(
        WorkpackageFieldDefinitionDTO instance) =>
    <String, dynamic>{
      'guid': instance.guid,
      'caption': instance.caption,
      'view': instance.view,
      'edit': instance.edit,
      'definition': instance.definition,
    };
