// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'definition_dto.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const DefinitionDTOSchema = Schema(
  name: r'DefinitionDTO',
  id: 4047547228183893326,
  properties: {
    r'allowDecimals': PropertySchema(
      id: 0,
      name: r'allowDecimals',
      type: IsarType.bool,
    ),
    r'fieldType': PropertySchema(
      id: 1,
      name: r'fieldType',
      type: IsarType.string,
    ),
    r'lookup': PropertySchema(
      id: 2,
      name: r'lookup',
      type: IsarType.object,
      target: r'LookupDTO',
    ),
    r'maxLength': PropertySchema(
      id: 3,
      name: r'maxLength',
      type: IsarType.long,
    ),
    r'order': PropertySchema(
      id: 4,
      name: r'order',
      type: IsarType.long,
    ),
    r'sharedValue': PropertySchema(
      id: 5,
      name: r'sharedValue',
      type: IsarType.bool,
    ),
    r'static': PropertySchema(
      id: 6,
      name: r'static',
      type: IsarType.bool,
    ),
    r'textBoxSize': PropertySchema(
      id: 7,
      name: r'textBoxSize',
      type: IsarType.string,
    ),
    r'toolTip': PropertySchema(
      id: 8,
      name: r'toolTip',
      type: IsarType.string,
    ),
    r'translationKey': PropertySchema(
      id: 9,
      name: r'translationKey',
      type: IsarType.string,
    )
  },
  estimateSize: _definitionDTOEstimateSize,
  serialize: _definitionDTOSerialize,
  deserialize: _definitionDTODeserialize,
  deserializeProp: _definitionDTODeserializeProp,
);

int _definitionDTOEstimateSize(
  DefinitionDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.fieldType;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.lookup;
    if (value != null) {
      bytesCount += 3 +
          LookupDTOSchema.estimateSize(
              value, allOffsets[LookupDTO]!, allOffsets);
    }
  }
  {
    final value = object.textBoxSize;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.toolTip;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.translationKey;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _definitionDTOSerialize(
  DefinitionDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.allowDecimals);
  writer.writeString(offsets[1], object.fieldType);
  writer.writeObject<LookupDTO>(
    offsets[2],
    allOffsets,
    LookupDTOSchema.serialize,
    object.lookup,
  );
  writer.writeLong(offsets[3], object.maxLength);
  writer.writeLong(offsets[4], object.order);
  writer.writeBool(offsets[5], object.sharedValue);
  writer.writeBool(offsets[6], object.static);
  writer.writeString(offsets[7], object.textBoxSize);
  writer.writeString(offsets[8], object.toolTip);
  writer.writeString(offsets[9], object.translationKey);
}

DefinitionDTO _definitionDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = DefinitionDTO(
    allowDecimals: reader.readBoolOrNull(offsets[0]),
    fieldType: reader.readStringOrNull(offsets[1]),
    lookup: reader.readObjectOrNull<LookupDTO>(
      offsets[2],
      LookupDTOSchema.deserialize,
      allOffsets,
    ),
    maxLength: reader.readLongOrNull(offsets[3]),
    order: reader.readLongOrNull(offsets[4]),
    sharedValue: reader.readBoolOrNull(offsets[5]),
    static: reader.readBoolOrNull(offsets[6]),
    textBoxSize: reader.readStringOrNull(offsets[7]),
    toolTip: reader.readStringOrNull(offsets[8]),
    translationKey: reader.readStringOrNull(offsets[9]),
  );
  return object;
}

P _definitionDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readObjectOrNull<LookupDTO>(
        offset,
        LookupDTOSchema.deserialize,
        allOffsets,
      )) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readBoolOrNull(offset)) as P;
    case 6:
      return (reader.readBoolOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readStringOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension DefinitionDTOQueryFilter
    on QueryBuilder<DefinitionDTO, DefinitionDTO, QFilterCondition> {
  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      allowDecimalsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'allowDecimals',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      allowDecimalsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'allowDecimals',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      allowDecimalsEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'allowDecimals',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'fieldType',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'fieldType',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'fieldType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'fieldType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'fieldType',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'fieldType',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      fieldTypeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'fieldType',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      lookupIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lookup',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      lookupIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lookup',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'maxLength',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'maxLength',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'maxLength',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'maxLength',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'maxLength',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      maxLengthBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'maxLength',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      orderBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'order',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      sharedValueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sharedValue',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      sharedValueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sharedValue',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      sharedValueEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sharedValue',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      staticIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'static',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      staticIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'static',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      staticEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'static',
        value: value,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'textBoxSize',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'textBoxSize',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'textBoxSize',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'textBoxSize',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'textBoxSize',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'textBoxSize',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      textBoxSizeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'textBoxSize',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'toolTip',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'toolTip',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'toolTip',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'toolTip',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'toolTip',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'toolTip',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      toolTipIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'toolTip',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'translationKey',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'translationKey',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'translationKey',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'translationKey',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'translationKey',
        value: '',
      ));
    });
  }

  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition>
      translationKeyIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'translationKey',
        value: '',
      ));
    });
  }
}

extension DefinitionDTOQueryObject
    on QueryBuilder<DefinitionDTO, DefinitionDTO, QFilterCondition> {
  QueryBuilder<DefinitionDTO, DefinitionDTO, QAfterFilterCondition> lookup(
      FilterQuery<LookupDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'lookup');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DefinitionDTO _$DefinitionDTOFromJson(Map<String, dynamic> json) =>
    DefinitionDTO(
      order: json['order'] as int?,
      static: json['static'] as bool?,
      toolTip: json['toolTip'] as String?,
      fieldType: json['fieldType'] as String?,
      maxLength: json['maxLength'] as int?,
      sharedValue: json['sharedValue'] as bool?,
      textBoxSize: json['textBoxSize'] as String?,
      translationKey: json['translationKey'] as String?,
      lookup: json['lookup'] == null
          ? null
          : LookupDTO.fromJson(json['lookup'] as Map<String, dynamic>),
      allowDecimals: json['allowDecimals'] as bool?,
    );

Map<String, dynamic> _$DefinitionDTOToJson(DefinitionDTO instance) =>
    <String, dynamic>{
      'order': instance.order,
      'static': instance.static,
      'toolTip': instance.toolTip,
      'fieldType': instance.fieldType,
      'maxLength': instance.maxLength,
      'sharedValue': instance.sharedValue,
      'textBoxSize': instance.textBoxSize,
      'translationKey': instance.translationKey,
      'lookup': instance.lookup,
      'allowDecimals': instance.allowDecimals,
    };
