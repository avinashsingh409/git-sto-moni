import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/lookup_value_dto.dart';
part 'lookup_dto.g.dart';

@embedded
@JsonSerializable()
class LookupDTO {
  String? name;
  bool? multiselect;
  List<LookupValueDTO>? values;

  /// Lookup DTO
  LookupDTO({
    this.name,
    this.values,
    this.multiselect,
  });

  factory LookupDTO.fromJson(Map<String, dynamic> json) => _$LookupDTOFromJson(json);
}
