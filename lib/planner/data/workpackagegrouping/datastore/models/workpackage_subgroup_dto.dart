import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
part 'workpackage_subgroup_dto.g.dart';

@embedded
@JsonSerializable()
class WorkpackageSubgroupDTO {
  int? id;
  String? name;
  int? order;
  int? worklistFieldGroupId;
  List<WorkpackageFieldDefinitionDTO>? fieldDefinitionData;

  /// WorkpackageSubgroup DTO
  WorkpackageSubgroupDTO({
    this.id,
    this.name,
    this.order,
    this.worklistFieldGroupId,
    this.fieldDefinitionData,
  });

  factory WorkpackageSubgroupDTO.fromJson(Map<String, dynamic> json) => _$WorkpackageSubgroupDTOFromJson(json);
}
