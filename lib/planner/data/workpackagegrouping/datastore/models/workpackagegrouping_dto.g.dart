// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackagegrouping_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetWorkpackageGroupingDTOCollection on Isar {
  IsarCollection<WorkpackageGroupingDTO> get workpackageGroupingDTOs =>
      this.collection();
}

const WorkpackageGroupingDTOSchema = CollectionSchema(
  name: r'WorkpackageGroupingDTO',
  id: 1115999246895621913,
  properties: {
    r'name': PropertySchema(
      id: 0,
      name: r'name',
      type: IsarType.string,
    ),
    r'order': PropertySchema(
      id: 1,
      name: r'order',
      type: IsarType.long,
    ),
    r'subgroups': PropertySchema(
      id: 2,
      name: r'subgroups',
      type: IsarType.objectList,
      target: r'WorkpackageSubgroupDTO',
    )
  },
  estimateSize: _workpackageGroupingDTOEstimateSize,
  serialize: _workpackageGroupingDTOSerialize,
  deserialize: _workpackageGroupingDTODeserialize,
  deserializeProp: _workpackageGroupingDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {
    r'WorkpackageSubgroupDTO': WorkpackageSubgroupDTOSchema,
    r'WorkpackageFieldDefinitionDTO': WorkpackageFieldDefinitionDTOSchema,
    r'DefinitionDTO': DefinitionDTOSchema,
    r'LookupDTO': LookupDTOSchema,
    r'LookupValueDTO': LookupValueDTOSchema
  },
  getId: _workpackageGroupingDTOGetId,
  getLinks: _workpackageGroupingDTOGetLinks,
  attach: _workpackageGroupingDTOAttach,
  version: '3.1.0+1',
);

int _workpackageGroupingDTOEstimateSize(
  WorkpackageGroupingDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.subgroups;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        final offsets = allOffsets[WorkpackageSubgroupDTO]!;
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += WorkpackageSubgroupDTOSchema.estimateSize(
              value, offsets, allOffsets);
        }
      }
    }
  }
  return bytesCount;
}

void _workpackageGroupingDTOSerialize(
  WorkpackageGroupingDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.name);
  writer.writeLong(offsets[1], object.order);
  writer.writeObjectList<WorkpackageSubgroupDTO>(
    offsets[2],
    allOffsets,
    WorkpackageSubgroupDTOSchema.serialize,
    object.subgroups,
  );
}

WorkpackageGroupingDTO _workpackageGroupingDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WorkpackageGroupingDTO(
    id: id,
    name: reader.readStringOrNull(offsets[0]),
    order: reader.readLongOrNull(offsets[1]),
  );
  object.subgroups = reader.readObjectList<WorkpackageSubgroupDTO>(
    offsets[2],
    WorkpackageSubgroupDTOSchema.deserialize,
    allOffsets,
    WorkpackageSubgroupDTO(),
  );
  return object;
}

P _workpackageGroupingDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    case 2:
      return (reader.readObjectList<WorkpackageSubgroupDTO>(
        offset,
        WorkpackageSubgroupDTOSchema.deserialize,
        allOffsets,
        WorkpackageSubgroupDTO(),
      )) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _workpackageGroupingDTOGetId(WorkpackageGroupingDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _workpackageGroupingDTOGetLinks(
    WorkpackageGroupingDTO object) {
  return [];
}

void _workpackageGroupingDTOAttach(
    IsarCollection<dynamic> col, Id id, WorkpackageGroupingDTO object) {
  object.id = id;
}

extension WorkpackageGroupingDTOQueryWhereSort
    on QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QWhere> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension WorkpackageGroupingDTOQueryWhere on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QWhereClause> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkpackageGroupingDTOQueryFilter on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QFilterCondition> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
          QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
          QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'order',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'order',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> orderBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'order',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'subgroups',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'subgroups',
      ));
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
      QAfterFilterCondition> subgroupsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'subgroups',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }
}

extension WorkpackageGroupingDTOQueryObject on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QFilterCondition> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO,
          QAfterFilterCondition>
      subgroupsElement(FilterQuery<WorkpackageSubgroupDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'subgroups');
    });
  }
}

extension WorkpackageGroupingDTOQueryLinks on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QFilterCondition> {}

extension WorkpackageGroupingDTOQuerySortBy
    on QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QSortBy> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      sortByOrder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'order', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      sortByOrderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'order', Sort.desc);
    });
  }
}

extension WorkpackageGroupingDTOQuerySortThenBy on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QSortThenBy> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenByOrder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'order', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QAfterSortBy>
      thenByOrderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'order', Sort.desc);
    });
  }
}

extension WorkpackageGroupingDTOQueryWhereDistinct
    on QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QDistinct> {
  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QDistinct>
      distinctByName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, WorkpackageGroupingDTO, QDistinct>
      distinctByOrder() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'order');
    });
  }
}

extension WorkpackageGroupingDTOQueryProperty on QueryBuilder<
    WorkpackageGroupingDTO, WorkpackageGroupingDTO, QQueryProperty> {
  QueryBuilder<WorkpackageGroupingDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, String?, QQueryOperations>
      nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, int?, QQueryOperations> orderProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'order');
    });
  }

  QueryBuilder<WorkpackageGroupingDTO, List<WorkpackageSubgroupDTO>?,
      QQueryOperations> subgroupsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'subgroups');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkpackageGroupingDTO _$WorkpackageGroupingDTOFromJson(
        Map<String, dynamic> json) =>
    WorkpackageGroupingDTO(
      id: json['id'] as int?,
      name: json['name'] as String?,
      order: json['order'] as int?,
    )..subgroups = (json['subgroups'] as List<dynamic>?)
        ?.map((e) => WorkpackageSubgroupDTO.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$WorkpackageGroupingDTOToJson(
        WorkpackageGroupingDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'order': instance.order,
      'subgroups': instance.subgroups,
    };
