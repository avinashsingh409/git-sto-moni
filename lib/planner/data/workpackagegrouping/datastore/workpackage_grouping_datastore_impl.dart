import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/workpackage_grouping_response.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/workpackage_grouping_api_service.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/workpackage_grouping_dao.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/datastore/workpackage_grouping_datastore.dart';

@LazySingleton(as: IWorkpackageGroupingDataStore)
class WorkpackageGroupingDataStoreImpl extends IWorkpackageGroupingDataStore {
  final WorkpackageGroupingApiService workpackageGroupingApiService;
  final WorkpackageGroupingDAO workpackageGroupingDao;
  WorkpackageGroupingDataStoreImpl(this.workpackageGroupingApiService, this.workpackageGroupingDao);

  @override
  Future<WorkpackageGroupingResponse?> getAllGroupings({required int turnaroundEventId}) {
    return workpackageGroupingApiService.getAllWorkpackageGroups(turnaroundEventId: turnaroundEventId).catchDioException();
  }

  @override
  Future<void> insertAllWorkpackageGroupings({required List<WorkpackageGroupingDTO> workpackageGroupingDTOs}) {
    return workpackageGroupingDao.upsertAllWorkPackageGroupings(workPackageGroupings: workpackageGroupingDTOs);
  }

  @override
  Future<void> deleteAllWorkpackageGroupings() {
    return workpackageGroupingDao.deleteAllWorkPackageGroupings();
  }

  @override
  Future<List<WorkpackageGroupingDTO>> getAllWorkPackageGroupings() {
    return workpackageGroupingDao.getAllWorkPackageGroupings();
  }

  @override
  Future<List<WorkpackageFieldDefinitionDTO>> getAllFieldDefinitionsBySubgroupId({required int subGroupId}) {
    return workpackageGroupingDao.getAllFieldDefinitions(subGroupId: subGroupId);
  }
}
