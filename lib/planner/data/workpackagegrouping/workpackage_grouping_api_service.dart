import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/workpackage_grouping_response.dart';

@injectable
class WorkpackageGroupingApiService {
  final Dio dio;

  WorkpackageGroupingApiService(this.dio);

  Future<WorkpackageGroupingResponse?> getAllWorkpackageGroups({required int turnaroundEventId}) async {
    final response = await dio.get(ApiEndPoints.getWorkpackagesGroupings(eventId: turnaroundEventId));
    final parsedResponse = WorkpackageGroupingResponse.fromJson(response.data);
    return parsedResponse;
  }
}
