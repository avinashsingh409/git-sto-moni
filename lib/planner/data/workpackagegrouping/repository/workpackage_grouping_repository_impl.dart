import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/workpackage_grouping_response.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/datastore/workpackage_grouping_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/repository/workpackage_grouping_repository.dart';

@LazySingleton(as: IWorkpackageGroupingRepository)
class WorkpackageGroupingRepositoryImpl extends IWorkpackageGroupingRepository {
  final IWorkpackageGroupingDataStore workpackageGroupingDataStore;
  WorkpackageGroupingRepositoryImpl(this.workpackageGroupingDataStore);

  @override
  Future<WorkpackageGroupingResponse?> getAllGroupings({required int turnaroundEventId}) {
    return workpackageGroupingDataStore.getAllGroupings(turnaroundEventId: turnaroundEventId);
  }

  @override
  Future<void> insertAllWorkpackageGroupings({required List<WorkpackageGroupingDTO> workpackageGroupingDTOs}) {
    return workpackageGroupingDataStore.insertAllWorkpackageGroupings(workpackageGroupingDTOs: workpackageGroupingDTOs);
  }

  @override
  Future<void> deleteAllWorkpackageGroupings() {
    return workpackageGroupingDataStore.deleteAllWorkpackageGroupings();
  }

  @override
  Future<List<WorkpackageGroupingDTO>> getAllWorkPackageGroupings() {
    return workpackageGroupingDataStore.getAllWorkPackageGroupings();
  }

  @override
  Future<List<WorkpackageFieldDefinitionDTO>> getAllFieldDefinitionsBySubGroupId({required int subGroupId}) {
    return workpackageGroupingDataStore.getAllFieldDefinitionsBySubgroupId(subGroupId: subGroupId);
  }
}
