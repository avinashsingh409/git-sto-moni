enum WorkpackageSearchTypeEnum {
  number,
  description,
  plannerName,
  functionalLocation,
  equipmentNumber,
  equipmentType,
  equipmentSubtype,
  status,
  equipmentTagNumber,
}
