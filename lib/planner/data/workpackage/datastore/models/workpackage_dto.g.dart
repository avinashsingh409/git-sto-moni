// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackage_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetWorkpackageDTOCollection on Isar {
  IsarCollection<WorkpackageDTO> get workpackageDTOs => this.collection();
}

const WorkpackageDTOSchema = CollectionSchema(
  name: r'WorkpackageDTO',
  id: 1611461986565223101,
  properties: {
    r'description': PropertySchema(
      id: 0,
      name: r'description',
      type: IsarType.string,
    ),
    r'dynamicColumns': PropertySchema(
      id: 1,
      name: r'dynamicColumns',
      type: IsarType.string,
    ),
    r'equipmentNumber': PropertySchema(
      id: 2,
      name: r'equipmentNumber',
      type: IsarType.string,
    ),
    r'equipmentTagNumber': PropertySchema(
      id: 3,
      name: r'equipmentTagNumber',
      type: IsarType.string,
    ),
    r'equipmentType': PropertySchema(
      id: 4,
      name: r'equipmentType',
      type: IsarType.object,
      target: r'EquipmentTypeDTO',
    ),
    r'equipmentTypeId': PropertySchema(
      id: 5,
      name: r'equipmentTypeId',
      type: IsarType.long,
    ),
    r'expenseType': PropertySchema(
      id: 6,
      name: r'expenseType',
      type: IsarType.string,
    ),
    r'functionalLocation': PropertySchema(
      id: 7,
      name: r'functionalLocation',
      type: IsarType.string,
    ),
    r'isChanged': PropertySchema(
      id: 8,
      name: r'isChanged',
      type: IsarType.bool,
    ),
    r'isLocked': PropertySchema(
      id: 9,
      name: r'isLocked',
      type: IsarType.bool,
    ),
    r'planner': PropertySchema(
      id: 10,
      name: r'planner',
      type: IsarType.long,
    ),
    r'plannerName': PropertySchema(
      id: 11,
      name: r'plannerName',
      type: IsarType.string,
    ),
    r'primaryContractorId': PropertySchema(
      id: 12,
      name: r'primaryContractorId',
      type: IsarType.long,
    ),
    r'processSystems': PropertySchema(
      id: 13,
      name: r'processSystems',
      type: IsarType.string,
    ),
    r'sapWorkOrder': PropertySchema(
      id: 14,
      name: r'sapWorkOrder',
      type: IsarType.string,
    ),
    r'scopeApprovalCode': PropertySchema(
      id: 15,
      name: r'scopeApprovalCode',
      type: IsarType.string,
    ),
    r'scopeIdentifier': PropertySchema(
      id: 16,
      name: r'scopeIdentifier',
      type: IsarType.string,
    ),
    r'scopeRequestNumber': PropertySchema(
      id: 17,
      name: r'scopeRequestNumber',
      type: IsarType.string,
    ),
    r'subEquipmentTypeDescription': PropertySchema(
      id: 18,
      name: r'subEquipmentTypeDescription',
      type: IsarType.string,
    ),
    r'unitNumber': PropertySchema(
      id: 19,
      name: r'unitNumber',
      type: IsarType.string,
    ),
    r'workPackageStatus': PropertySchema(
      id: 20,
      name: r'workPackageStatus',
      type: IsarType.string,
    ),
    r'workType': PropertySchema(
      id: 21,
      name: r'workType',
      type: IsarType.object,
      target: r'WorkTypeDTO',
    ),
    r'workTypeId': PropertySchema(
      id: 22,
      name: r'workTypeId',
      type: IsarType.long,
    )
  },
  estimateSize: _workpackageDTOEstimateSize,
  serialize: _workpackageDTOSerialize,
  deserialize: _workpackageDTODeserialize,
  deserializeProp: _workpackageDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {
    r'WorkTypeDTO': WorkTypeDTOSchema,
    r'EquipmentTypeDTO': EquipmentTypeDTOSchema
  },
  getId: _workpackageDTOGetId,
  getLinks: _workpackageDTOGetLinks,
  attach: _workpackageDTOAttach,
  version: '3.1.0+1',
);

int _workpackageDTOEstimateSize(
  WorkpackageDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.dynamicColumns;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.equipmentNumber;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.equipmentTagNumber;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.equipmentType;
    if (value != null) {
      bytesCount += 3 +
          EquipmentTypeDTOSchema.estimateSize(
              value, allOffsets[EquipmentTypeDTO]!, allOffsets);
    }
  }
  {
    final value = object.expenseType;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.functionalLocation;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.plannerName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.processSystems;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.sapWorkOrder;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.scopeApprovalCode;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.scopeIdentifier;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.scopeRequestNumber;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.subEquipmentTypeDescription;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.unitNumber;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.workPackageStatus;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.workType;
    if (value != null) {
      bytesCount += 3 +
          WorkTypeDTOSchema.estimateSize(
              value, allOffsets[WorkTypeDTO]!, allOffsets);
    }
  }
  return bytesCount;
}

void _workpackageDTOSerialize(
  WorkpackageDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.description);
  writer.writeString(offsets[1], object.dynamicColumns);
  writer.writeString(offsets[2], object.equipmentNumber);
  writer.writeString(offsets[3], object.equipmentTagNumber);
  writer.writeObject<EquipmentTypeDTO>(
    offsets[4],
    allOffsets,
    EquipmentTypeDTOSchema.serialize,
    object.equipmentType,
  );
  writer.writeLong(offsets[5], object.equipmentTypeId);
  writer.writeString(offsets[6], object.expenseType);
  writer.writeString(offsets[7], object.functionalLocation);
  writer.writeBool(offsets[8], object.isChanged);
  writer.writeBool(offsets[9], object.isLocked);
  writer.writeLong(offsets[10], object.planner);
  writer.writeString(offsets[11], object.plannerName);
  writer.writeLong(offsets[12], object.primaryContractorId);
  writer.writeString(offsets[13], object.processSystems);
  writer.writeString(offsets[14], object.sapWorkOrder);
  writer.writeString(offsets[15], object.scopeApprovalCode);
  writer.writeString(offsets[16], object.scopeIdentifier);
  writer.writeString(offsets[17], object.scopeRequestNumber);
  writer.writeString(offsets[18], object.subEquipmentTypeDescription);
  writer.writeString(offsets[19], object.unitNumber);
  writer.writeString(offsets[20], object.workPackageStatus);
  writer.writeObject<WorkTypeDTO>(
    offsets[21],
    allOffsets,
    WorkTypeDTOSchema.serialize,
    object.workType,
  );
  writer.writeLong(offsets[22], object.workTypeId);
}

WorkpackageDTO _workpackageDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WorkpackageDTO(
    description: reader.readStringOrNull(offsets[0]),
    dynamicColumns: reader.readStringOrNull(offsets[1]),
    equipmentNumber: reader.readStringOrNull(offsets[2]),
    equipmentTagNumber: reader.readStringOrNull(offsets[3]),
    equipmentType: reader.readObjectOrNull<EquipmentTypeDTO>(
      offsets[4],
      EquipmentTypeDTOSchema.deserialize,
      allOffsets,
    ),
    equipmentTypeId: reader.readLongOrNull(offsets[5]),
    expenseType: reader.readStringOrNull(offsets[6]),
    functionalLocation: reader.readStringOrNull(offsets[7]),
    id: id,
    isChanged: reader.readBoolOrNull(offsets[8]),
    isLocked: reader.readBoolOrNull(offsets[9]),
    planner: reader.readLongOrNull(offsets[10]),
    plannerName: reader.readStringOrNull(offsets[11]),
    primaryContractorId: reader.readLongOrNull(offsets[12]),
    processSystems: reader.readStringOrNull(offsets[13]),
    sapWorkOrder: reader.readStringOrNull(offsets[14]),
    scopeApprovalCode: reader.readStringOrNull(offsets[15]),
    scopeIdentifier: reader.readStringOrNull(offsets[16]),
    scopeRequestNumber: reader.readStringOrNull(offsets[17]),
    subEquipmentTypeDescription: reader.readStringOrNull(offsets[18]),
    unitNumber: reader.readStringOrNull(offsets[19]),
    workPackageStatus: reader.readStringOrNull(offsets[20]),
    workType: reader.readObjectOrNull<WorkTypeDTO>(
      offsets[21],
      WorkTypeDTOSchema.deserialize,
      allOffsets,
    ),
    workTypeId: reader.readLongOrNull(offsets[22]),
  );
  return object;
}

P _workpackageDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readObjectOrNull<EquipmentTypeDTO>(
        offset,
        EquipmentTypeDTOSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readBoolOrNull(offset)) as P;
    case 9:
      return (reader.readBoolOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    case 11:
      return (reader.readStringOrNull(offset)) as P;
    case 12:
      return (reader.readLongOrNull(offset)) as P;
    case 13:
      return (reader.readStringOrNull(offset)) as P;
    case 14:
      return (reader.readStringOrNull(offset)) as P;
    case 15:
      return (reader.readStringOrNull(offset)) as P;
    case 16:
      return (reader.readStringOrNull(offset)) as P;
    case 17:
      return (reader.readStringOrNull(offset)) as P;
    case 18:
      return (reader.readStringOrNull(offset)) as P;
    case 19:
      return (reader.readStringOrNull(offset)) as P;
    case 20:
      return (reader.readStringOrNull(offset)) as P;
    case 21:
      return (reader.readObjectOrNull<WorkTypeDTO>(
        offset,
        WorkTypeDTOSchema.deserialize,
        allOffsets,
      )) as P;
    case 22:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _workpackageDTOGetId(WorkpackageDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _workpackageDTOGetLinks(WorkpackageDTO object) {
  return [];
}

void _workpackageDTOAttach(
    IsarCollection<dynamic> col, Id id, WorkpackageDTO object) {
  object.id = id;
}

extension WorkpackageDTOQueryWhereSort
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QWhere> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension WorkpackageDTOQueryWhere
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QWhereClause> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkpackageDTOQueryFilter
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QFilterCondition> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dynamicColumns',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dynamicColumns',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dynamicColumns',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'dynamicColumns',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'dynamicColumns',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicColumns',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      dynamicColumnsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'dynamicColumns',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'equipmentNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'equipmentNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'equipmentNumber',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'equipmentNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'equipmentNumber',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'equipmentNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentNumberIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'equipmentNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'equipmentTagNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'equipmentTagNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'equipmentTagNumber',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'equipmentTagNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'equipmentTagNumber',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'equipmentTagNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTagNumberIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'equipmentTagNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'equipmentType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'equipmentType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'equipmentTypeId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'equipmentTypeId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'equipmentTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'equipmentTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'equipmentTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentTypeIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'equipmentTypeId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'expenseType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'expenseType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'expenseType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'expenseType',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'expenseType',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'expenseType',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      expenseTypeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'expenseType',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'functionalLocation',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'functionalLocation',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'functionalLocation',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'functionalLocation',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'functionalLocation',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'functionalLocation',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      functionalLocationIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'functionalLocation',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isChangedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isChanged',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isChangedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isChanged',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isChangedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isChanged',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isLockedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isLocked',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isLockedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isLocked',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      isLockedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isLocked',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'planner',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'planner',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'planner',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'planner',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'planner',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'planner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'plannerName',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'plannerName',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'plannerName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'plannerName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'plannerName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'plannerName',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      plannerNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'plannerName',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'primaryContractorId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'primaryContractorId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'primaryContractorId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'primaryContractorId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'primaryContractorId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      primaryContractorIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'primaryContractorId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'processSystems',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'processSystems',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'processSystems',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'processSystems',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'processSystems',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'processSystems',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      processSystemsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'processSystems',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sapWorkOrder',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sapWorkOrder',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'sapWorkOrder',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'sapWorkOrder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'sapWorkOrder',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sapWorkOrder',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      sapWorkOrderIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'sapWorkOrder',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'scopeApprovalCode',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'scopeApprovalCode',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'scopeApprovalCode',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'scopeApprovalCode',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'scopeApprovalCode',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeApprovalCode',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeApprovalCodeIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'scopeApprovalCode',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'scopeIdentifier',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'scopeIdentifier',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'scopeIdentifier',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'scopeIdentifier',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'scopeIdentifier',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeIdentifier',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeIdentifierIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'scopeIdentifier',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'scopeRequestNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'scopeRequestNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'scopeRequestNumber',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'scopeRequestNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'scopeRequestNumber',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'scopeRequestNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      scopeRequestNumberIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'scopeRequestNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'subEquipmentTypeDescription',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'subEquipmentTypeDescription',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'subEquipmentTypeDescription',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'subEquipmentTypeDescription',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'subEquipmentTypeDescription',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'subEquipmentTypeDescription',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      subEquipmentTypeDescriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'subEquipmentTypeDescription',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unitNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unitNumber',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unitNumber',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'unitNumber',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'unitNumber',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unitNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      unitNumberIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'unitNumber',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workPackageStatus',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workPackageStatus',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'workPackageStatus',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'workPackageStatus',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'workPackageStatus',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workPackageStatus',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workPackageStatusIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'workPackageStatus',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workType',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workTypeId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workTypeId',
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'workTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'workTypeId',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      workTypeIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'workTypeId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkpackageDTOQueryObject
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QFilterCondition> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition>
      equipmentType(FilterQuery<EquipmentTypeDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'equipmentType');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterFilterCondition> workType(
      FilterQuery<WorkTypeDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'workType');
    });
  }
}

extension WorkpackageDTOQueryLinks
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QFilterCondition> {}

extension WorkpackageDTOQuerySortBy
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QSortBy> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByDynamicColumns() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicColumns', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByDynamicColumnsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicColumns', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentTagNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTagNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentTagNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTagNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTypeId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByEquipmentTypeIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTypeId', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByExpenseType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expenseType', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByExpenseTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expenseType', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByFunctionalLocation() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'functionalLocation', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByFunctionalLocationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'functionalLocation', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> sortByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByIsChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> sortByIsLocked() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLocked', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByIsLockedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLocked', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> sortByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByPlannerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByPlannerName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannerName', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByPlannerNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannerName', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByPrimaryContractorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaryContractorId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByPrimaryContractorIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaryContractorId', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByProcessSystems() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'processSystems', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByProcessSystemsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'processSystems', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortBySapWorkOrder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sapWorkOrder', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortBySapWorkOrderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sapWorkOrder', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeApprovalCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeApprovalCode', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeApprovalCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeApprovalCode', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeIdentifier() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeIdentifier', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeIdentifierDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeIdentifier', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeRequestNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeRequestNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByScopeRequestNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeRequestNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortBySubEquipmentTypeDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'subEquipmentTypeDescription', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortBySubEquipmentTypeDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'subEquipmentTypeDescription', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByUnitNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByUnitNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByWorkPackageStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageStatus', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByWorkPackageStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageStatus', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByWorkTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workTypeId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      sortByWorkTypeIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workTypeId', Sort.desc);
    });
  }
}

extension WorkpackageDTOQuerySortThenBy
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QSortThenBy> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByDynamicColumns() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicColumns', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByDynamicColumnsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicColumns', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentTagNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTagNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentTagNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTagNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTypeId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByEquipmentTypeIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'equipmentTypeId', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByExpenseType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expenseType', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByExpenseTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expenseType', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByFunctionalLocation() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'functionalLocation', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByFunctionalLocationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'functionalLocation', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> thenByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByIsChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> thenByIsLocked() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLocked', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByIsLockedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLocked', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy> thenByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByPlannerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByPlannerName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannerName', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByPlannerNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannerName', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByPrimaryContractorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaryContractorId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByPrimaryContractorIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaryContractorId', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByProcessSystems() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'processSystems', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByProcessSystemsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'processSystems', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenBySapWorkOrder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sapWorkOrder', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenBySapWorkOrderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sapWorkOrder', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeApprovalCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeApprovalCode', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeApprovalCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeApprovalCode', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeIdentifier() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeIdentifier', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeIdentifierDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeIdentifier', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeRequestNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeRequestNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByScopeRequestNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'scopeRequestNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenBySubEquipmentTypeDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'subEquipmentTypeDescription', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenBySubEquipmentTypeDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'subEquipmentTypeDescription', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByUnitNumber() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitNumber', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByUnitNumberDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitNumber', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByWorkPackageStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageStatus', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByWorkPackageStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageStatus', Sort.desc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByWorkTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workTypeId', Sort.asc);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QAfterSortBy>
      thenByWorkTypeIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workTypeId', Sort.desc);
    });
  }
}

extension WorkpackageDTOQueryWhereDistinct
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> {
  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByDescription(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'description', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByDynamicColumns({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dynamicColumns',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByEquipmentNumber({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'equipmentNumber',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByEquipmentTagNumber({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'equipmentTagNumber',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByEquipmentTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'equipmentTypeId');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByExpenseType(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'expenseType', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByFunctionalLocation({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'functionalLocation',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isChanged');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByIsLocked() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isLocked');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'planner');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByPlannerName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'plannerName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByPrimaryContractorId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'primaryContractorId');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByProcessSystems({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'processSystems',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctBySapWorkOrder({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'sapWorkOrder', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByScopeApprovalCode({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'scopeApprovalCode',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByScopeIdentifier({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'scopeIdentifier',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByScopeRequestNumber({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'scopeRequestNumber',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctBySubEquipmentTypeDescription({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'subEquipmentTypeDescription',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct> distinctByUnitNumber(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unitNumber', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByWorkPackageStatus({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'workPackageStatus',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WorkpackageDTO, WorkpackageDTO, QDistinct>
      distinctByWorkTypeId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'workTypeId');
    });
  }
}

extension WorkpackageDTOQueryProperty
    on QueryBuilder<WorkpackageDTO, WorkpackageDTO, QQueryProperty> {
  QueryBuilder<WorkpackageDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      descriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'description');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      dynamicColumnsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dynamicColumns');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      equipmentNumberProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'equipmentNumber');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      equipmentTagNumberProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'equipmentTagNumber');
    });
  }

  QueryBuilder<WorkpackageDTO, EquipmentTypeDTO?, QQueryOperations>
      equipmentTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'equipmentType');
    });
  }

  QueryBuilder<WorkpackageDTO, int?, QQueryOperations>
      equipmentTypeIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'equipmentTypeId');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      expenseTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'expenseType');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      functionalLocationProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'functionalLocation');
    });
  }

  QueryBuilder<WorkpackageDTO, bool?, QQueryOperations> isChangedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isChanged');
    });
  }

  QueryBuilder<WorkpackageDTO, bool?, QQueryOperations> isLockedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isLocked');
    });
  }

  QueryBuilder<WorkpackageDTO, int?, QQueryOperations> plannerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'planner');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      plannerNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'plannerName');
    });
  }

  QueryBuilder<WorkpackageDTO, int?, QQueryOperations>
      primaryContractorIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'primaryContractorId');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      processSystemsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'processSystems');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      sapWorkOrderProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'sapWorkOrder');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      scopeApprovalCodeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'scopeApprovalCode');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      scopeIdentifierProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'scopeIdentifier');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      scopeRequestNumberProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'scopeRequestNumber');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      subEquipmentTypeDescriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'subEquipmentTypeDescription');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations> unitNumberProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unitNumber');
    });
  }

  QueryBuilder<WorkpackageDTO, String?, QQueryOperations>
      workPackageStatusProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workPackageStatus');
    });
  }

  QueryBuilder<WorkpackageDTO, WorkTypeDTO?, QQueryOperations>
      workTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workType');
    });
  }

  QueryBuilder<WorkpackageDTO, int?, QQueryOperations> workTypeIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workTypeId');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkpackageDTO _$WorkpackageDTOFromJson(Map<String, dynamic> json) =>
    WorkpackageDTO(
      id: json['id'] as int?,
      description: json['description'] as String?,
      scopeIdentifier: json['scopeIdentifier'] as String?,
      workType: json['workType'] == null
          ? null
          : WorkTypeDTO.fromJson(json['workType'] as Map<String, dynamic>),
      equipmentTypeId: json['equipmentTypeId'] as int?,
      workTypeId: json['workTypeId'] as int?,
      planner: json['planner'] as int?,
      plannerName: json['plannerName'] as String?,
      dynamicColumns: json['dynamicColumns'] as String?,
      equipmentType: json['equipmentType'] == null
          ? null
          : EquipmentTypeDTO.fromJson(
              json['equipmentType'] as Map<String, dynamic>),
      expenseType: json['expenseType'] as String?,
      processSystems: json['processSystems'] as String?,
      unitNumber: json['unitNumber'] as String?,
      scopeApprovalCode: json['scopeApprovalCode'] as String?,
      sapWorkOrder: json['sapWorkOrder'] as String?,
      scopeRequestNumber: json['scopeRequestNumber'] as String?,
      isLocked: json['isLocked'] as bool?,
      workPackageStatus: json['workPackageStatus'] as String?,
      primaryContractorId: json['primaryContractorId'] as int?,
      subEquipmentTypeDescription:
          json['subEquipmentTypeDescription'] as String?,
      functionalLocation: json['functionalLocation'] as String?,
      equipmentNumber: json['equipmentNumber'] as String?,
      equipmentTagNumber: json['equipmentTagNumber'] as String?,
    );

Map<String, dynamic> _$WorkpackageDTOToJson(WorkpackageDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'scopeIdentifier': instance.scopeIdentifier,
      'workTypeId': instance.workTypeId,
      'workType': instance.workType,
      'equipmentTypeId': instance.equipmentTypeId,
      'equipmentType': instance.equipmentType,
      'planner': instance.planner,
      'plannerName': instance.plannerName,
      'expenseType': instance.expenseType,
      'processSystems': instance.processSystems,
      'unitNumber': instance.unitNumber,
      'scopeApprovalCode': instance.scopeApprovalCode,
      'sapWorkOrder': instance.sapWorkOrder,
      'scopeRequestNumber': instance.scopeRequestNumber,
      'isLocked': instance.isLocked,
      'workPackageStatus': instance.workPackageStatus,
      'primaryContractorId': instance.primaryContractorId,
      'subEquipmentTypeDescription': instance.subEquipmentTypeDescription,
      'dynamicColumns': instance.dynamicColumns,
      'functionalLocation': instance.functionalLocation,
      'equipmentNumber': instance.equipmentNumber,
      'equipmentTagNumber': instance.equipmentTagNumber,
    };
