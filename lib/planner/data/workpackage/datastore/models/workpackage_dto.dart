import 'dart:convert';

import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/equipment_type_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/work_type_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_response.dart';
part 'workpackage_dto.g.dart';

@Collection()
@JsonSerializable()
class WorkpackageDTO {
  Id? id;
  String? description;
  String? scopeIdentifier;
  int? workTypeId;
  WorkTypeDTO? workType;
  int? equipmentTypeId;
  EquipmentTypeDTO? equipmentType;
  int? planner;
  String? plannerName;
  String? expenseType;
  String? processSystems;
  String? unitNumber;
  String? scopeApprovalCode;
  String? sapWorkOrder;
  String? scopeRequestNumber;
  bool? isLocked;
  String? workPackageStatus;
  int? primaryContractorId;
  String? subEquipmentTypeDescription;
  String? dynamicColumns;
  String? functionalLocation;
  String? equipmentNumber;
  String? equipmentTagNumber;

  //Booleans to track internal state
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool? isChanged;

  /// Workpackage DTO
  WorkpackageDTO({
    this.id,
    this.description,
    this.scopeIdentifier,
    this.workType,
    this.equipmentTypeId,
    this.workTypeId,
    this.planner,
    this.plannerName,
    this.dynamicColumns,
    this.equipmentType,
    this.expenseType,
    this.processSystems,
    this.unitNumber,
    this.scopeApprovalCode,
    this.sapWorkOrder,
    this.scopeRequestNumber,
    this.isLocked,
    this.workPackageStatus,
    this.primaryContractorId,
    this.subEquipmentTypeDescription,
    this.isChanged = false,
    this.functionalLocation,
    this.equipmentNumber,
    this.equipmentTagNumber,
  });

  factory WorkpackageDTO.fromModel({required WorkpackageResponse workpackageResponse}) {
    final workpackageDTO = WorkpackageDTO(
      id: workpackageResponse.id,
      description: workpackageResponse.description,
      scopeIdentifier: workpackageResponse.scopeIdentifier,
      workTypeId: workpackageResponse.workTypeId,
      workType: workpackageResponse.workType,
      equipmentTypeId: workpackageResponse.equipmentTypeId,
      equipmentType: workpackageResponse.equipmentType,
      planner: workpackageResponse.planner,
      plannerName: workpackageResponse.plannerName,
      expenseType: workpackageResponse.expenseType,
      processSystems: workpackageResponse.processSystems,
      unitNumber: workpackageResponse.unitNumber,
      scopeApprovalCode: workpackageResponse.scopeApprovalCode,
      sapWorkOrder: workpackageResponse.sapWorkOrder,
      scopeRequestNumber: workpackageResponse.scopeRequestNumber,
      isLocked: workpackageResponse.isLocked,
      workPackageStatus: workpackageResponse.workPackageStatus,
      primaryContractorId: workpackageResponse.primaryContractorId,
      subEquipmentTypeDescription: workpackageResponse.subEquipmentTypeDescription,
      dynamicColumns: jsonEncode(workpackageResponse.dynamicColumns),
      functionalLocation: workpackageResponse.functionalLocation,
      equipmentNumber: workpackageResponse.equipmentNumber,
      equipmentTagNumber: workpackageResponse.equipmentTagNumber,
    );
    return workpackageDTO;
  }

  factory WorkpackageDTO.fromJson(Map<String, dynamic> json) => _$WorkpackageDTOFromJson(json);
}
