// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workpackage_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WorkpackageResponse _$$_WorkpackageResponseFromJson(
        Map<String, dynamic> json) =>
    _$_WorkpackageResponse(
      id: json['id'] as int?,
      scopeIdentifier: json['scopeIdentifier'] as String?,
      description: json['description'] as String?,
      workTypeId: json['workTypeId'] as int?,
      workType: json['workType'] == null
          ? null
          : WorkTypeDTO.fromJson(json['workType'] as Map<String, dynamic>),
      equipmentTypeId: json['equipmentTypeId'] as int?,
      equipmentType: json['equipmentType'] == null
          ? null
          : EquipmentTypeDTO.fromJson(
              json['equipmentType'] as Map<String, dynamic>),
      planner: json['planner'] as int?,
      plannerName: json['plannerName'] as String?,
      expenseType: json['expenseType'] as String?,
      processSystems: json['processSystems'] as String?,
      unitNumber: json['unitNumber'] as String?,
      scopeApprovalCode: json['scopeApprovalCode'] as String?,
      sapWorkOrder: json['sapWorkOrder'] as String?,
      scopeRequestNumber: json['scopeRequestNumber'] as String?,
      isLocked: json['isLocked'] as bool?,
      workPackageStatus: json['workPackageStatus'] as String?,
      primaryContractorId: json['primaryContractorId'] as int?,
      subEquipmentTypeDescription:
          json['subEquipmentTypeDescription'] as String?,
      dynamicColumns: (json['dynamicColumns'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String?),
      ),
      attachments: (json['attachments'] as List<dynamic>?)
          ?.map((e) => AttachmentDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      functionalLocation: json['functionalLocation'] as String?,
      equipmentNumber: json['equipmentNumber'] as String?,
      equipmentTagNumber: json['equipmentTagNumber'] as String?,
    );

Map<String, dynamic> _$$_WorkpackageResponseToJson(
        _$_WorkpackageResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'scopeIdentifier': instance.scopeIdentifier,
      'description': instance.description,
      'workTypeId': instance.workTypeId,
      'workType': instance.workType,
      'equipmentTypeId': instance.equipmentTypeId,
      'equipmentType': instance.equipmentType,
      'planner': instance.planner,
      'plannerName': instance.plannerName,
      'expenseType': instance.expenseType,
      'processSystems': instance.processSystems,
      'unitNumber': instance.unitNumber,
      'scopeApprovalCode': instance.scopeApprovalCode,
      'sapWorkOrder': instance.sapWorkOrder,
      'scopeRequestNumber': instance.scopeRequestNumber,
      'isLocked': instance.isLocked,
      'workPackageStatus': instance.workPackageStatus,
      'primaryContractorId': instance.primaryContractorId,
      'subEquipmentTypeDescription': instance.subEquipmentTypeDescription,
      'dynamicColumns': instance.dynamicColumns,
      'attachments': instance.attachments,
      'functionalLocation': instance.functionalLocation,
      'equipmentNumber': instance.equipmentNumber,
      'equipmentTagNumber': instance.equipmentTagNumber,
    };
