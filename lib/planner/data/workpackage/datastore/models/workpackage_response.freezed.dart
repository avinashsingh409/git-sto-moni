// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'workpackage_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WorkpackageResponse _$WorkpackageResponseFromJson(Map<String, dynamic> json) {
  return _WorkpackageResponse.fromJson(json);
}

/// @nodoc
mixin _$WorkpackageResponse {
  int? get id => throw _privateConstructorUsedError;
  String? get scopeIdentifier => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  int? get workTypeId => throw _privateConstructorUsedError;
  WorkTypeDTO? get workType => throw _privateConstructorUsedError;
  int? get equipmentTypeId => throw _privateConstructorUsedError;
  EquipmentTypeDTO? get equipmentType => throw _privateConstructorUsedError;
  int? get planner => throw _privateConstructorUsedError;
  String? get plannerName => throw _privateConstructorUsedError;
  String? get expenseType => throw _privateConstructorUsedError;
  String? get processSystems => throw _privateConstructorUsedError;
  String? get unitNumber => throw _privateConstructorUsedError;
  String? get scopeApprovalCode => throw _privateConstructorUsedError;
  String? get sapWorkOrder => throw _privateConstructorUsedError;
  String? get scopeRequestNumber => throw _privateConstructorUsedError;
  bool? get isLocked => throw _privateConstructorUsedError;
  String? get workPackageStatus => throw _privateConstructorUsedError;
  int? get primaryContractorId => throw _privateConstructorUsedError;
  String? get subEquipmentTypeDescription => throw _privateConstructorUsedError;
  Map<String, String?>? get dynamicColumns =>
      throw _privateConstructorUsedError;
  List<AttachmentDTO>? get attachments => throw _privateConstructorUsedError;
  String? get functionalLocation => throw _privateConstructorUsedError;
  String? get equipmentNumber => throw _privateConstructorUsedError;
  String? get equipmentTagNumber => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WorkpackageResponseCopyWith<WorkpackageResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkpackageResponseCopyWith<$Res> {
  factory $WorkpackageResponseCopyWith(
          WorkpackageResponse value, $Res Function(WorkpackageResponse) then) =
      _$WorkpackageResponseCopyWithImpl<$Res, WorkpackageResponse>;
  @useResult
  $Res call(
      {int? id,
      String? scopeIdentifier,
      String? description,
      int? workTypeId,
      WorkTypeDTO? workType,
      int? equipmentTypeId,
      EquipmentTypeDTO? equipmentType,
      int? planner,
      String? plannerName,
      String? expenseType,
      String? processSystems,
      String? unitNumber,
      String? scopeApprovalCode,
      String? sapWorkOrder,
      String? scopeRequestNumber,
      bool? isLocked,
      String? workPackageStatus,
      int? primaryContractorId,
      String? subEquipmentTypeDescription,
      Map<String, String?>? dynamicColumns,
      List<AttachmentDTO>? attachments,
      String? functionalLocation,
      String? equipmentNumber,
      String? equipmentTagNumber});
}

/// @nodoc
class _$WorkpackageResponseCopyWithImpl<$Res, $Val extends WorkpackageResponse>
    implements $WorkpackageResponseCopyWith<$Res> {
  _$WorkpackageResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? scopeIdentifier = freezed,
    Object? description = freezed,
    Object? workTypeId = freezed,
    Object? workType = freezed,
    Object? equipmentTypeId = freezed,
    Object? equipmentType = freezed,
    Object? planner = freezed,
    Object? plannerName = freezed,
    Object? expenseType = freezed,
    Object? processSystems = freezed,
    Object? unitNumber = freezed,
    Object? scopeApprovalCode = freezed,
    Object? sapWorkOrder = freezed,
    Object? scopeRequestNumber = freezed,
    Object? isLocked = freezed,
    Object? workPackageStatus = freezed,
    Object? primaryContractorId = freezed,
    Object? subEquipmentTypeDescription = freezed,
    Object? dynamicColumns = freezed,
    Object? attachments = freezed,
    Object? functionalLocation = freezed,
    Object? equipmentNumber = freezed,
    Object? equipmentTagNumber = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      scopeIdentifier: freezed == scopeIdentifier
          ? _value.scopeIdentifier
          : scopeIdentifier // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      workTypeId: freezed == workTypeId
          ? _value.workTypeId
          : workTypeId // ignore: cast_nullable_to_non_nullable
              as int?,
      workType: freezed == workType
          ? _value.workType
          : workType // ignore: cast_nullable_to_non_nullable
              as WorkTypeDTO?,
      equipmentTypeId: freezed == equipmentTypeId
          ? _value.equipmentTypeId
          : equipmentTypeId // ignore: cast_nullable_to_non_nullable
              as int?,
      equipmentType: freezed == equipmentType
          ? _value.equipmentType
          : equipmentType // ignore: cast_nullable_to_non_nullable
              as EquipmentTypeDTO?,
      planner: freezed == planner
          ? _value.planner
          : planner // ignore: cast_nullable_to_non_nullable
              as int?,
      plannerName: freezed == plannerName
          ? _value.plannerName
          : plannerName // ignore: cast_nullable_to_non_nullable
              as String?,
      expenseType: freezed == expenseType
          ? _value.expenseType
          : expenseType // ignore: cast_nullable_to_non_nullable
              as String?,
      processSystems: freezed == processSystems
          ? _value.processSystems
          : processSystems // ignore: cast_nullable_to_non_nullable
              as String?,
      unitNumber: freezed == unitNumber
          ? _value.unitNumber
          : unitNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      scopeApprovalCode: freezed == scopeApprovalCode
          ? _value.scopeApprovalCode
          : scopeApprovalCode // ignore: cast_nullable_to_non_nullable
              as String?,
      sapWorkOrder: freezed == sapWorkOrder
          ? _value.sapWorkOrder
          : sapWorkOrder // ignore: cast_nullable_to_non_nullable
              as String?,
      scopeRequestNumber: freezed == scopeRequestNumber
          ? _value.scopeRequestNumber
          : scopeRequestNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      isLocked: freezed == isLocked
          ? _value.isLocked
          : isLocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      workPackageStatus: freezed == workPackageStatus
          ? _value.workPackageStatus
          : workPackageStatus // ignore: cast_nullable_to_non_nullable
              as String?,
      primaryContractorId: freezed == primaryContractorId
          ? _value.primaryContractorId
          : primaryContractorId // ignore: cast_nullable_to_non_nullable
              as int?,
      subEquipmentTypeDescription: freezed == subEquipmentTypeDescription
          ? _value.subEquipmentTypeDescription
          : subEquipmentTypeDescription // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicColumns: freezed == dynamicColumns
          ? _value.dynamicColumns
          : dynamicColumns // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      attachments: freezed == attachments
          ? _value.attachments
          : attachments // ignore: cast_nullable_to_non_nullable
              as List<AttachmentDTO>?,
      functionalLocation: freezed == functionalLocation
          ? _value.functionalLocation
          : functionalLocation // ignore: cast_nullable_to_non_nullable
              as String?,
      equipmentNumber: freezed == equipmentNumber
          ? _value.equipmentNumber
          : equipmentNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      equipmentTagNumber: freezed == equipmentTagNumber
          ? _value.equipmentTagNumber
          : equipmentTagNumber // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WorkpackageResponseCopyWith<$Res>
    implements $WorkpackageResponseCopyWith<$Res> {
  factory _$$_WorkpackageResponseCopyWith(_$_WorkpackageResponse value,
          $Res Function(_$_WorkpackageResponse) then) =
      __$$_WorkpackageResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      String? scopeIdentifier,
      String? description,
      int? workTypeId,
      WorkTypeDTO? workType,
      int? equipmentTypeId,
      EquipmentTypeDTO? equipmentType,
      int? planner,
      String? plannerName,
      String? expenseType,
      String? processSystems,
      String? unitNumber,
      String? scopeApprovalCode,
      String? sapWorkOrder,
      String? scopeRequestNumber,
      bool? isLocked,
      String? workPackageStatus,
      int? primaryContractorId,
      String? subEquipmentTypeDescription,
      Map<String, String?>? dynamicColumns,
      List<AttachmentDTO>? attachments,
      String? functionalLocation,
      String? equipmentNumber,
      String? equipmentTagNumber});
}

/// @nodoc
class __$$_WorkpackageResponseCopyWithImpl<$Res>
    extends _$WorkpackageResponseCopyWithImpl<$Res, _$_WorkpackageResponse>
    implements _$$_WorkpackageResponseCopyWith<$Res> {
  __$$_WorkpackageResponseCopyWithImpl(_$_WorkpackageResponse _value,
      $Res Function(_$_WorkpackageResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? scopeIdentifier = freezed,
    Object? description = freezed,
    Object? workTypeId = freezed,
    Object? workType = freezed,
    Object? equipmentTypeId = freezed,
    Object? equipmentType = freezed,
    Object? planner = freezed,
    Object? plannerName = freezed,
    Object? expenseType = freezed,
    Object? processSystems = freezed,
    Object? unitNumber = freezed,
    Object? scopeApprovalCode = freezed,
    Object? sapWorkOrder = freezed,
    Object? scopeRequestNumber = freezed,
    Object? isLocked = freezed,
    Object? workPackageStatus = freezed,
    Object? primaryContractorId = freezed,
    Object? subEquipmentTypeDescription = freezed,
    Object? dynamicColumns = freezed,
    Object? attachments = freezed,
    Object? functionalLocation = freezed,
    Object? equipmentNumber = freezed,
    Object? equipmentTagNumber = freezed,
  }) {
    return _then(_$_WorkpackageResponse(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      scopeIdentifier: freezed == scopeIdentifier
          ? _value.scopeIdentifier
          : scopeIdentifier // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      workTypeId: freezed == workTypeId
          ? _value.workTypeId
          : workTypeId // ignore: cast_nullable_to_non_nullable
              as int?,
      workType: freezed == workType
          ? _value.workType
          : workType // ignore: cast_nullable_to_non_nullable
              as WorkTypeDTO?,
      equipmentTypeId: freezed == equipmentTypeId
          ? _value.equipmentTypeId
          : equipmentTypeId // ignore: cast_nullable_to_non_nullable
              as int?,
      equipmentType: freezed == equipmentType
          ? _value.equipmentType
          : equipmentType // ignore: cast_nullable_to_non_nullable
              as EquipmentTypeDTO?,
      planner: freezed == planner
          ? _value.planner
          : planner // ignore: cast_nullable_to_non_nullable
              as int?,
      plannerName: freezed == plannerName
          ? _value.plannerName
          : plannerName // ignore: cast_nullable_to_non_nullable
              as String?,
      expenseType: freezed == expenseType
          ? _value.expenseType
          : expenseType // ignore: cast_nullable_to_non_nullable
              as String?,
      processSystems: freezed == processSystems
          ? _value.processSystems
          : processSystems // ignore: cast_nullable_to_non_nullable
              as String?,
      unitNumber: freezed == unitNumber
          ? _value.unitNumber
          : unitNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      scopeApprovalCode: freezed == scopeApprovalCode
          ? _value.scopeApprovalCode
          : scopeApprovalCode // ignore: cast_nullable_to_non_nullable
              as String?,
      sapWorkOrder: freezed == sapWorkOrder
          ? _value.sapWorkOrder
          : sapWorkOrder // ignore: cast_nullable_to_non_nullable
              as String?,
      scopeRequestNumber: freezed == scopeRequestNumber
          ? _value.scopeRequestNumber
          : scopeRequestNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      isLocked: freezed == isLocked
          ? _value.isLocked
          : isLocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      workPackageStatus: freezed == workPackageStatus
          ? _value.workPackageStatus
          : workPackageStatus // ignore: cast_nullable_to_non_nullable
              as String?,
      primaryContractorId: freezed == primaryContractorId
          ? _value.primaryContractorId
          : primaryContractorId // ignore: cast_nullable_to_non_nullable
              as int?,
      subEquipmentTypeDescription: freezed == subEquipmentTypeDescription
          ? _value.subEquipmentTypeDescription
          : subEquipmentTypeDescription // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicColumns: freezed == dynamicColumns
          ? _value._dynamicColumns
          : dynamicColumns // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      attachments: freezed == attachments
          ? _value._attachments
          : attachments // ignore: cast_nullable_to_non_nullable
              as List<AttachmentDTO>?,
      functionalLocation: freezed == functionalLocation
          ? _value.functionalLocation
          : functionalLocation // ignore: cast_nullable_to_non_nullable
              as String?,
      equipmentNumber: freezed == equipmentNumber
          ? _value.equipmentNumber
          : equipmentNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      equipmentTagNumber: freezed == equipmentTagNumber
          ? _value.equipmentTagNumber
          : equipmentTagNumber // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WorkpackageResponse implements _WorkpackageResponse {
  const _$_WorkpackageResponse(
      {required this.id,
      required this.scopeIdentifier,
      required this.description,
      required this.workTypeId,
      required this.workType,
      required this.equipmentTypeId,
      required this.equipmentType,
      required this.planner,
      required this.plannerName,
      required this.expenseType,
      required this.processSystems,
      required this.unitNumber,
      required this.scopeApprovalCode,
      required this.sapWorkOrder,
      required this.scopeRequestNumber,
      required this.isLocked,
      required this.workPackageStatus,
      required this.primaryContractorId,
      required this.subEquipmentTypeDescription,
      required final Map<String, String?>? dynamicColumns,
      required final List<AttachmentDTO>? attachments,
      required this.functionalLocation,
      required this.equipmentNumber,
      required this.equipmentTagNumber})
      : _dynamicColumns = dynamicColumns,
        _attachments = attachments;

  factory _$_WorkpackageResponse.fromJson(Map<String, dynamic> json) =>
      _$$_WorkpackageResponseFromJson(json);

  @override
  final int? id;
  @override
  final String? scopeIdentifier;
  @override
  final String? description;
  @override
  final int? workTypeId;
  @override
  final WorkTypeDTO? workType;
  @override
  final int? equipmentTypeId;
  @override
  final EquipmentTypeDTO? equipmentType;
  @override
  final int? planner;
  @override
  final String? plannerName;
  @override
  final String? expenseType;
  @override
  final String? processSystems;
  @override
  final String? unitNumber;
  @override
  final String? scopeApprovalCode;
  @override
  final String? sapWorkOrder;
  @override
  final String? scopeRequestNumber;
  @override
  final bool? isLocked;
  @override
  final String? workPackageStatus;
  @override
  final int? primaryContractorId;
  @override
  final String? subEquipmentTypeDescription;
  final Map<String, String?>? _dynamicColumns;
  @override
  Map<String, String?>? get dynamicColumns {
    final value = _dynamicColumns;
    if (value == null) return null;
    if (_dynamicColumns is EqualUnmodifiableMapView) return _dynamicColumns;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final List<AttachmentDTO>? _attachments;
  @override
  List<AttachmentDTO>? get attachments {
    final value = _attachments;
    if (value == null) return null;
    if (_attachments is EqualUnmodifiableListView) return _attachments;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final String? functionalLocation;
  @override
  final String? equipmentNumber;
  @override
  final String? equipmentTagNumber;

  @override
  String toString() {
    return 'WorkpackageResponse(id: $id, scopeIdentifier: $scopeIdentifier, description: $description, workTypeId: $workTypeId, workType: $workType, equipmentTypeId: $equipmentTypeId, equipmentType: $equipmentType, planner: $planner, plannerName: $plannerName, expenseType: $expenseType, processSystems: $processSystems, unitNumber: $unitNumber, scopeApprovalCode: $scopeApprovalCode, sapWorkOrder: $sapWorkOrder, scopeRequestNumber: $scopeRequestNumber, isLocked: $isLocked, workPackageStatus: $workPackageStatus, primaryContractorId: $primaryContractorId, subEquipmentTypeDescription: $subEquipmentTypeDescription, dynamicColumns: $dynamicColumns, attachments: $attachments, functionalLocation: $functionalLocation, equipmentNumber: $equipmentNumber, equipmentTagNumber: $equipmentTagNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WorkpackageResponse &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.scopeIdentifier, scopeIdentifier) ||
                other.scopeIdentifier == scopeIdentifier) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.workTypeId, workTypeId) ||
                other.workTypeId == workTypeId) &&
            (identical(other.workType, workType) ||
                other.workType == workType) &&
            (identical(other.equipmentTypeId, equipmentTypeId) ||
                other.equipmentTypeId == equipmentTypeId) &&
            (identical(other.equipmentType, equipmentType) ||
                other.equipmentType == equipmentType) &&
            (identical(other.planner, planner) || other.planner == planner) &&
            (identical(other.plannerName, plannerName) ||
                other.plannerName == plannerName) &&
            (identical(other.expenseType, expenseType) ||
                other.expenseType == expenseType) &&
            (identical(other.processSystems, processSystems) ||
                other.processSystems == processSystems) &&
            (identical(other.unitNumber, unitNumber) ||
                other.unitNumber == unitNumber) &&
            (identical(other.scopeApprovalCode, scopeApprovalCode) ||
                other.scopeApprovalCode == scopeApprovalCode) &&
            (identical(other.sapWorkOrder, sapWorkOrder) ||
                other.sapWorkOrder == sapWorkOrder) &&
            (identical(other.scopeRequestNumber, scopeRequestNumber) ||
                other.scopeRequestNumber == scopeRequestNumber) &&
            (identical(other.isLocked, isLocked) ||
                other.isLocked == isLocked) &&
            (identical(other.workPackageStatus, workPackageStatus) ||
                other.workPackageStatus == workPackageStatus) &&
            (identical(other.primaryContractorId, primaryContractorId) ||
                other.primaryContractorId == primaryContractorId) &&
            (identical(other.subEquipmentTypeDescription,
                    subEquipmentTypeDescription) ||
                other.subEquipmentTypeDescription ==
                    subEquipmentTypeDescription) &&
            const DeepCollectionEquality()
                .equals(other._dynamicColumns, _dynamicColumns) &&
            const DeepCollectionEquality()
                .equals(other._attachments, _attachments) &&
            (identical(other.functionalLocation, functionalLocation) ||
                other.functionalLocation == functionalLocation) &&
            (identical(other.equipmentNumber, equipmentNumber) ||
                other.equipmentNumber == equipmentNumber) &&
            (identical(other.equipmentTagNumber, equipmentTagNumber) ||
                other.equipmentTagNumber == equipmentTagNumber));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        scopeIdentifier,
        description,
        workTypeId,
        workType,
        equipmentTypeId,
        equipmentType,
        planner,
        plannerName,
        expenseType,
        processSystems,
        unitNumber,
        scopeApprovalCode,
        sapWorkOrder,
        scopeRequestNumber,
        isLocked,
        workPackageStatus,
        primaryContractorId,
        subEquipmentTypeDescription,
        const DeepCollectionEquality().hash(_dynamicColumns),
        const DeepCollectionEquality().hash(_attachments),
        functionalLocation,
        equipmentNumber,
        equipmentTagNumber
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WorkpackageResponseCopyWith<_$_WorkpackageResponse> get copyWith =>
      __$$_WorkpackageResponseCopyWithImpl<_$_WorkpackageResponse>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WorkpackageResponseToJson(
      this,
    );
  }
}

abstract class _WorkpackageResponse implements WorkpackageResponse {
  const factory _WorkpackageResponse(
      {required final int? id,
      required final String? scopeIdentifier,
      required final String? description,
      required final int? workTypeId,
      required final WorkTypeDTO? workType,
      required final int? equipmentTypeId,
      required final EquipmentTypeDTO? equipmentType,
      required final int? planner,
      required final String? plannerName,
      required final String? expenseType,
      required final String? processSystems,
      required final String? unitNumber,
      required final String? scopeApprovalCode,
      required final String? sapWorkOrder,
      required final String? scopeRequestNumber,
      required final bool? isLocked,
      required final String? workPackageStatus,
      required final int? primaryContractorId,
      required final String? subEquipmentTypeDescription,
      required final Map<String, String?>? dynamicColumns,
      required final List<AttachmentDTO>? attachments,
      required final String? functionalLocation,
      required final String? equipmentNumber,
      required final String? equipmentTagNumber}) = _$_WorkpackageResponse;

  factory _WorkpackageResponse.fromJson(Map<String, dynamic> json) =
      _$_WorkpackageResponse.fromJson;

  @override
  int? get id;
  @override
  String? get scopeIdentifier;
  @override
  String? get description;
  @override
  int? get workTypeId;
  @override
  WorkTypeDTO? get workType;
  @override
  int? get equipmentTypeId;
  @override
  EquipmentTypeDTO? get equipmentType;
  @override
  int? get planner;
  @override
  String? get plannerName;
  @override
  String? get expenseType;
  @override
  String? get processSystems;
  @override
  String? get unitNumber;
  @override
  String? get scopeApprovalCode;
  @override
  String? get sapWorkOrder;
  @override
  String? get scopeRequestNumber;
  @override
  bool? get isLocked;
  @override
  String? get workPackageStatus;
  @override
  int? get primaryContractorId;
  @override
  String? get subEquipmentTypeDescription;
  @override
  Map<String, String?>? get dynamicColumns;
  @override
  List<AttachmentDTO>? get attachments;
  @override
  String? get functionalLocation;
  @override
  String? get equipmentNumber;
  @override
  String? get equipmentTagNumber;
  @override
  @JsonKey(ignore: true)
  _$$_WorkpackageResponseCopyWith<_$_WorkpackageResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
