import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'work_type_dto.g.dart';

@embedded
@JsonSerializable()
class WorkTypeDTO {
  String? description;
  int? id;
  int? authorId;
  DateTime? dateCreated;
  bool? isDeleted;

  WorkTypeDTO({
    this.description,
    this.id,
    this.authorId,
    this.dateCreated,
    this.isDeleted,
  });

  factory WorkTypeDTO.fromJson(Map<String, dynamic> json) => _$WorkTypeDTOFromJson(json);
}
