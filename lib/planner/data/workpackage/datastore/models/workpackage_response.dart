import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/equipment_type_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/work_type_dto.dart';
part 'workpackage_response.freezed.dart';
part 'workpackage_response.g.dart';

@freezed
abstract class WorkpackageResponse with _$WorkpackageResponse {
  const factory WorkpackageResponse({
    required int? id,
    required String? scopeIdentifier,
    required String? description,
    required int? workTypeId,
    required WorkTypeDTO? workType,
    required int? equipmentTypeId,
    required EquipmentTypeDTO? equipmentType,
    required int? planner,
    required String? plannerName,
    required String? expenseType,
    required String? processSystems,
    required String? unitNumber,
    required String? scopeApprovalCode,
    required String? sapWorkOrder,
    required String? scopeRequestNumber,
    required bool? isLocked,
    required String? workPackageStatus,
    required int? primaryContractorId,
    required String? subEquipmentTypeDescription,
    required Map<String, String?>? dynamicColumns,
    required List<AttachmentDTO>? attachments,
    required String? functionalLocation,
    required String? equipmentNumber,
    required String? equipmentTagNumber,
  }) = _WorkpackageResponse;

  factory WorkpackageResponse.fromJson(Map<String, dynamic> json) => _$WorkpackageResponseFromJson(json);
}
