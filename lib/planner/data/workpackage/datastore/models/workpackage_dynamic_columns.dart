import 'package:injectable/injectable.dart';

@singleton
class WorkPackageDynamicColumns {
  static Map<String, String> fieldDefinitionsValueMap = {};

  void setFieldDefinitionsValueMap({Map<String, String>? newVal}) {
    if (newVal != null) {
      for (final entry in newVal.entries) {
        fieldDefinitionsValueMap.putIfAbsent(entry.key, () => entry.value);
      }
    }
  }
}
