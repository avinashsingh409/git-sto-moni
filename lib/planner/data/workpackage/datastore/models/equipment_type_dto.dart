import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'equipment_type_dto.g.dart';

@embedded
@JsonSerializable()
class EquipmentTypeDTO {
  String? name;
  String? description;
  int? tarPlanningMH;
  int? id;
  int? authorId;
  int? modifierId;
  DateTime? dateModified;
  DateTime? dateCreated;
  bool? isDeleted;

  EquipmentTypeDTO({
    this.name,
    this.description,
    this.tarPlanningMH,
    this.id,
    this.authorId,
    this.modifierId,
    this.dateModified,
    this.dateCreated,
    this.isDeleted,
  });

  factory EquipmentTypeDTO.fromJson(Map<String, dynamic> json) => _$EquipmentTypeDTOFromJson(json);
}
