import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_response.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/workpackage_api_service.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/workpackage_dao.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/datastore/workpackage_datastore.dart';

@LazySingleton(as: IWorkpackageDataStore)
class WorkpackageDataStoreImpl extends IWorkpackageDataStore {
  final WorkpackageApiService workpackageApiService;
  final WorkpackageDAO workpackageDao;
  WorkpackageDataStoreImpl(this.workpackageApiService, this.workpackageDao);

  @override
  Future<List<WorkpackageResponse>?> getOnlineWorkPackages({required int turnaroundEventId}) {
    return workpackageApiService.getWorkPackages(turnaroundEventId: turnaroundEventId).catchDioException();
  }

  @override
  Future<void> insertAllWorkpackages({required List<WorkpackageDTO> workpackageDTOs}) {
    return workpackageDao.upsertAllWorkPackages(workPackages: workpackageDTOs);
  }

  @override
  Future<void> deleteAllWorkpackages() {
    return workpackageDao.deleteAllWorkPackages();
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>> getAllCachedWorkPackages() {
    return workpackageDao.watchAllWorkPackages();
  }

  @override
  Future<void> updateStaticFields({required WorkpackageDTO newWorkpackageDTO}) {
    return workpackageDao.updateStaticFields(newWorkpackageDTO: newWorkpackageDTO);
  }

  @override
  BehaviorSubject<WorkpackageDTO?> getWorkpackagedById({required int workpackageId}) {
    return workpackageDao.watchWorkpackage(workpackageId: workpackageId);
  }

  @override
  Future<WorkpackageDTO?> getSingleWorkPackage({required int workPackageId}) {
    return workpackageDao.getSingleWorkPackage(workPackageId: workPackageId);
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesAlphabetically() {
    return workpackageDao.watchWorkpackagesAlphabetically();
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesPlannedByUser({required String preferredUsername}) {
    return workpackageDao.watchWorkpackagesPlannedByUser(preferredUsername: preferredUsername);
  }
}
