import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

@singleton
class SelectedWorkPackage {
  static WorkpackageDTO workpackageDTO = WorkpackageDTO();
  static String? dynamicColumns = '';

  void changeSelectedPackage(
      {required String? newDynamicColumns, required newWorkpackageId, required WorkpackageDTO selectedWorkpackage}) {
    workpackageDTO = selectedWorkpackage;
    dynamicColumns = null;
    dynamicColumns = newDynamicColumns;
  }
}
