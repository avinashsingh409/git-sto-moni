import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/enums/workpackage_search_type_enum.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/equipment_type_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

@injectable
class WorkpackageDAO {
  final isar = PersistenceModule.isar;

  WorkpackageDAO();

  /// Isar Collection of WorkpackageDTOs
  late IsarCollection<WorkpackageDTO> collection = isar.workpackageDTOs;

  /// Delete All WorkpackageDTOs
  Future<void> deleteAllWorkPackages() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All WorkpackageDTOs
  Future<List<WorkpackageDTO>> getAllWorkPackages() async {
    final workPackages = collection.where().findAll();
    return workPackages;
  }

  /// Get single WorkpackageDTO
  Future<WorkpackageDTO?> getSingleWorkPackage({required int workPackageId}) async {
    return collection.where().filter().idEqualTo(workPackageId).findFirst();
  }

  /// Watch All WorkPackages
  BehaviorSubject<List<WorkpackageDTO>> watchAllWorkPackages() {
    final bstream = BehaviorSubject<List<WorkpackageDTO>>();
    final query = collection.where(sort: Sort.desc).anyId().build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  /// Watch All WorkPackages
  BehaviorSubject<List<WorkpackageDTO>> watchWorkpackagesPlannedByUser({required String preferredUsername}) {
    final bstream = BehaviorSubject<List<WorkpackageDTO>>();
    final query = collection.where().filter().plannerNameContains(preferredUsername).build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  /// Watch All WorkPackages
  BehaviorSubject<List<WorkpackageDTO>> watchWorkpackagesAlphabetically() {
    final bstream = BehaviorSubject<List<WorkpackageDTO>>();
    final query = collection.where().sortByDescription().build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  //Update static fields on workpackage
  Future<void> updateStaticFields({required WorkpackageDTO newWorkpackageDTO}) async {
    return isar.writeTxn(() async {
      await collection.put(newWorkpackageDTO);
    });
  }

  /// Watch Work package
  BehaviorSubject<WorkpackageDTO?> watchWorkpackage({required int workpackageId}) {
    final bstream = BehaviorSubject<WorkpackageDTO?>();
    final stream = collection.watchObject(workpackageId, fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  /// Upsert All WorkpackageDTOs
  Future<List<int>> upsertAllWorkPackages({
    required List<WorkpackageDTO> workPackages,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(workPackages);
    });
  }

  // Get searched Work Packages
  BehaviorSubject<List<WorkpackageDTO>> getSearchedWorkPackages({required WorkpackageSearchTypeEnum field, required String keyword}) {
    final behaviorSubject = BehaviorSubject<List<WorkpackageDTO>>();
    var workPackages = collection.where().filter();
    Query<WorkpackageDTO> workPackagesQuery;

    switch (field) {
      case WorkpackageSearchTypeEnum.number:
        workPackagesQuery = workPackages
          .idEqualTo(int.tryParse(keyword)).build();
        break;
      case WorkpackageSearchTypeEnum.description:
        workPackagesQuery = workPackages
          .descriptionContains(keyword, caseSensitive: false).or()
          .descriptionStartsWith(keyword, caseSensitive: false).or()
          .descriptionEndsWith(keyword, caseSensitive: false).or()
          .descriptionMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.plannerName:
        workPackagesQuery = workPackages
          .plannerNameContains(keyword, caseSensitive: false).or()
          .plannerNameStartsWith(keyword, caseSensitive: false).or()
          .plannerNameEndsWith(keyword, caseSensitive: false).or()
          .plannerNameMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.functionalLocation:
        workPackagesQuery = workPackages
          .functionalLocationContains(keyword, caseSensitive: false).or()
          .functionalLocationStartsWith(keyword, caseSensitive: false).or()
          .functionalLocationEndsWith(keyword, caseSensitive: false).or()
          .functionalLocationMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.equipmentNumber:
        workPackagesQuery = workPackages
          .equipmentNumberContains(keyword, caseSensitive: false).or()
          .equipmentNumberStartsWith(keyword, caseSensitive: false).or()
          .equipmentNumberEndsWith(keyword, caseSensitive: false).or()
          .equipmentNumberMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.equipmentType:
        workPackagesQuery = workPackages
          .equipmentType((equipmentType) => equipmentType.nameContains(keyword, caseSensitive: false)).or()
          .equipmentType((equipmentType) => equipmentType.nameStartsWith(keyword, caseSensitive: false)).or()
          .equipmentType((equipmentType) => equipmentType.nameEndsWith(keyword, caseSensitive: false)).or()
          .equipmentType((equipmentType) => equipmentType.nameMatches(keyword, caseSensitive: false)).build();
        break;
      case WorkpackageSearchTypeEnum.equipmentSubtype:
        workPackagesQuery = workPackages
          .subEquipmentTypeDescriptionContains(keyword, caseSensitive: false).or()
          .subEquipmentTypeDescriptionStartsWith(keyword, caseSensitive: false).or()
          .subEquipmentTypeDescriptionEndsWith(keyword, caseSensitive: false).or()
          .subEquipmentTypeDescriptionMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.status:
        workPackagesQuery = workPackages
          .workPackageStatusContains(keyword, caseSensitive: false).or()
          .workPackageStatusStartsWith(keyword, caseSensitive: false).or()
          .workPackageStatusEndsWith(keyword, caseSensitive: false).or()
          .workPackageStatusMatches(keyword, caseSensitive: false).build();
        break;
      case WorkpackageSearchTypeEnum.equipmentTagNumber:
        workPackagesQuery = workPackages
          .equipmentTagNumberContains(keyword, caseSensitive: false).or()
          .equipmentTagNumberStartsWith(keyword, caseSensitive: false).or()
          .equipmentTagNumberEndsWith(keyword, caseSensitive: false).or()
          .equipmentTagNumberMatches(keyword, caseSensitive: false).build();
        break;
    }

    final stream = workPackagesQuery.watch(fireImmediately: true);
    behaviorSubject.addStream(stream);
    return behaviorSubject;
  }
}
