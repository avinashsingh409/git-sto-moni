import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/workpackage_exception.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_response.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/response/workpackage_grouping_response.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/repository/attachments_repository.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';
import 'package:sto_mobile_v2/planner/domain/planner_sync/repository/planner_sync_repository.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/datastore/workpackage_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/repository/workpackage_repository.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/repository/workpackage_grouping_repository.dart';

@LazySingleton(as: IWorkpackageRepository)
class WorkpackageRepositoryImpl extends IWorkpackageRepository {
  final IWorkpackageDataStore workpackageDataStore;
  final IWorkpackageGroupingRepository workpackageGroupingRepository;
  final IFieldDefinitionRepository fieldDefinitionRepository;
  final IAttachmentsRepository attachmentsRepository;
  final IPlannerSyncRepository plannerSyncRepository;
  final ISessionRepository sessionRepository;
  WorkpackageRepositoryImpl(
    this.workpackageDataStore,
    this.workpackageGroupingRepository,
    this.fieldDefinitionRepository,
    this.plannerSyncRepository,
    this.attachmentsRepository,
    this.sessionRepository,
  );

  @override
  Future<Either<Unit, WorkpackagesException>> getAndCachePlannerData({required int turnaroundEventId}) async {
    //Push data to Server
    final pushDataResult = await plannerSyncRepository.pushDataToServer(turnAroundEventId: turnaroundEventId);
    await plannerSyncRepository.uploadAttachments();
    pushDataResult.fold((_) {
      // The flow continues
    }, (exception) {
      return const Right(WorkpackagesException.unexpectedError());
    });
    //Clearing workpackage cache
    await Future.wait([
      workpackageDataStore.deleteAllWorkpackages(),
      workpackageGroupingRepository.deleteAllWorkpackageGroupings(),
      fieldDefinitionRepository.clearWorkpackageValues(),
      attachmentsRepository.deleteAllAttachments(),
    ]);

    //
    try {
      await Future.wait([
        workpackageDataStore.getOnlineWorkPackages(turnaroundEventId: turnaroundEventId),
        workpackageGroupingRepository.getAllGroupings(turnaroundEventId: turnaroundEventId),
      ]).then((response) async {
        //List of responses
        final workpackageData = response[0] as List<WorkpackageResponse>?;
        final workpackageGroupingData = response[1] as WorkpackageGroupingResponse?;

        //Workpackage Data
        if (workpackageData != null) {
          final List<WorkpackageDTO> workpackageDTOs = [];
          final List<FieldDefinitionValueDTO> fieldDefinitionValues = [];
          //List of attachments
          List<AttachmentDTO> attachments = [];
          for (WorkpackageResponse element in workpackageData) {
            workpackageDTOs.add(WorkpackageDTO.fromModel(workpackageResponse: element));
            final dynamicColumn = element.dynamicColumns;
            if (dynamicColumn != null) {
              dynamicColumn.forEach((key, value) {
                fieldDefinitionValues
                    .add(FieldDefinitionValueDTO(guid: key, value: value, isChanged: false, workpackageId: element.id ?? 0));
              });
            }
            final attachmentsFromResponse = element.attachments;
            if (attachmentsFromResponse != null && attachmentsFromResponse.isNotEmpty) {
              for (var attachmentItr in attachmentsFromResponse) {
                attachments.add(attachmentItr);
              }
            }
          }
          await workpackageDataStore.insertAllWorkpackages(workpackageDTOs: workpackageDTOs);
          await fieldDefinitionRepository.insertAllFieldDefinitionValues(fieldDefinitionValues: fieldDefinitionValues);
          await attachmentsRepository.insertAllAttachments(attachmentDTOs: attachments);
        }

        //Cache logic for grouping data
        if (workpackageGroupingData != null) {
          final staticFieldOptions = workpackageGroupingData.worklistFieldOptions;
          List<UserOptionsDTO> userOptions = [];
          List<ContractorOptionsDTO> contractorOptions = [];
          List<WorkTypeOptionsDTO> workTypeOptions = [];

          //Caching all the static Fields Options
          if (staticFieldOptions != null) {
            //Iterating through planner options
            if (staticFieldOptions.userOptions != null) {
              staticFieldOptions.userOptions?.forEach((key, value) {
                userOptions.add(UserOptionsDTO(id: key, userDetails: value));
              });
            }

            //Iterating through contractor options
            if (staticFieldOptions.contractorOptions != null) {
              staticFieldOptions.contractorOptions?.forEach((key, value) {
                contractorOptions.add(ContractorOptionsDTO(id: key, name: value));
              });
            }

            //Iteratng through workType options
            if (staticFieldOptions.workTypeOptions != null) {
              staticFieldOptions.workTypeOptions?.forEach((key, value) {
                workTypeOptions.add(WorkTypeOptionsDTO(id: key, description: value));
              });
            }
          }

          //Inserting all workpackages groups and subgroups
          await workpackageGroupingRepository.insertAllWorkpackageGroupings(
              workpackageGroupingDTOs: workpackageGroupingData.worklistGroups ?? []);

          //Inserting all static field options
          await fieldDefinitionRepository.cacheAllStaticFieldsOptions(
            userOptions: userOptions,
            workTypeOptions: workTypeOptions,
            contractorOptions: contractorOptions,
          );
        }
      });
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(WorkpackagesException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(WorkpackagesException.socketException());
        } else {
          return const Right(WorkpackagesException.unexpectedError());
        }
      }
      return const Right(WorkpackagesException.unexpectedError());
    } on ConnectionException catch (_) {
      return const Right(WorkpackagesException.socketException());
    }
    return const Left(unit);
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>> getAllCachedWorkPackages() {
    return workpackageDataStore.getAllCachedWorkPackages();
  }

  @override
  BehaviorSubject<WorkpackageDTO?> getWorkpackagedById() {
    return workpackageDataStore.getWorkpackagedById(workpackageId: SelectedWorkPackage.workpackageDTO.id ?? 0);
  }

  @override
  Future<WorkpackageDTO?> getSingleWorkPackage({required int workPackageId}) {
    return workpackageDataStore.getSingleWorkPackage(workPackageId: workPackageId);
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesAlphabetically() {
    return workpackageDataStore.getWorkpackagesAlphabetically();
  }

  @override
  BehaviorSubject<List<WorkpackageDTO>?> getWorkpackagesPlannedByUser() {
    //
    final session = sessionRepository.getSession();
    return session.fold((session) {
      return workpackageDataStore.getWorkpackagesPlannedByUser(
          preferredUsername: session.userInformation.preferredUsername ?? '');
    }, (_) {
      final exceptionStream = BehaviorSubject<List<WorkpackageDTO>>();
      return exceptionStream;
    });
  }
}
