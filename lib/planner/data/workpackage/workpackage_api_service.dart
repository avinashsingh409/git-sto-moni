import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_response.dart';

@injectable
class WorkpackageApiService {
  final Dio dio;

  WorkpackageApiService(this.dio);

  Future<List<WorkpackageResponse>?> getWorkPackages({required int turnaroundEventId}) async {
    final response = await dio.get(ApiEndPoints.getWorkpackages(eventId: turnaroundEventId));
    if (response.data != null) {
      List<WorkpackageResponse> items =
          List<WorkpackageResponse>.from(response.data.map((model) => WorkpackageResponse.fromJson(model)));
      return items;
    }
    return [];
  }
}
