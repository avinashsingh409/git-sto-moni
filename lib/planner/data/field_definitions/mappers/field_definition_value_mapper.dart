import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

@injectable
class FieldDefinitionValueMapper {
  bool mapAndReturnBoolVal({required String boolVal}) {
    if (boolVal == "true") {
      return true;
    } else {
      return false;
    }
  }

  DateTime? mapAndReturnDateTime({required String dateTimeString}) {
    try {
      DateTime parseDate = DateTime.parse(dateTimeString);
      var outputDate = DateFormat("yy-MM-dd HH:mm:ss").parse(parseDate.toString());
      var localTime = outputDate.toLocal();
      return localTime;
    } catch (_) {
      return null;
    }
  }
}
