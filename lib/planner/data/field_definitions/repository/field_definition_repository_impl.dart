import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/repository/sync_ticker_repository.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/translation_key_enum.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/field_definition_values_service.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_field_defition.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/datastore/field_definitions_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/repository/field_definition_repository.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/repository/workpackage_grouping_repository.dart';

@LazySingleton(as: IFieldDefinitionRepository)
class FieldDefinitionRepositoryImpl extends IFieldDefinitionRepository {
  final IFieldDefinitionDataStore fieldDefinitionDataStore;
  final FieldDefinitionValuesService fieldDefinitionValuesService;
  final IWorkpackageGroupingRepository workpackageGroupingRepository;
  final ISyncTickerRepository syncTickerRepository;
  FieldDefinitionRepositoryImpl(this.fieldDefinitionValuesService, this.fieldDefinitionDataStore,
      this.workpackageGroupingRepository, this.syncTickerRepository);

  @override
  Future<void> insertAllFieldDefinitionValues({required List<FieldDefinitionValueDTO> fieldDefinitionValues}) {
    return fieldDefinitionDataStore.insertAllFieldDefinitionValues(fieldDefinitionValues: fieldDefinitionValues);
  }

  @override
  Future<List<WorkpackageFieldDefinitionDTO>> getDynamicFieldDefinitionsWithValues({required int subGroupId}) async {
    final Map<String, String> answersMap = {};
    final Map<String, WorkpackageFieldDefinitionDTO> dynamicFieldDefsMap = {};
    await workpackageGroupingRepository
        .getAllFieldDefinitionsBySubGroupId(subGroupId: subGroupId)
        .then((allFieldDefinitions) async {
      final dynamicFieldDefitions = allFieldDefinitions
          .where((element) => element.definition?.static == false && (element.view != null && element.view!))
          .toList();
      final fieldDefinitionValues = await fieldDefinitionDataStore.getAllFieldDefinitionValuesFromWorkpackageId(
          workpackageId: SelectedWorkPackage.workpackageDTO.id ?? 0);

      //
      fieldDefinitionValues.map((defVal) {
        answersMap.addAll({"${defVal.guid}": "${defVal.value}"});
      }).toList();
      dynamicFieldDefitions.map((fieldDef) {
        dynamicFieldDefsMap.addAll({"${fieldDef.guid}": fieldDef});
      }).toList();

      //Iterating through fieldDefs
      for (var entry in answersMap.entries) {
        if (dynamicFieldDefsMap.containsKey(entry.key)) {
          var tempFieldDef = dynamicFieldDefsMap[entry.key];
          if (tempFieldDef != null) {
            final newVal = answersMap[entry.key];
            tempFieldDef = WorkpackageFieldDefinitionDTO(
              edit: tempFieldDef.edit,
              view: tempFieldDef.view,
              caption: tempFieldDef.caption,
              definition: tempFieldDef.definition,
              guid: tempFieldDef.guid,
              value: newVal,
            );
            dynamicFieldDefsMap[entry.key] = tempFieldDef;
          }
        }
      }
    });
    //Appending into a list
    List<WorkpackageFieldDefinitionDTO>? listOfFieldDefs = [];
    for (var element in dynamicFieldDefsMap.values) {
      listOfFieldDefs.add(element);
    }
    return listOfFieldDefs;
  }

  @override
  Future<void> updateDynamicFieldDefinitionValue({required String guid, required String newVal}) async {
    await syncTickerRepository.updateStatus();
    return fieldDefinitionDataStore.updateDynamicFieldDefinitionValue(guid: guid, newVal: newVal);
  }

  @override
  Future<List<WorkpackageFieldDefinitionDTO>> getStaticFieldDefinitionsWithValues({required int subGroupId}) async {
    final List<WorkpackageFieldDefinitionDTO> filteredStaticFields = [];
    await workpackageGroupingRepository
        .getAllFieldDefinitionsBySubGroupId(subGroupId: subGroupId)
        .then((allFieldDefinitions) async {
      //All static Field Definitions
      final staticFieldDefitions = allFieldDefinitions
          .where(
            (element) => element.definition?.static == true && (element.view != null && element.view == true),
          )
          .toList();
      final selectedWorkpackage = SelectedWorkPackage.workpackageDTO;
      //Filtered static field definitions list

      for (var field in staticFieldDefitions) {
        final translationKeyString = field.definition?.translationKey?.toLowerCase();
        final translationEnum = TranslationKey.values.firstWhere(
          (element) => element.name.toString() == translationKeyString,
          orElse: () => TranslationKey.none,
        );
        switch (translationEnum) {
          case TranslationKey.primarycontractor:
            var primaryContractor = selectedWorkpackage.primaryContractorId;
            field.value = primaryContractor?.toString();
            filteredStaticFields.add(field);
            break;
          case TranslationKey.worktype:
            var workTypeId = selectedWorkpackage.workTypeId;
            field.value = workTypeId?.toString() ?? '';
            filteredStaticFields.add(field);
            break;
          case TranslationKey.workscopedescription:
            var workScopeDescription = selectedWorkpackage.description;
            field.value = workScopeDescription;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.plannername:
            final plannerName = selectedWorkpackage.plannerName;
            field.value = plannerName;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.worklistid:
            final workListId = selectedWorkpackage.id;
            field.value = workListId?.toString() ?? '';
            filteredStaticFields.add(field);
            break;
          case TranslationKey.worklistitemstatus:
            final workListItemStatus = selectedWorkpackage.workPackageStatus;
            field.value = workListItemStatus;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.islocked:
            final isLocked = selectedWorkpackage.isLocked;
            field.value = isLocked.toString();
            filteredStaticFields.add(field);
            break;
          case TranslationKey.scoperequestnumber:
            final scopeRequestNumber = selectedWorkpackage.scopeRequestNumber;
            field.value = scopeRequestNumber;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.sapworkorder:
            final sapWorkOrder = selectedWorkpackage.sapWorkOrder;
            field.value = sapWorkOrder;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.scopeapprovalcode:
            final scopeapprovalcode = selectedWorkpackage.scopeApprovalCode;
            field.value = scopeapprovalcode;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.unitnumber:
            final unitNumber = selectedWorkpackage.unitNumber;
            field.value = unitNumber;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.processsystems:
            final processSystems = selectedWorkpackage.processSystems;
            field.value = processSystems;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.equipmenttype:
            final equipmentType = selectedWorkpackage.subEquipmentTypeDescription;
            field.value = equipmentType;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.stosubtype:
            final stoSubType = selectedWorkpackage.equipmentType?.description;
            field.value = stoSubType;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.expensetype:
            final expenseType = selectedWorkpackage.expenseType;
            field.value = expenseType;
            filteredStaticFields.add(field);
            break;
          case TranslationKey.none:
            break;
        }
      }
    });
    return filteredStaticFields;
  }

  @override
  Future<void> clearWorkpackageValues() async {
    await fieldDefinitionDataStore.clearWorkpackageCache();
    await fieldDefinitionDataStore.clearAllStaticFieldOptions();
  }

  @override
  Future<void> cacheAllStaticFieldsOptions(
      {required List<UserOptionsDTO>? userOptions,
      required List<WorkTypeOptionsDTO>? workTypeOptions,
      required List<ContractorOptionsDTO>? contractorOptions}) {
    return fieldDefinitionDataStore.cacheAllStaticFieldsOptions(
      userOptions: userOptions,
      workTypeOptions: workTypeOptions,
      contractorOptions: contractorOptions,
    );
  }

  @override
  Future<void> updateStaticFieldDefinitionValue(
      {required TranslationKey translationKey, required String newVal, required String? plannerName}) async {
    await syncTickerRepository.updateStatus();
    var currentVal = SelectedWorkPackage.workpackageDTO;
    if (translationKey == TranslationKey.primarycontractor) {
      await fieldDefinitionDataStore.cacheStaticFieldChanges(newValue: newVal, translationKey: 'PrimaryContractor');
      currentVal.primaryContractorId = int.tryParse(newVal);
    } else if (translationKey == TranslationKey.worktype) {
      await fieldDefinitionDataStore.cacheStaticFieldChanges(newValue: newVal, translationKey: 'WorkType');
      currentVal.workTypeId = int.tryParse(newVal);
    } else if (translationKey == TranslationKey.workscopedescription) {
      await fieldDefinitionDataStore.cacheStaticFieldChanges(newValue: newVal, translationKey: 'WorkScopeDescription');
      currentVal.description = newVal;
    } else if (translationKey == TranslationKey.plannername) {
      await fieldDefinitionDataStore.cacheStaticFieldChanges(newValue: newVal, translationKey: 'PlannerName');
      currentVal.plannerName = plannerName;
      currentVal.planner = int.tryParse(newVal);
    }
    await fieldDefinitionDataStore.updateStaticFields(newWorkpackageDTO: currentVal);
  }

  @override
  Future<List<ContractorOptionsDTO>?> getAllContractorsOptions() {
    return fieldDefinitionDataStore.getAllContractorsOptions();
  }

  @override
  Future<List<UserOptionsDTO>?> getAllPlannerOptions() {
    return fieldDefinitionDataStore.getAllPlannerOptions();
  }

  @override
  Future<List<WorkTypeOptionsDTO>?> getAllWorkTypeOptions() {
    return fieldDefinitionDataStore.getAllWorkTypeOptions();
  }

  @override
  Future<ContractorOptionsDTO?> getCurrentContractor() {
    return fieldDefinitionDataStore.getCurrentContractor();
  }

  @override
  Future<WorkTypeOptionsDTO?> getCurrentWorkType() {
    return fieldDefinitionDataStore.getCurrentWorkType();
  }

  @override
  Future<UserOptionsDTO?> getCurrentPlanner() {
    return fieldDefinitionDataStore.getCurrentPlanner();
  }
}
