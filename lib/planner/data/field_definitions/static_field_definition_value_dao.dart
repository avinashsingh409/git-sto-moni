import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class StaticFieldDefinitionValueDAO {
  final isar = PersistenceModule.isar;

  StaticFieldDefinitionValueDAO();

  /// Isar Collection of StaticFieldDefinitionValueDTOs
  late IsarCollection<StaticFieldDefinitionValueDTO> collection = isar.staticFieldDefinitionValueDTOs;

  /// Delete All StaticFieldDefinitionValueDTOs
  Future<void> deleteAllStaticFieldDefinitionValues() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  Future<void> updateStaticFieldDefinition({required String translationKey, required String newValue}) async {
    final existingDefinition = await collection
        .where()
        .filter()
        .workpackageIdEqualTo(SelectedWorkPackage.workpackageDTO.id)
        .and()
        .translationKeyEqualTo(translationKey)
        .findFirst();

    if (existingDefinition != null) {
      existingDefinition.value = newValue;
      isar.writeTxn(() async {
        await collection.put(existingDefinition);
      });
    } else {
      isar.writeTxn(() async {
        await collection.put(StaticFieldDefinitionValueDTO(
          workpackageId: SelectedWorkPackage.workpackageDTO.id,
          translationKey: translationKey,
          value: newValue,
        ));
      });
    }
  }

  /// Get All Static FieldDefinitionValueDTOs
  Future<List<StaticFieldDefinitionValueDTO>?> getAllChangedStaticFieldDefinitionValues() async {
    final fieldDefinitions = collection.where().findAll();
    return fieldDefinitions;
  }
}
