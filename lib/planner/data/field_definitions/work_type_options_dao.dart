import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class WorkTypeOptionsDAO {
  final isar = PersistenceModule.isar;

  WorkTypeOptionsDAO();

  /// Isar Collection of WorkTypeOptions DTO
  late IsarCollection<WorkTypeOptionsDTO> collection = isar.workTypeOptionsDTOs;

  /// Upsert All WorkType Options
  Future<List<int>> upsertAllWorkTypeOptions({
    required List<WorkTypeOptionsDTO> workTypeOptions,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(workTypeOptions);
    });
  }

  Future<WorkTypeOptionsDTO?> getCurrentWorkType() async {
    final workTypeId = SelectedWorkPackage.workpackageDTO.workTypeId;
    if (workTypeId != null) {
      final workType = await collection.where().filter().idEqualTo(workTypeId).findFirst();
      return workType;
    } else {
      return null;
    }
  }

  Future<List<WorkTypeOptionsDTO>?> getAllWorkTypeOptions() async {
    final workTypeOptions = await collection.where().findAll();
    workTypeOptions.sort((a, b) => a.description?.toLowerCase().compareTo(b.description?.toLowerCase() ?? '') ?? 0);
    return workTypeOptions;
  }

  /// Delete All WorkType Options
  Future<void> deleteAllWorkTypeOptions() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }
}
