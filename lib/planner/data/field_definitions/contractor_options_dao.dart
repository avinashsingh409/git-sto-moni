import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class ContractorOptionsDAO {
  final isar = PersistenceModule.isar;

  ContractorOptionsDAO();

  /// Isar Collection of ContractorOptions DTO
  late IsarCollection<ContractorOptionsDTO> collection = isar.contractorOptionsDTOs;

  /// Upsert All Contractor Options
  Future<List<int>> upsertAllContractorOptions({
    required List<ContractorOptionsDTO> contractorOptions,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(contractorOptions);
    });
  }

  Future<List<ContractorOptionsDTO>?> getAllContractorOptions() async {
    final contractorOptions = await collection.where().findAll();
    contractorOptions.sort((a, b) => a.name?.toLowerCase().compareTo(b.name?.toLowerCase() ?? '') ?? 0);
    return contractorOptions;
  }

  Future<ContractorOptionsDTO?> getCurrentContractor() async {
    final contractorId = SelectedWorkPackage.workpackageDTO.primaryContractorId;
    if (contractorId != null) {
      final contractor = await collection.where().filter().idEqualTo(contractorId).findFirst();
      return contractor;
    } else {
      return null;
    }
  }

  /// Delete All Contractor Options
  Future<void> deleteAllContractorOptions() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }
}
