import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class UserOptionsDAO {
  final isar = PersistenceModule.isar;

  UserOptionsDAO();

  /// Isar Collection of User Options DTO
  late IsarCollection<UserOptionsDTO> collection = isar.userOptionsDTOs;

  /// Upsert All User Options
  Future<List<int>> upsertAllUserOptions({
    required List<UserOptionsDTO> plannerOptions,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(plannerOptions);
    });
  }

  Future<List<UserOptionsDTO>> getPaginatedUserOptions({
    required int limit,
    required int offset,
  }) async {
    final options = await isar.userOptionsDTOs.buildQuery<UserOptionsDTO>(limit: limit, offset: offset).findAll();
    return options;
  }

  Future<List<UserOptionsDTO>?> getAllUserOptions() async {
    final plannerOptions = await collection.where().findAll();
    plannerOptions.sort((a, b) => a.userDetails?.toLowerCase().compareTo(b.userDetails?.toLowerCase() ?? '') ?? 0);
    return plannerOptions;
  }

  Future<UserOptionsDTO?> getCurrentPlanner() async {
    final currentPlanner = collection.where().filter().idEqualTo(SelectedWorkPackage.workpackageDTO.planner ?? 0).findFirst();
    return currentPlanner;
  }

  /// Delete All User Options
  Future<void> deleteAllUserOptions() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }
}
