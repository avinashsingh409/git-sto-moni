import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class FieldDefinitionValuesService {
  FieldDefinitionValuesService();

  String? getFieldDefinitionValue({required String guid}) {
    final dynamicColumnsString = SelectedWorkPackage.dynamicColumns;
    if (dynamicColumnsString != null) {
      final Map<String, dynamic> decodedColumns = jsonDecode(dynamicColumnsString);
      return decodedColumns[guid].toString();
    }
    return null;
  }
}
