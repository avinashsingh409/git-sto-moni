import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';

@injectable
class FieldDefinitionValueDAO {
  final isar = PersistenceModule.isar;

  FieldDefinitionValueDAO();

  /// Isar Collection of FieldDefinitionValueDTOs
  late IsarCollection<FieldDefinitionValueDTO> collection = isar.fieldDefinitionValueDTOs;

  /// Delete All FieldDefinitionValueDTOs
  Future<void> deleteAllFieldDefinitionValues() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  //Update FieldDefinition Values
  Future<void> updateFieldDefinitionValue({required String guid, required String newVal}) async {
    final itemNeededToBeUpdated = await collection
        .where()
        .filter()
        .guidEqualTo(guid)
        .and()
        .workpackageIdEqualTo(SelectedWorkPackage.workpackageDTO.id ?? 0)
        .findFirst();

    //Delete if its cleared that is passed '
    //
    if (itemNeededToBeUpdated != null) {
      itemNeededToBeUpdated.value = newVal;
      itemNeededToBeUpdated.isChanged = true;
      isar.writeTxn(() async {
        await collection.put(itemNeededToBeUpdated);
      });
    } else {
      isar.writeTxn(() async {
        await collection.put(FieldDefinitionValueDTO(
            guid: guid, isChanged: true, value: newVal, workpackageId: SelectedWorkPackage.workpackageDTO.id ?? 0));
      });
    }
  }

  /// Get All FieldDefinitionValueDTOs
  Future<List<FieldDefinitionValueDTO>?> getAllChangedFieldDefinitionValues() async {
    final fieldDefinitions = collection.where().filter().isChangedEqualTo(true).findAll();
    return fieldDefinitions;
  }

  /// Get All FieldDefinitionValueDTOs by WorkpackageId
  Future<List<FieldDefinitionValueDTO>> getAllFieldDefsByWorkpackageId({required int workpackageId}) async {
    final query = collection.where().filter().workpackageIdEqualTo(workpackageId).build().findAll();
    return query;
  }

  /// Upsert All FieldDefinitionValueDTOs
  Future<List<int>> upsertAllFieldDefinitionValues({
    required List<FieldDefinitionValueDTO> fieldDefinitionValues,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(fieldDefinitionValues);
    });
  }
}
