import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/contractor_options_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/field_definition_value_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/static_field_definition_value_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/user_options_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/work_type_options_dao.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/workpackage_dao.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/datastore/field_definitions_datastore.dart';

@LazySingleton(as: IFieldDefinitionDataStore)
class FieldDefinitionDataStoreImpl extends IFieldDefinitionDataStore {
  final FieldDefinitionValueDAO fieldDefinitionValueDAO;
  final StaticFieldDefinitionValueDAO staticFieldDefinitionValueDAO;
  final UserOptionsDAO userOptionsDAO;
  final ContractorOptionsDAO contractorOptionsDAO;
  final WorkTypeOptionsDAO workTypeOptionsDAO;
  final WorkpackageDAO workpackageDAO;

  FieldDefinitionDataStoreImpl(this.fieldDefinitionValueDAO, this.userOptionsDAO, this.contractorOptionsDAO,
      this.workTypeOptionsDAO, this.workpackageDAO, this.staticFieldDefinitionValueDAO);
  @override
  Future<void> insertAllFieldDefinitionValues({required List<FieldDefinitionValueDTO> fieldDefinitionValues}) {
    return fieldDefinitionValueDAO.upsertAllFieldDefinitionValues(fieldDefinitionValues: fieldDefinitionValues);
  }

  @override
  Future<List<FieldDefinitionValueDTO>> getAllFieldDefinitionValuesFromWorkpackageId({required int workpackageId}) {
    return fieldDefinitionValueDAO.getAllFieldDefsByWorkpackageId(workpackageId: workpackageId);
  }

  @override
  Future<void> clearWorkpackageCache() async {
    await fieldDefinitionValueDAO.deleteAllFieldDefinitionValues();
    await staticFieldDefinitionValueDAO.deleteAllStaticFieldDefinitionValues();
  }

  @override
  Future<void> updateDynamicFieldDefinitionValue({required String guid, required String newVal}) {
    return fieldDefinitionValueDAO.updateFieldDefinitionValue(guid: guid, newVal: newVal);
  }

  @override
  Future<List<FieldDefinitionValueDTO>?> getAllChangedFieldDefinitions() {
    return fieldDefinitionValueDAO.getAllChangedFieldDefinitionValues();
  }

  @override
  Future<void> cacheAllStaticFieldsOptions(
      {required List<UserOptionsDTO>? userOptions,
      required List<WorkTypeOptionsDTO>? workTypeOptions,
      required List<ContractorOptionsDTO>? contractorOptions}) async {
    if (userOptions != null) {
      await userOptionsDAO.upsertAllUserOptions(plannerOptions: userOptions);
    }
    if (workTypeOptions != null) {
      await workTypeOptionsDAO.upsertAllWorkTypeOptions(workTypeOptions: workTypeOptions);
    }
    if (contractorOptions != null) {
      await contractorOptionsDAO.upsertAllContractorOptions(contractorOptions: contractorOptions);
    }
  }

  @override
  Future<void> clearAllStaticFieldOptions() async {
    await userOptionsDAO.deleteAllUserOptions();
    await contractorOptionsDAO.deleteAllContractorOptions();
    await workTypeOptionsDAO.deleteAllWorkTypeOptions();
  }

  @override
  Future<void> updateStaticFields({required WorkpackageDTO newWorkpackageDTO}) {
    return workpackageDAO.updateStaticFields(newWorkpackageDTO: newWorkpackageDTO);
  }

  @override
  Future<List<ContractorOptionsDTO>?> getAllContractorsOptions() {
    return contractorOptionsDAO.getAllContractorOptions();
  }

  @override
  Future<List<UserOptionsDTO>?> getAllPlannerOptions() {
    return userOptionsDAO.getAllUserOptions();
  }

  @override
  Future<List<WorkTypeOptionsDTO>?> getAllWorkTypeOptions() {
    return workTypeOptionsDAO.getAllWorkTypeOptions();
  }

  @override
  Future<ContractorOptionsDTO?> getCurrentContractor() {
    return contractorOptionsDAO.getCurrentContractor();
  }

  @override
  Future<WorkTypeOptionsDTO?> getCurrentWorkType() {
    return workTypeOptionsDAO.getCurrentWorkType();
  }

  @override
  Future<UserOptionsDTO?> getCurrentPlanner() {
    return userOptionsDAO.getCurrentPlanner();
  }

  @override
  Future<void> cacheStaticFieldChanges({required String translationKey, required String newValue}) {
    return staticFieldDefinitionValueDAO.updateStaticFieldDefinition(translationKey: translationKey, newValue: newValue);
  }

  @override
  Future<List<StaticFieldDefinitionValueDTO>?> getAllChangedStaticFielddefinitions() {
    return staticFieldDefinitionValueDAO.getAllChangedStaticFieldDefinitionValues();
  }
}
