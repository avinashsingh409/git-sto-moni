import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'static_field_definition_value_dto.g.dart';

@Collection()
@JsonSerializable()
class StaticFieldDefinitionValueDTO {
  Id id = Isar.autoIncrement; //
  int? workpackageId;
  String? translationKey;
  String? value;

  /// StaticFieldDefinitionValue DTO
  StaticFieldDefinitionValueDTO({
    this.workpackageId,
    this.translationKey,
    this.value,
  });

  factory StaticFieldDefinitionValueDTO.fromJson(Map<String, dynamic> json) => _$StaticFieldDefinitionValueDTOFromJson(json);
}
