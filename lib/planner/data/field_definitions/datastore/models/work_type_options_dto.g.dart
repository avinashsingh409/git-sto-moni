// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work_type_options_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetWorkTypeOptionsDTOCollection on Isar {
  IsarCollection<WorkTypeOptionsDTO> get workTypeOptionsDTOs =>
      this.collection();
}

const WorkTypeOptionsDTOSchema = CollectionSchema(
  name: r'WorkTypeOptionsDTO',
  id: 9127594667245695752,
  properties: {
    r'description': PropertySchema(
      id: 0,
      name: r'description',
      type: IsarType.string,
    )
  },
  estimateSize: _workTypeOptionsDTOEstimateSize,
  serialize: _workTypeOptionsDTOSerialize,
  deserialize: _workTypeOptionsDTODeserialize,
  deserializeProp: _workTypeOptionsDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _workTypeOptionsDTOGetId,
  getLinks: _workTypeOptionsDTOGetLinks,
  attach: _workTypeOptionsDTOAttach,
  version: '3.1.0+1',
);

int _workTypeOptionsDTOEstimateSize(
  WorkTypeOptionsDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _workTypeOptionsDTOSerialize(
  WorkTypeOptionsDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.description);
}

WorkTypeOptionsDTO _workTypeOptionsDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WorkTypeOptionsDTO(
    description: reader.readStringOrNull(offsets[0]),
    id: id,
  );
  return object;
}

P _workTypeOptionsDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _workTypeOptionsDTOGetId(WorkTypeOptionsDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _workTypeOptionsDTOGetLinks(
    WorkTypeOptionsDTO object) {
  return [];
}

void _workTypeOptionsDTOAttach(
    IsarCollection<dynamic> col, Id id, WorkTypeOptionsDTO object) {
  object.id = id;
}

extension WorkTypeOptionsDTOQueryWhereSort
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QWhere> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension WorkTypeOptionsDTOQueryWhere
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QWhereClause> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkTypeOptionsDTOQueryFilter
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QFilterCondition> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WorkTypeOptionsDTOQueryObject
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QFilterCondition> {}

extension WorkTypeOptionsDTOQueryLinks
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QFilterCondition> {}

extension WorkTypeOptionsDTOQuerySortBy
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QSortBy> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      sortByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      sortByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }
}

extension WorkTypeOptionsDTOQuerySortThenBy
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QSortThenBy> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      thenByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      thenByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }
}

extension WorkTypeOptionsDTOQueryWhereDistinct
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QDistinct> {
  QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QDistinct>
      distinctByDescription({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'description', caseSensitive: caseSensitive);
    });
  }
}

extension WorkTypeOptionsDTOQueryProperty
    on QueryBuilder<WorkTypeOptionsDTO, WorkTypeOptionsDTO, QQueryProperty> {
  QueryBuilder<WorkTypeOptionsDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<WorkTypeOptionsDTO, String?, QQueryOperations>
      descriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'description');
    });
  }
}
