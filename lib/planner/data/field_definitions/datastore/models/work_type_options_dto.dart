import 'package:isar/isar.dart';
part 'work_type_options_dto.g.dart';

@Collection()
class WorkTypeOptionsDTO {
  Id id; //
  String? description;

  // WorkTypeOptions DTO
  WorkTypeOptionsDTO({
    required this.id,
    this.description,
  });
}
