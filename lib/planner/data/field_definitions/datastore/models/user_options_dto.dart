import 'package:isar/isar.dart';
part 'user_options_dto.g.dart';

@Collection()
class UserOptionsDTO {
  Id id; //
  String? userDetails;

  /// UserOptions DTO
  UserOptionsDTO({
    required this.id,
    this.userDetails,
  });
}
