import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'field_definition_value_dto.g.dart';

@Collection()
@JsonSerializable()
class FieldDefinitionValueDTO {
  Id id = Isar.autoIncrement; //
  int? workpackageId;
  String? guid;
  String? value;
  bool? isChanged;

  /// FieldDefinitionValue DTO
  FieldDefinitionValueDTO({
    this.guid,
    this.workpackageId,
    this.value,
    this.isChanged,
  });

  factory FieldDefinitionValueDTO.fromJson(Map<String, dynamic> json) => _$FieldDefinitionValueDTOFromJson(json);
}
