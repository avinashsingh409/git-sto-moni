// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_options_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetUserOptionsDTOCollection on Isar {
  IsarCollection<UserOptionsDTO> get userOptionsDTOs => this.collection();
}

const UserOptionsDTOSchema = CollectionSchema(
  name: r'UserOptionsDTO',
  id: -5051328888678352409,
  properties: {
    r'userDetails': PropertySchema(
      id: 0,
      name: r'userDetails',
      type: IsarType.string,
    )
  },
  estimateSize: _userOptionsDTOEstimateSize,
  serialize: _userOptionsDTOSerialize,
  deserialize: _userOptionsDTODeserialize,
  deserializeProp: _userOptionsDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _userOptionsDTOGetId,
  getLinks: _userOptionsDTOGetLinks,
  attach: _userOptionsDTOAttach,
  version: '3.1.0+1',
);

int _userOptionsDTOEstimateSize(
  UserOptionsDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.userDetails;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _userOptionsDTOSerialize(
  UserOptionsDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.userDetails);
}

UserOptionsDTO _userOptionsDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = UserOptionsDTO(
    id: id,
    userDetails: reader.readStringOrNull(offsets[0]),
  );
  return object;
}

P _userOptionsDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _userOptionsDTOGetId(UserOptionsDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _userOptionsDTOGetLinks(UserOptionsDTO object) {
  return [];
}

void _userOptionsDTOAttach(
    IsarCollection<dynamic> col, Id id, UserOptionsDTO object) {
  object.id = id;
}

extension UserOptionsDTOQueryWhereSort
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QWhere> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension UserOptionsDTOQueryWhere
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QWhereClause> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension UserOptionsDTOQueryFilter
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QFilterCondition> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'userDetails',
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'userDetails',
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'userDetails',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'userDetails',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'userDetails',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'userDetails',
        value: '',
      ));
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterFilterCondition>
      userDetailsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'userDetails',
        value: '',
      ));
    });
  }
}

extension UserOptionsDTOQueryObject
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QFilterCondition> {}

extension UserOptionsDTOQueryLinks
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QFilterCondition> {}

extension UserOptionsDTOQuerySortBy
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QSortBy> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy>
      sortByUserDetails() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userDetails', Sort.asc);
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy>
      sortByUserDetailsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userDetails', Sort.desc);
    });
  }
}

extension UserOptionsDTOQuerySortThenBy
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QSortThenBy> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy>
      thenByUserDetails() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userDetails', Sort.asc);
    });
  }

  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QAfterSortBy>
      thenByUserDetailsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userDetails', Sort.desc);
    });
  }
}

extension UserOptionsDTOQueryWhereDistinct
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QDistinct> {
  QueryBuilder<UserOptionsDTO, UserOptionsDTO, QDistinct> distinctByUserDetails(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'userDetails', caseSensitive: caseSensitive);
    });
  }
}

extension UserOptionsDTOQueryProperty
    on QueryBuilder<UserOptionsDTO, UserOptionsDTO, QQueryProperty> {
  QueryBuilder<UserOptionsDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<UserOptionsDTO, String?, QQueryOperations>
      userDetailsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'userDetails');
    });
  }
}
