// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'static_field_definition_value_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetStaticFieldDefinitionValueDTOCollection on Isar {
  IsarCollection<StaticFieldDefinitionValueDTO>
      get staticFieldDefinitionValueDTOs => this.collection();
}

const StaticFieldDefinitionValueDTOSchema = CollectionSchema(
  name: r'StaticFieldDefinitionValueDTO',
  id: -5341869168002545823,
  properties: {
    r'translationKey': PropertySchema(
      id: 0,
      name: r'translationKey',
      type: IsarType.string,
    ),
    r'value': PropertySchema(
      id: 1,
      name: r'value',
      type: IsarType.string,
    ),
    r'workpackageId': PropertySchema(
      id: 2,
      name: r'workpackageId',
      type: IsarType.long,
    )
  },
  estimateSize: _staticFieldDefinitionValueDTOEstimateSize,
  serialize: _staticFieldDefinitionValueDTOSerialize,
  deserialize: _staticFieldDefinitionValueDTODeserialize,
  deserializeProp: _staticFieldDefinitionValueDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _staticFieldDefinitionValueDTOGetId,
  getLinks: _staticFieldDefinitionValueDTOGetLinks,
  attach: _staticFieldDefinitionValueDTOAttach,
  version: '3.1.0+1',
);

int _staticFieldDefinitionValueDTOEstimateSize(
  StaticFieldDefinitionValueDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.translationKey;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.value;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _staticFieldDefinitionValueDTOSerialize(
  StaticFieldDefinitionValueDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.translationKey);
  writer.writeString(offsets[1], object.value);
  writer.writeLong(offsets[2], object.workpackageId);
}

StaticFieldDefinitionValueDTO _staticFieldDefinitionValueDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = StaticFieldDefinitionValueDTO(
    translationKey: reader.readStringOrNull(offsets[0]),
    value: reader.readStringOrNull(offsets[1]),
    workpackageId: reader.readLongOrNull(offsets[2]),
  );
  object.id = id;
  return object;
}

P _staticFieldDefinitionValueDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _staticFieldDefinitionValueDTOGetId(StaticFieldDefinitionValueDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _staticFieldDefinitionValueDTOGetLinks(
    StaticFieldDefinitionValueDTO object) {
  return [];
}

void _staticFieldDefinitionValueDTOAttach(
    IsarCollection<dynamic> col, Id id, StaticFieldDefinitionValueDTO object) {
  object.id = id;
}

extension StaticFieldDefinitionValueDTOQueryWhereSort on QueryBuilder<
    StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO, QWhere> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension StaticFieldDefinitionValueDTOQueryWhere on QueryBuilder<
    StaticFieldDefinitionValueDTO,
    StaticFieldDefinitionValueDTO,
    QWhereClause> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension StaticFieldDefinitionValueDTOQueryFilter on QueryBuilder<
    StaticFieldDefinitionValueDTO,
    StaticFieldDefinitionValueDTO,
    QFilterCondition> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'translationKey',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'translationKey',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'translationKey',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
          QAfterFilterCondition>
      translationKeyContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'translationKey',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
          QAfterFilterCondition>
      translationKeyMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'translationKey',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'translationKey',
        value: '',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> translationKeyIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'translationKey',
        value: '',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
          QAfterFilterCondition>
      valueContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
          QAfterFilterCondition>
      valueMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'value',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workpackageId',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workpackageId',
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'workpackageId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension StaticFieldDefinitionValueDTOQueryObject on QueryBuilder<
    StaticFieldDefinitionValueDTO,
    StaticFieldDefinitionValueDTO,
    QFilterCondition> {}

extension StaticFieldDefinitionValueDTOQueryLinks on QueryBuilder<
    StaticFieldDefinitionValueDTO,
    StaticFieldDefinitionValueDTO,
    QFilterCondition> {}

extension StaticFieldDefinitionValueDTOQuerySortBy on QueryBuilder<
    StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO, QSortBy> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByTranslationKey() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'translationKey', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByTranslationKeyDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'translationKey', Sort.desc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> sortByWorkpackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.desc);
    });
  }
}

extension StaticFieldDefinitionValueDTOQuerySortThenBy on QueryBuilder<
    StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO, QSortThenBy> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByTranslationKey() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'translationKey', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByTranslationKeyDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'translationKey', Sort.desc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.asc);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QAfterSortBy> thenByWorkpackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.desc);
    });
  }
}

extension StaticFieldDefinitionValueDTOQueryWhereDistinct on QueryBuilder<
    StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO, QDistinct> {
  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QDistinct> distinctByTranslationKey({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'translationKey',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QDistinct> distinctByValue({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'value', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, StaticFieldDefinitionValueDTO,
      QDistinct> distinctByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'workpackageId');
    });
  }
}

extension StaticFieldDefinitionValueDTOQueryProperty on QueryBuilder<
    StaticFieldDefinitionValueDTO,
    StaticFieldDefinitionValueDTO,
    QQueryProperty> {
  QueryBuilder<StaticFieldDefinitionValueDTO, int, QQueryOperations>
      idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, String?, QQueryOperations>
      translationKeyProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'translationKey');
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, String?, QQueryOperations>
      valueProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'value');
    });
  }

  QueryBuilder<StaticFieldDefinitionValueDTO, int?, QQueryOperations>
      workpackageIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workpackageId');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StaticFieldDefinitionValueDTO _$StaticFieldDefinitionValueDTOFromJson(
        Map<String, dynamic> json) =>
    StaticFieldDefinitionValueDTO(
      workpackageId: json['workpackageId'] as int?,
      translationKey: json['translationKey'] as String?,
      value: json['value'] as String?,
    )..id = json['id'] as int;

Map<String, dynamic> _$StaticFieldDefinitionValueDTOToJson(
        StaticFieldDefinitionValueDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'workpackageId': instance.workpackageId,
      'translationKey': instance.translationKey,
      'value': instance.value,
    };
