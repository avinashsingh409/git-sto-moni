// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'field_definition_value_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetFieldDefinitionValueDTOCollection on Isar {
  IsarCollection<FieldDefinitionValueDTO> get fieldDefinitionValueDTOs =>
      this.collection();
}

const FieldDefinitionValueDTOSchema = CollectionSchema(
  name: r'FieldDefinitionValueDTO',
  id: 8252986872400953871,
  properties: {
    r'guid': PropertySchema(
      id: 0,
      name: r'guid',
      type: IsarType.string,
    ),
    r'isChanged': PropertySchema(
      id: 1,
      name: r'isChanged',
      type: IsarType.bool,
    ),
    r'value': PropertySchema(
      id: 2,
      name: r'value',
      type: IsarType.string,
    ),
    r'workpackageId': PropertySchema(
      id: 3,
      name: r'workpackageId',
      type: IsarType.long,
    )
  },
  estimateSize: _fieldDefinitionValueDTOEstimateSize,
  serialize: _fieldDefinitionValueDTOSerialize,
  deserialize: _fieldDefinitionValueDTODeserialize,
  deserializeProp: _fieldDefinitionValueDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _fieldDefinitionValueDTOGetId,
  getLinks: _fieldDefinitionValueDTOGetLinks,
  attach: _fieldDefinitionValueDTOAttach,
  version: '3.1.0+1',
);

int _fieldDefinitionValueDTOEstimateSize(
  FieldDefinitionValueDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.guid;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.value;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _fieldDefinitionValueDTOSerialize(
  FieldDefinitionValueDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.guid);
  writer.writeBool(offsets[1], object.isChanged);
  writer.writeString(offsets[2], object.value);
  writer.writeLong(offsets[3], object.workpackageId);
}

FieldDefinitionValueDTO _fieldDefinitionValueDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = FieldDefinitionValueDTO(
    guid: reader.readStringOrNull(offsets[0]),
    isChanged: reader.readBoolOrNull(offsets[1]),
    value: reader.readStringOrNull(offsets[2]),
    workpackageId: reader.readLongOrNull(offsets[3]),
  );
  object.id = id;
  return object;
}

P _fieldDefinitionValueDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readBoolOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _fieldDefinitionValueDTOGetId(FieldDefinitionValueDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _fieldDefinitionValueDTOGetLinks(
    FieldDefinitionValueDTO object) {
  return [];
}

void _fieldDefinitionValueDTOAttach(
    IsarCollection<dynamic> col, Id id, FieldDefinitionValueDTO object) {
  object.id = id;
}

extension FieldDefinitionValueDTOQueryWhereSort
    on QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QWhere> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension FieldDefinitionValueDTOQueryWhere on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QWhereClause> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension FieldDefinitionValueDTOQueryFilter on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QFilterCondition> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'guid',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
          QAfterFilterCondition>
      guidContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
          QAfterFilterCondition>
      guidMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'guid',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> guidIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> isChangedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isChanged',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> isChangedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isChanged',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> isChangedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isChanged',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
          QAfterFilterCondition>
      valueContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
          QAfterFilterCondition>
      valueMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'value',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> valueIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workpackageId',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workpackageId',
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'workpackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO,
      QAfterFilterCondition> workpackageIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'workpackageId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension FieldDefinitionValueDTOQueryObject on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QFilterCondition> {}

extension FieldDefinitionValueDTOQueryLinks on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QFilterCondition> {}

extension FieldDefinitionValueDTOQuerySortBy
    on QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QSortBy> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByGuid() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByGuidDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByIsChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      sortByWorkpackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.desc);
    });
  }
}

extension FieldDefinitionValueDTOQuerySortThenBy on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QSortThenBy> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByGuid() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByGuidDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByIsChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isChanged', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.asc);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QAfterSortBy>
      thenByWorkpackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workpackageId', Sort.desc);
    });
  }
}

extension FieldDefinitionValueDTOQueryWhereDistinct on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QDistinct> {
  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QDistinct>
      distinctByGuid({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'guid', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QDistinct>
      distinctByIsChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isChanged');
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QDistinct>
      distinctByValue({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'value', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, FieldDefinitionValueDTO, QDistinct>
      distinctByWorkpackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'workpackageId');
    });
  }
}

extension FieldDefinitionValueDTOQueryProperty on QueryBuilder<
    FieldDefinitionValueDTO, FieldDefinitionValueDTO, QQueryProperty> {
  QueryBuilder<FieldDefinitionValueDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, String?, QQueryOperations>
      guidProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'guid');
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, bool?, QQueryOperations>
      isChangedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isChanged');
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, String?, QQueryOperations>
      valueProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'value');
    });
  }

  QueryBuilder<FieldDefinitionValueDTO, int?, QQueryOperations>
      workpackageIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workpackageId');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FieldDefinitionValueDTO _$FieldDefinitionValueDTOFromJson(
        Map<String, dynamic> json) =>
    FieldDefinitionValueDTO(
      guid: json['guid'] as String?,
      workpackageId: json['workpackageId'] as int?,
      value: json['value'] as String?,
      isChanged: json['isChanged'] as bool?,
    )..id = json['id'] as int;

Map<String, dynamic> _$FieldDefinitionValueDTOToJson(
        FieldDefinitionValueDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'workpackageId': instance.workpackageId,
      'guid': instance.guid,
      'value': instance.value,
      'isChanged': instance.isChanged,
    };
