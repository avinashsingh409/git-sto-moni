import 'package:isar/isar.dart';
part 'contractor_options.g.dart';

@Collection()
class ContractorOptionsDTO {
  Id id; //
  String? name;

  /// ContractorOptions DTO
  ContractorOptionsDTO({
    required this.id,
    this.name,
  });
}
