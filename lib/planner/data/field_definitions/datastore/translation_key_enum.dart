enum TranslationKey {
  primarycontractor,
  worktype,
  workscopedescription,
  plannername,
  worklistid,
  worklistitemstatus,
  islocked,
  scoperequestnumber,
  sapworkorder,
  scopeapprovalcode,
  unitnumber,
  processsystems,
  equipmenttype,
  stosubtype,
  expensetype,
  none,
}
