import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/attachment_exception.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/repository/sync_ticker_repository.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/datastore/attachments_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/repository/attachments_repository.dart';

@LazySingleton(as: IAttachmentsRepository)
class AttachmentsRepositoryImpl extends IAttachmentsRepository {
  final IAttachmentsDataStore attachmentsDataStore;
  final ISyncTickerRepository syncTickerRepository;
  AttachmentsRepositoryImpl(this.attachmentsDataStore, this.syncTickerRepository);

  @override
  Future<List<AttachmentDTO>?> getAttachments() {
    return attachmentsDataStore.getAttachments();
  }

  @override
  Future<void> insertAllAttachments({required List<AttachmentDTO> attachmentDTOs}) {
    return attachmentsDataStore.insertAllAttachments(attachmentDTOs: attachmentDTOs);
  }

  @override
  Future<void> deleteAllAttachments() {
    return attachmentsDataStore.deleteAllAttachments();
  }

  @override
  BehaviorSubject<List<AttachmentDTO>> getCachedAttachmentByWorklistId() {
    return attachmentsDataStore.getCachedAttachmentByWorklistId(workpackageId: SelectedWorkPackage.workpackageDTO.id ?? 0);
  }

  @override
  Future<int> updateImage({required String updatedImageThumbnailBytes, required AttachmentDTO currentAttachment}) async {
    await syncTickerRepository.updateStatus();
    final updatedAttachment = currentAttachment.copyWith(updatedImageThumbnailBytes: updatedImageThumbnailBytes);
    return attachmentsDataStore.updateImage(updatedAttachment: updatedAttachment);
  }

  @override
  Future<void> insertNewImage({required String newImage64String, required String name}) async {
    await syncTickerRepository.updateStatus();
    final AttachmentDTO newAttachment = AttachmentDTO(
      isChanged: true,
      isNewlyAdded: true,
      contentType: "image/png",
      imageBytes: newImage64String,
      isImage: true,
      name: '${DateTime.now().millisecondsSinceEpoch.toString()}_${SelectedWorkPackage.workpackageDTO.id?.toString()}_$name.png',
      objectId: SelectedWorkPackage.workpackageDTO.id,
      dateCreated: DateTime.now().toIso8601String(),
    );
    return attachmentsDataStore.insertNewImage(newAttachment: newAttachment);
  }

  @override
  Future<Either<Unit, AttachmentException>> markAttachmentAsDelete({required AttachmentDTO attachmentDTO}) async {
    final bool? isNewlyAdded = attachmentDTO.isNewlyAdded;
    if (isNewlyAdded != null && isNewlyAdded) {
      await syncTickerRepository.updateStatus();
      await attachmentsDataStore.markAttachmentAsDelete(attachmentDTO: attachmentDTO);
      return const Left(unit);
    }
    return const Right(AttachmentException.unauthorized());
  }

  @override
  Future<List<AttachmentDTO>?> getAttachmentsToUpload() {
    return attachmentsDataStore.getAttachmentsToUpload();
  }

  @override
  Future<List<AttachmentDTO>?> getAllDeletedAttachments() {
    return attachmentsDataStore.getAllDeletedAttachments();
  }
}
