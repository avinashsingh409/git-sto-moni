import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';

@injectable
class AttachmentsDAO {
  final isar = PersistenceModule.isar;

  AttachmentsDAO();

  /// Isar Collection of AttachmentsDTOs
  late IsarCollection<AttachmentDTO> collection = isar.attachmentDTOs;

  // Delete All Attachments
  Future<void> deleteAllAttachments() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All AttachmentsDTOs
  Future<List<AttachmentDTO>> getAllAttachments() async {
    final attachments = collection.where().findAll();
    return attachments;
  }

  Future<List<AttachmentDTO>?> getAllDeletedAttachments() async {
    final attachments = collection.where().filter().isDeletedEqualTo(true).findAll();
    return attachments;
  }

  BehaviorSubject<List<AttachmentDTO>> getCachedAttachmentByWorklistId({required int workpackageId}) {
    final bstream = BehaviorSubject<List<AttachmentDTO>>();
    final query = collection.where().filter().objectIdEqualTo(workpackageId).and().not().isDeletedEqualTo(true).build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  Future<List<AttachmentDTO>?> getAttachmentsToUpload() async {
    final newAttachments = collection.where().filter().isNewlyAddedEqualTo(true).findAll();
    return newAttachments;
  }

  Future<void> deleteOrMarkAattachment({required AttachmentDTO attachmentDTO}) async {
    await isar.writeTxn(() async {
      await isar.attachmentDTOs.delete(attachmentDTO.id ?? 000000);
    });
  }

  /// Watch All Attachments
  BehaviorSubject<List<AttachmentDTO>> watchAllAttachments() {
    final bstream = BehaviorSubject<List<AttachmentDTO>>();
    final query = collection.where(sort: Sort.desc).anyId().build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  Future<int> updateAttachment({required AttachmentDTO updatedAttachment}) async {
    return isar.writeTxn(() async {
      return collection.put(updatedAttachment);
    });
  }

  /// Upsert All AttachmentDTOs
  Future<List<int>> upsertAllAttachments({
    required List<AttachmentDTO> attachments,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(attachments);
    });
  }

  /// Insert new AttachmentDTO
  Future<int> insertNewAttachment({
    required AttachmentDTO attachment,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(attachment);
    });
  }
}
