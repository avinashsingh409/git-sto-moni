import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'attachment_dto.g.dart';

@Collection()
@JsonSerializable()
class AttachmentDTO {
  Id? id; //
  int? objectId;
  String? name;
  String? contentType;
  int? authorId;
  int? fileSize;
  String? dateCreated;
  bool? isImage;
  bool? isDefault;
  String? imageBytes;

  //Booleans to track internal state
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool? isChanged;
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool? isNewlyAdded;
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool? isDeleted;

  /// Attachment DTO
  AttachmentDTO({
    this.id,
    this.objectId,
    this.name,
    this.contentType,
    this.authorId,
    this.fileSize,
    this.dateCreated,
    this.isImage,
    this.isDefault,
    this.imageBytes,
    this.isChanged = false,
    this.isNewlyAdded = false,
    this.isDeleted = false,
  });

  AttachmentDTO copyWith({
    required String updatedImageThumbnailBytes,
  }) {
    return AttachmentDTO(
      id: id,
      objectId: objectId,
      name: name,
      contentType: contentType,
      authorId: authorId,
      fileSize: fileSize,
      dateCreated: dateCreated,
      isImage: isImage,
      isDefault: isDefault,
      isNewlyAdded: isNewlyAdded,
      isChanged: true,
      isDeleted: isDeleted,
      imageBytes: updatedImageThumbnailBytes,
    );
  }

  AttachmentDTO updateDelete({
    required bool updatedIsDeleted,
  }) {
    return AttachmentDTO(
      id: id,
      objectId: objectId,
      name: name,
      contentType: contentType,
      authorId: authorId,
      fileSize: fileSize,
      dateCreated: dateCreated,
      isImage: isImage,
      isDefault: isDefault,
      isNewlyAdded: isNewlyAdded,
      isChanged: isChanged,
      isDeleted: updatedIsDeleted,
      imageBytes: imageBytes,
    );
  }

  factory AttachmentDTO.fromJson(Map<String, dynamic> json) => _$AttachmentDTOFromJson(json);
}
