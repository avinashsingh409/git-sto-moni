import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/planner/data/attachments/attachment_api_service.dart';
import 'package:sto_mobile_v2/planner/data/attachments/attachments_dao.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/datastore/attachments_datastore.dart';

@LazySingleton(as: IAttachmentsDataStore)
class AttachmentDataStoreImpl extends IAttachmentsDataStore {
  final AttachmentApiService attachmentApiService;
  final AttachmentsDAO attachmentsDAO;

  AttachmentDataStoreImpl(this.attachmentApiService, this.attachmentsDAO);

  @override
  Future<List<AttachmentDTO>?> getAttachments() {
    return attachmentApiService.getAttachments().catchDioException();
  }

  @override
  Future<void> insertAllAttachments({required List<AttachmentDTO> attachmentDTOs}) {
    return attachmentsDAO.upsertAllAttachments(attachments: attachmentDTOs);
  }

  @override
  Future<void> deleteAllAttachments() {
    return attachmentsDAO.deleteAllAttachments();
  }

  @override
  BehaviorSubject<List<AttachmentDTO>> getCachedAttachmentByWorklistId({required int workpackageId}) {
    return attachmentsDAO.getCachedAttachmentByWorklistId(workpackageId: workpackageId);
  }

  @override
  Future<int> updateImage({required AttachmentDTO updatedAttachment}) {
    return attachmentsDAO.updateAttachment(updatedAttachment: updatedAttachment);
  }

  @override
  Future<void> insertNewImage({required AttachmentDTO newAttachment}) {
    return attachmentsDAO.insertNewAttachment(attachment: newAttachment);
  }

  @override
  Future<void> markAttachmentAsDelete({required AttachmentDTO attachmentDTO}) {
    return attachmentsDAO.deleteOrMarkAattachment(attachmentDTO: attachmentDTO);
  }

  @override
  Future<List<AttachmentDTO>?> getAttachmentsToUpload() {
    return attachmentsDAO.getAttachmentsToUpload();
  }

  @override
  Future<List<AttachmentDTO>?> getAllDeletedAttachments() {
    return attachmentsDAO.getAllDeletedAttachments();
  }
}
