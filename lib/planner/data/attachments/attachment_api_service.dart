import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';

//Todo Remove this entire call
@injectable
class AttachmentApiService {
  final Dio dio;

  AttachmentApiService(this.dio);

  Future<List<AttachmentDTO>?> getAttachments() async {
    return [];
  }
}
