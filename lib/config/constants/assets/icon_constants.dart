const icons = "assets/icons";

const iconPrometheusLogo = "$icons/ic_prometheuslogo_white.svg";
const iconExecutionLogo = "$icons/ic_sto-execution-logo.svg";
const iconIsolationLogo = "$icons/ic_sto-isolation-logo.svg";
const iconstoLogo = "$icons/ic_sto-logo.svg";
const iconStoLogoPng = "$icons/ic_sto-logo.png";
const iconPlannerLogo = "$icons/ic_sto-planner-logo.svg";
