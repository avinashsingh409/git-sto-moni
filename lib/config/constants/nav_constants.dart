/*
 * Common app routes:
 * These routes are used in the core application before the features or to configure the features 
 */
const cmnSplashScreen = "/Splash";
const cmnAppConfigScreen = "/AppConfig";
const cmnServerCredentialsScreen = "/ServerCredentials";
const cmnServerQRCodeScreen = "/QRCode";
const cmnLoginScreen = "/Login";
const cmnTurnAroundEventScreen = '/TurnAroundEvent';
const cmnAuthenticationScreen = '/AuthenticationView';
const cmnTurnAroundDataCache = '/TurnAroundDataCache';
const cmnUserCall = '/UserCall';
const cmnErrorPage = '/ErrorPage';

/* 
 * Execution app routes:
 * These routes are used in the execution side of the app 
 */
const exeTurnAroundActivitiesCacheScreen = '/TurnAroundActivityCache';
const exeTurnAroundActivityDetailsScreen = '/TurnAroundActivityDetails';
const exeFilterConfigScreen = '/FilterConfig';
const exeWbsScreen = '/Grouping';

/*
 * Planner app routes:
 * These routes are used in the planner side of the app
 */
const plnWorkpackageDetailsScreen = "/WorkpackagesDetails";
const plnFieldDefinitionsScreen = '/FieldDefition';
const plnAnnotationScreen = '/AttachmentAnnotation';

/* 
 * Home screen after app selection for both planner and execution share the same route
 * but config_route.dart has a method that seperates the navigation according to the 
 * currently selectedAppFeature.
 */
const homeScreen = "/Home";
