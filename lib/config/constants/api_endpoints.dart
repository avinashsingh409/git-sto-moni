class ApiEndPoints {
  static const _baseUrlEnding = "/stomanager/api";
  // static const _baseUrlEnding = "/api";

  static const coreHealthCheck = "/turnaround-health/health";

  static const files = "$_baseUrlEnding/files";

  static String defaultAttachment(String objectType, int objectId) =>
      "$files/defaultAttachment?objectType=$objectType&objectId=$objectId";

  static String getImageAsBytes(int fileId) => "$files/getImageAsBytes/$fileId";

  static const sTOXActivities = "$_baseUrlEnding/PrimaveraActivities";

  static String activities(int page, int limit) =>
      "${ApiEndPoints.sTOXActivities}/mobile?page=$page&limit=$limit";

  static const updateSTOXActivities = "$_baseUrlEnding/PrimaveraActivities";
  static const stoxActivityFields = "$_baseUrlEnding/mobile/activity-fields";
  static const stoxBlockedActivities = "$updateSTOXActivities/block";
  static const stoxComments = "$_baseUrlEnding/comments";
  static const stoxCommentsMobile = "$_baseUrlEnding/mobile/comments";
  static const blockingReasons = "$_baseUrlEnding/blocking-reasons";

  static String comments(String hostname, String projectId, int page, int limit) =>
      "${ApiEndPoints.stoxCommentsMobile}/$hostname/project/$projectId?page=$page&limit=$limit";
  static const stoxDynamicFields = "$_baseUrlEnding/stoxdynamicField";

  static const stoxTurnaroundEvents = "$_baseUrlEnding/mobile/execution/TurnaroundEvents";

  static stoxTurnaroundEventsToggle({required bool inProgress}) =>
      "$_baseUrlEnding/stoxturnaroundevents/toggleEventUpdateStatuses/$inProgress";

  static const stoxWbsLevels = "$_baseUrlEnding/wbslevel";

  static getAssociations({required int eventId}) => "$_baseUrlEnding/stoxturnaroundevents/associations/$eventId";

  static toggleUserAssociation({
    required int turnaroundEventId,
    required int userId,
  }) =>
      "$_baseUrlEnding/StoxTurnaroundEvents/toggleUserAssociation/$turnaroundEventId/$userId";

  static const user = "$_baseUrlEnding/Users/me";

  static const userSettingsPost = "$_baseUrlEnding/UserSettings";

  static userSettingsGetByUser(int userId) => "$_baseUrlEnding/UserSettings/user/$userId";

  static userSettingsPatchDelete(int userSettingId) => "$_baseUrlEnding/UserSettings/$userSettingId";

  static const sTOXWbsLevels = "$_baseUrlEnding/wbslevel";

  static stackFilters({
    required String hostname, 
    required String projectId
  }) => "$_baseUrlEnding/StackFilters/mobile/$hostname/project/$projectId";

  // Planner Endpoints

  static getWorkpackages({required int eventId}) => "$_baseUrlEnding/mobileWorklistItem/$eventId";

  static pushDataToServer({required int eventId}) => "$_baseUrlEnding/mobileWorklistItem/$eventId";

  static getWorkpackagesGroupings({required int eventId}) => "$_baseUrlEnding/mobileWorklistFieldGroup/$eventId";

  //License
  static const license = "$_baseUrlEnding/mobileConfiguration";

  //Attachment
  static const attachmentSync = "$_baseUrlEnding/mobileWorklistItem/attachment";
}
