import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/common/data/app_switcher/datastore/enums/app_features.dart';
import 'package:sto_mobile_v2/common/domain/app_switcher/repository/app_switcher_repository.dart';
import 'package:sto_mobile_v2/common/screens/turnaround_data_cache/turnaround_data_cache.dart';
import 'package:sto_mobile_v2/common/screens/user_call/user_call_screen.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_switcher_error_page.dart';
import 'package:sto_mobile_v2/config/constants/nav_constants.dart';
import 'package:sto_mobile_v2/config/navigation/models/activity_details_screen_args.dart';
import 'package:sto_mobile_v2/config/navigation/models/field_definition_args.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/activity_details_screen.dart';
import 'package:sto_mobile_v2/common/screens/app_config/app_config_screen.dart';
import 'package:sto_mobile_v2/execution/screens/execution_home/execution_home_screen.dart';
import 'package:sto_mobile_v2/common/screens/login/authentication_view.dart';
import 'package:sto_mobile_v2/common/screens/login/login_screen.dart';
import 'package:sto_mobile_v2/common/screens/qr_code_scan/qr_code_scan_screen.dart';
import 'package:sto_mobile_v2/common/screens/server_credentials/server_credentials_screen.dart';
import 'package:sto_mobile_v2/common/screens/splash/splash_screen.dart';
import 'package:sto_mobile_v2/common/screens/turnaround_event_cache/turnaround_event_screen.dart';
import 'package:sto_mobile_v2/execution/screens/filter_config/filter_config_screen.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/wbs_grouping_screen.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/screens/field_definitions/field_definitions_screen.dart';
import 'package:sto_mobile_v2/planner/screens/image_annotation/image_annotation_screen.dart';
import 'package:sto_mobile_v2/planner/screens/planner_home/planner_home_screen.dart';
import 'package:sto_mobile_v2/planner/screens/workpackage_details/workpackage_details_screen.dart';

class RouteGenerator {
  static IAppSwitcherRepository get appSwitcherRepository => getIt<IAppSwitcherRepository>();
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      /*
         Common Route generation. These are the generated routes for common routenames
         that is used on the entire application flow.
        */
      case cmnSplashScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const SplashScreen()),
        );
      case cmnAppConfigScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const AppConfigScreen()),
        );
      case cmnServerCredentialsScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const ServerCredentialsScreen()),
        );
      case cmnServerQRCodeScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const QRCodeScanScreen()),
        );
      case cmnLoginScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => LoginScreen(
                isSessionRestoration: arguments as bool,
              )),
        );
      case cmnUserCall:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const UserCallScreen()),
        );
      case cmnTurnAroundEventScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const TurnAroundEventScreen()),
        );
      case cmnAuthenticationScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => AuthenticationView(
                url: arguments as String,
              )),
        );
      case cmnTurnAroundDataCache:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const TurnAroundDataCache()),
        );
      case cmnErrorPage:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const AppSwitcherErrorPage()),
        );

      /*
         Execution Route generation. These are the generated routes for common routenames
         that is used on the entire application flow.
        */
      case exeTurnAroundActivityDetailsScreen:
        final activityDetailsArgs = arguments as ActivityDetailsScreenArguments;
        return MaterialPageRoute(
          builder: ((BuildContext context) => ActivityDetailsScreen(
                activityId: activityDetailsArgs.activityId,
                activity: activityDetailsArgs.activity,
              )),
        );

      case exeFilterConfigScreen:
        return MaterialPageRoute(
          builder: ((BuildContext context) => const FilterConfigScreen()),
        );

      case exeWbsScreen:
        final wbsNode = arguments as TreeNodeDTO;
        return MaterialPageRoute(
          builder: ((BuildContext context) => WbsGroupingScreen(wbsNode: wbsNode))
        );

      /*
         Planner Route generation. These are the generated routes for planner routenames
         that is used on the entire application flow.
        */

      case plnWorkpackageDetailsScreen:
        final workpackage = arguments as WorkpackageDTO;
        return MaterialPageRoute(
          builder: ((BuildContext context) => WorkpackageDetailsScreen(
                workpackage: workpackage,
              )),
        );

      case plnFieldDefinitionsScreen:
        final fieldDefinitionArgs = arguments as FieldDefinitionScreenArguments;
        return MaterialPageRoute(
          builder: ((BuildContext context) => FieldDefinitionScreen(
                subgroupDTO: fieldDefinitionArgs.workpackageSubgroupDTO,
                dynamicColumns: fieldDefinitionArgs.dynamicColumns,
              )),
        );

      case plnAnnotationScreen:
        final attachmentDTO = arguments as AttachmentDTO;
        return MaterialPageRoute(
          builder: ((BuildContext context) => ImageAnnotationScreen(
                attachmentDTO: attachmentDTO,
              )),
        );

      /* 
          Home screen route configuration for planner and execution. The appswitcherRepository handles the
          cached/selected home screen for the current home screen to be rendered.
        */

      case homeScreen:
        final currentSelectedFeature = appSwitcherRepository.getSelectedApp();
        switch (currentSelectedFeature) {
          case AppFeatures.planner:
            return MaterialPageRoute(
              builder: ((BuildContext context) => const PlannerHomeScreen()),
            );
          case AppFeatures.execution:
            return MaterialPageRoute(
              builder: ((BuildContext context) => const ExecutionHomeScreen()),
            );
          case AppFeatures.notSelected:
            return MaterialPageRoute(
              builder: (error) {
                return const AppSwitcherErrorPage();
              },
            );
        }

      // Error Route
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (error) {
        return Scaffold(
          body: Center(
            child: Text(S.of(error).notFound),
          ),
        );
      },
    );
  }
}
