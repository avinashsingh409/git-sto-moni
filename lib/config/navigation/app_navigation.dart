import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/constants/nav_constants.dart';
import 'package:sto_mobile_v2/config/navigation/models/activity_details_screen_args.dart';
import 'package:sto_mobile_v2/config/navigation/models/field_definition_args.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';

// ----------------------------------------------------------------------------------------------------
// Common route navigation methods:
// Routes that are used in the entire application flow
// ----------------------------------------------------------------------------------------------------

void jumpToSplashScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnSplashScreen, (_) => false);
}

void jumpToAppConfigScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnAppConfigScreen, (_) => false);
}

void jumpToServerCredentialsScreen({required BuildContext context}) {
  Navigator.of(context).pushNamed(cmnServerCredentialsScreen);
}

void jumpToQRCodeScreen({required BuildContext context}) {
  Navigator.of(context).pushNamed(cmnServerQRCodeScreen);
}

void jumpToLoginScreen({required BuildContext context, required bool isSessionRestoration}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnLoginScreen, (_) => false, arguments: isSessionRestoration);
}

void jumpToAuthenticationScreen({required BuildContext context, required String url}) {
  Navigator.of(context).pushNamed(cmnAuthenticationScreen, arguments: url);
}

void jumpToUserCallScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnUserCall, (_) => false);
}

void jumpToConfigureTurnAroundEventScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnTurnAroundEventScreen, (_) => false);
}

void jumpToTurnAroundDataCacheScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnTurnAroundDataCache, (route) => false);
}

void jumpToErrorPage({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(cmnErrorPage, (_) => false);
}

// ----------------------------------------------------------------------------------------------------
// Execution route navigation methods:
// Routes that are used on the execution side of the app
// ----------------------------------------------------------------------------------------------------

void jumpToTurnAroundActivitiesDetailsScreen({required BuildContext context, required int activityId, required String activity}) {
  Navigator.of(context).pushNamed(
    exeTurnAroundActivityDetailsScreen,
    arguments: ActivityDetailsScreenArguments(activityId: activityId, activity: activity),
  );
}

void jumpToFilterConfigScreen({required BuildContext context}) {
  Navigator.of(context).pushNamed(exeFilterConfigScreen);
}

void jumpToWbsScreen({required BuildContext context, required TreeNodeDTO wbsNode}) {
  Navigator.of(context).pushNamed(exeWbsScreen, arguments: wbsNode);
}

// ----------------------------------------------------------------------------------------------------
// Planner route navigation methods:
// Routes that are used on the planner side of the app
// ----------------------------------------------------------------------------------------------------

void jumpToWorkpackagesDetailsScreen({required BuildContext context, required WorkpackageDTO workpackageDTO}) {
  Navigator.of(context).pushNamed(plnWorkpackageDetailsScreen, arguments: workpackageDTO);
}

void jumpToFieldDefinitionScreen({required BuildContext context, required FieldDefinitionScreenArguments fieldDefinitionArgs}) {
  Navigator.of(context).pushNamed(plnFieldDefinitionsScreen, arguments: fieldDefinitionArgs);
}

void jumpToImageAnnotationScreen({required BuildContext context, required AttachmentDTO attachmentDTO}) {
  Navigator.of(context).pushNamed(plnAnnotationScreen, arguments: attachmentDTO);
}

// ----------------------------------------------------------------------------------------------------
// Home Screen route navigation method:
// Route that goes to the correct home page, as configured in config_route.dart
// ----------------------------------------------------------------------------------------------------

void jumpToHomeScreen({required BuildContext context}) {
  Navigator.of(context).pushNamedAndRemoveUntil(homeScreen, (_) => false);
}
