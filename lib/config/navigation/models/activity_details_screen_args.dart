class ActivityDetailsScreenArguments {
  final int activityId;
  final String activity;

  ActivityDetailsScreenArguments({required this.activityId, required this.activity});
}
