import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackage_subgroup_dto.dart';

class FieldDefinitionScreenArguments {
  final WorkpackageSubgroupDTO workpackageSubgroupDTO;
  final String? dynamicColumns;

  FieldDefinitionScreenArguments({required this.workpackageSubgroupDTO, required this.dynamicColumns});
}
