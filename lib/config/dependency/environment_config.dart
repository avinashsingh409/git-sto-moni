import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvironmentConfig {
  static Future<void> get development async => dotenv.load(fileName: ".env");
  static Future<void> get production async => dotenv.load(fileName: ".env");

  static Future intialize() async {
    if (kReleaseMode) {
      await EnvironmentConfig.production;
    } else {
      await EnvironmentConfig.development;
    }

    log("Environment loaded : ${dotenv.env}");
  }
}
