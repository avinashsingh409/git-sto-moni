import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/core/logger/debug_tree.dart';
import 'package:sto_mobile_v2/core/logger/log.dart';

import 'dependencies.dart';

@LazySingleton(as: Dependencies, env: [Environment.prod])
class ProductionDependenciesImpl extends Dependencies {
  @override
  Future<void> plantLogTrees() async {
    Log.plantTree(DebugTree());
  }
}
