import 'dart:math';

import 'package:flutter/material.dart';

class AppTheme {
  static const pgPrimaryRed = Color.fromRGBO(185, 23, 23, 1);

  static const pgPrimaryBlue = Color.fromRGBO(41, 122, 185, 1);

  static const pgPrimaryBlack = Color.fromRGBO(50, 50, 50, 1);

  static const pgPrimaryGray = Color.fromRGBO(196, 196, 196, 1);
  static const pgPrimaryLightGray = Color.fromRGBO(241, 241, 241, 1);
  static const pgPrimaryDarkGray = Color.fromRGBO(112, 112, 112, 1);

  static const pgAccentSuccess = Color.fromRGBO(68, 167, 21, 1);
  static const pgAccentSuccessTint = Color.fromRGBO(143, 202, 115, 1);

  static const pgAccentWarning = Color.fromRGBO(226, 125, 10, 1);
  static const pgAccentWarningTint = Color.fromRGBO(247, 194, 146, 1);

  static const pgAccentError = Color.fromRGBO(243, 47, 47, 1);
  static const pgAccentErrorTint90 = Color.fromRGBO(254, 234, 234, 1);

  static const pgAccentInformation = Color.fromRGBO(93, 145, 223, 1);
  ThemeData get mainTheme => ThemeData(
    primarySwatch: generateMaterialColor(pgPrimaryRed),
    unselectedWidgetColor: HexColor("#E9F3FF"),
    focusColor: const Color.fromRGBO(41, 122, 185, 1),
    backgroundColor: const Color.fromRGBO(234, 242, 248, 1),
    errorColor: Colors.grey.shade200,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        backgroundColor: pgPrimaryBlue,
      ),
    ),
  );

  MaterialColor generateMaterialColor(Color color) {
    return MaterialColor(color.value, {
      50: tintColor(color, 0.9),
      100: tintColor(color, 0.8),
      200: tintColor(color, 0.6),
      300: tintColor(color, 0.4),
      400: tintColor(color, 0.2),
      500: color,
      600: shadeColor(color, 0.1),
      700: shadeColor(color, 0.2),
      800: shadeColor(color, 0.3),
      900: shadeColor(color, 0.4),
    });
  }

  int tintValue(int value, double factor) => max(0, min((value + ((255 - value) * factor)).round(), 255));

  Color tintColor(Color color, double factor) =>
      Color.fromRGBO(tintValue(color.red, factor), tintValue(color.green, factor), tintValue(color.blue, factor), 1);

  int shadeValue(int value, double factor) => max(0, min(value - (value * factor).round(), 255));

  Color shadeColor(Color color, double factor) =>
      Color.fromRGBO(shadeValue(color.red, factor), shadeValue(color.green, factor), shadeValue(color.blue, factor), 1);

  // static const TextTheme _textTheme = TextTheme(
  //   headline1: screenHeadingStyleOne,
  //   headline2: screenHeadingStyleTwo,
  //   headline3: screenHeadingStyleThree,
  //   headline4: screenHeadingStyleThree,
  //   headline5: screenHeadingStyleThree,
  //   headline6: screenHeadingStyleSix, //app bar text style
  //   subtitle1: screenSubtitleStyle,
  //   subtitle2: screenSubtitleStyleTwo,
  //   bodyText1: screenBodyStyle,
  //   bodyText2: screenBodyStyleTwo,
  //   button: screenButtonStyle,
  //   caption: screenCaptionStyle,
  //   overline: screenOverlineStyle,
  // );

  // static TextTheme textThemeWhiteColor = TextTheme(
  //   headline1: screenHeadingStyleOne.copyWith(color: onSecondaryColor),
  //   headline2: screenHeadingStyleTwo.copyWith(color: onSecondaryColor),
  //   headline3: screenHeadingStyleThree.copyWith(color: onSecondaryColor),
  //   headline4: screenHeadingStyleThree.copyWith(color: onSecondaryColor),
  //   headline5: screenHeadingStyleThree.copyWith(color: onSecondaryColor),
  //   headline6: screenHeadingStyleSix.copyWith(
  //     color: onSecondaryColor,
  //   ), //app bar text style
  //   subtitle1: screenSubtitleStyle.copyWith(color: onSecondaryColor),
  //   subtitle2: screenSubtitleStyleTwo.copyWith(color: onSecondaryColor),
  //   bodyText1: screenBodyStyle.copyWith(color: onSecondaryColor),
  //   bodyText2: screenBodyStyleTwo.copyWith(color: onSecondaryColor),
  //   button: screenButtonStyle.copyWith(color: onSecondaryColor),
  //   caption: screenCaptionStyle.copyWith(color: onSecondaryColor),
  //   overline: screenOverlineStyle.copyWith(color: onSecondaryColor),
  // );

  // static const TextStyle screenHeadingStyleOne = TextStyle(
  //   fontSize: 40,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.bold,
  //   letterSpacing: -0.15,
  //   height: 1.25,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenHeadingStyleTwo = TextStyle(
  //   fontSize: 20,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w600,
  //   height: 1.4,
  //   letterSpacing: -0.1,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenHeadingStyleThree = TextStyle(
  //   fontSize: 16,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w600,
  //   height: 1.1,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenHeadingStyleSix = TextStyle(
  //   fontSize: 14,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w600,
  //   letterSpacing: -0.1,
  //   height: 1.25,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenHeadingErrorStyle = TextStyle(
  //   fontSize: 35,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.bold,
  //   letterSpacing: -0.15,
  //   height: 1.25,
  //   color: Colors.black87,
  // );

  // static const TextStyle screenSubtitleStyle = TextStyle(
  //   fontSize: 16,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w500,
  //   letterSpacing: 0,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenSubtitleStyleTwo = TextStyle(
  //   fontSize: 14,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w500,
  //   letterSpacing: 0,
  //   color: onPrimaryColor,
  // );
  // static const TextStyle screenBodyStyle = TextStyle(
  //   fontSize: 16,
  //   fontWeight: FontWeight.normal,
  //   height: 1.5,
  //   letterSpacing: 0,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenBodyStyleTwo = TextStyle(
  //   fontSize: 14,
  //   fontWeight: FontWeight.normal,
  //   height: 1.5,
  //   letterSpacing: 0,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenButtonStyle = TextStyle(
  //   fontSize: 16,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.w600,
  //   height: 1.5,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenCaptionStyle = TextStyle(
  //   fontSize: 12.0,
  //   fontWeight: FontWeight.w500,
  //   height: 1.16,
  //   color: onPrimaryColor,
  // );

  // static const TextStyle screenOverlineStyle = TextStyle(
  //   fontSize: 10.0,
  //   fontFamily: fontNameTrajanBold,
  //   fontWeight: FontWeight.normal,
  //   height: 1.5,
  //   letterSpacing: 0,
  //   color: onPrimaryColor,
  // );
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 8) {
      hexColor = hexColor.replaceRange(0, 1, "FF");
    }
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
