import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/config_route.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/splash/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // late StreamSubscription sub;

  @override
  Widget build(BuildContext context) {
    //Todo move this part in a module
    // Future<void> initUniLinks() async {
    //   sub = linkStream.listen((String? link) {
    //     Log.d(link.toString());
    //     if (link == 'com.prometheus.mob.sto://login-callback') {
    //       jumpToSplashScreen(context: GlobalNavigationKey().context!);
    //     }
    //   }, onError: (err) {});
    // }

    // initUniLinks();
    return MaterialApp(
      supportedLocales: const [
        Locale('en', ''),
        Locale('en', 'GB'),
        Locale('nl', ''),
      ],
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        AppLocalizationDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      onGenerateRoute: RouteGenerator.generateRoute,
      home: const SplashScreen(),
      title: 'STO Mobile',
      navigatorKey: GlobalNavigationKey().navigatorKey,
      theme: AppTheme().mainTheme,
    );
  }
}
