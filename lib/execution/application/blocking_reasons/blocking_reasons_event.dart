part of 'blocking_reasons_bloc.dart';

@freezed
class BlockingReasonsEvent with _$BlockingReasonsEvent {
  const factory BlockingReasonsEvent.getAllReasons() = _GetAllReasons;
  const factory BlockingReasonsEvent.addReason({required BlockingReasonDTO reason}) = _AddReason;
  const factory BlockingReasonsEvent.removeReason({required BlockingReasonDTO reason}) = _RemoveReason;
}