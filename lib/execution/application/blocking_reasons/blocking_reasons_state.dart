part of 'blocking_reasons_bloc.dart';

@freezed
class BlockingReasonsState with _$BlockingReasonsState {
  const factory BlockingReasonsState.initial() = _Initial;
  const factory BlockingReasonsState.loading() = _Loading;
  const factory BlockingReasonsState.success({required List<BlockingReasonDTO> reasons}) = _Success;
  const factory BlockingReasonsState.empty() = _Empty;
}
