// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'blocking_reasons_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$BlockingReasonsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllReasons,
    required TResult Function(BlockingReasonDTO reason) addReason,
    required TResult Function(BlockingReasonDTO reason) removeReason,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllReasons,
    TResult? Function(BlockingReasonDTO reason)? addReason,
    TResult? Function(BlockingReasonDTO reason)? removeReason,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllReasons,
    TResult Function(BlockingReasonDTO reason)? addReason,
    TResult Function(BlockingReasonDTO reason)? removeReason,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllReasons value) getAllReasons,
    required TResult Function(_AddReason value) addReason,
    required TResult Function(_RemoveReason value) removeReason,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllReasons value)? getAllReasons,
    TResult? Function(_AddReason value)? addReason,
    TResult? Function(_RemoveReason value)? removeReason,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllReasons value)? getAllReasons,
    TResult Function(_AddReason value)? addReason,
    TResult Function(_RemoveReason value)? removeReason,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BlockingReasonsEventCopyWith<$Res> {
  factory $BlockingReasonsEventCopyWith(BlockingReasonsEvent value,
          $Res Function(BlockingReasonsEvent) then) =
      _$BlockingReasonsEventCopyWithImpl<$Res, BlockingReasonsEvent>;
}

/// @nodoc
class _$BlockingReasonsEventCopyWithImpl<$Res,
        $Val extends BlockingReasonsEvent>
    implements $BlockingReasonsEventCopyWith<$Res> {
  _$BlockingReasonsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetAllReasonsCopyWith<$Res> {
  factory _$$_GetAllReasonsCopyWith(
          _$_GetAllReasons value, $Res Function(_$_GetAllReasons) then) =
      __$$_GetAllReasonsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetAllReasonsCopyWithImpl<$Res>
    extends _$BlockingReasonsEventCopyWithImpl<$Res, _$_GetAllReasons>
    implements _$$_GetAllReasonsCopyWith<$Res> {
  __$$_GetAllReasonsCopyWithImpl(
      _$_GetAllReasons _value, $Res Function(_$_GetAllReasons) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetAllReasons implements _GetAllReasons {
  const _$_GetAllReasons();

  @override
  String toString() {
    return 'BlockingReasonsEvent.getAllReasons()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetAllReasons);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllReasons,
    required TResult Function(BlockingReasonDTO reason) addReason,
    required TResult Function(BlockingReasonDTO reason) removeReason,
  }) {
    return getAllReasons();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllReasons,
    TResult? Function(BlockingReasonDTO reason)? addReason,
    TResult? Function(BlockingReasonDTO reason)? removeReason,
  }) {
    return getAllReasons?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllReasons,
    TResult Function(BlockingReasonDTO reason)? addReason,
    TResult Function(BlockingReasonDTO reason)? removeReason,
    required TResult orElse(),
  }) {
    if (getAllReasons != null) {
      return getAllReasons();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllReasons value) getAllReasons,
    required TResult Function(_AddReason value) addReason,
    required TResult Function(_RemoveReason value) removeReason,
  }) {
    return getAllReasons(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllReasons value)? getAllReasons,
    TResult? Function(_AddReason value)? addReason,
    TResult? Function(_RemoveReason value)? removeReason,
  }) {
    return getAllReasons?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllReasons value)? getAllReasons,
    TResult Function(_AddReason value)? addReason,
    TResult Function(_RemoveReason value)? removeReason,
    required TResult orElse(),
  }) {
    if (getAllReasons != null) {
      return getAllReasons(this);
    }
    return orElse();
  }
}

abstract class _GetAllReasons implements BlockingReasonsEvent {
  const factory _GetAllReasons() = _$_GetAllReasons;
}

/// @nodoc
abstract class _$$_AddReasonCopyWith<$Res> {
  factory _$$_AddReasonCopyWith(
          _$_AddReason value, $Res Function(_$_AddReason) then) =
      __$$_AddReasonCopyWithImpl<$Res>;
  @useResult
  $Res call({BlockingReasonDTO reason});
}

/// @nodoc
class __$$_AddReasonCopyWithImpl<$Res>
    extends _$BlockingReasonsEventCopyWithImpl<$Res, _$_AddReason>
    implements _$$_AddReasonCopyWith<$Res> {
  __$$_AddReasonCopyWithImpl(
      _$_AddReason _value, $Res Function(_$_AddReason) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? reason = null,
  }) {
    return _then(_$_AddReason(
      reason: null == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as BlockingReasonDTO,
    ));
  }
}

/// @nodoc

class _$_AddReason implements _AddReason {
  const _$_AddReason({required this.reason});

  @override
  final BlockingReasonDTO reason;

  @override
  String toString() {
    return 'BlockingReasonsEvent.addReason(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddReason &&
            (identical(other.reason, reason) || other.reason == reason));
  }

  @override
  int get hashCode => Object.hash(runtimeType, reason);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AddReasonCopyWith<_$_AddReason> get copyWith =>
      __$$_AddReasonCopyWithImpl<_$_AddReason>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllReasons,
    required TResult Function(BlockingReasonDTO reason) addReason,
    required TResult Function(BlockingReasonDTO reason) removeReason,
  }) {
    return addReason(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllReasons,
    TResult? Function(BlockingReasonDTO reason)? addReason,
    TResult? Function(BlockingReasonDTO reason)? removeReason,
  }) {
    return addReason?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllReasons,
    TResult Function(BlockingReasonDTO reason)? addReason,
    TResult Function(BlockingReasonDTO reason)? removeReason,
    required TResult orElse(),
  }) {
    if (addReason != null) {
      return addReason(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllReasons value) getAllReasons,
    required TResult Function(_AddReason value) addReason,
    required TResult Function(_RemoveReason value) removeReason,
  }) {
    return addReason(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllReasons value)? getAllReasons,
    TResult? Function(_AddReason value)? addReason,
    TResult? Function(_RemoveReason value)? removeReason,
  }) {
    return addReason?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllReasons value)? getAllReasons,
    TResult Function(_AddReason value)? addReason,
    TResult Function(_RemoveReason value)? removeReason,
    required TResult orElse(),
  }) {
    if (addReason != null) {
      return addReason(this);
    }
    return orElse();
  }
}

abstract class _AddReason implements BlockingReasonsEvent {
  const factory _AddReason({required final BlockingReasonDTO reason}) =
      _$_AddReason;

  BlockingReasonDTO get reason;
  @JsonKey(ignore: true)
  _$$_AddReasonCopyWith<_$_AddReason> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RemoveReasonCopyWith<$Res> {
  factory _$$_RemoveReasonCopyWith(
          _$_RemoveReason value, $Res Function(_$_RemoveReason) then) =
      __$$_RemoveReasonCopyWithImpl<$Res>;
  @useResult
  $Res call({BlockingReasonDTO reason});
}

/// @nodoc
class __$$_RemoveReasonCopyWithImpl<$Res>
    extends _$BlockingReasonsEventCopyWithImpl<$Res, _$_RemoveReason>
    implements _$$_RemoveReasonCopyWith<$Res> {
  __$$_RemoveReasonCopyWithImpl(
      _$_RemoveReason _value, $Res Function(_$_RemoveReason) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? reason = null,
  }) {
    return _then(_$_RemoveReason(
      reason: null == reason
          ? _value.reason
          : reason // ignore: cast_nullable_to_non_nullable
              as BlockingReasonDTO,
    ));
  }
}

/// @nodoc

class _$_RemoveReason implements _RemoveReason {
  const _$_RemoveReason({required this.reason});

  @override
  final BlockingReasonDTO reason;

  @override
  String toString() {
    return 'BlockingReasonsEvent.removeReason(reason: $reason)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RemoveReason &&
            (identical(other.reason, reason) || other.reason == reason));
  }

  @override
  int get hashCode => Object.hash(runtimeType, reason);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RemoveReasonCopyWith<_$_RemoveReason> get copyWith =>
      __$$_RemoveReasonCopyWithImpl<_$_RemoveReason>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAllReasons,
    required TResult Function(BlockingReasonDTO reason) addReason,
    required TResult Function(BlockingReasonDTO reason) removeReason,
  }) {
    return removeReason(reason);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAllReasons,
    TResult? Function(BlockingReasonDTO reason)? addReason,
    TResult? Function(BlockingReasonDTO reason)? removeReason,
  }) {
    return removeReason?.call(reason);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAllReasons,
    TResult Function(BlockingReasonDTO reason)? addReason,
    TResult Function(BlockingReasonDTO reason)? removeReason,
    required TResult orElse(),
  }) {
    if (removeReason != null) {
      return removeReason(reason);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAllReasons value) getAllReasons,
    required TResult Function(_AddReason value) addReason,
    required TResult Function(_RemoveReason value) removeReason,
  }) {
    return removeReason(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAllReasons value)? getAllReasons,
    TResult? Function(_AddReason value)? addReason,
    TResult? Function(_RemoveReason value)? removeReason,
  }) {
    return removeReason?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAllReasons value)? getAllReasons,
    TResult Function(_AddReason value)? addReason,
    TResult Function(_RemoveReason value)? removeReason,
    required TResult orElse(),
  }) {
    if (removeReason != null) {
      return removeReason(this);
    }
    return orElse();
  }
}

abstract class _RemoveReason implements BlockingReasonsEvent {
  const factory _RemoveReason({required final BlockingReasonDTO reason}) =
      _$_RemoveReason;

  BlockingReasonDTO get reason;
  @JsonKey(ignore: true)
  _$$_RemoveReasonCopyWith<_$_RemoveReason> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$BlockingReasonsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BlockingReasonDTO> reasons) success,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BlockingReasonDTO> reasons)? success,
    TResult? Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BlockingReasonDTO> reasons)? success,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BlockingReasonsStateCopyWith<$Res> {
  factory $BlockingReasonsStateCopyWith(BlockingReasonsState value,
          $Res Function(BlockingReasonsState) then) =
      _$BlockingReasonsStateCopyWithImpl<$Res, BlockingReasonsState>;
}

/// @nodoc
class _$BlockingReasonsStateCopyWithImpl<$Res,
        $Val extends BlockingReasonsState>
    implements $BlockingReasonsStateCopyWith<$Res> {
  _$BlockingReasonsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$BlockingReasonsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'BlockingReasonsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BlockingReasonDTO> reasons) success,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BlockingReasonDTO> reasons)? success,
    TResult? Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BlockingReasonDTO> reasons)? success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements BlockingReasonsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$BlockingReasonsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'BlockingReasonsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BlockingReasonDTO> reasons) success,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BlockingReasonDTO> reasons)? success,
    TResult? Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BlockingReasonDTO> reasons)? success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements BlockingReasonsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<BlockingReasonDTO> reasons});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$BlockingReasonsStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? reasons = null,
  }) {
    return _then(_$_Success(
      reasons: null == reasons
          ? _value._reasons
          : reasons // ignore: cast_nullable_to_non_nullable
              as List<BlockingReasonDTO>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success({required final List<BlockingReasonDTO> reasons})
      : _reasons = reasons;

  final List<BlockingReasonDTO> _reasons;
  @override
  List<BlockingReasonDTO> get reasons {
    if (_reasons is EqualUnmodifiableListView) return _reasons;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_reasons);
  }

  @override
  String toString() {
    return 'BlockingReasonsState.success(reasons: $reasons)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._reasons, _reasons));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_reasons));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BlockingReasonDTO> reasons) success,
    required TResult Function() empty,
  }) {
    return success(reasons);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BlockingReasonDTO> reasons)? success,
    TResult? Function()? empty,
  }) {
    return success?.call(reasons);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BlockingReasonDTO> reasons)? success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(reasons);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements BlockingReasonsState {
  const factory _Success({required final List<BlockingReasonDTO> reasons}) =
      _$_Success;

  List<BlockingReasonDTO> get reasons;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EmptyCopyWith<$Res> {
  factory _$$_EmptyCopyWith(_$_Empty value, $Res Function(_$_Empty) then) =
      __$$_EmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyCopyWithImpl<$Res>
    extends _$BlockingReasonsStateCopyWithImpl<$Res, _$_Empty>
    implements _$$_EmptyCopyWith<$Res> {
  __$$_EmptyCopyWithImpl(_$_Empty _value, $Res Function(_$_Empty) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Empty implements _Empty {
  const _$_Empty();

  @override
  String toString() {
    return 'BlockingReasonsState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Empty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<BlockingReasonDTO> reasons) success,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<BlockingReasonDTO> reasons)? success,
    TResult? Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<BlockingReasonDTO> reasons)? success,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Empty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Empty value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Empty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class _Empty implements BlockingReasonsState {
  const factory _Empty() = _$_Empty;
}
