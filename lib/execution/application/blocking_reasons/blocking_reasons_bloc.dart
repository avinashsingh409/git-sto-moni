import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/domain/blocking_reason/repository/blocking_reason_repository.dart';

part 'blocking_reasons_event.dart';
part 'blocking_reasons_state.dart';
part 'blocking_reasons_bloc.freezed.dart';

@injectable
class BlockingReasonsBloc extends Bloc<BlockingReasonsEvent, BlockingReasonsState> {
  final IBlockingReasonRepository blockingReasonRepository;
  final List<BlockingReasonDTO> selectedReasons = [];

  final _blockReasonsSubject = BehaviorSubject<List<BlockingReasonDTO>>.seeded([]);
  Stream<List<BlockingReasonDTO>> getBlockingReasons() => _blockReasonsSubject.stream;
  List<BlockingReasonDTO> reasons = [];

  BlockingReasonsBloc(this.blockingReasonRepository) : super(const _Initial()) {
    on<BlockingReasonsEvent>((event, emit) async {
      await event.when(getAllReasons: () async {
        emit(const BlockingReasonsState.loading());
        final reasons = await blockingReasonRepository.getCachedBlockingReasons();
        if (reasons != null) {
          emit(BlockingReasonsState.success(reasons: reasons));
        } else {
          emit(const BlockingReasonsState.empty());
        }
      }, addReason: (BlockingReasonDTO reason) {
        if (!reasons.contains(reason)) {
          reasons.add(reason);
          _blockReasonsSubject.sink.add(reasons);
        }
      }, removeReason: (BlockingReasonDTO reason) {
        reasons.remove(reason);
        _blockReasonsSubject.sink.add(reasons);
      });
    });

    add(const BlockingReasonsEvent.getAllReasons());
  }

  void dispose() {
    _blockReasonsSubject.close();
  }
}
