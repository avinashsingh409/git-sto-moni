import 'dart:collection';

import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_bread_crumb_entry_item.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';

part 'multi_level_grouping_event.dart';
part 'multi_level_grouping_state.dart';
part 'multi_level_grouping_bloc.freezed.dart';

@injectable
class MultiLevelGroupingBloc extends Bloc<MultiLevelGroupingEvent, MultiLevelGroupingState> {
  late List<GroupingTreeNodeDTO>? groupData;
  final IActivityGroupingRepository activityGroupingRepository;
  var selectedNodes = LinkedList<GroupingBreadCrumbEntryItem>();
  MultiLevelGroupingBloc(this.activityGroupingRepository) : super(const _Initial()) {
    on<MultiLevelGroupingEvent>((event, emit) async {
      await event.when(getInitialGrouping: (groupableFields, isPrevGroupAvailable) async {
        emit(const MultiLevelGroupingState.loading());
        if (isPrevGroupAvailable) {
          final groupingData = await activityGroupingRepository.getCachedGroupData();
          groupData = groupingData;
          emit(MultiLevelGroupingState.success(groups: groupingData.where((element) => element.level == 0).toList()));
          _addBaseLevelBreadCrumb();
        } else {
          List<ActivityFieldDTO> groupFieldList = [];
          if (groupableFields != null) {
            for (var element in groupableFields.entries) {
              groupFieldList.add(element.value);
            }
            switch (groupFieldList.length) {
              case 1:
                final level1 = await activityGroupingRepository.getLevel1Grouping(activityField: groupFieldList[0]);
                level1.fold((groups) {
                  groupData = groups;
                  _addBaseLevelBreadCrumb();
                  emit(MultiLevelGroupingState.success(groups: groups?.where((element) => element.level == 0).toList() ?? []));
                }, (noData) => null);
                break;
              case 2:
                final level2 = await activityGroupingRepository.getLevel2Grouping(activityFields: groupFieldList);
                level2.fold((groups) {
                  groupData = groups;
                  _addBaseLevelBreadCrumb();
                  emit(MultiLevelGroupingState.success(groups: groups?.where((element) => element.level == 0).toList() ?? []));
                }, (noData) => null);
                break;
              case 3:
                final level3 = await activityGroupingRepository.getLevel3Grouping(groupingFields: groupFieldList);
                level3.fold((groups) {
                  groupData = groups;
                  _addBaseLevelBreadCrumb();
                  emit(MultiLevelGroupingState.success(groups: groups?.where((element) => element.level == 0).toList() ?? []));
                }, (noData) => null);
            }
          }
        }
      }, getNodeOrActivities: (GroupingTreeNodeDTO currentNode) {
        selectedNodes.add(GroupingBreadCrumbEntryItem(groupingTreeNodeDTO: currentNode));
        emit(const MultiLevelGroupingState.loading());
        final groupChildrens = groupData?.where((element) => element.nodeParent == currentNode.nodeValue).toList();
        emit(MultiLevelGroupingState.success(groups: groupChildrens ?? []));
      });
    });
  }

  void _addBaseLevelBreadCrumb() {
    if (groupData != null && groupData != []) {
      final baseLevelRoot = groupData?.where((element) => element.level == -1).firstOrNull;
      if (baseLevelRoot != null) {
        selectedNodes.addFirst(GroupingBreadCrumbEntryItem(groupingTreeNodeDTO: baseLevelRoot));
      }
    }
  }

  void returnNewLinkedListAfterKId(GroupingBreadCrumbEntryItem selectedBreadCrumb) {
    var newLinkedList = LinkedList<GroupingBreadCrumbEntryItem>();
    for (GroupingBreadCrumbEntryItem item in selectedNodes) {
      if (item != selectedBreadCrumb) {
        newLinkedList.add(GroupingBreadCrumbEntryItem(groupingTreeNodeDTO: item.groupingTreeNodeDTO));
      } else {
        break;
      }
    }
    selectedNodes.clear();
    selectedNodes = newLinkedList;
  }
}
