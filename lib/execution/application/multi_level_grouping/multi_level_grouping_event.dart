part of 'multi_level_grouping_bloc.dart';

@freezed
class MultiLevelGroupingEvent with _$MultiLevelGroupingEvent {
  const factory MultiLevelGroupingEvent.getInitialGrouping({
    required Map<int, ActivityFieldDTO>? groupKeys,
    required bool isPrevGroupAvailable,
  }) = _GetInitialGrouping;
  const factory MultiLevelGroupingEvent.getNodeOrActivities({required GroupingTreeNodeDTO currentNode}) = _GetNodeOrActivities;
}
