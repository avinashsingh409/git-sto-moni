part of 'multi_level_grouping_bloc.dart';

@freezed
class MultiLevelGroupingState with _$MultiLevelGroupingState {
  const factory MultiLevelGroupingState.initial() = _Initial;
  const factory MultiLevelGroupingState.loading() = _Loading;
  const factory MultiLevelGroupingState.success({required List<GroupingTreeNodeDTO> groups}) = _Success;
  const factory MultiLevelGroupingState.error() = _Error;
}
