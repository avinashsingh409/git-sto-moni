// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'multi_level_grouping_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MultiLevelGroupingEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        getInitialGrouping,
    required TResult Function(GroupingTreeNodeDTO currentNode)
        getNodeOrActivities,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult? Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetInitialGrouping value) getInitialGrouping,
    required TResult Function(_GetNodeOrActivities value) getNodeOrActivities,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult? Function(_GetNodeOrActivities value)? getNodeOrActivities,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult Function(_GetNodeOrActivities value)? getNodeOrActivities,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultiLevelGroupingEventCopyWith<$Res> {
  factory $MultiLevelGroupingEventCopyWith(MultiLevelGroupingEvent value,
          $Res Function(MultiLevelGroupingEvent) then) =
      _$MultiLevelGroupingEventCopyWithImpl<$Res, MultiLevelGroupingEvent>;
}

/// @nodoc
class _$MultiLevelGroupingEventCopyWithImpl<$Res,
        $Val extends MultiLevelGroupingEvent>
    implements $MultiLevelGroupingEventCopyWith<$Res> {
  _$MultiLevelGroupingEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetInitialGroupingCopyWith<$Res> {
  factory _$$_GetInitialGroupingCopyWith(_$_GetInitialGrouping value,
          $Res Function(_$_GetInitialGrouping) then) =
      __$$_GetInitialGroupingCopyWithImpl<$Res>;
  @useResult
  $Res call({Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable});
}

/// @nodoc
class __$$_GetInitialGroupingCopyWithImpl<$Res>
    extends _$MultiLevelGroupingEventCopyWithImpl<$Res, _$_GetInitialGrouping>
    implements _$$_GetInitialGroupingCopyWith<$Res> {
  __$$_GetInitialGroupingCopyWithImpl(
      _$_GetInitialGrouping _value, $Res Function(_$_GetInitialGrouping) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? groupKeys = freezed,
    Object? isPrevGroupAvailable = null,
  }) {
    return _then(_$_GetInitialGrouping(
      groupKeys: freezed == groupKeys
          ? _value._groupKeys
          : groupKeys // ignore: cast_nullable_to_non_nullable
              as Map<int, ActivityFieldDTO>?,
      isPrevGroupAvailable: null == isPrevGroupAvailable
          ? _value.isPrevGroupAvailable
          : isPrevGroupAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_GetInitialGrouping implements _GetInitialGrouping {
  const _$_GetInitialGrouping(
      {required final Map<int, ActivityFieldDTO>? groupKeys,
      required this.isPrevGroupAvailable})
      : _groupKeys = groupKeys;

  final Map<int, ActivityFieldDTO>? _groupKeys;
  @override
  Map<int, ActivityFieldDTO>? get groupKeys {
    final value = _groupKeys;
    if (value == null) return null;
    if (_groupKeys is EqualUnmodifiableMapView) return _groupKeys;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final bool isPrevGroupAvailable;

  @override
  String toString() {
    return 'MultiLevelGroupingEvent.getInitialGrouping(groupKeys: $groupKeys, isPrevGroupAvailable: $isPrevGroupAvailable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetInitialGrouping &&
            const DeepCollectionEquality()
                .equals(other._groupKeys, _groupKeys) &&
            (identical(other.isPrevGroupAvailable, isPrevGroupAvailable) ||
                other.isPrevGroupAvailable == isPrevGroupAvailable));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_groupKeys), isPrevGroupAvailable);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetInitialGroupingCopyWith<_$_GetInitialGrouping> get copyWith =>
      __$$_GetInitialGroupingCopyWithImpl<_$_GetInitialGrouping>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        getInitialGrouping,
    required TResult Function(GroupingTreeNodeDTO currentNode)
        getNodeOrActivities,
  }) {
    return getInitialGrouping(groupKeys, isPrevGroupAvailable);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult? Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
  }) {
    return getInitialGrouping?.call(groupKeys, isPrevGroupAvailable);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
    required TResult orElse(),
  }) {
    if (getInitialGrouping != null) {
      return getInitialGrouping(groupKeys, isPrevGroupAvailable);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetInitialGrouping value) getInitialGrouping,
    required TResult Function(_GetNodeOrActivities value) getNodeOrActivities,
  }) {
    return getInitialGrouping(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult? Function(_GetNodeOrActivities value)? getNodeOrActivities,
  }) {
    return getInitialGrouping?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult Function(_GetNodeOrActivities value)? getNodeOrActivities,
    required TResult orElse(),
  }) {
    if (getInitialGrouping != null) {
      return getInitialGrouping(this);
    }
    return orElse();
  }
}

abstract class _GetInitialGrouping implements MultiLevelGroupingEvent {
  const factory _GetInitialGrouping(
      {required final Map<int, ActivityFieldDTO>? groupKeys,
      required final bool isPrevGroupAvailable}) = _$_GetInitialGrouping;

  Map<int, ActivityFieldDTO>? get groupKeys;
  bool get isPrevGroupAvailable;
  @JsonKey(ignore: true)
  _$$_GetInitialGroupingCopyWith<_$_GetInitialGrouping> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GetNodeOrActivitiesCopyWith<$Res> {
  factory _$$_GetNodeOrActivitiesCopyWith(_$_GetNodeOrActivities value,
          $Res Function(_$_GetNodeOrActivities) then) =
      __$$_GetNodeOrActivitiesCopyWithImpl<$Res>;
  @useResult
  $Res call({GroupingTreeNodeDTO currentNode});
}

/// @nodoc
class __$$_GetNodeOrActivitiesCopyWithImpl<$Res>
    extends _$MultiLevelGroupingEventCopyWithImpl<$Res, _$_GetNodeOrActivities>
    implements _$$_GetNodeOrActivitiesCopyWith<$Res> {
  __$$_GetNodeOrActivitiesCopyWithImpl(_$_GetNodeOrActivities _value,
      $Res Function(_$_GetNodeOrActivities) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currentNode = null,
  }) {
    return _then(_$_GetNodeOrActivities(
      currentNode: null == currentNode
          ? _value.currentNode
          : currentNode // ignore: cast_nullable_to_non_nullable
              as GroupingTreeNodeDTO,
    ));
  }
}

/// @nodoc

class _$_GetNodeOrActivities implements _GetNodeOrActivities {
  const _$_GetNodeOrActivities({required this.currentNode});

  @override
  final GroupingTreeNodeDTO currentNode;

  @override
  String toString() {
    return 'MultiLevelGroupingEvent.getNodeOrActivities(currentNode: $currentNode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetNodeOrActivities &&
            (identical(other.currentNode, currentNode) ||
                other.currentNode == currentNode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, currentNode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetNodeOrActivitiesCopyWith<_$_GetNodeOrActivities> get copyWith =>
      __$$_GetNodeOrActivitiesCopyWithImpl<_$_GetNodeOrActivities>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        getInitialGrouping,
    required TResult Function(GroupingTreeNodeDTO currentNode)
        getNodeOrActivities,
  }) {
    return getNodeOrActivities(currentNode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult? Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
  }) {
    return getNodeOrActivities?.call(currentNode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        getInitialGrouping,
    TResult Function(GroupingTreeNodeDTO currentNode)? getNodeOrActivities,
    required TResult orElse(),
  }) {
    if (getNodeOrActivities != null) {
      return getNodeOrActivities(currentNode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetInitialGrouping value) getInitialGrouping,
    required TResult Function(_GetNodeOrActivities value) getNodeOrActivities,
  }) {
    return getNodeOrActivities(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult? Function(_GetNodeOrActivities value)? getNodeOrActivities,
  }) {
    return getNodeOrActivities?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetInitialGrouping value)? getInitialGrouping,
    TResult Function(_GetNodeOrActivities value)? getNodeOrActivities,
    required TResult orElse(),
  }) {
    if (getNodeOrActivities != null) {
      return getNodeOrActivities(this);
    }
    return orElse();
  }
}

abstract class _GetNodeOrActivities implements MultiLevelGroupingEvent {
  const factory _GetNodeOrActivities(
          {required final GroupingTreeNodeDTO currentNode}) =
      _$_GetNodeOrActivities;

  GroupingTreeNodeDTO get currentNode;
  @JsonKey(ignore: true)
  _$$_GetNodeOrActivitiesCopyWith<_$_GetNodeOrActivities> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$MultiLevelGroupingState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<GroupingTreeNodeDTO> groups) success,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult? Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultiLevelGroupingStateCopyWith<$Res> {
  factory $MultiLevelGroupingStateCopyWith(MultiLevelGroupingState value,
          $Res Function(MultiLevelGroupingState) then) =
      _$MultiLevelGroupingStateCopyWithImpl<$Res, MultiLevelGroupingState>;
}

/// @nodoc
class _$MultiLevelGroupingStateCopyWithImpl<$Res,
        $Val extends MultiLevelGroupingState>
    implements $MultiLevelGroupingStateCopyWith<$Res> {
  _$MultiLevelGroupingStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$MultiLevelGroupingStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'MultiLevelGroupingState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<GroupingTreeNodeDTO> groups) success,
    required TResult Function() error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult? Function()? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MultiLevelGroupingState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$MultiLevelGroupingStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'MultiLevelGroupingState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<GroupingTreeNodeDTO> groups) success,
    required TResult Function() error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult? Function()? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements MultiLevelGroupingState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<GroupingTreeNodeDTO> groups});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$MultiLevelGroupingStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? groups = null,
  }) {
    return _then(_$_Success(
      groups: null == groups
          ? _value._groups
          : groups // ignore: cast_nullable_to_non_nullable
              as List<GroupingTreeNodeDTO>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success({required final List<GroupingTreeNodeDTO> groups})
      : _groups = groups;

  final List<GroupingTreeNodeDTO> _groups;
  @override
  List<GroupingTreeNodeDTO> get groups {
    if (_groups is EqualUnmodifiableListView) return _groups;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_groups);
  }

  @override
  String toString() {
    return 'MultiLevelGroupingState.success(groups: $groups)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality().equals(other._groups, _groups));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_groups));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<GroupingTreeNodeDTO> groups) success,
    required TResult Function() error,
  }) {
    return success(groups);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult? Function()? error,
  }) {
    return success?.call(groups);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(groups);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements MultiLevelGroupingState {
  const factory _Success({required final List<GroupingTreeNodeDTO> groups}) =
      _$_Success;

  List<GroupingTreeNodeDTO> get groups;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$MultiLevelGroupingStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error();

  @override
  String toString() {
    return 'MultiLevelGroupingState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Error);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<GroupingTreeNodeDTO> groups) success,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult? Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<GroupingTreeNodeDTO> groups)? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements MultiLevelGroupingState {
  const factory _Error() = _$_Error;
}
