import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';

part 'wbs_activity_event.dart';
part 'wbs_activity_state.dart';
part 'wbs_activity_bloc.freezed.dart';

@injectable
class WbsActivityBloc extends Bloc<WbsActivityEvent, WbsActivityState> {
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  WbsActivityBloc(this.turnAroundActivityRepository) : super(const _Initial()) {
    on<WbsActivityEvent>((event, emit) async {
      await event.when(getWbsActivities: (activity) async {
        final wbsActivitiesStream = turnAroundActivityRepository.getWbsActivity(activity: activity);
        await emit.onEach(
          wbsActivitiesStream.stream,
          onData: (activities) {
            if (activities != [] && activities.isNotEmpty) {
              emit(const WbsActivityState.initial());
              emit(WbsActivityState.success(activity: ActivityCardItemModel.fromModel(activityDTO: activities[0])));
            }
          },
        ).catchError((error) {
          emit(const WbsActivityState.error(errorMsg: "Unexpected Error"));
        });
      });
    });
  }
}
