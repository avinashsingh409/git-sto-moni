part of 'wbs_activity_bloc.dart';

@freezed
class WbsActivityState with _$WbsActivityState {
  const factory WbsActivityState.initial() = _Initial;
  const factory WbsActivityState.loading() = _Loading;
  const factory WbsActivityState.success({required ActivityCardItemModel activity}) = _Success;
  const factory WbsActivityState.error({required String errorMsg}) = _Error;
}
