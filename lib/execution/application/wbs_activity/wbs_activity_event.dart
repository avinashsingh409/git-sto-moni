part of 'wbs_activity_bloc.dart';

@freezed
class WbsActivityEvent with _$WbsActivityEvent {
  const factory WbsActivityEvent.getWbsActivities({required String activity}) = _GetWbsActivities;
}
