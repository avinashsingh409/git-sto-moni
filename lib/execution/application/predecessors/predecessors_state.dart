part of 'predecessors_bloc.dart';

@freezed
class PredecessorsState with _$PredecessorsState {
  const factory PredecessorsState.initial() = _Initial;
  const factory PredecessorsState.loading() = _Loading;
  const factory PredecessorsState.predecessorsLoaded({
    required List<ActivityCardItemModel>? predecessors,
    required List<PredecessorActivityDTO>? unsyncedPredecessors,
  }) = _PredecessorsLoaded;
  const factory PredecessorsState.error({required String errorMsg}) = _Error;
}
