import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/predecessor_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/activity_relation/repository/activity_relations_repository.dart';

part 'predecessors_event.dart';
part 'predecessors_state.dart';
part 'predecessors_bloc.freezed.dart';

@injectable
class PredecessorsBloc extends Bloc<PredecessorsEvent, PredecessorsState> {
  List<ActivityCardItemModel> predecessorsActivityCardModels = [];
  List<String> predecessorsActivitiesStringList = [];
  List<String> dbPredecessorsActivitiesStringList = [];
  final IActivityRelationsRepository activityRelationsRepository;
  PredecessorsBloc(this.activityRelationsRepository) : super(const _Initial()) {
    on<PredecessorsEvent>((event, emit) async {
      await event.when(getPredecessors: (predecessorActivities) async {
        emit(const PredecessorsState.loading());
        if (predecessorActivities!.isEmpty) {
          emit(const PredecessorsState.error(errorMsg: 'No Predecessors'));
        } else {
          for (var element in predecessorActivities) {
            predecessorsActivitiesStringList.add(element.activity!);
          }
          final predecessorsFromDB =
              await activityRelationsRepository.getPredecessorActivities(activities: predecessorsActivitiesStringList);
          if (predecessorsFromDB != null) {
            for (var activity in predecessorsFromDB) {
              dbPredecessorsActivitiesStringList.add(activity.activity ?? '');
              predecessorsActivityCardModels.add(ActivityCardItemModel.fromModel(activityDTO: activity));
            }
            var leftOutPredecessorsString =
                predecessorsActivitiesStringList.toSet().difference(dbPredecessorsActivitiesStringList.toSet()).toList();

            List<PredecessorActivityDTO>? leftOutPredecessors = [];
            for (var activityName in leftOutPredecessorsString) {
              for (var predecessor in predecessorActivities) {
                if (activityName != predecessor.activity) {
                  leftOutPredecessors.add(predecessor);
                }
              }
            }
            emit(PredecessorsState.predecessorsLoaded(
                predecessors: predecessorsActivityCardModels, unsyncedPredecessors: leftOutPredecessors));
          }
        }
      });
    });
  }
}
