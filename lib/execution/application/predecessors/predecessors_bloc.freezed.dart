// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'predecessors_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PredecessorsEvent {
  List<PredecessorActivityDTO>? get predecessorActivites =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            List<PredecessorActivityDTO>? predecessorActivites)
        getPredecessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<PredecessorActivityDTO>? predecessorActivites)?
        getPredecessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<PredecessorActivityDTO>? predecessorActivites)?
        getPredecessors,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPredecessors value) getPredecessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPredecessors value)? getPredecessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPredecessors value)? getPredecessors,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PredecessorsEventCopyWith<PredecessorsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PredecessorsEventCopyWith<$Res> {
  factory $PredecessorsEventCopyWith(
          PredecessorsEvent value, $Res Function(PredecessorsEvent) then) =
      _$PredecessorsEventCopyWithImpl<$Res, PredecessorsEvent>;
  @useResult
  $Res call({List<PredecessorActivityDTO>? predecessorActivites});
}

/// @nodoc
class _$PredecessorsEventCopyWithImpl<$Res, $Val extends PredecessorsEvent>
    implements $PredecessorsEventCopyWith<$Res> {
  _$PredecessorsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? predecessorActivites = freezed,
  }) {
    return _then(_value.copyWith(
      predecessorActivites: freezed == predecessorActivites
          ? _value.predecessorActivites
          : predecessorActivites // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GetPredecessorsCopyWith<$Res>
    implements $PredecessorsEventCopyWith<$Res> {
  factory _$$_GetPredecessorsCopyWith(
          _$_GetPredecessors value, $Res Function(_$_GetPredecessors) then) =
      __$$_GetPredecessorsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<PredecessorActivityDTO>? predecessorActivites});
}

/// @nodoc
class __$$_GetPredecessorsCopyWithImpl<$Res>
    extends _$PredecessorsEventCopyWithImpl<$Res, _$_GetPredecessors>
    implements _$$_GetPredecessorsCopyWith<$Res> {
  __$$_GetPredecessorsCopyWithImpl(
      _$_GetPredecessors _value, $Res Function(_$_GetPredecessors) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? predecessorActivites = freezed,
  }) {
    return _then(_$_GetPredecessors(
      predecessorActivites: freezed == predecessorActivites
          ? _value._predecessorActivites
          : predecessorActivites // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>?,
    ));
  }
}

/// @nodoc

class _$_GetPredecessors implements _GetPredecessors {
  const _$_GetPredecessors(
      {required final List<PredecessorActivityDTO>? predecessorActivites})
      : _predecessorActivites = predecessorActivites;

  final List<PredecessorActivityDTO>? _predecessorActivites;
  @override
  List<PredecessorActivityDTO>? get predecessorActivites {
    final value = _predecessorActivites;
    if (value == null) return null;
    if (_predecessorActivites is EqualUnmodifiableListView)
      return _predecessorActivites;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PredecessorsEvent.getPredecessors(predecessorActivites: $predecessorActivites)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetPredecessors &&
            const DeepCollectionEquality()
                .equals(other._predecessorActivites, _predecessorActivites));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_predecessorActivites));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetPredecessorsCopyWith<_$_GetPredecessors> get copyWith =>
      __$$_GetPredecessorsCopyWithImpl<_$_GetPredecessors>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            List<PredecessorActivityDTO>? predecessorActivites)
        getPredecessors,
  }) {
    return getPredecessors(predecessorActivites);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<PredecessorActivityDTO>? predecessorActivites)?
        getPredecessors,
  }) {
    return getPredecessors?.call(predecessorActivites);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<PredecessorActivityDTO>? predecessorActivites)?
        getPredecessors,
    required TResult orElse(),
  }) {
    if (getPredecessors != null) {
      return getPredecessors(predecessorActivites);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPredecessors value) getPredecessors,
  }) {
    return getPredecessors(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPredecessors value)? getPredecessors,
  }) {
    return getPredecessors?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPredecessors value)? getPredecessors,
    required TResult orElse(),
  }) {
    if (getPredecessors != null) {
      return getPredecessors(this);
    }
    return orElse();
  }
}

abstract class _GetPredecessors implements PredecessorsEvent {
  const factory _GetPredecessors(
          {required final List<PredecessorActivityDTO>? predecessorActivites}) =
      _$_GetPredecessors;

  @override
  List<PredecessorActivityDTO>? get predecessorActivites;
  @override
  @JsonKey(ignore: true)
  _$$_GetPredecessorsCopyWith<_$_GetPredecessors> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PredecessorsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)
        predecessorsLoaded,
    required TResult Function(String errorMsg) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_PredecessorsLoaded value) predecessorsLoaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PredecessorsStateCopyWith<$Res> {
  factory $PredecessorsStateCopyWith(
          PredecessorsState value, $Res Function(PredecessorsState) then) =
      _$PredecessorsStateCopyWithImpl<$Res, PredecessorsState>;
}

/// @nodoc
class _$PredecessorsStateCopyWithImpl<$Res, $Val extends PredecessorsState>
    implements $PredecessorsStateCopyWith<$Res> {
  _$PredecessorsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$PredecessorsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'PredecessorsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)
        predecessorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_PredecessorsLoaded value) predecessorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements PredecessorsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$PredecessorsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'PredecessorsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)
        predecessorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_PredecessorsLoaded value) predecessorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements PredecessorsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_PredecessorsLoadedCopyWith<$Res> {
  factory _$$_PredecessorsLoadedCopyWith(_$_PredecessorsLoaded value,
          $Res Function(_$_PredecessorsLoaded) then) =
      __$$_PredecessorsLoadedCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<ActivityCardItemModel>? predecessors,
      List<PredecessorActivityDTO>? unsyncedPredecessors});
}

/// @nodoc
class __$$_PredecessorsLoadedCopyWithImpl<$Res>
    extends _$PredecessorsStateCopyWithImpl<$Res, _$_PredecessorsLoaded>
    implements _$$_PredecessorsLoadedCopyWith<$Res> {
  __$$_PredecessorsLoadedCopyWithImpl(
      _$_PredecessorsLoaded _value, $Res Function(_$_PredecessorsLoaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? predecessors = freezed,
    Object? unsyncedPredecessors = freezed,
  }) {
    return _then(_$_PredecessorsLoaded(
      predecessors: freezed == predecessors
          ? _value._predecessors
          : predecessors // ignore: cast_nullable_to_non_nullable
              as List<ActivityCardItemModel>?,
      unsyncedPredecessors: freezed == unsyncedPredecessors
          ? _value._unsyncedPredecessors
          : unsyncedPredecessors // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>?,
    ));
  }
}

/// @nodoc

class _$_PredecessorsLoaded implements _PredecessorsLoaded {
  const _$_PredecessorsLoaded(
      {required final List<ActivityCardItemModel>? predecessors,
      required final List<PredecessorActivityDTO>? unsyncedPredecessors})
      : _predecessors = predecessors,
        _unsyncedPredecessors = unsyncedPredecessors;

  final List<ActivityCardItemModel>? _predecessors;
  @override
  List<ActivityCardItemModel>? get predecessors {
    final value = _predecessors;
    if (value == null) return null;
    if (_predecessors is EqualUnmodifiableListView) return _predecessors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<PredecessorActivityDTO>? _unsyncedPredecessors;
  @override
  List<PredecessorActivityDTO>? get unsyncedPredecessors {
    final value = _unsyncedPredecessors;
    if (value == null) return null;
    if (_unsyncedPredecessors is EqualUnmodifiableListView)
      return _unsyncedPredecessors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PredecessorsState.predecessorsLoaded(predecessors: $predecessors, unsyncedPredecessors: $unsyncedPredecessors)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PredecessorsLoaded &&
            const DeepCollectionEquality()
                .equals(other._predecessors, _predecessors) &&
            const DeepCollectionEquality()
                .equals(other._unsyncedPredecessors, _unsyncedPredecessors));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_predecessors),
      const DeepCollectionEquality().hash(_unsyncedPredecessors));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PredecessorsLoadedCopyWith<_$_PredecessorsLoaded> get copyWith =>
      __$$_PredecessorsLoadedCopyWithImpl<_$_PredecessorsLoaded>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)
        predecessorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return predecessorsLoaded(predecessors, unsyncedPredecessors);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return predecessorsLoaded?.call(predecessors, unsyncedPredecessors);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (predecessorsLoaded != null) {
      return predecessorsLoaded(predecessors, unsyncedPredecessors);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_PredecessorsLoaded value) predecessorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return predecessorsLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return predecessorsLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (predecessorsLoaded != null) {
      return predecessorsLoaded(this);
    }
    return orElse();
  }
}

abstract class _PredecessorsLoaded implements PredecessorsState {
  const factory _PredecessorsLoaded(
          {required final List<ActivityCardItemModel>? predecessors,
          required final List<PredecessorActivityDTO>? unsyncedPredecessors}) =
      _$_PredecessorsLoaded;

  List<ActivityCardItemModel>? get predecessors;
  List<PredecessorActivityDTO>? get unsyncedPredecessors;
  @JsonKey(ignore: true)
  _$$_PredecessorsLoadedCopyWith<_$_PredecessorsLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$PredecessorsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_Error(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'PredecessorsState.error(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)
        predecessorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return error(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return error?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? predecessors,
            List<PredecessorActivityDTO>? unsyncedPredecessors)?
        predecessorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_PredecessorsLoaded value) predecessorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_PredecessorsLoaded value)? predecessorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements PredecessorsState {
  const factory _Error({required final String errorMsg}) = _$_Error;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
