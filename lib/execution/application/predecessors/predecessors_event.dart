part of 'predecessors_bloc.dart';

@freezed
class PredecessorsEvent with _$PredecessorsEvent {
  const factory PredecessorsEvent.getPredecessors({required List<PredecessorActivityDTO>? predecessorActivites}) =
      _GetPredecessors;
}
