part of 'grouping_activity_bloc.dart';

@freezed
class GroupingActivityEvent with _$GroupingActivityEvent {
  const factory GroupingActivityEvent.getGroupedActivities({required String activityIds}) = _GetGroupedActivities;
}
