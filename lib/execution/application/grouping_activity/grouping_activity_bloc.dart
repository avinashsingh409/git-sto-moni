import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/grouping/selected_grouping_sort.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';

part 'grouping_activity_event.dart';
part 'grouping_activity_state.dart';
part 'grouping_activity_bloc.freezed.dart';

@injectable
class GroupingActivityBloc extends Bloc<GroupingActivityEvent, GroupingActivityState> {
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  List<ActivityCardItemModel> activityCardModels = [];
  BehaviorSubject activitiesStream = BehaviorSubject<List<ActivityDTO>>();
  GroupingActivityBloc(this.turnAroundActivityRepository) : super(const _Initial()) {
    on<GroupingActivityEvent>((event, emit) async {
      await event.when(getGroupedActivities: (activityIds) async {
        activitiesStream = turnAroundActivityRepository.getGroupedActivities(activityIds: activityIds);
        final sortField = SelectedMultiLevelSort.multiLevelSort;

        //Todo fix sort trigger
        if (sortField != null) {
          await emit.onEach(
            activitiesStream.stream,
            onData: (activities) {
              List<ActivityCardItemModel> activityCardModels = [];
              emit(const GroupingActivityState.initial());
              for (var activity in activities) {
                activityCardModels.add(ActivityCardItemModel.fromModel(activityDTO: activity, dynamicFieldName: sortField.field));
              }
              activityCardModels
                  .sort((a, b) => (a.dynamicFieldValue ?? '').toLowerCase().compareTo((b.dynamicFieldValue ?? '').toLowerCase()));
              emit(GroupingActivityState.success(
                activities: activityCardModels,
              ));
            },
          ).catchError((error) {
            emit(const GroupingActivityState.error(errorMsg: "No Activities with this Criteria"));
          });
        } else {
          await emit.onEach(
            activitiesStream.stream,
            onData: (activities) {
              List<ActivityCardItemModel> activityCardModels = [];
              emit(const GroupingActivityState.initial());
              for (var activity in activities) {
                activityCardModels.add(ActivityCardItemModel.fromModel(activityDTO: activity));
              }
              emit(GroupingActivityState.success(
                activities: activityCardModels,
              ));
            },
          ).catchError((error) {
            emit(const GroupingActivityState.error(errorMsg: "No Activities with this Criteria"));
          });
        }
      });
    });
  }
}
