part of 'grouping_activity_bloc.dart';

@freezed
class GroupingActivityState with _$GroupingActivityState {
  const factory GroupingActivityState.initial() = _Initial;
  const factory GroupingActivityState.loading() = _Loading;
  const factory GroupingActivityState.success({required List<ActivityCardItemModel> activities}) = _Success;
  const factory GroupingActivityState.error({required String errorMsg}) = _Error;
}
