part of 'done_for_the_day_cubit.dart';

@freezed
class DoneForTheDayState with _$DoneForTheDayState {
  const factory DoneForTheDayState.initial() = _Initial;
  const factory DoneForTheDayState.loading() = _Loading;
  const factory DoneForTheDayState.valueChanged({
    required bool isDoneForTheDay,
  }) = _ValueChanged;
}
