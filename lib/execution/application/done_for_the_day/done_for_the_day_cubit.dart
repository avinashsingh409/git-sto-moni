import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart';
part 'done_for_the_day_state.dart';
part 'done_for_the_day_cubit.freezed.dart';

@lazySingleton
class DoneForTheDayCubit extends Cubit<DoneForTheDayState> {
  final IDoneForTheDayRepository doneForTheDayRepository;
  DoneForTheDayCubit(this.doneForTheDayRepository) : super(const DoneForTheDayState.initial());

  void initializePane() {
    final doneForTheDay = doneForTheDayRepository.getDoneForTheDay();
    if (doneForTheDay != null && doneForTheDay.isDoneForTheDay != null) {
      emit(DoneForTheDayState.valueChanged(isDoneForTheDay: doneForTheDay.isDoneForTheDay!));
    }
  }

  Future<void> updatedDoneForTheDay({required bool isDoneForTheDay}) async {
    emit(const DoneForTheDayState.loading());
    await doneForTheDayRepository.updateDoneForTheDay(isDoneForTheDay: isDoneForTheDay);
    final doneForTheDay = doneForTheDayRepository.getDoneForTheDay();
    if (doneForTheDay != null && doneForTheDay.isDoneForTheDay != null) {
      emit(DoneForTheDayState.valueChanged(isDoneForTheDay: doneForTheDay.isDoneForTheDay!));
    }
  }
}
