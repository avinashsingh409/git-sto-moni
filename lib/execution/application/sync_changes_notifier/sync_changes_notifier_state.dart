part of 'sync_changes_notifier_bloc.dart';

@freezed
class SyncChangesNotifierState with _$SyncChangesNotifierState {
  const factory SyncChangesNotifierState.initial() = _Initial;
  const factory SyncChangesNotifierState.noChanges() = _NoChanges;
  const factory SyncChangesNotifierState.syncPending() = _SyncPending;
}
