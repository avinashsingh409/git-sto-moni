import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/repository/execution_sync_repository.dart';

part 'sync_changes_notifier_event.dart';
part 'sync_changes_notifier_state.dart';
part 'sync_changes_notifier_bloc.freezed.dart';

@injectable
class SyncChangesNotifierBloc extends Bloc<SyncChangesNotifierEvent, SyncChangesNotifierState> {
  final IExecutionSyncRepository syncRepository;
  SyncChangesNotifierBloc(this.syncRepository) : super(const _Initial()) {
    on<SyncChangesNotifierEvent>((event, emit) async {
      await event.when(getSyncStatus: () async {
        final statusBehaviorSubject = syncRepository.isSyncPending();
        await emit.onEach(statusBehaviorSubject, onData: (isSyncPending) {
          if (isSyncPending) {
            emit(const SyncChangesNotifierState.syncPending());
          } else {
            emit(const SyncChangesNotifierState.noChanges());
          }
        });
      });
    });
    add(const SyncChangesNotifierEvent.getSyncStatus());
  }
}
