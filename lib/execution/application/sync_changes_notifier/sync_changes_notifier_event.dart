part of 'sync_changes_notifier_bloc.dart';

@freezed
class SyncChangesNotifierEvent with _$SyncChangesNotifierEvent {
  const factory SyncChangesNotifierEvent.getSyncStatus() = _GetSyncStatus;
}
