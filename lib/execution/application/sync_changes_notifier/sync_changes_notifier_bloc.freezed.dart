// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sync_changes_notifier_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SyncChangesNotifierEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getSyncStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getSyncStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getSyncStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSyncStatus value) getSyncStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSyncStatus value)? getSyncStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSyncStatus value)? getSyncStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SyncChangesNotifierEventCopyWith<$Res> {
  factory $SyncChangesNotifierEventCopyWith(SyncChangesNotifierEvent value,
          $Res Function(SyncChangesNotifierEvent) then) =
      _$SyncChangesNotifierEventCopyWithImpl<$Res, SyncChangesNotifierEvent>;
}

/// @nodoc
class _$SyncChangesNotifierEventCopyWithImpl<$Res,
        $Val extends SyncChangesNotifierEvent>
    implements $SyncChangesNotifierEventCopyWith<$Res> {
  _$SyncChangesNotifierEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetSyncStatusCopyWith<$Res> {
  factory _$$_GetSyncStatusCopyWith(
          _$_GetSyncStatus value, $Res Function(_$_GetSyncStatus) then) =
      __$$_GetSyncStatusCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetSyncStatusCopyWithImpl<$Res>
    extends _$SyncChangesNotifierEventCopyWithImpl<$Res, _$_GetSyncStatus>
    implements _$$_GetSyncStatusCopyWith<$Res> {
  __$$_GetSyncStatusCopyWithImpl(
      _$_GetSyncStatus _value, $Res Function(_$_GetSyncStatus) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetSyncStatus implements _GetSyncStatus {
  const _$_GetSyncStatus();

  @override
  String toString() {
    return 'SyncChangesNotifierEvent.getSyncStatus()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetSyncStatus);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getSyncStatus,
  }) {
    return getSyncStatus();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getSyncStatus,
  }) {
    return getSyncStatus?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getSyncStatus,
    required TResult orElse(),
  }) {
    if (getSyncStatus != null) {
      return getSyncStatus();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSyncStatus value) getSyncStatus,
  }) {
    return getSyncStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSyncStatus value)? getSyncStatus,
  }) {
    return getSyncStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSyncStatus value)? getSyncStatus,
    required TResult orElse(),
  }) {
    if (getSyncStatus != null) {
      return getSyncStatus(this);
    }
    return orElse();
  }
}

abstract class _GetSyncStatus implements SyncChangesNotifierEvent {
  const factory _GetSyncStatus() = _$_GetSyncStatus;
}

/// @nodoc
mixin _$SyncChangesNotifierState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() noChanges,
    required TResult Function() syncPending,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? noChanges,
    TResult? Function()? syncPending,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? noChanges,
    TResult Function()? syncPending,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_NoChanges value) noChanges,
    required TResult Function(_SyncPending value) syncPending,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_NoChanges value)? noChanges,
    TResult? Function(_SyncPending value)? syncPending,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_NoChanges value)? noChanges,
    TResult Function(_SyncPending value)? syncPending,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SyncChangesNotifierStateCopyWith<$Res> {
  factory $SyncChangesNotifierStateCopyWith(SyncChangesNotifierState value,
          $Res Function(SyncChangesNotifierState) then) =
      _$SyncChangesNotifierStateCopyWithImpl<$Res, SyncChangesNotifierState>;
}

/// @nodoc
class _$SyncChangesNotifierStateCopyWithImpl<$Res,
        $Val extends SyncChangesNotifierState>
    implements $SyncChangesNotifierStateCopyWith<$Res> {
  _$SyncChangesNotifierStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$SyncChangesNotifierStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'SyncChangesNotifierState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() noChanges,
    required TResult Function() syncPending,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? noChanges,
    TResult? Function()? syncPending,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? noChanges,
    TResult Function()? syncPending,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_NoChanges value) noChanges,
    required TResult Function(_SyncPending value) syncPending,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_NoChanges value)? noChanges,
    TResult? Function(_SyncPending value)? syncPending,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_NoChanges value)? noChanges,
    TResult Function(_SyncPending value)? syncPending,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements SyncChangesNotifierState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_NoChangesCopyWith<$Res> {
  factory _$$_NoChangesCopyWith(
          _$_NoChanges value, $Res Function(_$_NoChanges) then) =
      __$$_NoChangesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoChangesCopyWithImpl<$Res>
    extends _$SyncChangesNotifierStateCopyWithImpl<$Res, _$_NoChanges>
    implements _$$_NoChangesCopyWith<$Res> {
  __$$_NoChangesCopyWithImpl(
      _$_NoChanges _value, $Res Function(_$_NoChanges) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoChanges implements _NoChanges {
  const _$_NoChanges();

  @override
  String toString() {
    return 'SyncChangesNotifierState.noChanges()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoChanges);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() noChanges,
    required TResult Function() syncPending,
  }) {
    return noChanges();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? noChanges,
    TResult? Function()? syncPending,
  }) {
    return noChanges?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? noChanges,
    TResult Function()? syncPending,
    required TResult orElse(),
  }) {
    if (noChanges != null) {
      return noChanges();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_NoChanges value) noChanges,
    required TResult Function(_SyncPending value) syncPending,
  }) {
    return noChanges(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_NoChanges value)? noChanges,
    TResult? Function(_SyncPending value)? syncPending,
  }) {
    return noChanges?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_NoChanges value)? noChanges,
    TResult Function(_SyncPending value)? syncPending,
    required TResult orElse(),
  }) {
    if (noChanges != null) {
      return noChanges(this);
    }
    return orElse();
  }
}

abstract class _NoChanges implements SyncChangesNotifierState {
  const factory _NoChanges() = _$_NoChanges;
}

/// @nodoc
abstract class _$$_SyncPendingCopyWith<$Res> {
  factory _$$_SyncPendingCopyWith(
          _$_SyncPending value, $Res Function(_$_SyncPending) then) =
      __$$_SyncPendingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SyncPendingCopyWithImpl<$Res>
    extends _$SyncChangesNotifierStateCopyWithImpl<$Res, _$_SyncPending>
    implements _$$_SyncPendingCopyWith<$Res> {
  __$$_SyncPendingCopyWithImpl(
      _$_SyncPending _value, $Res Function(_$_SyncPending) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SyncPending implements _SyncPending {
  const _$_SyncPending();

  @override
  String toString() {
    return 'SyncChangesNotifierState.syncPending()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SyncPending);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() noChanges,
    required TResult Function() syncPending,
  }) {
    return syncPending();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? noChanges,
    TResult? Function()? syncPending,
  }) {
    return syncPending?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? noChanges,
    TResult Function()? syncPending,
    required TResult orElse(),
  }) {
    if (syncPending != null) {
      return syncPending();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_NoChanges value) noChanges,
    required TResult Function(_SyncPending value) syncPending,
  }) {
    return syncPending(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_NoChanges value)? noChanges,
    TResult? Function(_SyncPending value)? syncPending,
  }) {
    return syncPending?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_NoChanges value)? noChanges,
    TResult Function(_SyncPending value)? syncPending,
    required TResult orElse(),
  }) {
    if (syncPending != null) {
      return syncPending(this);
    }
    return orElse();
  }
}

abstract class _SyncPending implements SyncChangesNotifierState {
  const factory _SyncPending() = _$_SyncPending;
}
