part of 'activity_details_screen_bloc.dart';

@freezed
class ActivityDetailsScreenEvent with _$ActivityDetailsScreenEvent {
  const factory ActivityDetailsScreenEvent.getActivityDetails({required int activityId}) = _GetActivityDetails;
  const factory ActivityDetailsScreenEvent.updateActivityProgress({required UpdatedActivityDTO updateActivityModel}) =
      _UpdateActivityProgress;
}
