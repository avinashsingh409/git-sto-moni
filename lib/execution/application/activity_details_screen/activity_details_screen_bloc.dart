import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/screens/widgets/show_toast.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/primavera_activity_status_enum.dart';
import 'package:sto_mobile_v2/execution/domain/comment/repository/turnaround_comment_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';
import 'package:sto_mobile_v2/common/screens/widgets/comments_dialog.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/predecessor_confirmation_dialog.dart';

part 'activity_details_screen_event.dart';
part 'activity_details_screen_state.dart';
part 'activity_details_screen_bloc.freezed.dart';

@injectable
class ActivityDetailsScreenBloc extends Bloc<ActivityDetailsScreenEvent, ActivityDetailsScreenState> {
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  final ITurnAroundEventRepository turnAroundEventRepository;
  final ITurnaroundActivitiesCommentRepository commentRepository;
  final ISessionRepository sessionRepository;

  final _activityBehaviorSubject = BehaviorSubject<ActivityDTO>();
  Stream<ActivityDTO> getActivityDetails() => _activityBehaviorSubject.stream;

  final _activityFieldSubject = BehaviorSubject<Map<String, String>>();
  Stream<Map<String, String>> getActivityFields() => _activityFieldSubject.stream;

  ActivityDetailsScreenBloc(
    this.turnAroundActivityRepository,
    this.turnAroundEventRepository,
    this.commentRepository,
    this.sessionRepository,
  ) : super(const _Initial()) {
    on<ActivityDetailsScreenEvent>((event, emit) async {
      event.when(getActivityDetails: (activityId) async {
        final activity = await turnAroundActivityRepository.getActivityById(activityId: activityId);
        if (activity != null) {
          _activityBehaviorSubject.add(activity);
          _activityBehaviorSubject.stream.listen((activity) async {
            final parsedActivityFieldMap = await turnAroundActivityRepository.getActivityFieldsWithValue(
              activityDTO: activity,
            );
            if (parsedActivityFieldMap != null) {
              _activityFieldSubject.add(parsedActivityFieldMap);
            }
          });
        }
      }, updateActivityProgress: (updateActivityModel) async {
        final activity = await getActivityDetails().first;
        if (activity.predecessorsStatus != PrimaveraActivityStatus.completed) {
          var cancel = false;
          await showDialog(
            context: GlobalNavigationKey().context!,
            builder: (BuildContext context) {
              return PredecessorConfirmationDialog(
                onCancel: () => cancel = true
              );
            }
          );
          if (cancel) {
            return;
          }
        }

        final backwardsCommentsEnabled = turnAroundEventRepository.getBackwardsCommentEnabled();
        await backwardsCommentsEnabled.fold((isBackWardsCommentsEnabled) async {
          if (isBackWardsCommentsEnabled && updateActivityModel.percentComplete != null) {
            if (activity.percentComplete! > updateActivityModel.percentComplete!) {
              final userInformation = sessionRepository.getSession();
              userInformation.fold(
                (session) async {
                  await showDialog(
                    context: GlobalNavigationKey().context!,
                    builder: (BuildContext context) {
                      return CommentDialog(
                        onCommentSubmitted: (comment) async {
                          await commentRepository.createComment(
                              objectId: activity.objectId!, userInformation: session.userInformation, comment: comment);
                          await _updateActivity(
                              activity: activity, updateActivityModel: updateActivityModel, isCommentUpdateRequired: true);
                        },
                      );
                    },
                  );
                },
                (exception) {},
              );
            }
          }
        }, (_) async {});

        final percentageRange = turnAroundEventRepository.getPercentageRange();
        await percentageRange.fold((percentageRange) async {
          // if the percentage range is not empty, the STO event is set to manual start date
          if (percentageRange.isNotEmpty &&
              activity.lastPercentComplete == 0 &&
              updateActivityModel.percentComplete != null &&
              activity.manualDateSet != true) {
            final range = percentageRange.split("-");
            final lower = int.parse(range[0]);
            final upper = int.parse(range[1]);
            if (updateActivityModel.percentComplete! * 100 >= lower && updateActivityModel.percentComplete! * 100 <= upper) {
              final date = await showDatePicker(
                  context: GlobalNavigationKey().context!,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now().subtract(const Duration(days: 3650)),
                  lastDate: DateTime.now().add(const Duration(days: 3650)));
              TimeOfDay? time;
              if (date != null) {
                time = await showTimePicker(context: GlobalNavigationKey().context!, initialTime: TimeOfDay.now());
              }
              final finishDate = activity.changedActualFinishDate ?? activity.actualFinishDate ?? activity.finishDate;
              if (date != null && time != null) {
                final manualDate = DateTime(date.year, date.month, date.day, time.hour, time.minute).toUtc();
                if (finishDate == null || manualDate.isBefore(finishDate)) {
                  updateActivityModel.manualStartDate = manualDate;
                  updateActivityModel.manualDateSet = true;
                  await _updateActivity(
                      activity: activity, updateActivityModel: updateActivityModel, isCommentUpdateRequired: false);
                } else {
                  showErrorToast(isBannerRed: true, errorText: "Start Date must be before Finish Date");
                }
              }
            } else {
              await _updateActivity(activity: activity, updateActivityModel: updateActivityModel, isCommentUpdateRequired: false);
            }
          } else {
            await _updateActivity(activity: activity, updateActivityModel: updateActivityModel, isCommentUpdateRequired: false);
          }
        }, (_) async {});
      });
    });
  }

  Future<void> _updateActivity(
      {required ActivityDTO activity,
      required UpdatedActivityDTO updateActivityModel,
      required bool isCommentUpdateRequired}) async {
    final updatedActivity = UpdatedActivityDTO(
      id: activity.id!,
      percentComplete: updateActivityModel.percentComplete ?? (activity.changed! ? activity.percentComplete : null),
      unableToWork: updateActivityModel.unableToWork,
      unableToWorkDateTime: updateActivityModel.unableToWorkDateTime,
      unableToWorkReasons: updateActivityModel.unableToWorkReasons,
      unableToWorkAdditionalInfo: updateActivityModel.unableToWorkAdditionalInfo,
      dateModified: DateTime.now(),
      objectId: activity.objectId,
      manualStartDate: updateActivityModel.manualStartDate,
      manualDateSet: updateActivityModel.manualDateSet ?? activity.manualDateSet ?? false,
      commentCount: isCommentUpdateRequired ? (activity.commentCount ?? 0) + 1 : activity.commentCount,
    );

    final changedActivity = activity.copyWith(
      updatedActivityDTO: updatedActivity,
    );
    _activityBehaviorSubject.add(changedActivity);
    _activityBehaviorSubject.stream.listen((activity) async {
      final parsedActivityFieldMap = await turnAroundActivityRepository.getActivityFieldsWithValue(
        activityDTO: activity,
      );
      if (parsedActivityFieldMap != null) {
        _activityFieldSubject.add(parsedActivityFieldMap);
      }
    });
    await turnAroundActivityRepository.updateActivity(
      currentActivity: activity,
      updateActivityModel: updateActivityModel,
    );
  }

  Color getProgressColor(double percentage) {
    if (percentage >= 0 && percentage < 0.50) {
      return Colors.red.shade700;
    } else if (percentage >= 0.50 && percentage < 1) {
      return Colors.orange.shade700;
    } else if (percentage == 1) {
      return Colors.green.shade700;
    }
    return Colors.grey;
  }

  @override
  Future<void> close() {
    _activityBehaviorSubject.drain();
    _activityFieldSubject.drain();
    return super.close();
  }
}

typedef CommentCallBack = void Function(int foo);
