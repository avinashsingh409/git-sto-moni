// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'activity_details_screen_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ActivityDetailsScreenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int activityId) getActivityDetails,
    required TResult Function(UpdatedActivityDTO updateActivityModel)
        updateActivityProgress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int activityId)? getActivityDetails,
    TResult? Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int activityId)? getActivityDetails,
    TResult Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetActivityDetails value) getActivityDetails,
    required TResult Function(_UpdateActivityProgress value)
        updateActivityProgress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetActivityDetails value)? getActivityDetails,
    TResult? Function(_UpdateActivityProgress value)? updateActivityProgress,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetActivityDetails value)? getActivityDetails,
    TResult Function(_UpdateActivityProgress value)? updateActivityProgress,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityDetailsScreenEventCopyWith<$Res> {
  factory $ActivityDetailsScreenEventCopyWith(ActivityDetailsScreenEvent value,
          $Res Function(ActivityDetailsScreenEvent) then) =
      _$ActivityDetailsScreenEventCopyWithImpl<$Res,
          ActivityDetailsScreenEvent>;
}

/// @nodoc
class _$ActivityDetailsScreenEventCopyWithImpl<$Res,
        $Val extends ActivityDetailsScreenEvent>
    implements $ActivityDetailsScreenEventCopyWith<$Res> {
  _$ActivityDetailsScreenEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetActivityDetailsCopyWith<$Res> {
  factory _$$_GetActivityDetailsCopyWith(_$_GetActivityDetails value,
          $Res Function(_$_GetActivityDetails) then) =
      __$$_GetActivityDetailsCopyWithImpl<$Res>;
  @useResult
  $Res call({int activityId});
}

/// @nodoc
class __$$_GetActivityDetailsCopyWithImpl<$Res>
    extends _$ActivityDetailsScreenEventCopyWithImpl<$Res,
        _$_GetActivityDetails> implements _$$_GetActivityDetailsCopyWith<$Res> {
  __$$_GetActivityDetailsCopyWithImpl(
      _$_GetActivityDetails _value, $Res Function(_$_GetActivityDetails) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? activityId = null,
  }) {
    return _then(_$_GetActivityDetails(
      activityId: null == activityId
          ? _value.activityId
          : activityId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_GetActivityDetails implements _GetActivityDetails {
  const _$_GetActivityDetails({required this.activityId});

  @override
  final int activityId;

  @override
  String toString() {
    return 'ActivityDetailsScreenEvent.getActivityDetails(activityId: $activityId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetActivityDetails &&
            (identical(other.activityId, activityId) ||
                other.activityId == activityId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, activityId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetActivityDetailsCopyWith<_$_GetActivityDetails> get copyWith =>
      __$$_GetActivityDetailsCopyWithImpl<_$_GetActivityDetails>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int activityId) getActivityDetails,
    required TResult Function(UpdatedActivityDTO updateActivityModel)
        updateActivityProgress,
  }) {
    return getActivityDetails(activityId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int activityId)? getActivityDetails,
    TResult? Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
  }) {
    return getActivityDetails?.call(activityId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int activityId)? getActivityDetails,
    TResult Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
    required TResult orElse(),
  }) {
    if (getActivityDetails != null) {
      return getActivityDetails(activityId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetActivityDetails value) getActivityDetails,
    required TResult Function(_UpdateActivityProgress value)
        updateActivityProgress,
  }) {
    return getActivityDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetActivityDetails value)? getActivityDetails,
    TResult? Function(_UpdateActivityProgress value)? updateActivityProgress,
  }) {
    return getActivityDetails?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetActivityDetails value)? getActivityDetails,
    TResult Function(_UpdateActivityProgress value)? updateActivityProgress,
    required TResult orElse(),
  }) {
    if (getActivityDetails != null) {
      return getActivityDetails(this);
    }
    return orElse();
  }
}

abstract class _GetActivityDetails implements ActivityDetailsScreenEvent {
  const factory _GetActivityDetails({required final int activityId}) =
      _$_GetActivityDetails;

  int get activityId;
  @JsonKey(ignore: true)
  _$$_GetActivityDetailsCopyWith<_$_GetActivityDetails> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateActivityProgressCopyWith<$Res> {
  factory _$$_UpdateActivityProgressCopyWith(_$_UpdateActivityProgress value,
          $Res Function(_$_UpdateActivityProgress) then) =
      __$$_UpdateActivityProgressCopyWithImpl<$Res>;
  @useResult
  $Res call({UpdatedActivityDTO updateActivityModel});
}

/// @nodoc
class __$$_UpdateActivityProgressCopyWithImpl<$Res>
    extends _$ActivityDetailsScreenEventCopyWithImpl<$Res,
        _$_UpdateActivityProgress>
    implements _$$_UpdateActivityProgressCopyWith<$Res> {
  __$$_UpdateActivityProgressCopyWithImpl(_$_UpdateActivityProgress _value,
      $Res Function(_$_UpdateActivityProgress) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updateActivityModel = null,
  }) {
    return _then(_$_UpdateActivityProgress(
      updateActivityModel: null == updateActivityModel
          ? _value.updateActivityModel
          : updateActivityModel // ignore: cast_nullable_to_non_nullable
              as UpdatedActivityDTO,
    ));
  }
}

/// @nodoc

class _$_UpdateActivityProgress implements _UpdateActivityProgress {
  const _$_UpdateActivityProgress({required this.updateActivityModel});

  @override
  final UpdatedActivityDTO updateActivityModel;

  @override
  String toString() {
    return 'ActivityDetailsScreenEvent.updateActivityProgress(updateActivityModel: $updateActivityModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateActivityProgress &&
            (identical(other.updateActivityModel, updateActivityModel) ||
                other.updateActivityModel == updateActivityModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, updateActivityModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateActivityProgressCopyWith<_$_UpdateActivityProgress> get copyWith =>
      __$$_UpdateActivityProgressCopyWithImpl<_$_UpdateActivityProgress>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int activityId) getActivityDetails,
    required TResult Function(UpdatedActivityDTO updateActivityModel)
        updateActivityProgress,
  }) {
    return updateActivityProgress(updateActivityModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int activityId)? getActivityDetails,
    TResult? Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
  }) {
    return updateActivityProgress?.call(updateActivityModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int activityId)? getActivityDetails,
    TResult Function(UpdatedActivityDTO updateActivityModel)?
        updateActivityProgress,
    required TResult orElse(),
  }) {
    if (updateActivityProgress != null) {
      return updateActivityProgress(updateActivityModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetActivityDetails value) getActivityDetails,
    required TResult Function(_UpdateActivityProgress value)
        updateActivityProgress,
  }) {
    return updateActivityProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetActivityDetails value)? getActivityDetails,
    TResult? Function(_UpdateActivityProgress value)? updateActivityProgress,
  }) {
    return updateActivityProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetActivityDetails value)? getActivityDetails,
    TResult Function(_UpdateActivityProgress value)? updateActivityProgress,
    required TResult orElse(),
  }) {
    if (updateActivityProgress != null) {
      return updateActivityProgress(this);
    }
    return orElse();
  }
}

abstract class _UpdateActivityProgress implements ActivityDetailsScreenEvent {
  const factory _UpdateActivityProgress(
          {required final UpdatedActivityDTO updateActivityModel}) =
      _$_UpdateActivityProgress;

  UpdatedActivityDTO get updateActivityModel;
  @JsonKey(ignore: true)
  _$$_UpdateActivityProgressCopyWith<_$_UpdateActivityProgress> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ActivityDetailsScreenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityDetailsScreenStateCopyWith<$Res> {
  factory $ActivityDetailsScreenStateCopyWith(ActivityDetailsScreenState value,
          $Res Function(ActivityDetailsScreenState) then) =
      _$ActivityDetailsScreenStateCopyWithImpl<$Res,
          ActivityDetailsScreenState>;
}

/// @nodoc
class _$ActivityDetailsScreenStateCopyWithImpl<$Res,
        $Val extends ActivityDetailsScreenState>
    implements $ActivityDetailsScreenStateCopyWith<$Res> {
  _$ActivityDetailsScreenStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ActivityDetailsScreenStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ActivityDetailsScreenState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ActivityDetailsScreenState {
  const factory _Initial() = _$_Initial;
}
