part of 'activity_details_screen_bloc.dart';

@freezed
class ActivityDetailsScreenState with _$ActivityDetailsScreenState {
  const factory ActivityDetailsScreenState.initial() = _Initial;
}
