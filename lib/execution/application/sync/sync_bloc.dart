import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_data/repository/turnaround_data_repository.dart';

part 'sync_event.dart';
part 'sync_state.dart';
part 'sync_bloc.freezed.dart';

@injectable
class SyncBloc extends Bloc<SyncEvent, SyncState> {
  final ITurnaroundDataRepository turnaroundDataRepository;
  SyncBloc(this.turnaroundDataRepository) : super(const _Initial()) {
    on<SyncEvent>((event, emit) async {
      await event.when(pushAndGetChangesFromServer: () async {
        emit(const SyncState.loading());
        final turnaroundData = await turnaroundDataRepository.getAllTurnaroundData();
        await turnaroundData.fold((success) async {
          emit(const SyncState.success());
        }, (exception) async {
          emit(const SyncState.error(errorMsg: 'Error syncing the data. Please try again Later.'));
        });
      });
    });
  }
}
