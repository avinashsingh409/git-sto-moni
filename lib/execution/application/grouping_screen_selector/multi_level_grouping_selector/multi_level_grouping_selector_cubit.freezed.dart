// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'multi_level_grouping_selector_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MultiLevelGroupingSelectorState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityFieldDTO> groupableFields) loaded,
    required TResult Function() noGroupConfiguration,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult? Function()? noGroupConfiguration,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult Function()? noGroupConfiguration,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_NoGroupConfiguration value) noGroupConfiguration,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_NoGroupConfiguration value)? noGroupConfiguration,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_NoGroupConfiguration value)? noGroupConfiguration,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultiLevelGroupingSelectorStateCopyWith<$Res> {
  factory $MultiLevelGroupingSelectorStateCopyWith(
          MultiLevelGroupingSelectorState value,
          $Res Function(MultiLevelGroupingSelectorState) then) =
      _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res,
          MultiLevelGroupingSelectorState>;
}

/// @nodoc
class _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res,
        $Val extends MultiLevelGroupingSelectorState>
    implements $MultiLevelGroupingSelectorStateCopyWith<$Res> {
  _$MultiLevelGroupingSelectorStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'MultiLevelGroupingSelectorState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityFieldDTO> groupableFields) loaded,
    required TResult Function() noGroupConfiguration,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult? Function()? noGroupConfiguration,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult Function()? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_NoGroupConfiguration value) noGroupConfiguration,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_NoGroupConfiguration value)? noGroupConfiguration,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_NoGroupConfiguration value)? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MultiLevelGroupingSelectorState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'MultiLevelGroupingSelectorState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityFieldDTO> groupableFields) loaded,
    required TResult Function() noGroupConfiguration,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult? Function()? noGroupConfiguration,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult Function()? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_NoGroupConfiguration value) noGroupConfiguration,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_NoGroupConfiguration value)? noGroupConfiguration,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_NoGroupConfiguration value)? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements MultiLevelGroupingSelectorState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_LoadedCopyWith<$Res> {
  factory _$$_LoadedCopyWith(_$_Loaded value, $Res Function(_$_Loaded) then) =
      __$$_LoadedCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ActivityFieldDTO> groupableFields});
}

/// @nodoc
class __$$_LoadedCopyWithImpl<$Res>
    extends _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res, _$_Loaded>
    implements _$$_LoadedCopyWith<$Res> {
  __$$_LoadedCopyWithImpl(_$_Loaded _value, $Res Function(_$_Loaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? groupableFields = null,
  }) {
    return _then(_$_Loaded(
      groupableFields: null == groupableFields
          ? _value._groupableFields
          : groupableFields // ignore: cast_nullable_to_non_nullable
              as List<ActivityFieldDTO>,
    ));
  }
}

/// @nodoc

class _$_Loaded implements _Loaded {
  const _$_Loaded({required final List<ActivityFieldDTO> groupableFields})
      : _groupableFields = groupableFields;

  final List<ActivityFieldDTO> _groupableFields;
  @override
  List<ActivityFieldDTO> get groupableFields {
    if (_groupableFields is EqualUnmodifiableListView) return _groupableFields;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_groupableFields);
  }

  @override
  String toString() {
    return 'MultiLevelGroupingSelectorState.loaded(groupableFields: $groupableFields)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Loaded &&
            const DeepCollectionEquality()
                .equals(other._groupableFields, _groupableFields));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_groupableFields));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoadedCopyWith<_$_Loaded> get copyWith =>
      __$$_LoadedCopyWithImpl<_$_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityFieldDTO> groupableFields) loaded,
    required TResult Function() noGroupConfiguration,
  }) {
    return loaded(groupableFields);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult? Function()? noGroupConfiguration,
  }) {
    return loaded?.call(groupableFields);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult Function()? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(groupableFields);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_NoGroupConfiguration value) noGroupConfiguration,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_NoGroupConfiguration value)? noGroupConfiguration,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_NoGroupConfiguration value)? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements MultiLevelGroupingSelectorState {
  const factory _Loaded(
      {required final List<ActivityFieldDTO> groupableFields}) = _$_Loaded;

  List<ActivityFieldDTO> get groupableFields;
  @JsonKey(ignore: true)
  _$$_LoadedCopyWith<_$_Loaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_NoGroupConfigurationCopyWith<$Res> {
  factory _$$_NoGroupConfigurationCopyWith(_$_NoGroupConfiguration value,
          $Res Function(_$_NoGroupConfiguration) then) =
      __$$_NoGroupConfigurationCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoGroupConfigurationCopyWithImpl<$Res>
    extends _$MultiLevelGroupingSelectorStateCopyWithImpl<$Res,
        _$_NoGroupConfiguration>
    implements _$$_NoGroupConfigurationCopyWith<$Res> {
  __$$_NoGroupConfigurationCopyWithImpl(_$_NoGroupConfiguration _value,
      $Res Function(_$_NoGroupConfiguration) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoGroupConfiguration implements _NoGroupConfiguration {
  const _$_NoGroupConfiguration();

  @override
  String toString() {
    return 'MultiLevelGroupingSelectorState.noGroupConfiguration()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoGroupConfiguration);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityFieldDTO> groupableFields) loaded,
    required TResult Function() noGroupConfiguration,
  }) {
    return noGroupConfiguration();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult? Function()? noGroupConfiguration,
  }) {
    return noGroupConfiguration?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityFieldDTO> groupableFields)? loaded,
    TResult Function()? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (noGroupConfiguration != null) {
      return noGroupConfiguration();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_NoGroupConfiguration value) noGroupConfiguration,
  }) {
    return noGroupConfiguration(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_NoGroupConfiguration value)? noGroupConfiguration,
  }) {
    return noGroupConfiguration?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_NoGroupConfiguration value)? noGroupConfiguration,
    required TResult orElse(),
  }) {
    if (noGroupConfiguration != null) {
      return noGroupConfiguration(this);
    }
    return orElse();
  }
}

abstract class _NoGroupConfiguration
    implements MultiLevelGroupingSelectorState {
  const factory _NoGroupConfiguration() = _$_NoGroupConfiguration;
}
