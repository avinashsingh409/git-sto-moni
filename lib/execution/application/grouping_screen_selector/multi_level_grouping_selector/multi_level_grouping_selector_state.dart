part of 'multi_level_grouping_selector_cubit.dart';

@freezed
class MultiLevelGroupingSelectorState with _$MultiLevelGroupingSelectorState {
  const factory MultiLevelGroupingSelectorState.initial() = _Initial;
  const factory MultiLevelGroupingSelectorState.loading() = _Loading;
  const factory MultiLevelGroupingSelectorState.loaded({required List<ActivityFieldDTO> groupableFields}) = _Loaded;
  const factory MultiLevelGroupingSelectorState.noGroupConfiguration() = _NoGroupConfiguration;
}
