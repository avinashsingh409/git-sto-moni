import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';

part 'multi_level_grouping_selector_state.dart';
part 'multi_level_grouping_selector_cubit.freezed.dart';

@injectable
class MultiLevelGroupingSelectorCubit extends Cubit<MultiLevelGroupingSelectorState> {
  final IActivityGroupingRepository activityGroupingRepository;
  MultiLevelGroupingSelectorCubit(this.activityGroupingRepository) : super(const MultiLevelGroupingSelectorState.initial());

  void getAllGroupableFields() async {
    final groupableFields = await activityGroupingRepository.getGroupableFields();
    if (groupableFields != null) {
      emit(MultiLevelGroupingSelectorState.loaded(groupableFields: groupableFields));
    } else {
      emit(const MultiLevelGroupingSelectorState.noGroupConfiguration());
    }
  }
}
