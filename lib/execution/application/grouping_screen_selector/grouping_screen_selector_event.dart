part of 'grouping_screen_selector_bloc.dart';

@freezed
class GroupingScreenSelectorEvent with _$GroupingScreenSelectorEvent {
  const factory GroupingScreenSelectorEvent.started() = _Started;
  const factory GroupingScreenSelectorEvent.getWbsGrouping() = _GetWbsGrouping;
  const factory GroupingScreenSelectorEvent.selectMultiLevel() = _SelectMultiLevel;
  const factory GroupingScreenSelectorEvent.filterMultiLevelGrouping({
    required Map<int, ActivityFieldDTO>? groupKeys,
    required Set<MultiLevelSortDTO>? sortParams,
  }) = _FilterMultiLevelGrouping;

  const factory GroupingScreenSelectorEvent.clearMultiLevelGrouping() = _ClearMultiLevelGrouping;
}
