part of 'grouping_screen_selector_bloc.dart';

@freezed
class GroupingScreenSelectorState with _$GroupingScreenSelectorState {
  const factory GroupingScreenSelectorState.initial() = _Initial;
  const factory GroupingScreenSelectorState.wbsSelected() = _WbsSelected;
  const factory GroupingScreenSelectorState.multiLevelConfigurePopUp() = _MultiLevelConfigurePopUp;
  const factory GroupingScreenSelectorState.multiLevelSelected(
      {required Map<int, ActivityFieldDTO>? groupKeys, required bool isPrevGroupAvailable}) = _MultiLevelSelected;
}
