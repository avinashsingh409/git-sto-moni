// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'grouping_screen_selector_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$GroupingScreenSelectorEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GroupingScreenSelectorEventCopyWith<$Res> {
  factory $GroupingScreenSelectorEventCopyWith(
          GroupingScreenSelectorEvent value,
          $Res Function(GroupingScreenSelectorEvent) then) =
      _$GroupingScreenSelectorEventCopyWithImpl<$Res,
          GroupingScreenSelectorEvent>;
}

/// @nodoc
class _$GroupingScreenSelectorEventCopyWithImpl<$Res,
        $Val extends GroupingScreenSelectorEvent>
    implements $GroupingScreenSelectorEventCopyWith<$Res> {
  _$GroupingScreenSelectorEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'GroupingScreenSelectorEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements GroupingScreenSelectorEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$$_GetWbsGroupingCopyWith<$Res> {
  factory _$$_GetWbsGroupingCopyWith(
          _$_GetWbsGrouping value, $Res Function(_$_GetWbsGrouping) then) =
      __$$_GetWbsGroupingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetWbsGroupingCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorEventCopyWithImpl<$Res, _$_GetWbsGrouping>
    implements _$$_GetWbsGroupingCopyWith<$Res> {
  __$$_GetWbsGroupingCopyWithImpl(
      _$_GetWbsGrouping _value, $Res Function(_$_GetWbsGrouping) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetWbsGrouping implements _GetWbsGrouping {
  const _$_GetWbsGrouping();

  @override
  String toString() {
    return 'GroupingScreenSelectorEvent.getWbsGrouping()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetWbsGrouping);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) {
    return getWbsGrouping();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) {
    return getWbsGrouping?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (getWbsGrouping != null) {
      return getWbsGrouping();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) {
    return getWbsGrouping(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) {
    return getWbsGrouping?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (getWbsGrouping != null) {
      return getWbsGrouping(this);
    }
    return orElse();
  }
}

abstract class _GetWbsGrouping implements GroupingScreenSelectorEvent {
  const factory _GetWbsGrouping() = _$_GetWbsGrouping;
}

/// @nodoc
abstract class _$$_SelectMultiLevelCopyWith<$Res> {
  factory _$$_SelectMultiLevelCopyWith(
          _$_SelectMultiLevel value, $Res Function(_$_SelectMultiLevel) then) =
      __$$_SelectMultiLevelCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SelectMultiLevelCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorEventCopyWithImpl<$Res, _$_SelectMultiLevel>
    implements _$$_SelectMultiLevelCopyWith<$Res> {
  __$$_SelectMultiLevelCopyWithImpl(
      _$_SelectMultiLevel _value, $Res Function(_$_SelectMultiLevel) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SelectMultiLevel implements _SelectMultiLevel {
  const _$_SelectMultiLevel();

  @override
  String toString() {
    return 'GroupingScreenSelectorEvent.selectMultiLevel()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SelectMultiLevel);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) {
    return selectMultiLevel();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) {
    return selectMultiLevel?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (selectMultiLevel != null) {
      return selectMultiLevel();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) {
    return selectMultiLevel(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) {
    return selectMultiLevel?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (selectMultiLevel != null) {
      return selectMultiLevel(this);
    }
    return orElse();
  }
}

abstract class _SelectMultiLevel implements GroupingScreenSelectorEvent {
  const factory _SelectMultiLevel() = _$_SelectMultiLevel;
}

/// @nodoc
abstract class _$$_FilterMultiLevelGroupingCopyWith<$Res> {
  factory _$$_FilterMultiLevelGroupingCopyWith(
          _$_FilterMultiLevelGrouping value,
          $Res Function(_$_FilterMultiLevelGrouping) then) =
      __$$_FilterMultiLevelGroupingCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {Map<int, ActivityFieldDTO>? groupKeys,
      Set<MultiLevelSortDTO>? sortParams});
}

/// @nodoc
class __$$_FilterMultiLevelGroupingCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorEventCopyWithImpl<$Res,
        _$_FilterMultiLevelGrouping>
    implements _$$_FilterMultiLevelGroupingCopyWith<$Res> {
  __$$_FilterMultiLevelGroupingCopyWithImpl(_$_FilterMultiLevelGrouping _value,
      $Res Function(_$_FilterMultiLevelGrouping) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? groupKeys = freezed,
    Object? sortParams = freezed,
  }) {
    return _then(_$_FilterMultiLevelGrouping(
      groupKeys: freezed == groupKeys
          ? _value._groupKeys
          : groupKeys // ignore: cast_nullable_to_non_nullable
              as Map<int, ActivityFieldDTO>?,
      sortParams: freezed == sortParams
          ? _value._sortParams
          : sortParams // ignore: cast_nullable_to_non_nullable
              as Set<MultiLevelSortDTO>?,
    ));
  }
}

/// @nodoc

class _$_FilterMultiLevelGrouping implements _FilterMultiLevelGrouping {
  const _$_FilterMultiLevelGrouping(
      {required final Map<int, ActivityFieldDTO>? groupKeys,
      required final Set<MultiLevelSortDTO>? sortParams})
      : _groupKeys = groupKeys,
        _sortParams = sortParams;

  final Map<int, ActivityFieldDTO>? _groupKeys;
  @override
  Map<int, ActivityFieldDTO>? get groupKeys {
    final value = _groupKeys;
    if (value == null) return null;
    if (_groupKeys is EqualUnmodifiableMapView) return _groupKeys;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Set<MultiLevelSortDTO>? _sortParams;
  @override
  Set<MultiLevelSortDTO>? get sortParams {
    final value = _sortParams;
    if (value == null) return null;
    if (_sortParams is EqualUnmodifiableSetView) return _sortParams;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(value);
  }

  @override
  String toString() {
    return 'GroupingScreenSelectorEvent.filterMultiLevelGrouping(groupKeys: $groupKeys, sortParams: $sortParams)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FilterMultiLevelGrouping &&
            const DeepCollectionEquality()
                .equals(other._groupKeys, _groupKeys) &&
            const DeepCollectionEquality()
                .equals(other._sortParams, _sortParams));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_groupKeys),
      const DeepCollectionEquality().hash(_sortParams));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FilterMultiLevelGroupingCopyWith<_$_FilterMultiLevelGrouping>
      get copyWith => __$$_FilterMultiLevelGroupingCopyWithImpl<
          _$_FilterMultiLevelGrouping>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) {
    return filterMultiLevelGrouping(groupKeys, sortParams);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) {
    return filterMultiLevelGrouping?.call(groupKeys, sortParams);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (filterMultiLevelGrouping != null) {
      return filterMultiLevelGrouping(groupKeys, sortParams);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) {
    return filterMultiLevelGrouping(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) {
    return filterMultiLevelGrouping?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (filterMultiLevelGrouping != null) {
      return filterMultiLevelGrouping(this);
    }
    return orElse();
  }
}

abstract class _FilterMultiLevelGrouping
    implements GroupingScreenSelectorEvent {
  const factory _FilterMultiLevelGrouping(
          {required final Map<int, ActivityFieldDTO>? groupKeys,
          required final Set<MultiLevelSortDTO>? sortParams}) =
      _$_FilterMultiLevelGrouping;

  Map<int, ActivityFieldDTO>? get groupKeys;
  Set<MultiLevelSortDTO>? get sortParams;
  @JsonKey(ignore: true)
  _$$_FilterMultiLevelGroupingCopyWith<_$_FilterMultiLevelGrouping>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ClearMultiLevelGroupingCopyWith<$Res> {
  factory _$$_ClearMultiLevelGroupingCopyWith(_$_ClearMultiLevelGrouping value,
          $Res Function(_$_ClearMultiLevelGrouping) then) =
      __$$_ClearMultiLevelGroupingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ClearMultiLevelGroupingCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorEventCopyWithImpl<$Res,
        _$_ClearMultiLevelGrouping>
    implements _$$_ClearMultiLevelGroupingCopyWith<$Res> {
  __$$_ClearMultiLevelGroupingCopyWithImpl(_$_ClearMultiLevelGrouping _value,
      $Res Function(_$_ClearMultiLevelGrouping) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ClearMultiLevelGrouping implements _ClearMultiLevelGrouping {
  const _$_ClearMultiLevelGrouping();

  @override
  String toString() {
    return 'GroupingScreenSelectorEvent.clearMultiLevelGrouping()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ClearMultiLevelGrouping);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() getWbsGrouping,
    required TResult Function() selectMultiLevel,
    required TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)
        filterMultiLevelGrouping,
    required TResult Function() clearMultiLevelGrouping,
  }) {
    return clearMultiLevelGrouping();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? getWbsGrouping,
    TResult? Function()? selectMultiLevel,
    TResult? Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult? Function()? clearMultiLevelGrouping,
  }) {
    return clearMultiLevelGrouping?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? getWbsGrouping,
    TResult Function()? selectMultiLevel,
    TResult Function(Map<int, ActivityFieldDTO>? groupKeys,
            Set<MultiLevelSortDTO>? sortParams)?
        filterMultiLevelGrouping,
    TResult Function()? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (clearMultiLevelGrouping != null) {
      return clearMultiLevelGrouping();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GetWbsGrouping value) getWbsGrouping,
    required TResult Function(_SelectMultiLevel value) selectMultiLevel,
    required TResult Function(_FilterMultiLevelGrouping value)
        filterMultiLevelGrouping,
    required TResult Function(_ClearMultiLevelGrouping value)
        clearMultiLevelGrouping,
  }) {
    return clearMultiLevelGrouping(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult? Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult? Function(_FilterMultiLevelGrouping value)?
        filterMultiLevelGrouping,
    TResult? Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
  }) {
    return clearMultiLevelGrouping?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GetWbsGrouping value)? getWbsGrouping,
    TResult Function(_SelectMultiLevel value)? selectMultiLevel,
    TResult Function(_FilterMultiLevelGrouping value)? filterMultiLevelGrouping,
    TResult Function(_ClearMultiLevelGrouping value)? clearMultiLevelGrouping,
    required TResult orElse(),
  }) {
    if (clearMultiLevelGrouping != null) {
      return clearMultiLevelGrouping(this);
    }
    return orElse();
  }
}

abstract class _ClearMultiLevelGrouping implements GroupingScreenSelectorEvent {
  const factory _ClearMultiLevelGrouping() = _$_ClearMultiLevelGrouping;
}

/// @nodoc
mixin _$GroupingScreenSelectorState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() wbsSelected,
    required TResult Function() multiLevelConfigurePopUp,
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        multiLevelSelected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? wbsSelected,
    TResult? Function()? multiLevelConfigurePopUp,
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? wbsSelected,
    TResult Function()? multiLevelConfigurePopUp,
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_WbsSelected value) wbsSelected,
    required TResult Function(_MultiLevelConfigurePopUp value)
        multiLevelConfigurePopUp,
    required TResult Function(_MultiLevelSelected value) multiLevelSelected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_WbsSelected value)? wbsSelected,
    TResult? Function(_MultiLevelConfigurePopUp value)?
        multiLevelConfigurePopUp,
    TResult? Function(_MultiLevelSelected value)? multiLevelSelected,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_WbsSelected value)? wbsSelected,
    TResult Function(_MultiLevelConfigurePopUp value)? multiLevelConfigurePopUp,
    TResult Function(_MultiLevelSelected value)? multiLevelSelected,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GroupingScreenSelectorStateCopyWith<$Res> {
  factory $GroupingScreenSelectorStateCopyWith(
          GroupingScreenSelectorState value,
          $Res Function(GroupingScreenSelectorState) then) =
      _$GroupingScreenSelectorStateCopyWithImpl<$Res,
          GroupingScreenSelectorState>;
}

/// @nodoc
class _$GroupingScreenSelectorStateCopyWithImpl<$Res,
        $Val extends GroupingScreenSelectorState>
    implements $GroupingScreenSelectorStateCopyWith<$Res> {
  _$GroupingScreenSelectorStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'GroupingScreenSelectorState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() wbsSelected,
    required TResult Function() multiLevelConfigurePopUp,
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        multiLevelSelected,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? wbsSelected,
    TResult? Function()? multiLevelConfigurePopUp,
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? wbsSelected,
    TResult Function()? multiLevelConfigurePopUp,
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_WbsSelected value) wbsSelected,
    required TResult Function(_MultiLevelConfigurePopUp value)
        multiLevelConfigurePopUp,
    required TResult Function(_MultiLevelSelected value) multiLevelSelected,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_WbsSelected value)? wbsSelected,
    TResult? Function(_MultiLevelConfigurePopUp value)?
        multiLevelConfigurePopUp,
    TResult? Function(_MultiLevelSelected value)? multiLevelSelected,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_WbsSelected value)? wbsSelected,
    TResult Function(_MultiLevelConfigurePopUp value)? multiLevelConfigurePopUp,
    TResult Function(_MultiLevelSelected value)? multiLevelSelected,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements GroupingScreenSelectorState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_WbsSelectedCopyWith<$Res> {
  factory _$$_WbsSelectedCopyWith(
          _$_WbsSelected value, $Res Function(_$_WbsSelected) then) =
      __$$_WbsSelectedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_WbsSelectedCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorStateCopyWithImpl<$Res, _$_WbsSelected>
    implements _$$_WbsSelectedCopyWith<$Res> {
  __$$_WbsSelectedCopyWithImpl(
      _$_WbsSelected _value, $Res Function(_$_WbsSelected) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_WbsSelected implements _WbsSelected {
  const _$_WbsSelected();

  @override
  String toString() {
    return 'GroupingScreenSelectorState.wbsSelected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_WbsSelected);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() wbsSelected,
    required TResult Function() multiLevelConfigurePopUp,
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        multiLevelSelected,
  }) {
    return wbsSelected();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? wbsSelected,
    TResult? Function()? multiLevelConfigurePopUp,
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
  }) {
    return wbsSelected?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? wbsSelected,
    TResult Function()? multiLevelConfigurePopUp,
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
    required TResult orElse(),
  }) {
    if (wbsSelected != null) {
      return wbsSelected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_WbsSelected value) wbsSelected,
    required TResult Function(_MultiLevelConfigurePopUp value)
        multiLevelConfigurePopUp,
    required TResult Function(_MultiLevelSelected value) multiLevelSelected,
  }) {
    return wbsSelected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_WbsSelected value)? wbsSelected,
    TResult? Function(_MultiLevelConfigurePopUp value)?
        multiLevelConfigurePopUp,
    TResult? Function(_MultiLevelSelected value)? multiLevelSelected,
  }) {
    return wbsSelected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_WbsSelected value)? wbsSelected,
    TResult Function(_MultiLevelConfigurePopUp value)? multiLevelConfigurePopUp,
    TResult Function(_MultiLevelSelected value)? multiLevelSelected,
    required TResult orElse(),
  }) {
    if (wbsSelected != null) {
      return wbsSelected(this);
    }
    return orElse();
  }
}

abstract class _WbsSelected implements GroupingScreenSelectorState {
  const factory _WbsSelected() = _$_WbsSelected;
}

/// @nodoc
abstract class _$$_MultiLevelConfigurePopUpCopyWith<$Res> {
  factory _$$_MultiLevelConfigurePopUpCopyWith(
          _$_MultiLevelConfigurePopUp value,
          $Res Function(_$_MultiLevelConfigurePopUp) then) =
      __$$_MultiLevelConfigurePopUpCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_MultiLevelConfigurePopUpCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorStateCopyWithImpl<$Res,
        _$_MultiLevelConfigurePopUp>
    implements _$$_MultiLevelConfigurePopUpCopyWith<$Res> {
  __$$_MultiLevelConfigurePopUpCopyWithImpl(_$_MultiLevelConfigurePopUp _value,
      $Res Function(_$_MultiLevelConfigurePopUp) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_MultiLevelConfigurePopUp implements _MultiLevelConfigurePopUp {
  const _$_MultiLevelConfigurePopUp();

  @override
  String toString() {
    return 'GroupingScreenSelectorState.multiLevelConfigurePopUp()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MultiLevelConfigurePopUp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() wbsSelected,
    required TResult Function() multiLevelConfigurePopUp,
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        multiLevelSelected,
  }) {
    return multiLevelConfigurePopUp();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? wbsSelected,
    TResult? Function()? multiLevelConfigurePopUp,
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
  }) {
    return multiLevelConfigurePopUp?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? wbsSelected,
    TResult Function()? multiLevelConfigurePopUp,
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
    required TResult orElse(),
  }) {
    if (multiLevelConfigurePopUp != null) {
      return multiLevelConfigurePopUp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_WbsSelected value) wbsSelected,
    required TResult Function(_MultiLevelConfigurePopUp value)
        multiLevelConfigurePopUp,
    required TResult Function(_MultiLevelSelected value) multiLevelSelected,
  }) {
    return multiLevelConfigurePopUp(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_WbsSelected value)? wbsSelected,
    TResult? Function(_MultiLevelConfigurePopUp value)?
        multiLevelConfigurePopUp,
    TResult? Function(_MultiLevelSelected value)? multiLevelSelected,
  }) {
    return multiLevelConfigurePopUp?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_WbsSelected value)? wbsSelected,
    TResult Function(_MultiLevelConfigurePopUp value)? multiLevelConfigurePopUp,
    TResult Function(_MultiLevelSelected value)? multiLevelSelected,
    required TResult orElse(),
  }) {
    if (multiLevelConfigurePopUp != null) {
      return multiLevelConfigurePopUp(this);
    }
    return orElse();
  }
}

abstract class _MultiLevelConfigurePopUp
    implements GroupingScreenSelectorState {
  const factory _MultiLevelConfigurePopUp() = _$_MultiLevelConfigurePopUp;
}

/// @nodoc
abstract class _$$_MultiLevelSelectedCopyWith<$Res> {
  factory _$$_MultiLevelSelectedCopyWith(_$_MultiLevelSelected value,
          $Res Function(_$_MultiLevelSelected) then) =
      __$$_MultiLevelSelectedCopyWithImpl<$Res>;
  @useResult
  $Res call({Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable});
}

/// @nodoc
class __$$_MultiLevelSelectedCopyWithImpl<$Res>
    extends _$GroupingScreenSelectorStateCopyWithImpl<$Res,
        _$_MultiLevelSelected> implements _$$_MultiLevelSelectedCopyWith<$Res> {
  __$$_MultiLevelSelectedCopyWithImpl(
      _$_MultiLevelSelected _value, $Res Function(_$_MultiLevelSelected) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? groupKeys = freezed,
    Object? isPrevGroupAvailable = null,
  }) {
    return _then(_$_MultiLevelSelected(
      groupKeys: freezed == groupKeys
          ? _value._groupKeys
          : groupKeys // ignore: cast_nullable_to_non_nullable
              as Map<int, ActivityFieldDTO>?,
      isPrevGroupAvailable: null == isPrevGroupAvailable
          ? _value.isPrevGroupAvailable
          : isPrevGroupAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_MultiLevelSelected implements _MultiLevelSelected {
  const _$_MultiLevelSelected(
      {required final Map<int, ActivityFieldDTO>? groupKeys,
      required this.isPrevGroupAvailable})
      : _groupKeys = groupKeys;

  final Map<int, ActivityFieldDTO>? _groupKeys;
  @override
  Map<int, ActivityFieldDTO>? get groupKeys {
    final value = _groupKeys;
    if (value == null) return null;
    if (_groupKeys is EqualUnmodifiableMapView) return _groupKeys;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final bool isPrevGroupAvailable;

  @override
  String toString() {
    return 'GroupingScreenSelectorState.multiLevelSelected(groupKeys: $groupKeys, isPrevGroupAvailable: $isPrevGroupAvailable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MultiLevelSelected &&
            const DeepCollectionEquality()
                .equals(other._groupKeys, _groupKeys) &&
            (identical(other.isPrevGroupAvailable, isPrevGroupAvailable) ||
                other.isPrevGroupAvailable == isPrevGroupAvailable));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_groupKeys), isPrevGroupAvailable);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MultiLevelSelectedCopyWith<_$_MultiLevelSelected> get copyWith =>
      __$$_MultiLevelSelectedCopyWithImpl<_$_MultiLevelSelected>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() wbsSelected,
    required TResult Function() multiLevelConfigurePopUp,
    required TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)
        multiLevelSelected,
  }) {
    return multiLevelSelected(groupKeys, isPrevGroupAvailable);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? wbsSelected,
    TResult? Function()? multiLevelConfigurePopUp,
    TResult? Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
  }) {
    return multiLevelSelected?.call(groupKeys, isPrevGroupAvailable);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? wbsSelected,
    TResult Function()? multiLevelConfigurePopUp,
    TResult Function(
            Map<int, ActivityFieldDTO>? groupKeys, bool isPrevGroupAvailable)?
        multiLevelSelected,
    required TResult orElse(),
  }) {
    if (multiLevelSelected != null) {
      return multiLevelSelected(groupKeys, isPrevGroupAvailable);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_WbsSelected value) wbsSelected,
    required TResult Function(_MultiLevelConfigurePopUp value)
        multiLevelConfigurePopUp,
    required TResult Function(_MultiLevelSelected value) multiLevelSelected,
  }) {
    return multiLevelSelected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_WbsSelected value)? wbsSelected,
    TResult? Function(_MultiLevelConfigurePopUp value)?
        multiLevelConfigurePopUp,
    TResult? Function(_MultiLevelSelected value)? multiLevelSelected,
  }) {
    return multiLevelSelected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_WbsSelected value)? wbsSelected,
    TResult Function(_MultiLevelConfigurePopUp value)? multiLevelConfigurePopUp,
    TResult Function(_MultiLevelSelected value)? multiLevelSelected,
    required TResult orElse(),
  }) {
    if (multiLevelSelected != null) {
      return multiLevelSelected(this);
    }
    return orElse();
  }
}

abstract class _MultiLevelSelected implements GroupingScreenSelectorState {
  const factory _MultiLevelSelected(
      {required final Map<int, ActivityFieldDTO>? groupKeys,
      required final bool isPrevGroupAvailable}) = _$_MultiLevelSelected;

  Map<int, ActivityFieldDTO>? get groupKeys;
  bool get isPrevGroupAvailable;
  @JsonKey(ignore: true)
  _$$_MultiLevelSelectedCopyWith<_$_MultiLevelSelected> get copyWith =>
      throw _privateConstructorUsedError;
}
