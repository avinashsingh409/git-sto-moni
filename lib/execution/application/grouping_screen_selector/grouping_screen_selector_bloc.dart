import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/selected_grouping_sort.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';

part 'grouping_screen_selector_event.dart';
part 'grouping_screen_selector_state.dart';
part 'grouping_screen_selector_bloc.freezed.dart';

@injectable
class GroupingScreenSelectorBloc extends Bloc<GroupingScreenSelectorEvent, GroupingScreenSelectorState> {
  final IActivityGroupingRepository activityGroupingRepository;
  GroupingScreenSelectorBloc(this.activityGroupingRepository) : super(const _Initial()) {
    on<GroupingScreenSelectorEvent>((event, emit) async {
      await event.when(
        started: () async => emit(const GroupingScreenSelectorState.initial()),
        getWbsGrouping: () async {
          emit(const GroupingScreenSelectorState.initial());
          emit(const GroupingScreenSelectorState.wbsSelected());
        },
        filterMultiLevelGrouping: (groupKeys, searchParam) async {
          emit(const GroupingScreenSelectorState.initial());
          if (searchParam != null && searchParam.isNotEmpty) {
            SelectedMultiLevelSort().updateSelectedMultiLevelSort(selectedMultiLevelSort: searchParam.first);
          }
          emit(GroupingScreenSelectorState.multiLevelSelected(groupKeys: groupKeys, isPrevGroupAvailable: false));
        },
        selectMultiLevel: () async {
          emit(const GroupingScreenSelectorState.initial());
          final isPrevGroupAvailable = await activityGroupingRepository.isPrevGroupAvailable();
          if (isPrevGroupAvailable) {
            emit(GroupingScreenSelectorState.multiLevelSelected(groupKeys: null, isPrevGroupAvailable: isPrevGroupAvailable));
          } else {
            emit(const GroupingScreenSelectorState.multiLevelConfigurePopUp());
          }
        },
        clearMultiLevelGrouping: () async {
          await activityGroupingRepository.clearMultiLevelGrouping();
          SelectedMultiLevelSort().resetSelectedMultiLevelSort();
          emit(const GroupingScreenSelectorState.initial());
        },
      );
    });
    add(const GroupingScreenSelectorEvent.started());
  }
}
