part of 'comment_bloc.dart';

@freezed
class CommentEvent with _$CommentEvent {
  const factory CommentEvent.getComments({required int object}) = _GetComments;
  const factory CommentEvent.addComment({required int object, required String comment, required ActivityDTO activityDTO}) =
      _AddComment;
}
