import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/comment/repository/turnaround_comment_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';

part 'comment_event.dart';
part 'comment_state.dart';
part 'comment_bloc.freezed.dart';

@injectable
class CommentBloc extends Bloc<CommentEvent, CommentState> {
  final ITurnaroundActivitiesCommentRepository commentRepository;
  final ISessionRepository sessionRepository;
  final ITurnAroundActivityRepository turnAroundActivityRepository;

  //Stream for all Comments
  final _allCommentsBehaviorSubject = BehaviorSubject<List<CommentDTO>>();
  Stream<List<CommentDTO>> getAllComments() => _allCommentsBehaviorSubject.stream;

  //Stream for my Comments
  final _myCommentsBehaviorSubject = BehaviorSubject<List<CommentDTO>>();
  Stream<List<CommentDTO>> getMyComments() => _myCommentsBehaviorSubject.stream;

  CommentBloc(this.commentRepository, this.sessionRepository, this.turnAroundActivityRepository) : super(const _Initial()) {
    on<CommentEvent>((event, emit) async {
      await event.when(getComments: (objectId) async {
        final userInformation = sessionRepository.getSession();
        userInformation.fold(
          (session) async {
            final allCommentsStream = commentRepository.getAllCachedComments(objectId: objectId);
            allCommentsStream.listen((event) {
              _allCommentsBehaviorSubject.add(event);
            });
            final myCommentsStream =
                commentRepository.getMyCachedComments(objectId: objectId, userInformation: session.userInformation);
            myCommentsStream.listen((event) {
              _myCommentsBehaviorSubject.add(event);
            });
          },
          (exception) {
            //Todo
          },
        );
      }, addComment: (objectId, comment, activityDTO) async {
        final userInformation = sessionRepository.getSession();
        userInformation.fold(
          (session) async {
            await commentRepository.createComment(objectId: objectId, userInformation: session.userInformation, comment: comment);
            final updateActivityModel = UpdatedActivityDTO(
              id: activityDTO.id!,
              dateModified: DateTime.now(),
              objectId: activityDTO.objectId,
              commentCount: (activityDTO.commentCount ?? 0) + 1,
            );
            await turnAroundActivityRepository.updateActivity(
              currentActivity: activityDTO,
              updateActivityModel: updateActivityModel,
            );
          },
          (exception) {
            //Todo
          },
        );
      });
    });
  }

  @override
  Future<void> close() {
    _allCommentsBehaviorSubject.close();
    _myCommentsBehaviorSubject.close();
    return super.close();
  }
}
