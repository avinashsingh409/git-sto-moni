import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/domain/execution_search/repository/execution_search_repository.dart';

part 'execution_search_event.dart';
part 'execution_search_state.dart';
part 'execution_search_bloc.freezed.dart';

@injectable
class ExecutionSearchBloc extends Bloc<ExecutionSearchEvent, ExecutionSearchState> {
  List<ActivityCardItemModel> activityCardModels = [];
  final IExecutionSearchRepository searchRepository;
  ExecutionSearchBloc(this.searchRepository) : super(const _Initial()) {
    on<ExecutionSearchEvent>((event, emit) async {
      await event.when(search: (activityKeyword) async {
        emit(const ExecutionSearchState.loading());
        final searchActivitiesStream = searchRepository.getSearchedTurnAroundEvent(activityKeyword: activityKeyword);
        await emit.onEach(
          searchActivitiesStream.stream,
          onData: (activities) {
            List<ActivityCardItemModel> activityCardModels = [];
            emit(const ExecutionSearchState.initial());
            for (var activity in activities) {
              activityCardModels.add(ActivityCardItemModel.fromModel(activityDTO: activity));
            }
            activityCardModels.sort((a, b) => a.activity.toLowerCase().compareTo(b.activity.toLowerCase()));
            emit(ExecutionSearchState.success(results: activityCardModels));
          },
        ).catchError((error) {
          emit(const ExecutionSearchState.error(errorMsg: "Unexpected Error"));
        });
      });
    });
  }
}
