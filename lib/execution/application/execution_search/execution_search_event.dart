part of 'execution_search_bloc.dart';

@freezed
class ExecutionSearchEvent with _$ExecutionSearchEvent {
  const factory ExecutionSearchEvent.search({required String activityKeyword}) = _Search;
}
