part of 'execution_search_bloc.dart';

@freezed
class ExecutionSearchState with _$ExecutionSearchState {
  const factory ExecutionSearchState.initial() = _Initial;
  const factory ExecutionSearchState.loading() = _Loading;
  const factory ExecutionSearchState.success({required List<ActivityCardItemModel> results}) = _Success;
  const factory ExecutionSearchState.error({required String errorMsg}) = _Error;
}
