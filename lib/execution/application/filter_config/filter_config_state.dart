part of 'filter_config_bloc.dart';

@freezed
class FilterConfigState with _$FilterConfigState {
  const factory FilterConfigState.initial() = _Initial;
  const factory FilterConfigState.preConfiguredFiltersLoaded({required int lookaheadVal}) = _PreConfiguredFiltersLoaded;
}
