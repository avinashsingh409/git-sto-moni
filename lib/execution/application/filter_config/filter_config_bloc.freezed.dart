// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'filter_config_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FilterConfigEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getPreConfiguredFilter,
    required TResult Function(String updatedValue) updatePlannedDateFilter,
    required TResult Function(LookaheadBaseDateEnum baseDate) updateBaseDate,
    required TResult Function(LookaheadRangeDateEnum rangeDate) updateRangeDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getPreConfiguredFilter,
    TResult? Function(String updatedValue)? updatePlannedDateFilter,
    TResult? Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult? Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getPreConfiguredFilter,
    TResult Function(String updatedValue)? updatePlannedDateFilter,
    TResult Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPreConfiguredFilter value)
        getPreConfiguredFilter,
    required TResult Function(_UpdatePlannedDateFilter value)
        updatePlannedDateFilter,
    required TResult Function(_UpdateBaseDate value) updateBaseDate,
    required TResult Function(_UpdateRangeDate value) updateRangeDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult? Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult? Function(_UpdateBaseDate value)? updateBaseDate,
    TResult? Function(_UpdateRangeDate value)? updateRangeDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult Function(_UpdateBaseDate value)? updateBaseDate,
    TResult Function(_UpdateRangeDate value)? updateRangeDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilterConfigEventCopyWith<$Res> {
  factory $FilterConfigEventCopyWith(
          FilterConfigEvent value, $Res Function(FilterConfigEvent) then) =
      _$FilterConfigEventCopyWithImpl<$Res, FilterConfigEvent>;
}

/// @nodoc
class _$FilterConfigEventCopyWithImpl<$Res, $Val extends FilterConfigEvent>
    implements $FilterConfigEventCopyWith<$Res> {
  _$FilterConfigEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetPreConfiguredFilterCopyWith<$Res> {
  factory _$$_GetPreConfiguredFilterCopyWith(_$_GetPreConfiguredFilter value,
          $Res Function(_$_GetPreConfiguredFilter) then) =
      __$$_GetPreConfiguredFilterCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetPreConfiguredFilterCopyWithImpl<$Res>
    extends _$FilterConfigEventCopyWithImpl<$Res, _$_GetPreConfiguredFilter>
    implements _$$_GetPreConfiguredFilterCopyWith<$Res> {
  __$$_GetPreConfiguredFilterCopyWithImpl(_$_GetPreConfiguredFilter _value,
      $Res Function(_$_GetPreConfiguredFilter) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetPreConfiguredFilter implements _GetPreConfiguredFilter {
  const _$_GetPreConfiguredFilter();

  @override
  String toString() {
    return 'FilterConfigEvent.getPreConfiguredFilter()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetPreConfiguredFilter);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getPreConfiguredFilter,
    required TResult Function(String updatedValue) updatePlannedDateFilter,
    required TResult Function(LookaheadBaseDateEnum baseDate) updateBaseDate,
    required TResult Function(LookaheadRangeDateEnum rangeDate) updateRangeDate,
  }) {
    return getPreConfiguredFilter();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getPreConfiguredFilter,
    TResult? Function(String updatedValue)? updatePlannedDateFilter,
    TResult? Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult? Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
  }) {
    return getPreConfiguredFilter?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getPreConfiguredFilter,
    TResult Function(String updatedValue)? updatePlannedDateFilter,
    TResult Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (getPreConfiguredFilter != null) {
      return getPreConfiguredFilter();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPreConfiguredFilter value)
        getPreConfiguredFilter,
    required TResult Function(_UpdatePlannedDateFilter value)
        updatePlannedDateFilter,
    required TResult Function(_UpdateBaseDate value) updateBaseDate,
    required TResult Function(_UpdateRangeDate value) updateRangeDate,
  }) {
    return getPreConfiguredFilter(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult? Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult? Function(_UpdateBaseDate value)? updateBaseDate,
    TResult? Function(_UpdateRangeDate value)? updateRangeDate,
  }) {
    return getPreConfiguredFilter?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult Function(_UpdateBaseDate value)? updateBaseDate,
    TResult Function(_UpdateRangeDate value)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (getPreConfiguredFilter != null) {
      return getPreConfiguredFilter(this);
    }
    return orElse();
  }
}

abstract class _GetPreConfiguredFilter implements FilterConfigEvent {
  const factory _GetPreConfiguredFilter() = _$_GetPreConfiguredFilter;
}

/// @nodoc
abstract class _$$_UpdatePlannedDateFilterCopyWith<$Res> {
  factory _$$_UpdatePlannedDateFilterCopyWith(_$_UpdatePlannedDateFilter value,
          $Res Function(_$_UpdatePlannedDateFilter) then) =
      __$$_UpdatePlannedDateFilterCopyWithImpl<$Res>;
  @useResult
  $Res call({String updatedValue});
}

/// @nodoc
class __$$_UpdatePlannedDateFilterCopyWithImpl<$Res>
    extends _$FilterConfigEventCopyWithImpl<$Res, _$_UpdatePlannedDateFilter>
    implements _$$_UpdatePlannedDateFilterCopyWith<$Res> {
  __$$_UpdatePlannedDateFilterCopyWithImpl(_$_UpdatePlannedDateFilter _value,
      $Res Function(_$_UpdatePlannedDateFilter) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updatedValue = null,
  }) {
    return _then(_$_UpdatePlannedDateFilter(
      updatedValue: null == updatedValue
          ? _value.updatedValue
          : updatedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_UpdatePlannedDateFilter implements _UpdatePlannedDateFilter {
  const _$_UpdatePlannedDateFilter({required this.updatedValue});

  @override
  final String updatedValue;

  @override
  String toString() {
    return 'FilterConfigEvent.updatePlannedDateFilter(updatedValue: $updatedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdatePlannedDateFilter &&
            (identical(other.updatedValue, updatedValue) ||
                other.updatedValue == updatedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, updatedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdatePlannedDateFilterCopyWith<_$_UpdatePlannedDateFilter>
      get copyWith =>
          __$$_UpdatePlannedDateFilterCopyWithImpl<_$_UpdatePlannedDateFilter>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getPreConfiguredFilter,
    required TResult Function(String updatedValue) updatePlannedDateFilter,
    required TResult Function(LookaheadBaseDateEnum baseDate) updateBaseDate,
    required TResult Function(LookaheadRangeDateEnum rangeDate) updateRangeDate,
  }) {
    return updatePlannedDateFilter(updatedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getPreConfiguredFilter,
    TResult? Function(String updatedValue)? updatePlannedDateFilter,
    TResult? Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult? Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
  }) {
    return updatePlannedDateFilter?.call(updatedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getPreConfiguredFilter,
    TResult Function(String updatedValue)? updatePlannedDateFilter,
    TResult Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updatePlannedDateFilter != null) {
      return updatePlannedDateFilter(updatedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPreConfiguredFilter value)
        getPreConfiguredFilter,
    required TResult Function(_UpdatePlannedDateFilter value)
        updatePlannedDateFilter,
    required TResult Function(_UpdateBaseDate value) updateBaseDate,
    required TResult Function(_UpdateRangeDate value) updateRangeDate,
  }) {
    return updatePlannedDateFilter(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult? Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult? Function(_UpdateBaseDate value)? updateBaseDate,
    TResult? Function(_UpdateRangeDate value)? updateRangeDate,
  }) {
    return updatePlannedDateFilter?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult Function(_UpdateBaseDate value)? updateBaseDate,
    TResult Function(_UpdateRangeDate value)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updatePlannedDateFilter != null) {
      return updatePlannedDateFilter(this);
    }
    return orElse();
  }
}

abstract class _UpdatePlannedDateFilter implements FilterConfigEvent {
  const factory _UpdatePlannedDateFilter({required final String updatedValue}) =
      _$_UpdatePlannedDateFilter;

  String get updatedValue;
  @JsonKey(ignore: true)
  _$$_UpdatePlannedDateFilterCopyWith<_$_UpdatePlannedDateFilter>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateBaseDateCopyWith<$Res> {
  factory _$$_UpdateBaseDateCopyWith(
          _$_UpdateBaseDate value, $Res Function(_$_UpdateBaseDate) then) =
      __$$_UpdateBaseDateCopyWithImpl<$Res>;
  @useResult
  $Res call({LookaheadBaseDateEnum baseDate});
}

/// @nodoc
class __$$_UpdateBaseDateCopyWithImpl<$Res>
    extends _$FilterConfigEventCopyWithImpl<$Res, _$_UpdateBaseDate>
    implements _$$_UpdateBaseDateCopyWith<$Res> {
  __$$_UpdateBaseDateCopyWithImpl(
      _$_UpdateBaseDate _value, $Res Function(_$_UpdateBaseDate) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? baseDate = null,
  }) {
    return _then(_$_UpdateBaseDate(
      baseDate: null == baseDate
          ? _value.baseDate
          : baseDate // ignore: cast_nullable_to_non_nullable
              as LookaheadBaseDateEnum,
    ));
  }
}

/// @nodoc

class _$_UpdateBaseDate implements _UpdateBaseDate {
  const _$_UpdateBaseDate({required this.baseDate});

  @override
  final LookaheadBaseDateEnum baseDate;

  @override
  String toString() {
    return 'FilterConfigEvent.updateBaseDate(baseDate: $baseDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateBaseDate &&
            (identical(other.baseDate, baseDate) ||
                other.baseDate == baseDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, baseDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateBaseDateCopyWith<_$_UpdateBaseDate> get copyWith =>
      __$$_UpdateBaseDateCopyWithImpl<_$_UpdateBaseDate>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getPreConfiguredFilter,
    required TResult Function(String updatedValue) updatePlannedDateFilter,
    required TResult Function(LookaheadBaseDateEnum baseDate) updateBaseDate,
    required TResult Function(LookaheadRangeDateEnum rangeDate) updateRangeDate,
  }) {
    return updateBaseDate(baseDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getPreConfiguredFilter,
    TResult? Function(String updatedValue)? updatePlannedDateFilter,
    TResult? Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult? Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
  }) {
    return updateBaseDate?.call(baseDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getPreConfiguredFilter,
    TResult Function(String updatedValue)? updatePlannedDateFilter,
    TResult Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updateBaseDate != null) {
      return updateBaseDate(baseDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPreConfiguredFilter value)
        getPreConfiguredFilter,
    required TResult Function(_UpdatePlannedDateFilter value)
        updatePlannedDateFilter,
    required TResult Function(_UpdateBaseDate value) updateBaseDate,
    required TResult Function(_UpdateRangeDate value) updateRangeDate,
  }) {
    return updateBaseDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult? Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult? Function(_UpdateBaseDate value)? updateBaseDate,
    TResult? Function(_UpdateRangeDate value)? updateRangeDate,
  }) {
    return updateBaseDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult Function(_UpdateBaseDate value)? updateBaseDate,
    TResult Function(_UpdateRangeDate value)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updateBaseDate != null) {
      return updateBaseDate(this);
    }
    return orElse();
  }
}

abstract class _UpdateBaseDate implements FilterConfigEvent {
  const factory _UpdateBaseDate(
      {required final LookaheadBaseDateEnum baseDate}) = _$_UpdateBaseDate;

  LookaheadBaseDateEnum get baseDate;
  @JsonKey(ignore: true)
  _$$_UpdateBaseDateCopyWith<_$_UpdateBaseDate> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateRangeDateCopyWith<$Res> {
  factory _$$_UpdateRangeDateCopyWith(
          _$_UpdateRangeDate value, $Res Function(_$_UpdateRangeDate) then) =
      __$$_UpdateRangeDateCopyWithImpl<$Res>;
  @useResult
  $Res call({LookaheadRangeDateEnum rangeDate});
}

/// @nodoc
class __$$_UpdateRangeDateCopyWithImpl<$Res>
    extends _$FilterConfigEventCopyWithImpl<$Res, _$_UpdateRangeDate>
    implements _$$_UpdateRangeDateCopyWith<$Res> {
  __$$_UpdateRangeDateCopyWithImpl(
      _$_UpdateRangeDate _value, $Res Function(_$_UpdateRangeDate) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? rangeDate = null,
  }) {
    return _then(_$_UpdateRangeDate(
      rangeDate: null == rangeDate
          ? _value.rangeDate
          : rangeDate // ignore: cast_nullable_to_non_nullable
              as LookaheadRangeDateEnum,
    ));
  }
}

/// @nodoc

class _$_UpdateRangeDate implements _UpdateRangeDate {
  const _$_UpdateRangeDate({required this.rangeDate});

  @override
  final LookaheadRangeDateEnum rangeDate;

  @override
  String toString() {
    return 'FilterConfigEvent.updateRangeDate(rangeDate: $rangeDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateRangeDate &&
            (identical(other.rangeDate, rangeDate) ||
                other.rangeDate == rangeDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, rangeDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateRangeDateCopyWith<_$_UpdateRangeDate> get copyWith =>
      __$$_UpdateRangeDateCopyWithImpl<_$_UpdateRangeDate>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getPreConfiguredFilter,
    required TResult Function(String updatedValue) updatePlannedDateFilter,
    required TResult Function(LookaheadBaseDateEnum baseDate) updateBaseDate,
    required TResult Function(LookaheadRangeDateEnum rangeDate) updateRangeDate,
  }) {
    return updateRangeDate(rangeDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getPreConfiguredFilter,
    TResult? Function(String updatedValue)? updatePlannedDateFilter,
    TResult? Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult? Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
  }) {
    return updateRangeDate?.call(rangeDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getPreConfiguredFilter,
    TResult Function(String updatedValue)? updatePlannedDateFilter,
    TResult Function(LookaheadBaseDateEnum baseDate)? updateBaseDate,
    TResult Function(LookaheadRangeDateEnum rangeDate)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updateRangeDate != null) {
      return updateRangeDate(rangeDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetPreConfiguredFilter value)
        getPreConfiguredFilter,
    required TResult Function(_UpdatePlannedDateFilter value)
        updatePlannedDateFilter,
    required TResult Function(_UpdateBaseDate value) updateBaseDate,
    required TResult Function(_UpdateRangeDate value) updateRangeDate,
  }) {
    return updateRangeDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult? Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult? Function(_UpdateBaseDate value)? updateBaseDate,
    TResult? Function(_UpdateRangeDate value)? updateRangeDate,
  }) {
    return updateRangeDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetPreConfiguredFilter value)? getPreConfiguredFilter,
    TResult Function(_UpdatePlannedDateFilter value)? updatePlannedDateFilter,
    TResult Function(_UpdateBaseDate value)? updateBaseDate,
    TResult Function(_UpdateRangeDate value)? updateRangeDate,
    required TResult orElse(),
  }) {
    if (updateRangeDate != null) {
      return updateRangeDate(this);
    }
    return orElse();
  }
}

abstract class _UpdateRangeDate implements FilterConfigEvent {
  const factory _UpdateRangeDate(
      {required final LookaheadRangeDateEnum rangeDate}) = _$_UpdateRangeDate;

  LookaheadRangeDateEnum get rangeDate;
  @JsonKey(ignore: true)
  _$$_UpdateRangeDateCopyWith<_$_UpdateRangeDate> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FilterConfigState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int lookaheadVal) preConfiguredFiltersLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(int lookaheadVal)? preConfiguredFiltersLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int lookaheadVal)? preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PreConfiguredFiltersLoaded value)
        preConfiguredFiltersLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilterConfigStateCopyWith<$Res> {
  factory $FilterConfigStateCopyWith(
          FilterConfigState value, $Res Function(FilterConfigState) then) =
      _$FilterConfigStateCopyWithImpl<$Res, FilterConfigState>;
}

/// @nodoc
class _$FilterConfigStateCopyWithImpl<$Res, $Val extends FilterConfigState>
    implements $FilterConfigStateCopyWith<$Res> {
  _$FilterConfigStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$FilterConfigStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'FilterConfigState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int lookaheadVal) preConfiguredFiltersLoaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(int lookaheadVal)? preConfiguredFiltersLoaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int lookaheadVal)? preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PreConfiguredFiltersLoaded value)
        preConfiguredFiltersLoaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FilterConfigState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_PreConfiguredFiltersLoadedCopyWith<$Res> {
  factory _$$_PreConfiguredFiltersLoadedCopyWith(
          _$_PreConfiguredFiltersLoaded value,
          $Res Function(_$_PreConfiguredFiltersLoaded) then) =
      __$$_PreConfiguredFiltersLoadedCopyWithImpl<$Res>;
  @useResult
  $Res call({int lookaheadVal});
}

/// @nodoc
class __$$_PreConfiguredFiltersLoadedCopyWithImpl<$Res>
    extends _$FilterConfigStateCopyWithImpl<$Res, _$_PreConfiguredFiltersLoaded>
    implements _$$_PreConfiguredFiltersLoadedCopyWith<$Res> {
  __$$_PreConfiguredFiltersLoadedCopyWithImpl(
      _$_PreConfiguredFiltersLoaded _value,
      $Res Function(_$_PreConfiguredFiltersLoaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lookaheadVal = null,
  }) {
    return _then(_$_PreConfiguredFiltersLoaded(
      lookaheadVal: null == lookaheadVal
          ? _value.lookaheadVal
          : lookaheadVal // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_PreConfiguredFiltersLoaded implements _PreConfiguredFiltersLoaded {
  const _$_PreConfiguredFiltersLoaded({required this.lookaheadVal});

  @override
  final int lookaheadVal;

  @override
  String toString() {
    return 'FilterConfigState.preConfiguredFiltersLoaded(lookaheadVal: $lookaheadVal)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PreConfiguredFiltersLoaded &&
            (identical(other.lookaheadVal, lookaheadVal) ||
                other.lookaheadVal == lookaheadVal));
  }

  @override
  int get hashCode => Object.hash(runtimeType, lookaheadVal);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PreConfiguredFiltersLoadedCopyWith<_$_PreConfiguredFiltersLoaded>
      get copyWith => __$$_PreConfiguredFiltersLoadedCopyWithImpl<
          _$_PreConfiguredFiltersLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int lookaheadVal) preConfiguredFiltersLoaded,
  }) {
    return preConfiguredFiltersLoaded(lookaheadVal);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(int lookaheadVal)? preConfiguredFiltersLoaded,
  }) {
    return preConfiguredFiltersLoaded?.call(lookaheadVal);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int lookaheadVal)? preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) {
    if (preConfiguredFiltersLoaded != null) {
      return preConfiguredFiltersLoaded(lookaheadVal);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_PreConfiguredFiltersLoaded value)
        preConfiguredFiltersLoaded,
  }) {
    return preConfiguredFiltersLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
  }) {
    return preConfiguredFiltersLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_PreConfiguredFiltersLoaded value)?
        preConfiguredFiltersLoaded,
    required TResult orElse(),
  }) {
    if (preConfiguredFiltersLoaded != null) {
      return preConfiguredFiltersLoaded(this);
    }
    return orElse();
  }
}

abstract class _PreConfiguredFiltersLoaded implements FilterConfigState {
  const factory _PreConfiguredFiltersLoaded({required final int lookaheadVal}) =
      _$_PreConfiguredFiltersLoaded;

  int get lookaheadVal;
  @JsonKey(ignore: true)
  _$$_PreConfiguredFiltersLoadedCopyWith<_$_PreConfiguredFiltersLoaded>
      get copyWith => throw _privateConstructorUsedError;
}
