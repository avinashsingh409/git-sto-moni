part of 'filter_config_bloc.dart';

@freezed
class FilterConfigEvent with _$FilterConfigEvent {
  const factory FilterConfigEvent.getPreConfiguredFilter() = _GetPreConfiguredFilter;
  const factory FilterConfigEvent.updatePlannedDateFilter({required String updatedValue}) = _UpdatePlannedDateFilter;
  const factory FilterConfigEvent.updateBaseDate({required LookaheadBaseDateEnum baseDate}) = _UpdateBaseDate;
  const factory FilterConfigEvent.updateRangeDate({required LookaheadRangeDateEnum rangeDate}) = _UpdateRangeDate;
}
