import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/repository/filter_config_repository.dart';

part 'filter_config_event.dart';
part 'filter_config_state.dart';
part 'filter_config_bloc.freezed.dart';

@injectable
class FilterConfigBloc extends Bloc<FilterConfigEvent, FilterConfigState> {
  final IFilterConfigRepository filterConfigRepository;
  
  // index of lookahead hours slider
  final BehaviorSubject<int> _plannerDateIndexStream = BehaviorSubject<int>.seeded(1);
  Stream<int> getPlannerDateIndex() => _plannerDateIndexStream.stream;

  // base date dropdown
  final BehaviorSubject<LookaheadBaseDateEnum> _lookaheadBaseDateStream = BehaviorSubject<LookaheadBaseDateEnum>.seeded(LookaheadBaseDateEnum.todaysDate);
  Stream<LookaheadBaseDateEnum> getBaseDate() => _lookaheadBaseDateStream.stream;

  // range date dropdown
  final BehaviorSubject<LookaheadRangeDateEnum> _lookaheadRangeDateStream = BehaviorSubject<LookaheadRangeDateEnum>.seeded(LookaheadRangeDateEnum.plannedStartDate);
  Stream<LookaheadRangeDateEnum> getRangeDate() => _lookaheadRangeDateStream.stream;

  FilterConfigBloc(this.filterConfigRepository) : super(const _Initial()) {
    on<FilterConfigEvent>((event, emit) async {
      await event.when(
        getPreConfiguredFilter: () async {
          final filters = await filterConfigRepository.getPreConfiguredFilters();
          for (var element in filters) {
            if (element.filterType == FilterTypeEnum.lookahead) {
              var tempPlannedDateFilterHour = double.parse(element.value ?? '1.0').toInt();
              _plannerDateIndexStream.add(tempPlannedDateFilterHour);
              _lookaheadBaseDateStream.add(element.baseDate ?? LookaheadBaseDateEnum.todaysDate);
              _lookaheadRangeDateStream.add(element.rangeDate ?? LookaheadRangeDateEnum.plannedStartDate);
            }
          }
        },
        updatePlannedDateFilter: (updatedValue) async {
          await filterConfigRepository.updateFilter(updatedVal: updatedValue, filterType: FilterTypeEnum.lookahead);
        },
        updateBaseDate: (LookaheadBaseDateEnum baseDate) async {
          await filterConfigRepository.updateBaseDate(baseDate: baseDate);
        },
        updateRangeDate: (LookaheadRangeDateEnum rangeDate) async {
          await filterConfigRepository.updateRangeDate(rangeDate: rangeDate);
        },
      );
    });
  }

  void updatePlannerIndex({required int updatedIndex}) {
    _plannerDateIndexStream.add(updatedIndex);
  }
}
