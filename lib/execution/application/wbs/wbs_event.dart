part of 'wbs_bloc.dart';

@freezed
class WbsEvent with _$WbsEvent {
  const factory WbsEvent.getInitialGrouping() = _GetInitialGrouping;
  const factory WbsEvent.getWbsGrouping({
    required TreeNodeDTO selectedNode,
    required int level,
  }) = _GetWbsGrouping;
}
