part of 'wbs_bloc.dart';

@freezed
class WbsState with _$WbsState {
  const factory WbsState.initial() = _Initial;
  const factory WbsState.loading() = _Loading;
  const factory WbsState.loaded({required List<TreeNodeDTO>? treeNodes}) = _Loaded;
  const factory WbsState.error({required String errorMsg}) = _Error;
}
