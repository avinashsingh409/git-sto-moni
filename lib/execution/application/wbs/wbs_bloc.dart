import 'dart:collection';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/wbs_bread_crumb_entry_item.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/repository/wbs_repository.dart';

part 'wbs_event.dart';
part 'wbs_state.dart';
part 'wbs_bloc.freezed.dart';

@injectable
class WbsBloc extends Bloc<WbsEvent, WbsState> {
  final IWbsRepository wbsRepository;
  int initialLevel = 0;
  late TreeNodeDTO currentNode;
  var selectedNodes = LinkedList<WbsBreadCrumbEntryItem>();
  WbsBloc(this.wbsRepository) : super(const _Initial()) {
    on<WbsEvent>((event, emit) async {
      await event.when(
        getInitialGrouping: () async {
          emit(const WbsState.loading());
          final treeNodes = await wbsRepository.getWbsTree(level: initialLevel, parentName: '');
          if (treeNodes != [] && treeNodes.isNotEmpty) {
            selectedNodes.addFirst(WbsBreadCrumbEntryItem(id: 0, treeNodeDTO: treeNodes[0]));
            emit(WbsState.loaded(treeNodes: treeNodes));
          } else {
            emit(const WbsState.error(errorMsg: "No WBS elements"));
          }
        },
        getWbsGrouping: (selectedNode, level) async {
          emit(const WbsState.loading());
          final treeNodes = await wbsRepository.getWbsTree(level: level, parentName: selectedNode.nodeName ?? '');
          if (selectedNode.nodeElementType == ElementTypeEnum.node) {
            selectedNodes.add(WbsBreadCrumbEntryItem(id: selectedNode.id!, treeNodeDTO: selectedNode));
          }
          emit(WbsState.loaded(treeNodes: treeNodes));
        },
      );
    });
  }

  void returnNewLinkedListAfterKId(int requestedId) {
    var newLinkedList = LinkedList<WbsBreadCrumbEntryItem>();
    for (WbsBreadCrumbEntryItem item in selectedNodes) {
      if (item.id != requestedId) {
        newLinkedList.add(WbsBreadCrumbEntryItem(id: item.id, treeNodeDTO: item.treeNodeDTO));
        break;
      }
    }
    selectedNodes.clear();
    selectedNodes = newLinkedList;
  }
}
