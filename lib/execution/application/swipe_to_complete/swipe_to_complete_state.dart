part of 'swipe_to_complete_bloc.dart';

@freezed
class SwipeToCompleteState with _$SwipeToCompleteState {
  const factory SwipeToCompleteState.initial() = _Initial;
}
