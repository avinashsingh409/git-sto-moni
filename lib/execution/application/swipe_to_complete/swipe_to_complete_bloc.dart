import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';
import 'package:sto_mobile_v2/common/domain/user_call/repository/user_call_repository.dart';
import 'package:sto_mobile_v2/common/screens/widgets/show_toast.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';

part 'swipe_to_complete_event.dart';
part 'swipe_to_complete_state.dart';
part 'swipe_to_complete_bloc.freezed.dart';

@injectable
class SwipeToCompleteBloc extends Bloc<SwipeToCompleteEvent, SwipeToCompleteState> {
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  final IUserCallRepository userCallRepository;
  final ITurnAroundEventRepository turnaroundEventRepository;

  SwipeToCompleteBloc(this.turnAroundActivityRepository, this.userCallRepository, this.turnaroundEventRepository)
      : super(const _Initial()) {
    on<SwipeToCompleteEvent>((event, emit) async {
      await event.when(swiped: (activityId, updateActivityModel) async {
        var isSwipeEnabled = true;
        final userIsSwipeEnabled = userCallRepository.getIsSwipeEnabled();
        turnaroundEventRepository
            .getTurnaroundEventSwipeEnabled()
            .fold((eventSwipeEnabled) => isSwipeEnabled = eventSwipeEnabled && userIsSwipeEnabled, (_) => isSwipeEnabled = false);
        if (isSwipeEnabled) {
          final activity = await turnAroundActivityRepository.getSwipeActivity(activityId: activityId);
          if (activity != null) {
            turnAroundActivityRepository.updateActivity(
              currentActivity: activity,
              updateActivityModel: updateActivityModel,
            );
          }
        } else {
          showErrorToast(isBannerRed: true, errorText: "Swipe is Disabled");
        }
      });
    });
  }
}
