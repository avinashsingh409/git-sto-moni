part of 'swipe_to_complete_bloc.dart';

@freezed
class SwipeToCompleteEvent with _$SwipeToCompleteEvent {
  const factory SwipeToCompleteEvent.swiped({required int activityId, required UpdatedActivityDTO updateActivityModel}) = _Swiped;
}
