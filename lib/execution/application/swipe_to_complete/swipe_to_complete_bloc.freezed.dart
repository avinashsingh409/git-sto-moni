// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'swipe_to_complete_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SwipeToCompleteEvent {
  int get activityId => throw _privateConstructorUsedError;
  UpdatedActivityDTO get updateActivityModel =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            int activityId, UpdatedActivityDTO updateActivityModel)
        swiped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int activityId, UpdatedActivityDTO updateActivityModel)?
        swiped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int activityId, UpdatedActivityDTO updateActivityModel)?
        swiped,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Swiped value) swiped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Swiped value)? swiped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Swiped value)? swiped,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SwipeToCompleteEventCopyWith<SwipeToCompleteEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SwipeToCompleteEventCopyWith<$Res> {
  factory $SwipeToCompleteEventCopyWith(SwipeToCompleteEvent value,
          $Res Function(SwipeToCompleteEvent) then) =
      _$SwipeToCompleteEventCopyWithImpl<$Res, SwipeToCompleteEvent>;
  @useResult
  $Res call({int activityId, UpdatedActivityDTO updateActivityModel});
}

/// @nodoc
class _$SwipeToCompleteEventCopyWithImpl<$Res,
        $Val extends SwipeToCompleteEvent>
    implements $SwipeToCompleteEventCopyWith<$Res> {
  _$SwipeToCompleteEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? activityId = null,
    Object? updateActivityModel = null,
  }) {
    return _then(_value.copyWith(
      activityId: null == activityId
          ? _value.activityId
          : activityId // ignore: cast_nullable_to_non_nullable
              as int,
      updateActivityModel: null == updateActivityModel
          ? _value.updateActivityModel
          : updateActivityModel // ignore: cast_nullable_to_non_nullable
              as UpdatedActivityDTO,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SwipedCopyWith<$Res>
    implements $SwipeToCompleteEventCopyWith<$Res> {
  factory _$$_SwipedCopyWith(_$_Swiped value, $Res Function(_$_Swiped) then) =
      __$$_SwipedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int activityId, UpdatedActivityDTO updateActivityModel});
}

/// @nodoc
class __$$_SwipedCopyWithImpl<$Res>
    extends _$SwipeToCompleteEventCopyWithImpl<$Res, _$_Swiped>
    implements _$$_SwipedCopyWith<$Res> {
  __$$_SwipedCopyWithImpl(_$_Swiped _value, $Res Function(_$_Swiped) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? activityId = null,
    Object? updateActivityModel = null,
  }) {
    return _then(_$_Swiped(
      activityId: null == activityId
          ? _value.activityId
          : activityId // ignore: cast_nullable_to_non_nullable
              as int,
      updateActivityModel: null == updateActivityModel
          ? _value.updateActivityModel
          : updateActivityModel // ignore: cast_nullable_to_non_nullable
              as UpdatedActivityDTO,
    ));
  }
}

/// @nodoc

class _$_Swiped implements _Swiped {
  const _$_Swiped(
      {required this.activityId, required this.updateActivityModel});

  @override
  final int activityId;
  @override
  final UpdatedActivityDTO updateActivityModel;

  @override
  String toString() {
    return 'SwipeToCompleteEvent.swiped(activityId: $activityId, updateActivityModel: $updateActivityModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Swiped &&
            (identical(other.activityId, activityId) ||
                other.activityId == activityId) &&
            (identical(other.updateActivityModel, updateActivityModel) ||
                other.updateActivityModel == updateActivityModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, activityId, updateActivityModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SwipedCopyWith<_$_Swiped> get copyWith =>
      __$$_SwipedCopyWithImpl<_$_Swiped>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            int activityId, UpdatedActivityDTO updateActivityModel)
        swiped,
  }) {
    return swiped(activityId, updateActivityModel);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int activityId, UpdatedActivityDTO updateActivityModel)?
        swiped,
  }) {
    return swiped?.call(activityId, updateActivityModel);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int activityId, UpdatedActivityDTO updateActivityModel)?
        swiped,
    required TResult orElse(),
  }) {
    if (swiped != null) {
      return swiped(activityId, updateActivityModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Swiped value) swiped,
  }) {
    return swiped(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Swiped value)? swiped,
  }) {
    return swiped?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Swiped value)? swiped,
    required TResult orElse(),
  }) {
    if (swiped != null) {
      return swiped(this);
    }
    return orElse();
  }
}

abstract class _Swiped implements SwipeToCompleteEvent {
  const factory _Swiped(
      {required final int activityId,
      required final UpdatedActivityDTO updateActivityModel}) = _$_Swiped;

  @override
  int get activityId;
  @override
  UpdatedActivityDTO get updateActivityModel;
  @override
  @JsonKey(ignore: true)
  _$$_SwipedCopyWith<_$_Swiped> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SwipeToCompleteState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SwipeToCompleteStateCopyWith<$Res> {
  factory $SwipeToCompleteStateCopyWith(SwipeToCompleteState value,
          $Res Function(SwipeToCompleteState) then) =
      _$SwipeToCompleteStateCopyWithImpl<$Res, SwipeToCompleteState>;
}

/// @nodoc
class _$SwipeToCompleteStateCopyWithImpl<$Res,
        $Val extends SwipeToCompleteState>
    implements $SwipeToCompleteStateCopyWith<$Res> {
  _$SwipeToCompleteStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$SwipeToCompleteStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'SwipeToCompleteState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements SwipeToCompleteState {
  const factory _Initial() = _$_Initial;
}
