import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/successors_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/activity_relation/repository/activity_relations_repository.dart';

part 'successors_event.dart';
part 'successors_state.dart';
part 'successors_bloc.freezed.dart';

@injectable
class SuccessorsBloc extends Bloc<SuccessorsEvent, SuccessorsState> {
  final IActivityRelationsRepository activityRelationsRepository;
  List<ActivityCardItemModel> successorsActivityCardModels = [];
  List<String> successorsActivitiesStringList = [];
  List<String> dbSuccessorsActivitiesStringList = [];
  SuccessorsBloc(this.activityRelationsRepository) : super(const _Initial()) {
    on<SuccessorsEvent>((event, emit) async {
      await event.when(getSuccessors: (sucessorActivities) async {
        emit(const SuccessorsState.loading());
        if (sucessorActivities!.isEmpty) {
          emit(const SuccessorsState.error(errorMsg: 'No Successors'));
        } else {
          for (var element in sucessorActivities) {
            successorsActivitiesStringList.add(element.activity ?? '');
          }
          final successorsFromDB =
              await activityRelationsRepository.getSuccessorActivities(activities: successorsActivitiesStringList);
          if (successorsFromDB != null) {
            for (var activity in successorsFromDB) {
              dbSuccessorsActivitiesStringList.add(activity.activity ?? '');
              successorsActivityCardModels.add(ActivityCardItemModel.fromModel(activityDTO: activity));
            }
          }
          var leftOutSuccesorsString =
              successorsActivitiesStringList.toSet().difference(dbSuccessorsActivitiesStringList.toSet()).toList();

          List<SuccessorsActivityDTO>? leftOutSucccesors = [];
          for (var activityName in leftOutSuccesorsString) {
            for (var successor in sucessorActivities) {
              if (activityName != successor.activity) {
                leftOutSucccesors.add(successor);
              }
            }
          }
          emit(SuccessorsState.successorsLoaded(successors: successorsActivityCardModels, unsyncedSuccesors: leftOutSucccesors));
        }
      });
    });
  }
}
