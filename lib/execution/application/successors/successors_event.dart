part of 'successors_bloc.dart';

@freezed
class SuccessorsEvent with _$SuccessorsEvent {
  const factory SuccessorsEvent.getSuccessors({required List<SuccessorsActivityDTO>? successorsActivites}) = _GetSuccessors;
}
