part of 'successors_bloc.dart';

@freezed
class SuccessorsState with _$SuccessorsState {
  const factory SuccessorsState.initial() = _Initial;
  const factory SuccessorsState.loading() = _Loading;
  const factory SuccessorsState.successorsLoaded({
    required List<ActivityCardItemModel>? successors,
    required List<SuccessorsActivityDTO>? unsyncedSuccesors,
  }) = _SuccessorsLoaded;
  const factory SuccessorsState.error({required String errorMsg}) = _Error;
}
