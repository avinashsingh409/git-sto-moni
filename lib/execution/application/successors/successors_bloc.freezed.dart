// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'successors_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SuccessorsEvent {
  List<SuccessorsActivityDTO>? get successorsActivites =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<SuccessorsActivityDTO>? successorsActivites)
        getSuccessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<SuccessorsActivityDTO>? successorsActivites)?
        getSuccessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<SuccessorsActivityDTO>? successorsActivites)?
        getSuccessors,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSuccessors value) getSuccessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSuccessors value)? getSuccessors,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSuccessors value)? getSuccessors,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SuccessorsEventCopyWith<SuccessorsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SuccessorsEventCopyWith<$Res> {
  factory $SuccessorsEventCopyWith(
          SuccessorsEvent value, $Res Function(SuccessorsEvent) then) =
      _$SuccessorsEventCopyWithImpl<$Res, SuccessorsEvent>;
  @useResult
  $Res call({List<SuccessorsActivityDTO>? successorsActivites});
}

/// @nodoc
class _$SuccessorsEventCopyWithImpl<$Res, $Val extends SuccessorsEvent>
    implements $SuccessorsEventCopyWith<$Res> {
  _$SuccessorsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? successorsActivites = freezed,
  }) {
    return _then(_value.copyWith(
      successorsActivites: freezed == successorsActivites
          ? _value.successorsActivites
          : successorsActivites // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GetSuccessorsCopyWith<$Res>
    implements $SuccessorsEventCopyWith<$Res> {
  factory _$$_GetSuccessorsCopyWith(
          _$_GetSuccessors value, $Res Function(_$_GetSuccessors) then) =
      __$$_GetSuccessorsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<SuccessorsActivityDTO>? successorsActivites});
}

/// @nodoc
class __$$_GetSuccessorsCopyWithImpl<$Res>
    extends _$SuccessorsEventCopyWithImpl<$Res, _$_GetSuccessors>
    implements _$$_GetSuccessorsCopyWith<$Res> {
  __$$_GetSuccessorsCopyWithImpl(
      _$_GetSuccessors _value, $Res Function(_$_GetSuccessors) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? successorsActivites = freezed,
  }) {
    return _then(_$_GetSuccessors(
      successorsActivites: freezed == successorsActivites
          ? _value._successorsActivites
          : successorsActivites // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>?,
    ));
  }
}

/// @nodoc

class _$_GetSuccessors implements _GetSuccessors {
  const _$_GetSuccessors(
      {required final List<SuccessorsActivityDTO>? successorsActivites})
      : _successorsActivites = successorsActivites;

  final List<SuccessorsActivityDTO>? _successorsActivites;
  @override
  List<SuccessorsActivityDTO>? get successorsActivites {
    final value = _successorsActivites;
    if (value == null) return null;
    if (_successorsActivites is EqualUnmodifiableListView)
      return _successorsActivites;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SuccessorsEvent.getSuccessors(successorsActivites: $successorsActivites)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetSuccessors &&
            const DeepCollectionEquality()
                .equals(other._successorsActivites, _successorsActivites));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_successorsActivites));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetSuccessorsCopyWith<_$_GetSuccessors> get copyWith =>
      __$$_GetSuccessorsCopyWithImpl<_$_GetSuccessors>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<SuccessorsActivityDTO>? successorsActivites)
        getSuccessors,
  }) {
    return getSuccessors(successorsActivites);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<SuccessorsActivityDTO>? successorsActivites)?
        getSuccessors,
  }) {
    return getSuccessors?.call(successorsActivites);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<SuccessorsActivityDTO>? successorsActivites)?
        getSuccessors,
    required TResult orElse(),
  }) {
    if (getSuccessors != null) {
      return getSuccessors(successorsActivites);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetSuccessors value) getSuccessors,
  }) {
    return getSuccessors(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetSuccessors value)? getSuccessors,
  }) {
    return getSuccessors?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetSuccessors value)? getSuccessors,
    required TResult orElse(),
  }) {
    if (getSuccessors != null) {
      return getSuccessors(this);
    }
    return orElse();
  }
}

abstract class _GetSuccessors implements SuccessorsEvent {
  const factory _GetSuccessors(
          {required final List<SuccessorsActivityDTO>? successorsActivites}) =
      _$_GetSuccessors;

  @override
  List<SuccessorsActivityDTO>? get successorsActivites;
  @override
  @JsonKey(ignore: true)
  _$$_GetSuccessorsCopyWith<_$_GetSuccessors> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SuccessorsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)
        successorsLoaded,
    required TResult Function(String errorMsg) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_SuccessorsLoaded value) successorsLoaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SuccessorsStateCopyWith<$Res> {
  factory $SuccessorsStateCopyWith(
          SuccessorsState value, $Res Function(SuccessorsState) then) =
      _$SuccessorsStateCopyWithImpl<$Res, SuccessorsState>;
}

/// @nodoc
class _$SuccessorsStateCopyWithImpl<$Res, $Val extends SuccessorsState>
    implements $SuccessorsStateCopyWith<$Res> {
  _$SuccessorsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$SuccessorsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'SuccessorsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)
        successorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_SuccessorsLoaded value) successorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements SuccessorsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$SuccessorsStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'SuccessorsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)
        successorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_SuccessorsLoaded value) successorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements SuccessorsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessorsLoadedCopyWith<$Res> {
  factory _$$_SuccessorsLoadedCopyWith(
          _$_SuccessorsLoaded value, $Res Function(_$_SuccessorsLoaded) then) =
      __$$_SuccessorsLoadedCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<ActivityCardItemModel>? successors,
      List<SuccessorsActivityDTO>? unsyncedSuccesors});
}

/// @nodoc
class __$$_SuccessorsLoadedCopyWithImpl<$Res>
    extends _$SuccessorsStateCopyWithImpl<$Res, _$_SuccessorsLoaded>
    implements _$$_SuccessorsLoadedCopyWith<$Res> {
  __$$_SuccessorsLoadedCopyWithImpl(
      _$_SuccessorsLoaded _value, $Res Function(_$_SuccessorsLoaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? successors = freezed,
    Object? unsyncedSuccesors = freezed,
  }) {
    return _then(_$_SuccessorsLoaded(
      successors: freezed == successors
          ? _value._successors
          : successors // ignore: cast_nullable_to_non_nullable
              as List<ActivityCardItemModel>?,
      unsyncedSuccesors: freezed == unsyncedSuccesors
          ? _value._unsyncedSuccesors
          : unsyncedSuccesors // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>?,
    ));
  }
}

/// @nodoc

class _$_SuccessorsLoaded implements _SuccessorsLoaded {
  const _$_SuccessorsLoaded(
      {required final List<ActivityCardItemModel>? successors,
      required final List<SuccessorsActivityDTO>? unsyncedSuccesors})
      : _successors = successors,
        _unsyncedSuccesors = unsyncedSuccesors;

  final List<ActivityCardItemModel>? _successors;
  @override
  List<ActivityCardItemModel>? get successors {
    final value = _successors;
    if (value == null) return null;
    if (_successors is EqualUnmodifiableListView) return _successors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SuccessorsActivityDTO>? _unsyncedSuccesors;
  @override
  List<SuccessorsActivityDTO>? get unsyncedSuccesors {
    final value = _unsyncedSuccesors;
    if (value == null) return null;
    if (_unsyncedSuccesors is EqualUnmodifiableListView)
      return _unsyncedSuccesors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SuccessorsState.successorsLoaded(successors: $successors, unsyncedSuccesors: $unsyncedSuccesors)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SuccessorsLoaded &&
            const DeepCollectionEquality()
                .equals(other._successors, _successors) &&
            const DeepCollectionEquality()
                .equals(other._unsyncedSuccesors, _unsyncedSuccesors));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_successors),
      const DeepCollectionEquality().hash(_unsyncedSuccesors));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessorsLoadedCopyWith<_$_SuccessorsLoaded> get copyWith =>
      __$$_SuccessorsLoadedCopyWithImpl<_$_SuccessorsLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)
        successorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return successorsLoaded(successors, unsyncedSuccesors);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return successorsLoaded?.call(successors, unsyncedSuccesors);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (successorsLoaded != null) {
      return successorsLoaded(successors, unsyncedSuccesors);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_SuccessorsLoaded value) successorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return successorsLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return successorsLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (successorsLoaded != null) {
      return successorsLoaded(this);
    }
    return orElse();
  }
}

abstract class _SuccessorsLoaded implements SuccessorsState {
  const factory _SuccessorsLoaded(
          {required final List<ActivityCardItemModel>? successors,
          required final List<SuccessorsActivityDTO>? unsyncedSuccesors}) =
      _$_SuccessorsLoaded;

  List<ActivityCardItemModel>? get successors;
  List<SuccessorsActivityDTO>? get unsyncedSuccesors;
  @JsonKey(ignore: true)
  _$$_SuccessorsLoadedCopyWith<_$_SuccessorsLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$SuccessorsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_Error(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'SuccessorsState.error(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)
        successorsLoaded,
    required TResult Function(String errorMsg) error,
  }) {
    return error(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult? Function(String errorMsg)? error,
  }) {
    return error?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<ActivityCardItemModel>? successors,
            List<SuccessorsActivityDTO>? unsyncedSuccesors)?
        successorsLoaded,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_SuccessorsLoaded value) successorsLoaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_SuccessorsLoaded value)? successorsLoaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements SuccessorsState {
  const factory _Error({required final String errorMsg}) = _$_Error;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
