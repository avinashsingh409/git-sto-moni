part of 'activity_list_screen_bloc.dart';

@freezed
class ActivityListScreenEvent with _$ActivityListScreenEvent {
  const factory ActivityListScreenEvent.getActivities() = _GetActivities;
}
