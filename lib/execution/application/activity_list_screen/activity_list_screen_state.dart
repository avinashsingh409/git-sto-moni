part of 'activity_list_screen_bloc.dart';

@freezed
class ActivityListScreenState with _$ActivityListScreenState {
  const factory ActivityListScreenState.initial() = _Initial;
  const factory ActivityListScreenState.loading() = _Loading;
  const factory ActivityListScreenState.success({
    required Map<String, List<ActivityCardItemModel>> activities,
    required int? count,
    required int filterCount,
    required List<WbsLevelStyleDTO> colors,
    required bool multiLevel,
  }) = _Success;
  const factory ActivityListScreenState.error({required String errorMsg}) = _Error;
}
