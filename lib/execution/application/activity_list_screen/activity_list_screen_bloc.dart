import 'dart:collection';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/enums/grouping_option_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/repository/wbs_repository.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/filter_pane.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/sort_pane.dart';
import 'package:sto_mobile_v2/execution/screens/execution_search/widgets/execution_search_bar.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/widgets/grouping_pane.dart';

part 'activity_list_screen_event.dart';
part 'activity_list_screen_state.dart';
part 'activity_list_screen_bloc.freezed.dart';

@injectable
class ActivityListScreenBloc extends Bloc<ActivityListScreenEvent, ActivityListScreenState> {
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  final IActivityGroupingRepository groupingRepository;
  final IWbsRepository wbsRepository;
  List<ActivityCardItemModel> activityCardModels = [];
  BehaviorSubject<List<ActivityDTO>> activitiesStream = BehaviorSubject<List<ActivityDTO>>();

  ActivityListScreenBloc(this.turnAroundActivityRepository, this.groupingRepository, this.wbsRepository) : super(const _Initial()) {
    on<ActivityListScreenEvent>((event, emit) async {
      await event.when(
        getActivities: () async {
          emit(const ActivityListScreenState.loading());
          // get all activities based on filters
          activitiesStream = await turnAroundActivityRepository.getHomeScreenActivities(
            staticFilters: SelectedFilters.selectedStaticFilters,
            stackFilters: SelectedFilters.selectedStackFilters,
            searchTerm: SelectedSearchTerm.searchController.text.trim()
          );
          await emit.onEach(
            activitiesStream.stream,
            onData: (activities) async {
              // sort the activities
              var sortTerm = SelectedSort.sortField?.staticFieldName ?? SelectedSort.sortField?.dynamicFieldName;
              if (sortTerm != null) {
                if (SelectedSort.ascending) {
                  activities.sort((a, b) => a.getProperty(sortTerm).toString().toLowerCase().compareTo(b.getProperty(sortTerm).toString().toLowerCase()));
                } else {
                  activities.sort((a, b) => b.getProperty(sortTerm).toString().toLowerCase().compareTo(a.getProperty(sortTerm).toString().toLowerCase()));
                }
              }

              // group the activities
              final groupedActivities = groupingRepository.groupActivities(
                activities: activities,
                groupingType: SelectedGrouping.selectedOption,
                groupingFields: SelectedGrouping().getSelectedLevels()
              );
              final colors = await wbsRepository.getAllCachedLevelStyles();

              emit(const ActivityListScreenState.initial());

              // convert the map of <Grouping Route, Activity List> to a map of <Grouping Route, Activity Card Model List>
              SplayTreeMap<String, List<ActivityCardItemModel>> activityCardModels = SplayTreeMap<String, List<ActivityCardItemModel>>();
              activityCardModels = SplayTreeMap.from(groupedActivities.map((route, activities) => MapEntry(route, activities.map((activity) => ActivityCardItemModel.fromModel(activityDTO: activity)).toList())));

              emit(ActivityListScreenState.success(
                activities: activityCardModels,
                count: activities.length,
                filterCount: SelectedFilters().size(),
                colors: colors,
                multiLevel: SelectedGrouping.selectedOption == GroupingOptionsEnum.multiLevel
              ));
            },
          ).catchError((error) {
            emit(const ActivityListScreenState.error(errorMsg: "Unexpected Error"));
          });
        },
      );
    });
  }
}
