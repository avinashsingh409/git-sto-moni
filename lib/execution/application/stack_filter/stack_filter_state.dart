part of 'stack_filter_bloc.dart';

@freezed
class StackFilterState with _$StackFilterState {
  const factory StackFilterState.initial() = _Initial;
  const factory StackFilterState.loading() = _Loading;
  const factory StackFilterState.success({required List<StackFilterDTO> stackFilters}) = _Success;
  const factory StackFilterState.error({required String message}) = _Error;
}
