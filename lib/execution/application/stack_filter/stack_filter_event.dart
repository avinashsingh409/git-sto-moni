part of 'stack_filter_bloc.dart';

@freezed
class StackFilterEvent with _$StackFilterEvent {
  const factory StackFilterEvent.getStackFilters() = _GetStackFilters;
}