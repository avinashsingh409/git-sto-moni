import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/repository/stack_filter_repository.dart';

part 'stack_filter_event.dart';
part 'stack_filter_state.dart';
part 'stack_filter_bloc.freezed.dart';

@injectable
class StackFilterBloc extends Bloc<StackFilterEvent, StackFilterState> {
  final IStackFilterRepository repository;
  List<StackFilterDTO> allStackFilters = [];
  List<FilterTypeEnum> selectedStaticFilters = [];
  List<int> selectedStackFilters = [];

  StackFilterBloc(this.repository) : super(const _Initial()) {
    on<StackFilterEvent>((event, emit) async {
      await event.when(
        getStackFilters: () async {
          allStackFilters = await repository.getStackFilters() ?? [];
          emit(StackFilterState.success(
            stackFilters: allStackFilters
          ));
        }
      );
    });
  }
}
