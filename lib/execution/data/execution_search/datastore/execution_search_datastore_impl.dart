import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/turnaround_activity_dao.dart';
import 'package:sto_mobile_v2/execution/domain/execution_search/datastore/execution_search_datastore.dart';

@LazySingleton(as: IExecutionSearchDataStore)
class SearchDataStoreImpl extends IExecutionSearchDataStore {
  final TurnAroundActivityDAO activityDAO;
  SearchDataStoreImpl(this.activityDAO);

  @override
  BehaviorSubject<List<ActivityDTO>> getSearchedTurnAroundEvent({required String activityKeyword}) {
    return activityDAO.getSearchedTurnAroundActivities(activityKeyword: activityKeyword);
  }
}
