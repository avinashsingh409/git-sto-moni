import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/execution_search/datastore/execution_search_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/execution_search/repository/execution_search_repository.dart';

@LazySingleton(as: IExecutionSearchRepository)
class SearchRepositoryImpl extends IExecutionSearchRepository {
  final IExecutionSearchDataStore dataStore;
  SearchRepositoryImpl(this.dataStore);

  @override
  BehaviorSubject<List<ActivityDTO>> getSearchedTurnAroundEvent({required String activityKeyword}) {
    return dataStore.getSearchedTurnAroundEvent(activityKeyword: activityKeyword);
  }
}
