import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class WbsElementsDao {
  final isar = PersistenceModule.isar;

  WbsElementsDao();

  /// Isar Collection of WBSLevelItems
  late IsarCollection<TreeNodeDTO> collection = isar.treeNodeDTOs;

  /// Delete All WBSElementsItem
  Future<void> deleteWbsLevelItems() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All WBSElementsItem
  Future<List<TreeNodeDTO>> getAllWbsLevelsItems() async {
    final wbsElements = collection.where().findAll();
    return wbsElements;
  }

  Future<List<TreeNodeDTO>> getSelectedWbsLevelItems({required int level, required String parentName}) async {
    final wbsElements = collection.where().filter().levelEqualTo(level).and().nodeParentEqualTo(parentName).findAll();
    return wbsElements;
  }

  /// Get WBSElement by code
  Future<TreeNodeDTO?> getWbsItemByCode({required String wbsCode}) async {
    return collection.where().filter().nodeNameEqualTo(wbsCode).findFirst();
  }

  /// Upsert All WBSElementsItem
  Future<List<int>> upsertAllWbsElementItems({
    required List<TreeNodeDTO> wbsElements,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(wbsElements);
    });
  }
}
