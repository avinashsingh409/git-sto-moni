import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';

import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class WbsLevelStyleDao {
  final isar = PersistenceModule.isar;

  WbsLevelStyleDao();

  /// Isar Collection of WBSLevelItems
  late IsarCollection<WbsLevelStyleDTO> collection = isar.wbsLevelStyleDTOs;

  /// Delete All WBSLevelItems
  Future<void> deleteWbsLevelStyles() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All WBSLevelItems
  Future<List<WbsLevelStyleDTO>> getAllWbsLevelStyles() async {
    final wbsLevels = collection.where().findAll();
    return wbsLevels;
  }

  /// Upsert All WBSLevelItems
  Future<List<int>> upsertAllWbsLevelStyles({
    required List<WbsLevelStyleDTO> wbsLevels,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(wbsLevels);
    });
  }
}
