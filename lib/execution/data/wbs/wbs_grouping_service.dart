import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';

@injectable
class WbsGroupingService {
  //** Get WBS Activities */
  List<TreeNodeDTO> wbsDTOList({required List<ActivityDTO> activities}) {
    Map<String, TreeNodeDTO> wbsMap = {};
    List<TreeNodeDTO> wbsDtos = [];
    for (var element in activities) {
      final wbsRoute = element.wbsRoute!;
      if (wbsRoute.isNotEmpty) {
        for (int wbsElementIndex = 0; wbsElementIndex < wbsRoute.length; wbsElementIndex++) {
          final String elementParent = wbsElementIndex == 0 ? "" : wbsRoute[wbsElementIndex - 1];
          ElementTypeEnum elementType;
          final String currentElementName = wbsRoute[wbsElementIndex];
          if (wbsElementIndex == 0) {
            elementType = ElementTypeEnum.root;
          } else if (currentElementName.contains('[%--%]')) {
            elementType = ElementTypeEnum.node;
          } else {
            elementType = ElementTypeEnum.leaf;
          }
          final wbsDTO = TreeNodeDTO(
              nodeName: currentElementName, level: wbsElementIndex, nodeParent: elementParent, nodeElementType: elementType);
          if (!wbsMap.containsKey(currentElementName)) {
            wbsMap.addAll({currentElementName: wbsDTO});
          }
        }
      }
    }
    for (var element in wbsMap.entries) {
      wbsDtos.add(element.value);
    }
    return wbsDtos;
  }
}
