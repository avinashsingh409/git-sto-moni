import 'package:injectable/injectable.dart';

import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/wbs_grouping_service.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/datastore/wbs_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/repository/wbs_repository.dart';

@LazySingleton(as: IWbsRepository)
class WbsRepositoryImpl extends IWbsRepository {
  final IWbsDataStore dataStore;
  final WbsGroupingService wbsGroupingService;
  WbsRepositoryImpl(this.dataStore, this.wbsGroupingService);

  @override
  Future<List<WbsLevelStyleDTO>> getWbsLevelsStyles() async {
    return dataStore.getWbsLevels();
  }

  @override
  Future<void> insertAllWbsLevelStyles({required List<WbsLevelStyleDTO> wbsLevelsStyles}) {
    return dataStore.insertAllWbsStyleItems(wbsLevels: wbsLevelsStyles);
  }

  @override
  Future<List<WbsLevelStyleDTO>> getAllCachedLevelStyles() {
    return dataStore.getAllCachedLevelItems();
  }

  @override
  Future<void> deleteAllWbsItems() {
    return dataStore.deleteAllWbsItems();
  }

  @override
  Future<void> getAndCacheWbsElements({required List<ActivityDTO> activities}) {
    final wbsDtos = wbsGroupingService.wbsDTOList(activities: activities);
    return dataStore.insertAllWbsElements(wbsDtos: wbsDtos);
  }

  @override
  Future<void> deleteAllWbsLevelStyles() {
    return dataStore.deleteAllWbsLevelStyles();
  }

  @override
  Future<List<TreeNodeDTO>> getWbsTree({required int level, required String parentName}) async {
    return dataStore.getWbsTree(level: level, parentName: parentName);
  }

  @override
  Future<TreeNodeDTO?> getWbsItemByCode({required String wbsCode}) async {
    return dataStore.getWbsItemByCode(wbsCode: wbsCode);
  }
}
