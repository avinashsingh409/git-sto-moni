import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/wbs_api_service.dart';
import 'package:sto_mobile_v2/execution/data/wbs/wbs_elements_dao.dart';
import 'package:sto_mobile_v2/execution/data/wbs/wbs_grouping_service.dart';
import 'package:sto_mobile_v2/execution/data/wbs/wbs_level_style_dao.dart';

import 'package:sto_mobile_v2/execution/domain/wbs/datastore/wbs_datastore.dart';

@LazySingleton(as: IWbsDataStore)
class WbsDataStoreImpl extends IWbsDataStore {
  final WbsApiService apiService;
  final WbsLevelStyleDao wbsLevelStyleDao;
  final WbsElementsDao wbsElementsDao;
  final WbsGroupingService groupingService;

  WbsDataStoreImpl(this.apiService, this.wbsLevelStyleDao, this.groupingService, this.wbsElementsDao);

  @override
  Future<List<WbsLevelStyleDTO>> getWbsLevels() async {
    return apiService.getWbsLevels().catchDioException();
  }

  @override
  Future<void> insertAllWbsStyleItems({required List<WbsLevelStyleDTO> wbsLevels}) async {
    await wbsLevelStyleDao.upsertAllWbsLevelStyles(wbsLevels: wbsLevels);
  }

  @override
  Future<List<WbsLevelStyleDTO>> getAllCachedLevelItems() async {
    return wbsLevelStyleDao.getAllWbsLevelStyles();
  }

  @override
  Future<void> deleteAllWbsItems() async {
    await wbsElementsDao.deleteWbsLevelItems();
  }

  @override
  Future<void> insertAllWbsElements({required List<TreeNodeDTO> wbsDtos}) {
    return wbsElementsDao.upsertAllWbsElementItems(wbsElements: wbsDtos);
  }

  @override
  Future<void> deleteAllWbsLevelStyles() async {
    await wbsLevelStyleDao.deleteWbsLevelStyles();
  }

  @override
  Future<List<TreeNodeDTO>> getWbsTree({required int level, required String parentName}) async {
    return await wbsElementsDao.getSelectedWbsLevelItems(level: level, parentName: parentName);
  }

  @override
  Future<TreeNodeDTO?> getWbsItemByCode({required String wbsCode}) async {
    return await wbsElementsDao.getWbsItemByCode(wbsCode: wbsCode);
  }
}
