import 'dart:collection';

import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';

final class WbsBreadCrumbEntryItem extends LinkedListEntry<WbsBreadCrumbEntryItem> {
  final int id;
  final TreeNodeDTO treeNodeDTO;

  WbsBreadCrumbEntryItem({required this.id, required this.treeNodeDTO});
}
