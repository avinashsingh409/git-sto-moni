import 'package:json_annotation/json_annotation.dart';

/// Activity Status Codes
enum ElementTypeEnum {
  /// Finished
  @JsonValue(0)
  root,

  /// Finished
  @JsonValue(1)
  node,

  /// Invalid
  @JsonValue(2)
  leaf,
}
