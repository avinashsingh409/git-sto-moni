// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tree_node_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetTreeNodeDTOCollection on Isar {
  IsarCollection<TreeNodeDTO> get treeNodeDTOs => this.collection();
}

const TreeNodeDTOSchema = CollectionSchema(
  name: r'TreeNodeDTO',
  id: 7746672317770192942,
  properties: {
    r'level': PropertySchema(
      id: 0,
      name: r'level',
      type: IsarType.long,
    ),
    r'nodeElementType': PropertySchema(
      id: 1,
      name: r'nodeElementType',
      type: IsarType.int,
      enumMap: _TreeNodeDTOnodeElementTypeEnumValueMap,
    ),
    r'nodeName': PropertySchema(
      id: 2,
      name: r'nodeName',
      type: IsarType.string,
    ),
    r'nodeParent': PropertySchema(
      id: 3,
      name: r'nodeParent',
      type: IsarType.string,
    )
  },
  estimateSize: _treeNodeDTOEstimateSize,
  serialize: _treeNodeDTOSerialize,
  deserialize: _treeNodeDTODeserialize,
  deserializeProp: _treeNodeDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _treeNodeDTOGetId,
  getLinks: _treeNodeDTOGetLinks,
  attach: _treeNodeDTOAttach,
  version: '3.1.0+1',
);

int _treeNodeDTOEstimateSize(
  TreeNodeDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.nodeName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.nodeParent;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _treeNodeDTOSerialize(
  TreeNodeDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.level);
  writer.writeInt(offsets[1], object.nodeElementType?.index);
  writer.writeString(offsets[2], object.nodeName);
  writer.writeString(offsets[3], object.nodeParent);
}

TreeNodeDTO _treeNodeDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = TreeNodeDTO(
    id: id,
    level: reader.readLongOrNull(offsets[0]),
    nodeElementType: _TreeNodeDTOnodeElementTypeValueEnumMap[
        reader.readIntOrNull(offsets[1])],
    nodeName: reader.readStringOrNull(offsets[2]),
    nodeParent: reader.readStringOrNull(offsets[3]),
  );
  return object;
}

P _treeNodeDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (_TreeNodeDTOnodeElementTypeValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _TreeNodeDTOnodeElementTypeEnumValueMap = {
  'root': 0,
  'node': 1,
  'leaf': 2,
};
const _TreeNodeDTOnodeElementTypeValueEnumMap = {
  0: ElementTypeEnum.root,
  1: ElementTypeEnum.node,
  2: ElementTypeEnum.leaf,
};

Id _treeNodeDTOGetId(TreeNodeDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _treeNodeDTOGetLinks(TreeNodeDTO object) {
  return [];
}

void _treeNodeDTOAttach(
    IsarCollection<dynamic> col, Id id, TreeNodeDTO object) {
  object.id = id;
}

extension TreeNodeDTOQueryWhereSort
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QWhere> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension TreeNodeDTOQueryWhere
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QWhereClause> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension TreeNodeDTOQueryFilter
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QFilterCondition> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> levelEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeElementType',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeElementType',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeEqualTo(ElementTypeEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeGreaterThan(
    ElementTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeLessThan(
    ElementTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeBetween(
    ElementTypeEnum? lower,
    ElementTypeEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeElementType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeName',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeName',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> nodeNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> nodeNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'nodeName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition> nodeNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'nodeName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeName',
        value: '',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'nodeName',
        value: '',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeParent',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeParent',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeParent',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'nodeParent',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeParent',
        value: '',
      ));
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'nodeParent',
        value: '',
      ));
    });
  }
}

extension TreeNodeDTOQueryObject
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QFilterCondition> {}

extension TreeNodeDTOQueryLinks
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QFilterCondition> {}

extension TreeNodeDTOQuerySortBy
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QSortBy> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy>
      sortByNodeElementTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByNodeName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeName', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByNodeNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeName', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByNodeParent() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> sortByNodeParentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.desc);
    });
  }
}

extension TreeNodeDTOQuerySortThenBy
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QSortThenBy> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy>
      thenByNodeElementTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByNodeName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeName', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByNodeNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeName', Sort.desc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByNodeParent() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.asc);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QAfterSortBy> thenByNodeParentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.desc);
    });
  }
}

extension TreeNodeDTOQueryWhereDistinct
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QDistinct> {
  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QDistinct> distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QDistinct>
      distinctByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeElementType');
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QDistinct> distinctByNodeName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TreeNodeDTO, TreeNodeDTO, QDistinct> distinctByNodeParent(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeParent', caseSensitive: caseSensitive);
    });
  }
}

extension TreeNodeDTOQueryProperty
    on QueryBuilder<TreeNodeDTO, TreeNodeDTO, QQueryProperty> {
  QueryBuilder<TreeNodeDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<TreeNodeDTO, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }

  QueryBuilder<TreeNodeDTO, ElementTypeEnum?, QQueryOperations>
      nodeElementTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeElementType');
    });
  }

  QueryBuilder<TreeNodeDTO, String?, QQueryOperations> nodeNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeName');
    });
  }

  QueryBuilder<TreeNodeDTO, String?, QQueryOperations> nodeParentProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeParent');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TreeNodeDTO _$TreeNodeDTOFromJson(Map<String, dynamic> json) => TreeNodeDTO(
      id: json['id'] as int?,
      level: json['level'] as int?,
      nodeName: json['nodeName'] as String?,
      nodeParent: json['nodeParent'] as String?,
      nodeElementType: $enumDecodeNullable(
          _$ElementTypeEnumEnumMap, json['nodeElementType']),
    );

Map<String, dynamic> _$TreeNodeDTOToJson(TreeNodeDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'level': instance.level,
      'nodeName': instance.nodeName,
      'nodeParent': instance.nodeParent,
      'nodeElementType': _$ElementTypeEnumEnumMap[instance.nodeElementType],
    };

const _$ElementTypeEnumEnumMap = {
  ElementTypeEnum.root: 0,
  ElementTypeEnum.node: 1,
  ElementTypeEnum.leaf: 2,
};
