import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'wbs_levelstyle_dto.g.dart';

@collection
@JsonSerializable()
class WbsLevelStyleDTO {
  Id? id;
  int? level;
  String? color;
  String? backgroundColor;

  WbsLevelStyleDTO({
    this.id,
    this.level,
    this.color,
    this.backgroundColor,
  });

  factory WbsLevelStyleDTO.fromJson(Map<String, dynamic> json) => _$WbsLevelStyleDTOFromJson(json);
}
