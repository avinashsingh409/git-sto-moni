import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';

part 'tree_node_dto.g.dart';

@Collection()
@JsonSerializable()
class TreeNodeDTO {
  Id? id;
  int? level;
  String? nodeName;
  String? nodeParent;
  @Enumerated(EnumType.ordinal32)
  ElementTypeEnum? nodeElementType;

  TreeNodeDTO({
    this.id,
    this.level,
    this.nodeName,
    this.nodeParent,
    this.nodeElementType,
  });
}
