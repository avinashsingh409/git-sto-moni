// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wbs_levelstyle_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetWbsLevelStyleDTOCollection on Isar {
  IsarCollection<WbsLevelStyleDTO> get wbsLevelStyleDTOs => this.collection();
}

const WbsLevelStyleDTOSchema = CollectionSchema(
  name: r'WbsLevelStyleDTO',
  id: 3137525033604649625,
  properties: {
    r'backgroundColor': PropertySchema(
      id: 0,
      name: r'backgroundColor',
      type: IsarType.string,
    ),
    r'color': PropertySchema(
      id: 1,
      name: r'color',
      type: IsarType.string,
    ),
    r'level': PropertySchema(
      id: 2,
      name: r'level',
      type: IsarType.long,
    )
  },
  estimateSize: _wbsLevelStyleDTOEstimateSize,
  serialize: _wbsLevelStyleDTOSerialize,
  deserialize: _wbsLevelStyleDTODeserialize,
  deserializeProp: _wbsLevelStyleDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _wbsLevelStyleDTOGetId,
  getLinks: _wbsLevelStyleDTOGetLinks,
  attach: _wbsLevelStyleDTOAttach,
  version: '3.1.0+1',
);

int _wbsLevelStyleDTOEstimateSize(
  WbsLevelStyleDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.backgroundColor;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.color;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _wbsLevelStyleDTOSerialize(
  WbsLevelStyleDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.backgroundColor);
  writer.writeString(offsets[1], object.color);
  writer.writeLong(offsets[2], object.level);
}

WbsLevelStyleDTO _wbsLevelStyleDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = WbsLevelStyleDTO(
    backgroundColor: reader.readStringOrNull(offsets[0]),
    color: reader.readStringOrNull(offsets[1]),
    id: id,
    level: reader.readLongOrNull(offsets[2]),
  );
  return object;
}

P _wbsLevelStyleDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _wbsLevelStyleDTOGetId(WbsLevelStyleDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _wbsLevelStyleDTOGetLinks(WbsLevelStyleDTO object) {
  return [];
}

void _wbsLevelStyleDTOAttach(
    IsarCollection<dynamic> col, Id id, WbsLevelStyleDTO object) {
  object.id = id;
}

extension WbsLevelStyleDTOQueryWhereSort
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QWhere> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension WbsLevelStyleDTOQueryWhere
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QWhereClause> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WbsLevelStyleDTOQueryFilter
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QFilterCondition> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'backgroundColor',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'backgroundColor',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'backgroundColor',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'backgroundColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'backgroundColor',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'backgroundColor',
        value: '',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      backgroundColorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'backgroundColor',
        value: '',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterFilterCondition>
      levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension WbsLevelStyleDTOQueryObject
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QFilterCondition> {}

extension WbsLevelStyleDTOQueryLinks
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QFilterCondition> {}

extension WbsLevelStyleDTOQuerySortBy
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QSortBy> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      sortByBackgroundColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backgroundColor', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      sortByBackgroundColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backgroundColor', Sort.desc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy> sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy> sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }
}

extension WbsLevelStyleDTOQuerySortThenBy
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QSortThenBy> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      thenByBackgroundColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backgroundColor', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      thenByBackgroundColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backgroundColor', Sort.desc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy> thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy> thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QAfterSortBy>
      thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }
}

extension WbsLevelStyleDTOQueryWhereDistinct
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QDistinct> {
  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QDistinct>
      distinctByBackgroundColor({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'backgroundColor',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QDistinct> distinctByColor(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QDistinct>
      distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }
}

extension WbsLevelStyleDTOQueryProperty
    on QueryBuilder<WbsLevelStyleDTO, WbsLevelStyleDTO, QQueryProperty> {
  QueryBuilder<WbsLevelStyleDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<WbsLevelStyleDTO, String?, QQueryOperations>
      backgroundColorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'backgroundColor');
    });
  }

  QueryBuilder<WbsLevelStyleDTO, String?, QQueryOperations> colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<WbsLevelStyleDTO, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WbsLevelStyleDTO _$WbsLevelStyleDTOFromJson(Map<String, dynamic> json) =>
    WbsLevelStyleDTO(
      id: json['id'] as int?,
      level: json['level'] as int?,
      color: json['color'] as String?,
      backgroundColor: json['backgroundColor'] as String?,
    );

Map<String, dynamic> _$WbsLevelStyleDTOToJson(WbsLevelStyleDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'level': instance.level,
      'color': instance.color,
      'backgroundColor': instance.backgroundColor,
    };
