import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';

@injectable
class WbsApiService {
  final Dio dio;

  WbsApiService(this.dio);

  Future<List<WbsLevelStyleDTO>> getWbsLevels() async {
    final response = await dio.get(ApiEndPoints.sTOXWbsLevels);
    List<WbsLevelStyleDTO> items = List<WbsLevelStyleDTO>.from(response.data.map((model) => WbsLevelStyleDTO.fromJson(model)));
    return items;
  }
}
