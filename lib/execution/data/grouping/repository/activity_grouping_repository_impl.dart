import 'dart:collection';

import 'package:collection/collection.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/core/utils/date_formatter.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/enums/grouping_option_enum.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/datastore/activity_grouping_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';

@LazySingleton(as: IActivityGroupingRepository)
class ActivityGroupingRepositoryImpl extends IActivityGroupingRepository {
  final IActivityGroupingDataStore activityGroupingDataStore;
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  ActivityGroupingRepositoryImpl(this.activityGroupingDataStore, this.turnAroundActivityRepository);

  @override
  Future<List<ActivityFieldDTO>?> getGroupableFields() {
    return activityGroupingDataStore.getGroupableFields();
  }

  @override
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel1Grouping({required ActivityFieldDTO activityField}) async {
    final allActivities = await turnAroundActivityRepository.getActivitiesForGrouping();
    if (allActivities != null) {
      final level1Group = activityGroupingDataStore.getLevel1Grouping(allActivities: allActivities, groupingField: activityField);
      if (level1Group != null) {
        await activityGroupingDataStore.cacheGroupedData(groupedElements: level1Group);
        return Left(level1Group);
      }
    }
    return const Right(unit);
  }

  @override
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel2Grouping({required List<ActivityFieldDTO> activityFields}) async {
    final allActivities = await turnAroundActivityRepository.getActivitiesForGrouping();
    if (allActivities != null) {
      final level2Group =
          activityGroupingDataStore.getLevel2Grouping(allActivities: allActivities, groupingFields: activityFields);
      if (level2Group != null) {
        await activityGroupingDataStore.cacheGroupedData(groupedElements: level2Group);
        return Left(level2Group);
      }
    }
    return const Right(unit);
  }

  @override
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel3Grouping({required List<ActivityFieldDTO> groupingFields}) async {
    final allActivities = await turnAroundActivityRepository.getActivitiesForGrouping();
    if (allActivities != null) {
      final level3Group =
          activityGroupingDataStore.getLevel3Grouping(allActivities: allActivities, groupingFields: groupingFields);
      if (level3Group != null) {
        await activityGroupingDataStore.cacheGroupedData(groupedElements: level3Group);
        return Left(level3Group);
      }
    }
    return const Right(unit);
  }

  @override
  Future<bool> isPrevGroupAvailable() {
    return activityGroupingDataStore.isPrevGroupAvailable();
  }

  @override
  Future<List<GroupingTreeNodeDTO>> getCachedGroupData() {
    return activityGroupingDataStore.getCachedGroupData();
  }

  @override
  Future<void> clearMultiLevelGrouping() {
    return activityGroupingDataStore.clearMultiLevelGrouping();
  }

  @override
  MultiLevelSortDTO? getSelectedMultiLevelSort() {
    return activityGroupingDataStore.getSelectedMultiLevelSort();
  }

  @override
  Future<void> saveSelectedMultiLevelSort({required MultiLevelSortDTO multiLevelSortDTO}) {
    return activityGroupingDataStore.saveSelectedMultiLevelSort(multiLevelSortDTO: multiLevelSortDTO);
  }

  @override
  Future<void> clearSelectedMultiLevelSort() {
    return activityGroupingDataStore.clearSelectedMultiLevelSort();
  }

  @override
  SplayTreeMap<String, List<ActivityDTO>> groupActivities({
    required List<ActivityDTO> activities,
    required GroupingOptionsEnum groupingType,
    required List<ActivityFieldDTO?> groupingFields,
  }) {
    switch (groupingType) {
      // no grouping (flat list)
      case GroupingOptionsEnum.clear:
        return SplayTreeMap.from({"": activities});
      // WBS grouping
      case GroupingOptionsEnum.wbs:
        return _groupByWbs(activities);
      // multi-level grouping
      case GroupingOptionsEnum.multiLevel:
        return _groupByMultiLevel(activities, groupingFields);
    }
  }

  // from a list of activities, group them by WBS
  SplayTreeMap<String, List<ActivityDTO>> _groupByWbs(List<ActivityDTO> activities) {
    SplayTreeMap<String, List<ActivityDTO>> results = SplayTreeMap<String, List<ActivityDTO>>();
    for (var activity in activities) {
      if (activity.wbsRoute?.isNotEmpty ?? false) {
        // compute the delimited key
        var route = activity.wbsRoute!.sublist(0, activity.wbsRoute!.length - 1);
        for (var i = 0; i < route.length; i++) {
          final routePiece = route[i];
          if (routePiece.contains("<[%%]>")) {
            route[i] = routePiece.split("<[%%]>")[1];
          }
        }
        final flatRoute = route.join("[**]");

        // add activity to results map
        if (results.containsKey(flatRoute)) {
          results[flatRoute]!.add(activity);
        } else {
          results[flatRoute] = [activity];
        }
      }
    }
    return results;
  }

  // from a list of activities, group them by given fields
  SplayTreeMap<String, List<ActivityDTO>> _groupByMultiLevel(List<ActivityDTO> activities, List<ActivityFieldDTO?> groupingFields) {
    SplayTreeMap<String, List<ActivityDTO>> results = SplayTreeMap<String, List<ActivityDTO>>();
    for (var activity in activities) {
      // compute the delimited key
      var route = [];
      for (var field in groupingFields) {
        if (field != null) {
          final result = activity.getProperty(field.staticFieldName ?? field.dynamicFieldName ?? "");
          if (result != null) {
            route.add(DateFormatter.handleDisplayDate(dateTimeString: result.toString(), long: false));
          }
        }
      }
      if (route.length != groupingFields.whereNotNull().length) continue;
      final flatRoute = route.join("[**]");

      // add activity to results map
      if (results.containsKey(flatRoute)) {
        results[flatRoute]!.add(activity);
      } else {
        results[flatRoute] = [activity];
      }
    }
    return results;
  }
}
