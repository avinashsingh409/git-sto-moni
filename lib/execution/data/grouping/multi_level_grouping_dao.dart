import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class MultiLevelGroupingDao {
  final isar = PersistenceModule.isar;

  MultiLevelGroupingDao();

  /// Isar Collection of GroupedElements
  late IsarCollection<GroupingTreeNodeDTO> collection = isar.groupingTreeNodeDTOs;

  /// Delete All GroupedElements
  Future<void> deleteAllGroupings() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All GroupedElements
  Future<List<GroupingTreeNodeDTO>> getAllMultiLevelGroupingItems() async {
    final groupedElements = collection.where().findAll();
    return groupedElements;
  }

  /// Upsert All GroupedElements
  Future<List<int>> upsertAllGroupedElements({
    required List<GroupingTreeNodeDTO> groupedElements,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(groupedElements);
    });
  }

  Future<bool> isPrevGroupAvailable() async {
    final existingGroup = await collection.where().anyId().build().findFirst();
    if (existingGroup != null) {
      return true;
    }
    return false;
  }
}
