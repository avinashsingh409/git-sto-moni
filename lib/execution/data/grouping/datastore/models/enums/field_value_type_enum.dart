/// Activity Status Codes
enum FieldValueTypeEnum {
  text,
  date,
  decimal,
  percentage,
}
