import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/enums/field_value_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';

part 'grouping_tree_node_dto.g.dart';

@Collection()
@JsonSerializable()
class GroupingTreeNodeDTO {
  Id? id;
  int? level;
  String? nodeValue;
  String? nodeParent;
  @Enumerated(EnumType.ordinal32)
  FieldValueTypeEnum fieldValueTypeEnum;
  @Enumerated(EnumType.ordinal32)
  ElementTypeEnum? nodeElementType;

  GroupingTreeNodeDTO({
    this.id,
    this.level,
    this.nodeValue,
    this.nodeParent,
    this.nodeElementType,
    this.fieldValueTypeEnum = FieldValueTypeEnum.text,
  });
}
