// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'multi_level_sort_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetMultiLevelSortDTOCollection on Isar {
  IsarCollection<MultiLevelSortDTO> get multiLevelSortDTOs => this.collection();
}

const MultiLevelSortDTOSchema = CollectionSchema(
  name: r'MultiLevelSortDTO',
  id: -7267939709904173194,
  properties: {
    r'field': PropertySchema(
      id: 0,
      name: r'field',
      type: IsarType.string,
    ),
    r'level': PropertySchema(
      id: 1,
      name: r'level',
      type: IsarType.long,
    )
  },
  estimateSize: _multiLevelSortDTOEstimateSize,
  serialize: _multiLevelSortDTOSerialize,
  deserialize: _multiLevelSortDTODeserialize,
  deserializeProp: _multiLevelSortDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _multiLevelSortDTOGetId,
  getLinks: _multiLevelSortDTOGetLinks,
  attach: _multiLevelSortDTOAttach,
  version: '3.1.0+1',
);

int _multiLevelSortDTOEstimateSize(
  MultiLevelSortDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.field;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _multiLevelSortDTOSerialize(
  MultiLevelSortDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.field);
  writer.writeLong(offsets[1], object.level);
}

MultiLevelSortDTO _multiLevelSortDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = MultiLevelSortDTO(
    field: reader.readStringOrNull(offsets[0]),
    id: id,
    level: reader.readLongOrNull(offsets[1]),
  );
  return object;
}

P _multiLevelSortDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _multiLevelSortDTOGetId(MultiLevelSortDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _multiLevelSortDTOGetLinks(
    MultiLevelSortDTO object) {
  return [];
}

void _multiLevelSortDTOAttach(
    IsarCollection<dynamic> col, Id id, MultiLevelSortDTO object) {
  object.id = id;
}

extension MultiLevelSortDTOQueryWhereSort
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QWhere> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension MultiLevelSortDTOQueryWhere
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QWhereClause> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension MultiLevelSortDTOQueryFilter
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QFilterCondition> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'field',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'field',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'field',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'field',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'field',
        value: '',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      fieldIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'field',
        value: '',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterFilterCondition>
      levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension MultiLevelSortDTOQueryObject
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QFilterCondition> {}

extension MultiLevelSortDTOQueryLinks
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QFilterCondition> {}

extension MultiLevelSortDTOQuerySortBy
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QSortBy> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      sortByField() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.asc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      sortByFieldDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.desc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }
}

extension MultiLevelSortDTOQuerySortThenBy
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QSortThenBy> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      thenByField() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.asc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      thenByFieldDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.desc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QAfterSortBy>
      thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }
}

extension MultiLevelSortDTOQueryWhereDistinct
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QDistinct> {
  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QDistinct> distinctByField(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'field', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QDistinct>
      distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }
}

extension MultiLevelSortDTOQueryProperty
    on QueryBuilder<MultiLevelSortDTO, MultiLevelSortDTO, QQueryProperty> {
  QueryBuilder<MultiLevelSortDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<MultiLevelSortDTO, String?, QQueryOperations> fieldProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'field');
    });
  }

  QueryBuilder<MultiLevelSortDTO, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MultiLevelSortDTO _$MultiLevelSortDTOFromJson(Map<String, dynamic> json) =>
    MultiLevelSortDTO(
      id: json['id'] as int?,
      level: json['level'] as int?,
      field: json['field'] as String?,
    );

Map<String, dynamic> _$MultiLevelSortDTOToJson(MultiLevelSortDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'level': instance.level,
      'field': instance.field,
    };
