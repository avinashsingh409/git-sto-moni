import 'dart:collection';

import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';

final class GroupingBreadCrumbEntryItem extends LinkedListEntry<GroupingBreadCrumbEntryItem> {
  final GroupingTreeNodeDTO groupingTreeNodeDTO;

  GroupingBreadCrumbEntryItem({required this.groupingTreeNodeDTO});
}
