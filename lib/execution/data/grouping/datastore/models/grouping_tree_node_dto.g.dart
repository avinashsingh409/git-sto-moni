// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'grouping_tree_node_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGroupingTreeNodeDTOCollection on Isar {
  IsarCollection<GroupingTreeNodeDTO> get groupingTreeNodeDTOs =>
      this.collection();
}

const GroupingTreeNodeDTOSchema = CollectionSchema(
  name: r'GroupingTreeNodeDTO',
  id: -7973697642549175787,
  properties: {
    r'fieldValueTypeEnum': PropertySchema(
      id: 0,
      name: r'fieldValueTypeEnum',
      type: IsarType.int,
      enumMap: _GroupingTreeNodeDTOfieldValueTypeEnumEnumValueMap,
    ),
    r'level': PropertySchema(
      id: 1,
      name: r'level',
      type: IsarType.long,
    ),
    r'nodeElementType': PropertySchema(
      id: 2,
      name: r'nodeElementType',
      type: IsarType.int,
      enumMap: _GroupingTreeNodeDTOnodeElementTypeEnumValueMap,
    ),
    r'nodeParent': PropertySchema(
      id: 3,
      name: r'nodeParent',
      type: IsarType.string,
    ),
    r'nodeValue': PropertySchema(
      id: 4,
      name: r'nodeValue',
      type: IsarType.string,
    )
  },
  estimateSize: _groupingTreeNodeDTOEstimateSize,
  serialize: _groupingTreeNodeDTOSerialize,
  deserialize: _groupingTreeNodeDTODeserialize,
  deserializeProp: _groupingTreeNodeDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _groupingTreeNodeDTOGetId,
  getLinks: _groupingTreeNodeDTOGetLinks,
  attach: _groupingTreeNodeDTOAttach,
  version: '3.1.0+1',
);

int _groupingTreeNodeDTOEstimateSize(
  GroupingTreeNodeDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.nodeParent;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.nodeValue;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _groupingTreeNodeDTOSerialize(
  GroupingTreeNodeDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeInt(offsets[0], object.fieldValueTypeEnum.index);
  writer.writeLong(offsets[1], object.level);
  writer.writeInt(offsets[2], object.nodeElementType?.index);
  writer.writeString(offsets[3], object.nodeParent);
  writer.writeString(offsets[4], object.nodeValue);
}

GroupingTreeNodeDTO _groupingTreeNodeDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GroupingTreeNodeDTO(
    fieldValueTypeEnum: _GroupingTreeNodeDTOfieldValueTypeEnumValueEnumMap[
            reader.readIntOrNull(offsets[0])] ??
        FieldValueTypeEnum.text,
    id: id,
    level: reader.readLongOrNull(offsets[1]),
    nodeElementType: _GroupingTreeNodeDTOnodeElementTypeValueEnumMap[
        reader.readIntOrNull(offsets[2])],
    nodeParent: reader.readStringOrNull(offsets[3]),
    nodeValue: reader.readStringOrNull(offsets[4]),
  );
  return object;
}

P _groupingTreeNodeDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (_GroupingTreeNodeDTOfieldValueTypeEnumValueEnumMap[
              reader.readIntOrNull(offset)] ??
          FieldValueTypeEnum.text) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    case 2:
      return (_GroupingTreeNodeDTOnodeElementTypeValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _GroupingTreeNodeDTOfieldValueTypeEnumEnumValueMap = {
  'text': 0,
  'date': 1,
  'decimal': 2,
  'percentage': 3,
};
const _GroupingTreeNodeDTOfieldValueTypeEnumValueEnumMap = {
  0: FieldValueTypeEnum.text,
  1: FieldValueTypeEnum.date,
  2: FieldValueTypeEnum.decimal,
  3: FieldValueTypeEnum.percentage,
};
const _GroupingTreeNodeDTOnodeElementTypeEnumValueMap = {
  'root': 0,
  'node': 1,
  'leaf': 2,
};
const _GroupingTreeNodeDTOnodeElementTypeValueEnumMap = {
  0: ElementTypeEnum.root,
  1: ElementTypeEnum.node,
  2: ElementTypeEnum.leaf,
};

Id _groupingTreeNodeDTOGetId(GroupingTreeNodeDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _groupingTreeNodeDTOGetLinks(
    GroupingTreeNodeDTO object) {
  return [];
}

void _groupingTreeNodeDTOAttach(
    IsarCollection<dynamic> col, Id id, GroupingTreeNodeDTO object) {
  object.id = id;
}

extension GroupingTreeNodeDTOQueryWhereSort
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QWhere> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension GroupingTreeNodeDTOQueryWhere
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QWhereClause> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GroupingTreeNodeDTOQueryFilter on QueryBuilder<GroupingTreeNodeDTO,
    GroupingTreeNodeDTO, QFilterCondition> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      fieldValueTypeEnumEqualTo(FieldValueTypeEnum value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'fieldValueTypeEnum',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      fieldValueTypeEnumGreaterThan(
    FieldValueTypeEnum value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'fieldValueTypeEnum',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      fieldValueTypeEnumLessThan(
    FieldValueTypeEnum value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'fieldValueTypeEnum',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      fieldValueTypeEnumBetween(
    FieldValueTypeEnum lower,
    FieldValueTypeEnum upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'fieldValueTypeEnum',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeElementType',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeElementType',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeEqualTo(ElementTypeEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeGreaterThan(
    ElementTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeLessThan(
    ElementTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeElementType',
        value: value,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeElementTypeBetween(
    ElementTypeEnum? lower,
    ElementTypeEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeElementType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeParent',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeParent',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeParent',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'nodeParent',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'nodeParent',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeParent',
        value: '',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeParentIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'nodeParent',
        value: '',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nodeValue',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nodeValue',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nodeValue',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'nodeValue',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'nodeValue',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nodeValue',
        value: '',
      ));
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterFilterCondition>
      nodeValueIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'nodeValue',
        value: '',
      ));
    });
  }
}

extension GroupingTreeNodeDTOQueryObject on QueryBuilder<GroupingTreeNodeDTO,
    GroupingTreeNodeDTO, QFilterCondition> {}

extension GroupingTreeNodeDTOQueryLinks on QueryBuilder<GroupingTreeNodeDTO,
    GroupingTreeNodeDTO, QFilterCondition> {}

extension GroupingTreeNodeDTOQuerySortBy
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QSortBy> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByFieldValueTypeEnum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldValueTypeEnum', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByFieldValueTypeEnumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldValueTypeEnum', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeElementTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeParent() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeParentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeValue', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      sortByNodeValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeValue', Sort.desc);
    });
  }
}

extension GroupingTreeNodeDTOQuerySortThenBy
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QSortThenBy> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByFieldValueTypeEnum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldValueTypeEnum', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByFieldValueTypeEnumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldValueTypeEnum', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeElementTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeElementType', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeParent() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeParentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeParent', Sort.desc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeValue', Sort.asc);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QAfterSortBy>
      thenByNodeValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nodeValue', Sort.desc);
    });
  }
}

extension GroupingTreeNodeDTOQueryWhereDistinct
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct> {
  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct>
      distinctByFieldValueTypeEnum() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'fieldValueTypeEnum');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct>
      distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct>
      distinctByNodeElementType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeElementType');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct>
      distinctByNodeParent({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeParent', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QDistinct>
      distinctByNodeValue({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nodeValue', caseSensitive: caseSensitive);
    });
  }
}

extension GroupingTreeNodeDTOQueryProperty
    on QueryBuilder<GroupingTreeNodeDTO, GroupingTreeNodeDTO, QQueryProperty> {
  QueryBuilder<GroupingTreeNodeDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, FieldValueTypeEnum, QQueryOperations>
      fieldValueTypeEnumProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'fieldValueTypeEnum');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, ElementTypeEnum?, QQueryOperations>
      nodeElementTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeElementType');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, String?, QQueryOperations>
      nodeParentProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeParent');
    });
  }

  QueryBuilder<GroupingTreeNodeDTO, String?, QQueryOperations>
      nodeValueProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nodeValue');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GroupingTreeNodeDTO _$GroupingTreeNodeDTOFromJson(Map<String, dynamic> json) =>
    GroupingTreeNodeDTO(
      id: json['id'] as int?,
      level: json['level'] as int?,
      nodeValue: json['nodeValue'] as String?,
      nodeParent: json['nodeParent'] as String?,
      nodeElementType: $enumDecodeNullable(
          _$ElementTypeEnumEnumMap, json['nodeElementType']),
      fieldValueTypeEnum: $enumDecodeNullable(
              _$FieldValueTypeEnumEnumMap, json['fieldValueTypeEnum']) ??
          FieldValueTypeEnum.text,
    );

Map<String, dynamic> _$GroupingTreeNodeDTOToJson(
        GroupingTreeNodeDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'level': instance.level,
      'nodeValue': instance.nodeValue,
      'nodeParent': instance.nodeParent,
      'fieldValueTypeEnum':
          _$FieldValueTypeEnumEnumMap[instance.fieldValueTypeEnum]!,
      'nodeElementType': _$ElementTypeEnumEnumMap[instance.nodeElementType],
    };

const _$ElementTypeEnumEnumMap = {
  ElementTypeEnum.root: 0,
  ElementTypeEnum.node: 1,
  ElementTypeEnum.leaf: 2,
};

const _$FieldValueTypeEnumEnumMap = {
  FieldValueTypeEnum.text: 'text',
  FieldValueTypeEnum.date: 'date',
  FieldValueTypeEnum.decimal: 'decimal',
  FieldValueTypeEnum.percentage: 'percentage',
};
