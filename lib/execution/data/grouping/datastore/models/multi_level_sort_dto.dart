import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'multi_level_sort_dto.g.dart';

@Collection()
@JsonSerializable()
class MultiLevelSortDTO {
  Id? id;
  int? level;
  String? field;

  MultiLevelSortDTO({
    this.id,
    this.level,
    this.field,
  });
}
