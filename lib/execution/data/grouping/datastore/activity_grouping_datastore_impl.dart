import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/activities_grouping_service.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/multi_level_grouping_dao.dart';
import 'package:sto_mobile_v2/execution/data/grouping/multi_level_sort_dao.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/activity_field_dao.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/datastore/activity_grouping_datastore.dart';

@LazySingleton(as: IActivityGroupingDataStore)
class ActivityGroupingDataStoreImpl extends IActivityGroupingDataStore {
  final ActivityFieldDAO activityFieldDAO;
  final ActivitiesGroupingService activitiesGroupingService;
  final MultiLevelGroupingDao multiLevelGroupingDao;
  final MultiLevelSortingDao multiLevelSortingDao;

  ActivityGroupingDataStoreImpl(
      this.activityFieldDAO, this.activitiesGroupingService, this.multiLevelGroupingDao, this.multiLevelSortingDao);
  @override
  Future<List<ActivityFieldDTO>?> getGroupableFields() {
    return activityFieldDAO.getGroupableActivityFields();
  }

  @override
  List<GroupingTreeNodeDTO>? getLevel1Grouping(
      {required List<ActivityDTO> allActivities, required ActivityFieldDTO groupingField}) {
    return activitiesGroupingService.getLevel1Grouping(dbActivities: allActivities, groupingField: groupingField);
  }

  @override
  List<GroupingTreeNodeDTO>? getLevel2Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingFields}) {
    return activitiesGroupingService.getLevel2Grouping(allActivities: allActivities, groupingField: groupingFields);
  }

  @override
  List<GroupingTreeNodeDTO>? getLevel3Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingFields}) {
    return activitiesGroupingService.getLevel3Grouping(allActivities: allActivities, groupingField: groupingFields);
  }

  @override
  Future<void> cacheGroupedData({required List<GroupingTreeNodeDTO> groupedElements}) async {
    await multiLevelGroupingDao.deleteAllGroupings();
    await multiLevelGroupingDao.upsertAllGroupedElements(groupedElements: groupedElements);
  }

  @override
  Future<bool> isPrevGroupAvailable() {
    return multiLevelGroupingDao.isPrevGroupAvailable();
  }

  @override
  Future<List<GroupingTreeNodeDTO>> getCachedGroupData() {
    return multiLevelGroupingDao.getAllMultiLevelGroupingItems();
  }

  @override
  Future<void> clearMultiLevelGrouping() {
    return multiLevelGroupingDao.deleteAllGroupings();
  }

  @override
  MultiLevelSortDTO? getSelectedMultiLevelSort() {
    return multiLevelSortingDao.getSortingLevel();
  }

  @override
  Future<void> saveSelectedMultiLevelSort({required MultiLevelSortDTO multiLevelSortDTO}) {
    return multiLevelSortingDao.upsertSortingParam(sortParams: multiLevelSortDTO);
  }

  @override
  Future<void> clearSelectedMultiLevelSort() {
    return multiLevelSortingDao.deleteAllSorting();
  }
}
