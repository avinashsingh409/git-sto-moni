import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class MultiLevelSortingDao {
  final isar = PersistenceModule.isar;

  MultiLevelSortingDao();

  /// Isar Collection of MultiLevelSortElement
  late IsarCollection<MultiLevelSortDTO> collection = isar.multiLevelSortDTOs;

  /// Delete All MultiLevelSorts
  Future<void> deleteAllSorting() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get Initial Sorting
  MultiLevelSortDTO? getSortingLevel() {
    final sortingFilterDTO = collection.getSync(1);
    return sortingFilterDTO;
  }

  /// Get All MultiLevelSorts
  Future<List<MultiLevelSortDTO>> getAllMultiLevelSorting() async {
    final sortParam = collection.where().findAll();
    return sortParam;
  }

  /// Upsert MultiLevelSorts
  Future<int> upsertSortingParam({
    required MultiLevelSortDTO sortParams,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(sortParams);
    });
  }
}
