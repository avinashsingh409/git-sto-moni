import 'dart:collection';
import 'dart:convert';
import 'package:collection/collection.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/enums/field_value_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';

@injectable
class ActivitiesGroupingService {
  List<GroupingTreeNodeDTO>? getLevel1Grouping(
      {required List<ActivityDTO> dbActivities, required ActivityFieldDTO groupingField}) {
    List<GroupingTreeNodeDTO> rootGroupTree = [];
    if (groupingField.fieldType != fieldTypeDynamic) {
      final itrGroup = groupBy(
        dbActivities,
        (obj) => obj.getProperty(groupingField.staticFieldName ?? ''),
      );
      rootGroupTree.add(GroupingTreeNodeDTO(
        level: -1,
        nodeElementType: ElementTypeEnum.root,
        nodeParent: null,
        nodeValue: 'Base Level',
      ));

      final splayTreeMapGrouping = SplayTreeMap<dynamic, List<ActivityDTO>>.from(itrGroup, (a, b) => a.compareTo(b));
      splayTreeMapGrouping.forEach((key, value) {
        if (key.toString() != "null") {
          List<int> activityIds = [];
          for (var element in value) {
            activityIds.add(element.id ?? 0);
          }
          rootGroupTree.add(GroupingTreeNodeDTO(
            level: 0,
            nodeElementType: ElementTypeEnum.root,
            nodeParent: 'Base Level',
            nodeValue: key.toString(),
            fieldValueTypeEnum: groupingField.staticFieldName != null && groupingField.staticFieldName == "percentComplete"
                ? FieldValueTypeEnum.percentage
                : FieldValueTypeEnum.text,
          ));
          rootGroupTree.add(GroupingTreeNodeDTO(
            level: 1,
            nodeParent: key.toString(),
            nodeElementType: ElementTypeEnum.leaf,
            nodeValue: activityIds.join(',').toString(),
          ));
        }
      });
    } else {
      Map<String, List<ActivityDTO>?> dynamicGroupMap =
          getDynamicGrouping(activitiesToGroup: dbActivities, groupingField: groupingField);

      final splayTreeMapGrouping = SplayTreeMap<String, List<ActivityDTO>?>.from(dynamicGroupMap, (a, b) => a.compareTo(b));
      //
      splayTreeMapGrouping.forEach((key, value) {
        List<int> activityIds = [];
        for (var element in value ?? []) {
          activityIds.add(element.id ?? 0);
        }
        rootGroupTree.add(
            GroupingTreeNodeDTO(level: -1, nodeElementType: ElementTypeEnum.root, nodeParent: null, nodeValue: 'Base Level'));
        rootGroupTree.add(GroupingTreeNodeDTO(
            level: 0, nodeElementType: ElementTypeEnum.root, nodeParent: 'Base Level', nodeValue: key.toString()));
        rootGroupTree.add(GroupingTreeNodeDTO(
          level: 1,
          nodeParent: key.toString(),
          nodeElementType: ElementTypeEnum.leaf,
          nodeValue: activityIds.join(',').toString(),
        ));
      });
    }
    //
    return rootGroupTree;
  }

  List<GroupingTreeNodeDTO>? getLevel2Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingField}) {
    final ActivityFieldDTO level2GroupingField = groupingField[1];

    var rootLevel = getLevel1Grouping(dbActivities: allActivities, groupingField: groupingField[0]);
    if (rootLevel != null) {
      final prevLeaves = rootLevel.where((element) => element.level == 1).toList();
      for (var leaf in prevLeaves) {
        List<ActivityDTO> activitiesToGroup = [];
        List<String> activitiesIdsInLeaf = leaf.nodeValue?.split(',') ?? [];
        List<int> listOfIds = [];

        if (activitiesIdsInLeaf != []) {
          listOfIds = activitiesIdsInLeaf.map(int.parse).toList();
        }

        for (var id in listOfIds) {
          activitiesToGroup.add(allActivities.where((element) => element.id == id).first);
        }

        if (level2GroupingField.fieldType != fieldTypeDynamic) {
          final itrGroup = groupBy(
            activitiesToGroup,
            (obj) => obj.getProperty(level2GroupingField.staticFieldName ?? ''),
          );
          final splayTreeMapGrouping = SplayTreeMap<dynamic, List<ActivityDTO>>.from(itrGroup, (a, b) => a.compareTo(b));
          splayTreeMapGrouping.forEach((key, value) {
            if (key.toString() != "null") {
              List<int> activityIds = [];
              for (var element in value) {
                activityIds.add(element.id ?? 0);
              }

              rootLevel.add(GroupingTreeNodeDTO(
                level: 1,
                nodeElementType: ElementTypeEnum.node,
                nodeParent: leaf.nodeParent,
                nodeValue: "${leaf.nodeParent}<[%%]>${key.toString()}",
                fieldValueTypeEnum:
                    level2GroupingField.staticFieldName != null && level2GroupingField.staticFieldName == "percentComplete"
                        ? FieldValueTypeEnum.percentage
                        : FieldValueTypeEnum.text,
              ));
              rootLevel.add(GroupingTreeNodeDTO(
                level: 2,
                nodeParent: "${leaf.nodeParent}<[%%]>${key.toString()}",
                nodeElementType: ElementTypeEnum.leaf,
                nodeValue: activityIds.join(',').toString(),
              ));
            }
          });

          //
        } else {
          Map<String, List<ActivityDTO>?> dynamicGroupMap =
              getDynamicGrouping(activitiesToGroup: activitiesToGroup, groupingField: level2GroupingField);

          final splayTreeMapGrouping = SplayTreeMap<String, List<ActivityDTO>?>.from(dynamicGroupMap, (a, b) => a.compareTo(b));
          splayTreeMapGrouping.forEach((key, value) {
            List<int> activityIds = [];
            for (var element in value ?? []) {
              activityIds.add(element.id ?? 0);
            }
            rootLevel.add(
              GroupingTreeNodeDTO(
                  level: 1,
                  nodeElementType: ElementTypeEnum.root,
                  nodeParent: leaf.nodeParent,
                  nodeValue: "${leaf.nodeParent}<[%%]>${key.toString()}"),
            );
            rootLevel.add(GroupingTreeNodeDTO(
              level: 2,
              nodeParent: "${leaf.nodeParent}<[%%]>${key.toString()}",
              nodeElementType: ElementTypeEnum.leaf,
              nodeValue: activityIds.join(',').toString(),
            ));
          });
        }
        rootLevel.remove(leaf);
      }
    }
    return rootLevel;
  }

  List<GroupingTreeNodeDTO>? getLevel3Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingField}) {
    final ActivityFieldDTO level3GroupingField = groupingField[2];
    var rootLevel = getLevel2Grouping(allActivities: allActivities, groupingField: groupingField);
    if (rootLevel != null) {
      final prevLeafs = rootLevel.where((element) => element.level == 2).toList();
      for (var leaf in prevLeafs) {
        List<ActivityDTO> activitiesToGroup = [];
        List<String> activitiesIdsInLeaf = leaf.nodeValue?.split(',') ?? [];
        List<int> listOfIds = [];

        if (activitiesIdsInLeaf != []) {
          listOfIds = activitiesIdsInLeaf.map(int.parse).toList();
        }
        //
        for (var id in listOfIds) {
          activitiesToGroup.add(allActivities.where((element) => element.id == id).first);
        }

        //
        if (level3GroupingField.fieldType != fieldTypeDynamic) {
          final itrGroup = groupBy(
            activitiesToGroup,
            (obj) => obj.getProperty(level3GroupingField.staticFieldName ?? ''),
          );

          final splayTreeMapGrouping = SplayTreeMap<dynamic, List<ActivityDTO>>.from(itrGroup, (a, b) => a.compareTo(b));
          splayTreeMapGrouping.forEach((key, value) {
            if (key.toString() != "null") {
              List<int> activityIds = [];
              for (var element in value) {
                activityIds.add(element.id ?? 0);
              }
              rootLevel.add(
                GroupingTreeNodeDTO(
                  level: 2,
                  nodeElementType: ElementTypeEnum.node,
                  nodeParent: leaf.nodeParent,
                  nodeValue: "${leaf.nodeParent}<[%%]>${key.toString()}",
                  fieldValueTypeEnum:
                      level3GroupingField.staticFieldName != null && level3GroupingField.staticFieldName == "percentComplete"
                          ? FieldValueTypeEnum.percentage
                          : FieldValueTypeEnum.text,
                ),
              );
              rootLevel.add(GroupingTreeNodeDTO(
                level: 3,
                nodeParent: "${leaf.nodeParent}<[%%]>${key.toString()}",
                nodeElementType: ElementTypeEnum.leaf,
                nodeValue: activityIds.join(',').toString(),
              ));
            }
          });
        } else {
          Map<String, List<ActivityDTO>?> dynamicGroupMap =
              getDynamicGrouping(activitiesToGroup: activitiesToGroup, groupingField: level3GroupingField);
          final splayTreeMapGrouping = SplayTreeMap<String, List<ActivityDTO>?>.from(dynamicGroupMap, (a, b) => a.compareTo(b));
          splayTreeMapGrouping.forEach((key, value) {
            List<int> activityIds = [];
            for (var element in value ?? []) {
              activityIds.add(element.id ?? 0);
            }
            rootLevel.add(
              GroupingTreeNodeDTO(
                level: 2,
                nodeElementType: ElementTypeEnum.root,
                nodeParent: leaf.nodeParent,
                nodeValue: "${leaf.nodeParent}<[%%]>${key.toString()}",
              ),
            );
            rootLevel.add(GroupingTreeNodeDTO(
              level: 3,
              nodeParent: "${leaf.nodeParent}<[%%]>${key.toString()}",
              nodeElementType: ElementTypeEnum.leaf,
              nodeValue: activityIds.join(',').toString(),
            ));
          });
        }
        rootLevel.remove(leaf);
      }
    }
    return rootLevel;
  }

  Map<String, List<ActivityDTO>?> getDynamicGrouping(
      {required List<ActivityDTO> activitiesToGroup, required ActivityFieldDTO groupingField}) {
    final Map<String, List<ActivityDTO>?> dynamicGroupMap = {};
    for (var element in activitiesToGroup) {
      Map<String, dynamic>? dynamicFieldData = element.dynamicFieldData != null ? jsonDecode(element.dynamicFieldData ?? '') : {};
      Map<String, dynamic>? dynamicFieldDateTimeData =
          element.dynamicFieldDateTimeData != null ? jsonDecode(element.dynamicFieldDateTimeData ?? '') : {};
      Map<String, dynamic>? dynamicFieldNumberData =
          element.dynamicFieldNumberData != null ? jsonDecode(element.dynamicFieldNumberData ?? '') : {};

      if (dynamicFieldData != null && dynamicFieldData.containsKey(groupingField.dynamicFieldName)) {
        final dynamicValue = dynamicFieldData[groupingField.dynamicFieldName]?.toString();
        if (dynamicValue != null) {
          List<ActivityDTO>? tempData = [];
          final previousList = dynamicGroupMap[dynamicValue];
          tempData = List.from(previousList ?? []);
          tempData.add(element);
          dynamicGroupMap[dynamicValue] = tempData;
        }
      }
      if (dynamicFieldDateTimeData != null && dynamicFieldDateTimeData.containsKey(groupingField.dynamicFieldName)) {
        final dynamicValue = dynamicFieldDateTimeData[groupingField.dynamicFieldName]?.toString();
        if (dynamicValue != null) {
          List<ActivityDTO>? tempData = [];
          final previousList = dynamicGroupMap[dynamicValue];
          tempData = List.from(previousList ?? []);
          tempData.add(element);
          dynamicGroupMap[dynamicValue] = tempData;
        }
      }
      if (dynamicFieldNumberData != null && dynamicFieldNumberData.containsKey(groupingField.dynamicFieldName)) {
        final dynamicValue = dynamicFieldNumberData[groupingField.dynamicFieldName]?.toString();
        if (dynamicValue != null) {
          List<ActivityDTO>? tempData = [];
          final previousList = dynamicGroupMap[dynamicValue];
          tempData = List.from(previousList ?? []);
          tempData.add(element);
          dynamicGroupMap[dynamicValue] = tempData;
        }
      }
    }
    return dynamicGroupMap;
  }
}

const int fieldTypeMandatory = 2;
const int fieldTypeDynamic = 1;
const int fieldTypeStatic = 0;
