import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

@lazySingleton
class SelectedMultiLevelSort {
  static MultiLevelSortDTO? multiLevelSort;
  IActivityGroupingRepository get _activityGroupingRepository => getIt<IActivityGroupingRepository>();

  void updateSelectedMultiLevelSort({required MultiLevelSortDTO? selectedMultiLevelSort}) async {
    if (selectedMultiLevelSort != null) {
      multiLevelSort = selectedMultiLevelSort;
      await _activityGroupingRepository.saveSelectedMultiLevelSort(multiLevelSortDTO: selectedMultiLevelSort);
    }
  }

  void resetSelectedMultiLevelSort() async {
    await _activityGroupingRepository.clearSelectedMultiLevelSort();
    multiLevelSort = null;
  }
}
