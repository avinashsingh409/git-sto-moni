import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_response.dart';

@injectable
class TurnAroundActivityApiService {
  final Dio dio;

  TurnAroundActivityApiService(this.dio);

  Future<ActivityResponse> getTurnAroundActivities({required int index, required int limit}) async {
    final response = await dio.get(ApiEndPoints.activities(index, limit));
    final activityResponse = ActivityResponse.fromJson(response.data);
    return activityResponse;
  }
}
