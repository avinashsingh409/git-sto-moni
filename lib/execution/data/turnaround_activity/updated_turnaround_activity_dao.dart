import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';

import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class UpdatedTurnAroundActivityDAO {
  final isar = PersistenceModule.isar;

  /// Isar Collection of TurnAroundActivities
  late IsarCollection<UpdatedActivityDTO> collection = isar.updatedActivityDTOs;

  /// Delete All Updated TurnAroundActivities
  Future<void> deleteTurnAroudActivities() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All TurnAroundActivities
  Future<List<UpdatedActivityDTO>> getAllTurnAroundActivities() async {
    final updatedTurnAroundActivies = collection.where().findAll();
    return updatedTurnAroundActivies;
  }

  Future<void> updateActivity({required UpdatedActivityDTO updatedActivity}) async {
    await isar.writeTxn(() async {
      await collection.put(updatedActivity);
    });
  }

  /// Watch sync pending
  BehaviorSubject<bool> isSyncPending() {
    final bstream = BehaviorSubject<bool>();
    final query = collection.where().anyId().build();
    final stream = query.watch(fireImmediately: true);
    stream.listen((item) {
      if (item.isNotEmpty) {
        bstream.add(true);
      } else {
        bstream.add(false);
      }
    });
    return bstream;
  }

  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent() async {
    final updatedChanges = await collection.where().anyId().build().findAll();
    if (updatedChanges.isNotEmpty) {
      return CacheStatus.unsafe;
    } else {
      return CacheStatus.safe;
    }
  }
}
