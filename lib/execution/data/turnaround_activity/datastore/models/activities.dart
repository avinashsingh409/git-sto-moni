import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity.dart';
part 'activities.freezed.dart';
part 'activities.g.dart';

@freezed
abstract class Activities with _$Activities {
  const factory Activities({
    required int totalCount,
    required int page,
    required int limit,
    required List<Activity>? results,
  }) = _Activities;

  factory Activities.fromJson(Map<String, dynamic> json) => _$ActivitiesFromJson(json);
}
