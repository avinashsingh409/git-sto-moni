// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Activity _$$_ActivityFromJson(Map<String, dynamic> json) => _$_Activity(
      id: json['id'] as int?,
      activity: json['activity'] as String?,
      description: json['description'] as String?,
      dateCreated: json['dateCreated'] == null
          ? null
          : DateTime.parse(json['dateCreated'] as String),
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      finishDate: json['finishDate'] == null
          ? null
          : DateTime.parse(json['finishDate'] as String),
      remainingDuration: (json['remainingDuration'] as num?)?.toDouble(),
      percentComplete: (json['percentComplete'] as num?)?.toDouble(),
      actualStartDate: json['actualStartDate'] == null
          ? null
          : DateTime.parse(json['actualStartDate'] as String),
      actualFinishDate: json['actualFinishDate'] == null
          ? null
          : DateTime.parse(json['actualFinishDate'] as String),
      hostName: json['hostName'] as String?,
      projectId: json['projectId'] as String?,
      changedPercentComplete:
          (json['changedPercentComplete'] as num?)?.toDouble(),
      dynamicFieldData:
          (json['dynamicFieldData'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String?),
      ),
      dynamicFieldDateTimeData:
          (json['dynamicFieldDateTimeData'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String?),
      ),
      dynamicFieldNumberData:
          json['dynamicFieldNumberData'] as Map<String, dynamic>?,
      changed: json['changed'] as bool?,
      wbsObjectId: json['wbsObjectId'] as int?,
      percentCompleteColor: json['percentCompleteColor'] as String?,
      wbsRoute: (json['wbsRoute'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      objectId: json['objectId'] as int?,
      calendarName: json['calendarName'] as String?,
      totalFloat: (json['totalFloat'] as num?)?.toDouble(),
      plannedDuration: (json['plannedDuration'] as num?)?.toDouble(),
      sentForPrimaveraSync: json['sentForPrimaveraSync'] as bool?,
      commentCount: json['commentCount'] as int?,
      plannedStartDate: json['plannedStartDate'] == null
          ? null
          : DateTime.parse(json['plannedStartDate'] as String),
      plannedFinishDate: json['plannedFinishDate'] == null
          ? null
          : DateTime.parse(json['plannedFinishDate'] as String),
      remainingEarlyStartDate: json['remainingEarlyStartDate'] == null
          ? null
          : DateTime.parse(json['remainingEarlyStartDate'] as String),
      remainingEarlyFinishDate: json['remainingEarlyFinishDate'] == null
          ? null
          : DateTime.parse(json['remainingEarlyFinishDate'] as String),
      budgetedLaborUnits: (json['budgetedLaborUnits'] as num?)?.toDouble(),
      budgetedNonLaborUnits:
          (json['budgetedNonLaborUnits'] as num?)?.toDouble(),
      remainingLaborUnits: (json['remainingLaborUnits'] as num?)?.toDouble(),
      remainingNonLaborUnits:
          (json['remainingNonLaborUnits'] as num?)?.toDouble(),
      isDeleted: json['isDeleted'] as bool?,
      unableToWork: json['unableToWork'] as bool?,
      preApproved: json['preApproved'] as bool?,
      wbsName: json['wbsName'] as String?,
      freeFloat: (json['freeFloat'] as num?)?.toDouble(),
      changedActualStartDate: json['changedActualStartDate'] == null
          ? null
          : DateTime.parse(json['changedActualStartDate'] as String),
      changedActualFinishDate: json['changedActualFinishDate'] == null
          ? null
          : DateTime.parse(json['changedActualFinishDate'] as String),
      modifiedById: json['modifiedById'] as int?,
      modifiedByName: json['modifiedByName'] as String?,
      dateModified: json['dateModified'] == null
          ? null
          : DateTime.parse(json['dateModified'] as String),
      unableToWorkDateTime: json['unableToWorkDateTime'] == null
          ? null
          : DateTime.parse(json['unableToWorkDateTime'] as String),
      unableToWorkReasons: (json['unableToWorkReasons'] as List<dynamic>?)
          ?.map((e) => e as int?)
          .toList(),
      unableToWorkAdditionalInfo: json['unableToWorkAdditionalInfo'] as String?,
      unableToWorkImageFilePaths:
          (json['unableToWorkImageFilePaths'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
      backendHasAttachments: json['backendHasAttachments'] as bool?,
      holder: json['holder'] as String?,
      rejectedPercentComplete:
          (json['rejectedPercentComplete'] as num?)?.toDouble(),
      rejectedStartDate: json['rejectedStartDate'] == null
          ? null
          : DateTime.parse(json['rejectedStartDate'] as String),
      rejectedFinishDate: json['rejectedFinishDate'] == null
          ? null
          : DateTime.parse(json['rejectedFinishDate'] as String),
      predecessors: (json['predecessors'] as List<dynamic>?)
          ?.map(
              (e) => PredecessorActivityDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      successors: (json['successors'] as List<dynamic>?)
          ?.map(
              (e) => SuccessorsActivityDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      predecessorsStatus: $enumDecodeNullable(
          _$PrimaveraActivityStatusEnumMap, json['predecessorsStatus']),
      successorsStatus: $enumDecodeNullable(
          _$PrimaveraActivityStatusEnumMap, json['successorsStatus']),
      manualDateSet: json['manualDateSet'] as bool?,
      lastPercentComplete: (json['lastPercentComplete'] as num?)?.toDouble(),
      workPackageId: json['workPackageId'] as int?,
      dataDate: json['dataDate'] == null
          ? null
          : DateTime.parse(json['dataDate'] as String),
    );

Map<String, dynamic> _$$_ActivityToJson(_$_Activity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'activity': instance.activity,
      'description': instance.description,
      'dateCreated': instance.dateCreated?.toIso8601String(),
      'startDate': instance.startDate?.toIso8601String(),
      'finishDate': instance.finishDate?.toIso8601String(),
      'remainingDuration': instance.remainingDuration,
      'percentComplete': instance.percentComplete,
      'actualStartDate': instance.actualStartDate?.toIso8601String(),
      'actualFinishDate': instance.actualFinishDate?.toIso8601String(),
      'hostName': instance.hostName,
      'projectId': instance.projectId,
      'changedPercentComplete': instance.changedPercentComplete,
      'dynamicFieldData': instance.dynamicFieldData,
      'dynamicFieldDateTimeData': instance.dynamicFieldDateTimeData,
      'dynamicFieldNumberData': instance.dynamicFieldNumberData,
      'changed': instance.changed,
      'wbsObjectId': instance.wbsObjectId,
      'percentCompleteColor': instance.percentCompleteColor,
      'wbsRoute': instance.wbsRoute,
      'objectId': instance.objectId,
      'calendarName': instance.calendarName,
      'totalFloat': instance.totalFloat,
      'plannedDuration': instance.plannedDuration,
      'sentForPrimaveraSync': instance.sentForPrimaveraSync,
      'commentCount': instance.commentCount,
      'plannedStartDate': instance.plannedStartDate?.toIso8601String(),
      'plannedFinishDate': instance.plannedFinishDate?.toIso8601String(),
      'remainingEarlyStartDate':
          instance.remainingEarlyStartDate?.toIso8601String(),
      'remainingEarlyFinishDate':
          instance.remainingEarlyFinishDate?.toIso8601String(),
      'budgetedLaborUnits': instance.budgetedLaborUnits,
      'budgetedNonLaborUnits': instance.budgetedNonLaborUnits,
      'remainingLaborUnits': instance.remainingLaborUnits,
      'remainingNonLaborUnits': instance.remainingNonLaborUnits,
      'isDeleted': instance.isDeleted,
      'unableToWork': instance.unableToWork,
      'preApproved': instance.preApproved,
      'wbsName': instance.wbsName,
      'freeFloat': instance.freeFloat,
      'changedActualStartDate':
          instance.changedActualStartDate?.toIso8601String(),
      'changedActualFinishDate':
          instance.changedActualFinishDate?.toIso8601String(),
      'modifiedById': instance.modifiedById,
      'modifiedByName': instance.modifiedByName,
      'dateModified': instance.dateModified?.toIso8601String(),
      'unableToWorkDateTime': instance.unableToWorkDateTime?.toIso8601String(),
      'unableToWorkReasons': instance.unableToWorkReasons,
      'unableToWorkAdditionalInfo': instance.unableToWorkAdditionalInfo,
      'unableToWorkImageFilePaths': instance.unableToWorkImageFilePaths,
      'backendHasAttachments': instance.backendHasAttachments,
      'holder': instance.holder,
      'rejectedPercentComplete': instance.rejectedPercentComplete,
      'rejectedStartDate': instance.rejectedStartDate?.toIso8601String(),
      'rejectedFinishDate': instance.rejectedFinishDate?.toIso8601String(),
      'predecessors': instance.predecessors,
      'successors': instance.successors,
      'predecessorsStatus':
          _$PrimaveraActivityStatusEnumMap[instance.predecessorsStatus],
      'successorsStatus':
          _$PrimaveraActivityStatusEnumMap[instance.successorsStatus],
      'manualDateSet': instance.manualDateSet,
      'lastPercentComplete': instance.lastPercentComplete,
      'workPackageId': instance.workPackageId,
      'dataDate': instance.dataDate?.toIso8601String(),
    };

const _$PrimaveraActivityStatusEnumMap = {
  PrimaveraActivityStatus.notStarted: 1,
  PrimaveraActivityStatus.inProgress: 2,
  PrimaveraActivityStatus.completed: 3,
  PrimaveraActivityStatus.unableToWork: 4,
};
