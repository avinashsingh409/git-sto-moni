// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ActivityResponse _$$_ActivityResponseFromJson(Map<String, dynamic> json) =>
    _$_ActivityResponse(
      activities: json['activities'] == null
          ? null
          : Activities.fromJson(json['activities'] as Map<String, dynamic>),
      activityFields: (json['activityFields'] as List<dynamic>?)
          ?.map((e) => ActivityFieldDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ActivityResponseToJson(_$_ActivityResponse instance) =>
    <String, dynamic>{
      'activities': instance.activities,
      'activityFields': instance.activityFields,
    };
