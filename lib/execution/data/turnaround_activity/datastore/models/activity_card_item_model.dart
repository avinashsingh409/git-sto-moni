import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/predecessor_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/successors_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/primavera_activity_status_enum.dart';

part 'activity_card_item_model.freezed.dart';

@freezed
abstract class ActivityCardItemModel with _$ActivityCardItemModel {
  const factory ActivityCardItemModel({
    required int id,
    required DateTime startDate,
    required DateTime finishDate,
    required double displayedPercentComplete,
    required String activity,
    required String description,
    required bool unableToWork,
    required List<PredecessorActivityDTO> predecessors,
    required List<SuccessorsActivityDTO> successors,
    required PrimaveraActivityStatus? predecessorsStatus,
    required PrimaveraActivityStatus? successorsStatus,
    required int? commentCount,
    required String? displayDate,
    required String? displayFinishDate,
    required String? dynamicFieldValue,
  }) = _ActivityCardItemModel;

  factory ActivityCardItemModel.fromModel({required ActivityDTO activityDTO, String? dynamicFieldName}) {
    //Adding dynamic field value to sort in UI
    String? dynamicFieldValue;
    if (dynamicFieldName != null) {
      if (activityDTO.dynamicFieldData != null && activityDTO.dynamicFieldData!.contains(dynamicFieldName)) {
        final dynamicMap = jsonDecode(activityDTO.dynamicFieldData!);
        dynamicFieldValue = dynamicMap[dynamicFieldName];
      } else if (activityDTO.dynamicFieldDateTimeData != null && activityDTO.dynamicFieldNumberData!.contains(dynamicFieldName)) {
        final dynamicMap = jsonDecode(activityDTO.dynamicFieldDateTimeData!);
        dynamicFieldValue = dynamicMap[dynamicFieldName];
      } else if (activityDTO.dynamicFieldNumberData != null && activityDTO.dynamicFieldNumberData!.contains(dynamicFieldName)) {
        final dynamicMap = jsonDecode(activityDTO.dynamicFieldNumberData!);
        dynamicFieldValue = dynamicMap[dynamicFieldName];
      }
    }
    final activityCardItemModel = ActivityCardItemModel(
      id: activityDTO.id ?? 999999999,
      startDate: activityDTO.startDate ?? DateTime.now(),
      finishDate: activityDTO.finishDate ?? DateTime.now(),
      displayedPercentComplete: activityDTO.percentComplete ?? 0,
      activity: activityDTO.activity ?? '',
      unableToWork: activityDTO.unableToWork ?? false,
      description: activityDTO.description ?? '',
      predecessors: activityDTO.predecessors ?? [],
      successors: activityDTO.successors ?? [],
      predecessorsStatus: activityDTO.predecessorsStatus,
      successorsStatus: activityDTO.successorsStatus,
      commentCount: activityDTO.commentCount,
      displayDate: activityDTO.displayDate,
      displayFinishDate: activityDTO.displayFinishDate,
      dynamicFieldValue: dynamicFieldValue,
    );
    return activityCardItemModel;
  }
}
