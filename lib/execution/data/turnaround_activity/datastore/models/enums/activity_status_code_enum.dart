import 'package:json_annotation/json_annotation.dart';

/// Activity Status Codes
enum ActivityStatusEnum {
  /// Finished
  @JsonValue(0)
  finished,

  /// Invalid
  @JsonValue(1)
  invalid,

  /// Not Started
  @JsonValue(2)
  notStarted,

  /// Paused
  @JsonValue(3)
  paused,

  /// Started
  @JsonValue(4)
  started;
}
