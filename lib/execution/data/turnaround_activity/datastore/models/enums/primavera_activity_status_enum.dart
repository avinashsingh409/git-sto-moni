import 'package:json_annotation/json_annotation.dart';

enum PrimaveraActivityStatus {
  @JsonValue(1)
  notStarted,

  @JsonValue(2)
  inProgress,

  @JsonValue(3)
  completed,

  @JsonValue(4)
  unableToWork
}
