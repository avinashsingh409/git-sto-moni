import 'package:json_annotation/json_annotation.dart';

/// Activity Status Codes
enum RelationshipTypeEnum {
  /// finish to start
  @JsonValue(0)
  fs,

  /// finish to finish
  @JsonValue(1)
  ff,

  /// start to start
  @JsonValue(2)
  ss,

  /// start to finish
  @JsonValue(3)
  sf,
}
