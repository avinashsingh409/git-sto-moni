// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'activity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Activity _$ActivityFromJson(Map<String, dynamic> json) {
  return _Activity.fromJson(json);
}

/// @nodoc
mixin _$Activity {
  int? get id => throw _privateConstructorUsedError;
  String? get activity => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  DateTime? get dateCreated => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get finishDate => throw _privateConstructorUsedError;
  double? get remainingDuration => throw _privateConstructorUsedError;
  double? get percentComplete => throw _privateConstructorUsedError;
  DateTime? get actualStartDate => throw _privateConstructorUsedError;
  DateTime? get actualFinishDate => throw _privateConstructorUsedError;
  String? get hostName => throw _privateConstructorUsedError;
  String? get projectId => throw _privateConstructorUsedError;
  double? get changedPercentComplete => throw _privateConstructorUsedError;
  Map<String, String?>? get dynamicFieldData =>
      throw _privateConstructorUsedError;
  Map<String, String?>? get dynamicFieldDateTimeData =>
      throw _privateConstructorUsedError;
  Map<dynamic, dynamic>? get dynamicFieldNumberData =>
      throw _privateConstructorUsedError;
  bool? get changed => throw _privateConstructorUsedError;
  int? get wbsObjectId => throw _privateConstructorUsedError;
  String? get percentCompleteColor => throw _privateConstructorUsedError;
  List<String>? get wbsRoute => throw _privateConstructorUsedError;
  int? get objectId => throw _privateConstructorUsedError;
  String? get calendarName => throw _privateConstructorUsedError;
  double? get totalFloat => throw _privateConstructorUsedError;
  double? get plannedDuration => throw _privateConstructorUsedError;
  bool? get sentForPrimaveraSync => throw _privateConstructorUsedError;
  int? get commentCount => throw _privateConstructorUsedError;
  DateTime? get plannedStartDate => throw _privateConstructorUsedError;
  DateTime? get plannedFinishDate => throw _privateConstructorUsedError;
  DateTime? get remainingEarlyStartDate => throw _privateConstructorUsedError;
  DateTime? get remainingEarlyFinishDate => throw _privateConstructorUsedError;
  double? get budgetedLaborUnits => throw _privateConstructorUsedError;
  double? get budgetedNonLaborUnits => throw _privateConstructorUsedError;
  double? get remainingLaborUnits => throw _privateConstructorUsedError;
  double? get remainingNonLaborUnits => throw _privateConstructorUsedError;
  bool? get isDeleted => throw _privateConstructorUsedError;
  bool? get unableToWork => throw _privateConstructorUsedError;
  bool? get preApproved => throw _privateConstructorUsedError;
  String? get wbsName => throw _privateConstructorUsedError;
  double? get freeFloat => throw _privateConstructorUsedError;
  DateTime? get changedActualStartDate => throw _privateConstructorUsedError;
  DateTime? get changedActualFinishDate => throw _privateConstructorUsedError;
  int? get modifiedById => throw _privateConstructorUsedError;
  String? get modifiedByName => throw _privateConstructorUsedError;
  DateTime? get dateModified => throw _privateConstructorUsedError;
  DateTime? get unableToWorkDateTime => throw _privateConstructorUsedError;
  List<int?>? get unableToWorkReasons => throw _privateConstructorUsedError;
  String? get unableToWorkAdditionalInfo => throw _privateConstructorUsedError;
  List<String>? get unableToWorkImageFilePaths =>
      throw _privateConstructorUsedError;
  bool? get backendHasAttachments => throw _privateConstructorUsedError;
  String? get holder => throw _privateConstructorUsedError;
  double? get rejectedPercentComplete => throw _privateConstructorUsedError;
  DateTime? get rejectedStartDate => throw _privateConstructorUsedError;
  DateTime? get rejectedFinishDate => throw _privateConstructorUsedError;
  List<PredecessorActivityDTO>? get predecessors =>
      throw _privateConstructorUsedError;
  List<SuccessorsActivityDTO>? get successors =>
      throw _privateConstructorUsedError;
  PrimaveraActivityStatus? get predecessorsStatus =>
      throw _privateConstructorUsedError;
  PrimaveraActivityStatus? get successorsStatus =>
      throw _privateConstructorUsedError;
  bool? get manualDateSet => throw _privateConstructorUsedError;
  double? get lastPercentComplete => throw _privateConstructorUsedError;
  int? get workPackageId => throw _privateConstructorUsedError;
  DateTime? get dataDate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActivityCopyWith<Activity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityCopyWith<$Res> {
  factory $ActivityCopyWith(Activity value, $Res Function(Activity) then) =
      _$ActivityCopyWithImpl<$Res, Activity>;
  @useResult
  $Res call(
      {int? id,
      String? activity,
      String? description,
      DateTime? dateCreated,
      DateTime? startDate,
      DateTime? finishDate,
      double? remainingDuration,
      double? percentComplete,
      DateTime? actualStartDate,
      DateTime? actualFinishDate,
      String? hostName,
      String? projectId,
      double? changedPercentComplete,
      Map<String, String?>? dynamicFieldData,
      Map<String, String?>? dynamicFieldDateTimeData,
      Map<dynamic, dynamic>? dynamicFieldNumberData,
      bool? changed,
      int? wbsObjectId,
      String? percentCompleteColor,
      List<String>? wbsRoute,
      int? objectId,
      String? calendarName,
      double? totalFloat,
      double? plannedDuration,
      bool? sentForPrimaveraSync,
      int? commentCount,
      DateTime? plannedStartDate,
      DateTime? plannedFinishDate,
      DateTime? remainingEarlyStartDate,
      DateTime? remainingEarlyFinishDate,
      double? budgetedLaborUnits,
      double? budgetedNonLaborUnits,
      double? remainingLaborUnits,
      double? remainingNonLaborUnits,
      bool? isDeleted,
      bool? unableToWork,
      bool? preApproved,
      String? wbsName,
      double? freeFloat,
      DateTime? changedActualStartDate,
      DateTime? changedActualFinishDate,
      int? modifiedById,
      String? modifiedByName,
      DateTime? dateModified,
      DateTime? unableToWorkDateTime,
      List<int?>? unableToWorkReasons,
      String? unableToWorkAdditionalInfo,
      List<String>? unableToWorkImageFilePaths,
      bool? backendHasAttachments,
      String? holder,
      double? rejectedPercentComplete,
      DateTime? rejectedStartDate,
      DateTime? rejectedFinishDate,
      List<PredecessorActivityDTO>? predecessors,
      List<SuccessorsActivityDTO>? successors,
      PrimaveraActivityStatus? predecessorsStatus,
      PrimaveraActivityStatus? successorsStatus,
      bool? manualDateSet,
      double? lastPercentComplete,
      int? workPackageId,
      DateTime? dataDate});
}

/// @nodoc
class _$ActivityCopyWithImpl<$Res, $Val extends Activity>
    implements $ActivityCopyWith<$Res> {
  _$ActivityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? activity = freezed,
    Object? description = freezed,
    Object? dateCreated = freezed,
    Object? startDate = freezed,
    Object? finishDate = freezed,
    Object? remainingDuration = freezed,
    Object? percentComplete = freezed,
    Object? actualStartDate = freezed,
    Object? actualFinishDate = freezed,
    Object? hostName = freezed,
    Object? projectId = freezed,
    Object? changedPercentComplete = freezed,
    Object? dynamicFieldData = freezed,
    Object? dynamicFieldDateTimeData = freezed,
    Object? dynamicFieldNumberData = freezed,
    Object? changed = freezed,
    Object? wbsObjectId = freezed,
    Object? percentCompleteColor = freezed,
    Object? wbsRoute = freezed,
    Object? objectId = freezed,
    Object? calendarName = freezed,
    Object? totalFloat = freezed,
    Object? plannedDuration = freezed,
    Object? sentForPrimaveraSync = freezed,
    Object? commentCount = freezed,
    Object? plannedStartDate = freezed,
    Object? plannedFinishDate = freezed,
    Object? remainingEarlyStartDate = freezed,
    Object? remainingEarlyFinishDate = freezed,
    Object? budgetedLaborUnits = freezed,
    Object? budgetedNonLaborUnits = freezed,
    Object? remainingLaborUnits = freezed,
    Object? remainingNonLaborUnits = freezed,
    Object? isDeleted = freezed,
    Object? unableToWork = freezed,
    Object? preApproved = freezed,
    Object? wbsName = freezed,
    Object? freeFloat = freezed,
    Object? changedActualStartDate = freezed,
    Object? changedActualFinishDate = freezed,
    Object? modifiedById = freezed,
    Object? modifiedByName = freezed,
    Object? dateModified = freezed,
    Object? unableToWorkDateTime = freezed,
    Object? unableToWorkReasons = freezed,
    Object? unableToWorkAdditionalInfo = freezed,
    Object? unableToWorkImageFilePaths = freezed,
    Object? backendHasAttachments = freezed,
    Object? holder = freezed,
    Object? rejectedPercentComplete = freezed,
    Object? rejectedStartDate = freezed,
    Object? rejectedFinishDate = freezed,
    Object? predecessors = freezed,
    Object? successors = freezed,
    Object? predecessorsStatus = freezed,
    Object? successorsStatus = freezed,
    Object? manualDateSet = freezed,
    Object? lastPercentComplete = freezed,
    Object? workPackageId = freezed,
    Object? dataDate = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      activity: freezed == activity
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      dateCreated: freezed == dateCreated
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      startDate: freezed == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      finishDate: freezed == finishDate
          ? _value.finishDate
          : finishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingDuration: freezed == remainingDuration
          ? _value.remainingDuration
          : remainingDuration // ignore: cast_nullable_to_non_nullable
              as double?,
      percentComplete: freezed == percentComplete
          ? _value.percentComplete
          : percentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      actualStartDate: freezed == actualStartDate
          ? _value.actualStartDate
          : actualStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      actualFinishDate: freezed == actualFinishDate
          ? _value.actualFinishDate
          : actualFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      hostName: freezed == hostName
          ? _value.hostName
          : hostName // ignore: cast_nullable_to_non_nullable
              as String?,
      projectId: freezed == projectId
          ? _value.projectId
          : projectId // ignore: cast_nullable_to_non_nullable
              as String?,
      changedPercentComplete: freezed == changedPercentComplete
          ? _value.changedPercentComplete
          : changedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      dynamicFieldData: freezed == dynamicFieldData
          ? _value.dynamicFieldData
          : dynamicFieldData // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      dynamicFieldDateTimeData: freezed == dynamicFieldDateTimeData
          ? _value.dynamicFieldDateTimeData
          : dynamicFieldDateTimeData // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      dynamicFieldNumberData: freezed == dynamicFieldNumberData
          ? _value.dynamicFieldNumberData
          : dynamicFieldNumberData // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      changed: freezed == changed
          ? _value.changed
          : changed // ignore: cast_nullable_to_non_nullable
              as bool?,
      wbsObjectId: freezed == wbsObjectId
          ? _value.wbsObjectId
          : wbsObjectId // ignore: cast_nullable_to_non_nullable
              as int?,
      percentCompleteColor: freezed == percentCompleteColor
          ? _value.percentCompleteColor
          : percentCompleteColor // ignore: cast_nullable_to_non_nullable
              as String?,
      wbsRoute: freezed == wbsRoute
          ? _value.wbsRoute
          : wbsRoute // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      objectId: freezed == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as int?,
      calendarName: freezed == calendarName
          ? _value.calendarName
          : calendarName // ignore: cast_nullable_to_non_nullable
              as String?,
      totalFloat: freezed == totalFloat
          ? _value.totalFloat
          : totalFloat // ignore: cast_nullable_to_non_nullable
              as double?,
      plannedDuration: freezed == plannedDuration
          ? _value.plannedDuration
          : plannedDuration // ignore: cast_nullable_to_non_nullable
              as double?,
      sentForPrimaveraSync: freezed == sentForPrimaveraSync
          ? _value.sentForPrimaveraSync
          : sentForPrimaveraSync // ignore: cast_nullable_to_non_nullable
              as bool?,
      commentCount: freezed == commentCount
          ? _value.commentCount
          : commentCount // ignore: cast_nullable_to_non_nullable
              as int?,
      plannedStartDate: freezed == plannedStartDate
          ? _value.plannedStartDate
          : plannedStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      plannedFinishDate: freezed == plannedFinishDate
          ? _value.plannedFinishDate
          : plannedFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingEarlyStartDate: freezed == remainingEarlyStartDate
          ? _value.remainingEarlyStartDate
          : remainingEarlyStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingEarlyFinishDate: freezed == remainingEarlyFinishDate
          ? _value.remainingEarlyFinishDate
          : remainingEarlyFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      budgetedLaborUnits: freezed == budgetedLaborUnits
          ? _value.budgetedLaborUnits
          : budgetedLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      budgetedNonLaborUnits: freezed == budgetedNonLaborUnits
          ? _value.budgetedNonLaborUnits
          : budgetedNonLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      remainingLaborUnits: freezed == remainingLaborUnits
          ? _value.remainingLaborUnits
          : remainingLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      remainingNonLaborUnits: freezed == remainingNonLaborUnits
          ? _value.remainingNonLaborUnits
          : remainingNonLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool?,
      unableToWork: freezed == unableToWork
          ? _value.unableToWork
          : unableToWork // ignore: cast_nullable_to_non_nullable
              as bool?,
      preApproved: freezed == preApproved
          ? _value.preApproved
          : preApproved // ignore: cast_nullable_to_non_nullable
              as bool?,
      wbsName: freezed == wbsName
          ? _value.wbsName
          : wbsName // ignore: cast_nullable_to_non_nullable
              as String?,
      freeFloat: freezed == freeFloat
          ? _value.freeFloat
          : freeFloat // ignore: cast_nullable_to_non_nullable
              as double?,
      changedActualStartDate: freezed == changedActualStartDate
          ? _value.changedActualStartDate
          : changedActualStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      changedActualFinishDate: freezed == changedActualFinishDate
          ? _value.changedActualFinishDate
          : changedActualFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      modifiedById: freezed == modifiedById
          ? _value.modifiedById
          : modifiedById // ignore: cast_nullable_to_non_nullable
              as int?,
      modifiedByName: freezed == modifiedByName
          ? _value.modifiedByName
          : modifiedByName // ignore: cast_nullable_to_non_nullable
              as String?,
      dateModified: freezed == dateModified
          ? _value.dateModified
          : dateModified // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      unableToWorkDateTime: freezed == unableToWorkDateTime
          ? _value.unableToWorkDateTime
          : unableToWorkDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      unableToWorkReasons: freezed == unableToWorkReasons
          ? _value.unableToWorkReasons
          : unableToWorkReasons // ignore: cast_nullable_to_non_nullable
              as List<int?>?,
      unableToWorkAdditionalInfo: freezed == unableToWorkAdditionalInfo
          ? _value.unableToWorkAdditionalInfo
          : unableToWorkAdditionalInfo // ignore: cast_nullable_to_non_nullable
              as String?,
      unableToWorkImageFilePaths: freezed == unableToWorkImageFilePaths
          ? _value.unableToWorkImageFilePaths
          : unableToWorkImageFilePaths // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      backendHasAttachments: freezed == backendHasAttachments
          ? _value.backendHasAttachments
          : backendHasAttachments // ignore: cast_nullable_to_non_nullable
              as bool?,
      holder: freezed == holder
          ? _value.holder
          : holder // ignore: cast_nullable_to_non_nullable
              as String?,
      rejectedPercentComplete: freezed == rejectedPercentComplete
          ? _value.rejectedPercentComplete
          : rejectedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      rejectedStartDate: freezed == rejectedStartDate
          ? _value.rejectedStartDate
          : rejectedStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      rejectedFinishDate: freezed == rejectedFinishDate
          ? _value.rejectedFinishDate
          : rejectedFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      predecessors: freezed == predecessors
          ? _value.predecessors
          : predecessors // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>?,
      successors: freezed == successors
          ? _value.successors
          : successors // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>?,
      predecessorsStatus: freezed == predecessorsStatus
          ? _value.predecessorsStatus
          : predecessorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      successorsStatus: freezed == successorsStatus
          ? _value.successorsStatus
          : successorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      manualDateSet: freezed == manualDateSet
          ? _value.manualDateSet
          : manualDateSet // ignore: cast_nullable_to_non_nullable
              as bool?,
      lastPercentComplete: freezed == lastPercentComplete
          ? _value.lastPercentComplete
          : lastPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      workPackageId: freezed == workPackageId
          ? _value.workPackageId
          : workPackageId // ignore: cast_nullable_to_non_nullable
              as int?,
      dataDate: freezed == dataDate
          ? _value.dataDate
          : dataDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ActivityCopyWith<$Res> implements $ActivityCopyWith<$Res> {
  factory _$$_ActivityCopyWith(
          _$_Activity value, $Res Function(_$_Activity) then) =
      __$$_ActivityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      String? activity,
      String? description,
      DateTime? dateCreated,
      DateTime? startDate,
      DateTime? finishDate,
      double? remainingDuration,
      double? percentComplete,
      DateTime? actualStartDate,
      DateTime? actualFinishDate,
      String? hostName,
      String? projectId,
      double? changedPercentComplete,
      Map<String, String?>? dynamicFieldData,
      Map<String, String?>? dynamicFieldDateTimeData,
      Map<dynamic, dynamic>? dynamicFieldNumberData,
      bool? changed,
      int? wbsObjectId,
      String? percentCompleteColor,
      List<String>? wbsRoute,
      int? objectId,
      String? calendarName,
      double? totalFloat,
      double? plannedDuration,
      bool? sentForPrimaveraSync,
      int? commentCount,
      DateTime? plannedStartDate,
      DateTime? plannedFinishDate,
      DateTime? remainingEarlyStartDate,
      DateTime? remainingEarlyFinishDate,
      double? budgetedLaborUnits,
      double? budgetedNonLaborUnits,
      double? remainingLaborUnits,
      double? remainingNonLaborUnits,
      bool? isDeleted,
      bool? unableToWork,
      bool? preApproved,
      String? wbsName,
      double? freeFloat,
      DateTime? changedActualStartDate,
      DateTime? changedActualFinishDate,
      int? modifiedById,
      String? modifiedByName,
      DateTime? dateModified,
      DateTime? unableToWorkDateTime,
      List<int?>? unableToWorkReasons,
      String? unableToWorkAdditionalInfo,
      List<String>? unableToWorkImageFilePaths,
      bool? backendHasAttachments,
      String? holder,
      double? rejectedPercentComplete,
      DateTime? rejectedStartDate,
      DateTime? rejectedFinishDate,
      List<PredecessorActivityDTO>? predecessors,
      List<SuccessorsActivityDTO>? successors,
      PrimaveraActivityStatus? predecessorsStatus,
      PrimaveraActivityStatus? successorsStatus,
      bool? manualDateSet,
      double? lastPercentComplete,
      int? workPackageId,
      DateTime? dataDate});
}

/// @nodoc
class __$$_ActivityCopyWithImpl<$Res>
    extends _$ActivityCopyWithImpl<$Res, _$_Activity>
    implements _$$_ActivityCopyWith<$Res> {
  __$$_ActivityCopyWithImpl(
      _$_Activity _value, $Res Function(_$_Activity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? activity = freezed,
    Object? description = freezed,
    Object? dateCreated = freezed,
    Object? startDate = freezed,
    Object? finishDate = freezed,
    Object? remainingDuration = freezed,
    Object? percentComplete = freezed,
    Object? actualStartDate = freezed,
    Object? actualFinishDate = freezed,
    Object? hostName = freezed,
    Object? projectId = freezed,
    Object? changedPercentComplete = freezed,
    Object? dynamicFieldData = freezed,
    Object? dynamicFieldDateTimeData = freezed,
    Object? dynamicFieldNumberData = freezed,
    Object? changed = freezed,
    Object? wbsObjectId = freezed,
    Object? percentCompleteColor = freezed,
    Object? wbsRoute = freezed,
    Object? objectId = freezed,
    Object? calendarName = freezed,
    Object? totalFloat = freezed,
    Object? plannedDuration = freezed,
    Object? sentForPrimaveraSync = freezed,
    Object? commentCount = freezed,
    Object? plannedStartDate = freezed,
    Object? plannedFinishDate = freezed,
    Object? remainingEarlyStartDate = freezed,
    Object? remainingEarlyFinishDate = freezed,
    Object? budgetedLaborUnits = freezed,
    Object? budgetedNonLaborUnits = freezed,
    Object? remainingLaborUnits = freezed,
    Object? remainingNonLaborUnits = freezed,
    Object? isDeleted = freezed,
    Object? unableToWork = freezed,
    Object? preApproved = freezed,
    Object? wbsName = freezed,
    Object? freeFloat = freezed,
    Object? changedActualStartDate = freezed,
    Object? changedActualFinishDate = freezed,
    Object? modifiedById = freezed,
    Object? modifiedByName = freezed,
    Object? dateModified = freezed,
    Object? unableToWorkDateTime = freezed,
    Object? unableToWorkReasons = freezed,
    Object? unableToWorkAdditionalInfo = freezed,
    Object? unableToWorkImageFilePaths = freezed,
    Object? backendHasAttachments = freezed,
    Object? holder = freezed,
    Object? rejectedPercentComplete = freezed,
    Object? rejectedStartDate = freezed,
    Object? rejectedFinishDate = freezed,
    Object? predecessors = freezed,
    Object? successors = freezed,
    Object? predecessorsStatus = freezed,
    Object? successorsStatus = freezed,
    Object? manualDateSet = freezed,
    Object? lastPercentComplete = freezed,
    Object? workPackageId = freezed,
    Object? dataDate = freezed,
  }) {
    return _then(_$_Activity(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      activity: freezed == activity
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      dateCreated: freezed == dateCreated
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      startDate: freezed == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      finishDate: freezed == finishDate
          ? _value.finishDate
          : finishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingDuration: freezed == remainingDuration
          ? _value.remainingDuration
          : remainingDuration // ignore: cast_nullable_to_non_nullable
              as double?,
      percentComplete: freezed == percentComplete
          ? _value.percentComplete
          : percentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      actualStartDate: freezed == actualStartDate
          ? _value.actualStartDate
          : actualStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      actualFinishDate: freezed == actualFinishDate
          ? _value.actualFinishDate
          : actualFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      hostName: freezed == hostName
          ? _value.hostName
          : hostName // ignore: cast_nullable_to_non_nullable
              as String?,
      projectId: freezed == projectId
          ? _value.projectId
          : projectId // ignore: cast_nullable_to_non_nullable
              as String?,
      changedPercentComplete: freezed == changedPercentComplete
          ? _value.changedPercentComplete
          : changedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      dynamicFieldData: freezed == dynamicFieldData
          ? _value._dynamicFieldData
          : dynamicFieldData // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      dynamicFieldDateTimeData: freezed == dynamicFieldDateTimeData
          ? _value._dynamicFieldDateTimeData
          : dynamicFieldDateTimeData // ignore: cast_nullable_to_non_nullable
              as Map<String, String?>?,
      dynamicFieldNumberData: freezed == dynamicFieldNumberData
          ? _value._dynamicFieldNumberData
          : dynamicFieldNumberData // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      changed: freezed == changed
          ? _value.changed
          : changed // ignore: cast_nullable_to_non_nullable
              as bool?,
      wbsObjectId: freezed == wbsObjectId
          ? _value.wbsObjectId
          : wbsObjectId // ignore: cast_nullable_to_non_nullable
              as int?,
      percentCompleteColor: freezed == percentCompleteColor
          ? _value.percentCompleteColor
          : percentCompleteColor // ignore: cast_nullable_to_non_nullable
              as String?,
      wbsRoute: freezed == wbsRoute
          ? _value._wbsRoute
          : wbsRoute // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      objectId: freezed == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as int?,
      calendarName: freezed == calendarName
          ? _value.calendarName
          : calendarName // ignore: cast_nullable_to_non_nullable
              as String?,
      totalFloat: freezed == totalFloat
          ? _value.totalFloat
          : totalFloat // ignore: cast_nullable_to_non_nullable
              as double?,
      plannedDuration: freezed == plannedDuration
          ? _value.plannedDuration
          : plannedDuration // ignore: cast_nullable_to_non_nullable
              as double?,
      sentForPrimaveraSync: freezed == sentForPrimaveraSync
          ? _value.sentForPrimaveraSync
          : sentForPrimaveraSync // ignore: cast_nullable_to_non_nullable
              as bool?,
      commentCount: freezed == commentCount
          ? _value.commentCount
          : commentCount // ignore: cast_nullable_to_non_nullable
              as int?,
      plannedStartDate: freezed == plannedStartDate
          ? _value.plannedStartDate
          : plannedStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      plannedFinishDate: freezed == plannedFinishDate
          ? _value.plannedFinishDate
          : plannedFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingEarlyStartDate: freezed == remainingEarlyStartDate
          ? _value.remainingEarlyStartDate
          : remainingEarlyStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      remainingEarlyFinishDate: freezed == remainingEarlyFinishDate
          ? _value.remainingEarlyFinishDate
          : remainingEarlyFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      budgetedLaborUnits: freezed == budgetedLaborUnits
          ? _value.budgetedLaborUnits
          : budgetedLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      budgetedNonLaborUnits: freezed == budgetedNonLaborUnits
          ? _value.budgetedNonLaborUnits
          : budgetedNonLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      remainingLaborUnits: freezed == remainingLaborUnits
          ? _value.remainingLaborUnits
          : remainingLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      remainingNonLaborUnits: freezed == remainingNonLaborUnits
          ? _value.remainingNonLaborUnits
          : remainingNonLaborUnits // ignore: cast_nullable_to_non_nullable
              as double?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool?,
      unableToWork: freezed == unableToWork
          ? _value.unableToWork
          : unableToWork // ignore: cast_nullable_to_non_nullable
              as bool?,
      preApproved: freezed == preApproved
          ? _value.preApproved
          : preApproved // ignore: cast_nullable_to_non_nullable
              as bool?,
      wbsName: freezed == wbsName
          ? _value.wbsName
          : wbsName // ignore: cast_nullable_to_non_nullable
              as String?,
      freeFloat: freezed == freeFloat
          ? _value.freeFloat
          : freeFloat // ignore: cast_nullable_to_non_nullable
              as double?,
      changedActualStartDate: freezed == changedActualStartDate
          ? _value.changedActualStartDate
          : changedActualStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      changedActualFinishDate: freezed == changedActualFinishDate
          ? _value.changedActualFinishDate
          : changedActualFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      modifiedById: freezed == modifiedById
          ? _value.modifiedById
          : modifiedById // ignore: cast_nullable_to_non_nullable
              as int?,
      modifiedByName: freezed == modifiedByName
          ? _value.modifiedByName
          : modifiedByName // ignore: cast_nullable_to_non_nullable
              as String?,
      dateModified: freezed == dateModified
          ? _value.dateModified
          : dateModified // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      unableToWorkDateTime: freezed == unableToWorkDateTime
          ? _value.unableToWorkDateTime
          : unableToWorkDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      unableToWorkReasons: freezed == unableToWorkReasons
          ? _value._unableToWorkReasons
          : unableToWorkReasons // ignore: cast_nullable_to_non_nullable
              as List<int?>?,
      unableToWorkAdditionalInfo: freezed == unableToWorkAdditionalInfo
          ? _value.unableToWorkAdditionalInfo
          : unableToWorkAdditionalInfo // ignore: cast_nullable_to_non_nullable
              as String?,
      unableToWorkImageFilePaths: freezed == unableToWorkImageFilePaths
          ? _value._unableToWorkImageFilePaths
          : unableToWorkImageFilePaths // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      backendHasAttachments: freezed == backendHasAttachments
          ? _value.backendHasAttachments
          : backendHasAttachments // ignore: cast_nullable_to_non_nullable
              as bool?,
      holder: freezed == holder
          ? _value.holder
          : holder // ignore: cast_nullable_to_non_nullable
              as String?,
      rejectedPercentComplete: freezed == rejectedPercentComplete
          ? _value.rejectedPercentComplete
          : rejectedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      rejectedStartDate: freezed == rejectedStartDate
          ? _value.rejectedStartDate
          : rejectedStartDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      rejectedFinishDate: freezed == rejectedFinishDate
          ? _value.rejectedFinishDate
          : rejectedFinishDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      predecessors: freezed == predecessors
          ? _value._predecessors
          : predecessors // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>?,
      successors: freezed == successors
          ? _value._successors
          : successors // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>?,
      predecessorsStatus: freezed == predecessorsStatus
          ? _value.predecessorsStatus
          : predecessorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      successorsStatus: freezed == successorsStatus
          ? _value.successorsStatus
          : successorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      manualDateSet: freezed == manualDateSet
          ? _value.manualDateSet
          : manualDateSet // ignore: cast_nullable_to_non_nullable
              as bool?,
      lastPercentComplete: freezed == lastPercentComplete
          ? _value.lastPercentComplete
          : lastPercentComplete // ignore: cast_nullable_to_non_nullable
              as double?,
      workPackageId: freezed == workPackageId
          ? _value.workPackageId
          : workPackageId // ignore: cast_nullable_to_non_nullable
              as int?,
      dataDate: freezed == dataDate
          ? _value.dataDate
          : dataDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Activity implements _Activity {
  const _$_Activity(
      {required this.id,
      required this.activity,
      required this.description,
      required this.dateCreated,
      required this.startDate,
      required this.finishDate,
      required this.remainingDuration,
      required this.percentComplete,
      required this.actualStartDate,
      required this.actualFinishDate,
      required this.hostName,
      required this.projectId,
      required this.changedPercentComplete,
      required final Map<String, String?>? dynamicFieldData,
      required final Map<String, String?>? dynamicFieldDateTimeData,
      required final Map<dynamic, dynamic>? dynamicFieldNumberData,
      required this.changed,
      required this.wbsObjectId,
      required this.percentCompleteColor,
      required final List<String>? wbsRoute,
      required this.objectId,
      required this.calendarName,
      required this.totalFloat,
      required this.plannedDuration,
      required this.sentForPrimaveraSync,
      required this.commentCount,
      required this.plannedStartDate,
      required this.plannedFinishDate,
      required this.remainingEarlyStartDate,
      required this.remainingEarlyFinishDate,
      required this.budgetedLaborUnits,
      required this.budgetedNonLaborUnits,
      required this.remainingLaborUnits,
      required this.remainingNonLaborUnits,
      required this.isDeleted,
      required this.unableToWork,
      required this.preApproved,
      required this.wbsName,
      required this.freeFloat,
      required this.changedActualStartDate,
      required this.changedActualFinishDate,
      required this.modifiedById,
      required this.modifiedByName,
      required this.dateModified,
      required this.unableToWorkDateTime,
      required final List<int?>? unableToWorkReasons,
      required this.unableToWorkAdditionalInfo,
      required final List<String>? unableToWorkImageFilePaths,
      required this.backendHasAttachments,
      required this.holder,
      required this.rejectedPercentComplete,
      required this.rejectedStartDate,
      required this.rejectedFinishDate,
      required final List<PredecessorActivityDTO>? predecessors,
      required final List<SuccessorsActivityDTO>? successors,
      required this.predecessorsStatus,
      required this.successorsStatus,
      required this.manualDateSet,
      required this.lastPercentComplete,
      required this.workPackageId,
      required this.dataDate})
      : _dynamicFieldData = dynamicFieldData,
        _dynamicFieldDateTimeData = dynamicFieldDateTimeData,
        _dynamicFieldNumberData = dynamicFieldNumberData,
        _wbsRoute = wbsRoute,
        _unableToWorkReasons = unableToWorkReasons,
        _unableToWorkImageFilePaths = unableToWorkImageFilePaths,
        _predecessors = predecessors,
        _successors = successors;

  factory _$_Activity.fromJson(Map<String, dynamic> json) =>
      _$$_ActivityFromJson(json);

  @override
  final int? id;
  @override
  final String? activity;
  @override
  final String? description;
  @override
  final DateTime? dateCreated;
  @override
  final DateTime? startDate;
  @override
  final DateTime? finishDate;
  @override
  final double? remainingDuration;
  @override
  final double? percentComplete;
  @override
  final DateTime? actualStartDate;
  @override
  final DateTime? actualFinishDate;
  @override
  final String? hostName;
  @override
  final String? projectId;
  @override
  final double? changedPercentComplete;
  final Map<String, String?>? _dynamicFieldData;
  @override
  Map<String, String?>? get dynamicFieldData {
    final value = _dynamicFieldData;
    if (value == null) return null;
    if (_dynamicFieldData is EqualUnmodifiableMapView) return _dynamicFieldData;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Map<String, String?>? _dynamicFieldDateTimeData;
  @override
  Map<String, String?>? get dynamicFieldDateTimeData {
    final value = _dynamicFieldDateTimeData;
    if (value == null) return null;
    if (_dynamicFieldDateTimeData is EqualUnmodifiableMapView)
      return _dynamicFieldDateTimeData;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Map<dynamic, dynamic>? _dynamicFieldNumberData;
  @override
  Map<dynamic, dynamic>? get dynamicFieldNumberData {
    final value = _dynamicFieldNumberData;
    if (value == null) return null;
    if (_dynamicFieldNumberData is EqualUnmodifiableMapView)
      return _dynamicFieldNumberData;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final bool? changed;
  @override
  final int? wbsObjectId;
  @override
  final String? percentCompleteColor;
  final List<String>? _wbsRoute;
  @override
  List<String>? get wbsRoute {
    final value = _wbsRoute;
    if (value == null) return null;
    if (_wbsRoute is EqualUnmodifiableListView) return _wbsRoute;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final int? objectId;
  @override
  final String? calendarName;
  @override
  final double? totalFloat;
  @override
  final double? plannedDuration;
  @override
  final bool? sentForPrimaveraSync;
  @override
  final int? commentCount;
  @override
  final DateTime? plannedStartDate;
  @override
  final DateTime? plannedFinishDate;
  @override
  final DateTime? remainingEarlyStartDate;
  @override
  final DateTime? remainingEarlyFinishDate;
  @override
  final double? budgetedLaborUnits;
  @override
  final double? budgetedNonLaborUnits;
  @override
  final double? remainingLaborUnits;
  @override
  final double? remainingNonLaborUnits;
  @override
  final bool? isDeleted;
  @override
  final bool? unableToWork;
  @override
  final bool? preApproved;
  @override
  final String? wbsName;
  @override
  final double? freeFloat;
  @override
  final DateTime? changedActualStartDate;
  @override
  final DateTime? changedActualFinishDate;
  @override
  final int? modifiedById;
  @override
  final String? modifiedByName;
  @override
  final DateTime? dateModified;
  @override
  final DateTime? unableToWorkDateTime;
  final List<int?>? _unableToWorkReasons;
  @override
  List<int?>? get unableToWorkReasons {
    final value = _unableToWorkReasons;
    if (value == null) return null;
    if (_unableToWorkReasons is EqualUnmodifiableListView)
      return _unableToWorkReasons;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final String? unableToWorkAdditionalInfo;
  final List<String>? _unableToWorkImageFilePaths;
  @override
  List<String>? get unableToWorkImageFilePaths {
    final value = _unableToWorkImageFilePaths;
    if (value == null) return null;
    if (_unableToWorkImageFilePaths is EqualUnmodifiableListView)
      return _unableToWorkImageFilePaths;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final bool? backendHasAttachments;
  @override
  final String? holder;
  @override
  final double? rejectedPercentComplete;
  @override
  final DateTime? rejectedStartDate;
  @override
  final DateTime? rejectedFinishDate;
  final List<PredecessorActivityDTO>? _predecessors;
  @override
  List<PredecessorActivityDTO>? get predecessors {
    final value = _predecessors;
    if (value == null) return null;
    if (_predecessors is EqualUnmodifiableListView) return _predecessors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<SuccessorsActivityDTO>? _successors;
  @override
  List<SuccessorsActivityDTO>? get successors {
    final value = _successors;
    if (value == null) return null;
    if (_successors is EqualUnmodifiableListView) return _successors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final PrimaveraActivityStatus? predecessorsStatus;
  @override
  final PrimaveraActivityStatus? successorsStatus;
  @override
  final bool? manualDateSet;
  @override
  final double? lastPercentComplete;
  @override
  final int? workPackageId;
  @override
  final DateTime? dataDate;

  @override
  String toString() {
    return 'Activity(id: $id, activity: $activity, description: $description, dateCreated: $dateCreated, startDate: $startDate, finishDate: $finishDate, remainingDuration: $remainingDuration, percentComplete: $percentComplete, actualStartDate: $actualStartDate, actualFinishDate: $actualFinishDate, hostName: $hostName, projectId: $projectId, changedPercentComplete: $changedPercentComplete, dynamicFieldData: $dynamicFieldData, dynamicFieldDateTimeData: $dynamicFieldDateTimeData, dynamicFieldNumberData: $dynamicFieldNumberData, changed: $changed, wbsObjectId: $wbsObjectId, percentCompleteColor: $percentCompleteColor, wbsRoute: $wbsRoute, objectId: $objectId, calendarName: $calendarName, totalFloat: $totalFloat, plannedDuration: $plannedDuration, sentForPrimaveraSync: $sentForPrimaveraSync, commentCount: $commentCount, plannedStartDate: $plannedStartDate, plannedFinishDate: $plannedFinishDate, remainingEarlyStartDate: $remainingEarlyStartDate, remainingEarlyFinishDate: $remainingEarlyFinishDate, budgetedLaborUnits: $budgetedLaborUnits, budgetedNonLaborUnits: $budgetedNonLaborUnits, remainingLaborUnits: $remainingLaborUnits, remainingNonLaborUnits: $remainingNonLaborUnits, isDeleted: $isDeleted, unableToWork: $unableToWork, preApproved: $preApproved, wbsName: $wbsName, freeFloat: $freeFloat, changedActualStartDate: $changedActualStartDate, changedActualFinishDate: $changedActualFinishDate, modifiedById: $modifiedById, modifiedByName: $modifiedByName, dateModified: $dateModified, unableToWorkDateTime: $unableToWorkDateTime, unableToWorkReasons: $unableToWorkReasons, unableToWorkAdditionalInfo: $unableToWorkAdditionalInfo, unableToWorkImageFilePaths: $unableToWorkImageFilePaths, backendHasAttachments: $backendHasAttachments, holder: $holder, rejectedPercentComplete: $rejectedPercentComplete, rejectedStartDate: $rejectedStartDate, rejectedFinishDate: $rejectedFinishDate, predecessors: $predecessors, successors: $successors, predecessorsStatus: $predecessorsStatus, successorsStatus: $successorsStatus, manualDateSet: $manualDateSet, lastPercentComplete: $lastPercentComplete, workPackageId: $workPackageId, dataDate: $dataDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Activity &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.activity, activity) ||
                other.activity == activity) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.dateCreated, dateCreated) ||
                other.dateCreated == dateCreated) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.finishDate, finishDate) ||
                other.finishDate == finishDate) &&
            (identical(other.remainingDuration, remainingDuration) ||
                other.remainingDuration == remainingDuration) &&
            (identical(other.percentComplete, percentComplete) ||
                other.percentComplete == percentComplete) &&
            (identical(other.actualStartDate, actualStartDate) ||
                other.actualStartDate == actualStartDate) &&
            (identical(other.actualFinishDate, actualFinishDate) ||
                other.actualFinishDate == actualFinishDate) &&
            (identical(other.hostName, hostName) ||
                other.hostName == hostName) &&
            (identical(other.projectId, projectId) ||
                other.projectId == projectId) &&
            (identical(other.changedPercentComplete, changedPercentComplete) ||
                other.changedPercentComplete == changedPercentComplete) &&
            const DeepCollectionEquality()
                .equals(other._dynamicFieldData, _dynamicFieldData) &&
            const DeepCollectionEquality().equals(
                other._dynamicFieldDateTimeData, _dynamicFieldDateTimeData) &&
            const DeepCollectionEquality().equals(
                other._dynamicFieldNumberData, _dynamicFieldNumberData) &&
            (identical(other.changed, changed) || other.changed == changed) &&
            (identical(other.wbsObjectId, wbsObjectId) ||
                other.wbsObjectId == wbsObjectId) &&
            (identical(other.percentCompleteColor, percentCompleteColor) ||
                other.percentCompleteColor == percentCompleteColor) &&
            const DeepCollectionEquality().equals(other._wbsRoute, _wbsRoute) &&
            (identical(other.objectId, objectId) ||
                other.objectId == objectId) &&
            (identical(other.calendarName, calendarName) ||
                other.calendarName == calendarName) &&
            (identical(other.totalFloat, totalFloat) ||
                other.totalFloat == totalFloat) &&
            (identical(other.plannedDuration, plannedDuration) ||
                other.plannedDuration == plannedDuration) &&
            (identical(other.sentForPrimaveraSync, sentForPrimaveraSync) ||
                other.sentForPrimaveraSync == sentForPrimaveraSync) &&
            (identical(other.commentCount, commentCount) ||
                other.commentCount == commentCount) &&
            (identical(other.plannedStartDate, plannedStartDate) ||
                other.plannedStartDate == plannedStartDate) &&
            (identical(other.plannedFinishDate, plannedFinishDate) ||
                other.plannedFinishDate == plannedFinishDate) &&
            (identical(other.remainingEarlyStartDate, remainingEarlyStartDate) ||
                other.remainingEarlyStartDate == remainingEarlyStartDate) &&
            (identical(other.remainingEarlyFinishDate, remainingEarlyFinishDate) ||
                other.remainingEarlyFinishDate == remainingEarlyFinishDate) &&
            (identical(other.budgetedLaborUnits, budgetedLaborUnits) ||
                other.budgetedLaborUnits == budgetedLaborUnits) &&
            (identical(other.budgetedNonLaborUnits, budgetedNonLaborUnits) ||
                other.budgetedNonLaborUnits == budgetedNonLaborUnits) &&
            (identical(other.remainingLaborUnits, remainingLaborUnits) ||
                other.remainingLaborUnits == remainingLaborUnits) &&
            (identical(other.remainingNonLaborUnits, remainingNonLaborUnits) ||
                other.remainingNonLaborUnits == remainingNonLaborUnits) &&
            (identical(other.isDeleted, isDeleted) ||
                other.isDeleted == isDeleted) &&
            (identical(other.unableToWork, unableToWork) ||
                other.unableToWork == unableToWork) &&
            (identical(other.preApproved, preApproved) ||
                other.preApproved == preApproved) &&
            (identical(other.wbsName, wbsName) || other.wbsName == wbsName) &&
            (identical(other.freeFloat, freeFloat) ||
                other.freeFloat == freeFloat) &&
            (identical(other.changedActualStartDate, changedActualStartDate) ||
                other.changedActualStartDate == changedActualStartDate) &&
            (identical(other.changedActualFinishDate, changedActualFinishDate) ||
                other.changedActualFinishDate == changedActualFinishDate) &&
            (identical(other.modifiedById, modifiedById) ||
                other.modifiedById == modifiedById) &&
            (identical(other.modifiedByName, modifiedByName) || other.modifiedByName == modifiedByName) &&
            (identical(other.dateModified, dateModified) || other.dateModified == dateModified) &&
            (identical(other.unableToWorkDateTime, unableToWorkDateTime) || other.unableToWorkDateTime == unableToWorkDateTime) &&
            const DeepCollectionEquality().equals(other._unableToWorkReasons, _unableToWorkReasons) &&
            (identical(other.unableToWorkAdditionalInfo, unableToWorkAdditionalInfo) || other.unableToWorkAdditionalInfo == unableToWorkAdditionalInfo) &&
            const DeepCollectionEquality().equals(other._unableToWorkImageFilePaths, _unableToWorkImageFilePaths) &&
            (identical(other.backendHasAttachments, backendHasAttachments) || other.backendHasAttachments == backendHasAttachments) &&
            (identical(other.holder, holder) || other.holder == holder) &&
            (identical(other.rejectedPercentComplete, rejectedPercentComplete) || other.rejectedPercentComplete == rejectedPercentComplete) &&
            (identical(other.rejectedStartDate, rejectedStartDate) || other.rejectedStartDate == rejectedStartDate) &&
            (identical(other.rejectedFinishDate, rejectedFinishDate) || other.rejectedFinishDate == rejectedFinishDate) &&
            const DeepCollectionEquality().equals(other._predecessors, _predecessors) &&
            const DeepCollectionEquality().equals(other._successors, _successors) &&
            (identical(other.predecessorsStatus, predecessorsStatus) || other.predecessorsStatus == predecessorsStatus) &&
            (identical(other.successorsStatus, successorsStatus) || other.successorsStatus == successorsStatus) &&
            (identical(other.manualDateSet, manualDateSet) || other.manualDateSet == manualDateSet) &&
            (identical(other.lastPercentComplete, lastPercentComplete) || other.lastPercentComplete == lastPercentComplete) &&
            (identical(other.workPackageId, workPackageId) || other.workPackageId == workPackageId) &&
            (identical(other.dataDate, dataDate) || other.dataDate == dataDate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        activity,
        description,
        dateCreated,
        startDate,
        finishDate,
        remainingDuration,
        percentComplete,
        actualStartDate,
        actualFinishDate,
        hostName,
        projectId,
        changedPercentComplete,
        const DeepCollectionEquality().hash(_dynamicFieldData),
        const DeepCollectionEquality().hash(_dynamicFieldDateTimeData),
        const DeepCollectionEquality().hash(_dynamicFieldNumberData),
        changed,
        wbsObjectId,
        percentCompleteColor,
        const DeepCollectionEquality().hash(_wbsRoute),
        objectId,
        calendarName,
        totalFloat,
        plannedDuration,
        sentForPrimaveraSync,
        commentCount,
        plannedStartDate,
        plannedFinishDate,
        remainingEarlyStartDate,
        remainingEarlyFinishDate,
        budgetedLaborUnits,
        budgetedNonLaborUnits,
        remainingLaborUnits,
        remainingNonLaborUnits,
        isDeleted,
        unableToWork,
        preApproved,
        wbsName,
        freeFloat,
        changedActualStartDate,
        changedActualFinishDate,
        modifiedById,
        modifiedByName,
        dateModified,
        unableToWorkDateTime,
        const DeepCollectionEquality().hash(_unableToWorkReasons),
        unableToWorkAdditionalInfo,
        const DeepCollectionEquality().hash(_unableToWorkImageFilePaths),
        backendHasAttachments,
        holder,
        rejectedPercentComplete,
        rejectedStartDate,
        rejectedFinishDate,
        const DeepCollectionEquality().hash(_predecessors),
        const DeepCollectionEquality().hash(_successors),
        predecessorsStatus,
        successorsStatus,
        manualDateSet,
        lastPercentComplete,
        workPackageId,
        dataDate
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ActivityCopyWith<_$_Activity> get copyWith =>
      __$$_ActivityCopyWithImpl<_$_Activity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ActivityToJson(
      this,
    );
  }
}

abstract class _Activity implements Activity {
  const factory _Activity(
      {required final int? id,
      required final String? activity,
      required final String? description,
      required final DateTime? dateCreated,
      required final DateTime? startDate,
      required final DateTime? finishDate,
      required final double? remainingDuration,
      required final double? percentComplete,
      required final DateTime? actualStartDate,
      required final DateTime? actualFinishDate,
      required final String? hostName,
      required final String? projectId,
      required final double? changedPercentComplete,
      required final Map<String, String?>? dynamicFieldData,
      required final Map<String, String?>? dynamicFieldDateTimeData,
      required final Map<dynamic, dynamic>? dynamicFieldNumberData,
      required final bool? changed,
      required final int? wbsObjectId,
      required final String? percentCompleteColor,
      required final List<String>? wbsRoute,
      required final int? objectId,
      required final String? calendarName,
      required final double? totalFloat,
      required final double? plannedDuration,
      required final bool? sentForPrimaveraSync,
      required final int? commentCount,
      required final DateTime? plannedStartDate,
      required final DateTime? plannedFinishDate,
      required final DateTime? remainingEarlyStartDate,
      required final DateTime? remainingEarlyFinishDate,
      required final double? budgetedLaborUnits,
      required final double? budgetedNonLaborUnits,
      required final double? remainingLaborUnits,
      required final double? remainingNonLaborUnits,
      required final bool? isDeleted,
      required final bool? unableToWork,
      required final bool? preApproved,
      required final String? wbsName,
      required final double? freeFloat,
      required final DateTime? changedActualStartDate,
      required final DateTime? changedActualFinishDate,
      required final int? modifiedById,
      required final String? modifiedByName,
      required final DateTime? dateModified,
      required final DateTime? unableToWorkDateTime,
      required final List<int?>? unableToWorkReasons,
      required final String? unableToWorkAdditionalInfo,
      required final List<String>? unableToWorkImageFilePaths,
      required final bool? backendHasAttachments,
      required final String? holder,
      required final double? rejectedPercentComplete,
      required final DateTime? rejectedStartDate,
      required final DateTime? rejectedFinishDate,
      required final List<PredecessorActivityDTO>? predecessors,
      required final List<SuccessorsActivityDTO>? successors,
      required final PrimaveraActivityStatus? predecessorsStatus,
      required final PrimaveraActivityStatus? successorsStatus,
      required final bool? manualDateSet,
      required final double? lastPercentComplete,
      required final int? workPackageId,
      required final DateTime? dataDate}) = _$_Activity;

  factory _Activity.fromJson(Map<String, dynamic> json) = _$_Activity.fromJson;

  @override
  int? get id;
  @override
  String? get activity;
  @override
  String? get description;
  @override
  DateTime? get dateCreated;
  @override
  DateTime? get startDate;
  @override
  DateTime? get finishDate;
  @override
  double? get remainingDuration;
  @override
  double? get percentComplete;
  @override
  DateTime? get actualStartDate;
  @override
  DateTime? get actualFinishDate;
  @override
  String? get hostName;
  @override
  String? get projectId;
  @override
  double? get changedPercentComplete;
  @override
  Map<String, String?>? get dynamicFieldData;
  @override
  Map<String, String?>? get dynamicFieldDateTimeData;
  @override
  Map<dynamic, dynamic>? get dynamicFieldNumberData;
  @override
  bool? get changed;
  @override
  int? get wbsObjectId;
  @override
  String? get percentCompleteColor;
  @override
  List<String>? get wbsRoute;
  @override
  int? get objectId;
  @override
  String? get calendarName;
  @override
  double? get totalFloat;
  @override
  double? get plannedDuration;
  @override
  bool? get sentForPrimaveraSync;
  @override
  int? get commentCount;
  @override
  DateTime? get plannedStartDate;
  @override
  DateTime? get plannedFinishDate;
  @override
  DateTime? get remainingEarlyStartDate;
  @override
  DateTime? get remainingEarlyFinishDate;
  @override
  double? get budgetedLaborUnits;
  @override
  double? get budgetedNonLaborUnits;
  @override
  double? get remainingLaborUnits;
  @override
  double? get remainingNonLaborUnits;
  @override
  bool? get isDeleted;
  @override
  bool? get unableToWork;
  @override
  bool? get preApproved;
  @override
  String? get wbsName;
  @override
  double? get freeFloat;
  @override
  DateTime? get changedActualStartDate;
  @override
  DateTime? get changedActualFinishDate;
  @override
  int? get modifiedById;
  @override
  String? get modifiedByName;
  @override
  DateTime? get dateModified;
  @override
  DateTime? get unableToWorkDateTime;
  @override
  List<int?>? get unableToWorkReasons;
  @override
  String? get unableToWorkAdditionalInfo;
  @override
  List<String>? get unableToWorkImageFilePaths;
  @override
  bool? get backendHasAttachments;
  @override
  String? get holder;
  @override
  double? get rejectedPercentComplete;
  @override
  DateTime? get rejectedStartDate;
  @override
  DateTime? get rejectedFinishDate;
  @override
  List<PredecessorActivityDTO>? get predecessors;
  @override
  List<SuccessorsActivityDTO>? get successors;
  @override
  PrimaveraActivityStatus? get predecessorsStatus;
  @override
  PrimaveraActivityStatus? get successorsStatus;
  @override
  bool? get manualDateSet;
  @override
  double? get lastPercentComplete;
  @override
  int? get workPackageId;
  @override
  DateTime? get dataDate;
  @override
  @JsonKey(ignore: true)
  _$$_ActivityCopyWith<_$_Activity> get copyWith =>
      throw _privateConstructorUsedError;
}
