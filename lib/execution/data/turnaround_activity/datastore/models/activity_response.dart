import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activities.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
part 'activity_response.freezed.dart';
part 'activity_response.g.dart';

@freezed
abstract class ActivityResponse with _$ActivityResponse {
  const factory ActivityResponse({
    required Activities? activities,
    required List<ActivityFieldDTO>? activityFields,
  }) = _ActivityResponse;

  factory ActivityResponse.fromJson(Map<String, dynamic> json) => _$ActivityResponseFromJson(json);
}
