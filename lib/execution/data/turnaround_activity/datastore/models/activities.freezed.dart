// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'activities.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Activities _$ActivitiesFromJson(Map<String, dynamic> json) {
  return _Activities.fromJson(json);
}

/// @nodoc
mixin _$Activities {
  int get totalCount => throw _privateConstructorUsedError;
  int get page => throw _privateConstructorUsedError;
  int get limit => throw _privateConstructorUsedError;
  List<Activity>? get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActivitiesCopyWith<Activities> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivitiesCopyWith<$Res> {
  factory $ActivitiesCopyWith(
          Activities value, $Res Function(Activities) then) =
      _$ActivitiesCopyWithImpl<$Res, Activities>;
  @useResult
  $Res call({int totalCount, int page, int limit, List<Activity>? results});
}

/// @nodoc
class _$ActivitiesCopyWithImpl<$Res, $Val extends Activities>
    implements $ActivitiesCopyWith<$Res> {
  _$ActivitiesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = null,
    Object? page = null,
    Object? limit = null,
    Object? results = freezed,
  }) {
    return _then(_value.copyWith(
      totalCount: null == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      results: freezed == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Activity>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ActivitiesCopyWith<$Res>
    implements $ActivitiesCopyWith<$Res> {
  factory _$$_ActivitiesCopyWith(
          _$_Activities value, $Res Function(_$_Activities) then) =
      __$$_ActivitiesCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int totalCount, int page, int limit, List<Activity>? results});
}

/// @nodoc
class __$$_ActivitiesCopyWithImpl<$Res>
    extends _$ActivitiesCopyWithImpl<$Res, _$_Activities>
    implements _$$_ActivitiesCopyWith<$Res> {
  __$$_ActivitiesCopyWithImpl(
      _$_Activities _value, $Res Function(_$_Activities) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? totalCount = null,
    Object? page = null,
    Object? limit = null,
    Object? results = freezed,
  }) {
    return _then(_$_Activities(
      totalCount: null == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      results: freezed == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Activity>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Activities implements _Activities {
  const _$_Activities(
      {required this.totalCount,
      required this.page,
      required this.limit,
      required final List<Activity>? results})
      : _results = results;

  factory _$_Activities.fromJson(Map<String, dynamic> json) =>
      _$$_ActivitiesFromJson(json);

  @override
  final int totalCount;
  @override
  final int page;
  @override
  final int limit;
  final List<Activity>? _results;
  @override
  List<Activity>? get results {
    final value = _results;
    if (value == null) return null;
    if (_results is EqualUnmodifiableListView) return _results;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'Activities(totalCount: $totalCount, page: $page, limit: $limit, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Activities &&
            (identical(other.totalCount, totalCount) ||
                other.totalCount == totalCount) &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.limit, limit) || other.limit == limit) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, totalCount, page, limit,
      const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ActivitiesCopyWith<_$_Activities> get copyWith =>
      __$$_ActivitiesCopyWithImpl<_$_Activities>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ActivitiesToJson(
      this,
    );
  }
}

abstract class _Activities implements Activities {
  const factory _Activities(
      {required final int totalCount,
      required final int page,
      required final int limit,
      required final List<Activity>? results}) = _$_Activities;

  factory _Activities.fromJson(Map<String, dynamic> json) =
      _$_Activities.fromJson;

  @override
  int get totalCount;
  @override
  int get page;
  @override
  int get limit;
  @override
  List<Activity>? get results;
  @override
  @JsonKey(ignore: true)
  _$$_ActivitiesCopyWith<_$_Activities> get copyWith =>
      throw _privateConstructorUsedError;
}
