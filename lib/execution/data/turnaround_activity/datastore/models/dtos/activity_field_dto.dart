import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'activity_field_dto.g.dart';

@Collection()
@JsonSerializable()
class ActivityFieldDTO {
  @JsonKey(includeFromJson: false, includeToJson: false)
  Id id = Isar.autoIncrement;
  String? guid;
  String? field;
  String? dynamicFieldName;
  String? staticFieldName;
  int? fieldType;
  bool? enabled;

  ActivityFieldDTO({
    this.guid,
    this.field,
    this.staticFieldName,
    this.dynamicFieldName,
    this.fieldType,
    this.enabled,
  });

  factory ActivityFieldDTO.fromJson(Map<String, dynamic> json) => _$ActivityFieldDTOFromJson(json);
}
