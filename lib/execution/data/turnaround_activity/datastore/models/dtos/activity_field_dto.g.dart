// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_field_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetActivityFieldDTOCollection on Isar {
  IsarCollection<ActivityFieldDTO> get activityFieldDTOs => this.collection();
}

const ActivityFieldDTOSchema = CollectionSchema(
  name: r'ActivityFieldDTO',
  id: -7191738112550722654,
  properties: {
    r'dynamicFieldName': PropertySchema(
      id: 0,
      name: r'dynamicFieldName',
      type: IsarType.string,
    ),
    r'enabled': PropertySchema(
      id: 1,
      name: r'enabled',
      type: IsarType.bool,
    ),
    r'field': PropertySchema(
      id: 2,
      name: r'field',
      type: IsarType.string,
    ),
    r'fieldType': PropertySchema(
      id: 3,
      name: r'fieldType',
      type: IsarType.long,
    ),
    r'guid': PropertySchema(
      id: 4,
      name: r'guid',
      type: IsarType.string,
    ),
    r'staticFieldName': PropertySchema(
      id: 5,
      name: r'staticFieldName',
      type: IsarType.string,
    )
  },
  estimateSize: _activityFieldDTOEstimateSize,
  serialize: _activityFieldDTOSerialize,
  deserialize: _activityFieldDTODeserialize,
  deserializeProp: _activityFieldDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _activityFieldDTOGetId,
  getLinks: _activityFieldDTOGetLinks,
  attach: _activityFieldDTOAttach,
  version: '3.1.0+1',
);

int _activityFieldDTOEstimateSize(
  ActivityFieldDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.dynamicFieldName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.field;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.guid;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.staticFieldName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _activityFieldDTOSerialize(
  ActivityFieldDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.dynamicFieldName);
  writer.writeBool(offsets[1], object.enabled);
  writer.writeString(offsets[2], object.field);
  writer.writeLong(offsets[3], object.fieldType);
  writer.writeString(offsets[4], object.guid);
  writer.writeString(offsets[5], object.staticFieldName);
}

ActivityFieldDTO _activityFieldDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ActivityFieldDTO(
    dynamicFieldName: reader.readStringOrNull(offsets[0]),
    enabled: reader.readBoolOrNull(offsets[1]),
    field: reader.readStringOrNull(offsets[2]),
    fieldType: reader.readLongOrNull(offsets[3]),
    guid: reader.readStringOrNull(offsets[4]),
    staticFieldName: reader.readStringOrNull(offsets[5]),
  );
  object.id = id;
  return object;
}

P _activityFieldDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readBoolOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _activityFieldDTOGetId(ActivityFieldDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _activityFieldDTOGetLinks(ActivityFieldDTO object) {
  return [];
}

void _activityFieldDTOAttach(
    IsarCollection<dynamic> col, Id id, ActivityFieldDTO object) {
  object.id = id;
}

extension ActivityFieldDTOQueryWhereSort
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QWhere> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension ActivityFieldDTOQueryWhere
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QWhereClause> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ActivityFieldDTOQueryFilter
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QFilterCondition> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dynamicFieldName',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dynamicFieldName',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dynamicFieldName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'dynamicFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'dynamicFieldName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      dynamicFieldNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'dynamicFieldName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      enabledIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'enabled',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      enabledIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'enabled',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      enabledEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'enabled',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'field',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'field',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'field',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'field',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'field',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'field',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'field',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'fieldType',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'fieldType',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'fieldType',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'fieldType',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'fieldType',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      fieldTypeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'fieldType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'guid',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'guid',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'guid',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'guid',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      guidIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'guid',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'staticFieldName',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'staticFieldName',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'staticFieldName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'staticFieldName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'staticFieldName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'staticFieldName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterFilterCondition>
      staticFieldNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'staticFieldName',
        value: '',
      ));
    });
  }
}

extension ActivityFieldDTOQueryObject
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QFilterCondition> {}

extension ActivityFieldDTOQueryLinks
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QFilterCondition> {}

extension ActivityFieldDTOQuerySortBy
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QSortBy> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByDynamicFieldName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldName', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByDynamicFieldNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldName', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'enabled', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByEnabledDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'enabled', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy> sortByField() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByFieldDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByFieldType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldType', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByFieldTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldType', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy> sortByGuid() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByGuidDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByStaticFieldName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'staticFieldName', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      sortByStaticFieldNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'staticFieldName', Sort.desc);
    });
  }
}

extension ActivityFieldDTOQuerySortThenBy
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QSortThenBy> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByDynamicFieldName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldName', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByDynamicFieldNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldName', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'enabled', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByEnabledDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'enabled', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy> thenByField() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByFieldDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'field', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByFieldType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldType', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByFieldTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'fieldType', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy> thenByGuid() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByGuidDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'guid', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByStaticFieldName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'staticFieldName', Sort.asc);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QAfterSortBy>
      thenByStaticFieldNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'staticFieldName', Sort.desc);
    });
  }
}

extension ActivityFieldDTOQueryWhereDistinct
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct> {
  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct>
      distinctByDynamicFieldName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dynamicFieldName',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct>
      distinctByEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'enabled');
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct> distinctByField(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'field', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct>
      distinctByFieldType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'fieldType');
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct> distinctByGuid(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'guid', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QDistinct>
      distinctByStaticFieldName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'staticFieldName',
          caseSensitive: caseSensitive);
    });
  }
}

extension ActivityFieldDTOQueryProperty
    on QueryBuilder<ActivityFieldDTO, ActivityFieldDTO, QQueryProperty> {
  QueryBuilder<ActivityFieldDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<ActivityFieldDTO, String?, QQueryOperations>
      dynamicFieldNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dynamicFieldName');
    });
  }

  QueryBuilder<ActivityFieldDTO, bool?, QQueryOperations> enabledProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'enabled');
    });
  }

  QueryBuilder<ActivityFieldDTO, String?, QQueryOperations> fieldProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'field');
    });
  }

  QueryBuilder<ActivityFieldDTO, int?, QQueryOperations> fieldTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'fieldType');
    });
  }

  QueryBuilder<ActivityFieldDTO, String?, QQueryOperations> guidProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'guid');
    });
  }

  QueryBuilder<ActivityFieldDTO, String?, QQueryOperations>
      staticFieldNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'staticFieldName');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivityFieldDTO _$ActivityFieldDTOFromJson(Map<String, dynamic> json) =>
    ActivityFieldDTO(
      guid: json['guid'] as String?,
      field: json['field'] as String?,
      staticFieldName: json['staticFieldName'] as String?,
      dynamicFieldName: json['dynamicFieldName'] as String?,
      fieldType: json['fieldType'] as int?,
      enabled: json['enabled'] as bool?,
    );

Map<String, dynamic> _$ActivityFieldDTOToJson(ActivityFieldDTO instance) =>
    <String, dynamic>{
      'guid': instance.guid,
      'field': instance.field,
      'dynamicFieldName': instance.dynamicFieldName,
      'staticFieldName': instance.staticFieldName,
      'fieldType': instance.fieldType,
      'enabled': instance.enabled,
    };
