import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/activity_status_code_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/relationship_type_code.dart';
part 'predecessor_activity_dto.g.dart';

@embedded
@JsonSerializable()
class PredecessorActivityDTO {
  String? parentActivity;
  String? projectId;
  String? activity;
  String? description;
  @Enumerated(EnumType.ordinal32)
  RelationshipTypeEnum? relationshipType;
  @Enumerated(EnumType.ordinal32)
  ActivityStatusEnum? activityStatus;

  /// Predecessor Activity DTO
  PredecessorActivityDTO({
    this.parentActivity,
    this.projectId,
    this.description,
    this.activity,
    this.relationshipType,
    this.activityStatus,
  });

  factory PredecessorActivityDTO.fromJson(Map<String, dynamic> json) => _$PredecessorActivityDTOFromJson(json);
}
