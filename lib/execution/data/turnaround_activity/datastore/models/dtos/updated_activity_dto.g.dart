// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'updated_activity_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetUpdatedActivityDTOCollection on Isar {
  IsarCollection<UpdatedActivityDTO> get updatedActivityDTOs =>
      this.collection();
}

const UpdatedActivityDTOSchema = CollectionSchema(
  name: r'UpdatedActivityDTO',
  id: 8140881946324011249,
  properties: {
    r'commentCount': PropertySchema(
      id: 0,
      name: r'commentCount',
      type: IsarType.long,
    ),
    r'dateModified': PropertySchema(
      id: 1,
      name: r'dateModified',
      type: IsarType.dateTime,
    ),
    r'manualDateSet': PropertySchema(
      id: 2,
      name: r'manualDateSet',
      type: IsarType.bool,
    ),
    r'manualStartDate': PropertySchema(
      id: 3,
      name: r'manualStartDate',
      type: IsarType.dateTime,
    ),
    r'objectId': PropertySchema(
      id: 4,
      name: r'objectId',
      type: IsarType.long,
    ),
    r'percentComplete': PropertySchema(
      id: 5,
      name: r'percentComplete',
      type: IsarType.double,
    ),
    r'unableToWork': PropertySchema(
      id: 6,
      name: r'unableToWork',
      type: IsarType.bool,
    ),
    r'unableToWorkAdditionalInfo': PropertySchema(
      id: 7,
      name: r'unableToWorkAdditionalInfo',
      type: IsarType.string,
    ),
    r'unableToWorkDateTime': PropertySchema(
      id: 8,
      name: r'unableToWorkDateTime',
      type: IsarType.dateTime,
    ),
    r'unableToWorkReasons': PropertySchema(
      id: 9,
      name: r'unableToWorkReasons',
      type: IsarType.longList,
    )
  },
  estimateSize: _updatedActivityDTOEstimateSize,
  serialize: _updatedActivityDTOSerialize,
  deserialize: _updatedActivityDTODeserialize,
  deserializeProp: _updatedActivityDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _updatedActivityDTOGetId,
  getLinks: _updatedActivityDTOGetLinks,
  attach: _updatedActivityDTOAttach,
  version: '3.1.0+1',
);

int _updatedActivityDTOEstimateSize(
  UpdatedActivityDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.unableToWorkAdditionalInfo;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.unableToWorkReasons;
    if (value != null) {
      bytesCount += 3 + value.length * 8;
    }
  }
  return bytesCount;
}

void _updatedActivityDTOSerialize(
  UpdatedActivityDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.commentCount);
  writer.writeDateTime(offsets[1], object.dateModified);
  writer.writeBool(offsets[2], object.manualDateSet);
  writer.writeDateTime(offsets[3], object.manualStartDate);
  writer.writeLong(offsets[4], object.objectId);
  writer.writeDouble(offsets[5], object.percentComplete);
  writer.writeBool(offsets[6], object.unableToWork);
  writer.writeString(offsets[7], object.unableToWorkAdditionalInfo);
  writer.writeDateTime(offsets[8], object.unableToWorkDateTime);
  writer.writeLongList(offsets[9], object.unableToWorkReasons);
}

UpdatedActivityDTO _updatedActivityDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = UpdatedActivityDTO(
    commentCount: reader.readLongOrNull(offsets[0]),
    dateModified: reader.readDateTimeOrNull(offsets[1]),
    id: id,
    manualDateSet: reader.readBoolOrNull(offsets[2]),
    manualStartDate: reader.readDateTimeOrNull(offsets[3]),
    objectId: reader.readLongOrNull(offsets[4]),
    percentComplete: reader.readDoubleOrNull(offsets[5]),
    unableToWork: reader.readBoolOrNull(offsets[6]),
    unableToWorkAdditionalInfo: reader.readStringOrNull(offsets[7]),
    unableToWorkDateTime: reader.readDateTimeOrNull(offsets[8]),
    unableToWorkReasons: reader.readLongList(offsets[9]),
  );
  return object;
}

P _updatedActivityDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 2:
      return (reader.readBoolOrNull(offset)) as P;
    case 3:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readDoubleOrNull(offset)) as P;
    case 6:
      return (reader.readBoolOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 9:
      return (reader.readLongList(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _updatedActivityDTOGetId(UpdatedActivityDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _updatedActivityDTOGetLinks(
    UpdatedActivityDTO object) {
  return [];
}

void _updatedActivityDTOAttach(
    IsarCollection<dynamic> col, Id id, UpdatedActivityDTO object) {
  object.id = id;
}

extension UpdatedActivityDTOQueryWhereSort
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QWhere> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension UpdatedActivityDTOQueryWhere
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QWhereClause> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension UpdatedActivityDTOQueryFilter
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QFilterCondition> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'commentCount',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'commentCount',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      commentCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'commentCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      dateModifiedBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateModified',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualDateSetIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'manualDateSet',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualDateSetIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'manualDateSet',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualDateSetEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'manualDateSet',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'manualStartDate',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'manualStartDate',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'manualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'manualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'manualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      manualStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'manualStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      objectIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'percentComplete',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'percentComplete',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      percentCompleteBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'percentComplete',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWork',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWork',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWork',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkAdditionalInfo',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkAdditionalInfo',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkAdditionalInfo',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'unableToWorkAdditionalInfo',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkAdditionalInfo',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'unableToWorkAdditionalInfo',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkDateTime',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkDateTime',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkDateTime',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkReasons',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }
}

extension UpdatedActivityDTOQueryObject
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QFilterCondition> {}

extension UpdatedActivityDTOQueryLinks
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QFilterCondition> {}

extension UpdatedActivityDTOQuerySortBy
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QSortBy> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByCommentCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByManualDateSetDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByManualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualStartDate', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByManualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualStartDate', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWorkDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWorkAdditionalInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWorkAdditionalInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      sortByUnableToWorkDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.desc);
    });
  }
}

extension UpdatedActivityDTOQuerySortThenBy
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QSortThenBy> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByCommentCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByManualDateSetDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByManualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualStartDate', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByManualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualStartDate', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWorkDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWorkAdditionalInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWorkAdditionalInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.desc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.asc);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QAfterSortBy>
      thenByUnableToWorkDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.desc);
    });
  }
}

extension UpdatedActivityDTOQueryWhereDistinct
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct> {
  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'commentCount');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateModified');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'manualDateSet');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByManualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'manualStartDate');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectId');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'percentComplete');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWork');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByUnableToWorkAdditionalInfo({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkAdditionalInfo',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkDateTime');
    });
  }

  QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QDistinct>
      distinctByUnableToWorkReasons() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkReasons');
    });
  }
}

extension UpdatedActivityDTOQueryProperty
    on QueryBuilder<UpdatedActivityDTO, UpdatedActivityDTO, QQueryProperty> {
  QueryBuilder<UpdatedActivityDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<UpdatedActivityDTO, int?, QQueryOperations>
      commentCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'commentCount');
    });
  }

  QueryBuilder<UpdatedActivityDTO, DateTime?, QQueryOperations>
      dateModifiedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateModified');
    });
  }

  QueryBuilder<UpdatedActivityDTO, bool?, QQueryOperations>
      manualDateSetProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'manualDateSet');
    });
  }

  QueryBuilder<UpdatedActivityDTO, DateTime?, QQueryOperations>
      manualStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'manualStartDate');
    });
  }

  QueryBuilder<UpdatedActivityDTO, int?, QQueryOperations> objectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectId');
    });
  }

  QueryBuilder<UpdatedActivityDTO, double?, QQueryOperations>
      percentCompleteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'percentComplete');
    });
  }

  QueryBuilder<UpdatedActivityDTO, bool?, QQueryOperations>
      unableToWorkProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWork');
    });
  }

  QueryBuilder<UpdatedActivityDTO, String?, QQueryOperations>
      unableToWorkAdditionalInfoProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkAdditionalInfo');
    });
  }

  QueryBuilder<UpdatedActivityDTO, DateTime?, QQueryOperations>
      unableToWorkDateTimeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkDateTime');
    });
  }

  QueryBuilder<UpdatedActivityDTO, List<int>?, QQueryOperations>
      unableToWorkReasonsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkReasons');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdatedActivityDTO _$UpdatedActivityDTOFromJson(Map<String, dynamic> json) =>
    UpdatedActivityDTO(
      id: json['id'] as int?,
      percentComplete: (json['percentComplete'] as num?)?.toDouble(),
      unableToWork: json['unableToWork'] as bool?,
      unableToWorkDateTime: json['unableToWorkDateTime'] == null
          ? null
          : DateTime.parse(json['unableToWorkDateTime'] as String),
      unableToWorkReasons: (json['unableToWorkReasons'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
      unableToWorkAdditionalInfo: json['unableToWorkAdditionalInfo'] as String?,
      objectId: json['objectId'] as int?,
      dateModified: json['dateModified'] == null
          ? null
          : DateTime.parse(json['dateModified'] as String),
      manualStartDate: json['manualStartDate'] == null
          ? null
          : DateTime.parse(json['manualStartDate'] as String),
      manualDateSet: json['manualDateSet'] as bool?,
      commentCount: json['commentCount'] as int?,
    );

Map<String, dynamic> _$UpdatedActivityDTOToJson(UpdatedActivityDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'percentComplete': instance.percentComplete,
      'unableToWork': instance.unableToWork,
      'unableToWorkDateTime': instance.unableToWorkDateTime?.toIso8601String(),
      'unableToWorkReasons': instance.unableToWorkReasons,
      'unableToWorkAdditionalInfo': instance.unableToWorkAdditionalInfo,
      'objectId': instance.objectId,
      'dateModified': instance.dateModified?.toIso8601String(),
      'manualStartDate': instance.manualStartDate?.toIso8601String(),
      'manualDateSet': instance.manualDateSet,
      'commentCount': instance.commentCount,
    };
