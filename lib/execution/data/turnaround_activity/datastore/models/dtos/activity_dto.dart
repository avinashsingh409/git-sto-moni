import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/predecessor_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/successors_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/primavera_activity_status_enum.dart';

part 'activity_dto.g.dart';

@Collection()
@JsonSerializable()
class ActivityDTO {
  Id? id;
  String? activity;
  String? description;
  DateTime? dateCreated;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? startDate;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? finishDate;
  double? remainingDuration;
  double? percentComplete;
  String? hostName;
  String? projectId;
  String? dynamicFieldData;
  String? dynamicFieldDateTimeData;
  String? dynamicFieldNumberData;
  bool? changed;
  int? wbsObjectId;
  String? percentCompleteColor;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? actualStartDate;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? actualFinishDate;
  List<String>? wbsRoute;
  int? objectId;
  String? calendarName;
  double? totalFloat;
  double? plannedDuration;
  bool? sentForPrimaveraSync;
  int? commentCount;
  DateTime? plannedStartDate;
  DateTime? plannedFinishDate;
  DateTime? remainingEarlyStartDate;
  DateTime? remainingEarlyFinishDate;
  double? budgetedLaborUnits;
  double? budgetedNonLaborUnits;
  double? remainingLaborUnits;
  double? remainingNonLaborUnits;
  bool? isDeleted;
  bool? unableToWork;
  bool? preApproved;
  String? wbsName;
  double? freeFloat;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? changedActualStartDate;
  @JsonKey(includeFromJson: false, includeToJson: false)
  DateTime? changedActualFinishDate;
  int? modifiedById;
  String? modifiedByName;
  DateTime? dateModified;
  DateTime? unableToWorkDateTime;
  List<int?>? unableToWorkReasons;
  String? unableToWorkAdditionalInfo;
  List<String>? unableToWorkImageFilePaths;
  bool? backendHasAttachments;
  String? holder;
  double? rejectedPercentComplete;
  DateTime? rejectedStartDate;
  DateTime? rejectedFinishDate;
  List<PredecessorActivityDTO>? predecessors;
  List<SuccessorsActivityDTO>? successors;
  @Enumerated(EnumType.ordinal32)
  PrimaveraActivityStatus? predecessorsStatus;
  @Enumerated(EnumType.ordinal32)
  PrimaveraActivityStatus? successorsStatus;
  bool? manualDateSet;
  double? lastPercentComplete;
  String? displayDate;
  String? displayFinishDate;
  int? workPackageId;
  DateTime? dataDate;

  ActivityDTO({
    this.id,
    this.activity,
    this.description,
    this.startDate,
    this.finishDate,
    this.remainingDuration,
    this.percentComplete,
    this.hostName,
    this.projectId,
    this.changed,
    this.wbsObjectId,
    this.percentCompleteColor,
    this.wbsRoute,
    this.objectId,
    this.calendarName,
    this.totalFloat,
    this.plannedDuration,
    this.sentForPrimaveraSync,
    this.commentCount,
    this.actualStartDate,
    this.actualFinishDate,
    this.plannedStartDate,
    this.plannedFinishDate,
    this.remainingEarlyStartDate,
    this.remainingEarlyFinishDate,
    this.budgetedLaborUnits,
    this.budgetedNonLaborUnits,
    this.remainingLaborUnits,
    this.remainingNonLaborUnits,
    this.isDeleted,
    this.unableToWork,
    this.preApproved,
    this.wbsName,
    this.freeFloat,
    this.changedActualStartDate,
    this.changedActualFinishDate,
    this.modifiedById,
    this.modifiedByName,
    this.dateModified,
    this.unableToWorkDateTime,
    this.unableToWorkReasons,
    this.unableToWorkAdditionalInfo,
    this.unableToWorkImageFilePaths,
    this.backendHasAttachments,
    this.holder,
    this.dateCreated,
    this.rejectedPercentComplete,
    this.rejectedStartDate,
    this.rejectedFinishDate,
    this.predecessors,
    this.successors,
    this.predecessorsStatus,
    this.successorsStatus,
    this.dynamicFieldData,
    this.dynamicFieldDateTimeData,
    this.dynamicFieldNumberData,
    this.displayDate,
    this.displayFinishDate,
    this.manualDateSet,
    this.lastPercentComplete,
    this.workPackageId,
    this.dataDate,
  });

  factory ActivityDTO.fromJson(Map<String, dynamic> json) => _$ActivityDTOFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityDTOToJson(this);

  factory ActivityDTO.fromModel({required Activity activityModel}) {
    final activityDTO = ActivityDTO(
      id: activityModel.id,
      activity: activityModel.activity,
      backendHasAttachments: activityModel.backendHasAttachments,
      budgetedLaborUnits: double.parse((activityModel.budgetedLaborUnits ?? 0).toStringAsFixed(2)),
      budgetedNonLaborUnits: double.parse((activityModel.budgetedNonLaborUnits ?? 0).toStringAsFixed(2)),
      calendarName: activityModel.calendarName,
      changed: activityModel.changed,
      changedActualFinishDate: activityModel.changedActualFinishDate,
      changedActualStartDate: activityModel.changedActualStartDate,
      commentCount: activityModel.commentCount,
      dateCreated: activityModel.dateCreated,
      dateModified: activityModel.dateModified,
      description: activityModel.description,
      finishDate: activityModel.finishDate,
      freeFloat: double.parse((activityModel.freeFloat ?? 0).toStringAsFixed(2)),
      holder: activityModel.holder,
      hostName: activityModel.hostName,
      isDeleted: activityModel.isDeleted,
      modifiedById: activityModel.modifiedById,
      modifiedByName: activityModel.modifiedByName,
      objectId: activityModel.objectId,
      percentComplete: double.parse((activityModel.percentComplete ?? 0).toStringAsFixed(2)),
      percentCompleteColor: activityModel.percentCompleteColor,
      plannedDuration: double.parse((activityModel.plannedDuration ?? 0).toStringAsFixed(2)),
      plannedFinishDate: activityModel.plannedFinishDate,
      plannedStartDate: activityModel.plannedStartDate,
      preApproved: activityModel.preApproved,
      predecessors: activityModel.predecessors,
      predecessorsStatus: activityModel.predecessorsStatus,
      projectId: activityModel.projectId,
      rejectedFinishDate: activityModel.rejectedFinishDate,
      rejectedPercentComplete: activityModel.rejectedPercentComplete,
      rejectedStartDate: activityModel.rejectedStartDate,
      remainingDuration: double.parse((activityModel.remainingDuration ?? 0).toStringAsFixed(2)),
      remainingEarlyFinishDate: activityModel.remainingEarlyFinishDate,
      remainingEarlyStartDate: activityModel.remainingEarlyStartDate,
      remainingLaborUnits: double.parse((activityModel.remainingLaborUnits ?? 0).toStringAsFixed(2)),
      remainingNonLaborUnits: double.parse((activityModel.remainingNonLaborUnits ?? 0).toStringAsFixed(2)),
      sentForPrimaveraSync: activityModel.sentForPrimaveraSync,
      startDate: activityModel.startDate,
      successors: activityModel.successors,
      successorsStatus: activityModel.successorsStatus,
      totalFloat: activityModel.totalFloat != null ? double.parse((activityModel.totalFloat!).toStringAsFixed(2)) : null,
      unableToWork: activityModel.unableToWork,
      unableToWorkAdditionalInfo: activityModel.unableToWorkAdditionalInfo,
      unableToWorkDateTime: activityModel.unableToWorkDateTime,
      unableToWorkImageFilePaths: activityModel.unableToWorkImageFilePaths,
      unableToWorkReasons: activityModel.unableToWorkReasons,
      wbsName: activityModel.wbsName,
      wbsObjectId: activityModel.wbsObjectId,
      wbsRoute: activityModel.wbsRoute,
      dynamicFieldData: jsonEncode(activityModel.dynamicFieldData),
      dynamicFieldDateTimeData: jsonEncode(activityModel.dynamicFieldDateTimeData),
      dynamicFieldNumberData: jsonEncode(activityModel.dynamicFieldNumberData),
      manualDateSet: activityModel.manualDateSet,
      lastPercentComplete: double.parse((activityModel.lastPercentComplete ?? 0).toStringAsFixed(2)),
      actualFinishDate: activityModel.actualFinishDate,
      // serializing display dates in mobile
      displayDate: (activityModel.changedActualStartDate != null)
          ? "${activityModel.changedActualStartDate.toString()} A"
          : (activityModel.actualStartDate != null)
              ? "${activityModel.actualStartDate.toString()} A"
              : activityModel.startDate.toString(),
      displayFinishDate: (activityModel.changedActualFinishDate != null)
          ? "${activityModel.changedActualFinishDate.toString()} A"
          : (activityModel.actualFinishDate != null)
              ? "${activityModel.actualFinishDate.toString()} A"
              : activityModel.finishDate.toString(),
      workPackageId: activityModel.workPackageId,
      dataDate: activityModel.dataDate,
    );
    return activityDTO;
  }

  ActivityDTO copyWith({
    required UpdatedActivityDTO updatedActivityDTO,
  }) {
    var now = DateTime.now();
    return ActivityDTO(
      id: id,
      activity: activity,
      description: description,
      startDate: startDate,
      finishDate: finishDate,
      remainingDuration: remainingDuration,
      percentComplete: updatedActivityDTO.percentComplete ?? percentComplete,
      hostName: hostName,
      projectId: projectId,
      changed: changed,
      wbsObjectId: wbsObjectId,
      percentCompleteColor: percentCompleteColor,
      wbsRoute: wbsRoute,
      objectId: updatedActivityDTO.objectId ?? objectId,
      calendarName: calendarName,
      totalFloat: totalFloat,
      plannedDuration: plannedDuration,
      sentForPrimaveraSync: sentForPrimaveraSync,
      commentCount: updatedActivityDTO.commentCount ?? commentCount,
      plannedStartDate: plannedStartDate,
      plannedFinishDate: plannedFinishDate,
      remainingEarlyStartDate: remainingEarlyStartDate,
      remainingEarlyFinishDate: remainingEarlyFinishDate,
      budgetedLaborUnits: budgetedLaborUnits,
      budgetedNonLaborUnits: budgetedNonLaborUnits,
      remainingLaborUnits: remainingLaborUnits,
      remainingNonLaborUnits: remainingNonLaborUnits,
      isDeleted: isDeleted,
      unableToWork: updatedActivityDTO.unableToWork ?? unableToWork,
      preApproved: preApproved,
      wbsName: wbsName,
      freeFloat: freeFloat,
      changedActualStartDate: updatedActivityDTO.manualStartDate ?? changedActualStartDate,
      changedActualFinishDate: updatedActivityDTO.percentComplete == 1 ? now : changedActualFinishDate,
      modifiedById: modifiedById,
      modifiedByName: modifiedByName,
      dateModified: updatedActivityDTO.dateModified ?? dateModified,
      unableToWorkDateTime: updatedActivityDTO.unableToWorkDateTime ?? unableToWorkDateTime,
      unableToWorkReasons: updatedActivityDTO.unableToWorkReasons,
      unableToWorkAdditionalInfo: updatedActivityDTO.unableToWorkAdditionalInfo ?? unableToWorkAdditionalInfo,
      unableToWorkImageFilePaths: unableToWorkImageFilePaths,
      backendHasAttachments: backendHasAttachments,
      holder: holder,
      dateCreated: dateCreated,
      rejectedPercentComplete: rejectedPercentComplete,
      rejectedStartDate: rejectedStartDate,
      rejectedFinishDate: rejectedFinishDate,
      predecessors: predecessors,
      successors: successors,
      predecessorsStatus: predecessorsStatus,
      successorsStatus: successorsStatus,
      displayDate:
          updatedActivityDTO.manualStartDate != null ? "${updatedActivityDTO.manualStartDate.toString()} A" : displayDate,
      displayFinishDate: updatedActivityDTO.percentComplete == 1 ? "${now.toString()} A" : displayFinishDate,
      manualDateSet: updatedActivityDTO.manualDateSet,
      lastPercentComplete: lastPercentComplete,
      actualFinishDate: actualFinishDate,
      workPackageId: workPackageId,
      dataDate: dataDate,
    );
  }

  dynamic getProperty(String propertyName) {
    // check for static field
    var mapRep = toJson();
    if (mapRep.containsKey(propertyName)) {
      return mapRep[propertyName];
    }
    
    // check for dynamic field
    // string
    final Map<String, dynamic>? dynamicDataJson = dynamicFieldData != null ? jsonDecode(dynamicFieldData ?? "") : {};
    if (dynamicDataJson != null && dynamicDataJson.containsKey(propertyName)) {
      return dynamicDataJson[propertyName] ?? "N/A";
    }

    // date
    final Map<String, dynamic>? dynamicDateTimeDataJson = dynamicFieldDateTimeData != null ? jsonDecode(dynamicFieldDateTimeData ?? "") : {};
    if (dynamicDateTimeDataJson != null && dynamicDateTimeDataJson.containsKey(propertyName)) {
      return dynamicDateTimeDataJson[propertyName] ?? "N/A";
    }

    // number
    final Map<String, dynamic>? dynamicNumberDataJson = dynamicFieldNumberData != null ? jsonDecode(dynamicFieldNumberData ?? "") : {};
    if (dynamicNumberDataJson != null && dynamicNumberDataJson.containsKey(propertyName)) {
      return dynamicNumberDataJson[propertyName] ?? "N/A";
    }

    // default to null
    return null;
  }
}
