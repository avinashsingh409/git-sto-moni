// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'successors_activity_dto.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const SuccessorsActivityDTOSchema = Schema(
  name: r'SuccessorsActivityDTO',
  id: 3972433234270285796,
  properties: {
    r'activity': PropertySchema(
      id: 0,
      name: r'activity',
      type: IsarType.string,
    ),
    r'activityStatus': PropertySchema(
      id: 1,
      name: r'activityStatus',
      type: IsarType.int,
      enumMap: _SuccessorsActivityDTOactivityStatusEnumValueMap,
    ),
    r'description': PropertySchema(
      id: 2,
      name: r'description',
      type: IsarType.string,
    ),
    r'parentActivity': PropertySchema(
      id: 3,
      name: r'parentActivity',
      type: IsarType.string,
    ),
    r'projectId': PropertySchema(
      id: 4,
      name: r'projectId',
      type: IsarType.string,
    ),
    r'relationshipType': PropertySchema(
      id: 5,
      name: r'relationshipType',
      type: IsarType.int,
      enumMap: _SuccessorsActivityDTOrelationshipTypeEnumValueMap,
    )
  },
  estimateSize: _successorsActivityDTOEstimateSize,
  serialize: _successorsActivityDTOSerialize,
  deserialize: _successorsActivityDTODeserialize,
  deserializeProp: _successorsActivityDTODeserializeProp,
);

int _successorsActivityDTOEstimateSize(
  SuccessorsActivityDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.activity;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.parentActivity;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.projectId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _successorsActivityDTOSerialize(
  SuccessorsActivityDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.activity);
  writer.writeInt(offsets[1], object.activityStatus?.index);
  writer.writeString(offsets[2], object.description);
  writer.writeString(offsets[3], object.parentActivity);
  writer.writeString(offsets[4], object.projectId);
  writer.writeInt(offsets[5], object.relationshipType?.index);
}

SuccessorsActivityDTO _successorsActivityDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = SuccessorsActivityDTO(
    activity: reader.readStringOrNull(offsets[0]),
    activityStatus: _SuccessorsActivityDTOactivityStatusValueEnumMap[
        reader.readIntOrNull(offsets[1])],
    description: reader.readStringOrNull(offsets[2]),
    parentActivity: reader.readStringOrNull(offsets[3]),
    projectId: reader.readStringOrNull(offsets[4]),
    relationshipType: _SuccessorsActivityDTOrelationshipTypeValueEnumMap[
        reader.readIntOrNull(offsets[5])],
  );
  return object;
}

P _successorsActivityDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (_SuccessorsActivityDTOactivityStatusValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (_SuccessorsActivityDTOrelationshipTypeValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _SuccessorsActivityDTOactivityStatusEnumValueMap = {
  'finished': 0,
  'invalid': 1,
  'notStarted': 2,
  'paused': 3,
  'started': 4,
};
const _SuccessorsActivityDTOactivityStatusValueEnumMap = {
  0: ActivityStatusEnum.finished,
  1: ActivityStatusEnum.invalid,
  2: ActivityStatusEnum.notStarted,
  3: ActivityStatusEnum.paused,
  4: ActivityStatusEnum.started,
};
const _SuccessorsActivityDTOrelationshipTypeEnumValueMap = {
  'fs': 0,
  'ff': 1,
  'ss': 2,
  'sf': 3,
};
const _SuccessorsActivityDTOrelationshipTypeValueEnumMap = {
  0: RelationshipTypeEnum.fs,
  1: RelationshipTypeEnum.ff,
  2: RelationshipTypeEnum.ss,
  3: RelationshipTypeEnum.sf,
};

extension SuccessorsActivityDTOQueryFilter on QueryBuilder<
    SuccessorsActivityDTO, SuccessorsActivityDTO, QFilterCondition> {
  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      activityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      activityMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'activity',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activityStatus',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activityStatus',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusEqualTo(ActivityStatusEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusGreaterThan(
    ActivityStatusEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusLessThan(
    ActivityStatusEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> activityStatusBetween(
    ActivityStatusEnum? lower,
    ActivityStatusEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activityStatus',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'parentActivity',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'parentActivity',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'parentActivity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      parentActivityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      parentActivityMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'parentActivity',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentActivity',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> parentActivityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'parentActivity',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'projectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      projectIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      projectIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'projectId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> projectIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> relationshipTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relationshipType',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> relationshipTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relationshipType',
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
          QAfterFilterCondition>
      relationshipTypeEqualTo(RelationshipTypeEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> relationshipTypeGreaterThan(
    RelationshipTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> relationshipTypeLessThan(
    RelationshipTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<SuccessorsActivityDTO, SuccessorsActivityDTO,
      QAfterFilterCondition> relationshipTypeBetween(
    RelationshipTypeEnum? lower,
    RelationshipTypeEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relationshipType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension SuccessorsActivityDTOQueryObject on QueryBuilder<
    SuccessorsActivityDTO, SuccessorsActivityDTO, QFilterCondition> {}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuccessorsActivityDTO _$SuccessorsActivityDTOFromJson(
        Map<String, dynamic> json) =>
    SuccessorsActivityDTO(
      parentActivity: json['parentActivity'] as String?,
      projectId: json['projectId'] as String?,
      description: json['description'] as String?,
      activity: json['activity'] as String?,
      relationshipType: $enumDecodeNullable(
          _$RelationshipTypeEnumEnumMap, json['relationshipType']),
      activityStatus: $enumDecodeNullable(
          _$ActivityStatusEnumEnumMap, json['activityStatus']),
    );

Map<String, dynamic> _$SuccessorsActivityDTOToJson(
        SuccessorsActivityDTO instance) =>
    <String, dynamic>{
      'parentActivity': instance.parentActivity,
      'projectId': instance.projectId,
      'activity': instance.activity,
      'description': instance.description,
      'relationshipType':
          _$RelationshipTypeEnumEnumMap[instance.relationshipType],
      'activityStatus': _$ActivityStatusEnumEnumMap[instance.activityStatus],
    };

const _$RelationshipTypeEnumEnumMap = {
  RelationshipTypeEnum.fs: 0,
  RelationshipTypeEnum.ff: 1,
  RelationshipTypeEnum.ss: 2,
  RelationshipTypeEnum.sf: 3,
};

const _$ActivityStatusEnumEnumMap = {
  ActivityStatusEnum.finished: 0,
  ActivityStatusEnum.invalid: 1,
  ActivityStatusEnum.notStarted: 2,
  ActivityStatusEnum.paused: 3,
  ActivityStatusEnum.started: 4,
};
