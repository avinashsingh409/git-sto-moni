// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'predecessor_activity_dto.dart';

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const PredecessorActivityDTOSchema = Schema(
  name: r'PredecessorActivityDTO',
  id: -5266997424458340293,
  properties: {
    r'activity': PropertySchema(
      id: 0,
      name: r'activity',
      type: IsarType.string,
    ),
    r'activityStatus': PropertySchema(
      id: 1,
      name: r'activityStatus',
      type: IsarType.int,
      enumMap: _PredecessorActivityDTOactivityStatusEnumValueMap,
    ),
    r'description': PropertySchema(
      id: 2,
      name: r'description',
      type: IsarType.string,
    ),
    r'parentActivity': PropertySchema(
      id: 3,
      name: r'parentActivity',
      type: IsarType.string,
    ),
    r'projectId': PropertySchema(
      id: 4,
      name: r'projectId',
      type: IsarType.string,
    ),
    r'relationshipType': PropertySchema(
      id: 5,
      name: r'relationshipType',
      type: IsarType.int,
      enumMap: _PredecessorActivityDTOrelationshipTypeEnumValueMap,
    )
  },
  estimateSize: _predecessorActivityDTOEstimateSize,
  serialize: _predecessorActivityDTOSerialize,
  deserialize: _predecessorActivityDTODeserialize,
  deserializeProp: _predecessorActivityDTODeserializeProp,
);

int _predecessorActivityDTOEstimateSize(
  PredecessorActivityDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.activity;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.parentActivity;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.projectId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _predecessorActivityDTOSerialize(
  PredecessorActivityDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.activity);
  writer.writeInt(offsets[1], object.activityStatus?.index);
  writer.writeString(offsets[2], object.description);
  writer.writeString(offsets[3], object.parentActivity);
  writer.writeString(offsets[4], object.projectId);
  writer.writeInt(offsets[5], object.relationshipType?.index);
}

PredecessorActivityDTO _predecessorActivityDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = PredecessorActivityDTO(
    activity: reader.readStringOrNull(offsets[0]),
    activityStatus: _PredecessorActivityDTOactivityStatusValueEnumMap[
        reader.readIntOrNull(offsets[1])],
    description: reader.readStringOrNull(offsets[2]),
    parentActivity: reader.readStringOrNull(offsets[3]),
    projectId: reader.readStringOrNull(offsets[4]),
    relationshipType: _PredecessorActivityDTOrelationshipTypeValueEnumMap[
        reader.readIntOrNull(offsets[5])],
  );
  return object;
}

P _predecessorActivityDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (_PredecessorActivityDTOactivityStatusValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (_PredecessorActivityDTOrelationshipTypeValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _PredecessorActivityDTOactivityStatusEnumValueMap = {
  'finished': 0,
  'invalid': 1,
  'notStarted': 2,
  'paused': 3,
  'started': 4,
};
const _PredecessorActivityDTOactivityStatusValueEnumMap = {
  0: ActivityStatusEnum.finished,
  1: ActivityStatusEnum.invalid,
  2: ActivityStatusEnum.notStarted,
  3: ActivityStatusEnum.paused,
  4: ActivityStatusEnum.started,
};
const _PredecessorActivityDTOrelationshipTypeEnumValueMap = {
  'fs': 0,
  'ff': 1,
  'ss': 2,
  'sf': 3,
};
const _PredecessorActivityDTOrelationshipTypeValueEnumMap = {
  0: RelationshipTypeEnum.fs,
  1: RelationshipTypeEnum.ff,
  2: RelationshipTypeEnum.ss,
  3: RelationshipTypeEnum.sf,
};

extension PredecessorActivityDTOQueryFilter on QueryBuilder<
    PredecessorActivityDTO, PredecessorActivityDTO, QFilterCondition> {
  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      activityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      activityMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'activity',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activityStatus',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activityStatus',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusEqualTo(ActivityStatusEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusGreaterThan(
    ActivityStatusEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusLessThan(
    ActivityStatusEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activityStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> activityStatusBetween(
    ActivityStatusEnum? lower,
    ActivityStatusEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activityStatus',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'parentActivity',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'parentActivity',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'parentActivity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      parentActivityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'parentActivity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      parentActivityMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'parentActivity',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'parentActivity',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> parentActivityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'parentActivity',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'projectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      projectIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      projectIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'projectId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> projectIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> relationshipTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relationshipType',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> relationshipTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relationshipType',
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
          QAfterFilterCondition>
      relationshipTypeEqualTo(RelationshipTypeEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> relationshipTypeGreaterThan(
    RelationshipTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> relationshipTypeLessThan(
    RelationshipTypeEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relationshipType',
        value: value,
      ));
    });
  }

  QueryBuilder<PredecessorActivityDTO, PredecessorActivityDTO,
      QAfterFilterCondition> relationshipTypeBetween(
    RelationshipTypeEnum? lower,
    RelationshipTypeEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relationshipType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension PredecessorActivityDTOQueryObject on QueryBuilder<
    PredecessorActivityDTO, PredecessorActivityDTO, QFilterCondition> {}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PredecessorActivityDTO _$PredecessorActivityDTOFromJson(
        Map<String, dynamic> json) =>
    PredecessorActivityDTO(
      parentActivity: json['parentActivity'] as String?,
      projectId: json['projectId'] as String?,
      description: json['description'] as String?,
      activity: json['activity'] as String?,
      relationshipType: $enumDecodeNullable(
          _$RelationshipTypeEnumEnumMap, json['relationshipType']),
      activityStatus: $enumDecodeNullable(
          _$ActivityStatusEnumEnumMap, json['activityStatus']),
    );

Map<String, dynamic> _$PredecessorActivityDTOToJson(
        PredecessorActivityDTO instance) =>
    <String, dynamic>{
      'parentActivity': instance.parentActivity,
      'projectId': instance.projectId,
      'activity': instance.activity,
      'description': instance.description,
      'relationshipType':
          _$RelationshipTypeEnumEnumMap[instance.relationshipType],
      'activityStatus': _$ActivityStatusEnumEnumMap[instance.activityStatus],
    };

const _$RelationshipTypeEnumEnumMap = {
  RelationshipTypeEnum.fs: 0,
  RelationshipTypeEnum.ff: 1,
  RelationshipTypeEnum.ss: 2,
  RelationshipTypeEnum.sf: 3,
};

const _$ActivityStatusEnumEnumMap = {
  ActivityStatusEnum.finished: 0,
  ActivityStatusEnum.invalid: 1,
  ActivityStatusEnum.notStarted: 2,
  ActivityStatusEnum.paused: 3,
  ActivityStatusEnum.started: 4,
};
