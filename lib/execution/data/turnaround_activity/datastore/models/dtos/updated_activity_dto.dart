import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'updated_activity_dto.g.dart';

@Collection()
@JsonSerializable()
class UpdatedActivityDTO {
  Id? id;
  double? percentComplete;
  bool? unableToWork;
  DateTime? unableToWorkDateTime;
  List<int>? unableToWorkReasons;
  String? unableToWorkAdditionalInfo;
  int? objectId;
  DateTime? dateModified;
  DateTime? manualStartDate;
  bool? manualDateSet;
  int? commentCount;

  UpdatedActivityDTO({
    this.id,
    this.percentComplete,
    this.unableToWork,
    this.unableToWorkDateTime,
    this.unableToWorkReasons,
    this.unableToWorkAdditionalInfo,
    this.objectId,
    this.dateModified,
    this.manualStartDate,
    this.manualDateSet,
    this.commentCount,
  });

  factory UpdatedActivityDTO.fromJson(Map<String, dynamic> json) => _$UpdatedActivityDTOFromJson(json);

  // Convert JSON to UpdateActivityRequestDTO
  Map<String, dynamic> toJson() => _$UpdatedActivityDTOToJson(this);
}
