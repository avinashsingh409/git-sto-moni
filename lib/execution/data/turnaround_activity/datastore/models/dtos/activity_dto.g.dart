// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetActivityDTOCollection on Isar {
  IsarCollection<ActivityDTO> get activityDTOs => this.collection();
}

const ActivityDTOSchema = CollectionSchema(
  name: r'ActivityDTO',
  id: 1789258896148594914,
  properties: {
    r'activity': PropertySchema(
      id: 0,
      name: r'activity',
      type: IsarType.string,
    ),
    r'actualFinishDate': PropertySchema(
      id: 1,
      name: r'actualFinishDate',
      type: IsarType.dateTime,
    ),
    r'actualStartDate': PropertySchema(
      id: 2,
      name: r'actualStartDate',
      type: IsarType.dateTime,
    ),
    r'backendHasAttachments': PropertySchema(
      id: 3,
      name: r'backendHasAttachments',
      type: IsarType.bool,
    ),
    r'budgetedLaborUnits': PropertySchema(
      id: 4,
      name: r'budgetedLaborUnits',
      type: IsarType.double,
    ),
    r'budgetedNonLaborUnits': PropertySchema(
      id: 5,
      name: r'budgetedNonLaborUnits',
      type: IsarType.double,
    ),
    r'calendarName': PropertySchema(
      id: 6,
      name: r'calendarName',
      type: IsarType.string,
    ),
    r'changed': PropertySchema(
      id: 7,
      name: r'changed',
      type: IsarType.bool,
    ),
    r'changedActualFinishDate': PropertySchema(
      id: 8,
      name: r'changedActualFinishDate',
      type: IsarType.dateTime,
    ),
    r'changedActualStartDate': PropertySchema(
      id: 9,
      name: r'changedActualStartDate',
      type: IsarType.dateTime,
    ),
    r'commentCount': PropertySchema(
      id: 10,
      name: r'commentCount',
      type: IsarType.long,
    ),
    r'dataDate': PropertySchema(
      id: 11,
      name: r'dataDate',
      type: IsarType.dateTime,
    ),
    r'dateCreated': PropertySchema(
      id: 12,
      name: r'dateCreated',
      type: IsarType.dateTime,
    ),
    r'dateModified': PropertySchema(
      id: 13,
      name: r'dateModified',
      type: IsarType.dateTime,
    ),
    r'description': PropertySchema(
      id: 14,
      name: r'description',
      type: IsarType.string,
    ),
    r'displayDate': PropertySchema(
      id: 15,
      name: r'displayDate',
      type: IsarType.string,
    ),
    r'displayFinishDate': PropertySchema(
      id: 16,
      name: r'displayFinishDate',
      type: IsarType.string,
    ),
    r'dynamicFieldData': PropertySchema(
      id: 17,
      name: r'dynamicFieldData',
      type: IsarType.string,
    ),
    r'dynamicFieldDateTimeData': PropertySchema(
      id: 18,
      name: r'dynamicFieldDateTimeData',
      type: IsarType.string,
    ),
    r'dynamicFieldNumberData': PropertySchema(
      id: 19,
      name: r'dynamicFieldNumberData',
      type: IsarType.string,
    ),
    r'finishDate': PropertySchema(
      id: 20,
      name: r'finishDate',
      type: IsarType.dateTime,
    ),
    r'freeFloat': PropertySchema(
      id: 21,
      name: r'freeFloat',
      type: IsarType.double,
    ),
    r'holder': PropertySchema(
      id: 22,
      name: r'holder',
      type: IsarType.string,
    ),
    r'hostName': PropertySchema(
      id: 23,
      name: r'hostName',
      type: IsarType.string,
    ),
    r'isDeleted': PropertySchema(
      id: 24,
      name: r'isDeleted',
      type: IsarType.bool,
    ),
    r'lastPercentComplete': PropertySchema(
      id: 25,
      name: r'lastPercentComplete',
      type: IsarType.double,
    ),
    r'manualDateSet': PropertySchema(
      id: 26,
      name: r'manualDateSet',
      type: IsarType.bool,
    ),
    r'modifiedById': PropertySchema(
      id: 27,
      name: r'modifiedById',
      type: IsarType.long,
    ),
    r'modifiedByName': PropertySchema(
      id: 28,
      name: r'modifiedByName',
      type: IsarType.string,
    ),
    r'objectId': PropertySchema(
      id: 29,
      name: r'objectId',
      type: IsarType.long,
    ),
    r'percentComplete': PropertySchema(
      id: 30,
      name: r'percentComplete',
      type: IsarType.double,
    ),
    r'percentCompleteColor': PropertySchema(
      id: 31,
      name: r'percentCompleteColor',
      type: IsarType.string,
    ),
    r'plannedDuration': PropertySchema(
      id: 32,
      name: r'plannedDuration',
      type: IsarType.double,
    ),
    r'plannedFinishDate': PropertySchema(
      id: 33,
      name: r'plannedFinishDate',
      type: IsarType.dateTime,
    ),
    r'plannedStartDate': PropertySchema(
      id: 34,
      name: r'plannedStartDate',
      type: IsarType.dateTime,
    ),
    r'preApproved': PropertySchema(
      id: 35,
      name: r'preApproved',
      type: IsarType.bool,
    ),
    r'predecessors': PropertySchema(
      id: 36,
      name: r'predecessors',
      type: IsarType.objectList,
      target: r'PredecessorActivityDTO',
    ),
    r'predecessorsStatus': PropertySchema(
      id: 37,
      name: r'predecessorsStatus',
      type: IsarType.int,
      enumMap: _ActivityDTOpredecessorsStatusEnumValueMap,
    ),
    r'projectId': PropertySchema(
      id: 38,
      name: r'projectId',
      type: IsarType.string,
    ),
    r'rejectedFinishDate': PropertySchema(
      id: 39,
      name: r'rejectedFinishDate',
      type: IsarType.dateTime,
    ),
    r'rejectedPercentComplete': PropertySchema(
      id: 40,
      name: r'rejectedPercentComplete',
      type: IsarType.double,
    ),
    r'rejectedStartDate': PropertySchema(
      id: 41,
      name: r'rejectedStartDate',
      type: IsarType.dateTime,
    ),
    r'remainingDuration': PropertySchema(
      id: 42,
      name: r'remainingDuration',
      type: IsarType.double,
    ),
    r'remainingEarlyFinishDate': PropertySchema(
      id: 43,
      name: r'remainingEarlyFinishDate',
      type: IsarType.dateTime,
    ),
    r'remainingEarlyStartDate': PropertySchema(
      id: 44,
      name: r'remainingEarlyStartDate',
      type: IsarType.dateTime,
    ),
    r'remainingLaborUnits': PropertySchema(
      id: 45,
      name: r'remainingLaborUnits',
      type: IsarType.double,
    ),
    r'remainingNonLaborUnits': PropertySchema(
      id: 46,
      name: r'remainingNonLaborUnits',
      type: IsarType.double,
    ),
    r'sentForPrimaveraSync': PropertySchema(
      id: 47,
      name: r'sentForPrimaveraSync',
      type: IsarType.bool,
    ),
    r'startDate': PropertySchema(
      id: 48,
      name: r'startDate',
      type: IsarType.dateTime,
    ),
    r'successors': PropertySchema(
      id: 49,
      name: r'successors',
      type: IsarType.objectList,
      target: r'SuccessorsActivityDTO',
    ),
    r'successorsStatus': PropertySchema(
      id: 50,
      name: r'successorsStatus',
      type: IsarType.int,
      enumMap: _ActivityDTOsuccessorsStatusEnumValueMap,
    ),
    r'totalFloat': PropertySchema(
      id: 51,
      name: r'totalFloat',
      type: IsarType.double,
    ),
    r'unableToWork': PropertySchema(
      id: 52,
      name: r'unableToWork',
      type: IsarType.bool,
    ),
    r'unableToWorkAdditionalInfo': PropertySchema(
      id: 53,
      name: r'unableToWorkAdditionalInfo',
      type: IsarType.string,
    ),
    r'unableToWorkDateTime': PropertySchema(
      id: 54,
      name: r'unableToWorkDateTime',
      type: IsarType.dateTime,
    ),
    r'unableToWorkImageFilePaths': PropertySchema(
      id: 55,
      name: r'unableToWorkImageFilePaths',
      type: IsarType.stringList,
    ),
    r'unableToWorkReasons': PropertySchema(
      id: 56,
      name: r'unableToWorkReasons',
      type: IsarType.longList,
    ),
    r'wbsName': PropertySchema(
      id: 57,
      name: r'wbsName',
      type: IsarType.string,
    ),
    r'wbsObjectId': PropertySchema(
      id: 58,
      name: r'wbsObjectId',
      type: IsarType.long,
    ),
    r'wbsRoute': PropertySchema(
      id: 59,
      name: r'wbsRoute',
      type: IsarType.stringList,
    ),
    r'workPackageId': PropertySchema(
      id: 60,
      name: r'workPackageId',
      type: IsarType.long,
    )
  },
  estimateSize: _activityDTOEstimateSize,
  serialize: _activityDTOSerialize,
  deserialize: _activityDTODeserialize,
  deserializeProp: _activityDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {
    r'PredecessorActivityDTO': PredecessorActivityDTOSchema,
    r'SuccessorsActivityDTO': SuccessorsActivityDTOSchema
  },
  getId: _activityDTOGetId,
  getLinks: _activityDTOGetLinks,
  attach: _activityDTOAttach,
  version: '3.1.0+1',
);

int _activityDTOEstimateSize(
  ActivityDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.activity;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.calendarName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.displayDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.displayFinishDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.dynamicFieldData;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.dynamicFieldDateTimeData;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.dynamicFieldNumberData;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.holder;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.hostName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.modifiedByName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.percentCompleteColor;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.predecessors;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        final offsets = allOffsets[PredecessorActivityDTO]!;
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += PredecessorActivityDTOSchema.estimateSize(
              value, offsets, allOffsets);
        }
      }
    }
  }
  {
    final value = object.projectId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.successors;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        final offsets = allOffsets[SuccessorsActivityDTO]!;
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += SuccessorsActivityDTOSchema.estimateSize(
              value, offsets, allOffsets);
        }
      }
    }
  }
  {
    final value = object.unableToWorkAdditionalInfo;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.unableToWorkImageFilePaths;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.unableToWorkReasons;
    if (value != null) {
      bytesCount += 3 + value.length * 8;
    }
  }
  {
    final value = object.wbsName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.wbsRoute;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  return bytesCount;
}

void _activityDTOSerialize(
  ActivityDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.activity);
  writer.writeDateTime(offsets[1], object.actualFinishDate);
  writer.writeDateTime(offsets[2], object.actualStartDate);
  writer.writeBool(offsets[3], object.backendHasAttachments);
  writer.writeDouble(offsets[4], object.budgetedLaborUnits);
  writer.writeDouble(offsets[5], object.budgetedNonLaborUnits);
  writer.writeString(offsets[6], object.calendarName);
  writer.writeBool(offsets[7], object.changed);
  writer.writeDateTime(offsets[8], object.changedActualFinishDate);
  writer.writeDateTime(offsets[9], object.changedActualStartDate);
  writer.writeLong(offsets[10], object.commentCount);
  writer.writeDateTime(offsets[11], object.dataDate);
  writer.writeDateTime(offsets[12], object.dateCreated);
  writer.writeDateTime(offsets[13], object.dateModified);
  writer.writeString(offsets[14], object.description);
  writer.writeString(offsets[15], object.displayDate);
  writer.writeString(offsets[16], object.displayFinishDate);
  writer.writeString(offsets[17], object.dynamicFieldData);
  writer.writeString(offsets[18], object.dynamicFieldDateTimeData);
  writer.writeString(offsets[19], object.dynamicFieldNumberData);
  writer.writeDateTime(offsets[20], object.finishDate);
  writer.writeDouble(offsets[21], object.freeFloat);
  writer.writeString(offsets[22], object.holder);
  writer.writeString(offsets[23], object.hostName);
  writer.writeBool(offsets[24], object.isDeleted);
  writer.writeDouble(offsets[25], object.lastPercentComplete);
  writer.writeBool(offsets[26], object.manualDateSet);
  writer.writeLong(offsets[27], object.modifiedById);
  writer.writeString(offsets[28], object.modifiedByName);
  writer.writeLong(offsets[29], object.objectId);
  writer.writeDouble(offsets[30], object.percentComplete);
  writer.writeString(offsets[31], object.percentCompleteColor);
  writer.writeDouble(offsets[32], object.plannedDuration);
  writer.writeDateTime(offsets[33], object.plannedFinishDate);
  writer.writeDateTime(offsets[34], object.plannedStartDate);
  writer.writeBool(offsets[35], object.preApproved);
  writer.writeObjectList<PredecessorActivityDTO>(
    offsets[36],
    allOffsets,
    PredecessorActivityDTOSchema.serialize,
    object.predecessors,
  );
  writer.writeInt(offsets[37], object.predecessorsStatus?.index);
  writer.writeString(offsets[38], object.projectId);
  writer.writeDateTime(offsets[39], object.rejectedFinishDate);
  writer.writeDouble(offsets[40], object.rejectedPercentComplete);
  writer.writeDateTime(offsets[41], object.rejectedStartDate);
  writer.writeDouble(offsets[42], object.remainingDuration);
  writer.writeDateTime(offsets[43], object.remainingEarlyFinishDate);
  writer.writeDateTime(offsets[44], object.remainingEarlyStartDate);
  writer.writeDouble(offsets[45], object.remainingLaborUnits);
  writer.writeDouble(offsets[46], object.remainingNonLaborUnits);
  writer.writeBool(offsets[47], object.sentForPrimaveraSync);
  writer.writeDateTime(offsets[48], object.startDate);
  writer.writeObjectList<SuccessorsActivityDTO>(
    offsets[49],
    allOffsets,
    SuccessorsActivityDTOSchema.serialize,
    object.successors,
  );
  writer.writeInt(offsets[50], object.successorsStatus?.index);
  writer.writeDouble(offsets[51], object.totalFloat);
  writer.writeBool(offsets[52], object.unableToWork);
  writer.writeString(offsets[53], object.unableToWorkAdditionalInfo);
  writer.writeDateTime(offsets[54], object.unableToWorkDateTime);
  writer.writeStringList(offsets[55], object.unableToWorkImageFilePaths);
  writer.writeLongList(offsets[56], object.unableToWorkReasons);
  writer.writeString(offsets[57], object.wbsName);
  writer.writeLong(offsets[58], object.wbsObjectId);
  writer.writeStringList(offsets[59], object.wbsRoute);
  writer.writeLong(offsets[60], object.workPackageId);
}

ActivityDTO _activityDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ActivityDTO(
    activity: reader.readStringOrNull(offsets[0]),
    actualFinishDate: reader.readDateTimeOrNull(offsets[1]),
    actualStartDate: reader.readDateTimeOrNull(offsets[2]),
    backendHasAttachments: reader.readBoolOrNull(offsets[3]),
    budgetedLaborUnits: reader.readDoubleOrNull(offsets[4]),
    budgetedNonLaborUnits: reader.readDoubleOrNull(offsets[5]),
    calendarName: reader.readStringOrNull(offsets[6]),
    changed: reader.readBoolOrNull(offsets[7]),
    changedActualFinishDate: reader.readDateTimeOrNull(offsets[8]),
    changedActualStartDate: reader.readDateTimeOrNull(offsets[9]),
    commentCount: reader.readLongOrNull(offsets[10]),
    dataDate: reader.readDateTimeOrNull(offsets[11]),
    dateCreated: reader.readDateTimeOrNull(offsets[12]),
    dateModified: reader.readDateTimeOrNull(offsets[13]),
    description: reader.readStringOrNull(offsets[14]),
    displayDate: reader.readStringOrNull(offsets[15]),
    displayFinishDate: reader.readStringOrNull(offsets[16]),
    dynamicFieldData: reader.readStringOrNull(offsets[17]),
    dynamicFieldDateTimeData: reader.readStringOrNull(offsets[18]),
    dynamicFieldNumberData: reader.readStringOrNull(offsets[19]),
    finishDate: reader.readDateTimeOrNull(offsets[20]),
    freeFloat: reader.readDoubleOrNull(offsets[21]),
    holder: reader.readStringOrNull(offsets[22]),
    hostName: reader.readStringOrNull(offsets[23]),
    id: id,
    isDeleted: reader.readBoolOrNull(offsets[24]),
    lastPercentComplete: reader.readDoubleOrNull(offsets[25]),
    manualDateSet: reader.readBoolOrNull(offsets[26]),
    modifiedById: reader.readLongOrNull(offsets[27]),
    modifiedByName: reader.readStringOrNull(offsets[28]),
    objectId: reader.readLongOrNull(offsets[29]),
    percentComplete: reader.readDoubleOrNull(offsets[30]),
    percentCompleteColor: reader.readStringOrNull(offsets[31]),
    plannedDuration: reader.readDoubleOrNull(offsets[32]),
    plannedFinishDate: reader.readDateTimeOrNull(offsets[33]),
    plannedStartDate: reader.readDateTimeOrNull(offsets[34]),
    preApproved: reader.readBoolOrNull(offsets[35]),
    predecessors: reader.readObjectList<PredecessorActivityDTO>(
      offsets[36],
      PredecessorActivityDTOSchema.deserialize,
      allOffsets,
      PredecessorActivityDTO(),
    ),
    predecessorsStatus: _ActivityDTOpredecessorsStatusValueEnumMap[
        reader.readIntOrNull(offsets[37])],
    projectId: reader.readStringOrNull(offsets[38]),
    rejectedFinishDate: reader.readDateTimeOrNull(offsets[39]),
    rejectedPercentComplete: reader.readDoubleOrNull(offsets[40]),
    rejectedStartDate: reader.readDateTimeOrNull(offsets[41]),
    remainingDuration: reader.readDoubleOrNull(offsets[42]),
    remainingEarlyFinishDate: reader.readDateTimeOrNull(offsets[43]),
    remainingEarlyStartDate: reader.readDateTimeOrNull(offsets[44]),
    remainingLaborUnits: reader.readDoubleOrNull(offsets[45]),
    remainingNonLaborUnits: reader.readDoubleOrNull(offsets[46]),
    sentForPrimaveraSync: reader.readBoolOrNull(offsets[47]),
    startDate: reader.readDateTimeOrNull(offsets[48]),
    successors: reader.readObjectList<SuccessorsActivityDTO>(
      offsets[49],
      SuccessorsActivityDTOSchema.deserialize,
      allOffsets,
      SuccessorsActivityDTO(),
    ),
    successorsStatus: _ActivityDTOsuccessorsStatusValueEnumMap[
        reader.readIntOrNull(offsets[50])],
    totalFloat: reader.readDoubleOrNull(offsets[51]),
    unableToWork: reader.readBoolOrNull(offsets[52]),
    unableToWorkAdditionalInfo: reader.readStringOrNull(offsets[53]),
    unableToWorkDateTime: reader.readDateTimeOrNull(offsets[54]),
    unableToWorkImageFilePaths: reader.readStringList(offsets[55]),
    unableToWorkReasons: reader.readLongOrNullList(offsets[56]),
    wbsName: reader.readStringOrNull(offsets[57]),
    wbsObjectId: reader.readLongOrNull(offsets[58]),
    wbsRoute: reader.readStringList(offsets[59]),
    workPackageId: reader.readLongOrNull(offsets[60]),
  );
  return object;
}

P _activityDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 2:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 3:
      return (reader.readBoolOrNull(offset)) as P;
    case 4:
      return (reader.readDoubleOrNull(offset)) as P;
    case 5:
      return (reader.readDoubleOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readBoolOrNull(offset)) as P;
    case 8:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 9:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    case 11:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 12:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 13:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 14:
      return (reader.readStringOrNull(offset)) as P;
    case 15:
      return (reader.readStringOrNull(offset)) as P;
    case 16:
      return (reader.readStringOrNull(offset)) as P;
    case 17:
      return (reader.readStringOrNull(offset)) as P;
    case 18:
      return (reader.readStringOrNull(offset)) as P;
    case 19:
      return (reader.readStringOrNull(offset)) as P;
    case 20:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 21:
      return (reader.readDoubleOrNull(offset)) as P;
    case 22:
      return (reader.readStringOrNull(offset)) as P;
    case 23:
      return (reader.readStringOrNull(offset)) as P;
    case 24:
      return (reader.readBoolOrNull(offset)) as P;
    case 25:
      return (reader.readDoubleOrNull(offset)) as P;
    case 26:
      return (reader.readBoolOrNull(offset)) as P;
    case 27:
      return (reader.readLongOrNull(offset)) as P;
    case 28:
      return (reader.readStringOrNull(offset)) as P;
    case 29:
      return (reader.readLongOrNull(offset)) as P;
    case 30:
      return (reader.readDoubleOrNull(offset)) as P;
    case 31:
      return (reader.readStringOrNull(offset)) as P;
    case 32:
      return (reader.readDoubleOrNull(offset)) as P;
    case 33:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 34:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 35:
      return (reader.readBoolOrNull(offset)) as P;
    case 36:
      return (reader.readObjectList<PredecessorActivityDTO>(
        offset,
        PredecessorActivityDTOSchema.deserialize,
        allOffsets,
        PredecessorActivityDTO(),
      )) as P;
    case 37:
      return (_ActivityDTOpredecessorsStatusValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 38:
      return (reader.readStringOrNull(offset)) as P;
    case 39:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 40:
      return (reader.readDoubleOrNull(offset)) as P;
    case 41:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 42:
      return (reader.readDoubleOrNull(offset)) as P;
    case 43:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 44:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 45:
      return (reader.readDoubleOrNull(offset)) as P;
    case 46:
      return (reader.readDoubleOrNull(offset)) as P;
    case 47:
      return (reader.readBoolOrNull(offset)) as P;
    case 48:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 49:
      return (reader.readObjectList<SuccessorsActivityDTO>(
        offset,
        SuccessorsActivityDTOSchema.deserialize,
        allOffsets,
        SuccessorsActivityDTO(),
      )) as P;
    case 50:
      return (_ActivityDTOsuccessorsStatusValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 51:
      return (reader.readDoubleOrNull(offset)) as P;
    case 52:
      return (reader.readBoolOrNull(offset)) as P;
    case 53:
      return (reader.readStringOrNull(offset)) as P;
    case 54:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 55:
      return (reader.readStringList(offset)) as P;
    case 56:
      return (reader.readLongOrNullList(offset)) as P;
    case 57:
      return (reader.readStringOrNull(offset)) as P;
    case 58:
      return (reader.readLongOrNull(offset)) as P;
    case 59:
      return (reader.readStringList(offset)) as P;
    case 60:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _ActivityDTOpredecessorsStatusEnumValueMap = {
  'notStarted': 0,
  'inProgress': 1,
  'completed': 2,
  'unableToWork': 3,
};
const _ActivityDTOpredecessorsStatusValueEnumMap = {
  0: PrimaveraActivityStatus.notStarted,
  1: PrimaveraActivityStatus.inProgress,
  2: PrimaveraActivityStatus.completed,
  3: PrimaveraActivityStatus.unableToWork,
};
const _ActivityDTOsuccessorsStatusEnumValueMap = {
  'notStarted': 0,
  'inProgress': 1,
  'completed': 2,
  'unableToWork': 3,
};
const _ActivityDTOsuccessorsStatusValueEnumMap = {
  0: PrimaveraActivityStatus.notStarted,
  1: PrimaveraActivityStatus.inProgress,
  2: PrimaveraActivityStatus.completed,
  3: PrimaveraActivityStatus.unableToWork,
};

Id _activityDTOGetId(ActivityDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _activityDTOGetLinks(ActivityDTO object) {
  return [];
}

void _activityDTOAttach(
    IsarCollection<dynamic> col, Id id, ActivityDTO object) {
  object.id = id;
}

extension ActivityDTOQueryWhereSort
    on QueryBuilder<ActivityDTO, ActivityDTO, QWhere> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension ActivityDTOQueryWhere
    on QueryBuilder<ActivityDTO, ActivityDTO, QWhereClause> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ActivityDTOQueryFilter
    on QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activity',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> activityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> activityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'activity',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> activityMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'activity',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      activityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'activity',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'actualFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'actualFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'actualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'actualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'actualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualFinishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'actualFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'actualStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'actualStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'actualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'actualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'actualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      actualStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'actualStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      backendHasAttachmentsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'backendHasAttachments',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      backendHasAttachmentsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'backendHasAttachments',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      backendHasAttachmentsEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'backendHasAttachments',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'budgetedLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'budgetedLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'budgetedLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'budgetedLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'budgetedLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedLaborUnitsBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'budgetedLaborUnits',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'budgetedNonLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'budgetedNonLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'budgetedNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'budgetedNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'budgetedNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      budgetedNonLaborUnitsBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'budgetedNonLaborUnits',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'calendarName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'calendarName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'calendarName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'calendarName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'calendarName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'calendarName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      calendarNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'calendarName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'changed',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'changed',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> changedEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'changed',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'changedActualFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'changedActualFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'changedActualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'changedActualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'changedActualFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualFinishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'changedActualFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'changedActualStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'changedActualStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'changedActualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'changedActualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'changedActualStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      changedActualStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'changedActualStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'commentCount',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'commentCount',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'commentCount',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      commentCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'commentCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dataDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dataDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dataDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dataDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> dataDateEqualTo(
      DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dataDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dataDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dataDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dataDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dataDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> dataDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dataDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dateCreated',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dateCreated',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateCreated',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateCreated',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateCreated',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateCreatedBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateCreated',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dateModifiedBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateModified',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'displayDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'displayDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'displayDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'displayDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'displayDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayDate',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'displayDate',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'displayFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'displayFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'displayFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'displayFinishDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'displayFinishDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayFinishDate',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      displayFinishDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'displayFinishDate',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dynamicFieldData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dynamicFieldData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dynamicFieldData',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'dynamicFieldData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'dynamicFieldData',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'dynamicFieldData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dynamicFieldDateTimeData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dynamicFieldDateTimeData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dynamicFieldDateTimeData',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'dynamicFieldDateTimeData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'dynamicFieldDateTimeData',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldDateTimeData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldDateTimeDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'dynamicFieldDateTimeData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dynamicFieldNumberData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dynamicFieldNumberData',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dynamicFieldNumberData',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'dynamicFieldNumberData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'dynamicFieldNumberData',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dynamicFieldNumberData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      dynamicFieldNumberDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'dynamicFieldNumberData',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'finishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'finishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'finishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'finishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'finishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      finishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'finishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'freeFloat',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'freeFloat',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'freeFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'freeFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'freeFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      freeFloatBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'freeFloat',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'holder',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      holderIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'holder',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      holderGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'holder',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      holderStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'holder',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> holderMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'holder',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      holderIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'holder',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      holderIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'holder',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> hostNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> hostNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'hostName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> hostNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'hostName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      hostNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      isDeletedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isDeleted',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      isDeletedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isDeleted',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      isDeletedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isDeleted',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lastPercentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lastPercentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      lastPercentCompleteBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastPercentComplete',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      manualDateSetIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'manualDateSet',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      manualDateSetIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'manualDateSet',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      manualDateSetEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'manualDateSet',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'modifiedById',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'modifiedById',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'modifiedById',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'modifiedById',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'modifiedById',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'modifiedById',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'modifiedByName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'modifiedByName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'modifiedByName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'modifiedByName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'modifiedByName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'modifiedByName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      modifiedByNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'modifiedByName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      objectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      objectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> objectIdEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      objectIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      objectIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> objectIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'percentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'percentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'percentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'percentComplete',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'percentCompleteColor',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'percentCompleteColor',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'percentCompleteColor',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'percentCompleteColor',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'percentCompleteColor',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentCompleteColor',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      percentCompleteColorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'percentCompleteColor',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'plannedDuration',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'plannedDuration',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'plannedDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'plannedDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'plannedDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedDurationBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'plannedDuration',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'plannedFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'plannedFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'plannedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'plannedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'plannedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedFinishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'plannedFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'plannedStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'plannedStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'plannedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'plannedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'plannedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      plannedStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'plannedStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      preApprovedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'preApproved',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      preApprovedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'preApproved',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      preApprovedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'preApproved',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'predecessors',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'predecessors',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'predecessors',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'predecessorsStatus',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'predecessorsStatus',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusEqualTo(PrimaveraActivityStatus? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'predecessorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusGreaterThan(
    PrimaveraActivityStatus? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'predecessorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusLessThan(
    PrimaveraActivityStatus? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'predecessorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsStatusBetween(
    PrimaveraActivityStatus? lower,
    PrimaveraActivityStatus? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'predecessorsStatus',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'projectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'projectId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      projectIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'rejectedFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'rejectedFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'rejectedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'rejectedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'rejectedFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedFinishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'rejectedFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'rejectedPercentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'rejectedPercentComplete',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'rejectedPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'rejectedPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'rejectedPercentComplete',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedPercentCompleteBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'rejectedPercentComplete',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'rejectedStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'rejectedStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'rejectedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'rejectedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'rejectedStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      rejectedStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'rejectedStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'remainingDuration',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'remainingDuration',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'remainingDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'remainingDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'remainingDuration',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingDurationBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'remainingDuration',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'remainingEarlyFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'remainingEarlyFinishDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'remainingEarlyFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'remainingEarlyFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'remainingEarlyFinishDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyFinishDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'remainingEarlyFinishDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'remainingEarlyStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'remainingEarlyStartDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'remainingEarlyStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'remainingEarlyStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'remainingEarlyStartDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingEarlyStartDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'remainingEarlyStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'remainingLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'remainingLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'remainingLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'remainingLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'remainingLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingLaborUnitsBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'remainingLaborUnits',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'remainingNonLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'remainingNonLaborUnits',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'remainingNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'remainingNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'remainingNonLaborUnits',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      remainingNonLaborUnitsBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'remainingNonLaborUnits',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      sentForPrimaveraSyncIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sentForPrimaveraSync',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      sentForPrimaveraSyncIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sentForPrimaveraSync',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      sentForPrimaveraSyncEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sentForPrimaveraSync',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'startDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'startDate',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'startDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'startDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'startDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      startDateBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'startDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'successors',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'successors',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'successors',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'successorsStatus',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'successorsStatus',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusEqualTo(PrimaveraActivityStatus? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'successorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusGreaterThan(
    PrimaveraActivityStatus? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'successorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusLessThan(
    PrimaveraActivityStatus? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'successorsStatus',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsStatusBetween(
    PrimaveraActivityStatus? lower,
    PrimaveraActivityStatus? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'successorsStatus',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'totalFloat',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'totalFloat',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'totalFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'totalFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'totalFloat',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      totalFloatBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'totalFloat',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWork',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWork',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWork',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkAdditionalInfo',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkAdditionalInfo',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkAdditionalInfo',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'unableToWorkAdditionalInfo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'unableToWorkAdditionalInfo',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkAdditionalInfo',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkAdditionalInfoIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'unableToWorkAdditionalInfo',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkDateTime',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkDateTime',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkDateTimeBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkDateTime',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkImageFilePaths',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkImageFilePaths',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkImageFilePaths',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'unableToWorkImageFilePaths',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'unableToWorkImageFilePaths',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkImageFilePaths',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'unableToWorkImageFilePaths',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkImageFilePathsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkImageFilePaths',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.elementIsNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.elementIsNotNull(
        property: r'unableToWorkReasons',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unableToWorkReasons',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsElementBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unableToWorkReasons',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      unableToWorkReasonsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'unableToWorkReasons',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'wbsName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'wbsName',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'wbsName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'wbsName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> wbsNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'wbsName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wbsName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'wbsName',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'wbsObjectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'wbsObjectId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wbsObjectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'wbsObjectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'wbsObjectId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsObjectIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'wbsObjectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'wbsRoute',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'wbsRoute',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'wbsRoute',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'wbsRoute',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'wbsRoute',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'wbsRoute',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'wbsRoute',
        value: '',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      wbsRouteLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'wbsRoute',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'workPackageId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'workPackageId',
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'workPackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'workPackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'workPackageId',
        value: value,
      ));
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      workPackageIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'workPackageId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ActivityDTOQueryObject
    on QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      predecessorsElement(FilterQuery<PredecessorActivityDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'predecessors');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition>
      successorsElement(FilterQuery<SuccessorsActivityDTO> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'successors');
    });
  }
}

extension ActivityDTOQueryLinks
    on QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> {}

extension ActivityDTOQuerySortBy
    on QueryBuilder<ActivityDTO, ActivityDTO, QSortBy> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByActivity() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activity', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByActivityDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activity', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByActualFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByActualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBackendHasAttachments() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backendHasAttachments', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBackendHasAttachmentsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backendHasAttachments', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBudgetedLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBudgetedLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBudgetedNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedNonLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByBudgetedNonLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedNonLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByCalendarName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'calendarName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByCalendarNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'calendarName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changed', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changed', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByChangedActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByChangedActualFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByChangedActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByChangedActualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByCommentCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDataDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dataDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDataDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dataDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDateCreated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreated', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDateCreatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreated', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDisplayDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByDisplayDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDisplayFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDisplayFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldDateTimeData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldDateTimeData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldDateTimeDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldDateTimeData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldNumberData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldNumberData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByDynamicFieldNumberDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldNumberData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByFreeFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'freeFloat', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByFreeFloatDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'freeFloat', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByHolder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'holder', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByHolderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'holder', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByIsDeleted() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDeleted', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByIsDeletedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDeleted', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByLastPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastPercentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByLastPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastPercentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByManualDateSetDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByModifiedById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedById', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByModifiedByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedById', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByModifiedByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedByName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByModifiedByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedByName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPercentCompleteColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentCompleteColor', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPercentCompleteColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentCompleteColor', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByPlannedDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedDuration', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPlannedDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedDuration', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPlannedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPlannedFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPlannedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPlannedStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByPreApproved() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'preApproved', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByPreApprovedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'preApproved', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPredecessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'predecessorsStatus', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByPredecessorsStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'predecessorsStatus', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedPercentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedPercentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRejectedStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingDuration', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingDuration', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingEarlyFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingEarlyFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingEarlyStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingEarlyStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingNonLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByRemainingNonLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingNonLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortBySentForPrimaveraSync() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sentForPrimaveraSync', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortBySentForPrimaveraSyncDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sentForPrimaveraSync', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'startDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'startDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortBySuccessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'successorsStatus', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortBySuccessorsStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'successorsStatus', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByTotalFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalFloat', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByTotalFloatDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalFloat', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByUnableToWorkDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByUnableToWorkAdditionalInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByUnableToWorkAdditionalInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByUnableToWorkDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByWbsName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByWbsNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByWbsObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsObjectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByWbsObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsObjectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> sortByWorkPackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      sortByWorkPackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageId', Sort.desc);
    });
  }
}

extension ActivityDTOQuerySortThenBy
    on QueryBuilder<ActivityDTO, ActivityDTO, QSortThenBy> {
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByActivity() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activity', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByActivityDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activity', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByActualFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByActualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'actualStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBackendHasAttachments() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backendHasAttachments', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBackendHasAttachmentsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backendHasAttachments', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBudgetedLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBudgetedLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBudgetedNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedNonLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByBudgetedNonLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'budgetedNonLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByCalendarName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'calendarName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByCalendarNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'calendarName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changed', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByChangedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changed', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByChangedActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByChangedActualFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByChangedActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByChangedActualStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'changedActualStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByCommentCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentCount', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDataDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dataDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDataDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dataDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDateCreated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreated', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDateCreatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreated', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDisplayDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByDisplayDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDisplayFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDisplayFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldDateTimeData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldDateTimeData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldDateTimeDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldDateTimeData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldNumberData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldNumberData', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByDynamicFieldNumberDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dynamicFieldNumberData', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByFreeFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'freeFloat', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByFreeFloatDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'freeFloat', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByHolder() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'holder', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByHolderDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'holder', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByIsDeleted() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDeleted', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByIsDeletedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDeleted', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByLastPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastPercentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByLastPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastPercentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByManualDateSetDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'manualDateSet', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByModifiedById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedById', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByModifiedByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedById', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByModifiedByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedByName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByModifiedByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'modifiedByName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPercentCompleteColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentCompleteColor', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPercentCompleteColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentCompleteColor', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByPlannedDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedDuration', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPlannedDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedDuration', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPlannedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPlannedFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPlannedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPlannedStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'plannedStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByPreApproved() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'preApproved', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByPreApprovedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'preApproved', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPredecessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'predecessorsStatus', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByPredecessorsStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'predecessorsStatus', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedPercentComplete', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedPercentCompleteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedPercentComplete', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRejectedStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rejectedStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingDuration', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingDuration', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingEarlyFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyFinishDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingEarlyFinishDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyFinishDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingEarlyStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyStartDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingEarlyStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingEarlyStartDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingNonLaborUnits', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByRemainingNonLaborUnitsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'remainingNonLaborUnits', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenBySentForPrimaveraSync() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sentForPrimaveraSync', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenBySentForPrimaveraSyncDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sentForPrimaveraSync', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'startDate', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'startDate', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenBySuccessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'successorsStatus', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenBySuccessorsStatusDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'successorsStatus', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByTotalFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalFloat', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByTotalFloatDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalFloat', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByUnableToWorkDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWork', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByUnableToWorkAdditionalInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByUnableToWorkAdditionalInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkAdditionalInfo', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByUnableToWorkDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unableToWorkDateTime', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByWbsName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsName', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByWbsNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsName', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByWbsObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsObjectId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByWbsObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'wbsObjectId', Sort.desc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy> thenByWorkPackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageId', Sort.asc);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QAfterSortBy>
      thenByWorkPackageIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'workPackageId', Sort.desc);
    });
  }
}

extension ActivityDTOQueryWhereDistinct
    on QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> {
  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByActivity(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'activity', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'actualFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'actualStartDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByBackendHasAttachments() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'backendHasAttachments');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByBudgetedLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'budgetedLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByBudgetedNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'budgetedNonLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByCalendarName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'calendarName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByChanged() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'changed');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByChangedActualFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'changedActualFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByChangedActualStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'changedActualStartDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByCommentCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'commentCount');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDataDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dataDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDateCreated() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateCreated');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateModified');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDescription(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'description', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDisplayDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'displayDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDisplayFinishDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'displayFinishDate',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByDynamicFieldData(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dynamicFieldData',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByDynamicFieldDateTimeData({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dynamicFieldDateTimeData',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByDynamicFieldNumberData({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dynamicFieldNumberData',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'finishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByFreeFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'freeFloat');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByHolder(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'holder', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByHostName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'hostName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByIsDeleted() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isDeleted');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByLastPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastPercentComplete');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByManualDateSet() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'manualDateSet');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByModifiedById() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'modifiedById');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByModifiedByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'modifiedByName',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectId');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'percentComplete');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPercentCompleteColor({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'percentCompleteColor',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPlannedDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'plannedDuration');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPlannedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'plannedFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPlannedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'plannedStartDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByPreApproved() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'preApproved');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByPredecessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'predecessorsStatus');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByProjectId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'projectId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRejectedFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'rejectedFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRejectedPercentComplete() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'rejectedPercentComplete');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRejectedStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'rejectedStartDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRemainingDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'remainingDuration');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRemainingEarlyFinishDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'remainingEarlyFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRemainingEarlyStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'remainingEarlyStartDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRemainingLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'remainingLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByRemainingNonLaborUnits() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'remainingNonLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctBySentForPrimaveraSync() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'sentForPrimaveraSync');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'startDate');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctBySuccessorsStatus() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'successorsStatus');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByTotalFloat() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'totalFloat');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByUnableToWork() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWork');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByUnableToWorkAdditionalInfo({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkAdditionalInfo',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByUnableToWorkDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkDateTime');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByUnableToWorkImageFilePaths() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkImageFilePaths');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct>
      distinctByUnableToWorkReasons() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unableToWorkReasons');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByWbsName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'wbsName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByWbsObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'wbsObjectId');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByWbsRoute() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'wbsRoute');
    });
  }

  QueryBuilder<ActivityDTO, ActivityDTO, QDistinct> distinctByWorkPackageId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'workPackageId');
    });
  }
}

extension ActivityDTOQueryProperty
    on QueryBuilder<ActivityDTO, ActivityDTO, QQueryProperty> {
  QueryBuilder<ActivityDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> activityProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'activity');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      actualFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'actualFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      actualStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'actualStartDate');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations>
      backendHasAttachmentsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'backendHasAttachments');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      budgetedLaborUnitsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'budgetedLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      budgetedNonLaborUnitsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'budgetedNonLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> calendarNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'calendarName');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations> changedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'changed');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      changedActualFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'changedActualFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      changedActualStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'changedActualStartDate');
    });
  }

  QueryBuilder<ActivityDTO, int?, QQueryOperations> commentCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'commentCount');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations> dataDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dataDate');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations> dateCreatedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateCreated');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      dateModifiedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateModified');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> descriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'description');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> displayDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'displayDate');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      displayFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'displayFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      dynamicFieldDataProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dynamicFieldData');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      dynamicFieldDateTimeDataProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dynamicFieldDateTimeData');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      dynamicFieldNumberDataProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dynamicFieldNumberData');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations> finishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'finishDate');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations> freeFloatProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'freeFloat');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> holderProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'holder');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> hostNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'hostName');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations> isDeletedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isDeleted');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      lastPercentCompleteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastPercentComplete');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations> manualDateSetProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'manualDateSet');
    });
  }

  QueryBuilder<ActivityDTO, int?, QQueryOperations> modifiedByIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'modifiedById');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      modifiedByNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'modifiedByName');
    });
  }

  QueryBuilder<ActivityDTO, int?, QQueryOperations> objectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectId');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      percentCompleteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'percentComplete');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      percentCompleteColorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'percentCompleteColor');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      plannedDurationProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'plannedDuration');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      plannedFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'plannedFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      plannedStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'plannedStartDate');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations> preApprovedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'preApproved');
    });
  }

  QueryBuilder<ActivityDTO, List<PredecessorActivityDTO>?, QQueryOperations>
      predecessorsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'predecessors');
    });
  }

  QueryBuilder<ActivityDTO, PrimaveraActivityStatus?, QQueryOperations>
      predecessorsStatusProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'predecessorsStatus');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> projectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'projectId');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      rejectedFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'rejectedFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      rejectedPercentCompleteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'rejectedPercentComplete');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      rejectedStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'rejectedStartDate');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      remainingDurationProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'remainingDuration');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      remainingEarlyFinishDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'remainingEarlyFinishDate');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      remainingEarlyStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'remainingEarlyStartDate');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      remainingLaborUnitsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'remainingLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations>
      remainingNonLaborUnitsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'remainingNonLaborUnits');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations>
      sentForPrimaveraSyncProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'sentForPrimaveraSync');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations> startDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'startDate');
    });
  }

  QueryBuilder<ActivityDTO, List<SuccessorsActivityDTO>?, QQueryOperations>
      successorsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'successors');
    });
  }

  QueryBuilder<ActivityDTO, PrimaveraActivityStatus?, QQueryOperations>
      successorsStatusProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'successorsStatus');
    });
  }

  QueryBuilder<ActivityDTO, double?, QQueryOperations> totalFloatProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'totalFloat');
    });
  }

  QueryBuilder<ActivityDTO, bool?, QQueryOperations> unableToWorkProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWork');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations>
      unableToWorkAdditionalInfoProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkAdditionalInfo');
    });
  }

  QueryBuilder<ActivityDTO, DateTime?, QQueryOperations>
      unableToWorkDateTimeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkDateTime');
    });
  }

  QueryBuilder<ActivityDTO, List<String>?, QQueryOperations>
      unableToWorkImageFilePathsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkImageFilePaths');
    });
  }

  QueryBuilder<ActivityDTO, List<int?>?, QQueryOperations>
      unableToWorkReasonsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unableToWorkReasons');
    });
  }

  QueryBuilder<ActivityDTO, String?, QQueryOperations> wbsNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'wbsName');
    });
  }

  QueryBuilder<ActivityDTO, int?, QQueryOperations> wbsObjectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'wbsObjectId');
    });
  }

  QueryBuilder<ActivityDTO, List<String>?, QQueryOperations>
      wbsRouteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'wbsRoute');
    });
  }

  QueryBuilder<ActivityDTO, int?, QQueryOperations> workPackageIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'workPackageId');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivityDTO _$ActivityDTOFromJson(Map<String, dynamic> json) => ActivityDTO(
      id: json['id'] as int?,
      activity: json['activity'] as String?,
      description: json['description'] as String?,
      remainingDuration: (json['remainingDuration'] as num?)?.toDouble(),
      percentComplete: (json['percentComplete'] as num?)?.toDouble(),
      hostName: json['hostName'] as String?,
      projectId: json['projectId'] as String?,
      changed: json['changed'] as bool?,
      wbsObjectId: json['wbsObjectId'] as int?,
      percentCompleteColor: json['percentCompleteColor'] as String?,
      wbsRoute: (json['wbsRoute'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      objectId: json['objectId'] as int?,
      calendarName: json['calendarName'] as String?,
      totalFloat: (json['totalFloat'] as num?)?.toDouble(),
      plannedDuration: (json['plannedDuration'] as num?)?.toDouble(),
      sentForPrimaveraSync: json['sentForPrimaveraSync'] as bool?,
      commentCount: json['commentCount'] as int?,
      plannedStartDate: json['plannedStartDate'] == null
          ? null
          : DateTime.parse(json['plannedStartDate'] as String),
      plannedFinishDate: json['plannedFinishDate'] == null
          ? null
          : DateTime.parse(json['plannedFinishDate'] as String),
      remainingEarlyStartDate: json['remainingEarlyStartDate'] == null
          ? null
          : DateTime.parse(json['remainingEarlyStartDate'] as String),
      remainingEarlyFinishDate: json['remainingEarlyFinishDate'] == null
          ? null
          : DateTime.parse(json['remainingEarlyFinishDate'] as String),
      budgetedLaborUnits: (json['budgetedLaborUnits'] as num?)?.toDouble(),
      budgetedNonLaborUnits:
          (json['budgetedNonLaborUnits'] as num?)?.toDouble(),
      remainingLaborUnits: (json['remainingLaborUnits'] as num?)?.toDouble(),
      remainingNonLaborUnits:
          (json['remainingNonLaborUnits'] as num?)?.toDouble(),
      isDeleted: json['isDeleted'] as bool?,
      unableToWork: json['unableToWork'] as bool?,
      preApproved: json['preApproved'] as bool?,
      wbsName: json['wbsName'] as String?,
      freeFloat: (json['freeFloat'] as num?)?.toDouble(),
      modifiedById: json['modifiedById'] as int?,
      modifiedByName: json['modifiedByName'] as String?,
      dateModified: json['dateModified'] == null
          ? null
          : DateTime.parse(json['dateModified'] as String),
      unableToWorkDateTime: json['unableToWorkDateTime'] == null
          ? null
          : DateTime.parse(json['unableToWorkDateTime'] as String),
      unableToWorkReasons: (json['unableToWorkReasons'] as List<dynamic>?)
          ?.map((e) => e as int?)
          .toList(),
      unableToWorkAdditionalInfo: json['unableToWorkAdditionalInfo'] as String?,
      unableToWorkImageFilePaths:
          (json['unableToWorkImageFilePaths'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
      backendHasAttachments: json['backendHasAttachments'] as bool?,
      holder: json['holder'] as String?,
      dateCreated: json['dateCreated'] == null
          ? null
          : DateTime.parse(json['dateCreated'] as String),
      rejectedPercentComplete:
          (json['rejectedPercentComplete'] as num?)?.toDouble(),
      rejectedStartDate: json['rejectedStartDate'] == null
          ? null
          : DateTime.parse(json['rejectedStartDate'] as String),
      rejectedFinishDate: json['rejectedFinishDate'] == null
          ? null
          : DateTime.parse(json['rejectedFinishDate'] as String),
      predecessors: (json['predecessors'] as List<dynamic>?)
          ?.map(
              (e) => PredecessorActivityDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      successors: (json['successors'] as List<dynamic>?)
          ?.map(
              (e) => SuccessorsActivityDTO.fromJson(e as Map<String, dynamic>))
          .toList(),
      predecessorsStatus: $enumDecodeNullable(
          _$PrimaveraActivityStatusEnumMap, json['predecessorsStatus']),
      successorsStatus: $enumDecodeNullable(
          _$PrimaveraActivityStatusEnumMap, json['successorsStatus']),
      dynamicFieldData: json['dynamicFieldData'] as String?,
      dynamicFieldDateTimeData: json['dynamicFieldDateTimeData'] as String?,
      dynamicFieldNumberData: json['dynamicFieldNumberData'] as String?,
      displayDate: json['displayDate'] as String?,
      displayFinishDate: json['displayFinishDate'] as String?,
      manualDateSet: json['manualDateSet'] as bool?,
      lastPercentComplete: (json['lastPercentComplete'] as num?)?.toDouble(),
      workPackageId: json['workPackageId'] as int?,
      dataDate: json['dataDate'] == null
          ? null
          : DateTime.parse(json['dataDate'] as String),
    );

Map<String, dynamic> _$ActivityDTOToJson(ActivityDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'activity': instance.activity,
      'description': instance.description,
      'dateCreated': instance.dateCreated?.toIso8601String(),
      'remainingDuration': instance.remainingDuration,
      'percentComplete': instance.percentComplete,
      'hostName': instance.hostName,
      'projectId': instance.projectId,
      'dynamicFieldData': instance.dynamicFieldData,
      'dynamicFieldDateTimeData': instance.dynamicFieldDateTimeData,
      'dynamicFieldNumberData': instance.dynamicFieldNumberData,
      'changed': instance.changed,
      'wbsObjectId': instance.wbsObjectId,
      'percentCompleteColor': instance.percentCompleteColor,
      'wbsRoute': instance.wbsRoute,
      'objectId': instance.objectId,
      'calendarName': instance.calendarName,
      'totalFloat': instance.totalFloat,
      'plannedDuration': instance.plannedDuration,
      'sentForPrimaveraSync': instance.sentForPrimaveraSync,
      'commentCount': instance.commentCount,
      'plannedStartDate': instance.plannedStartDate?.toIso8601String(),
      'plannedFinishDate': instance.plannedFinishDate?.toIso8601String(),
      'remainingEarlyStartDate':
          instance.remainingEarlyStartDate?.toIso8601String(),
      'remainingEarlyFinishDate':
          instance.remainingEarlyFinishDate?.toIso8601String(),
      'budgetedLaborUnits': instance.budgetedLaborUnits,
      'budgetedNonLaborUnits': instance.budgetedNonLaborUnits,
      'remainingLaborUnits': instance.remainingLaborUnits,
      'remainingNonLaborUnits': instance.remainingNonLaborUnits,
      'isDeleted': instance.isDeleted,
      'unableToWork': instance.unableToWork,
      'preApproved': instance.preApproved,
      'wbsName': instance.wbsName,
      'freeFloat': instance.freeFloat,
      'modifiedById': instance.modifiedById,
      'modifiedByName': instance.modifiedByName,
      'dateModified': instance.dateModified?.toIso8601String(),
      'unableToWorkDateTime': instance.unableToWorkDateTime?.toIso8601String(),
      'unableToWorkReasons': instance.unableToWorkReasons,
      'unableToWorkAdditionalInfo': instance.unableToWorkAdditionalInfo,
      'unableToWorkImageFilePaths': instance.unableToWorkImageFilePaths,
      'backendHasAttachments': instance.backendHasAttachments,
      'holder': instance.holder,
      'rejectedPercentComplete': instance.rejectedPercentComplete,
      'rejectedStartDate': instance.rejectedStartDate?.toIso8601String(),
      'rejectedFinishDate': instance.rejectedFinishDate?.toIso8601String(),
      'predecessors': instance.predecessors,
      'successors': instance.successors,
      'predecessorsStatus':
          _$PrimaveraActivityStatusEnumMap[instance.predecessorsStatus],
      'successorsStatus':
          _$PrimaveraActivityStatusEnumMap[instance.successorsStatus],
      'manualDateSet': instance.manualDateSet,
      'lastPercentComplete': instance.lastPercentComplete,
      'displayDate': instance.displayDate,
      'displayFinishDate': instance.displayFinishDate,
      'workPackageId': instance.workPackageId,
      'dataDate': instance.dataDate?.toIso8601String(),
    };

const _$PrimaveraActivityStatusEnumMap = {
  PrimaveraActivityStatus.notStarted: 1,
  PrimaveraActivityStatus.inProgress: 2,
  PrimaveraActivityStatus.completed: 3,
  PrimaveraActivityStatus.unableToWork: 4,
};
