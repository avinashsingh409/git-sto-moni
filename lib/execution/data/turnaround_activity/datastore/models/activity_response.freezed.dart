// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'activity_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ActivityResponse _$ActivityResponseFromJson(Map<String, dynamic> json) {
  return _ActivityResponse.fromJson(json);
}

/// @nodoc
mixin _$ActivityResponse {
  Activities? get activities => throw _privateConstructorUsedError;
  List<ActivityFieldDTO>? get activityFields =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ActivityResponseCopyWith<ActivityResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityResponseCopyWith<$Res> {
  factory $ActivityResponseCopyWith(
          ActivityResponse value, $Res Function(ActivityResponse) then) =
      _$ActivityResponseCopyWithImpl<$Res, ActivityResponse>;
  @useResult
  $Res call({Activities? activities, List<ActivityFieldDTO>? activityFields});

  $ActivitiesCopyWith<$Res>? get activities;
}

/// @nodoc
class _$ActivityResponseCopyWithImpl<$Res, $Val extends ActivityResponse>
    implements $ActivityResponseCopyWith<$Res> {
  _$ActivityResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? activities = freezed,
    Object? activityFields = freezed,
  }) {
    return _then(_value.copyWith(
      activities: freezed == activities
          ? _value.activities
          : activities // ignore: cast_nullable_to_non_nullable
              as Activities?,
      activityFields: freezed == activityFields
          ? _value.activityFields
          : activityFields // ignore: cast_nullable_to_non_nullable
              as List<ActivityFieldDTO>?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ActivitiesCopyWith<$Res>? get activities {
    if (_value.activities == null) {
      return null;
    }

    return $ActivitiesCopyWith<$Res>(_value.activities!, (value) {
      return _then(_value.copyWith(activities: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ActivityResponseCopyWith<$Res>
    implements $ActivityResponseCopyWith<$Res> {
  factory _$$_ActivityResponseCopyWith(
          _$_ActivityResponse value, $Res Function(_$_ActivityResponse) then) =
      __$$_ActivityResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Activities? activities, List<ActivityFieldDTO>? activityFields});

  @override
  $ActivitiesCopyWith<$Res>? get activities;
}

/// @nodoc
class __$$_ActivityResponseCopyWithImpl<$Res>
    extends _$ActivityResponseCopyWithImpl<$Res, _$_ActivityResponse>
    implements _$$_ActivityResponseCopyWith<$Res> {
  __$$_ActivityResponseCopyWithImpl(
      _$_ActivityResponse _value, $Res Function(_$_ActivityResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? activities = freezed,
    Object? activityFields = freezed,
  }) {
    return _then(_$_ActivityResponse(
      activities: freezed == activities
          ? _value.activities
          : activities // ignore: cast_nullable_to_non_nullable
              as Activities?,
      activityFields: freezed == activityFields
          ? _value._activityFields
          : activityFields // ignore: cast_nullable_to_non_nullable
              as List<ActivityFieldDTO>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ActivityResponse implements _ActivityResponse {
  const _$_ActivityResponse(
      {required this.activities,
      required final List<ActivityFieldDTO>? activityFields})
      : _activityFields = activityFields;

  factory _$_ActivityResponse.fromJson(Map<String, dynamic> json) =>
      _$$_ActivityResponseFromJson(json);

  @override
  final Activities? activities;
  final List<ActivityFieldDTO>? _activityFields;
  @override
  List<ActivityFieldDTO>? get activityFields {
    final value = _activityFields;
    if (value == null) return null;
    if (_activityFields is EqualUnmodifiableListView) return _activityFields;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ActivityResponse(activities: $activities, activityFields: $activityFields)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ActivityResponse &&
            (identical(other.activities, activities) ||
                other.activities == activities) &&
            const DeepCollectionEquality()
                .equals(other._activityFields, _activityFields));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, activities,
      const DeepCollectionEquality().hash(_activityFields));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ActivityResponseCopyWith<_$_ActivityResponse> get copyWith =>
      __$$_ActivityResponseCopyWithImpl<_$_ActivityResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ActivityResponseToJson(
      this,
    );
  }
}

abstract class _ActivityResponse implements ActivityResponse {
  const factory _ActivityResponse(
          {required final Activities? activities,
          required final List<ActivityFieldDTO>? activityFields}) =
      _$_ActivityResponse;

  factory _ActivityResponse.fromJson(Map<String, dynamic> json) =
      _$_ActivityResponse.fromJson;

  @override
  Activities? get activities;
  @override
  List<ActivityFieldDTO>? get activityFields;
  @override
  @JsonKey(ignore: true)
  _$$_ActivityResponseCopyWith<_$_ActivityResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
