// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'activity_card_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ActivityCardItemModel {
  int get id => throw _privateConstructorUsedError;
  DateTime get startDate => throw _privateConstructorUsedError;
  DateTime get finishDate => throw _privateConstructorUsedError;
  double get displayedPercentComplete => throw _privateConstructorUsedError;
  String get activity => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  bool get unableToWork => throw _privateConstructorUsedError;
  List<PredecessorActivityDTO> get predecessors =>
      throw _privateConstructorUsedError;
  List<SuccessorsActivityDTO> get successors =>
      throw _privateConstructorUsedError;
  PrimaveraActivityStatus? get predecessorsStatus =>
      throw _privateConstructorUsedError;
  PrimaveraActivityStatus? get successorsStatus =>
      throw _privateConstructorUsedError;
  int? get commentCount => throw _privateConstructorUsedError;
  String? get displayDate => throw _privateConstructorUsedError;
  String? get displayFinishDate => throw _privateConstructorUsedError;
  String? get dynamicFieldValue => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ActivityCardItemModelCopyWith<ActivityCardItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActivityCardItemModelCopyWith<$Res> {
  factory $ActivityCardItemModelCopyWith(ActivityCardItemModel value,
          $Res Function(ActivityCardItemModel) then) =
      _$ActivityCardItemModelCopyWithImpl<$Res, ActivityCardItemModel>;
  @useResult
  $Res call(
      {int id,
      DateTime startDate,
      DateTime finishDate,
      double displayedPercentComplete,
      String activity,
      String description,
      bool unableToWork,
      List<PredecessorActivityDTO> predecessors,
      List<SuccessorsActivityDTO> successors,
      PrimaveraActivityStatus? predecessorsStatus,
      PrimaveraActivityStatus? successorsStatus,
      int? commentCount,
      String? displayDate,
      String? displayFinishDate,
      String? dynamicFieldValue});
}

/// @nodoc
class _$ActivityCardItemModelCopyWithImpl<$Res,
        $Val extends ActivityCardItemModel>
    implements $ActivityCardItemModelCopyWith<$Res> {
  _$ActivityCardItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? startDate = null,
    Object? finishDate = null,
    Object? displayedPercentComplete = null,
    Object? activity = null,
    Object? description = null,
    Object? unableToWork = null,
    Object? predecessors = null,
    Object? successors = null,
    Object? predecessorsStatus = freezed,
    Object? successorsStatus = freezed,
    Object? commentCount = freezed,
    Object? displayDate = freezed,
    Object? displayFinishDate = freezed,
    Object? dynamicFieldValue = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      startDate: null == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      finishDate: null == finishDate
          ? _value.finishDate
          : finishDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      displayedPercentComplete: null == displayedPercentComplete
          ? _value.displayedPercentComplete
          : displayedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double,
      activity: null == activity
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      unableToWork: null == unableToWork
          ? _value.unableToWork
          : unableToWork // ignore: cast_nullable_to_non_nullable
              as bool,
      predecessors: null == predecessors
          ? _value.predecessors
          : predecessors // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>,
      successors: null == successors
          ? _value.successors
          : successors // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>,
      predecessorsStatus: freezed == predecessorsStatus
          ? _value.predecessorsStatus
          : predecessorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      successorsStatus: freezed == successorsStatus
          ? _value.successorsStatus
          : successorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      commentCount: freezed == commentCount
          ? _value.commentCount
          : commentCount // ignore: cast_nullable_to_non_nullable
              as int?,
      displayDate: freezed == displayDate
          ? _value.displayDate
          : displayDate // ignore: cast_nullable_to_non_nullable
              as String?,
      displayFinishDate: freezed == displayFinishDate
          ? _value.displayFinishDate
          : displayFinishDate // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicFieldValue: freezed == dynamicFieldValue
          ? _value.dynamicFieldValue
          : dynamicFieldValue // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ActivityCardItemModelCopyWith<$Res>
    implements $ActivityCardItemModelCopyWith<$Res> {
  factory _$$_ActivityCardItemModelCopyWith(_$_ActivityCardItemModel value,
          $Res Function(_$_ActivityCardItemModel) then) =
      __$$_ActivityCardItemModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      DateTime startDate,
      DateTime finishDate,
      double displayedPercentComplete,
      String activity,
      String description,
      bool unableToWork,
      List<PredecessorActivityDTO> predecessors,
      List<SuccessorsActivityDTO> successors,
      PrimaveraActivityStatus? predecessorsStatus,
      PrimaveraActivityStatus? successorsStatus,
      int? commentCount,
      String? displayDate,
      String? displayFinishDate,
      String? dynamicFieldValue});
}

/// @nodoc
class __$$_ActivityCardItemModelCopyWithImpl<$Res>
    extends _$ActivityCardItemModelCopyWithImpl<$Res, _$_ActivityCardItemModel>
    implements _$$_ActivityCardItemModelCopyWith<$Res> {
  __$$_ActivityCardItemModelCopyWithImpl(_$_ActivityCardItemModel _value,
      $Res Function(_$_ActivityCardItemModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? startDate = null,
    Object? finishDate = null,
    Object? displayedPercentComplete = null,
    Object? activity = null,
    Object? description = null,
    Object? unableToWork = null,
    Object? predecessors = null,
    Object? successors = null,
    Object? predecessorsStatus = freezed,
    Object? successorsStatus = freezed,
    Object? commentCount = freezed,
    Object? displayDate = freezed,
    Object? displayFinishDate = freezed,
    Object? dynamicFieldValue = freezed,
  }) {
    return _then(_$_ActivityCardItemModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      startDate: null == startDate
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      finishDate: null == finishDate
          ? _value.finishDate
          : finishDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      displayedPercentComplete: null == displayedPercentComplete
          ? _value.displayedPercentComplete
          : displayedPercentComplete // ignore: cast_nullable_to_non_nullable
              as double,
      activity: null == activity
          ? _value.activity
          : activity // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      unableToWork: null == unableToWork
          ? _value.unableToWork
          : unableToWork // ignore: cast_nullable_to_non_nullable
              as bool,
      predecessors: null == predecessors
          ? _value._predecessors
          : predecessors // ignore: cast_nullable_to_non_nullable
              as List<PredecessorActivityDTO>,
      successors: null == successors
          ? _value._successors
          : successors // ignore: cast_nullable_to_non_nullable
              as List<SuccessorsActivityDTO>,
      predecessorsStatus: freezed == predecessorsStatus
          ? _value.predecessorsStatus
          : predecessorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      successorsStatus: freezed == successorsStatus
          ? _value.successorsStatus
          : successorsStatus // ignore: cast_nullable_to_non_nullable
              as PrimaveraActivityStatus?,
      commentCount: freezed == commentCount
          ? _value.commentCount
          : commentCount // ignore: cast_nullable_to_non_nullable
              as int?,
      displayDate: freezed == displayDate
          ? _value.displayDate
          : displayDate // ignore: cast_nullable_to_non_nullable
              as String?,
      displayFinishDate: freezed == displayFinishDate
          ? _value.displayFinishDate
          : displayFinishDate // ignore: cast_nullable_to_non_nullable
              as String?,
      dynamicFieldValue: freezed == dynamicFieldValue
          ? _value.dynamicFieldValue
          : dynamicFieldValue // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ActivityCardItemModel implements _ActivityCardItemModel {
  const _$_ActivityCardItemModel(
      {required this.id,
      required this.startDate,
      required this.finishDate,
      required this.displayedPercentComplete,
      required this.activity,
      required this.description,
      required this.unableToWork,
      required final List<PredecessorActivityDTO> predecessors,
      required final List<SuccessorsActivityDTO> successors,
      required this.predecessorsStatus,
      required this.successorsStatus,
      required this.commentCount,
      required this.displayDate,
      required this.displayFinishDate,
      required this.dynamicFieldValue})
      : _predecessors = predecessors,
        _successors = successors;

  @override
  final int id;
  @override
  final DateTime startDate;
  @override
  final DateTime finishDate;
  @override
  final double displayedPercentComplete;
  @override
  final String activity;
  @override
  final String description;
  @override
  final bool unableToWork;
  final List<PredecessorActivityDTO> _predecessors;
  @override
  List<PredecessorActivityDTO> get predecessors {
    if (_predecessors is EqualUnmodifiableListView) return _predecessors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_predecessors);
  }

  final List<SuccessorsActivityDTO> _successors;
  @override
  List<SuccessorsActivityDTO> get successors {
    if (_successors is EqualUnmodifiableListView) return _successors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_successors);
  }

  @override
  final PrimaveraActivityStatus? predecessorsStatus;
  @override
  final PrimaveraActivityStatus? successorsStatus;
  @override
  final int? commentCount;
  @override
  final String? displayDate;
  @override
  final String? displayFinishDate;
  @override
  final String? dynamicFieldValue;

  @override
  String toString() {
    return 'ActivityCardItemModel(id: $id, startDate: $startDate, finishDate: $finishDate, displayedPercentComplete: $displayedPercentComplete, activity: $activity, description: $description, unableToWork: $unableToWork, predecessors: $predecessors, successors: $successors, predecessorsStatus: $predecessorsStatus, successorsStatus: $successorsStatus, commentCount: $commentCount, displayDate: $displayDate, displayFinishDate: $displayFinishDate, dynamicFieldValue: $dynamicFieldValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ActivityCardItemModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.finishDate, finishDate) ||
                other.finishDate == finishDate) &&
            (identical(
                    other.displayedPercentComplete, displayedPercentComplete) ||
                other.displayedPercentComplete == displayedPercentComplete) &&
            (identical(other.activity, activity) ||
                other.activity == activity) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.unableToWork, unableToWork) ||
                other.unableToWork == unableToWork) &&
            const DeepCollectionEquality()
                .equals(other._predecessors, _predecessors) &&
            const DeepCollectionEquality()
                .equals(other._successors, _successors) &&
            (identical(other.predecessorsStatus, predecessorsStatus) ||
                other.predecessorsStatus == predecessorsStatus) &&
            (identical(other.successorsStatus, successorsStatus) ||
                other.successorsStatus == successorsStatus) &&
            (identical(other.commentCount, commentCount) ||
                other.commentCount == commentCount) &&
            (identical(other.displayDate, displayDate) ||
                other.displayDate == displayDate) &&
            (identical(other.displayFinishDate, displayFinishDate) ||
                other.displayFinishDate == displayFinishDate) &&
            (identical(other.dynamicFieldValue, dynamicFieldValue) ||
                other.dynamicFieldValue == dynamicFieldValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      startDate,
      finishDate,
      displayedPercentComplete,
      activity,
      description,
      unableToWork,
      const DeepCollectionEquality().hash(_predecessors),
      const DeepCollectionEquality().hash(_successors),
      predecessorsStatus,
      successorsStatus,
      commentCount,
      displayDate,
      displayFinishDate,
      dynamicFieldValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ActivityCardItemModelCopyWith<_$_ActivityCardItemModel> get copyWith =>
      __$$_ActivityCardItemModelCopyWithImpl<_$_ActivityCardItemModel>(
          this, _$identity);
}

abstract class _ActivityCardItemModel implements ActivityCardItemModel {
  const factory _ActivityCardItemModel(
      {required final int id,
      required final DateTime startDate,
      required final DateTime finishDate,
      required final double displayedPercentComplete,
      required final String activity,
      required final String description,
      required final bool unableToWork,
      required final List<PredecessorActivityDTO> predecessors,
      required final List<SuccessorsActivityDTO> successors,
      required final PrimaveraActivityStatus? predecessorsStatus,
      required final PrimaveraActivityStatus? successorsStatus,
      required final int? commentCount,
      required final String? displayDate,
      required final String? displayFinishDate,
      required final String? dynamicFieldValue}) = _$_ActivityCardItemModel;

  @override
  int get id;
  @override
  DateTime get startDate;
  @override
  DateTime get finishDate;
  @override
  double get displayedPercentComplete;
  @override
  String get activity;
  @override
  String get description;
  @override
  bool get unableToWork;
  @override
  List<PredecessorActivityDTO> get predecessors;
  @override
  List<SuccessorsActivityDTO> get successors;
  @override
  PrimaveraActivityStatus? get predecessorsStatus;
  @override
  PrimaveraActivityStatus? get successorsStatus;
  @override
  int? get commentCount;
  @override
  String? get displayDate;
  @override
  String? get displayFinishDate;
  @override
  String? get dynamicFieldValue;
  @override
  @JsonKey(ignore: true)
  _$$_ActivityCardItemModelCopyWith<_$_ActivityCardItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}
