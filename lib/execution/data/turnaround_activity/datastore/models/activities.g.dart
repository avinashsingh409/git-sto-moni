// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activities.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Activities _$$_ActivitiesFromJson(Map<String, dynamic> json) =>
    _$_Activities(
      totalCount: json['totalCount'] as int,
      page: json['page'] as int,
      limit: json['limit'] as int,
      results: (json['results'] as List<dynamic>?)
          ?.map((e) => Activity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ActivitiesToJson(_$_Activities instance) =>
    <String, dynamic>{
      'totalCount': instance.totalCount,
      'page': instance.page,
      'limit': instance.limit,
      'results': instance.results,
    };
