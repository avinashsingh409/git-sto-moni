import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/activity_field_dao.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_response.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/turnaround_activity_api_service.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/turnaround_activity_dao.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/updated_turnaround_activity_dao.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/datastore/filter_config_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/datastore/stack_filter_datastore.dart';

import 'package:sto_mobile_v2/execution/domain/turnaround_activity/datastore/turnaround_activity_datastore.dart';

@LazySingleton(as: ITurnAroundActivityDataStore)
class TurnAroundActivityDataStoreImpl extends ITurnAroundActivityDataStore {
  final TurnAroundActivityApiService turnAroundActivityApiService;
  final TurnAroundActivityDAO activityDAO;
  final IFilterConfigDataStore filterConfigDataStore;
  final UpdatedTurnAroundActivityDAO updatedTurnAroundActivityDAO;
  final ActivityFieldDAO activityFieldDAO;
  final IStackFilterDataStore stackFilterDataStore;

  TurnAroundActivityDataStoreImpl(
    this.turnAroundActivityApiService, 
    this.activityDAO, 
    this.updatedTurnAroundActivityDAO,
    this.activityFieldDAO, 
    this.filterConfigDataStore,
    this.stackFilterDataStore
  );

  @override
  Future<ActivityResponse> getOnlineTurnAroundActivities({required int index, required int limit}) {
    return turnAroundActivityApiService.getTurnAroundActivities(index: index, limit: limit).catchDioException();
  }

  @override
  Future<void> insertAllActivities({required List<ActivityDTO> turnAroundDTOs}) {
    return activityDAO.upsertAllActivities(turnAroundDTOs: turnAroundDTOs);
  }

  @override
  Future<void> deleteTurnAroudActivities() {
    return activityDAO.deleteTurnAroudActivities();
  }

  @override
  Future<BehaviorSubject<List<ActivityDTO>>> getHomeScreenActivities({
    required List<FilterTypeEnum> staticFilters,
    required List<int> stackFilters,
    required String searchTerm,
  }) async {
    // if the look ahead filter is applied, get its configuration
    final filterConfig = staticFilters.contains(FilterTypeEnum.lookahead) ? await filterConfigDataStore.getLookaheadHours() : null;

    // get the list of activity IDs that correspond to each stack filter
    var stackFilterActivityIds = await Future.wait(stackFilters.map((filterId) async {
      final list = await stackFilterDataStore.getActivitiesByFilter(filterId: filterId) ?? [];
      return list;
    }));
    return activityDAO.getHomeScreenActivities(
      staticFilters: staticFilters, 
      stackFilterActivityIds: stackFilterActivityIds,
      filterConfig: filterConfig,
      searchTerm: searchTerm,
    );
  }

  @override
  Future<ActivityDTO?> getActivityById({required int activityId}) {
    return activityDAO.getActivity(activityId: activityId);
  }

  @override
  Future<void> updateActivity({required ActivityDTO updatedActivity}) async {
    await activityDAO.updateActivity(updatedActivity: updatedActivity);
  }

  @override
  Future<void> cacheActivityInTransactionsTable({required UpdatedActivityDTO updatedActivityDTO}) async {
    await updatedTurnAroundActivityDAO.updateActivity(updatedActivity: updatedActivityDTO);
  }

  @override
  Future<ActivityDTO?> getSwipeActivity({required int activityId}) {
    return activityDAO.getTurnAroundActivityById(id: activityId);
  }

  @override
  BehaviorSubject<List<ActivityDTO>> getWbsActivity({required String activity}) {
    return activityDAO.watchWbsActivities(activity: activity);
  }

  @override
  Future<List<UpdatedActivityDTO>> getAllUpdatedActivies() {
    return updatedTurnAroundActivityDAO.getAllTurnAroundActivities();
  }

  @override
  BehaviorSubject<bool> isSyncPending() {
    return updatedTurnAroundActivityDAO.isSyncPending();
  }

  @override
  Future<void> deleteAllCachedUpdatedActivies() {
    return updatedTurnAroundActivityDAO.deleteTurnAroudActivities();
  }

  @override
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent() {
    return updatedTurnAroundActivityDAO.getCacheStatusForLogoutAndChangingEvent();
  }

  @override
  Future<void> insertAllActivityFields({required List<ActivityFieldDTO> activityFields}) {
    return activityFieldDAO.upsertAllActivityFields(activityFieldDTOs: activityFields);
  }

  @override
  Future<void> deleteActivityFields() {
    return activityFieldDAO.deleteAllActivityFieldDTOs();
  }

  @override
  Future<List<ActivityFieldDTO>?> getActivityFields() {
    return activityFieldDAO.getAllActivityFieldDTOs();
  }

  @override
  Future<List<ActivityDTO>?> getActivitiesForGrouping() {
    return activityDAO.getAllTurnAroundActivities();
  }

  @override
  BehaviorSubject<List<ActivityDTO>> getGroupedActivities({required List<int> activityIds}) {
    return activityDAO.getGroupedActivities(activityIds: activityIds);
  }

  @override
  Future<ActivityDTO?> getActivityByWorkPackageId({required int workPackageId}) {
    return activityDAO.getActivityByWorkPackageId(workPackageId: workPackageId);
  }
}
