import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class ActivityFieldDAO {
  final isar = PersistenceModule.isar;

  /// Isar Collection of ActivityFieldDTO
  late IsarCollection<ActivityFieldDTO> collection = isar.activityFieldDTOs;

  /// Delete All ActivityFieldDTOs
  Future<void> deleteAllActivityFieldDTOs() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All ActivityFieldDTOs
  Future<List<ActivityFieldDTO>> getAllActivityFieldDTOs() async {
    final activityFieldDTOs = collection.where().findAll();
    return activityFieldDTOs;
  }

  Future<List<ActivityFieldDTO>?> getGroupableActivityFields() async {
    final groupingFields = await collection.where().findAll();
    groupingFields.sort((a, b) => a.field?.toLowerCase().compareTo(b.field?.toLowerCase() ?? '') ?? 0);
    return groupingFields;
  }

  /// Upsert All ActivityFieldDTOs
  Future<List<int>> upsertAllActivityFields({
    required List<ActivityFieldDTO> activityFieldDTOs,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(activityFieldDTOs);
    });
  }
}
