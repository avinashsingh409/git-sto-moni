import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';

@injectable
class ActivityFieldMapper {
  Future<Map<String, String>?> mapActivityFields(
      {required ActivityDTO activityDTO, required List<ActivityFieldDTO>? activityFields}) async {
    Map<String, String> fieldWithValues = {};
    Map<String, dynamic> combinedDynamicFieldValue = {};
    if (activityFields != null) {
      if (activityDTO.dynamicFieldData != null && activityDTO.dynamicFieldData != '') {
        combinedDynamicFieldValue.addAll(jsonDecode(activityDTO.dynamicFieldData ?? ''));
      }
      if (activityDTO.dynamicFieldDateTimeData != null && activityDTO.dynamicFieldDateTimeData != '') {
        combinedDynamicFieldValue.addAll(jsonDecode(activityDTO.dynamicFieldDateTimeData ?? ''));
      }
      if (activityDTO.dynamicFieldNumberData != null && activityDTO.dynamicFieldNumberData != '') {
        combinedDynamicFieldValue.addAll(jsonDecode(activityDTO.dynamicFieldNumberData ?? ''));
      }
      final activityJson = activityDTO.toJson();
      for (var fieldDTO in activityFields) {
        if (fieldDTO.fieldType != fieldTypeDynamic && fieldDTO.staticFieldName != "changedPercentComplete") {
          final String parsedFieldValue;
          switch (fieldDTO.staticFieldName) {
            case "percentComplete":
              parsedFieldValue = "${(activityJson[fieldDTO.staticFieldName] * 100).toStringAsFixed(0)}%";
              break;
            case "plannedDuration":
              parsedFieldValue = activityJson[fieldDTO.staticFieldName].toStringAsFixed(2);
              break;
            case "plannedStartDate":
              final date = DateTime.tryParse(activityJson[fieldDTO.staticFieldName]);
              parsedFieldValue = date != null ? date.toUtc().toString() : "N/A";
              break;
            case "plannedFinishDate":
              final date = DateTime.tryParse(activityJson[fieldDTO.staticFieldName]);
              parsedFieldValue = date != null ? date.toUtc().toString() : "N/A";
              break;
            default:
              parsedFieldValue =
                  activityJson[fieldDTO.staticFieldName] == null ? 'N/A' : activityJson[fieldDTO.staticFieldName].toString();
          }
          fieldWithValues[fieldDTO.field ?? 'N/A'] = parsedFieldValue;
        } else {
          final parsedFieldValue = combinedDynamicFieldValue[fieldDTO.dynamicFieldName].toString();
          fieldWithValues[fieldDTO.field ?? 'N/A'] = parsedFieldValue == "null" ? 'N/A' : parsedFieldValue;
        }
      }
    }
    if (activityDTO.displayDate != null) {
      fieldWithValues['Start Date'] = activityDTO.displayDate ?? "N/A";
    }
    if (activityDTO.displayFinishDate != null) {
      fieldWithValues['Finish Date'] = activityDTO.displayFinishDate ?? "N/A";
    }
    return fieldWithValues;
  }
}

const int fieldTypeMandatory = 2;
const int fieldTypeDynamic = 1;
const int fieldTypeStatic = 0;
