import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class TurnAroundActivityDAO {
  final isar = PersistenceModule.isar;

  // Isar Collection of TurnAroundActivities
  late IsarCollection<ActivityDTO> collection = isar.activityDTOs;

  // Delete all turnaround activities
  Future<void> deleteTurnAroudActivities() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  // Get all turnaround activities
  Future<List<ActivityDTO>> getAllTurnAroundActivities() async {
    final turnaroundActivities = collection.where().findAll();
    return turnaroundActivities;
  }

  // ----------------------------------------------------------------------------------------------------
  // Home Screen Filter, Sort, and Search
  // ----------------------------------------------------------------------------------------------------

  // Given every filter parameter from the home screen, return all applicable activities
  Future<BehaviorSubject<List<ActivityDTO>>> getHomeScreenActivities({
    required List<FilterTypeEnum> staticFilters,
    required List<List<int>> stackFilterActivityIds,
    required ExecutionFilterDTO? filterConfig,
    required String searchTerm,
  }) async {
    // initialize behavior subject and query
    final behaviorSubject = BehaviorSubject<List<ActivityDTO>>();
    var query = collection.where().filter();
    List<QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> Function(QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition>)> queryGroups = [];

    // filter by static filters
    if (staticFilters.contains(FilterTypeEnum.lookahead) && filterConfig != null) {
      final List<int> hoursOptions = [12, 24, 36, 48, 64, 72];
      final hours = hoursOptions[double.parse(filterConfig.value ?? '').toInt()];
      queryGroups.add((q) => _filterByLookahead(query: q, hours: hours, baseDate: filterConfig.baseDate!, rangeDate: filterConfig.rangeDate!));
    }
    if (staticFilters.contains(FilterTypeEnum.notCompleted)) {
      queryGroups.add((q) => _filterByNotCompleted(query: q));
    }
    if (staticFilters.contains(FilterTypeEnum.overdueActivities)) {
      queryGroups.add((q) => _filterByOverdue(query: q));
    }
    if (staticFilters.contains(FilterTypeEnum.criticalPath)) {
      queryGroups.add((q) => _filterByCriticalPath(query: q));
    }

    // filter by stack filters
    for (final activityIds in stackFilterActivityIds) {
      queryGroups.add((q) => _filterByStackFilter(query: q, activityIds: activityIds));
    }

    // apply search
    if (searchTerm.isNotEmpty) {
      queryGroups.add((q) => _searchByTerm(query: q, searchTerm: searchTerm));
    }

    // apply filter groups
    Query<ActivityDTO> finalQuery;
    if (queryGroups.isNotEmpty) {
      final lastIndex = queryGroups.length - 1;
      for (int i = 0; i < lastIndex; i++) {
        query = query.group(queryGroups[i]).and();
      }
      finalQuery = query.group(queryGroups[lastIndex]).build();
    } else {
      finalQuery = collection.where().build();
    }

    // convert to behavior subject
    final stream = finalQuery.watch(fireImmediately: true);
    behaviorSubject.addStream(stream);
    return behaviorSubject;
  }

  // ----------------------------------------------------------------------------------------------------
  // Filter Logic
  // ----------------------------------------------------------------------------------------------------

  // --------------------------------------------------
  // Lookahead & Helpers
  // --------------------------------------------------

  // Refine a query given a lookahead filter
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _filterByLookahead({
    required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query,
    required int hours,
    required LookaheadBaseDateEnum baseDate, 
    required LookaheadRangeDateEnum rangeDate
  }) {
    List<ActivityDTO> activities;
    switch (baseDate) {
      case LookaheadBaseDateEnum.dataDate:
        activities = _filterByDate(rangeDate, hours, false);
      case LookaheadBaseDateEnum.todaysDate:
        activities = _filterByDate(rangeDate, hours, true);
    }
    // the isar .anyOf() method starts with the entire query and refines it for each element of activities.
    // therefore, if activities is empty, it returns the entire query. this ternary is to subvert that logic.
    return activities.isEmpty ? query.idEqualTo(0) : query.anyOf(activities, (q, ActivityDTO activity) => q.idEqualTo(activity.id));
  }

  // based on the selected range date, filter activities
  List<ActivityDTO> _filterByDate(LookaheadRangeDateEnum rangeDate, int hours, bool today) {
    var activities = collection.where().findAllSync();
    var now = DateTime.now();
    switch (rangeDate) {
      case LookaheadRangeDateEnum.startDate:
        return activities.where((x) {
          // decide which date to use
          final actualBaseDate = today ? now : x.dataDate;
          return _includeActivity(actualBaseDate, x.startDate, hours);
        }).toList();
      case LookaheadRangeDateEnum.plannedStartDate:
        return activities.where((x) {
          // decide which date to use
          final actualBaseDate = today ? now : x.dataDate;
          return _includeActivity(actualBaseDate, x.plannedStartDate, hours);
        }).toList();
      case LookaheadRangeDateEnum.remainingEarlyStartDate:
        return activities.where((x) {
          // decide which date to use
          final actualBaseDate = today ? now : x.dataDate;
          return _includeActivity(actualBaseDate, x.remainingEarlyStartDate, hours);
        }).toList();
    }
  }

  // only include activities if the specified date is within the correct range of the given date
  bool _includeActivity(DateTime? baseDate, DateTime? rangeDate, int hours) {
    if (baseDate == null || rangeDate == null) return false;
    return rangeDate.isAfter(baseDate) && rangeDate.isBefore(baseDate.add(Duration(hours: hours)));
  }

  // --------------------------------------------------
  // Not Completed
  // --------------------------------------------------

  // Refine a query by incomplete activities
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _filterByNotCompleted({required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query}) {
    return query.not().percentCompleteEqualTo(1);
  }

  // --------------------------------------------------
  // Overdue
  // --------------------------------------------------

  // Refine a query by overdue activities
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _filterByOverdue({required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query}) {
    return query.totalFloatIsNotNull().and().totalFloatLessThan(0);
  }

  // --------------------------------------------------
  // Critical Path
  // --------------------------------------------------

  // Refine a query by critical path activities
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _filterByCriticalPath({required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query}) {
    return query.totalFloatIsNotNull().and().totalFloatEqualTo(0);
  }

  // --------------------------------------------------
  // Stack Filter
  // --------------------------------------------------

  // Refine a query given a stack filter
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _filterByStackFilter({required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query, required List<int> activityIds}) {
    // the isar .anyOf() method starts with the entire query and refines it for each element of activityIds.
    // therefore, if activityIds is empty, it returns the entire query. this ternary is to subvert that logic.
    return activityIds.isEmpty ? query.idEqualTo(0) : query.anyOf(activityIds, (q, int id) => q.idEqualTo(id));
  }

  // ----------------------------------------------------------------------------------------------------
  // Apply Search
  // ----------------------------------------------------------------------------------------------------

  // Apply a search term
  QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> _searchByTerm({required QueryBuilder<ActivityDTO, ActivityDTO, QFilterCondition> query, required String searchTerm}) {
    return query
      // description
      .descriptionContains(searchTerm, caseSensitive: false).or()
      .descriptionStartsWith(searchTerm, caseSensitive: false).or()
      .descriptionEndsWith(searchTerm, caseSensitive: false).or()
      .descriptionMatches(searchTerm, caseSensitive: false).or()
      // activity name
      .activityContains(searchTerm, caseSensitive: false).or()
      .activityStartsWith(searchTerm, caseSensitive: false).or()
      .activityEndsWith(searchTerm, caseSensitive: false).or()
      .activityMatches(searchTerm, caseSensitive: false).or()
      // dynamic field data
      .dynamicFieldDataContains(searchTerm, caseSensitive: false).or()
      .dynamicFieldDataStartsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldDataEndsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldDataMatches(searchTerm, caseSensitive: false).or()
      // dynamic datetime data
      .dynamicFieldDateTimeDataContains(searchTerm, caseSensitive: false).or()
      .dynamicFieldDateTimeDataStartsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldDateTimeDataEndsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldDateTimeDataMatches(searchTerm, caseSensitive: false).or()
      // dynamic number data
      .dynamicFieldNumberDataContains(searchTerm, caseSensitive: false).or()
      .dynamicFieldNumberDataStartsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldNumberDataEndsWith(searchTerm, caseSensitive: false).or()
      .dynamicFieldNumberDataMatches(searchTerm, caseSensitive: false);
  }

  // ----------------------------------------------------------------------------------------------------
  // Convert to BehaviorSubject
  // ----------------------------------------------------------------------------------------------------

  BehaviorSubject<List<ActivityDTO>> getActivitiesFromQuery(QueryBuilder<ActivityDTO, ActivityDTO, QAfterFilterCondition> query) {
    final behaviorStream = BehaviorSubject<List<ActivityDTO>>();
    final stream = query.build().watch(fireImmediately: true);
    behaviorStream.addStream(stream);
    return behaviorStream;
  }

  // ----------------------------------------------------------------------------------------------------

  Future<List<ActivityDTO>?> getAllRelationActivities({required List<String> activities}) async {
    final turnAroundActivites = collection
      .where()
      .filter()
      .anyOf(activities, (iterable, activity) => iterable.activityMatches(activity, caseSensitive: false))
      .findAll();
    return turnAroundActivites;
  }

  // Get turnaround activity by ID
  Future<ActivityDTO?> getTurnAroundActivityById({required int id}) async {
    final turnAroundActivity = collection.where().idEqualTo(id).findFirst();
    return turnAroundActivity;
  }

  // Get turnaround activities by a given search parameter
  BehaviorSubject<List<ActivityDTO>> getSearchedTurnAroundActivities({required String activityKeyword}) {
    final bstream = BehaviorSubject<List<ActivityDTO>>();
    final turnaroundActivities = collection
      .where()
      .filter()
      .descriptionContains(activityKeyword, caseSensitive: false)
      .or()
      .descriptionStartsWith(activityKeyword, caseSensitive: false)
      .or()
      .descriptionEndsWith(activityKeyword, caseSensitive: false)
      .or()
      .descriptionMatches(activityKeyword, caseSensitive: false)
      .or()
      //Activity id
      .activityContains(activityKeyword, caseSensitive: false)
      .or()
      .activityStartsWith(activityKeyword, caseSensitive: false)
      .or()
      .activityEndsWith(activityKeyword, caseSensitive: false)
      .or()
      .activityMatches(activityKeyword, caseSensitive: false)
      .or()
      //dynamicFieldData
      .dynamicFieldDataContains(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDataStartsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDataEndsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDataMatches(activityKeyword, caseSensitive: false)
      //dyamicDateTimeData
      .dynamicFieldDateTimeDataContains(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDateTimeDataStartsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDateTimeDataEndsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldDateTimeDataMatches(activityKeyword, caseSensitive: false)
      //dynamicNumberData
      .dynamicFieldNumberDataContains(
        activityKeyword,
        caseSensitive: false,
      )
      .or()
      .dynamicFieldNumberDataStartsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldNumberDataEndsWith(activityKeyword, caseSensitive: false)
      .or()
      .dynamicFieldNumberDataMatches(activityKeyword, caseSensitive: false)
      .build();
    final stream = turnaroundActivities.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  // Upsert all turnaround activities
  Future<List<int>> upsertAllActivities({
    required List<ActivityDTO> turnAroundDTOs,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(turnAroundDTOs);
    });
  }

  // Watch Activity
  Future<ActivityDTO?> getActivity({required int activityId}) {
    final data = collection.where().filter().idEqualTo(activityId).findFirst();
    return data;
  }

  // Watch All WBS Activities
  BehaviorSubject<List<ActivityDTO>> watchWbsActivities({required String activity}) {
    final bstream = BehaviorSubject<List<ActivityDTO>>();
    final query = collection.where().filter().activityEqualTo(activity, caseSensitive: false).build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  BehaviorSubject<List<ActivityDTO>> getGroupedActivities({required List<int> activityIds}) {
    final bstream = BehaviorSubject<List<ActivityDTO>>();
    final query = collection.where().filter().anyOf(activityIds, (iterable, id) => iterable.idEqualTo(id)).build();
    final stream = query.watch(fireImmediately: true);
    bstream.addStream(stream);
    return bstream;
  }

  // Update activity
  Future<void> updateActivity({required ActivityDTO updatedActivity}) async {
    await isar.writeTxn(() async {
      await collection.put(updatedActivity);
    });
  }

  // Get first activity by work package ID
  Future<ActivityDTO?> getActivityByWorkPackageId({required int workPackageId}) {
    return collection.where().filter().workPackageIdEqualTo(workPackageId).findFirst();
  }
}
