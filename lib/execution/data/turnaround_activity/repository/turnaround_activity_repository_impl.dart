import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_activity_exception.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_response.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/mappers/activity_field_mapper.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/domain/blocking_reason/repository/blocking_reason_repository.dart';

import 'package:sto_mobile_v2/execution/domain/comment/repository/turnaround_comment_repository.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/repository/execution_sync_repository.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/repository/stack_filter_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/datastore/turnaround_activity_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/repository/wbs_repository.dart';

@LazySingleton(as: ITurnAroundActivityRepository)
class TurnAroundActivityRepositoryImpl extends ITurnAroundActivityRepository {
  final ITurnAroundActivityDataStore turnAroundActivityDataStore;
  final IWbsRepository wbsRepository;
  final ITurnAroundEventRepository turnAroundEventRepository;
  final ITurnaroundActivitiesCommentRepository commentRepository;
  final IExecutionSyncRepository syncRepository;
  final IBlockingReasonRepository blockingReasonRepository;
  final ActivityFieldMapper activityFieldMapper;
  final IStackFilterRepository stackFilterRepository;

  TurnAroundActivityRepositoryImpl(
    this.turnAroundActivityDataStore,
    this.turnAroundEventRepository,
    this.commentRepository,
    this.wbsRepository,
    this.syncRepository,
    this.blockingReasonRepository,
    this.activityFieldMapper,
    this.stackFilterRepository,
  );

  @override
  Future<Either<Unit, TurnAroundActivityException>> getAndCacheExecutionData(
      {required TurnAroundEventDTO turnAroundEventDTO}) async {
    if (turnAroundEventDTO.primaveraProjectId == "N/A") {
      return const Right(TurnAroundActivityException.noP6ProjectAssigned());
    }
    //Syncing data first
    final pushDataResult = await syncRepository.pushDataToServer(turnaroundEventId: turnAroundEventDTO.id ?? 0);
    pushDataResult.fold((_) {
      // The flow continues
    }, (exception) {
      return const Right(TurnAroundActivityException.unexpectedError());
    });
    //Clearing data next
    clearExecutionCache();
    int index = 1;
    //Limit to 5000 for mobile scope.
    int limit = 200000;
    try {
      // Filters API
      // outside of the array to ensure the backend cache is populated before being accessed -- otherwise, race condition likely.
      final filterResponse = await stackFilterRepository.fetchStackFilters(
        hostname: turnAroundEventDTO.hostName ?? "",
        projectId: turnAroundEventDTO.primaveraProjectId ?? ""
      ) as List<StackFilterDTO>;
      await Future.wait([
        // Activities API
        turnAroundActivityDataStore.getOnlineTurnAroundActivities(
          index: index,
          limit: limit
        ),

        // Comments API
        commentRepository.getActivitiesComments(
          hostName: turnAroundEventDTO.hostName ?? "",
          projectId: turnAroundEventDTO.primaveraProjectId ?? "",
          index: index,
          limit: limit
        ),

        // WBS Style API
        wbsRepository.getWbsLevelsStyles(),

        // Blocking Reasons API
        blockingReasonRepository.getBlockingReasons(),
      ]).then((response) async {
        // Cache Stack Filters
        await stackFilterRepository.insertStackFilters(stackFilterDTOs: filterResponse);

        // Cache turnaround activities
        final activityResponse = response[0] as ActivityResponse;
        final activityResults = activityResponse.activities?.results;
        if (activityResults != null && activityResults.isNotEmpty) {
          final activityDTOs =
              activityResults.map((activityModel) => ActivityDTO.fromModel(activityModel: activityModel)).toList();
          await turnAroundActivityDataStore.insertAllActivities(turnAroundDTOs: activityDTOs);
          // Map and cache WBS
          await wbsRepository.getAndCacheWbsElements(activities: activityDTOs);
        }

        // Cache activity fields
        if (activityResponse.activityFields != null && activityResponse.activityFields!.isNotEmpty) {
          await turnAroundActivityDataStore.insertAllActivityFields(activityFields: activityResponse.activityFields ?? []);
        }

        // Cache comments from turnaround activites
        final commentResponse = response[1] as CommentResponse;
        final commentResults = commentResponse.results;
        if (commentResults != null && commentResults.isNotEmpty) {
          await commentRepository.cacheAllComments(commentDTOs: commentResults);
        }

        // Cache wbs level styles
        final wbslevelStyles = response[2] as List<WbsLevelStyleDTO>;
        await wbsRepository.insertAllWbsLevelStyles(wbsLevelsStyles: wbslevelStyles);

        // Cache blocking reasons
        final blockingReasons = response[3] as List<BlockingReasonDTO>;
        await blockingReasonRepository.insertBlockingReasons(blockingReasonDTOs: blockingReasons);
      });
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(TurnAroundActivityException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(TurnAroundActivityException.socketException());
        } else {
          return const Right(TurnAroundActivityException.unexpectedError());
        }
      }
      return const Right(TurnAroundActivityException.unexpectedError());
    } on ConnectionException catch (_) {
      return const Right(TurnAroundActivityException.socketException());
    }
    return const Left(unit);
  }

  Future<void> clearExecutionCache() async {
    // Clear filters
    await stackFilterRepository.deleteAllStackFilters();
    // Clear activities
    await turnAroundActivityDataStore.deleteTurnAroudActivities();
    await turnAroundActivityDataStore.deleteAllCachedUpdatedActivies();
    // Clear activity fields
    await turnAroundActivityDataStore.deleteActivityFields();
    // Clear comments
    await commentRepository.deleteAllComments();
    await commentRepository.deleteAllCachedComments();
    // Clear WBS
    await wbsRepository.deleteAllWbsItems();
    await wbsRepository.deleteAllWbsLevelStyles();
    // Clear blocking reasons
    await blockingReasonRepository.deleteAllBlockingReasons();
  }

  @override
  Future<BehaviorSubject<List<ActivityDTO>>> getHomeScreenActivities({
    required List<FilterTypeEnum> staticFilters,
    required List<int> stackFilters,
    required String searchTerm,
  }) {
    return turnAroundActivityDataStore.getHomeScreenActivities(
      staticFilters: staticFilters, 
      stackFilters: stackFilters,
      searchTerm: searchTerm,
    );
  }

  @override
  Future<ActivityDTO?> getActivityById({required int activityId}) {
    return turnAroundActivityDataStore.getActivityById(activityId: activityId);
  }

  @override
  Future<void> updateActivity({
    required ActivityDTO currentActivity,
    required UpdatedActivityDTO updateActivityModel,
  }) async {
    final updatedActivity = UpdatedActivityDTO(
      id: currentActivity.id!,
      percentComplete: updateActivityModel.percentComplete ?? (currentActivity.changed! ? currentActivity.percentComplete : null),
      unableToWork: updateActivityModel.unableToWork,
      unableToWorkDateTime: updateActivityModel.unableToWorkDateTime,
      unableToWorkReasons: updateActivityModel.unableToWorkReasons,
      unableToWorkAdditionalInfo: updateActivityModel.unableToWorkAdditionalInfo,
      dateModified: DateTime.now(),
      objectId: currentActivity.objectId,
      manualStartDate: updateActivityModel.manualStartDate ?? currentActivity.changedActualStartDate,
      manualDateSet: currentActivity.manualDateSet == true ? true : updateActivityModel.manualDateSet,
      commentCount: updateActivityModel.commentCount,
    );
    await turnAroundActivityDataStore.updateActivity(
      updatedActivity: currentActivity.copyWith(
        updatedActivityDTO: updatedActivity,
      ),
    );
    await turnAroundActivityDataStore.cacheActivityInTransactionsTable(updatedActivityDTO: updatedActivity);
  }

  @override
  Future<ActivityDTO?> getSwipeActivity({required int activityId}) {
    return turnAroundActivityDataStore.getSwipeActivity(activityId: activityId);
  }

  @override
  BehaviorSubject<List<ActivityDTO>> getWbsActivity({required String activity}) {
    return turnAroundActivityDataStore.getWbsActivity(activity: activity);
  }

  @override
  Future<Map<String, String>?> getActivityFieldsWithValue({required ActivityDTO activityDTO}) async {
    final activityFields = await turnAroundActivityDataStore.getActivityFields();
    return activityFieldMapper.mapActivityFields(activityDTO: activityDTO, activityFields: activityFields);
  }

  @override
  Future<List<ActivityDTO>?> getActivitiesForGrouping() {
    return turnAroundActivityDataStore.getActivitiesForGrouping();
  }

  @override
  BehaviorSubject<List<ActivityDTO>> getGroupedActivities({required String activityIds}) {
    List<String> activitiesIdsInLeaf = activityIds.split(',');
    List<int> listOfIds = [];

    if (activitiesIdsInLeaf != []) {
      listOfIds = activitiesIdsInLeaf.map(int.parse).toList();
    }
    return turnAroundActivityDataStore.getGroupedActivities(activityIds: listOfIds);
  }

  @override
  Future<ActivityDTO?> getActivityByWorkPackageId({required int workPackageId}) {
    return turnAroundActivityDataStore.getActivityByWorkPackageId(workPackageId: workPackageId);
  }
}
