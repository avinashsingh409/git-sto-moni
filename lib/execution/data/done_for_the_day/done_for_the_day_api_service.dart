import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_association.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';

@injectable
class DoneForTheDayApiService {
  final Dio dio;

  DoneForTheDayApiService(this.dio);

  Future<void> syncDoneForTheDay({required int turnaroundEventId, required bool isDoneForTheDay}) async {
    final response = await dio.get('${ApiEndPoints.getAssociations(eventId: turnaroundEventId)}');
    final jsonData = response.data;
    if (jsonData != null) {
      List<TurnAroundAssociation> turnaroundAssociations =
          List<TurnAroundAssociation>.from(response.data.map((model) => TurnAroundAssociation.fromJson(model)));
      final userResponse = await dio.get(ApiEndPoints.user);
      if (userResponse.data != null) {
        final userId = userResponse.data["id"];
        await Future.forEach(turnaroundAssociations, (element) async {
          if (element.userId == userId) {
            await dio.patch(
              '${ApiEndPoints.stoxTurnaroundEventsToggle(inProgress: !isDoneForTheDay)}',
              data: [element.id],
            );
          }
        });
      }
    }
  }
}
