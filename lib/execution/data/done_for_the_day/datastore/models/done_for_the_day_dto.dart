import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'done_for_the_day_dto.g.dart';

@collection
@JsonSerializable()
class DoneForTheDayDTO {
  Id? id;
  bool? isDoneForTheDay;
  DateTime? lastUpdatedDateTime;

  DoneForTheDayDTO({
    this.id,
    this.isDoneForTheDay,
    this.lastUpdatedDateTime,
  });

  factory DoneForTheDayDTO.fromJson(Map<String, dynamic> json) => _$DoneForTheDayDTOFromJson(json);
}
