// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'done_for_the_day_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetDoneForTheDayDTOCollection on Isar {
  IsarCollection<DoneForTheDayDTO> get doneForTheDayDTOs => this.collection();
}

const DoneForTheDayDTOSchema = CollectionSchema(
  name: r'DoneForTheDayDTO',
  id: -6000342396428053006,
  properties: {
    r'isDoneForTheDay': PropertySchema(
      id: 0,
      name: r'isDoneForTheDay',
      type: IsarType.bool,
    ),
    r'lastUpdatedDateTime': PropertySchema(
      id: 1,
      name: r'lastUpdatedDateTime',
      type: IsarType.dateTime,
    )
  },
  estimateSize: _doneForTheDayDTOEstimateSize,
  serialize: _doneForTheDayDTOSerialize,
  deserialize: _doneForTheDayDTODeserialize,
  deserializeProp: _doneForTheDayDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _doneForTheDayDTOGetId,
  getLinks: _doneForTheDayDTOGetLinks,
  attach: _doneForTheDayDTOAttach,
  version: '3.1.0+1',
);

int _doneForTheDayDTOEstimateSize(
  DoneForTheDayDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _doneForTheDayDTOSerialize(
  DoneForTheDayDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.isDoneForTheDay);
  writer.writeDateTime(offsets[1], object.lastUpdatedDateTime);
}

DoneForTheDayDTO _doneForTheDayDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = DoneForTheDayDTO(
    id: id,
    isDoneForTheDay: reader.readBoolOrNull(offsets[0]),
    lastUpdatedDateTime: reader.readDateTimeOrNull(offsets[1]),
  );
  return object;
}

P _doneForTheDayDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readDateTimeOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _doneForTheDayDTOGetId(DoneForTheDayDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _doneForTheDayDTOGetLinks(DoneForTheDayDTO object) {
  return [];
}

void _doneForTheDayDTOAttach(
    IsarCollection<dynamic> col, Id id, DoneForTheDayDTO object) {
  object.id = id;
}

extension DoneForTheDayDTOQueryWhereSort
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QWhere> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension DoneForTheDayDTOQueryWhere
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QWhereClause> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension DoneForTheDayDTOQueryFilter
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QFilterCondition> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      isDoneForTheDayIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isDoneForTheDay',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      isDoneForTheDayIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isDoneForTheDay',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      isDoneForTheDayEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isDoneForTheDay',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lastUpdatedDateTime',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lastUpdatedDateTime',
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastUpdatedDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastUpdatedDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastUpdatedDateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterFilterCondition>
      lastUpdatedDateTimeBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastUpdatedDateTime',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension DoneForTheDayDTOQueryObject
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QFilterCondition> {}

extension DoneForTheDayDTOQueryLinks
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QFilterCondition> {}

extension DoneForTheDayDTOQuerySortBy
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QSortBy> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      sortByIsDoneForTheDay() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDoneForTheDay', Sort.asc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      sortByIsDoneForTheDayDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDoneForTheDay', Sort.desc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      sortByLastUpdatedDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdatedDateTime', Sort.asc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      sortByLastUpdatedDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdatedDateTime', Sort.desc);
    });
  }
}

extension DoneForTheDayDTOQuerySortThenBy
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QSortThenBy> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      thenByIsDoneForTheDay() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDoneForTheDay', Sort.asc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      thenByIsDoneForTheDayDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isDoneForTheDay', Sort.desc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      thenByLastUpdatedDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdatedDateTime', Sort.asc);
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QAfterSortBy>
      thenByLastUpdatedDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdatedDateTime', Sort.desc);
    });
  }
}

extension DoneForTheDayDTOQueryWhereDistinct
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QDistinct> {
  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QDistinct>
      distinctByIsDoneForTheDay() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isDoneForTheDay');
    });
  }

  QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QDistinct>
      distinctByLastUpdatedDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastUpdatedDateTime');
    });
  }
}

extension DoneForTheDayDTOQueryProperty
    on QueryBuilder<DoneForTheDayDTO, DoneForTheDayDTO, QQueryProperty> {
  QueryBuilder<DoneForTheDayDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<DoneForTheDayDTO, bool?, QQueryOperations>
      isDoneForTheDayProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isDoneForTheDay');
    });
  }

  QueryBuilder<DoneForTheDayDTO, DateTime?, QQueryOperations>
      lastUpdatedDateTimeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastUpdatedDateTime');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoneForTheDayDTO _$DoneForTheDayDTOFromJson(Map<String, dynamic> json) =>
    DoneForTheDayDTO(
      id: json['id'] as int?,
      isDoneForTheDay: json['isDoneForTheDay'] as bool?,
      lastUpdatedDateTime: json['lastUpdatedDateTime'] == null
          ? null
          : DateTime.parse(json['lastUpdatedDateTime'] as String),
    );

Map<String, dynamic> _$DoneForTheDayDTOToJson(DoneForTheDayDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isDoneForTheDay': instance.isDoneForTheDay,
      'lastUpdatedDateTime': instance.lastUpdatedDateTime?.toIso8601String(),
    };
