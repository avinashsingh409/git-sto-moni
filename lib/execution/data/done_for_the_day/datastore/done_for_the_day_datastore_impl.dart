import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/datastore/models/done_for_the_day_dto.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/done_for_the_day_api_service.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/done_for_the_day_dao.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/datastore/done_for_the_day_datastore.dart';

@LazySingleton(as: IDoneForTheDayDataStore)
class DoneForTheDayDataStoreImpl extends IDoneForTheDayDataStore {
  final DoneForTheDayDAO doneForTheDayDAO;
  final DoneForTheDayApiService doneForTheDayApiService;

  DoneForTheDayDataStoreImpl(this.doneForTheDayDAO, this.doneForTheDayApiService);

  @override
  Future<int> intiailizeDoneForTheDay() {
    return doneForTheDayDAO.intiailizeDoneForTheDay();
  }

  @override
  Future<int> resetDoneForTheDay() {
    return doneForTheDayDAO.resetDoneForTheDay();
  }

  @override
  Future<int> updateDoneForTheDay({required bool isDoneForTheDay}) {
    return doneForTheDayDAO.upsertDoneForTheDay(isDoneForTheDay: isDoneForTheDay);
  }

  @override
  DoneForTheDayDTO? getDoneForTheDay() {
    return doneForTheDayDAO.getDoneForTheDay();
  }

  @override
  Future<void> syncDoneForTheDay({required int turnaroundEventId, required bool isDoneForTheDay}) {
    return doneForTheDayApiService
        .syncDoneForTheDay(turnaroundEventId: turnaroundEventId, isDoneForTheDay: isDoneForTheDay)
        .catchDioException();
  }
}
