import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/datastore/models/done_for_the_day_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class DoneForTheDayDAO {
  final isar = PersistenceModule.isar;

  /// Isar Collection of Done For the Day
  late IsarCollection<DoneForTheDayDTO> collection = isar.doneForTheDayDTOs;

  /// Get Done for the day
  DoneForTheDayDTO? getDoneForTheDay() {
    final doneForTheDayDTO = collection.getSync(1);
    return doneForTheDayDTO;
  }

  Future<int> intiailizeDoneForTheDay() {
    return isar.writeTxn(() {
      return collection.put(DoneForTheDayDTO(id: 1, isDoneForTheDay: false, lastUpdatedDateTime: DateTime.now()));
    });
  }

  // Upsert Done for the day
  Future<int> upsertDoneForTheDay({required bool isDoneForTheDay}) async {
    return isar.writeTxn(() {
      return collection.put(DoneForTheDayDTO(id: 1, isDoneForTheDay: isDoneForTheDay, lastUpdatedDateTime: DateTime.now()));
    });
  }

  Future<int> resetDoneForTheDay() {
    return isar.writeTxn(() {
      return collection.put(DoneForTheDayDTO(id: 1, isDoneForTheDay: false, lastUpdatedDateTime: DateTime.now()));
    });
  }

  // Delete Done for the day
  Future<void> deleteDoneForTheDay() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }
}
