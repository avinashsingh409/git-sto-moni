import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/datastore/models/done_for_the_day_dto.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/datastore/done_for_the_day_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart';

@LazySingleton(as: IDoneForTheDayRepository)
class DoneForTheDayRepositoryImpl extends IDoneForTheDayRepository {
  final IDoneForTheDayDataStore doneForTheDayDataStore;
  DoneForTheDayRepositoryImpl(this.doneForTheDayDataStore);

  @override
  Future<int> intiailizeDoneForTheDay() {
    return doneForTheDayDataStore.intiailizeDoneForTheDay();
  }

  @override
  Future<int> resetDoneForTheDay() {
    return doneForTheDayDataStore.resetDoneForTheDay();
  }

  @override
  Future<int> updateDoneForTheDay({required bool isDoneForTheDay}) {
    return doneForTheDayDataStore.updateDoneForTheDay(isDoneForTheDay: isDoneForTheDay);
  }

  @override
  DoneForTheDayDTO? getDoneForTheDay() {
    return doneForTheDayDataStore.getDoneForTheDay();
  }

  @override
  Future<void> syncDoneForTheDay({required int turnaroundEventId, required bool isDoneForTheDay}) {
    return doneForTheDayDataStore
        .syncDoneForTheDay(turnaroundEventId: turnaroundEventId, isDoneForTheDay: isDoneForTheDay)
        .catchDioException();
  }
}
