import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/activity_relation/datastore/activity_relations_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/activity_relation/repository/activity_relations_repository.dart';

@LazySingleton(as: IActivityRelationsRepository)
class ActivityRelationsRepositoryImpl extends IActivityRelationsRepository {
  final IActivityRelationsDataStore activityRelationsDataStore;
  ActivityRelationsRepositoryImpl(this.activityRelationsDataStore);

  @override
  Future<List<ActivityDTO>?> getPredecessorActivities({required List<String> activities}) async {
    return activityRelationsDataStore.getPredecessorActivities(activities: activities);
  }

  @override
  Future<List<ActivityDTO>?> getSuccessorActivities({required List<String> activities}) async {
    return activityRelationsDataStore.getSuccessorActivities(activities: activities);
  }
}
