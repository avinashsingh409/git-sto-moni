import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/turnaround_activity_dao.dart';
import 'package:sto_mobile_v2/execution/domain/activity_relation/datastore/activity_relations_datastore.dart';

@LazySingleton(as: IActivityRelationsDataStore)
class ActivityRelationsDataStoreImpl extends IActivityRelationsDataStore {
  final TurnAroundActivityDAO turnAroundActivityDAO;
  ActivityRelationsDataStoreImpl(this.turnAroundActivityDAO);

  @override
  Future<List<ActivityDTO>?> getPredecessorActivities({required List<String> activities}) async {
    return turnAroundActivityDAO.getAllRelationActivities(activities: activities);
  }

  @override
  Future<List<ActivityDTO>?> getSuccessorActivities({required List<String> activities}) async {
    return turnAroundActivityDAO.getAllRelationActivities(activities: activities);
  }
}
