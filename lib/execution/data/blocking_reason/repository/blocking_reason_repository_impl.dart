import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/domain/blocking_reason/datastore/blocking_reason_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/blocking_reason/repository/blocking_reason_repository.dart';

@LazySingleton(as: IBlockingReasonRepository)
class BlockingReasonRepositoryImpl extends IBlockingReasonRepository {
  final IBlockingReasonDataStore blockingReasonDataStore;
  BlockingReasonRepositoryImpl(this.blockingReasonDataStore);

  @override
  Future<List<BlockingReasonDTO>?> getBlockingReasons() {
    return blockingReasonDataStore.getBlockingReasons();
  }

  @override
  Future<void> insertBlockingReasons({required List<BlockingReasonDTO> blockingReasonDTOs}) {
    return blockingReasonDataStore.insertBlockingReasons(blockingReasonDTOs: blockingReasonDTOs);
  }

  @override
  Future<void> deleteAllBlockingReasons() {
    return blockingReasonDataStore.deleteAllBlockingReasons();
  }

  @override
  Future<List<BlockingReasonDTO>?> getCachedBlockingReasons() {
    return blockingReasonDataStore.getCachedBlockingReasons();
  }
}