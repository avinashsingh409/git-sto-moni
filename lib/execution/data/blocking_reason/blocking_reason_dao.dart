import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class BlockingReasonDAO {
  final isar = PersistenceModule.isar;
  BlockingReasonDAO();

  late IsarCollection<BlockingReasonDTO> collection = isar.blockingReasonDTOs;

  Future<List<int>> insertBlockingReasons({required List<BlockingReasonDTO> blockingReasonDTOs}) async {
    return isar.writeTxn(() async {
      return collection.putAll(blockingReasonDTOs);
    });
  }

  Future<void> deleteAllBlockingReasons() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  Future<List<BlockingReasonDTO>?> getCachedBlockingReasons() async {
    return collection.where().findAll();
  }
}