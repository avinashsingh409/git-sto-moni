import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/blocking_reason_api_service.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/blocking_reason_dao.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/domain/blocking_reason/datastore/blocking_reason_datastore.dart';

@LazySingleton(as: IBlockingReasonDataStore)
class BlockingReasonDataStoreImpl extends IBlockingReasonDataStore {
  final BlockingReasonApiService blockingReasonApiService;
  final BlockingReasonDAO blockingReasonDAO;
  BlockingReasonDataStoreImpl(this.blockingReasonApiService, this.blockingReasonDAO);

  @override
  Future<List<BlockingReasonDTO>?> getBlockingReasons() {
    return blockingReasonApiService.getBlockingReasons().catchDioException();
  }

  @override
  Future<void> insertBlockingReasons({required List<BlockingReasonDTO> blockingReasonDTOs}) {
    return blockingReasonDAO.insertBlockingReasons(blockingReasonDTOs: blockingReasonDTOs);
  }

  @override
  Future<void> deleteAllBlockingReasons() {
    return blockingReasonDAO.deleteAllBlockingReasons();
  }

  @override
  Future<List<BlockingReasonDTO>?> getCachedBlockingReasons() {
    return blockingReasonDAO.getCachedBlockingReasons();
  }
}
