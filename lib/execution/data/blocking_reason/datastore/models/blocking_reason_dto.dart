import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'blocking_reason_dto.g.dart';

@collection
@JsonSerializable()
class BlockingReasonDTO {
  Id? id;
  String? reasonText;
  bool? commentRequired;
  bool? stoExecutionUnable;

  BlockingReasonDTO({
    this.id,
    this.reasonText,
    this.commentRequired,
    this.stoExecutionUnable,
  });

  factory BlockingReasonDTO.fromJson(Map<String, dynamic> json) => _$BlockingReasonDTOFromJson(json);
}