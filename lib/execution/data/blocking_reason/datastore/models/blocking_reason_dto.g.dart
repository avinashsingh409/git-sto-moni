// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blocking_reason_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetBlockingReasonDTOCollection on Isar {
  IsarCollection<BlockingReasonDTO> get blockingReasonDTOs => this.collection();
}

const BlockingReasonDTOSchema = CollectionSchema(
  name: r'BlockingReasonDTO',
  id: -6322274863349452482,
  properties: {
    r'commentRequired': PropertySchema(
      id: 0,
      name: r'commentRequired',
      type: IsarType.bool,
    ),
    r'reasonText': PropertySchema(
      id: 1,
      name: r'reasonText',
      type: IsarType.string,
    ),
    r'stoExecutionUnable': PropertySchema(
      id: 2,
      name: r'stoExecutionUnable',
      type: IsarType.bool,
    )
  },
  estimateSize: _blockingReasonDTOEstimateSize,
  serialize: _blockingReasonDTOSerialize,
  deserialize: _blockingReasonDTODeserialize,
  deserializeProp: _blockingReasonDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _blockingReasonDTOGetId,
  getLinks: _blockingReasonDTOGetLinks,
  attach: _blockingReasonDTOAttach,
  version: '3.1.0+1',
);

int _blockingReasonDTOEstimateSize(
  BlockingReasonDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.reasonText;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _blockingReasonDTOSerialize(
  BlockingReasonDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.commentRequired);
  writer.writeString(offsets[1], object.reasonText);
  writer.writeBool(offsets[2], object.stoExecutionUnable);
}

BlockingReasonDTO _blockingReasonDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = BlockingReasonDTO(
    commentRequired: reader.readBoolOrNull(offsets[0]),
    id: id,
    reasonText: reader.readStringOrNull(offsets[1]),
    stoExecutionUnable: reader.readBoolOrNull(offsets[2]),
  );
  return object;
}

P _blockingReasonDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _blockingReasonDTOGetId(BlockingReasonDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _blockingReasonDTOGetLinks(
    BlockingReasonDTO object) {
  return [];
}

void _blockingReasonDTOAttach(
    IsarCollection<dynamic> col, Id id, BlockingReasonDTO object) {
  object.id = id;
}

extension BlockingReasonDTOQueryWhereSort
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QWhere> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension BlockingReasonDTOQueryWhere
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QWhereClause> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension BlockingReasonDTOQueryFilter
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QFilterCondition> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      commentRequiredIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'commentRequired',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      commentRequiredIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'commentRequired',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      commentRequiredEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentRequired',
        value: value,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'reasonText',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'reasonText',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'reasonText',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'reasonText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'reasonText',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'reasonText',
        value: '',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      reasonTextIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'reasonText',
        value: '',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      stoExecutionUnableIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'stoExecutionUnable',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      stoExecutionUnableIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'stoExecutionUnable',
      ));
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterFilterCondition>
      stoExecutionUnableEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'stoExecutionUnable',
        value: value,
      ));
    });
  }
}

extension BlockingReasonDTOQueryObject
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QFilterCondition> {}

extension BlockingReasonDTOQueryLinks
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QFilterCondition> {}

extension BlockingReasonDTOQuerySortBy
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QSortBy> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByCommentRequired() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentRequired', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByCommentRequiredDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentRequired', Sort.desc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByReasonText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'reasonText', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByReasonTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'reasonText', Sort.desc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByStoExecutionUnable() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stoExecutionUnable', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      sortByStoExecutionUnableDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stoExecutionUnable', Sort.desc);
    });
  }
}

extension BlockingReasonDTOQuerySortThenBy
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QSortThenBy> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByCommentRequired() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentRequired', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByCommentRequiredDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentRequired', Sort.desc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByReasonText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'reasonText', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByReasonTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'reasonText', Sort.desc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByStoExecutionUnable() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stoExecutionUnable', Sort.asc);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QAfterSortBy>
      thenByStoExecutionUnableDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stoExecutionUnable', Sort.desc);
    });
  }
}

extension BlockingReasonDTOQueryWhereDistinct
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QDistinct> {
  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QDistinct>
      distinctByCommentRequired() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'commentRequired');
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QDistinct>
      distinctByReasonText({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'reasonText', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QDistinct>
      distinctByStoExecutionUnable() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'stoExecutionUnable');
    });
  }
}

extension BlockingReasonDTOQueryProperty
    on QueryBuilder<BlockingReasonDTO, BlockingReasonDTO, QQueryProperty> {
  QueryBuilder<BlockingReasonDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<BlockingReasonDTO, bool?, QQueryOperations>
      commentRequiredProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'commentRequired');
    });
  }

  QueryBuilder<BlockingReasonDTO, String?, QQueryOperations>
      reasonTextProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'reasonText');
    });
  }

  QueryBuilder<BlockingReasonDTO, bool?, QQueryOperations>
      stoExecutionUnableProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'stoExecutionUnable');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BlockingReasonDTO _$BlockingReasonDTOFromJson(Map<String, dynamic> json) =>
    BlockingReasonDTO(
      id: json['id'] as int?,
      reasonText: json['reasonText'] as String?,
      commentRequired: json['commentRequired'] as bool?,
      stoExecutionUnable: json['stoExecutionUnable'] as bool?,
    );

Map<String, dynamic> _$BlockingReasonDTOToJson(BlockingReasonDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'reasonText': instance.reasonText,
      'commentRequired': instance.commentRequired,
      'stoExecutionUnable': instance.stoExecutionUnable,
    };
