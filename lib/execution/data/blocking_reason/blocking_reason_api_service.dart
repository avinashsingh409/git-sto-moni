import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';

@injectable
class BlockingReasonApiService {
  final Dio dio;
  BlockingReasonApiService(this.dio);

  Future<List<BlockingReasonDTO>?> getBlockingReasons() async {
    final response = await dio.get(ApiEndPoints.blockingReasons);
    if (response.data != null) {
      List<BlockingReasonDTO> results = List<BlockingReasonDTO>.from(response.data.map((model) => BlockingReasonDTO.fromJson(model)));
      return results;
    }
    return [];
  }
}