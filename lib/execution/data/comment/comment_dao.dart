import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class CommentDAO {
  final isar = PersistenceModule.isar;

  CommentDAO();

  /// Isar Collection of Comments
  late IsarCollection<CommentDTO> collection = isar.commentDTOs;

  /// Delete All Comments
  Future<void> deleteComments() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Upsert All Comments
  Future<List<int>> upsertAllComments({
    required List<CommentDTO> commentDTOs,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(commentDTOs);
    });
  }

  /// Watch All Comments
  Stream<List<CommentDTO>> watchAllComments({required int objectId}) {
    final query = collection.where().filter().objectIdEqualTo(objectId).sortByDateModifiedDesc().build();
    final stream = query.watch(fireImmediately: true);
    return stream;
  }

  /// Watch my Comments
  Stream<List<CommentDTO>> watchMyComments({required int objectId, required String username}) {
    final query =
        collection.where().filter().objectIdEqualTo(objectId).and().usernameEqualTo(username).sortByDateModifiedDesc().build();
    final stream = query.watch(fireImmediately: true);
    return stream;
  }

  /// Upsert One Comment
  Future<int> upsertComment({
    required CommentDTO commentDTO,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(commentDTO);
    });
  }
}
