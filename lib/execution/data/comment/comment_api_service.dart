import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';

@injectable
class CommentApiService {
  final Dio dio;

  CommentApiService(this.dio);

  Future<CommentResponse> getTurnAroundActivities(
      {required String hostName, required String projectId, required int index, required int limit}) async {
    final response = await dio.get(ApiEndPoints.comments(hostName, projectId, index, limit));
    final commentResponse = CommentResponse.fromJson(response.data);
    return commentResponse;
  }
}
