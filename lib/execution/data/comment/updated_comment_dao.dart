import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class UpdatedCommentDAO {
  final isar = PersistenceModule.isar;

  UpdatedCommentDAO();

  /// Isar Collection of Comments
  late IsarCollection<UpdatedCommentDTO> collection = isar.updatedCommentDTOs;

  /// Delete All Comments
  Future<void> deleteAllCachedComments() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Upsert One Comment
  Future<int> upsertComment({
    required UpdatedCommentDTO commentDTO,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(commentDTO);
    });
  }

  /// Get All UpdatedComments
  Future<List<UpdatedCommentDTO>> getAllUpdatedComments() async {
    final updatedComments = collection.where().findAll();
    return updatedComments;
  }
}
