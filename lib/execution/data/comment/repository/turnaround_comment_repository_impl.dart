import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/domain/comment/datastore/turnaround_comment_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/comment/repository/turnaround_comment_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';

@LazySingleton(as: ITurnaroundActivitiesCommentRepository)
class TurnaroundActivitiesCommentRepositoryImpl extends ITurnaroundActivitiesCommentRepository {
  final ITurnaroundActivitiesCommentDataStore commentDataStore;
  final ITurnAroundEventRepository turnAroundEventRepository;
  TurnaroundActivitiesCommentRepositoryImpl(this.commentDataStore, this.turnAroundEventRepository);

  @override
  Future<CommentResponse> getActivitiesComments(
      {required String hostName, required String projectId, required int index, required int limit}) {
    return commentDataStore.getComments(hostName: hostName, projectId: projectId, index: index, limit: limit);
  }

  @override
  Stream<List<CommentDTO>> getAllCachedComments({required int objectId}) {
    return commentDataStore.getAllCachedComments(objectId: objectId);
  }

  @override
  Stream<List<CommentDTO>> getMyCachedComments({required int objectId, required UserInformation userInformation}) {
    return commentDataStore.getMyCachedComments(objectId: objectId, username: userInformation.preferredUsername ?? "");
  }

  @override
  Future<void> createComment({
    required int objectId,
    required UserInformation userInformation,
    required String comment,
  }) async {
    final selectedTurnAroundEventId = turnAroundEventRepository.getSelectedTurnAroundEventId();
    selectedTurnAroundEventId.fold((selectedTurnAroundEventId) async {
      final event = await turnAroundEventRepository.getSelectedTurnAroundEvent(id: selectedTurnAroundEventId);
      await commentDataStore.createComment(
          commentDTO: CommentDTO(
        id: DateTime.now().microsecondsSinceEpoch,
        comment: comment,
        commentHtml: "<p> $comment </p>",
        commentJson: "{\"ops\":[{\"insert\":\"$comment\"}]}",
        dateModified: DateTime.now(),
        objectId: objectId,
        userFullName: userInformation.name,
        username: userInformation.preferredUsername,
        hostName: event!.hostName,
        projectId: event.primaveraProjectId,
      ));
      await commentDataStore.insertUpdatedComment(
          commentDTO: UpdatedCommentDTO(
        id: DateTime.now().microsecondsSinceEpoch,
        comment: comment,
        commentHtml: "<p> $comment </p>",
        commentJson: "{\"ops\":[{\"insert\":\"$comment\"}]}",
        dateModified: DateTime.now(),
        objectId: objectId,
        hostName: event.hostName,
        projectId: event.primaveraProjectId,
      ));

      //Todo add Exception here
    }, (_) => null);
  }

  @override
  Future<void> cacheAllComments({required List<CommentDTO> commentDTOs}) {
    return commentDataStore.cacheAllComments(commentDTOs: commentDTOs);
  }

  @override
  Future<void> deleteAllComments() {
    return commentDataStore.deleteAllComments();
  }

  @override
  Future<void> deleteAllCachedComments() {
    return commentDataStore.deleteAllCachedComments();
  }
}
