import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/comment/comment_api_service.dart';
import 'package:sto_mobile_v2/execution/data/comment/comment_dao.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/updated_comment_dao.dart';

import 'package:sto_mobile_v2/execution/domain/comment/datastore/turnaround_comment_datastore.dart';

@LazySingleton(as: ITurnaroundActivitiesCommentDataStore)
class TurnaroundActivitiesCommentDataStoreImpl extends ITurnaroundActivitiesCommentDataStore {
  final CommentApiService commentApiService;
  final CommentDAO commentDAO;
  final UpdatedCommentDAO updatedCommentDAO;

  TurnaroundActivitiesCommentDataStoreImpl(this.commentApiService, this.commentDAO, this.updatedCommentDAO);

  @override
  Future<CommentResponse> getComments(
      {required String hostName, required String projectId, required int index, required int limit}) {
    return commentApiService
        .getTurnAroundActivities(hostName: hostName, projectId: projectId, index: index, limit: limit)
        .catchDioException();
  }

  @override
  Future<void> cacheAllComments({required List<CommentDTO> commentDTOs}) {
    return commentDAO.upsertAllComments(commentDTOs: commentDTOs);
  }

  @override
  Future<void> deleteAllComments() {
    return commentDAO.deleteComments();
  }

  @override
  Future<void> createComment({required CommentDTO commentDTO}) {
    return commentDAO.upsertComment(commentDTO: commentDTO);
  }

  @override
  Stream<List<CommentDTO>> getAllCachedComments({required int objectId}) {
    return commentDAO.watchAllComments(objectId: objectId);
  }

  @override
  Stream<List<CommentDTO>> getMyCachedComments({required int objectId, required String username}) {
    return commentDAO.watchMyComments(username: username, objectId: objectId);
  }

  @override
  Future<void> insertUpdatedComment({required UpdatedCommentDTO commentDTO}) {
    return updatedCommentDAO.upsertComment(commentDTO: commentDTO);
  }

  @override
  Future<void> deleteAllCachedComments() {
    return updatedCommentDAO.deleteAllCachedComments();
  }

  @override
  Future<List<UpdatedCommentDTO>> getAllUpdatedComments() {
    return updatedCommentDAO.getAllUpdatedComments();
  }
}
