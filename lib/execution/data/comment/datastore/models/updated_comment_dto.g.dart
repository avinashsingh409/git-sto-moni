// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'updated_comment_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetUpdatedCommentDTOCollection on Isar {
  IsarCollection<UpdatedCommentDTO> get updatedCommentDTOs => this.collection();
}

const UpdatedCommentDTOSchema = CollectionSchema(
  name: r'UpdatedCommentDTO',
  id: 6484348552573622511,
  properties: {
    r'comment': PropertySchema(
      id: 0,
      name: r'comment',
      type: IsarType.string,
    ),
    r'commentHtml': PropertySchema(
      id: 1,
      name: r'commentHtml',
      type: IsarType.string,
    ),
    r'commentJson': PropertySchema(
      id: 2,
      name: r'commentJson',
      type: IsarType.string,
    ),
    r'dateModified': PropertySchema(
      id: 3,
      name: r'dateModified',
      type: IsarType.dateTime,
    ),
    r'hostName': PropertySchema(
      id: 4,
      name: r'hostName',
      type: IsarType.string,
    ),
    r'objectId': PropertySchema(
      id: 5,
      name: r'objectId',
      type: IsarType.long,
    ),
    r'objectType': PropertySchema(
      id: 6,
      name: r'objectType',
      type: IsarType.long,
    ),
    r'projectId': PropertySchema(
      id: 7,
      name: r'projectId',
      type: IsarType.string,
    )
  },
  estimateSize: _updatedCommentDTOEstimateSize,
  serialize: _updatedCommentDTOSerialize,
  deserialize: _updatedCommentDTODeserialize,
  deserializeProp: _updatedCommentDTODeserializeProp,
  idName: r'id',
  indexes: {
    r'dateModified': IndexSchema(
      id: 7664096291674774918,
      name: r'dateModified',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'dateModified',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _updatedCommentDTOGetId,
  getLinks: _updatedCommentDTOGetLinks,
  attach: _updatedCommentDTOAttach,
  version: '3.1.0+1',
);

int _updatedCommentDTOEstimateSize(
  UpdatedCommentDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.comment;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.commentHtml;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.commentJson;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.hostName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.projectId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _updatedCommentDTOSerialize(
  UpdatedCommentDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.comment);
  writer.writeString(offsets[1], object.commentHtml);
  writer.writeString(offsets[2], object.commentJson);
  writer.writeDateTime(offsets[3], object.dateModified);
  writer.writeString(offsets[4], object.hostName);
  writer.writeLong(offsets[5], object.objectId);
  writer.writeLong(offsets[6], object.objectType);
  writer.writeString(offsets[7], object.projectId);
}

UpdatedCommentDTO _updatedCommentDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = UpdatedCommentDTO(
    comment: reader.readStringOrNull(offsets[0]),
    commentHtml: reader.readStringOrNull(offsets[1]),
    commentJson: reader.readStringOrNull(offsets[2]),
    dateModified: reader.readDateTimeOrNull(offsets[3]),
    hostName: reader.readStringOrNull(offsets[4]),
    id: id,
    objectId: reader.readLongOrNull(offsets[5]),
    objectType: reader.readLongOrNull(offsets[6]),
    projectId: reader.readStringOrNull(offsets[7]),
  );
  return object;
}

P _updatedCommentDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _updatedCommentDTOGetId(UpdatedCommentDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _updatedCommentDTOGetLinks(
    UpdatedCommentDTO object) {
  return [];
}

void _updatedCommentDTOAttach(
    IsarCollection<dynamic> col, Id id, UpdatedCommentDTO object) {
  object.id = id;
}

extension UpdatedCommentDTOQueryWhereSort
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QWhere> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhere>
      anyDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'dateModified'),
      );
    });
  }
}

extension UpdatedCommentDTOQueryWhere
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QWhereClause> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'dateModified',
        value: [null],
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateModified',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedEqualTo(DateTime? dateModified) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'dateModified',
        value: [dateModified],
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedNotEqualTo(DateTime? dateModified) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateModified',
              lower: [],
              upper: [dateModified],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateModified',
              lower: [dateModified],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateModified',
              lower: [dateModified],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateModified',
              lower: [],
              upper: [dateModified],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedGreaterThan(
    DateTime? dateModified, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateModified',
        lower: [dateModified],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedLessThan(
    DateTime? dateModified, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateModified',
        lower: [],
        upper: [dateModified],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterWhereClause>
      dateModifiedBetween(
    DateTime? lowerDateModified,
    DateTime? upperDateModified, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateModified',
        lower: [lowerDateModified],
        includeLower: includeLower,
        upper: [upperDateModified],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension UpdatedCommentDTOQueryFilter
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QFilterCondition> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'comment',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'comment',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'comment',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'comment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'comment',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'comment',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'comment',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'commentHtml',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'commentHtml',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'commentHtml',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'commentHtml',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'commentHtml',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentHtml',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentHtmlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'commentHtml',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'commentJson',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'commentJson',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'commentJson',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'commentJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'commentJson',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'commentJson',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      commentJsonIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'commentJson',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dateModified',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateModified',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      dateModifiedBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateModified',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'hostName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'hostName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      hostNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectType',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectType',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectType',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectType',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectType',
        value: value,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      objectTypeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'projectId',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'projectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'projectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'projectId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'projectId',
        value: '',
      ));
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterFilterCondition>
      projectIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'projectId',
        value: '',
      ));
    });
  }
}

extension UpdatedCommentDTOQueryObject
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QFilterCondition> {}

extension UpdatedCommentDTOQueryLinks
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QFilterCondition> {}

extension UpdatedCommentDTOQuerySortBy
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QSortBy> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByComment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'comment', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByCommentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'comment', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByCommentHtml() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentHtml', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByCommentHtmlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentHtml', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByCommentJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentJson', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByCommentJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentJson', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByObjectType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectType', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByObjectTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectType', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      sortByProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.desc);
    });
  }
}

extension UpdatedCommentDTOQuerySortThenBy
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QSortThenBy> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByComment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'comment', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByCommentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'comment', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByCommentHtml() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentHtml', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByCommentHtmlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentHtml', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByCommentJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentJson', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByCommentJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'commentJson', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByDateModifiedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateModified', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByObjectType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectType', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByObjectTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectType', Sort.desc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.asc);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QAfterSortBy>
      thenByProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'projectId', Sort.desc);
    });
  }
}

extension UpdatedCommentDTOQueryWhereDistinct
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct> {
  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByComment({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'comment', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByCommentHtml({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'commentHtml', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByCommentJson({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'commentJson', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByDateModified() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateModified');
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByHostName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'hostName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectId');
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByObjectType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectType');
    });
  }

  QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QDistinct>
      distinctByProjectId({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'projectId', caseSensitive: caseSensitive);
    });
  }
}

extension UpdatedCommentDTOQueryProperty
    on QueryBuilder<UpdatedCommentDTO, UpdatedCommentDTO, QQueryProperty> {
  QueryBuilder<UpdatedCommentDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<UpdatedCommentDTO, String?, QQueryOperations> commentProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'comment');
    });
  }

  QueryBuilder<UpdatedCommentDTO, String?, QQueryOperations>
      commentHtmlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'commentHtml');
    });
  }

  QueryBuilder<UpdatedCommentDTO, String?, QQueryOperations>
      commentJsonProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'commentJson');
    });
  }

  QueryBuilder<UpdatedCommentDTO, DateTime?, QQueryOperations>
      dateModifiedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateModified');
    });
  }

  QueryBuilder<UpdatedCommentDTO, String?, QQueryOperations>
      hostNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'hostName');
    });
  }

  QueryBuilder<UpdatedCommentDTO, int?, QQueryOperations> objectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectId');
    });
  }

  QueryBuilder<UpdatedCommentDTO, int?, QQueryOperations> objectTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectType');
    });
  }

  QueryBuilder<UpdatedCommentDTO, String?, QQueryOperations>
      projectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'projectId');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdatedCommentDTO _$UpdatedCommentDTOFromJson(Map<String, dynamic> json) =>
    UpdatedCommentDTO(
      objectId: json['objectId'] as int?,
      comment: json['comment'] as String?,
      commentJson: json['commentJson'] as String?,
      commentHtml: json['commentHtml'] as String?,
      dateModified: json['dateModified'] == null
          ? null
          : DateTime.parse(json['dateModified'] as String),
      objectType: json['objectType'] as int? ?? 1,
      hostName: json['PrimaveraHostName'] as String?,
      projectId: json['projectId'] as String?,
    );

Map<String, dynamic> _$UpdatedCommentDTOToJson(UpdatedCommentDTO instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'comment': instance.comment,
      'commentJson': instance.commentJson,
      'commentHtml': instance.commentHtml,
      'dateModified': instance.dateModified?.toIso8601String(),
      'objectType': instance.objectType,
      'PrimaveraHostName': instance.hostName,
      'projectId': instance.projectId,
    };
