import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'updated_comment_dto.g.dart';

@collection
@JsonSerializable()
class UpdatedCommentDTO {
  @JsonKey(ignore: true)
  Id? id;
  int? objectId;
  String? comment;
  String? commentJson;
  String? commentHtml;
  @Index()
  DateTime? dateModified;
  int? objectType;

  @JsonKey(name: "PrimaveraHostName")
  String? hostName;
  String? projectId;

  UpdatedCommentDTO({
    this.id,
    this.objectId,
    this.comment,
    this.commentJson,
    this.commentHtml,
    this.dateModified,
    //Setting objectType to 1 since dart enum value is bad
    this.objectType = 1,
    this.hostName,
    this.projectId,
  });

  factory UpdatedCommentDTO.fromJson(Map<String, dynamic> json) => _$UpdatedCommentDTOFromJson(json);

  // Convert JSON to UpdateActivityRequestDTO
  Map<String, dynamic> toJson() => _$UpdatedCommentDTOToJson(this);
}
