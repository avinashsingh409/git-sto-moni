import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'comment_dto.g.dart';

@collection
@JsonSerializable()
class CommentDTO {
  Id? id;
  int? objectId;
  String? comment;
  String? commentJson;
  String? commentHtml;
  @Index()
  DateTime? dateModified;
  int? objectType;
  String? userFullName;
  String? username;
  @JsonKey(name: "primaveraHostName")
  String? hostName;
  String? projectId;

  CommentDTO({
    this.id,
    this.objectId,
    this.comment,
    this.commentJson,
    this.commentHtml,
    this.dateModified,
    this.objectType,
    this.userFullName,
    this.username,
    this.hostName,
    this.projectId,
  });

  factory CommentDTO.fromJson(Map<String, dynamic> json) => _$CommentDTOFromJson(json);
}
