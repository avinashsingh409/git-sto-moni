import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';

part 'comment_response.freezed.dart';
part 'comment_response.g.dart';

@freezed
abstract class CommentResponse with _$CommentResponse {
  const factory CommentResponse({
    required int totalCount,
    required int page,
    required int limit,
    required List<CommentDTO>? results,
  }) = _CommentResponse;

  factory CommentResponse.fromJson(Map<String, dynamic> json) => _$CommentResponseFromJson(json);
}
