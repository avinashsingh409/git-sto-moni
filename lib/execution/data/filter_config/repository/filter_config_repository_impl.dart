import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/datastore/filter_config_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/repository/filter_config_repository.dart';

@LazySingleton(as: IFilterConfigRepository)
class FilterConfigRepositoryImpl extends IFilterConfigRepository {
  final IFilterConfigDataStore filterConfigDataStore;
  FilterConfigRepositoryImpl(this.filterConfigDataStore);

  @override
  Future<List<int>> intializeFilters() {
    return filterConfigDataStore.intializeFilters();
  }

  @override
  Future<int> updateFilter({required String updatedVal, required FilterTypeEnum filterType}) {
    return filterConfigDataStore.updateFilter(updatedVal: updatedVal, filterType: filterType);
  }

  @override
  Future<List<ExecutionFilterDTO>> getPreConfiguredFilters() {
    return filterConfigDataStore.getPreConfiguredFilters();
  }

  @override
  Future<int> updateBaseDate({required LookaheadBaseDateEnum baseDate}) {
    return filterConfigDataStore.updateBaseDate(baseDate: baseDate);
  }

  @override
  Future<int> updateRangeDate({required LookaheadRangeDateEnum rangeDate}) {
    return filterConfigDataStore.updateRangeDate(rangeDate: rangeDate);
  }
}
