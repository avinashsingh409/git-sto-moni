enum LookaheadRangeDateEnum {
  startDate,
  plannedStartDate,
  remainingEarlyStartDate,
}

extension LookaheadRangeDate on LookaheadRangeDateEnum {
  String get title {
    switch (this) {
      case LookaheadRangeDateEnum.startDate:
        return "Start Date";
      case LookaheadRangeDateEnum.plannedStartDate:
        return "Planned Start Date";
      case LookaheadRangeDateEnum.remainingEarlyStartDate:
        return "Remaining Early Start Date";
    }
  }
}