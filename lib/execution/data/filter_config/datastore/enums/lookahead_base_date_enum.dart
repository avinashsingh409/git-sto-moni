enum LookaheadBaseDateEnum {
  todaysDate,
  dataDate,
}

extension LookaheadBaseDate on LookaheadBaseDateEnum {
  String get title {
    switch (this) {
      case LookaheadBaseDateEnum.todaysDate:
        return "Today's Date";
      case LookaheadBaseDateEnum.dataDate:
        return "Data Date";
    }
  }
}