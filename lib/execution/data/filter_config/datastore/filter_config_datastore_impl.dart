import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/execution_filter_dao.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/datastore/filter_config_datastore.dart';

@LazySingleton(as: IFilterConfigDataStore)
class FilterConfigDataStoreImpl extends IFilterConfigDataStore {
  final ExecutionFilterDAO executionFilterDAO;
  FilterConfigDataStoreImpl(this.executionFilterDAO);

  @override
  Future<List<int>> intializeFilters() {
    return executionFilterDAO.intializeFilters();
  }

  @override
  Future<List<int>> resetFilters() {
    return executionFilterDAO.resetFilters();
  }

  @override
  Future<int> updateFilter({required String updatedVal, required FilterTypeEnum filterType}) {
    return executionFilterDAO.updateFilter(updatedVal: updatedVal, filterType: filterType);
  }

  @override
  Future<List<ExecutionFilterDTO>> getPreConfiguredFilters() {
    return executionFilterDAO.getAllFilterConfigs();
  }

  @override
  Future<ExecutionFilterDTO?> getLookaheadHours() {
    return executionFilterDAO.getLookaheadHours();
  }

  @override
  Future<int> updateBaseDate({required LookaheadBaseDateEnum baseDate}) {
    return executionFilterDAO.updateBaseDate(baseDate: baseDate);
  }

  @override
  Future<int> updateRangeDate({required LookaheadRangeDateEnum rangeDate}) {
    return executionFilterDAO.updateRangeDate(rangeDate: rangeDate);
  }
}
