import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
part 'execution_filter_dto.g.dart';

@collection
@JsonSerializable()
class ExecutionFilterDTO {
  Id? id;
  String filterName;
  @Enumerated(EnumType.ordinal32)
  FilterTypeEnum filterType;
  String? value;
  @Enumerated(EnumType.ordinal32)
  LookaheadBaseDateEnum? baseDate;
  @Enumerated(EnumType.ordinal32)
  LookaheadRangeDateEnum? rangeDate;

  ExecutionFilterDTO({
    this.id,
    required this.filterType,
    required this.filterName,
    this.value,
    this.baseDate,
    this.rangeDate,
  });

  ExecutionFilterDTO copyWith({required String updatedValue}) {
    return ExecutionFilterDTO(
      id: id,
      filterType: filterType,
      filterName: filterName,
      value: updatedValue,
      baseDate: baseDate,
      rangeDate: rangeDate,
    );
  }

  factory ExecutionFilterDTO.fromJson(Map<String, dynamic> json) => _$ExecutionFilterDTOFromJson(json);
}
