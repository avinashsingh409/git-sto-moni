// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'execution_filter_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetExecutionFilterDTOCollection on Isar {
  IsarCollection<ExecutionFilterDTO> get executionFilterDTOs =>
      this.collection();
}

const ExecutionFilterDTOSchema = CollectionSchema(
  name: r'ExecutionFilterDTO',
  id: 6359844460801303223,
  properties: {
    r'baseDate': PropertySchema(
      id: 0,
      name: r'baseDate',
      type: IsarType.int,
      enumMap: _ExecutionFilterDTObaseDateEnumValueMap,
    ),
    r'filterName': PropertySchema(
      id: 1,
      name: r'filterName',
      type: IsarType.string,
    ),
    r'filterType': PropertySchema(
      id: 2,
      name: r'filterType',
      type: IsarType.int,
      enumMap: _ExecutionFilterDTOfilterTypeEnumValueMap,
    ),
    r'rangeDate': PropertySchema(
      id: 3,
      name: r'rangeDate',
      type: IsarType.int,
      enumMap: _ExecutionFilterDTOrangeDateEnumValueMap,
    ),
    r'value': PropertySchema(
      id: 4,
      name: r'value',
      type: IsarType.string,
    )
  },
  estimateSize: _executionFilterDTOEstimateSize,
  serialize: _executionFilterDTOSerialize,
  deserialize: _executionFilterDTODeserialize,
  deserializeProp: _executionFilterDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _executionFilterDTOGetId,
  getLinks: _executionFilterDTOGetLinks,
  attach: _executionFilterDTOAttach,
  version: '3.1.0+1',
);

int _executionFilterDTOEstimateSize(
  ExecutionFilterDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.filterName.length * 3;
  {
    final value = object.value;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _executionFilterDTOSerialize(
  ExecutionFilterDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeInt(offsets[0], object.baseDate?.index);
  writer.writeString(offsets[1], object.filterName);
  writer.writeInt(offsets[2], object.filterType.index);
  writer.writeInt(offsets[3], object.rangeDate?.index);
  writer.writeString(offsets[4], object.value);
}

ExecutionFilterDTO _executionFilterDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ExecutionFilterDTO(
    baseDate: _ExecutionFilterDTObaseDateValueEnumMap[
        reader.readIntOrNull(offsets[0])],
    filterName: reader.readString(offsets[1]),
    filterType: _ExecutionFilterDTOfilterTypeValueEnumMap[
            reader.readIntOrNull(offsets[2])] ??
        FilterTypeEnum.notCompleted,
    id: id,
    rangeDate: _ExecutionFilterDTOrangeDateValueEnumMap[
        reader.readIntOrNull(offsets[3])],
    value: reader.readStringOrNull(offsets[4]),
  );
  return object;
}

P _executionFilterDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (_ExecutionFilterDTObaseDateValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 1:
      return (reader.readString(offset)) as P;
    case 2:
      return (_ExecutionFilterDTOfilterTypeValueEnumMap[
              reader.readIntOrNull(offset)] ??
          FilterTypeEnum.notCompleted) as P;
    case 3:
      return (_ExecutionFilterDTOrangeDateValueEnumMap[
          reader.readIntOrNull(offset)]) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

const _ExecutionFilterDTObaseDateEnumValueMap = {
  'todaysDate': 0,
  'dataDate': 1,
};
const _ExecutionFilterDTObaseDateValueEnumMap = {
  0: LookaheadBaseDateEnum.todaysDate,
  1: LookaheadBaseDateEnum.dataDate,
};
const _ExecutionFilterDTOfilterTypeEnumValueMap = {
  'notCompleted': 0,
  'lookahead': 1,
  'overdueActivities': 2,
  'criticalPath': 3,
};
const _ExecutionFilterDTOfilterTypeValueEnumMap = {
  0: FilterTypeEnum.notCompleted,
  1: FilterTypeEnum.lookahead,
  2: FilterTypeEnum.overdueActivities,
  3: FilterTypeEnum.criticalPath,
};
const _ExecutionFilterDTOrangeDateEnumValueMap = {
  'startDate': 0,
  'plannedStartDate': 1,
  'remainingEarlyStartDate': 2,
};
const _ExecutionFilterDTOrangeDateValueEnumMap = {
  0: LookaheadRangeDateEnum.startDate,
  1: LookaheadRangeDateEnum.plannedStartDate,
  2: LookaheadRangeDateEnum.remainingEarlyStartDate,
};

Id _executionFilterDTOGetId(ExecutionFilterDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _executionFilterDTOGetLinks(
    ExecutionFilterDTO object) {
  return [];
}

void _executionFilterDTOAttach(
    IsarCollection<dynamic> col, Id id, ExecutionFilterDTO object) {
  object.id = id;
}

extension ExecutionFilterDTOQueryWhereSort
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QWhere> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension ExecutionFilterDTOQueryWhere
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QWhereClause> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ExecutionFilterDTOQueryFilter
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QFilterCondition> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'baseDate',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'baseDate',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateEqualTo(LookaheadBaseDateEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'baseDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateGreaterThan(
    LookaheadBaseDateEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'baseDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateLessThan(
    LookaheadBaseDateEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'baseDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      baseDateBetween(
    LookaheadBaseDateEnum? lower,
    LookaheadBaseDateEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'baseDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'filterName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'filterName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'filterName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'filterName',
        value: '',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'filterName',
        value: '',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterTypeEqualTo(FilterTypeEnum value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'filterType',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterTypeGreaterThan(
    FilterTypeEnum value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'filterType',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterTypeLessThan(
    FilterTypeEnum value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'filterType',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      filterTypeBetween(
    FilterTypeEnum lower,
    FilterTypeEnum upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'filterType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'rangeDate',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'rangeDate',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateEqualTo(LookaheadRangeDateEnum? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'rangeDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateGreaterThan(
    LookaheadRangeDateEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'rangeDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateLessThan(
    LookaheadRangeDateEnum? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'rangeDate',
        value: value,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      rangeDateBetween(
    LookaheadRangeDateEnum? lower,
    LookaheadRangeDateEnum? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'rangeDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'value',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'value',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: '',
      ));
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterFilterCondition>
      valueIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'value',
        value: '',
      ));
    });
  }
}

extension ExecutionFilterDTOQueryObject
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QFilterCondition> {}

extension ExecutionFilterDTOQueryLinks
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QFilterCondition> {}

extension ExecutionFilterDTOQuerySortBy
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QSortBy> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByBaseDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseDate', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByBaseDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseDate', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByFilterName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterName', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByFilterNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterName', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByFilterType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterType', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByFilterTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterType', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByRangeDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rangeDate', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByRangeDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rangeDate', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      sortByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }
}

extension ExecutionFilterDTOQuerySortThenBy
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QSortThenBy> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByBaseDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseDate', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByBaseDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseDate', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByFilterName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterName', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByFilterNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterName', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByFilterType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterType', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByFilterTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'filterType', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByRangeDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rangeDate', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByRangeDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'rangeDate', Sort.desc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByValue() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.asc);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QAfterSortBy>
      thenByValueDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'value', Sort.desc);
    });
  }
}

extension ExecutionFilterDTOQueryWhereDistinct
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct> {
  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct>
      distinctByBaseDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'baseDate');
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct>
      distinctByFilterName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'filterName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct>
      distinctByFilterType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'filterType');
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct>
      distinctByRangeDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'rangeDate');
    });
  }

  QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QDistinct>
      distinctByValue({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'value', caseSensitive: caseSensitive);
    });
  }
}

extension ExecutionFilterDTOQueryProperty
    on QueryBuilder<ExecutionFilterDTO, ExecutionFilterDTO, QQueryProperty> {
  QueryBuilder<ExecutionFilterDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<ExecutionFilterDTO, LookaheadBaseDateEnum?, QQueryOperations>
      baseDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'baseDate');
    });
  }

  QueryBuilder<ExecutionFilterDTO, String, QQueryOperations>
      filterNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'filterName');
    });
  }

  QueryBuilder<ExecutionFilterDTO, FilterTypeEnum, QQueryOperations>
      filterTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'filterType');
    });
  }

  QueryBuilder<ExecutionFilterDTO, LookaheadRangeDateEnum?, QQueryOperations>
      rangeDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'rangeDate');
    });
  }

  QueryBuilder<ExecutionFilterDTO, String?, QQueryOperations> valueProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'value');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExecutionFilterDTO _$ExecutionFilterDTOFromJson(Map<String, dynamic> json) =>
    ExecutionFilterDTO(
      id: json['id'] as int?,
      filterType: $enumDecode(_$FilterTypeEnumEnumMap, json['filterType']),
      filterName: json['filterName'] as String,
      value: json['value'] as String?,
      baseDate:
          $enumDecodeNullable(_$LookaheadBaseDateEnumEnumMap, json['baseDate']),
      rangeDate: $enumDecodeNullable(
          _$LookaheadRangeDateEnumEnumMap, json['rangeDate']),
    );

Map<String, dynamic> _$ExecutionFilterDTOToJson(ExecutionFilterDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'filterName': instance.filterName,
      'filterType': _$FilterTypeEnumEnumMap[instance.filterType]!,
      'value': instance.value,
      'baseDate': _$LookaheadBaseDateEnumEnumMap[instance.baseDate],
      'rangeDate': _$LookaheadRangeDateEnumEnumMap[instance.rangeDate],
    };

const _$FilterTypeEnumEnumMap = {
  FilterTypeEnum.notCompleted: 'notCompleted',
  FilterTypeEnum.lookahead: 'lookahead',
  FilterTypeEnum.overdueActivities: 'overdueActivities',
  FilterTypeEnum.criticalPath: 'criticalPath',
};

const _$LookaheadBaseDateEnumEnumMap = {
  LookaheadBaseDateEnum.todaysDate: 'todaysDate',
  LookaheadBaseDateEnum.dataDate: 'dataDate',
};

const _$LookaheadRangeDateEnumEnumMap = {
  LookaheadRangeDateEnum.startDate: 'startDate',
  LookaheadRangeDateEnum.plannedStartDate: 'plannedStartDate',
  LookaheadRangeDateEnum.remainingEarlyStartDate: 'remainingEarlyStartDate',
};
