import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class ExecutionFilterDAO {
  final isar = PersistenceModule.isar;

  /// Isar Collection of Execution Filter DTOs
  late IsarCollection<ExecutionFilterDTO> collection = isar.executionFilterDTOs;

  final List<ExecutionFilterDTO> initialFilters = [
    ExecutionFilterDTO(
      id: 1,
      filterName: 'Lookahead',
      filterType: FilterTypeEnum.lookahead,
      value: '2',
      baseDate: LookaheadBaseDateEnum.todaysDate,
      rangeDate: LookaheadRangeDateEnum.plannedStartDate,
    ),
  ];

  // Check filter initialization
  ExecutionFilterDTO? checkFilterInitialization() {
    final doneForTheDayDTO = collection.getSync(1);
    return doneForTheDayDTO;
  }

  Future<List<int>> intializeFilters() {
    return isar.writeTxn(() {
      return collection.putAll(initialFilters);
    });
  }

  Future<List<int>> resetFilters() {
    return isar.writeTxn(() {
      return collection.putAll(initialFilters);
    });
  }

  /// Get All filterConfigs
  Future<List<ExecutionFilterDTO>> getAllFilterConfigs() async {
    final filterConfigs = collection.where().findAll();
    return filterConfigs;
  }

  Future<ExecutionFilterDTO?> getLookaheadHours() async {
    final plannedStartDate = collection.where().filter().filterTypeEqualTo(FilterTypeEnum.lookahead).findFirst();
    return plannedStartDate;
  }

  Future<int> updateFilter({required String updatedVal, required FilterTypeEnum filterType}) async {
    final currentFilter = await getLookaheadHours();
    return isar.writeTxn(() {
      return collection.put(
        ExecutionFilterDTO(
          id: 1,
          filterName: 'Lookahead',
          filterType: FilterTypeEnum.lookahead,
          value: updatedVal,
          baseDate: currentFilter?.baseDate,
          rangeDate: currentFilter?.rangeDate,
        ),
      );
    });
  }

  Future<int> updateBaseDate({required LookaheadBaseDateEnum baseDate}) async {
    final currentFilter = await getLookaheadHours();
    return isar.writeTxn(() {
      return collection.put(
        ExecutionFilterDTO(
          id: 1,
          filterName: 'Lookahead',
          filterType: FilterTypeEnum.lookahead,
          value: currentFilter?.value,
          baseDate: baseDate,
          rangeDate: currentFilter?.rangeDate,
        )
      );
    });
  }

  Future<int> updateRangeDate({required LookaheadRangeDateEnum rangeDate}) async {
    final currentFilter = await getLookaheadHours();
    return isar.writeTxn(() {
      return collection.put(
        ExecutionFilterDTO(
          id: 1,
          filterName: 'Lookahead',
          filterType: FilterTypeEnum.lookahead,
          value: currentFilter?.value,
          baseDate: currentFilter?.baseDate,
          rangeDate: rangeDate,
        )
      );
    });
  }
}
