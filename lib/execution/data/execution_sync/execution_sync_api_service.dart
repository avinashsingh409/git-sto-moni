import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

@injectable
class ExecutionSyncApiService {
  final Dio dio;

  ExecutionSyncApiService(this.dio);

  Future<void> pushActivitiesToServer(
      {required List<UpdatedActivityDTO> updatedDTOs, required String hostName, required String projectId}) async {
    Map<String, dynamic> data = {'patch': updatedDTOs};
    await dio.patch('${ApiEndPoints.updateSTOXActivities}/$hostName/$projectId/mobile', data: data);
  }

  Future<void> pushCommentsToServer({required List<UpdatedCommentDTO> updatedDTOs}) async {
    await dio.post('${ApiEndPoints.stoxComments}/massChange', data: updatedDTOs);
  }
}
