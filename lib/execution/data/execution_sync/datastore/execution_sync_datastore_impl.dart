import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/execution_sync_api_service.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/comment/datastore/turnaround_comment_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/datastore/execution_sync_datastore.dart';

import 'package:sto_mobile_v2/execution/domain/turnaround_activity/datastore/turnaround_activity_datastore.dart';

@LazySingleton(as: IExecutionSyncDataStore)
class ExecutionSyncDataStoreImpl extends IExecutionSyncDataStore {
  final ITurnAroundActivityDataStore turnAroundActivityDataStore;
  final ITurnaroundActivitiesCommentDataStore commentDataStore;
  final ExecutionSyncApiService apiService;

  ExecutionSyncDataStoreImpl(this.apiService, this.turnAroundActivityDataStore, this.commentDataStore);

  @override
  BehaviorSubject<bool> isSyncPending() {
    return turnAroundActivityDataStore.isSyncPending();
  }

  @override
  Future<List<UpdatedActivityDTO>> getAllUpdatedActivies() {
    return turnAroundActivityDataStore.getAllUpdatedActivies();
  }

  @override
  Future<void> pushActivitiesToServer(
      {required List<UpdatedActivityDTO> updatedDTOs, required String hostName, required String projectId}) {
    return apiService
        .pushActivitiesToServer(updatedDTOs: updatedDTOs, hostName: hostName, projectId: projectId)
        .catchDioException();
  }

  @override
  Future<List<UpdatedCommentDTO>> getAllUpdatedComments() {
    return commentDataStore.getAllUpdatedComments();
  }

  @override
  Future<void> pushCommentsToServer({required List<UpdatedCommentDTO> updatedDTOs}) {
    return apiService.pushCommentsToServer(updatedDTOs: updatedDTOs).catchDioException();
  }

  @override
  Future<void> clearCachedTransactions() async {
    await turnAroundActivityDataStore.deleteAllCachedUpdatedActivies();
    await commentDataStore.deleteAllCachedComments();
  }

  @override
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent() {
    return turnAroundActivityDataStore.getCacheStatusForLogoutAndChangingEvent();
  }
}
