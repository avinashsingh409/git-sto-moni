import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/sync_exception.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/datastore/execution_sync_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/repository/execution_sync_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';

@LazySingleton(as: IExecutionSyncRepository)
class ExecutionSyncRepositoryImpl extends IExecutionSyncRepository {
  final IExecutionSyncDataStore syncDataStore;
  final ITurnAroundEventRepository turnAroundEventRepository;
  final IDoneForTheDayRepository doneForTheDayRepository;

  ExecutionSyncRepositoryImpl(
    this.syncDataStore,
    this.turnAroundEventRepository,
    this.doneForTheDayRepository,
  );

  @override
  BehaviorSubject<bool> isSyncPending() {
    return syncDataStore.isSyncPending();
  }

  @override
  Future<Either<Unit, SyncException>> pushDataToServer({required int turnaroundEventId}) async {
    try {
      await _pushTransactionsToServer(turnaroundEventId);
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(SyncException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(SyncException.socketException());
        }
        return const Right(SyncException.unexpectedError());
      }
    } on ConnectionException catch (_) {
      return const Right(SyncException.socketException());
    }

    return const Left(unit);
  }

  Future<void> _pushTransactionsToServer(int selectedTurnAroundEventId) async {
    final offlineCacheData = await Future.wait([
      turnAroundEventRepository.getSelectedTurnAroundEvent(id: selectedTurnAroundEventId),
      syncDataStore.getAllUpdatedActivies(),
      syncDataStore.getAllUpdatedComments()
    ]);
    final TurnAroundEventDTO event = offlineCacheData[0] as TurnAroundEventDTO;
    final List<UpdatedActivityDTO> turnAroundActivities = offlineCacheData[1] as List<UpdatedActivityDTO>;
    final List<UpdatedCommentDTO> comments = offlineCacheData[2] as List<UpdatedCommentDTO>;
    //Sending 0 here because backend expects 0 and gets id from httpContext
    if (turnAroundActivities != [] && turnAroundActivities.isNotEmpty) {
      await syncDataStore.pushActivitiesToServer(
          updatedDTOs: turnAroundActivities, hostName: event.hostName!, projectId: event.primaveraProjectId!);
    }
    if (comments != [] && comments.isNotEmpty) {
      await syncDataStore.pushCommentsToServer(updatedDTOs: comments);
    }
    final lastDoneForTheDay = doneForTheDayRepository.getDoneForTheDay();
    if (lastDoneForTheDay != null) {
      await doneForTheDayRepository.syncDoneForTheDay(
          turnaroundEventId: selectedTurnAroundEventId, isDoneForTheDay: lastDoneForTheDay.isDoneForTheDay!);
    }
  }

  @override
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent() async {
    return syncDataStore.getCacheStatusForLogoutAndChangingEvent();
  }
}
