import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/repository/stack_filter_api_service.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/repository/stack_filter_dao.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/datastore/stack_filter_datastore.dart';

@LazySingleton(as: IStackFilterDataStore)
class StackFilterDataStoreImpl extends IStackFilterDataStore {
  final StackFilterApiService stackFilterApiService;
  final StackFilterDAO stackFilterDAO;
  StackFilterDataStoreImpl(this.stackFilterApiService, this.stackFilterDAO);

  @override
  Future<List<StackFilterDTO>?> fetchStackFilters({required String hostname, required String projectId}) {
    return stackFilterApiService.fetchStackFilters(hostname: hostname, projectId: projectId).catchDioException();
  }

  @override
  Future<void> insertStackFilters({required List<StackFilterDTO> stackFilterDTOs}) {
    return stackFilterDAO.insertStackFilters(stackFilterDTOs: stackFilterDTOs);
  }

  @override
  Future<void> deleteAllStackFilters() {
    return stackFilterDAO.deleteAllStackFilters();
  }

  @override
  Future<List<StackFilterDTO>?> getStackFilters() {
    return stackFilterDAO.getStackFilters();
  }

  @override
  Future<List<int>?> getActivitiesByFilter({required int filterId}) {
    return stackFilterDAO.getActivitiesByFilter(filterId: filterId);
  }
}
