// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stack_filter_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetStackFilterDTOCollection on Isar {
  IsarCollection<StackFilterDTO> get stackFilterDTOs => this.collection();
}

const StackFilterDTOSchema = CollectionSchema(
  name: r'StackFilterDTO',
  id: -918050762490976863,
  properties: {
    r'activities': PropertySchema(
      id: 0,
      name: r'activities',
      type: IsarType.longList,
    ),
    r'name': PropertySchema(
      id: 1,
      name: r'name',
      type: IsarType.string,
    )
  },
  estimateSize: _stackFilterDTOEstimateSize,
  serialize: _stackFilterDTOSerialize,
  deserialize: _stackFilterDTODeserialize,
  deserializeProp: _stackFilterDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _stackFilterDTOGetId,
  getLinks: _stackFilterDTOGetLinks,
  attach: _stackFilterDTOAttach,
  version: '3.1.0+1',
);

int _stackFilterDTOEstimateSize(
  StackFilterDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.activities;
    if (value != null) {
      bytesCount += 3 + value.length * 8;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _stackFilterDTOSerialize(
  StackFilterDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLongList(offsets[0], object.activities);
  writer.writeString(offsets[1], object.name);
}

StackFilterDTO _stackFilterDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = StackFilterDTO(
    activities: reader.readLongList(offsets[0]),
    id: id,
    name: reader.readStringOrNull(offsets[1]),
  );
  return object;
}

P _stackFilterDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongList(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _stackFilterDTOGetId(StackFilterDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _stackFilterDTOGetLinks(StackFilterDTO object) {
  return [];
}

void _stackFilterDTOAttach(
    IsarCollection<dynamic> col, Id id, StackFilterDTO object) {
  object.id = id;
}

extension StackFilterDTOQueryWhereSort
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QWhere> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension StackFilterDTOQueryWhere
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QWhereClause> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension StackFilterDTOQueryFilter
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QFilterCondition> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activities',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activities',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesElementEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activities',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesElementGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'activities',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesElementLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'activities',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesElementBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'activities',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      activitiesLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'activities',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }
}

extension StackFilterDTOQueryObject
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QFilterCondition> {}

extension StackFilterDTOQueryLinks
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QFilterCondition> {}

extension StackFilterDTOQuerySortBy
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QSortBy> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }
}

extension StackFilterDTOQuerySortThenBy
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QSortThenBy> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }
}

extension StackFilterDTOQueryWhereDistinct
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QDistinct> {
  QueryBuilder<StackFilterDTO, StackFilterDTO, QDistinct>
      distinctByActivities() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'activities');
    });
  }

  QueryBuilder<StackFilterDTO, StackFilterDTO, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }
}

extension StackFilterDTOQueryProperty
    on QueryBuilder<StackFilterDTO, StackFilterDTO, QQueryProperty> {
  QueryBuilder<StackFilterDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<StackFilterDTO, List<int>?, QQueryOperations>
      activitiesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'activities');
    });
  }

  QueryBuilder<StackFilterDTO, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StackFilterDTO _$StackFilterDTOFromJson(Map<String, dynamic> json) =>
    StackFilterDTO(
      id: json['id'] as int?,
      name: json['name'] as String?,
      activities:
          (json['activities'] as List<dynamic>?)?.map((e) => e as int).toList(),
    );

Map<String, dynamic> _$StackFilterDTOToJson(StackFilterDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'activities': instance.activities,
    };
