import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'stack_filter_dto.g.dart';

@collection
@JsonSerializable()
class StackFilterDTO {
  Id? id;
  String? name;
  List<int>? activities;

  StackFilterDTO({
    this.id,
    this.name,
    this.activities,
  });

  factory StackFilterDTO.fromJson(Map<String, dynamic> json) => _$StackFilterDTOFromJson(json);
}
