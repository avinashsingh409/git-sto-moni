import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/datastore/stack_filter_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/stack_filter/repository/stack_filter_repository.dart';

@LazySingleton(as: IStackFilterRepository)
class StackFilterRepositoryImpl extends IStackFilterRepository {
  final IStackFilterDataStore stackFilterDataStore;
  StackFilterRepositoryImpl(this.stackFilterDataStore);

  @override
  Future<List<StackFilterDTO>?> fetchStackFilters({required String hostname, required String projectId}) {
    return stackFilterDataStore.fetchStackFilters(hostname: hostname, projectId: projectId);
  }

  @override
  Future<void> insertStackFilters({required List<StackFilterDTO> stackFilterDTOs}) {
    return stackFilterDataStore.insertStackFilters(stackFilterDTOs: stackFilterDTOs);
  }

  @override
  Future<void> deleteAllStackFilters() {
    return stackFilterDataStore.deleteAllStackFilters();
  }

  @override
  Future<List<StackFilterDTO>?> getStackFilters() {
    return stackFilterDataStore.getStackFilters();
  }
}
