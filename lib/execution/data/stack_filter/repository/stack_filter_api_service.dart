import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';

@injectable
class StackFilterApiService {
  final Dio dio;
  StackFilterApiService(this.dio);

  Future<List<StackFilterDTO>?> fetchStackFilters({required String hostname, required String projectId}) async {
    final response = await dio.get(ApiEndPoints.stackFilters(hostname: hostname, projectId: projectId));
    if (response.data != null) {
      return List<StackFilterDTO>.from(response.data.map((model) => StackFilterDTO.fromJson(model)));
    }
    return [];
  }
}
