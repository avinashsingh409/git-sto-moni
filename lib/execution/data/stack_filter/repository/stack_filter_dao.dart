import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class StackFilterDAO {
  final isar = PersistenceModule.isar;
  StackFilterDAO();

  late IsarCollection<StackFilterDTO> collection = isar.stackFilterDTOs;

  Future<List<int>> insertStackFilters({required List<StackFilterDTO> stackFilterDTOs}) async {
    return isar.writeTxn(() async {
      return collection.putAll(stackFilterDTOs);
    });
  }

  Future<void> deleteAllStackFilters() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  Future<List<StackFilterDTO>?> getStackFilters() async {
    return collection.where().findAll();
  }

  Future<List<int>?> getActivitiesByFilter({required int filterId}) {
    return collection.where().idEqualTo(filterId).activitiesProperty().build().findFirst();
  }
}
