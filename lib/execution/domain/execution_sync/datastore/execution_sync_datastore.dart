import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

abstract class IExecutionSyncDataStore {
  BehaviorSubject<bool> isSyncPending();
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent();
  Future<List<UpdatedActivityDTO>> getAllUpdatedActivies();
  Future<List<UpdatedCommentDTO>> getAllUpdatedComments();
  Future<void> pushActivitiesToServer(
      {required List<UpdatedActivityDTO> updatedDTOs, required String hostName, required String projectId});
  Future<void> pushCommentsToServer({required List<UpdatedCommentDTO> updatedDTOs});

  Future<void> clearCachedTransactions();
}
