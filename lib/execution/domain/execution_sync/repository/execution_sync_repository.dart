import 'package:dartz/dartz.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/exception/sync_exception.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';

abstract class IExecutionSyncRepository {
  BehaviorSubject<bool> isSyncPending();
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent();
  Future<Either<Unit, SyncException>> pushDataToServer({required int turnaroundEventId});
}
