import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';

abstract class IFilterConfigDataStore {
  Future<List<int>> intializeFilters();
  Future<List<int>> resetFilters();
  Future<List<ExecutionFilterDTO>> getPreConfiguredFilters();
  Future<ExecutionFilterDTO?> getLookaheadHours();
  Future<int> updateFilter({required String updatedVal, required FilterTypeEnum filterType});
  Future<int> updateBaseDate({required LookaheadBaseDateEnum baseDate});
  Future<int> updateRangeDate({required LookaheadRangeDateEnum rangeDate});
}
