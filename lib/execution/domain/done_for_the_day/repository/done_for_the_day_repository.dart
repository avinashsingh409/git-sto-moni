import 'package:sto_mobile_v2/execution/data/done_for_the_day/datastore/models/done_for_the_day_dto.dart';

abstract class IDoneForTheDayRepository {
  Future<int> intiailizeDoneForTheDay();
  DoneForTheDayDTO? getDoneForTheDay();
  Future<int> updateDoneForTheDay({required bool isDoneForTheDay});
  Future<int> resetDoneForTheDay();
  Future<void> syncDoneForTheDay({required int turnaroundEventId, required bool isDoneForTheDay});
}
