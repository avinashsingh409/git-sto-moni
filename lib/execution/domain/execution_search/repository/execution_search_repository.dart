import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';

abstract class IExecutionSearchRepository {
  BehaviorSubject<List<ActivityDTO>> getSearchedTurnAroundEvent({required String activityKeyword});
}
