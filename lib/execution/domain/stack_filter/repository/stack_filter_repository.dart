import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';

abstract class IStackFilterRepository {
  Future<List<StackFilterDTO>?> fetchStackFilters({required String hostname, required String projectId});
  Future<void> insertStackFilters({required List<StackFilterDTO> stackFilterDTOs});
  Future<void> deleteAllStackFilters();
  Future<List<StackFilterDTO>?> getStackFilters();
}
