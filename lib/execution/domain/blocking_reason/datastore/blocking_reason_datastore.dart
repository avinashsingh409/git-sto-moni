import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';

abstract class IBlockingReasonDataStore {
  Future<List<BlockingReasonDTO>?> getBlockingReasons();
  Future<void> insertBlockingReasons({required List<BlockingReasonDTO> blockingReasonDTOs});
  Future<void> deleteAllBlockingReasons();
  Future<List<BlockingReasonDTO>?> getCachedBlockingReasons();
}
