import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';

abstract class IActivityGroupingDataStore {
  Future<List<ActivityFieldDTO>?> getGroupableFields();
  List<GroupingTreeNodeDTO>? getLevel1Grouping(
      {required List<ActivityDTO> allActivities, required ActivityFieldDTO groupingField});
  List<GroupingTreeNodeDTO>? getLevel2Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingFields});
  List<GroupingTreeNodeDTO>? getLevel3Grouping(
      {required List<ActivityDTO> allActivities, required List<ActivityFieldDTO> groupingFields});
  Future<void> cacheGroupedData({required List<GroupingTreeNodeDTO> groupedElements});
  Future<List<GroupingTreeNodeDTO>> getCachedGroupData();
  MultiLevelSortDTO? getSelectedMultiLevelSort();
  Future<void> saveSelectedMultiLevelSort({required MultiLevelSortDTO multiLevelSortDTO});
  Future<void> clearSelectedMultiLevelSort();
  Future<bool> isPrevGroupAvailable();
  Future<void> clearMultiLevelGrouping();
}
