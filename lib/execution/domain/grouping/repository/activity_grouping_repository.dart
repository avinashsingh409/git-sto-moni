import 'dart:collection';

import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/enums/grouping_option_enum.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';

abstract class IActivityGroupingRepository {
  Future<List<ActivityFieldDTO>?> getGroupableFields();
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel1Grouping({required ActivityFieldDTO activityField});
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel2Grouping({required List<ActivityFieldDTO> activityFields});
  Future<Either<List<GroupingTreeNodeDTO>?, Unit>> getLevel3Grouping({required List<ActivityFieldDTO> groupingFields});
  Future<bool> isPrevGroupAvailable();
  MultiLevelSortDTO? getSelectedMultiLevelSort();
  Future<void> saveSelectedMultiLevelSort({required MultiLevelSortDTO multiLevelSortDTO});
  Future<void> clearSelectedMultiLevelSort();
  Future<List<GroupingTreeNodeDTO>> getCachedGroupData();
  Future<void> clearMultiLevelGrouping();
  SplayTreeMap<String, List<ActivityDTO>> groupActivities({
    required List<ActivityDTO> activities,
    required GroupingOptionsEnum groupingType,
    required List<ActivityFieldDTO?> groupingFields,
  });
}
