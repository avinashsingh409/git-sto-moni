import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/execution/data/execution_sync/datastore/enums/cache_status.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_response.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

abstract class ITurnAroundActivityDataStore {
  Future<ActivityResponse> getOnlineTurnAroundActivities({required int index, required int limit});
  Future<void> insertAllActivities({required List<ActivityDTO> turnAroundDTOs});
  Future<void> insertAllActivityFields({required List<ActivityFieldDTO> activityFields});
  Future<void> deleteTurnAroudActivities();
  Future<List<ActivityFieldDTO>?> getActivityFields();
  Future<void> deleteActivityFields();
  Future<BehaviorSubject<List<ActivityDTO>>> getHomeScreenActivities({
    required List<FilterTypeEnum> staticFilters,
    required List<int> stackFilters,
    required String searchTerm,
  });
  BehaviorSubject<List<ActivityDTO>> getWbsActivity({required String activity});
  BehaviorSubject<List<ActivityDTO>> getGroupedActivities({required List<int> activityIds});
  Future<ActivityDTO?> getActivityById({required int activityId});
  Future<ActivityDTO?> getSwipeActivity({required int activityId});
  Future<void> updateActivity({required ActivityDTO updatedActivity});
  Future<void> cacheActivityInTransactionsTable({required UpdatedActivityDTO updatedActivityDTO});
  BehaviorSubject<bool> isSyncPending();
  Future<CacheStatus> getCacheStatusForLogoutAndChangingEvent();
  Future<List<UpdatedActivityDTO>> getAllUpdatedActivies();
  Future<void> deleteAllCachedUpdatedActivies();
  Future<List<ActivityDTO>?> getActivitiesForGrouping();
  Future<ActivityDTO?> getActivityByWorkPackageId({required int workPackageId});
}
