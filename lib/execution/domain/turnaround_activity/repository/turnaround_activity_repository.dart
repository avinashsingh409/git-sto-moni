import 'package:dartz/dartz.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_activity_exception.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

abstract class ITurnAroundActivityRepository {
  Future<Either<Unit, TurnAroundActivityException>> getAndCacheExecutionData({required TurnAroundEventDTO turnAroundEventDTO});
  Future<BehaviorSubject<List<ActivityDTO>>> getHomeScreenActivities({
    required List<FilterTypeEnum> staticFilters,
    required List<int> stackFilters,
    required String searchTerm,
  });
  BehaviorSubject<List<ActivityDTO>> getWbsActivity({required String activity});
  BehaviorSubject<List<ActivityDTO>> getGroupedActivities({required String activityIds});
  Future<ActivityDTO?> getActivityById({required int activityId});
  Future<ActivityDTO?> getSwipeActivity({required int activityId});
  Future<void> updateActivity({
    required ActivityDTO currentActivity,
    required UpdatedActivityDTO updateActivityModel,
  });
  Future<Map<String, String>?> getActivityFieldsWithValue({required ActivityDTO activityDTO});
  Future<List<ActivityDTO>?> getActivitiesForGrouping();
  Future<ActivityDTO?> getActivityByWorkPackageId({required int workPackageId});
}
