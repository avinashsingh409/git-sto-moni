import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';

abstract class ITurnaroundActivitiesCommentDataStore {
  Future<CommentResponse> getComments(
      {required String hostName, required String projectId, required int index, required int limit});
  Future<void> cacheAllComments({required List<CommentDTO> commentDTOs});
  Future<void> createComment({required CommentDTO commentDTO});
  Future<void> insertUpdatedComment({required UpdatedCommentDTO commentDTO});
  Stream<List<CommentDTO>> getAllCachedComments({required int objectId});
  Stream<List<CommentDTO>> getMyCachedComments({required int objectId, required String username});
  Future<List<UpdatedCommentDTO>> getAllUpdatedComments();
  Future<void> deleteAllComments();
  Future<void> deleteAllCachedComments();
  //
}
