import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_response.dart';

abstract class ITurnaroundActivitiesCommentRepository {
  Future<CommentResponse> getActivitiesComments(
      {required String hostName, required String projectId, required int index, required int limit});
  Future<void> createComment({
    required int objectId,
    required UserInformation userInformation,
    required String comment,
  });

  Future<void> deleteAllComments();
  Future<void> cacheAllComments({required List<CommentDTO> commentDTOs});
  Future<void> deleteAllCachedComments();

  //Get comments from Offline DB
  Stream<List<CommentDTO>> getAllCachedComments({required int objectId});
  Stream<List<CommentDTO>> getMyCachedComments({required int objectId, required UserInformation userInformation});
}
