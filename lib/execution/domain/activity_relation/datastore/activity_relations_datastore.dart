import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';

abstract class IActivityRelationsDataStore {
  Future<List<ActivityDTO>?> getPredecessorActivities({required List<String> activities});
  Future<List<ActivityDTO>?> getSuccessorActivities({required List<String> activities});
}
