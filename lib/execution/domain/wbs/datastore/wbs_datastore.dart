import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';

abstract class IWbsDataStore {
  Future<List<WbsLevelStyleDTO>> getWbsLevels();
  Future<void> insertAllWbsStyleItems({required List<WbsLevelStyleDTO> wbsLevels});
  Future<List<WbsLevelStyleDTO>> getAllCachedLevelItems();
  Future<void> deleteAllWbsItems();
  Future<void> deleteAllWbsLevelStyles();
  Future<void> insertAllWbsElements({required List<TreeNodeDTO> wbsDtos});
  Future<List<TreeNodeDTO>> getWbsTree({required int level, required String parentName});
  Future<TreeNodeDTO?> getWbsItemByCode({required String wbsCode});
}
