import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';

abstract class IWbsRepository {
  Future<List<WbsLevelStyleDTO>> getWbsLevelsStyles();
  Future<void> insertAllWbsLevelStyles({required List<WbsLevelStyleDTO> wbsLevelsStyles});
  Future<List<WbsLevelStyleDTO>> getAllCachedLevelStyles();
  Future<void> deleteAllWbsLevelStyles();
  Future<void> deleteAllWbsItems();
  Future<void> getAndCacheWbsElements({required List<ActivityDTO> activities});
  Future<List<TreeNodeDTO>> getWbsTree({required int level, required String parentName});
  Future<TreeNodeDTO?> getWbsItemByCode({required String wbsCode});
}
