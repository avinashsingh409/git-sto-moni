import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/sync_changes_notifier/sync_changes_notifier_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/activity_list_screen.dart';
import 'package:sto_mobile_v2/execution/screens/sync/sync_screen.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

final List<Widget> screens = [
  const ActivityListScreen(),
  const SyncScreen(),
];

var navItems = [
  const BottomNavigationBarItem(
    icon: Icon(
      Icons.home,
    ),
    label: 'Home',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
  BottomNavigationBarItem(
    icon: BlocProvider(
      create: (context) => getIt<SyncChangesNotifierBloc>(),
      child: BlocBuilder<SyncChangesNotifierBloc, SyncChangesNotifierState>(
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const SizedBox(),
            noChanges: () => const Icon(Icons.sync),
            syncPending: () => Stack(children: [
              Positioned(
                right: 0,
                child: Container(
                  padding: const EdgeInsets.all(1),
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: const BoxConstraints(
                    minWidth: 12,
                    minHeight: 12,
                  ),
                ),
              ),
              const Icon(Icons.sync),
            ],
          ));
        },
      ),
    ),
    label: 'Sync',
    backgroundColor: AppTheme.pgPrimaryRed,
  ),
];
