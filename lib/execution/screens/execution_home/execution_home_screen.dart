import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/logout/logout_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_drawer.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_switcher.dart';
import 'package:sto_mobile_v2/common/screens/widgets/data_loss_dialog.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/app_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/execution_home/execution_home_screen_constants.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import '../grouping/widgets/grouping_pane.dart';

class ExecutionHomeScreen extends StatefulWidget {
  const ExecutionHomeScreen({
    super.key,
  });

  @override
  State<ExecutionHomeScreen> createState() => _ExecutionHomeScreenState();
}

class _ExecutionHomeScreenState extends State<ExecutionHomeScreen> {
  final logoutBloc = getIt<LogoutBloc>();
  final activityListBloc = getIt<ActivityListScreenBloc>();
  int currentIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: logoutBloc,
      child: BlocListener<LogoutBloc, LogoutState>(
        listener: (context, state) {
          state.maybeWhen(
            orElse: () {},
            cacheNotEmpty: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DataLossDialog(
                    onConfirm: () async {
                      logoutBloc.add(const LogoutEvent.forceLogout());
                    }
                  );
                }
              );
            },
            error: (errMsg) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                      backgroundColor: AppTheme.pgPrimaryRed,
                      content: Center(
                        child: Text(
                          errMsg,
                        ),
                      ),
                      duration: const Duration(seconds: 4)),
                );
            },
            success: () {
              jumpToLoginScreen(context: context, isSessionRestoration: false);
            },
          );
        },
        child: BlocProvider(
          create: (context) => activityListBloc,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            key: _scaffoldKey,
            appBar: AppBar(
              title: const Text('STO $execution'),
              centerTitle: true,
              leading: IconButton(
                iconSize: 32,
                onPressed: () {
                  _scaffoldKey.currentState?.openDrawer();
                },
                icon: const Icon(
                  Icons.menu,
                  color: Colors.white,
                ),
              ),
              actions: [
                if (currentIndex == 0) const GroupingIcon(),
                const AppSwitcherCog(),
              ],
            ),
            body: IndexedStack(index: currentIndex, children: screens),
            drawer: AppDrawer(
              logoutBloc: logoutBloc,
            ),
            bottomNavigationBar: SizedBox(
              height: MediaQuery.of(context).size.height * .106,
              child: BottomNavigationBar(
                showSelectedLabels: true,
                showUnselectedLabels: true,
                // key: GlobalNavigationKey().navigatorKey,
                type: BottomNavigationBarType.shifting,
                currentIndex: currentIndex,
                unselectedItemColor: Colors.white60,
                elevation: 0,
                items: navItems,
                onTap: (index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
