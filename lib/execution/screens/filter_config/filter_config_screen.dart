import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/filter_config/filter_config_bloc.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_base_date_enum.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/lookahead_range_date_enum.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class FilterConfigScreen extends StatelessWidget {
  const FilterConfigScreen({super.key});

  @override
  Widget build(BuildContext context) {
    List<int> values = [12, 24, 36, 48, 64, 72];
    final bloc = getIt<FilterConfigBloc>();
    bloc.add(const FilterConfigEvent.getPreConfiguredFilter());
    return BlocProvider.value(
      value: bloc,
      child: BlocProvider.value(
        value: bloc,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Configure Filters'),
          ),
          body: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 32,
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 28),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Look Ahead',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        'Find Activities whose Range Date is within a set number of hours away from their Base Date',
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          color: Colors.black87,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
                Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  child: Column(
                    children: [
                      StreamBuilder(
                        stream: bloc.getPlannerDateIndex(),
                        builder: ((context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const LoadingIndicator();
                          } else if (snapshot.connectionState == ConnectionState.active ||
                              snapshot.connectionState == ConnectionState.done) {
                            if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            } else if (snapshot.hasData) {
                              if (snapshot.data != null) {
                                final index = snapshot.data!;
                                return SliderTheme(
                                  data: SliderTheme.of(context).copyWith(
                                    activeTrackColor: Colors.red[700],
                                    inactiveTrackColor: Colors.red[100],
                                    trackShape: const RoundedRectSliderTrackShape(),
                                    trackHeight: 4.0,
                                    thumbShape: const _ThumbShape(),
                                    thumbColor: Colors.redAccent,
                                    overlayColor: Colors.red.withAlpha(32),
                                    overlayShape: const RoundSliderOverlayShape(overlayRadius: 28.0),
                                    tickMarkShape: const RoundSliderTickMarkShape(),
                                    activeTickMarkColor: Colors.red[700],
                                    inactiveTickMarkColor: Colors.red[100],
                                    valueIndicatorShape: const PaddleSliderValueIndicatorShape(),
                                    valueIndicatorColor: Colors.redAccent,
                                    valueIndicatorTextStyle: const TextStyle(
                                      color: Colors.white,
                                    ),
                                    showValueIndicator: ShowValueIndicator.never,
                                  ),
                                  child: Slider(
                                      label: values[index].toString(),
                                      min: 0,
                                      max: values.length - 1.toDouble(),
                                      value: index.toDouble(),
                                      divisions: values.length - 1,
                                      onChanged: (changedVal) {
                                        bloc.updatePlannerIndex(updatedIndex: changedVal.toInt());
                                        bloc.add(
                                          FilterConfigEvent.updatePlannedDateFilter(
                                            updatedValue: changedVal.toString(),
                                          ),
                                        );
                                      }),
                                );
                              } else {
                                return const Text("Error parsing Activity");
                              }
                            }
                          }
                          return const SizedBox();
                        }),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 26),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              values[0].toString(),
                              style: const TextStyle(
                                fontWeight: FontWeight.w300,
                                color: Colors.black87,
                              ),
                            ),
                            Text(
                              values[values.length - 1].toString(),
                              style: const TextStyle(
                                fontWeight: FontWeight.w300,
                                color: Colors.black87,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),
                      StreamBuilder(
                        stream: bloc.getBaseDate(),
                        builder: ((context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const LoadingIndicator();
                          } else if (snapshot.connectionState == ConnectionState.active ||
                              snapshot.connectionState == ConnectionState.done) {
                            if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            } else if (snapshot.hasData) {
                              if (snapshot.data != null) {
                                return DropdownMenu<LookaheadBaseDateEnum>(
                                  dropdownMenuEntries: LookaheadBaseDateEnum.values.map<DropdownMenuEntry<LookaheadBaseDateEnum>>((LookaheadBaseDateEnum field) =>
                                    DropdownMenuEntry<LookaheadBaseDateEnum>(label: field.title, value: field)
                                  ).toList(),
                                  onSelected: (baseDate) => bloc.add(FilterConfigEvent.updateBaseDate(baseDate: baseDate ?? LookaheadBaseDateEnum.todaysDate)),
                                  label: const Text("Base Date"),
                                  initialSelection: snapshot.data,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                );
                              } else {
                                return const Text("Error parsing Activity");
                              }
                            }
                          }
                          return const SizedBox();
                        }),
                      ),
                      const SizedBox(height: 20),
                      StreamBuilder(
                        stream: bloc.getRangeDate(),
                        builder: ((context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const LoadingIndicator();
                          } else if (snapshot.connectionState == ConnectionState.active ||
                              snapshot.connectionState == ConnectionState.done) {
                            if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            } else if (snapshot.hasData) {
                              if (snapshot.data != null) {
                                return DropdownMenu<LookaheadRangeDateEnum>(
                                  dropdownMenuEntries: LookaheadRangeDateEnum.values.map<DropdownMenuEntry<LookaheadRangeDateEnum>>((LookaheadRangeDateEnum field) =>
                                    DropdownMenuEntry<LookaheadRangeDateEnum>(label: field.title, value: field)
                                  ).toList(),
                                  onSelected: (rangeDate) => bloc.add(FilterConfigEvent.updateRangeDate(rangeDate: rangeDate ?? LookaheadRangeDateEnum.plannedStartDate)),
                                  label: const Text("Range Date"),
                                  initialSelection: snapshot.data,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                );
                              } else {
                                return const Text("Error parsing Activity");
                              }
                            }
                          }
                          return const SizedBox();
                        }),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.1,
                ),
                Center(
                  child: SizedBox(
                    child: Icon(
                      size: MediaQuery.of(context).size.height * 0.3,
                      Icons.filter_alt,
                      color: AppTheme.pgPrimaryRed.withOpacity(0.1),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ThumbShape extends RoundSliderThumbShape {
  final _indicatorShape = const PaddleSliderValueIndicatorShape();

  const _ThumbShape();

  @override
  void paint(PaintingContext context, Offset center,
      {required Animation<double> activationAnimation,
      required Animation<double> enableAnimation,
      required bool isDiscrete,
      required TextPainter labelPainter,
      required RenderBox parentBox,
      required SliderThemeData sliderTheme,
      required TextDirection textDirection,
      required double value,
      required double textScaleFactor,
      required Size sizeWithOverflow}) {
    super.paint(
      context,
      center,
      activationAnimation: activationAnimation,
      enableAnimation: enableAnimation,
      sliderTheme: sliderTheme,
      value: value,
      textScaleFactor: textScaleFactor,
      sizeWithOverflow: sizeWithOverflow,
      isDiscrete: isDiscrete,
      labelPainter: labelPainter,
      parentBox: parentBox,
      textDirection: textDirection,
    );
    _indicatorShape.paint(
      context,
      center,
      activationAnimation: const AlwaysStoppedAnimation(1),
      enableAnimation: enableAnimation,
      labelPainter: labelPainter,
      parentBox: parentBox,
      sliderTheme: sliderTheme,
      value: value,
// test different testScaleFactor to find your best fit
      textScaleFactor: 0.6,
      sizeWithOverflow: sizeWithOverflow,
      isDiscrete: isDiscrete,
      textDirection: textDirection,
    );
  }
}
