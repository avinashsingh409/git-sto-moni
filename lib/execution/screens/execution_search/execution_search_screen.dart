import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/execution/application/execution_search/execution_search_bloc.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/execution_search/widgets/execution_search_result_pane.dart';
import 'package:sto_mobile_v2/execution/screens/execution_search/widgets/execution_search_pane.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class ExecutionSearchScreen extends StatefulWidget {
  const ExecutionSearchScreen({super.key});

  @override
  State<ExecutionSearchScreen> createState() => _ExecutionSearchScreenState();
}

class _ExecutionSearchScreenState extends State<ExecutionSearchScreen> {
  final TextEditingController searchController = TextEditingController();

  final bloc = getIt<ExecutionSearchBloc>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            ExecutionSearchPane(
              searchController: searchController,
              bloc: bloc,
            ),
            BlocBuilder<ExecutionSearchBloc, ExecutionSearchState>(
              builder: (context, state) {
                return state.when(
                  initial: () => const SizedBox(),
                  loading: () => const LoadingIndicator(),
                  success: (results) => ExecutionSearchResultPane(
                    results: results,
                  ),
                  error: (errormsg) => Flexible(
                    flex: 3,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.error,
                            color: Colors.red,
                            size: 40,
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Text(
                            errormsg,
                            style: const TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
