import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';

class ExecutionSearchResultPane extends StatelessWidget {
  final List<ActivityCardItemModel> results;
  const ExecutionSearchResultPane({
    required this.results,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Expanded(
      flex: 4,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        // ActivityListView has significantly changed; this page should no longer be necessary, but leaving it just in case.
        // TODO: Reevaluate necessary code
        // child: ActivityListView(data: results, isFromHomeScreen: false),
        child: Center(child: Text("Not Implemented"))
      ),
    );
  }
}
