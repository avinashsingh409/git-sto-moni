import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';

class ExecutionSearchBar extends StatelessWidget {  
  const ExecutionSearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final searchController = SelectedSearchTerm.searchController;
    return TextFormField(
      controller: searchController,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        labelText: 'Search',
        contentPadding: const EdgeInsets.symmetric(horizontal: 8),
        suffixIcon: IconButton(icon: const Icon(Icons.clear), onPressed: () {
          searchController.clear();
          BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
        })
      ),
      onFieldSubmitted: (searchInput) => BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities()),
    );
  }
}

@lazySingleton
class SelectedSearchTerm {
  static TextEditingController searchController = TextEditingController();
}