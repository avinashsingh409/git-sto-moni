import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/execution_search/execution_search_bloc.dart';

class ExecutionSearchPane extends StatelessWidget {
  final TextEditingController searchController;
  final ExecutionSearchBloc bloc;
  const ExecutionSearchPane({
    Key? key,
    required this.searchController,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Find Activities',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: searchController,
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Enter activity',
                    ),
                    onFieldSubmitted: (searchInput) => bloc.add(
                      ExecutionSearchEvent.search(
                        activityKeyword: searchInput.trim(),
                      ),
                    ),
                  ),
                ),
                CircleAvatar(
                  radius: 30,
                  backgroundColor: AppTheme.pgPrimaryRed,
                  child: IconButton(
                    icon: const Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      bloc.add(ExecutionSearchEvent.search(activityKeyword: searchController.text));
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
