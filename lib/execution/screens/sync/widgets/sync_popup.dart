import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/sync/sync_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/sync/widgets/syncing_indicator.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

Future<void> syncDataPopUp({
  required BuildContext context,
}) {
  final bloc = getIt<SyncBloc>();
  return showModalBottomSheet<void>(
    isScrollControlled: true,
    isDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async => false,
        child: BlocProvider(
          create: (context) => bloc,
          child: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.warning),
                    Text('Please wait while the data syncs'),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                BlocBuilder<SyncBloc, SyncState>(
                  builder: (context, state) {
                    return state.maybeWhen(
                      orElse: () => Column(children: [
                        const SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.50,
                                child: ElevatedButton.icon(
                                    label: Text(
                                      S.current.sync.toUpperCase(),
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                    icon: const Icon(Icons.sync),
                                    // onPressed: null,
                                    onPressed: () {
                                      bloc.add(const SyncEvent.pushAndGetChangesFromServer());
                                    }),
                              ),
                            ),
                            ElevatedButton.icon(
                                label: Text(
                                  S.current.close.toUpperCase(),
                                  style: const TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                                icon: const Icon(Icons.close),
                                // onPressed: null,
                                onPressed: () => Navigator.of(context).pop()),
                          ],
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                      ]),
                      loading: () => const Column(
                        children: [
                          SyncingIndicator(),
                          SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                      success: () => Column(
                        children: [
                          const Center(
                            child: Icon(
                              Icons.done,
                              color: Colors.green,
                              size: 80,
                            ),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          ElevatedButton.icon(
                              label: Text(
                                S.current.close.toUpperCase(),
                                style: const TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                              icon: const Icon(Icons.close),
                              // onPressed: null,
                              onPressed: () => Navigator.of(context).pop()),
                          const SizedBox(
                            height: 24,
                          ),
                        ],
                      ),
                      error: (errorMsg) => Center(
                        child: Column(
                          children: [
                            const Icon(
                              Icons.error,
                              size: 80,
                              color: Colors.red,
                            ),
                            Text(errorMsg),
                            const SizedBox(
                              height: 12,
                            ),
                            ElevatedButton.icon(
                                label: Text(
                                  S.current.close.toUpperCase(),
                                  style: const TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                                icon: const Icon(Icons.close),
                                // onPressed: null,
                                onPressed: () => Navigator.of(context).pop()),
                            const SizedBox(
                              height: 12,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
