import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

class SyncingIndicator extends StatelessWidget {
  const SyncingIndicator({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 40,
            height: 40,
            margin: const EdgeInsets.only(bottom: 20),
            child: const CircularProgressIndicator(
              color: AppTheme.pgPrimaryRed,
              strokeWidth: 4,
            ),
          ),
          const Text(
            "Syncing Data..",
            style: TextStyle(
              color: AppTheme.pgPrimaryDarkGray,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
