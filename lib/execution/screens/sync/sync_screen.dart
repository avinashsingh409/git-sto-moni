import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/screens/done_for_the_day/done_for_the_day_pane.dart';
import 'package:sto_mobile_v2/execution/screens/sync/widgets/sync_popup.dart';

import 'package:sto_mobile_v2/common/screens/turnaround_event_cache/widgets/turnaround_event_selector.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_version.dart';

class SyncScreen extends StatelessWidget {
  const SyncScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 28,
        ),
        const TurnAroundEventSelector(
          requiresOnlineSync: false,
        ),
        const SizedBox(
          height: 24,
        ),
        const DoneForTheDayPane(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.85,
            child: ElevatedButton.icon(
                label: Text(
                  S.current.sync.toUpperCase(),
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
                icon: const Icon(Icons.sync),
                // onPressed: null,
                onPressed: () => syncDataPopUp(context: context)),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Center(child: AppVersion()),
        ),
      ],
    );
  }
}
