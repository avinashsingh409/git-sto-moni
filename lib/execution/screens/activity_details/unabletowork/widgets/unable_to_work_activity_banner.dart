import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/localization/material_localizations_extensions.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/unabletowork/unblock_view/unblock_view_popup.dart';

class UnableToWorkActivityBanner extends StatelessWidget {
  final ActivityDetailsScreenBloc bloc;
  final ActivityDTO activity;
  const UnableToWorkActivityBanner({super.key, required this.activity, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: HexColor("#FEEAEA"),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 7.5, horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  S.of(context).blockAlertText,
                  style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 18, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  MaterialLocalizations.of(context).formatTimeAndDate(activity.unableToWorkDateTime!),
                  style: const TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            TextButton(
              onPressed: () async {
                unblockViewPopUp(activity: activity, context: context, bloc: bloc);
              },
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Theme.of(context).primaryColor),
              ),
              child: Text("${S.of(context).blockAlertSeeMore} >"),
            ),
          ],
        ),
        // ),
      ),
    );
  }
}
