import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/blocking_reasons/blocking_reasons_bloc.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';

class ReasonsList extends StatelessWidget {
  final List<BlockingReasonDTO>? allReasons;
  final BlockingReasonsBloc bloc;
  final TextEditingController additionalInfoController;
  const ReasonsList({
    Key? key,
    required this.allReasons,
    required this.bloc, 
    required this.additionalInfoController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text("Reason(s):"),
        DropdownSearch(
          items: allReasons ?? [],
          onChanged: (reason) => { bloc.add(BlockingReasonsEvent.addReason(reason: reason)), },
          itemAsString: (reason) => reason?.reasonText,
          popupProps: const PopupProps.menu(showSearchBox: true, constraints: BoxConstraints(maxHeight: 250)),
        ),
        SizedBox(height: 175, child: StreamBuilder(
            stream: bloc.getBlockingReasons(),
            builder: ((context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else if (snapshot.hasData) {
                  final reasons = snapshot.data;
                  if (reasons != null && reasons.isNotEmpty) {
                    return Column(children: [
                      SizedBox(height: 50, child: 
                        ListView.builder(
                          padding: const EdgeInsets.only(top: 10),
                          scrollDirection: Axis.horizontal,
                          itemCount: reasons.length,
                          itemBuilder: (context, index) {
                            return GestureDetector( 
                              onTap: () => bloc.add(BlockingReasonsEvent.removeReason(reason: reasons[index])),
                              child: Container(
                                padding: const EdgeInsets.all(4),
                                margin: const EdgeInsets.only(right: 4),
                                decoration: BoxDecoration(border: Border.all(color: AppTheme.pgPrimaryDarkGray), color: AppTheme.pgPrimaryGray, borderRadius: BorderRadius.circular(5)),
                                child: Row(children: [
                                  Text(reasons[index].reasonText ?? ""),
                                  const SizedBox(width: 5),
                                  const Icon(Icons.close, size: 16)
                                ])
                              ),
                            );
                          },
                        )
                      ),
                      getCommentBox(reasons)
                    ]);
                  }
                  return const Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Icon(Icons.help_outline, size: 50, color: AppTheme.pgPrimaryRed),
                    SizedBox(height: 10),
                    Text("Select one or more reasons", style: TextStyle(color: AppTheme.pgPrimaryRed))
                  ]);
                }
              }
              return const SizedBox(height: 16);
            }),
          ),
        ),
      ]
    );
  }

  Widget getCommentBox(List<BlockingReasonDTO> reasons) {
    if (reasons.any((reason) => reason.commentRequired == true)) {
      return Container(margin: const EdgeInsets.only(top: 10), child:
        TextField(
          controller: additionalInfoController,
          minLines: 3,
          maxLines: 3,
          cursorColor: Colors.black,
          decoration: InputDecoration(
            focusColor: Colors.white,
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.pgPrimaryBlue, width: 2),
            ),
            border: const OutlineInputBorder(borderSide: BorderSide(width: 2)),
            hintText: S.current.unableToWorkAdditionalInfoHintText
          ),
        )
      );
    }
    return Container();
  }
}
