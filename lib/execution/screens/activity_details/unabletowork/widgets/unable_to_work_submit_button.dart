import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/blocking_reasons/blocking_reasons_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

class UnableToWorkSubmitButton extends StatelessWidget {
  final TextEditingController additionalInfoController;
  final BlockingReasonsBloc blockingReasonsBloc;
  final Function(UpdatedActivityDTO updatedActivityDTO) onBtnSubmitted;
  const UnableToWorkSubmitButton({
    Key? key,
    required this.additionalInfoController,
    required this.blockingReasonsBloc, required this.onBtnSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: blockingReasonsBloc.getBlockingReasons(),
      builder: ((context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (snapshot.hasData) {
            final reasons = snapshot.data;
            if (reasons != null && reasons.isNotEmpty) {
              return ElevatedButton(
                onPressed: () {
                  bool showCommentValidator = reasons.any((reason) => reason.commentRequired == true) && additionalInfoController.text.isEmpty;
                  if (reasons.isNotEmpty && reasons != [] && !showCommentValidator) {
                    onBtnSubmitted(UpdatedActivityDTO(
                      unableToWork: true,
                      unableToWorkAdditionalInfo: reasons.any((reason) => reason.commentRequired == true) ? additionalInfoController.text : "",
                      unableToWorkDateTime: DateTime.now(),
                      unableToWorkReasons: reasons.map((reason) => reason.id ?? 0).toList()
                    ));
                    FocusManager.instance.primaryFocus?.unfocus();
                    Navigator.of(context).pop();
                  } else {
                    _showValidationAlert(context);
                  }
                },
                child: Text(S.current.submitButton),
              );
            }
          }
        }
        return ElevatedButton(onPressed: null, child: Text(S.current.submitButton));
      })
    );
  }
}

Future<dynamic> _showValidationAlert(BuildContext context) {
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "Please Enter a Comment.",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
            ),
            const SizedBox(
              height: 12,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  FocusManager.instance.primaryFocus?.unfocus();
                },
                child: const Text("Close")),
          ],
        ));
      });
}
