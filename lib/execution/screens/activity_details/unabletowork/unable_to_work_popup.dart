import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/execution/application/blocking_reasons/blocking_reasons_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/unabletowork/widgets/reasons_list.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/unabletowork/widgets/unable_to_work_submit_button.dart';

Future<void> unableToWorkPopUp({
  required BuildContext context, 
  required Function(UpdatedActivityDTO updatedActivityDTO) onBtnSubmitted,
}) {
  final TextEditingController additionalInfoController = TextEditingController();
  final blockingReasonsBloc = getIt<BlockingReasonsBloc>();

  return showModalBottomSheet<void>(
    isScrollControlled: true,
    context: context,
    builder: (BuildContext context) {
      return BlocProvider.value(
        value: blockingReasonsBloc, 
        child: SizedBox(
          height: MediaQuery.of(context).copyWith().size.height * 0.4,
          child: Padding(padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
           child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            BlocBuilder<BlockingReasonsBloc, BlockingReasonsState>(
              builder: (context, state) {
                return state.when(
                  initial: () => const SizedBox(),
                  loading: () => const LoadingIndicator(),
                  success: (reasons) => ReasonsList(allReasons: reasons, bloc: blockingReasonsBloc, additionalInfoController: additionalInfoController),
                  empty: () => const Center(child: Text('No Blocking Reasons Found'))
                );
              },
            ), 
            UnableToWorkSubmitButton(
              additionalInfoController: additionalInfoController,
              blockingReasonsBloc: blockingReasonsBloc,
              onBtnSubmitted: (updatedActivityDTO) => onBtnSubmitted(updatedActivityDTO),
            ),
           ])
          )
        )
      );
    }
  );
}