import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/localization/material_localizations_extensions.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/application/blocking_reasons/blocking_reasons_bloc.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

Future<void> unblockViewPopUp(
    {required BuildContext context,
    required ActivityDTO activity,
    required ActivityDetailsScreenBloc bloc}) {
  final blockingReasonsBloc = getIt<BlockingReasonsBloc>();
  return showModalBottomSheet<void>(
      // isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return BlocProvider.value(
          value: blockingReasonsBloc,
          child: SizedBox(
            height: MediaQuery.of(context).copyWith().size.height * 0.4,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              child: ListView(
                children: [
                  Text(
                    "${S.of(context).blockDetailsReason}: ",
                    style: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 16,
                      ),
                      BlocBuilder<BlockingReasonsBloc, BlockingReasonsState>(
                        builder: (context, state) {
                          return state.when(
                            initial: () => const SizedBox(), 
                            loading: () => const LoadingIndicator(), 
                            success: (reasons) => Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                              if (activity.unableToWorkReasons != null)
                                for (int? activityReason in activity.unableToWorkReasons!) 
                                  Text("- ${findReason(reasons, activityReason)}", style: const TextStyle(fontSize: 15)),
                              if (activity.unableToWorkAdditionalInfo != null)
                                Text(activity.unableToWorkAdditionalInfo ?? "", style: const TextStyle(fontSize: 15))
                            ],), 
                            empty: () => const Center(child: Text('No Blocking Reasons Found'))
                          );
                        },
                      ),
                    ],
                  ),
                  const Divider(),
                  Text(
                    "${S.of(context).blockDetailsSubmission}: ",
                    style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: 15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            child: Text(
                              activity.modifiedByName ?? "-",
                              style: const TextStyle(fontSize: 15),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          SizedBox(
                            child: Text(
                              MaterialLocalizations.of(context)
                                  .formatTimeAndDate(
                                      activity.unableToWorkDateTime!),
                              style: const TextStyle(fontSize: 15),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  const Divider(),
                  const SizedBox(
                    height: 12,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      onPressed: () async {
                        bloc.add(
                            ActivityDetailsScreenEvent.updateActivityProgress(
                          updateActivityModel: (UpdatedActivityDTO(
                              unableToWork: false,
                              unableToWorkAdditionalInfo: null,
                              unableToWorkDateTime: null,
                              unableToWorkReasons: null)),
                        ));
                        Navigator.of(context).pop();
                      },
                      child: Text(S.of(context).blockDetailsAbleToWork),
                    ),
                  ),
                  const SizedBox(
                    height: 52,
                  )
                ],
              ),
            ),
          ),
        );
      });
}

String findReason(List<BlockingReasonDTO> reasons, int? activityReason) {
  if (activityReason != null) {
    BlockingReasonDTO? filteredReason = reasons.where((x) => x.id == activityReason).firstOrNull;
    if (filteredReason != null) {
      return filteredReason.reasonText ?? "";
    }
  }
  return "";
}
