import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/core/utils/date_formatter.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';

class ActivityFieldsTab extends StatelessWidget {
  final ActivityDTO activity;
  final ActivityDetailsScreenBloc bloc;
  const ActivityFieldsTab({super.key, required this.activity, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Expanded(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(left: 18, top: 12),
            child: StreamBuilder(
              stream: bloc.getActivityFields(),
              builder: ((context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const LoadingIndicator();
                } else if (snapshot.connectionState == ConnectionState.active ||
                    snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else if (snapshot.hasData) {
                    if (snapshot.data != null) {
                      final detailsMap = snapshot.data!;
                      return Column(
                        mainAxisSize: MainAxisSize.max,
                        children: detailsMap.entries.map((entry) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 2,
                            ),
                            child: Row(
                              children: [
                                Text(
                                  "${entry.key}: ",
                                  style: TextStyle(
                                      fontSize: Theme.of(context).textTheme.titleSmall?.fontSize, fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  DateFormatter.handleDisplayDate(dateTimeString: entry.value.toString()),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: const TextStyle(letterSpacing: -1, fontSize: 13),
                                )
                              ],
                            ),
                          );
                        }).toList(),
                      );
                    } else {
                      return const Text("Error parsing Activity");
                    }
                  }
                }
                return const SizedBox();
              }),
            ),
          ),
        ),
      ),
    );
  }
}
