import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/predecessors/predecessors_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/predecessor_activity_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/relation_not_synced_item.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/activity_card_item.dart';

class PredecessorsTab extends StatefulWidget {
  final List<PredecessorActivityDTO>? predecessors;
  const PredecessorsTab({super.key, required this.predecessors});

  @override
  State<PredecessorsTab> createState() => _PredecessorsTabState();
}

class _PredecessorsTabState extends State<PredecessorsTab> {
  final bloc = getIt<PredecessorsBloc>();

  @override
  void initState() {
    bloc.add(PredecessorsEvent.getPredecessors(predecessorActivites: widget.predecessors));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<PredecessorsBloc, PredecessorsState>(
        builder: (context, state) {
          return state.maybeWhen(
              orElse: () => const SizedBox(),
              error: (errorMsg) => Center(
                    child: Text(errorMsg),
                  ),
              predecessorsLoaded: (predecessors, unsyncedPredecessors) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Text(style: const TextStyle(fontWeight: FontWeight.bold), S.of(context).predecessors),
                      const SizedBox(
                        height: 4,
                      ),
                      if (predecessors != null && predecessors.isNotEmpty && predecessors != [])
                        ConstrainedBox(
                          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.4, minHeight: 56.0),
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: predecessors.length,
                            itemBuilder: (context, index) {
                              return ActivityCardItem(
                                activityCardItemModel: predecessors[index],
                                isSwipeAllowed: false,
                              );
                            },
                          ),
                        ),
                      const SizedBox(
                        height: 12,
                      ),
                      const Divider(
                        color: Colors.black54,
                      ),
                      if (unsyncedPredecessors != null && unsyncedPredecessors.isNotEmpty && unsyncedPredecessors != [])
                        Column(
                          children: [
                            const SizedBox(
                              height: 4,
                            ),
                            const Text(
                              'These Activities are not synced per Mobile Filters.',
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: unsyncedPredecessors.length,
                              itemBuilder: (context, index) {
                                return PredecessorNotSyncedItem(
                                  predecessorActivityDTO: unsyncedPredecessors[index],
                                );
                              },
                            ),
                          ],
                        ),
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
