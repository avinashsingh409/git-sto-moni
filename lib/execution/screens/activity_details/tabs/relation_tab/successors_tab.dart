import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/successors/successors_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/successors_activity_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/relation_not_synced_item.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/activity_card_item.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class SuccessorsTab extends StatefulWidget {
  final List<SuccessorsActivityDTO>? successors;
  const SuccessorsTab({super.key, required this.successors});

  @override
  State<SuccessorsTab> createState() => _SuccessorsTabState();
}

class _SuccessorsTabState extends State<SuccessorsTab> {
  final bloc = getIt<SuccessorsBloc>();

  @override
  void initState() {
    bloc.add(SuccessorsEvent.getSuccessors(successorsActivites: widget.successors));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<SuccessorsBloc, SuccessorsState>(
        builder: (context, state) {
          return state.maybeWhen(
              orElse: () => const SizedBox(),
              loading: () => const Center(
                    child: LoadingIndicator(),
                  ),
              error: (errorMsg) => Center(
                    child: Text(errorMsg),
                  ),
              successorsLoaded: (successors, unsyncedSuccessors) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Text(style: const TextStyle(fontWeight: FontWeight.bold), S.of(context).successors),
                      const SizedBox(
                        height: 4,
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.4, minHeight: 56.0),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: successors!.length,
                          itemBuilder: (context, index) {
                            return ActivityCardItem(
                              activityCardItemModel: successors[index],
                              isSwipeAllowed: false,
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      const Divider(
                        color: Colors.black54,
                      ),
                      if (unsyncedSuccessors != null && unsyncedSuccessors.isNotEmpty && unsyncedSuccessors != [])
                        Column(
                          children: [
                            const SizedBox(
                              height: 4,
                            ),
                            const Text(
                              'These Activities are not synced per Applied Filters.',
                              style: TextStyle(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: unsyncedSuccessors.length,
                              itemBuilder: (context, index) {
                                return SuccessorNotSyncedItem(
                                  successorActivityDTO: unsyncedSuccessors[index],
                                );
                              },
                            ),
                          ],
                        ),
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
