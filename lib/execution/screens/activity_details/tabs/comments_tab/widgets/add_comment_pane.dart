import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/comment/comment_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';

class AddCommentPane extends StatefulWidget {
  final int objectId;
  final CommentBloc bloc;
  final ActivityDTO activityDTO;
  const AddCommentPane({
    Key? key,
    required this.bloc,
    required this.objectId,
    required this.activityDTO,
  }) : super(key: key);

  @override
  State<AddCommentPane> createState() => _AddCommentPaneState();
}

class _AddCommentPaneState extends State<AddCommentPane> {
  final TextEditingController commentController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Colors.grey.shade700,
      borderOnForeground: true,
      elevation: 5,
      margin: const EdgeInsets.all(4),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: commentController,
              minLines: 5,
              maxLines: 5,
              maxLength: 200,
              decoration: InputDecoration(
                hintText: S.of(context).commentsHintText,
              ),
            ),
            const Divider(),
            ElevatedButton(
              onPressed: () {
                if (commentController.text.isNotEmpty) {
                  widget.bloc.add(CommentEvent.addComment(
                    object: widget.objectId,
                    comment: commentController.text.trim(),
                    activityDTO: widget.activityDTO,
                  ));
                  commentController.clear();
                }
              },
              child: Text(S.of(context).activityPostButton),
            ),
            const SizedBox(
              height: 12,
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    commentController.dispose();
    super.dispose();
  }
}
