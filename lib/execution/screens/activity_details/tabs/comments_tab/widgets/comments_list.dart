import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/comments_tab/widgets/comment_item.dart';

class CommentsList extends StatelessWidget {
  final List<CommentDTO> comments;
  const CommentsList({super.key, required this.comments});

  @override
  Widget build(BuildContext context) {
    final commentList = getCommentWidgetList(comments);
    if (commentList.length > 2) {
      return ExpandableNotifier(
        child: ScrollOnExpand(
          child: Expandable(
            collapsed: Column(
              children: [
                Column(children: comments.length < 2 ? commentList : commentList.sublist(0, 2)),
                ExpandableButton(
                  child: Text(
                    "${S.of(context).commentsShowMore} (${commentList.length - 2})",
                    style: const TextStyle(color: AppTheme.pgPrimaryBlue),
                  ),
                )
              ],
            ),
            expanded: Column(
              children: [
                Column(
                  children: commentList,
                ),
                ExpandableButton(
                  child: Text(
                    S.of(context).commentsShowFewer,
                    style: const TextStyle(color: AppTheme.pgPrimaryBlue),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    } else if (commentList.isNotEmpty && commentList.length <= 2) {
      return Column(
        children: commentList,
      );
    } else {
      return SizedBox(child: Text(S.of(context).noComments));
    }
  }

  List<CommentItem> getCommentWidgetList(List<CommentDTO> list) {
    List<CommentItem> commentList = [];
    for (var comment in list) {
      commentList.add(CommentItem(commentDTO: comment));
    }
    return commentList;
  }
}
