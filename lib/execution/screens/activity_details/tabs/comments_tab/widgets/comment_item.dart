import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/localization/material_localizations_extensions.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';

class CommentItem extends StatelessWidget {
  final CommentDTO commentDTO;
  const CommentItem({super.key, required this.commentDTO});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 4),
      color: Colors.grey.shade200,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 12, 20, 16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  commentDTO.userFullName!,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
                Text(
                  MaterialLocalizations.of(context).formatTimeAndDate(commentDTO.dateModified!),
                  style: const TextStyle(fontSize: 14, fontStyle: FontStyle.italic),
                ),
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            if (commentDTO.comment != null)
              ReadMoreText(
                commentDTO.comment!.trimRight(),
                trimLines: 2,
                trimMode: TrimMode.Line,
                trimCollapsedText: S.of(context).commentWidgetShowMore,
                trimExpandedText: S.of(context).commentWidgetShowLess,
                colorClickableText: AppTheme.pgPrimaryBlue,
                style: const TextStyle(color: Colors.black, fontSize: 14),
              )
          ],
        ),
      ),
    );
  }
}
