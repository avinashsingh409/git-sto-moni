import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/execution/application/comment/comment_bloc.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/comments_tab/widgets/comments_list.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class MyCommentsView extends StatelessWidget {
  const MyCommentsView({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  final CommentBloc bloc;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<CommentDTO>>(
      stream: bloc.getMyComments(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LoadingIndicator();
        } else if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (snapshot.hasData) {
            if (snapshot.data != null && snapshot.data!.isNotEmpty) {
              return CommentsList(
                comments: snapshot.data!,
              );
            } else {
              return const Text("No Comments");
            }
          }
        }
        return const SizedBox();
      },
    );
  }
}

class AllCommentsView extends StatelessWidget {
  const AllCommentsView({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  final CommentBloc bloc;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<CommentDTO>>(
      stream: bloc.getAllComments(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LoadingIndicator();
        } else if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (snapshot.hasData) {
            if (snapshot.data != null && snapshot.data!.isNotEmpty) {
              return CommentsList(
                comments: snapshot.data!,
              );
            } else {
              return const Text("No Comments");
            }
          }
        }
        return const SizedBox();
      },
    );
  }
}
