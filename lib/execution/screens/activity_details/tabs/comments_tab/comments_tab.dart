import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/comment/comment_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/comments_tab/comment_list_views.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/comments_tab/widgets/add_comment_pane.dart';

class CommentsTab extends StatefulWidget {
  final ActivityDTO activityDTO;

  const CommentsTab({super.key, required this.activityDTO});

  @override
  State<CommentsTab> createState() => _CommentsTabState();
}

class _CommentsTabState extends State<CommentsTab> {
  final bloc = getIt<CommentBloc>();

  @override
  void initState() {
    super.initState();
    bloc.add(CommentEvent.getComments(object: widget.activityDTO.objectId!));
  }

  int segmentedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AddCommentPane(
              bloc: bloc,
              objectId: widget.activityDTO.objectId!,
              activityDTO: widget.activityDTO,
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              S.of(context).activityComments,
              style: const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 12,
            ),
            CupertinoSlidingSegmentedControl(
              groupValue: segmentedIndex,
              onValueChanged: (int? index) {
                setState(() {
                  segmentedIndex = index!;
                });
              },
              children: {
                0: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                  child: Text(
                    (S.of(context).commentsMostRecent).toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                      color: segmentedIndex == 0 ? AppTheme.pgPrimaryBlue : HexColor("#707070"),
                    ),
                  ),
                ),
                1: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                  child: Text(
                    (S.of(context).commentsYourComments).toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: segmentedIndex == 1 ? AppTheme.pgPrimaryBlue : HexColor("#707070"),
                    ),
                  ),
                ),
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              child: segmentedIndex == 0 ? AllCommentsView(bloc: bloc) : MyCommentsView(bloc: bloc),
            ),
          ],
        ),
      ),
    );
  }
}
