import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

class CustomPercentageAlertDialog extends StatefulWidget {
  final ActivityDetailsScreenBloc bloc;
  const CustomPercentageAlertDialog({
    super.key,
    required this.bloc,
  });

  @override
  State<CustomPercentageAlertDialog> createState() => _CustomPercentageAlertDialogState();
}

class _CustomPercentageAlertDialogState extends State<CustomPercentageAlertDialog> {
  final TextEditingController customPercentageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: SizedBox(
      height: 184,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "${S.current.customPercentageDescriptiveText}:",
          ),
          const SizedBox(
            height: 16,
          ),
          TextField(
            controller: customPercentageController,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(5),
              border: const OutlineInputBorder(
                borderSide: BorderSide(width: 1, color: Colors.grey),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              hintText: S.current.customPercentageHintText,
            ),
            keyboardType: TextInputType.number,
            autofocus: true,
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 42,
            child: Center(
              child: ElevatedButton(
                onPressed: () async {
                  if (customPercentageController.text.isNotEmpty &&
                      num.tryParse(customPercentageController.text) != null &&
                      num.tryParse(customPercentageController.text)! <= 100) {
                    widget.bloc.add(
                      ActivityDetailsScreenEvent.updateActivityProgress(
                          updateActivityModel: UpdatedActivityDTO(
                        percentComplete: double.tryParse(customPercentageController.text)!.roundToDouble() / 100,
                      )),
                    );
                    Navigator.of(context).pop();
                  } else {
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(
                        const SnackBar(
                            backgroundColor: AppTheme.pgPrimaryRed,
                            content: Text(
                              "Invalid input",
                            ),
                            duration: Duration(seconds: 1)),
                      );
                  }
                  FocusManager.instance.primaryFocus?.unfocus();
                },
                child: Text(S.current.submitButton),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
