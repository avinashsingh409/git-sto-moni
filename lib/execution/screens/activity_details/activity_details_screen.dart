import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/activity_details_body.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class ActivityDetailsScreen extends StatefulWidget {
  final int activityId;
  final String activity;
  const ActivityDetailsScreen({super.key, required this.activityId, required this.activity});

  @override
  State<ActivityDetailsScreen> createState() => _ActivityDetailsScreenState();
}

class _ActivityDetailsScreenState extends State<ActivityDetailsScreen> {
  final bloc = getIt<ActivityDetailsScreenBloc>();

  @override
  void initState() {
    bloc.add(ActivityDetailsScreenEvent.getActivityDetails(activityId: widget.activityId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.activity),
        ),
        body: StreamBuilder(
          stream: bloc.getActivityDetails(),
          builder: ((context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const LoadingIndicator();
            } else if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else if (snapshot.hasData) {
                if (snapshot.data != null) {
                  final activity = snapshot.data!;
                  return ActivityDetailsBody(activity: activity, bloc: bloc);
                } else {
                  return const Text("Error parsing Activity");
                }
              }
            }
            return const SizedBox();
          }),
        ),
      ),
    );
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }
}
