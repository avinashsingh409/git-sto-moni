import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/activity_details_style.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/comments_tab/comments_tab.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/relation_tab/predecessors_tab.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/relation_tab/successors_tab.dart';

class ActivityTabMenu extends StatelessWidget {
  const ActivityTabMenu({
    super.key,
    required this.activity,
  });

  final ActivityDTO activity;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TabButton(
              buttonName: S.current.activityComments,
              onBtnPressed: () {
                showActivityDetailsMenuModal(
                  context,
                  CommentsTab(activityDTO: activity),
                );
              }),
          TabButton(
              buttonName: S.current.predecessors,
              onBtnPressed: () {
                showActivityDetailsMenuModal(
                  context,
                  PredecessorsTab(predecessors: activity.predecessors),
                );
              }),
          TabButton(
            buttonName: S.current.successors,
            onBtnPressed: () {
              showActivityDetailsMenuModal(
                context,
                SuccessorsTab(successors: activity.successors),
              );
            },
          ),
        ],
      ),
    );
  }

  Future<void> showActivityDetailsMenuModal(BuildContext context, Widget menuChild) {
    return showModalBottomSheet<void>(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        builder: (BuildContext context) {
          return SizedBox(
            height: MediaQuery.of(context).size.height * 0.72,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  menuChild,
                ],
              ),
            ),
          );
        });
  }
}

class TabButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onBtnPressed;
  const TabButton({
    super.key,
    required this.buttonName,
    required this.onBtnPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onBtnPressed(),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(color: Theme.of(context).primaryColor, borderRadius: BorderRadius.circular(6), boxShadow: [
          BoxShadow(
            color: Colors.black45,
            blurRadius: 1,
            offset: Offset.fromDirection(3),
          ),
        ]),
        child: Tab(
          child: Text(
            buttonName,
            style: tabMenuStyle,
          ),
        ),
      ),
    );
  }
}
