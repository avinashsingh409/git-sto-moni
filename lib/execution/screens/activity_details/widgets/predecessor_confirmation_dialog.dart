import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class PredecessorConfirmationDialog extends StatefulWidget {
  final Function() onCancel;
  const PredecessorConfirmationDialog({super.key, required this.onCancel});

  @override
  State<PredecessorConfirmationDialog> createState() => _PredecessorConfirmationDialogState();
}

class _PredecessorConfirmationDialogState extends State<PredecessorConfirmationDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Predecessor(s) Incomplete"),
      content: const Text("A predecessor activity for this activity has not been completed or started. Do you want to continue with the progress?"),
      actions: [
        TextButton(
          onPressed: () {
            widget.onCancel();
            Navigator.of(context).pop(false);
          },
          style: TextButton.styleFrom(
            foregroundColor: AppTheme.pgPrimaryDarkGray,
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            )
          ),
          child: Text(S.of(context).cancel.toUpperCase()),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          style: TextButton.styleFrom(
            foregroundColor: AppTheme.pgPrimaryBlue,
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            )
          ),
          child: Text(S.of(context).confirm.toUpperCase()),
        ),
      ],
    );
  }
}