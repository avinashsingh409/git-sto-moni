import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_linker_exception.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/utils/app_linker.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/primavera_activity_status_enum.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/custom_percentage/custom_percentage_popup.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/tabs/activity_fields/activity_fields.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/unabletowork/unable_to_work_popup.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/unabletowork/widgets/unable_to_work_activity_banner.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/activity_details_tab_menu.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/activity_not_ready_to_start.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/activity_ready_to_start.dart';
import 'package:sto_mobile_v2/execution/screens/activity_details/widgets/progress_update_pane.dart';

class ActivityDetailsBody extends StatelessWidget {
  final ActivityDTO activity;
  final ActivityDetailsScreenBloc bloc;
  const ActivityDetailsBody({
    Key? key,
    required this.activity,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 1,
      child: Column(
        children: [
          if (activity.unableToWork ?? false) UnableToWorkActivityBanner(activity: activity, bloc: bloc),
          if (activity.unableToWorkReasons == null && activity.predecessorsStatus != PrimaveraActivityStatus.completed)
            ActivityNotReadyBanner(dateTime: activity.displayDate ?? DateTime.now().toString()),
          if (activity.unableToWorkReasons == null && activity.predecessorsStatus == PrimaveraActivityStatus.completed)
            ActivityReadyToStartBanner(dateTime: activity.displayDate ?? DateTime.now().toString()),
          Container(
            color: Colors.grey.shade100,
            height: MediaQuery.of(context).size.height * 0.8,
            child: Stack(fit: StackFit.passthrough, children: [
              Column(
                children: [
                  Card(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 15,
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text((S.current.progress),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Text(
                                      //Todo fix this will crash
                                      "${(activity.percentComplete! * 100).toInt()} %",
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                LinearPercentIndicator(
                                  barRadius: const Radius.circular(4),
                                  progressColor: bloc.getProgressColor(activity.percentComplete ?? 0),
                                  lineHeight: 30,
                                  percent: activity.percentComplete ?? 0,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          ProgressUpdatePane(
                            bloc: bloc,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                onPressed: () async {
                                  unableToWorkPopUp(context: context, onBtnSubmitted: (updatedActivity) {
                                    bloc.add(ActivityDetailsScreenEvent.updateActivityProgress(
                                      updateActivityModel: updatedActivity,
                                    ));
                                  });
                                },
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 5,
                                  ),
                                  child: Text(S.current.unableToWorkButton),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Theme.of(context).unselectedWidgetColor),
                                  foregroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryBlue),
                                ),
                                onPressed: () async {
                                  showDialog(
                                      context: context,
                                      builder: (context) => CustomPercentageAlertDialog(
                                            bloc: bloc,
                                          ));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Text(S.current.chooseCustomPercentage),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  ActivityTabMenu(activity: activity),
                  const SizedBox(
                    height: 4,
                  ),
                  if (activity.workPackageId != null) Padding(padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10), child: 
                    ElevatedButton(
                      onPressed: () async {
                        final workPackage = await AppLinker().getLinkedWorkPackage(activity.workPackageId ?? 0);
                        workPackage.fold(
                          (workPackage) {
                            jumpToWorkpackagesDetailsScreen(context: context, workpackageDTO: workPackage);
                          },
                          (exception) {
                            showDialog(
                              context: context, 
                              builder: (context) => AlertDialog(
                                title: Text(AppLinkerException.getErrorTitle(exception)),
                                content: Text(AppLinkerException.getErrorMessage(exception)),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                    style: TextButton.styleFrom(
                                      foregroundColor: AppTheme.pgPrimaryRed,
                                      textStyle: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      )
                                    ),
                                    child: Text(S.of(context).coreWarningDialogOkay.toUpperCase()),
                                  )
                                ],
                              )
                            );
                          }
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppTheme.pgPrimaryRed,
                        minimumSize: const Size.fromHeight(42)
                      ),
                      child: const Text("View STO Planner Work Package", style: TextStyle(fontWeight: FontWeight.w700)),
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  const Divider(),
                  Container(
                    margin: const EdgeInsets.only(
                      left: 18,
                    ),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 12,
                        ),
                        Text(
                          activity.description ?? '',
                          style:
                              TextStyle(fontSize: Theme.of(context).textTheme.titleLarge?.fontSize, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          activity.activity ?? '',
                          style: TextStyle(fontSize: Theme.of(context).textTheme.titleSmall?.fontSize),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                      ],
                    ),
                  ),
                  ActivityFieldsTab(
                    activity: activity,
                    bloc: bloc,
                  ),
                ],
              ),
              //Barrier for unable to work panel
              Visibility(
                visible: activity.unableToWork ?? false,
                child: ModalBarrier(
                  dismissible: false,
                  color: Colors.black.withOpacity(0.4),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
