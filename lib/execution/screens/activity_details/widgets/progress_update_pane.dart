import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/activity_details_screen/activity_details_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';

class ProgressUpdatePane extends StatelessWidget {
  final ActivityDetailsScreenBloc bloc;

  const ProgressUpdatePane({super.key, required this.bloc});
  // final Future<bool?> Function() dialog;
  // const ProgressUpdatePane({super.key, required this.activityId, required this.dialog});

  progressCallback(BuildContext context, double percentage) async {
    bloc.add(ActivityDetailsScreenEvent.updateActivityProgress(
      updateActivityModel: UpdatedActivityDTO(
        percentComplete: percentage,
      )
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RoundActivityPercentageButton(
            percentage: 0.25,
            progressCallBack: () => progressCallback(context, 0.25),
          ),
          RoundActivityPercentageButton(
            percentage: 0.50,
            progressCallBack: () => progressCallback(context, 0.5),
          ),
          RoundActivityPercentageButton(
            percentage: 0.75,
            progressCallBack: () => progressCallback(context, 0.75),
          ),
          RoundActivityPercentageButton(
            percentage: 1.00,
            progressCallBack: () => progressCallback(context, 1),
          ),
        ],
      ),
    );
  }
}

class RoundActivityPercentageButton extends StatelessWidget {
  final double percentage;

  final VoidCallback progressCallBack;

  const RoundActivityPercentageButton({super.key, required this.percentage, required this.progressCallBack});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        enableFeedback: true,
        backgroundColor: MaterialStateProperty.all(Theme.of(context).unselectedWidgetColor),
        foregroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryBlue),
        shape: MaterialStateProperty.all(const CircleBorder()),
        padding: MaterialStateProperty.all(const EdgeInsets.all(2.5)),
      ),
      onPressed: () async {
        progressCallBack();
        // final confirm = await dialog();
        // if (confirm == null || !confirm) return;
        // bool? allowedToProgress = true;
        // if (await activityController.commentNeeded(percentage))
        //   allowedToProgress = await showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return CommentDialog(
        //         activityId: _activityId,
        //         title: S.current.addComment,
        //         text: S.current.provideNegativeProgressReason,
        //       );
        //     },
        //   );
      },
      autofocus: false,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Text(
          "${(percentage * 100).toInt()}%",
          style: const TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
