import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/utils/date_formatter.dart';

class ActivityReadyToStartBanner extends StatelessWidget {
  final String dateTime;

  const ActivityReadyToStartBanner({super.key, required this.dateTime});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: AppTheme.pgAccentSuccessTint,
      child: Column(
        children: [
          Text(
            S.of(context).readyToStart,
            style: const TextStyle(color: AppTheme.pgPrimaryBlack, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Text(DateFormatter.handleDisplayDate(dateTimeString: dateTime))
        ],
      ),
    );
  }
}
