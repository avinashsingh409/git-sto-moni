import 'package:sto_mobile_v2/core/utils/date_formatter.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/enums/field_value_type_enum.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';

String getNodeString({required GroupingTreeNodeDTO node}) {
  var nodeName = node.nodeValue;
  if (nodeName != null) {
    if (nodeName.contains("<[%%]>")) {
      final tempNodeName = nodeName.split('<[%%]>');
      nodeName = tempNodeName[tempNodeName.length - 1];
    }
    if (node.fieldValueTypeEnum == FieldValueTypeEnum.percentage) {
      final floatPercent = double.tryParse(nodeName);
      if (floatPercent != null) {
        final percentageString = "${(floatPercent * 100).toStringAsFixed(0)}%";
        return percentageString;
      }
    }
    return DateFormatter.handleDisplayDate(dateTimeString: nodeName);
  }
  return '';
}
