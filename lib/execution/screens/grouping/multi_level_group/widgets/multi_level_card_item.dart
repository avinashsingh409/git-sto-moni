import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/group_string_helper.dart';

class MultiLevelCarddItem extends StatelessWidget {
  final GroupingTreeNodeDTO groupingTreeNode;
  const MultiLevelCarddItem({
    super.key,
    required this.groupingTreeNode,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 5,
        color: Colors.white,
        margin: const EdgeInsets.all(5),
        borderOnForeground: true,
        child: Row(
          children: [
            const SizedBox(
              height: 60,
              width: 17,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  //
                  color: Colors.green,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      getNodeString(node: groupingTreeNode),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
