import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/group_string_helper.dart';

class WbsBreadCrumbCardItem extends StatelessWidget {
  final GroupingTreeNodeDTO treeNode;
  const WbsBreadCrumbCardItem({super.key, required this.treeNode});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      color: Colors.white,
      margin: const EdgeInsets.all(5),
      borderOnForeground: true,
      child: Row(
        children: [
          const SizedBox(
            height: 34,
            width: 34,
            child: Padding(
              padding: EdgeInsets.all(4.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
              ),
            ),
          ),
          Text(
            getNodeString(node: treeNode),
            textAlign: TextAlign.start,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
            ),
          ),
          const SizedBox(
            height: 50,
            width: 17,
          )
        ],
      ),
    );
  }
}
