import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/execution/application/grouping_activity/grouping_activity_bloc.dart';
import 'package:sto_mobile_v2/execution/application/multi_level_grouping/multi_level_grouping_bloc.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/widgets/grouping_breadcrumb_card_item.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/widgets/multi_level_card_item.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class MultiLevelGroupingScreen extends StatelessWidget {
  final Map<int, ActivityFieldDTO>? groupKeys;
  final bool isPrevGroupAvailable;
  const MultiLevelGroupingScreen({super.key, this.groupKeys, required this.isPrevGroupAvailable});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<MultiLevelGroupingBloc>();
    bloc.add(MultiLevelGroupingEvent.getInitialGrouping(groupKeys: groupKeys, isPrevGroupAvailable: isPrevGroupAvailable));
    return BlocProvider.value(
      value: bloc,
      child: BlocBuilder<MultiLevelGroupingBloc, MultiLevelGroupingState>(
        builder: (context, state) {
          return state.when(
            error: () => const Center(
              child: Text('errorMsg'),
            ),
            initial: () => const SizedBox(),
            loading: () => const Center(child: LoadingIndicator()),
            success: (List<GroupingTreeNodeDTO> treeElements) {
              return Column(
                children: [
                  LayoutBuilder(
                    builder: (BuildContext context, BoxConstraints viewportContraints) {
                      return SingleChildScrollView(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: viewportContraints.minHeight,
                            maxHeight: viewportContraints.minHeight + 48,
                            minWidth: viewportContraints.minWidth,
                            maxWidth: viewportContraints.maxWidth,
                          ),
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: bloc.selectedNodes.length,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    context.read<MultiLevelGroupingBloc>().add(MultiLevelGroupingEvent.getNodeOrActivities(
                                        currentNode: bloc.selectedNodes.elementAt(index).groupingTreeNodeDTO));
                                    bloc.returnNewLinkedListAfterKId(bloc.selectedNodes.elementAt(index));
                                  },
                                  child: WbsBreadCrumbCardItem(
                                    treeNode: bloc.selectedNodes.elementAt(index).groupingTreeNodeDTO,
                                  ),
                                );
                              }),
                        ),
                      );
                    },
                  ),
                  Expanded(
                    child: treeElements.isNotEmpty
                        ? ListView.builder(
                            itemCount: treeElements.length,
                            itemBuilder: (context, index) {
                              final currentNode = treeElements[index];
                              switch (treeElements[index].nodeElementType) {
                                case ElementTypeEnum.root:
                                  return GestureDetector(
                                      onTap: () => context
                                          .read<MultiLevelGroupingBloc>()
                                          .add(MultiLevelGroupingEvent.getNodeOrActivities(currentNode: currentNode)),
                                      child: MultiLevelCarddItem(groupingTreeNode: treeElements[index]));
                                case ElementTypeEnum.node:
                                  return GestureDetector(
                                      onTap: () => context
                                          .read<MultiLevelGroupingBloc>()
                                          .add(MultiLevelGroupingEvent.getNodeOrActivities(currentNode: currentNode)),
                                      child: MultiLevelCarddItem(groupingTreeNode: treeElements[index]));
                                case ElementTypeEnum.leaf:
                                  return MultiLevelActivityList(activityIds: currentNode.nodeValue ?? '');
                                default:
                                  return const SizedBox();
                              }
                            })
                        : const Center(
                            child: Text("No Data or Additional Groups with this Criteria"),
                          ),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}

class MultiLevelActivityList extends StatelessWidget {
  final String activityIds;
  const MultiLevelActivityList({
    super.key,
    required this.activityIds,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<GroupingActivityBloc>();
    bloc.add(GroupingActivityEvent.getGroupedActivities(activityIds: activityIds));
    return BlocProvider.value(
      value: bloc,
      child: BlocBuilder<GroupingActivityBloc, GroupingActivityState>(
        builder: (context, state) {
          return state.when(
              initial: () => const SizedBox(),
              loading: () => const LoadingIndicator(),
              success: (cardItems) => SizedBox(
                    height: MediaQuery.of(context).size.height * 0.63,
                    // ActivityListView has significantly changed; this page should no longer be necessary, but leaving it just in case.
                    // TODO: Reevaluate necessary code
                    // child: ActivityListView(
                    //   data: cardItems,
                    //   isFromHomeScreen: true,
                    // ),
                    child: const Center(child: Text("Not Implemented")),
                  ),
              error: (String errorMsg) {
                return Center(
                  child: Text(errorMsg),
                );
              });
        },
      ),
    );
  }
}
