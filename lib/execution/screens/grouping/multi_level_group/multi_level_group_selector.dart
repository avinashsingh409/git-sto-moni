import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/common/screens/widgets/sto_elevated_button.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/grouping_screen_selector/multi_level_grouping_selector/multi_level_grouping_selector_cubit.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class MultiLevelGroupSelector extends StatefulWidget {
  final Function(Map<int, ActivityFieldDTO>? groupKeys, Set<MultiLevelSortDTO>? sortParam) onValueChanged;
  const MultiLevelGroupSelector({
    super.key,
    required this.onValueChanged,
  });

  @override
  State<MultiLevelGroupSelector> createState() => _MultiLevelGroupSelectorState();
}

class _MultiLevelGroupSelectorState extends State<MultiLevelGroupSelector> {
  Map<int, ActivityFieldDTO> groupKeys = {};
  Set<MultiLevelSortDTO> sortParams = {};
  @override
  Widget build(BuildContext context) {
    final cubit = getIt<MultiLevelGroupingSelectorCubit>();
    cubit.getAllGroupableFields();
    return BlocProvider.value(
      value: cubit,
      child: BlocBuilder<MultiLevelGroupingSelectorCubit, MultiLevelGroupingSelectorState>(
        builder: (context, state) {
          return state.when(
            initial: () => const SizedBox(),
            loading: () => const Center(
              child: LoadingIndicator(),
            ),
            loaded: (groupableFields) => Center(
              child: Card(
                child: Container(
                  margin: const EdgeInsets.all(12),
                  height: MediaQuery.of(context).size.height * 0.60,
                  color: Colors.white,
                  child: Column(
                    children: [
                      const SizedBox(height: 20),
                      Text(
                        S.current.level1,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      DropdownSearch<ActivityFieldDTO>(
                        items: groupableFields,
                        itemAsString: (item) => item.field ?? '',
                        popupProps: PopupProps.menu(
                          showSearchBox: true,
                          searchFieldProps: TextFieldProps(
                            autofocus: false,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.search),
                              hintText: S.current.stoEventDropDownSearchBoxHintText,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        onChanged: (changedActivityField) {
                          if (changedActivityField != null) {
                            groupKeys[1] = changedActivityField;
                          }
                        },
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      Text(
                        S.current.level2,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      DropdownSearch<ActivityFieldDTO>(
                        items: groupableFields,
                        itemAsString: (item) => item.field ?? '',
                        popupProps: PopupProps.menu(
                          showSearchBox: true,
                          searchFieldProps: TextFieldProps(
                            autofocus: false,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.search),
                              hintText: S.current.stoEventDropDownSearchBoxHintText,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        onChanged: (changedActivityField) {
                          if (changedActivityField != null) {
                            groupKeys[2] = changedActivityField;
                          }
                        },
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      Text(
                        S.current.level3,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      DropdownSearch<ActivityFieldDTO>(
                        items: groupableFields,
                        itemAsString: (item) => item.field ?? '',
                        popupProps: PopupProps.menu(
                          showSearchBox: true,
                          searchFieldProps: TextFieldProps(
                            autofocus: false,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.search),
                              hintText: S.current.stoEventDropDownSearchBoxHintText,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        onChanged: (changedActivityField) {
                          if (changedActivityField != null) {
                            groupKeys[3] = changedActivityField;
                          }
                        },
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      Text(
                        S.current.levelSortField,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      DropdownSearch<ActivityFieldDTO>(
                        items: groupableFields.where((element) => element.fieldType == 1).toList(),
                        itemAsString: (item) => item.field ?? '',
                        popupProps: PopupProps.menu(
                          showSearchBox: true,
                          searchFieldProps: TextFieldProps(
                            autofocus: false,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.search),
                              hintText: S.current.stoEventDropDownSearchBoxHintText,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        onChanged: (sortByActivityField) {
                          sortParams.clear();
                          sortParams.add(MultiLevelSortDTO(
                            id: 1,
                            field: sortByActivityField?.dynamicFieldName ?? '',
                            level: 3,
                          ));
                        },
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      STOElevatedButton(
                        buttonName: 'Done',
                        onPressed: () {
                          if (groupKeys.isNotEmpty) {
                            List<int> keys = groupKeys.keys.toList()..sort();
                            var lastElement = keys[keys.length - 1];
                            if (keys.length != lastElement) {
                              //Catch err Here
                            } else {
                              widget.onValueChanged(groupKeys, sortParams);
                            }
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            noGroupConfiguration: () => const Center(
              child: Text('No Groupable Fields'),
            ),
          );
        },
      ),
    );
  }
}
