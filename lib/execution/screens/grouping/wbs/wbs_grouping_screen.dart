import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/execution/application/wbs/wbs_bloc.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/widgets/wbs_grouping_body.dart';

import '../../../../injectable/injection.dart';

class WbsGroupingScreen extends StatelessWidget {
  final TreeNodeDTO wbsNode;
  const WbsGroupingScreen({super.key, required this.wbsNode});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<WbsBloc>();
    bloc.add(WbsEvent.getWbsGrouping(selectedNode: wbsNode, level: wbsNode.level! + 1));

    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<WbsBloc, WbsState>(
        builder: (context, state) {
          return state.when(
            error: (errorMsg) => Center(
              child: Text(errorMsg),
            ),
            initial: () => const SizedBox(),
            loading: () => const Center(child: LoadingIndicator()),
            loaded: (treeElements) {
              return Scaffold(
                appBar: AppBar(title: const Text("Work Package")),
                body: WBSGroupingBody(bloc: bloc, treeElements: treeElements ?? [])
              );
            }
          );
        },
      ),
    );
  }
}