import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/execution/application/wbs/wbs_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/widgets/wbs_grouping_body.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class ActivityWbsGroupedListView extends StatefulWidget {
  const ActivityWbsGroupedListView({
    Key? key,
  }) : super(key: key);

  @override
  State<ActivityWbsGroupedListView> createState() => _ActivityWbsGroupedListViewState();
}

class _ActivityWbsGroupedListViewState extends State<ActivityWbsGroupedListView> {
  final bloc = getIt<WbsBloc>();
  @override
  void initState() {
    bloc.add(const WbsEvent.getInitialGrouping());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<WbsBloc, WbsState>(
        builder: (context, state) {
          return state.when(
            error: (errorMsg) => Center(
              child: Text(errorMsg),
            ),
            initial: () => const SizedBox(),
            loading: () => const Center(child: LoadingIndicator()),
            loaded: (treeElements) {
              return WBSGroupingBody(bloc: bloc, treeElements: treeElements ?? []);
            }
          );
        },
      ),
    );
  }
}
