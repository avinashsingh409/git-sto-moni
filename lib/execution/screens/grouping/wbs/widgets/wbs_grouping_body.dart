import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/execution/application/wbs/wbs_bloc.dart';
import 'package:sto_mobile_v2/execution/application/wbs_activity/wbs_activity_bloc.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/activity_card_item.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/widgets/wbs_breadcrumb_card_item.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/widgets/wbs_level_card_item.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class WBSGroupingBody extends StatelessWidget {
  final List<TreeNodeDTO> treeElements;
  final WbsBloc bloc;

  const WBSGroupingBody({
    super.key,
    required this.bloc,
    required this.treeElements,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportContraints) {
            return treeElements.isNotEmpty ? SingleChildScrollView(child: 
              ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportContraints.minHeight,
                  maxHeight: viewportContraints.minHeight + 48,
                  minWidth: viewportContraints.minWidth,
                  maxWidth: viewportContraints.maxWidth,
                ),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: bloc.selectedNodes.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        bloc.add(WbsEvent.getWbsGrouping(
                          selectedNode: bloc.selectedNodes.elementAt(index).treeNodeDTO,
                          level: bloc.selectedNodes.elementAt(index).treeNodeDTO.level! + 1,
                        ));
                        bloc.returnNewLinkedListAfterKId(bloc.selectedNodes.elementAt(index).treeNodeDTO.level! + 1);
                      },
                      child: WbsBreadCrumbCardItem(
                        treeNode: bloc.selectedNodes.elementAt(index).treeNodeDTO,
                      ),
                    );
                  }
                ),
              ),
            ) : const SizedBox();
          },
        ),
        Expanded(
          child: treeElements.isNotEmpty ? ListView.builder(
            itemCount: treeElements.length,
            itemBuilder: (context, index) {
              final currentNode = treeElements[index];
              switch (treeElements[index].nodeElementType) {
                case ElementTypeEnum.root:
                  return GestureDetector(
                    onTap: () => bloc.add(WbsEvent.getWbsGrouping(selectedNode: currentNode, level: currentNode.level! + 1)),
                    child: WBSLevelCardItem(treeNode: treeElements[index])
                  );
                case ElementTypeEnum.node:
                  return GestureDetector(
                    onTap: () => bloc.add(WbsEvent.getWbsGrouping(selectedNode: currentNode, level: currentNode.level! + 1)),
                    child: WBSLevelCardItem(treeNode: treeElements[index])
                  );
                case ElementTypeEnum.leaf:
                  return WbsActivityList(activity: currentNode.nodeName!);
                default:
                  return const SizedBox();
              }
            }
          ) : const Center(child: Text("No Activities with this Criteria")),
        ),
      ],
    );
  }
}

class WbsActivityList extends StatelessWidget {
  final String activity;
  const WbsActivityList({
    super.key,
    required this.activity,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<WbsActivityBloc>();
    bloc.add(WbsActivityEvent.getWbsActivities(activity: activity));
    return BlocProvider.value(
      value: bloc,
      child: BlocBuilder<WbsActivityBloc, WbsActivityState>(
        builder: (context, state) {
          return state.when(
              initial: () => const SizedBox(),
              loading: () => const LoadingIndicator(),
              success: (cardItem) => ActivityCardItem(activityCardItemModel: cardItem, isSwipeAllowed: false),
              error: (String errorMsg) {
                return Center(
                  child: Text(errorMsg),
                );
              });
        },
      ),
    );
  }
}
