import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/enums/element_type_enum.dart';

class WBSLevelCardItem extends StatelessWidget {
  final TreeNodeDTO treeNode;
  const WBSLevelCardItem({
    super.key,
    required this.treeNode,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 5,
        color: Colors.white,
        margin: const EdgeInsets.all(5),
        borderOnForeground: true,
        child: Row(
          children: [
            const SizedBox(
              height: 60,
              width: 17,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  //
                  color: Colors.green,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      treeNode.nodeElementType == ElementTypeEnum.node
                          ? getWbsNodeString(nodeName: treeNode.nodeName!)
                          : treeNode.nodeName!,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getWbsNodeString({required String nodeName}) {
    final elements = nodeName.split('<[%%]>');
    final parsedNodeName = elements[1];
    return parsedNodeName;
  }
}
