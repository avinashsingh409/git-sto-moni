import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/execution/application/grouping_screen_selector/grouping_screen_selector_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/multi_level_group_selector.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/multi_level_group/multi_level_grouping_screen.dart';
import 'package:sto_mobile_v2/execution/screens/grouping/wbs/wbs_screen.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class GroupingScreen extends StatelessWidget {
  const GroupingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<GroupingScreenSelectorBloc>();
    return BlocProvider.value(
      value: bloc,
      child: BlocConsumer<GroupingScreenSelectorBloc, GroupingScreenSelectorState>(
        listener: (context, state) {
          state.when(
            initial: () => {},
            wbsSelected: () => {},
            multiLevelSelected: (_, __) => {},
            multiLevelConfigurePopUp: () => showDialog(
              context: context,
              builder: (context) => MultiLevelGroupSelector(
                onValueChanged: (Map<int, ActivityFieldDTO>? groupKeys, searchPram) {
                  if (groupKeys?.entries != null) {
                    bloc.add(GroupingScreenSelectorEvent.filterMultiLevelGrouping(groupKeys: groupKeys, sortParams: searchPram));
                    Navigator.of(context).pop();
                  }
                },
              ),
            ),
          );
        },
        builder: (context, state) {
          return state.when(
            initial: () => const OptionsRow(),
            wbsSelected: () => const SizedBox(
              child: GroupResultPane(
                groupWidget: ActivityWbsGroupedListView(),
                groupName: 'WBS Grouping',
              ),
            ),
            multiLevelSelected: (groupKeys, isPrevGroupAvailable) => SizedBox(
              child: GroupResultPane(
                groupWidget: MultiLevelGroupingScreen(
                  groupKeys: groupKeys ?? {},
                  isPrevGroupAvailable: isPrevGroupAvailable,
                ),
                groupName: 'Multi-Level Grouping',
              ),
            ),
            multiLevelConfigurePopUp: () => const OptionsRow(),
          );
        },
      ),
    );
  }
}

class GroupResultPane extends StatelessWidget {
  final String groupName;
  final Widget groupWidget;
  const GroupResultPane({
    super.key,
    required this.groupWidget,
    required this.groupName,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 4,
        ),
        Stack(
          children: [
            IconButton(
              onPressed: () => context.read<GroupingScreenSelectorBloc>().add(const GroupingScreenSelectorEvent.started()),
              icon: const Icon(
                Icons.arrow_back,
                size: 32,
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(14.0),
                child: Text(groupName, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              ),
            ),
            groupName.contains("Multi")
                ? Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: GestureDetector(
                        onTap: () => context
                            .read<GroupingScreenSelectorBloc>()
                            .add(const GroupingScreenSelectorEvent.clearMultiLevelGrouping()),
                        child: Container(
                          width: 80,
                          height: 32,
                          decoration: BoxDecoration(
                            color: Colors.red.shade400,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(8),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 7,
                                offset: const Offset(2, 3),
                              ),
                            ],
                          ),
                          margin: const EdgeInsets.only(
                            top: 8,
                          ),
                          padding: const EdgeInsets.only(top: 2, right: 4),
                          child: const Center(
                            child: Text(
                              'Reset',
                              style: TextStyle(fontWeight: FontWeight.w800, color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        const Divider(color: Colors.black45),
        Flexible(child: groupWidget),
      ],
    );
  }
}

class OptionsRow extends StatelessWidget {
  const OptionsRow({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 18),
      child: Column(
        children: [
          const Text('Grouping Options', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
          const SizedBox(
            height: 12,
          ),
          const Divider(color: Colors.black45),
          const SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () => context.read<GroupingScreenSelectorBloc>().add(const GroupingScreenSelectorEvent.getWbsGrouping()),
                child: Container(
                  height: 160,
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: const [
                        0.2,
                        0.9,
                      ],
                      colors: [
                        Colors.red.shade200,
                        Colors.orangeAccent.shade200,
                      ],
                    ),
                    color: Colors.red,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  child: const Center(
                    child: Text(
                      'WBS',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              GestureDetector(
                onTap: () => context.read<GroupingScreenSelectorBloc>().add(const GroupingScreenSelectorEvent.selectMultiLevel()),
                child: Container(
                  height: 160,
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: const [
                        0.2,
                        0.9,
                      ],
                      colors: [
                        Colors.blue.shade400,
                        Colors.red.shade200,
                      ],
                    ),
                    color: Colors.red,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  child: const Center(
                    child: Text(
                      'Multi-Level',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
