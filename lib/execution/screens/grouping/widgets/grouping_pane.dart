import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/application/grouping_screen_selector/multi_level_grouping_selector/multi_level_grouping_selector_cubit.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/enums/grouping_option_enum.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';

import '../../../../injectable/injection.dart';

// ----------------------------------------------------------------------------------------------------
// Grouping Pane
// ----------------------------------------------------------------------------------------------------

class GroupingPane extends StatefulWidget {
  const GroupingPane({super.key});

  @override
  State<GroupingPane> createState() => _GroupingPaneState();
}

class _GroupingPaneState extends State<GroupingPane> {
  final MultiLevelGroupingSelectorCubit cubit = getIt<MultiLevelGroupingSelectorCubit>();

  @override
  Widget build(BuildContext context) {
    cubit.getAllGroupableFields();
    var levelOne = SelectedGrouping.levelOne;
    var levelTwo = SelectedGrouping.levelTwo;
    var levelThree = SelectedGrouping.levelThree;

    return AlertDialog(
      title: const Text("Multi-Level Grouping", style: TextStyle(fontWeight: FontWeight.bold)),
      // --------------------------------------------------
      // field selectors
      // --------------------------------------------------
      content: BlocProvider(
        create: (context) => cubit,
        child: BlocBuilder<MultiLevelGroupingSelectorCubit, MultiLevelGroupingSelectorState>(
          builder: (context, state) {
            return state.when(
              initial: () => const SizedBox(),
              loading: () => const LoadingIndicator(),
              noGroupConfiguration: () => const Center(child: Text("No Groupable Fields")),
              loaded: (fields) {
                return StatefulBuilder(
                  builder: (context, setState) {
                    return Theme(
                      data: Theme.of(context).copyWith(unselectedWidgetColor: AppTheme.pgPrimaryBlack),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Divider(
                            color: AppTheme.pgPrimaryBlack,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Column(children: [
                              const SizedBox(height: 10),
                              // --------------------------------------------------
                              // Level One
                              // --------------------------------------------------
                              Text(S.current.level1),
                              const SizedBox(height: 5),
                              DropdownSearch<ActivityFieldDTO>(
                                items: fields,
                                itemAsString: (field) => field.field ?? "",
                                selectedItem: levelOne,
                                popupProps: PopupProps.menu(
                                  showSearchBox: true,
                                  searchFieldProps: TextFieldProps(
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      prefixIcon: const Icon(Icons.search),
                                      hintText: S.current.stoEventDropDownSearchBoxHintText,
                                      border: const OutlineInputBorder()
                                    ),
                                  ),
                                ),
                                onChanged: (field) => setState(() {levelOne = field;}),
                              ),
                              const SizedBox(height: 20),
                              // --------------------------------------------------
                              // Level Two
                              // --------------------------------------------------
                              Text(S.current.level2),
                              const SizedBox(height: 5),
                              DropdownSearch<ActivityFieldDTO>(
                                items: fields,
                                itemAsString: (field) => field.field ?? "",
                                selectedItem: levelTwo,
                                popupProps: PopupProps.menu(
                                  showSearchBox: true,
                                  searchFieldProps: TextFieldProps(
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      prefixIcon: const Icon(Icons.search),
                                      hintText: S.current.stoEventDropDownSearchBoxHintText,
                                      border: const OutlineInputBorder()
                                    ),
                                  ),
                                ),
                                onChanged: (field) => setState(() {levelTwo = field;}),
                              ),
                              const SizedBox(height: 20),
                              // --------------------------------------------------
                              // Level Three
                              // --------------------------------------------------
                              Text(S.current.level3),
                              const SizedBox(height: 5),
                              DropdownSearch<ActivityFieldDTO>(
                                items: fields,
                                itemAsString: (field) => field.field ?? "",
                                selectedItem: levelThree,
                                popupProps: PopupProps.menu(
                                  showSearchBox: true,
                                  searchFieldProps: TextFieldProps(
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      prefixIcon: const Icon(Icons.search),
                                      hintText: S.current.stoEventDropDownSearchBoxHintText,
                                      border: const OutlineInputBorder()
                                    ),
                                  ),
                                ),
                                onChanged: (field) => setState(() {levelThree = field;}),
                              ),
                            ])
                          )
                        ]
                      )
                    );
                  }
                );
              }
            );
          }
        )
      ),
      // --------------------------------------------------
      // actions
      // --------------------------------------------------
      actions: [
        Padding(padding: const EdgeInsets.only(bottom: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  if (_validateFieldSelections(levelOne, levelTwo, levelThree)) {
                    SelectedGrouping().selectMultiLevel(levelOne, levelTwo, levelThree);
                    BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
                    Navigator.of(context).pop();
                  }
                },
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Confirm'),
                ),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Cancel'),
                ),
              ),
            ],
          ),
        )
      ]
    );
  }

  bool _validateFieldSelections(ActivityFieldDTO? levelOne, ActivityFieldDTO? levelTwo, ActivityFieldDTO? levelThree) {
    // at least one field must be chosen
    if (levelOne == null && levelTwo == null && levelThree == null) return false;

    // map the non-null chosen fields by level
    final list = [levelOne, levelTwo, levelThree];
    Map<int, ActivityFieldDTO> map = {};
    for (int i = 1; i <= 3; i++) {
      if (list[i - 1] != null) {
        map[i] = list[i - 1]!;
      }
    }

    // validate the length against the keys
    final keys = map.keys.toList()..sort();
    final last = keys[keys.length - 1];
    return keys.length == last;
  }
}

// ----------------------------------------------------------------------------------------------------
// Grouping Selector Icon
// ----------------------------------------------------------------------------------------------------

class GroupingIcon extends StatefulWidget {
  const GroupingIcon({super.key});

  @override
  State<GroupingIcon> createState() => _GroupingIconState();
}

class _GroupingIconState extends State<GroupingIcon> {
  GroupingOptionsEnum selectedGrouping = SelectedGrouping.selectedOption;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: const Icon(Icons.account_tree),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<GroupingOptionsEnum>>[
        PopupMenuItem<GroupingOptionsEnum>(
          value: GroupingOptionsEnum.wbs,
          child: const Text("WBS Grouping"),
          onTap: () {
            SelectedGrouping().selectWbs();
            BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
          },
        ),
        PopupMenuItem<GroupingOptionsEnum>(
          value: GroupingOptionsEnum.multiLevel,
          child: const Text("Multi-Level Grouping"),
          onTap: () {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              showDialog(
                context: context,
                builder: (_) {
                  return BlocProvider.value(
                    value: BlocProvider.of<ActivityListScreenBloc>(context),
                    child: const GroupingPane(),
                  );
                } 
              );
            });
          },
        ),
        const PopupMenuDivider(),
        PopupMenuItem<GroupingOptionsEnum>(
          value: GroupingOptionsEnum.clear,
          child: const Text("Clear"),
          onTap: () {
            SelectedGrouping().clearGrouping();
            BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
          },
        ),
      ]
    );
  }
}

// ----------------------------------------------------------------------------------------------------
// Selected Grouping Singleton
// ----------------------------------------------------------------------------------------------------

@lazySingleton
class SelectedGrouping {
  static GroupingOptionsEnum selectedOption = GroupingOptionsEnum.clear;
  static ActivityFieldDTO? levelOne;
  static ActivityFieldDTO? levelTwo;
  static ActivityFieldDTO? levelThree;

  List<ActivityFieldDTO?> getSelectedLevels() => [levelOne, levelTwo, levelThree];

  void selectWbs() {
    selectedOption = GroupingOptionsEnum.wbs;
    levelOne = null;
    levelTwo = null;
    levelThree = null;
  }

  void selectMultiLevel(ActivityFieldDTO? one, ActivityFieldDTO? two, ActivityFieldDTO? three) {
    selectedOption = GroupingOptionsEnum.multiLevel;
    levelOne = one;
    levelTwo = two;
    levelThree = three;
  }
  
  void clearGrouping() {
    selectedOption = GroupingOptionsEnum.clear;
    levelOne = null;
    levelTwo = null;
    levelThree = null;
  }
}