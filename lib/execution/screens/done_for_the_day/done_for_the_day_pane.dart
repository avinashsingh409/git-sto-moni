import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/done_for_the_day/done_for_the_day_cubit.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class DoneForTheDayPane extends StatelessWidget {
  const DoneForTheDayPane({super.key});

  @override
  Widget build(BuildContext context) {
    final cubit = getIt<DoneForTheDayCubit>();
    cubit.initializePane();
    return BlocProvider.value(
      value: cubit,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: BlocBuilder<DoneForTheDayCubit, DoneForTheDayState>(
              builder: (context, state) {
                return state.when(
                    initial: () => const SizedBox(),
                    valueChanged: (isDone) {
                      return CheckboxListTile(
                          value: isDone,
                          activeColor: AppTheme.pgPrimaryBlue,
                          tileColor: isDone ? const Color(0xffEAF2F8) : null,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: isDone ? const Color(0xffB0D7F4) : Colors.grey.shade300,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          title: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                              S.current.doneForToday,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: AppTheme.pgPrimaryBlue,
                              ),
                            ),
                          ),
                          onChanged: (isDoneForTheDay) {
                            if (isDoneForTheDay != null) {
                              cubit.updatedDoneForTheDay(isDoneForTheDay: isDoneForTheDay);
                            }
                          });
                    },
                    loading: () => const Center(
                          child: CircularProgressIndicator(),
                        ));
              },
            ),
          ),
        ),
      ),
    );
  }
}
