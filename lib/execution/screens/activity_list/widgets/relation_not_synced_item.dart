import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/predecessor_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/successors_activity_dto.dart';

class PredecessorNotSyncedItem extends StatelessWidget {
  final PredecessorActivityDTO? predecessorActivityDTO;
  const PredecessorNotSyncedItem({super.key, this.predecessorActivityDTO});

  @override
  Widget build(BuildContext context) {
    return RelationCommonCard(
      activity: predecessorActivityDTO?.activity,
      description: predecessorActivityDTO?.description,
    );
  }
}

class SuccessorNotSyncedItem extends StatelessWidget {
  final SuccessorsActivityDTO? successorActivityDTO;
  const SuccessorNotSyncedItem({super.key, this.successorActivityDTO});

  @override
  Widget build(BuildContext context) {
    return RelationCommonCard(
      activity: successorActivityDTO?.activity,
      description: successorActivityDTO?.description,
    );
  }
}

class RelationCommonCard extends StatelessWidget {
  final String? activity;
  final String? description;
  const RelationCommonCard({
    super.key,
    required this.activity,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        elevation: 5,
        margin: const EdgeInsets.all(5),
        borderOnForeground: true,
        child: Row(
          children: [
            const SizedBox(
              height: 75,
              width: 17,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: AppTheme.pgPrimaryRed,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "$activity - $description",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: AppTheme.pgPrimaryRed,
                        fontWeight: FontWeight.bold,
                        fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
