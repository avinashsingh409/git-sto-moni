import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/activity_card_item.dart';

class ActivityListView extends StatelessWidget {
  final List<WbsLevelStyleDTO> colors;
  final bool multiLevel;
  const ActivityListView({
    Key? key,
    required this.data,
    required this.colors,
    required this.multiLevel,
  }) : super(key: key);

  final Map<String, List<ActivityCardItemModel>> data;

  @override
  Widget build(BuildContext context) {
    if (data.isNotEmpty) {
      // handle flat list
      if (data.length == 1 && data.containsKey("")) {
        final activities = data[""]!;
        if (activities.isNotEmpty) {
          return ListView.builder(
            itemCount: activities.length,
            prototypeItem: ActivityCardItem(activityCardItemModel: activities[0], isSwipeAllowed: false),
            itemBuilder: (context, index) {
              final activityListItem = activities[index];
              return ActivityCardItem(activityCardItemModel: activityListItem, isSwipeAllowed: false);
            }
          );
        }
      }

      // handle grouping
      else {
        return Padding(padding: const EdgeInsets.only(bottom: 5),
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              final key = data.keys.elementAt(index);
              final route = key.split("[**]");
              final color = colors.where((color) => color.level == (multiLevel ? route.length : route.length - 1)).firstOrNull;
              final headerColor = HexColor(color?.backgroundColor ?? "#FFFFFF");
              final textColor = HexColor(color?.color ?? "#000000");
              final activities = data[key]!;
              // list of groupings
              return Container(
                decoration: const BoxDecoration(color: Colors.white, border: Border.symmetric(horizontal: BorderSide())),
                margin: const EdgeInsets.only(bottom: 5),
                child: ExpansionTile(
                  title: SizedBox(height: 20,
                    // list of levels in route
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: route.length * 2 - 1,
                      itemBuilder: (context, index) {
                        if (index.isOdd) {
                          return const Padding(padding: EdgeInsets.symmetric(horizontal: 10), child: Icon(Icons.chevron_right, size: 20));
                        }
                        return Text(route[index ~/ 2]);
                      }
                    )
                  ),
                  shape: const Border(),
                  collapsedIconColor: textColor,
                  textColor: textColor,
                  iconColor: textColor,
                  collapsedTextColor: textColor,
                  collapsedBackgroundColor: headerColor,
                  backgroundColor: headerColor,
                  children: [
                    // list of activities
                    Container(
                      decoration: const BoxDecoration(color: Colors.white, border: BorderDirectional(top: BorderSide())),
                      child: ListView.builder(
                        itemCount: activities.length,
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          final activityListItem = activities[index];
                          return ActivityCardItem(activityCardItemModel: activityListItem, isSwipeAllowed: false);
                        },
                      )
                    )
                  ]
                )
              );
            }
          )
        );
      }
    }
    return const Center(
      child: Text("No activities"),
    );
  }
}