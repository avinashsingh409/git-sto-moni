import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/application/grouping_screen_selector/multi_level_grouping_selector/multi_level_grouping_selector_cubit.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

// ----------------------------------------------------------------------------------------------------
// Sort Pane
// ----------------------------------------------------------------------------------------------------

class SortPane extends StatefulWidget {
  const SortPane({super.key});

  @override
  State<SortPane> createState() => _SortPaneState();
}

class _SortPaneState extends State<SortPane> {
  final MultiLevelGroupingSelectorCubit cubit = getIt<MultiLevelGroupingSelectorCubit>();
  
  @override
  Widget build(BuildContext context) {
    cubit.getAllGroupableFields();
    var sortField = SelectedSort.sortField;
    var ascending = SelectedSort.ascending;

    return AlertDialog(
      // --------------------------------------------------
      // title and clear
      // --------------------------------------------------
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Sort By:', style: TextStyle(fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.only(top: 2),
            child: SizedBox(
              height: 30,
              width: 100,
              child: ElevatedButton(
                onPressed: () {
                  SelectedSort.sortField = null;
                  SelectedSort.ascending = true;
                  BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
                  Navigator.of(context).pop();
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.grey[500]),
                ),
                child: const Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 5,
                  ),
                  child: Text('X  Clear'),
                ),
              ),
            ),
          ),
        ],
      ),
      // --------------------------------------------------
      // field and direction selectors
      // --------------------------------------------------
      content: BlocProvider(
        create: (context) => cubit,
        child: BlocBuilder<MultiLevelGroupingSelectorCubit, MultiLevelGroupingSelectorState>(
          builder: (context, state) {
            return state.when(
              initial: () => const SizedBox(),
              loading: () => const LoadingIndicator(),
              noGroupConfiguration: () => const Center(child: Text("No Sortable Fields")),
              loaded: (fields) {
                return StatefulBuilder(
                  builder: (context, setState) {
                    return Theme(
                      data: Theme.of(context).copyWith(unselectedWidgetColor: AppTheme.pgPrimaryBlack),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Divider(
                            color: AppTheme.pgPrimaryBlack,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Column(children: [
                              const SizedBox(height: 5),
                              // --------------------------------------------------
                              // field selector
                              // --------------------------------------------------
                              DropdownSearch<ActivityFieldDTO>(
                                items: fields,
                                itemAsString: (field) => field.field ?? "",
                                selectedItem: sortField,
                                popupProps: PopupProps.menu(
                                  showSearchBox: true,
                                  searchFieldProps: TextFieldProps(
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      prefixIcon: const Icon(Icons.search),
                                      hintText: S.current.stoEventDropDownSearchBoxHintText,
                                      border: const OutlineInputBorder()
                                    ),
                                  ),
                                ),
                                onChanged: (field) => setState(() {sortField = field;}),
                              ),
                              // --------------------------------------------------
                              // direction selector
                              // --------------------------------------------------
                              RadioListTile<bool>(
                                title: const Text("Ascending"),
                                value: true,
                                groupValue: ascending,
                                onChanged: (field) => setState(() {ascending = true;})
                              ),
                              RadioListTile<bool>(
                                title: const Text("Descending"),
                                value: false,
                                groupValue: ascending,
                                onChanged: (field) => setState(() {ascending = false;})
                              )
                            ])
                          )
                        ]
                      )
                    );
                  }
                );
              }
            );
          }
        )
      ),
      // --------------------------------------------------
      // actions
      // --------------------------------------------------
      actions: [
        Padding(
          padding: const EdgeInsets.only(bottom: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  SelectedSort.sortField = sortField;
                  SelectedSort.ascending = ascending;
                  BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
                  Navigator.of(context).pop();
                },
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Sort'),
                ),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Cancel'),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

// ----------------------------------------------------------------------------------------------------
// Selected Sort Singleton
// ----------------------------------------------------------------------------------------------------

@lazySingleton
class SelectedSort {
  static ActivityFieldDTO? sortField;
  static bool ascending = true;
}