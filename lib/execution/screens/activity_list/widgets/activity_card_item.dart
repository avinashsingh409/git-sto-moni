import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/core/utils/date_formatter.dart';
import 'package:sto_mobile_v2/execution/application/swipe_to_complete/swipe_to_complete_bloc.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/activity_card_item_model.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/enums/primavera_activity_status_enum.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:swipe_to/swipe_to.dart';

class ActivityCardItem extends StatefulWidget {
  final ActivityCardItemModel activityCardItemModel;
  final bool isSwipeAllowed;
  const ActivityCardItem({super.key, required this.activityCardItemModel, required this.isSwipeAllowed});

  @override
  State<ActivityCardItem> createState() => _ActivityCardItemState();
}

class _ActivityCardItemState extends State<ActivityCardItem> {
  final bloc = getIt<SwipeToCompleteBloc>();
  @override
  Widget build(BuildContext context) {
    Color progressColor = AppTheme.pgPrimaryGray;
    if (widget.activityCardItemModel.displayedPercentComplete >= 0 &&
        widget.activityCardItemModel.displayedPercentComplete < 0.75) {
      progressColor = Colors.red.shade700;
    } else if (widget.activityCardItemModel.displayedPercentComplete >= 0.75 &&
        widget.activityCardItemModel.displayedPercentComplete < 1) {
      progressColor = Colors.orange.shade700;
    } else if (widget.activityCardItemModel.displayedPercentComplete == 1) {
      progressColor = Colors.green.shade700;
    }

    Color activityFlagColor = AppTheme.pgPrimaryGray;
    if (widget.activityCardItemModel.unableToWork == true) {
      activityFlagColor = Colors.red.shade700;
    } else if (widget.activityCardItemModel.predecessorsStatus != null &&
        widget.activityCardItemModel.predecessorsStatus != PrimaveraActivityStatus.completed) {
      activityFlagColor = Colors.orange.shade700;
    } else if (widget.activityCardItemModel.predecessorsStatus != null &&
        widget.activityCardItemModel.predecessorsStatus == PrimaveraActivityStatus.completed) {
      activityFlagColor = Colors.green.shade700;
    } else {
      activityFlagColor = Colors.blue;
    }

    return BlocProvider.value(
      value: bloc,
      child: Center(
        child: GestureDetector(
          onTap: () async {
            jumpToTurnAroundActivitiesDetailsScreen(
                context: context, activityId: widget.activityCardItemModel.id, activity: widget.activityCardItemModel.activity);
          },
          child: SwipeTo(
            animationDuration: const Duration(milliseconds: 300),
            onRightSwipe: () {
              if (!widget.activityCardItemModel.unableToWork) {
                bloc.add(SwipeToCompleteEvent.swiped(
                    activityId: widget.activityCardItemModel.id, updateActivityModel: UpdatedActivityDTO(percentComplete: 1)));
              }
            },
            child: Card(
              elevation: 5,
              color: Colors.white,
              margin: const EdgeInsets.all(5),
              borderOnForeground: true,
              child: Row(
                children: [
                  SizedBox(
                    height: 75,
                    width: 17,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: activityFlagColor,
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(4),
                          bottomLeft: Radius.circular(4),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(
                              height: 2,
                            ),
                            const Icon(
                              Icons.comment,
                              size: 14,
                              color: Colors.white,
                            ),
                            const SizedBox(
                              height: 2,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 2),
                              child: Text(
                                widget.activityCardItemModel.commentCount == null
                                    ? '0'
                                    : widget.activityCardItemModel.commentCount.toString(),
                                style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 12, color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "${widget.activityCardItemModel.activity} - ${widget.activityCardItemModel.description}",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Theme.of(context).textTheme.titleMedium?.fontSize,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  S.current.startDate,
                                  style: TextStyle(
                                    fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                                    color: Colors.grey.shade700,
                                    fontStyle: FontStyle.italic,
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  S.current.endDate,
                                  style: TextStyle(
                                    fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                                    color: Colors.grey.shade700,
                                    fontStyle: FontStyle.italic,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Text(
                                  DateFormatter.handleDisplayDate(
                                      dateTimeString: widget.activityCardItemModel.displayDate ?? "", long: false),
                                  style: TextStyle(
                                    fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                                    overflow: TextOverflow.ellipsis,
                                    color: Colors.grey.shade700,
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Text(
                                  DateFormatter.handleDisplayDate(
                                      dateTimeString: widget.activityCardItemModel.displayFinishDate ?? "", long: false),
                                  style: TextStyle(
                                    fontSize: Theme.of(context).textTheme.titleSmall?.fontSize,
                                    overflow: TextOverflow.ellipsis,
                                    color: Colors.grey.shade700,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  !widget.activityCardItemModel.unableToWork
                      ? CircularPercentIndicator(
                          radius: 35,
                          center: Text("${(widget.activityCardItemModel.displayedPercentComplete * 100).toInt()}%"),
                          percent: widget.activityCardItemModel.displayedPercentComplete,
                          progressColor: progressColor,
                        )
                      : SizedBox(
                          height: 60,
                          width: 60,
                          child: Icon(Icons.warning_amber_rounded, color: Colors.red.shade700, size: 52),
                        ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
