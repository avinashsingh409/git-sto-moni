import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/application/stack_filter/stack_filter_bloc.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/enums/filter_type_enum.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

// ----------------------------------------------------------------------------------------------------
// Filter Pane
// ----------------------------------------------------------------------------------------------------

class FilterPane extends StatefulWidget {
  const FilterPane({super.key});

  @override
  State<FilterPane> createState() => _FilterPaneState(); 
}

class _FilterPaneState extends State<FilterPane> {

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<StackFilterBloc>();
    bloc.add(const StackFilterEvent.getStackFilters());
    var selectedStaticFilters = SelectedFilters.selectedStaticFilters;
    var selectedStackFilters = SelectedFilters.selectedStackFilters;

    return AlertDialog(
      // --------------------------------------------------
      // title and clear
      // --------------------------------------------------
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Filter By:', style: TextStyle(fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.only(top: 2),
            child: SizedBox(
              height: 30,
              width: 100,
              child: ElevatedButton(
                onPressed: () {
                  SelectedFilters().clearFilters();
                  BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
                  Navigator.of(context).pop();
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.grey[500]),
                ),
                child: const Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 5,
                  ),
                  child: Text('X  Clear'),
                ),
              ),
            ),
          ),
        ],
      ),
      // --------------------------------------------------
      // filter list
      // --------------------------------------------------
      content: BlocProvider.value(
        value: bloc,
        child: BlocBuilder<StackFilterBloc, StackFilterState>(
          builder: (context, state) {
            return state.maybeWhen(
              orElse: () => const SizedBox(height: 20),
              error: (message) => Center(child: Text(message)),
              loading: () => const LoadingIndicator(),
              success: (stackFilters) {
                return StatefulBuilder(
                  builder: (context, setState) {
                    return Theme(
                      data: Theme.of(context).copyWith(unselectedWidgetColor: AppTheme.pgPrimaryBlack),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Divider(
                            color: AppTheme.pgPrimaryBlack,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ListView.builder(
                              itemCount: stackFilters.length + 4,
                              itemBuilder: (context, index) {
                                // --------------------------------------------------
                                // static filters
                                // --------------------------------------------------
                                if (index == 0) {
                                  return CheckboxListTile(
                                    title: const Text("Look Ahead"),
                                    value: selectedStaticFilters.contains(FilterTypeEnum.lookahead),
                                    onChanged: (value) {
                                      const filter = FilterTypeEnum.lookahead;
                                      toggleStaticFilter(filter, selectedStaticFilters);
                                      setState(() {
                                        selectedStaticFilters = selectedStaticFilters;
                                      });
                                    }
                                  );
                                } else if (index == 1) {
                                  return CheckboxListTile(
                                    title: const Text("Not Completed"),
                                    value: selectedStaticFilters.contains(FilterTypeEnum.notCompleted),
                                    onChanged: (value) {
                                      const filter = FilterTypeEnum.notCompleted;
                                      toggleStaticFilter(filter, selectedStaticFilters);
                                      setState(() {
                                        selectedStaticFilters = selectedStaticFilters;
                                      });
                                    }
                                  );
                                } else if (index == 2) {
                                  return CheckboxListTile(
                                    title: const Text("Overdue Activities"),
                                    value: selectedStaticFilters.contains(FilterTypeEnum.overdueActivities),
                                    onChanged: (value) {
                                      const filter = FilterTypeEnum.overdueActivities;
                                      toggleStaticFilter(filter, selectedStaticFilters);
                                      setState(() {
                                        selectedStaticFilters = selectedStaticFilters;
                                      });
                                    }
                                  );
                                } else if (index == 3) {
                                  return CheckboxListTile(
                                    title: const Text("Critical Path"),
                                    value: selectedStaticFilters.contains(FilterTypeEnum.criticalPath),
                                    onChanged: (value) {
                                      const filter = FilterTypeEnum.criticalPath;
                                      toggleStaticFilter(filter, selectedStaticFilters);
                                      setState(() {
                                        selectedStaticFilters = selectedStaticFilters;
                                      });
                                    }
                                  );
                                }
                                // --------------------------------------------------
                                // stack filters
                                // --------------------------------------------------
                                // adjust index to use stack filter list
                                index -= 4;
                                final stackFilter = stackFilters[index];
                                return CheckboxListTile(
                                  title: Text(stackFilter.name ?? ""),
                                  value: selectedStackFilters.contains(stackFilter.id), 
                                  onChanged: (value) {
                                    if (stackFilter.id != null) {
                                      toggleStackFilter(stackFilter.id!, selectedStackFilters);
                                      setState(() {
                                        selectedStackFilters = selectedStackFilters;
                                      });
                                    }
                                  }
                                );
                              }
                            )
                          )
                        ]
                      )
                    );
                  }
                );
              }
            );
          }
        )
      ),
      // --------------------------------------------------
      // actions
      // --------------------------------------------------
      actions: [
        Padding(
          padding: const EdgeInsets.only(bottom: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  SelectedFilters().setSelectedFilters(selectedStaticFilters, selectedStackFilters);
                  BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
                  Navigator.of(context).pop();
                },
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Filter'),
                ),
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).pop(),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryRed)),
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Text('Cancel'),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  void toggleStaticFilter(FilterTypeEnum filter, List<FilterTypeEnum> filterList) {
    if (filterList.contains(filter)) {
      filterList.remove(filter);
    } else {
      filterList.add(filter);
    }
  }

  void toggleStackFilter(int filter, List<int> filterList) {
    if (filterList.contains(filter)) {
      filterList.remove(filter);
    } else {
      filterList.add(filter);
    }
  }
}

// ----------------------------------------------------------------------------------------------------
// Selected Filter Singleton
// ----------------------------------------------------------------------------------------------------

@lazySingleton
class SelectedFilters {
  static List<FilterTypeEnum> selectedStaticFilters = [];
  static List<int> selectedStackFilters = [];

  void setSelectedFilters(List<FilterTypeEnum> staticFilters, List<int> stackFilters) {
    selectedStaticFilters = staticFilters;
    selectedStackFilters = stackFilters;
  }

  void clearFilters() {
    selectedStaticFilters = [];
    selectedStackFilters = [];
  }

  int size() {
    return selectedStaticFilters.length + selectedStackFilters.length;
  }
}
