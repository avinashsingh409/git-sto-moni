import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/turnaround_event_selector_cubit.dart/turnaround_event_selector_cubit.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/execution/application/activity_list_screen/activity_list_screen_bloc.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/sort_pane.dart';
import 'package:sto_mobile_v2/execution/screens/execution_search/widgets/execution_search_bar.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/activity_list_style.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/activity_list_view.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/execution/screens/activity_list/widgets/filter_pane.dart';

class ActivityListScreen extends StatelessWidget {
  const ActivityListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ActivityListScreenBloc>(context).add(const ActivityListScreenEvent.getActivities());
    final turnaroundEventSelectorCubit = getIt<TurnaroundEventSelectorCubit>();
    turnaroundEventSelectorCubit.getCurrentSelectedTurnaroundEvent();

    return BlocProvider.value(
      value: BlocProvider.of<ActivityListScreenBloc>(context),
      child: Column(
        children: [
          const SizedBox(
            height: 12,
          ),
          // --------------------------------------------------
          // Header and Filters
          // --------------------------------------------------
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: Padding(padding: const EdgeInsets.symmetric(horizontal: 8),
                child: BlocProvider.value(
                  value: turnaroundEventSelectorCubit,
                  child: BlocBuilder<TurnaroundEventSelectorCubit, TurnaroundEventSelectorState>(
                    builder: (context, state) {
                      return state.when(
                        initial: () => const Text(""),
                        loading: () => const Text(""),
                        turnAroundEventChanged: (_) => const Text(""),
                        selectedEvent: (event) => Text(
                          event,
                          style: TextStyle(
                            fontSize: Theme.of(context).textTheme.titleLarge?.fontSize,
                            color: AppTheme.pgPrimaryBlack,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                        )
                      );
                    }
                  )
                )
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  BlocBuilder<ActivityListScreenBloc, ActivityListScreenState>(
                    builder: (context, state) {
                      return state.maybeWhen(
                        orElse: () => const SizedBox(),
                        success: (_, __, filterCount, ___, ____) {
                          return Text("${filterCount > 0 ? filterCount : "No"} ${filterCount == 1 ? "filter" : "filters"} applied", style: filterTitleStyle);
                        }
                      );
                    },
                  ),
                  const SizedBox(width: 12),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (_) {
                          return BlocProvider.value(
                            value: BlocProvider.of<ActivityListScreenBloc>(context),
                            child: const FilterPane(),
                          );
                        },
                      );
                    },
                    child: const Padding(
                      padding: EdgeInsets.only(right: 12),
                      child: Icon(
                        size: 32,
                        Icons.filter_alt,
                        color: AppTheme.pgPrimaryRed,
                      ),
                    ),
                  ),
                ],
              ),
            ]
          ),
          const SizedBox(height: 12),
          // --------------------------------------------------
          // Activity Count and Sort
          // --------------------------------------------------
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(padding: const EdgeInsets.symmetric(horizontal: 8),
                child: BlocBuilder<ActivityListScreenBloc, ActivityListScreenState>(
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox(),
                      loading: () => const SizedBox(),
                      error: (_) => const SizedBox(),
                      success: (_, count, __, ___, ____) {
                        return Text("${count ?? 0} ${(count ?? 0) == 1 ? "Activity" : "Activities"}",
                          style: const TextStyle(fontWeight: FontWeight.w400),
                        );
                      }
                    );
                  }
                )
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    BlocBuilder<ActivityListScreenBloc, ActivityListScreenState>(
                      builder: (context, state) {
                        return state.when(
                          initial: () => const SizedBox(),
                          loading: () => const SizedBox(),
                          error: (_) => const SizedBox(),
                          success: (_, __, ___, ____, _____) {
                            final sortField = SelectedSort.sortField?.field;
                            final icon = SelectedSort.ascending ? const Icon(Icons.arrow_upward, size: 15) : const Icon(Icons.arrow_downward, size: 15);
                            if (sortField != null) {
                              return Expanded(child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: Text("Sort By: $sortField", style: filterTitleStyle, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end)
                                  ),
                                  Padding(padding: const EdgeInsets.only(left: 5), child: icon)
                                ]
                              ));
                            }
                            return const Expanded(
                              child: Text("No sort applied", style: filterTitleStyle, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end)
                            );
                          }
                        );
                      }
                    ),
                    const SizedBox(width: 12),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (_) {
                            return BlocProvider.value(
                              value: BlocProvider.of<ActivityListScreenBloc>(context),
                              child: const SortPane(),
                            );
                          },
                        );
                      },
                      child: const Padding(
                        padding: EdgeInsets.only(right: 12),
                        child: Icon(
                          size: 32,
                          Icons.sort,
                          color: AppTheme.pgPrimaryRed,
                        ),
                      ),
                    ),
                  ],
                )
              ),
            ]
          ),
          const SizedBox(height: 12),
          // --------------------------------------------------
          // Search Bar
          // --------------------------------------------------
          const Padding(padding: EdgeInsets.symmetric(horizontal: 8), 
            child: ExecutionSearchBar()
          ),
          const SizedBox(height: 12),
          // --------------------------------------------------
          // Activities List
          // --------------------------------------------------
          BlocBuilder<ActivityListScreenBloc, ActivityListScreenState>(
            builder: (context, state) {
              return state.when(
                initial: () => const SizedBox(),
                loading: () => const LoadingIndicator(),
                error: (errorMsg) => Center(child: Text(errorMsg)),
                success: (activities, _, __, colors, multiLevel) {
                  return Expanded(
                    child: ActivityListView(
                      data: activities,
                      colors: colors,
                      multiLevel: multiLevel,
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
