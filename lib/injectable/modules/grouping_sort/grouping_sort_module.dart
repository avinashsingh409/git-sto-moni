import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/data/grouping/selected_grouping_sort.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/repository/activity_grouping_repository.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

@lazySingleton
class GroupingSortModule {
  static Future<void> initializeGroupingSort() async {
    IActivityGroupingRepository activityGroupingRepository = getIt<IActivityGroupingRepository>();
    final selectedMultiLevelSort = activityGroupingRepository.getSelectedMultiLevelSort();
    if (selectedMultiLevelSort == null) {
      SelectedMultiLevelSort().updateSelectedMultiLevelSort(selectedMultiLevelSort: selectedMultiLevelSort);
    }
  }
}
