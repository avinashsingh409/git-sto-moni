// import 'dart:io';

// import 'package:flutter_appcenter_bundle/flutter_appcenter_bundle.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
// import 'package:injectable/injectable.dart';

// @singleton
// class AppCenterConfig {
//   static Future initializeAppCenter() async {
//     if (Platform.isAndroid || Platform.isIOS) {
//       await AppCenter.startAsync(
//         appSecretAndroid: dotenv.env['APPCENTER_APP_SECRET_ANDROID'].toString(),
//         appSecretIOS: dotenv.env['APPCENTER_APP_SECRET_IOS'].toString(),
//         enableDistribute: false,
//       );
//       await AppCenter.configureDistributeDebugAsync(enabled: false);
//     }
//   }
// }
