import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/network/interceptor/api_header_interceptor.dart';
import 'package:sto_mobile_v2/common/data/network/interceptor/base_url_interceptor.dart';
import 'package:sto_mobile_v2/common/data/network/interceptor/exception_interceptor.dart';
import 'package:sto_mobile_v2/common/data/network/interceptor/network_interceptor.dart';
import 'package:sto_mobile_v2/core/logger/http_formatter.dart';

@module
abstract class NetworkModule {
  HttpFormatter get httpFormatter => HttpFormatter(
        includeRequestHeaders: true,
        includeHeaders: ["Content-Type", "Accept", "Authorization"],
      );

  Dio getDio(
    BaseUrlInterceptor baseUrlInterceptor,
    HttpFormatter httpFormatter,
    ApiHeadersInterceptor apiHeaders,
    ExceptionInterceptor exceptionInterceptor,
    NetworkConnectionInterceptor networkConnectionInterceptor,
  ) {
    final dio = Dio(
      BaseOptions(
        connectTimeout: 4 * 1000, // 4 seconds
      ),
    );
    dio
      ..interceptors.add(baseUrlInterceptor)
      ..interceptors.add(networkConnectionInterceptor)
      ..interceptors.add(apiHeaders)
      ..interceptors.add(exceptionInterceptor);
    if (!kReleaseMode) {
      dio.interceptors.add(httpFormatter);
    }
    return dio;
  }
}
