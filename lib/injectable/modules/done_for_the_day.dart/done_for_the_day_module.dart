import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

@lazySingleton
class DoneForTheDayModule {
  static Future<void> initializeDoneForTheDay() async {
    IDoneForTheDayRepository doneForTheDayRepository = getIt<IDoneForTheDayRepository>();
    final lastDoneForTheDay = doneForTheDayRepository.getDoneForTheDay();
    if (lastDoneForTheDay == null) {
      await doneForTheDayRepository.intiailizeDoneForTheDay();
    }
  }
}
