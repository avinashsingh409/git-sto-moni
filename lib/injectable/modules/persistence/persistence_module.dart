import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';
import 'package:sto_mobile_v2/common/data/sync_ticker/datastore/models/sync_ticker_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_dto.dart';
import 'package:sto_mobile_v2/execution/data/blocking_reason/datastore/models/blocking_reason_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/comment/datastore/models/updated_comment_dto.dart';
import 'package:sto_mobile_v2/execution/data/done_for_the_day/datastore/models/done_for_the_day_dto.dart';
import 'package:sto_mobile_v2/execution/data/filter_config/datastore/models/execution_filter_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/grouping_tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/grouping/datastore/models/multi_level_sort_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/activity_field_dto.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/datastore/models/dtos/updated_activity_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/wbs_levelstyle_dto.dart';
import 'package:sto_mobile_v2/execution/data/stack_filter/datastore/models/stack_filter_dto.dart';
import 'package:sto_mobile_v2/planner/data/attachments/datastore/models/attachment_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/contractor_options.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/static_field_definition_value_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/user_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/datastore/models/work_type_options_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackagegrouping/datastore/models/workpackagegrouping_dto.dart';

@singleton
class PersistenceModule {
  static late Isar isar;
  static Future initializeIsar() async {
    final dir = await getApplicationSupportDirectory();
    isar = await Isar.open([
      AppConfigDTOSchema,
      UserDTOSchema,
      SessionDTOSchema,
      TurnAroundEventDTOSchema,
      ActivityDTOSchema,
      ActivityFieldDTOSchema,
      CommentDTOSchema,
      UpdatedActivityDTOSchema,
      UpdatedCommentDTOSchema,
      WbsLevelStyleDTOSchema,
      GroupingTreeNodeDTOSchema,
      TreeNodeDTOSchema,
      AppLogsDTOSchema,
      DoneForTheDayDTOSchema,
      WorkpackageDTOSchema,
      WorkpackageGroupingDTOSchema,
      FieldDefinitionValueDTOSchema,
      StaticFieldDefinitionValueDTOSchema,
      AttachmentDTOSchema,
      UserOptionsDTOSchema,
      WorkTypeOptionsDTOSchema,
      ContractorOptionsDTOSchema,
      LicenseDTOSchema,
      SyncTickerDTOSchema,
      BlockingReasonDTOSchema,
      ExecutionFilterDTOSchema,
      MultiLevelSortDTOSchema,
      StackFilterDTOSchema,
    ], directory: dir.path);
  }
}
