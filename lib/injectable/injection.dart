import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/injectable/modules/done_for_the_day.dart/done_for_the_day_module.dart';
import 'package:sto_mobile_v2/injectable/modules/grouping_sort/grouping_sort_module.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

import '../config/dependency/dependencies.dart';
import '../config/dependency/environment_config.dart';
import 'injection.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
Future<void> configureDependencies() async {
  await EnvironmentConfig.intialize();
  await $initGetIt(
    getIt,
    environment: kReleaseMode ? Environment.prod : Environment.dev,
  );
  getIt<Dependencies>();
  // await AppCenterConfig.initializeAppCenter();
  await PersistenceModule.initializeIsar();
  await DoneForTheDayModule.initializeDoneForTheDay();
  await GroupingSortModule.initializeGroupingSort();
}
