// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i117;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i58;

import '../common/application/app_switcher/app_switcher_bloc.dart' as _i194;
import '../common/application/error_404/error_404_bloc.dart' as _i219;
import '../common/application/login/login_bloc.dart' as _i108;
import '../common/application/logout/logout_bloc.dart' as _i211;
import '../common/application/profile_drawer/profile_drawer_cubit.dart'
    as _i111;
import '../common/application/qr_code/qr_code_scanner_bloc.dart' as _i47;
import '../common/application/report_problem/report_problem_cubit.dart' as _i48;
import '../common/application/server_credentials/server_credentials_bloc.dart'
    as _i55;
import '../common/application/splash/splash_bloc.dart' as _i113;
import '../common/application/sync_ticker/sync_ticker_bloc.dart' as _i115;
import '../common/application/turnaround_data_cache/turnaround_data_cache_bloc.dart'
    as _i233;
import '../common/application/turnaround_event/turnaround_event_bloc.dart'
    as _i189;
import '../common/application/turnaround_event_selector_cubit.dart/turnaround_event_selector_cubit.dart'
    as _i190;
import '../common/application/user_call/user_call_bloc.dart' as _i191;
import '../common/data/app_config/app_config_dao.dart' as _i6;
import '../common/data/app_config/datastore/app_config_datastore_impl.dart'
    as _i29;
import '../common/data/app_config/repository/app_config_repository_impl.dart'
    as _i31;
import '../common/data/app_switcher/datastore/app_switcher_datastore_impl.dart'
    as _i122;
import '../common/data/app_switcher/repository/app_switcher_repository_impl.dart'
    as _i124;
import '../common/data/data_loss/datastore/data_loss_datastore_impl.dart'
    as _i87;
import '../common/data/data_loss/repository/data_loss_repository_impl.dart'
    as _i89;
import '../common/data/exception/app_logs_dao.dart' as _i8;
import '../common/data/exception/datastore/app_logs_datastore_impl.dart'
    as _i33;
import '../common/data/exception/repository/app_logs_repository_impl.dart'
    as _i35;
import '../common/data/license/datastore/license_datastore_impl.dart' as _i152;
import '../common/data/license/license_api_service.dart' as _i129;
import '../common/data/license/license_dao.dart' as _i42;
import '../common/data/license/repository/license_repository_impl.dart'
    as _i154;
import '../common/data/network/interceptor/api_header_interceptor.dart'
    as _i116;
import '../common/data/network/interceptor/base_url_interceptor.dart' as _i77;
import '../common/data/network/interceptor/exception_interceptor.dart' as _i78;
import '../common/data/network/interceptor/network_interceptor.dart' as _i45;
import '../common/data/network/repository/network_info_impl.dart' as _i41;
import '../common/data/session/datastore/logout_datastore_impl.dart' as _i204;
import '../common/data/session/datastore/session_datastore_impl.dart' as _i101;
import '../common/data/session/managers/session_preference_manager.dart'
    as _i112;
import '../common/data/session/repository/logout_repository_impl.dart' as _i206;
import '../common/data/session/repository/session_repository_impl.dart'
    as _i103;
import '../common/data/session/session_dao.dart' as _i56;
import '../common/data/session/session_openid_client.dart' as _i57;
import '../common/data/sync_ticker/datastore/sync_ticker_datastore_impl.dart'
    as _i105;
import '../common/data/sync_ticker/repository/sync_ticker_repository_impl.dart'
    as _i107;
import '../common/data/sync_ticker/sync_ticker_dao.dart' as _i61;
import '../common/data/turnaround_data/repository/turnaround_data_repository_impl.dart'
    as _i229;
import '../common/data/turnaround_event/datastore/turnaround_event_datastore_impl.dart'
    as _i166;
import '../common/data/turnaround_event/repository/turnaround_event_repository_impl.dart'
    as _i168;
import '../common/data/turnaround_event/turnaround_event_api_service.dart'
    as _i133;
import '../common/data/turnaround_event/turnaround_event_dao.dart' as _i63;
import '../common/data/user_calls/datastore/user_call_datastore_impl.dart'
    as _i174;
import '../common/data/user_calls/repository/user_call_repository_impl.dart'
    as _i176;
import '../common/data/user_calls/user_call_api_service.dart' as _i134;
import '../common/data/user_calls/user_call_dao.dart' as _i66;
import '../common/domain/app_config/datastore/app_config_datastore.dart'
    as _i28;
import '../common/domain/app_config/repository/app_config_repository.dart'
    as _i30;
import '../common/domain/app_switcher/datastore/app_switcher_datastore.dart'
    as _i121;
import '../common/domain/app_switcher/repository/app_switcher_repository.dart'
    as _i123;
import '../common/domain/data_loss/datastore/data_loss_datastore.dart' as _i86;
import '../common/domain/data_loss/repository/data_loss_repository.dart'
    as _i88;
import '../common/domain/exception/datastore/app_logs_datastore.dart' as _i32;
import '../common/domain/exception/repository/app_logs_repository.dart' as _i34;
import '../common/domain/license/datastore/license_datastore.dart' as _i151;
import '../common/domain/license/repository/license_repository.dart' as _i153;
import '../common/domain/network/network_info.dart' as _i40;
import '../common/domain/session/datastore/logout_datastore.dart' as _i203;
import '../common/domain/session/datastore/session_datastore.dart' as _i100;
import '../common/domain/session/repository/logout_repository.dart' as _i205;
import '../common/domain/session/repository/session_repository.dart' as _i102;
import '../common/domain/sync_ticker/datastore/snyc_ticker_datastore.dart'
    as _i104;
import '../common/domain/sync_ticker/repository/sync_ticker_repository.dart'
    as _i106;
import '../common/domain/turnaround_data/repository/turnaround_data_repository.dart'
    as _i228;
import '../common/domain/turnaround_event/datastore/turnaround_event_datastore.dart'
    as _i165;
import '../common/domain/turnaround_event/repository/turnaround_event_repository.dart'
    as _i167;
import '../common/domain/user_call/datastore/user_call_datastore.dart' as _i173;
import '../common/domain/user_call/repository/user_call_repository.dart'
    as _i175;
import '../config/dependency/dependencies.dart' as _i14;
import '../config/dependency/development_dependencies.dart' as _i16;
import '../config/dependency/production_dependencies.dart' as _i15;
import '../core/logger/http_formatter.dart' as _i27;
import '../core/utils/app_linker.dart' as _i7;
import '../execution/application/activity_details_screen/activity_details_screen_bloc.dart'
    as _i217;
import '../execution/application/activity_list_screen/activity_list_screen_bloc.dart'
    as _i234;
import '../execution/application/blocking_reasons/blocking_reasons_bloc.dart'
    as _i196;
import '../execution/application/comment/comment_bloc.dart' as _i218;
import '../execution/application/done_for_the_day/done_for_the_day_cubit.dart'
    as _i142;
import '../execution/application/execution_search/execution_search_bloc.dart'
    as _i119;
import '../execution/application/filter_config/filter_config_bloc.dart' as _i79;
import '../execution/application/grouping_activity/grouping_activity_bloc.dart'
    as _i225;
import '../execution/application/grouping_screen_selector/grouping_screen_selector_bloc.dart'
    as _i235;
import '../execution/application/grouping_screen_selector/multi_level_grouping_selector/multi_level_grouping_selector_cubit.dart'
    as _i231;
import '../execution/application/multi_level_grouping/multi_level_grouping_bloc.dart'
    as _i230;
import '../execution/application/predecessors/predecessors_bloc.dart' as _i110;
import '../execution/application/stack_filter/stack_filter_bloc.dart' as _i188;
import '../execution/application/successors/successors_bloc.dart' as _i114;
import '../execution/application/swipe_to_complete/swipe_to_complete_bloc.dart'
    as _i212;
import '../execution/application/sync/sync_bloc.dart' as _i232;
import '../execution/application/sync_changes_notifier/sync_changes_notifier_bloc.dart'
    as _i213;
import '../execution/application/wbs/wbs_bloc.dart' as _i192;
import '../execution/application/wbs_activity/wbs_activity_bloc.dart' as _i214;
import '../execution/data/activity_relation/datastore/activity_relations_datastore_impl.dart'
    as _i83;
import '../execution/data/activity_relation/repository/activity_relations_repository_impl.dart'
    as _i85;
import '../execution/data/blocking_reason/blocking_reason_api_service.dart'
    as _i140;
import '../execution/data/blocking_reason/blocking_reason_dao.dart' as _i11;
import '../execution/data/blocking_reason/datastore/blocking_reason_datastore_impl.dart'
    as _i148;
import '../execution/data/blocking_reason/repository/blocking_reason_repository_impl.dart'
    as _i150;
import '../execution/data/comment/comment_api_service.dart' as _i141;
import '../execution/data/comment/comment_dao.dart' as _i12;
import '../execution/data/comment/datastore/turnaround_comment_datastore_impl.dart'
    as _i170;
import '../execution/data/comment/repository/turnaround_comment_repository_impl.dart'
    as _i172;
import '../execution/data/comment/updated_comment_dao.dart' as _i64;
import '../execution/data/done_for_the_day/datastore/done_for_the_day_datastore_impl.dart'
    as _i126;
import '../execution/data/done_for_the_day/done_for_the_day_api_service.dart'
    as _i118;
import '../execution/data/done_for_the_day/done_for_the_day_dao.dart' as _i17;
import '../execution/data/done_for_the_day/repository/done_for_the_day_repository_impl.dart'
    as _i128;
import '../execution/data/execution_search/datastore/execution_search_datastore_impl.dart'
    as _i91;
import '../execution/data/execution_search/repository/execution_search_repository_impl.dart'
    as _i93;
import '../execution/data/execution_sync/datastore/execution_sync_datastore_impl.dart'
    as _i198;
import '../execution/data/execution_sync/execution_sync_api_service.dart'
    as _i120;
import '../execution/data/execution_sync/repository/execution_sync_repository_impl.dart'
    as _i200;
import '../execution/data/filter_config/datastore/filter_config_datastore_impl.dart'
    as _i37;
import '../execution/data/filter_config/execution_filter_dao.dart' as _i19;
import '../execution/data/filter_config/repository/filter_config_repository_impl.dart'
    as _i39;
import '../execution/data/grouping/activities_grouping_service.dart' as _i3;
import '../execution/data/grouping/datastore/activity_grouping_datastore_impl.dart'
    as _i81;
import '../execution/data/grouping/multi_level_grouping_dao.dart' as _i43;
import '../execution/data/grouping/multi_level_sort_dao.dart' as _i44;
import '../execution/data/grouping/repository/activity_grouping_repository_impl.dart'
    as _i227;
import '../execution/data/grouping/selected_grouping_sort.dart' as _i51;
import '../execution/data/stack_filter/datastore/stack_filter_datastore_impl.dart'
    as _i160;
import '../execution/data/stack_filter/repository/stack_filter_api_service.dart'
    as _i131;
import '../execution/data/stack_filter/repository/stack_filter_dao.dart'
    as _i59;
import '../execution/data/stack_filter/repository/stack_filter_repository_impl.dart'
    as _i162;
import '../execution/data/turnaround_activity/activity_field_dao.dart' as _i4;
import '../execution/data/turnaround_activity/datastore/turnaround_activity_datastore_impl.dart'
    as _i164;
import '../execution/data/turnaround_activity/mappers/activity_field_mapper.dart'
    as _i5;
import '../execution/data/turnaround_activity/repository/turnaround_activity_repository_impl.dart'
    as _i208;
import '../execution/data/turnaround_activity/turnaround_activity_api_service.dart'
    as _i132;
import '../execution/data/turnaround_activity/turnaround_activity_dao.dart'
    as _i62;
import '../execution/data/turnaround_activity/updated_turnaround_activity_dao.dart'
    as _i65;
import '../execution/data/wbs/datastore/wbs_datastore_impl.dart' as _i178;
import '../execution/data/wbs/repository/wbs_repository_impl.dart' as _i180;
import '../execution/data/wbs/wbs_api_service.dart' as _i135;
import '../execution/data/wbs/wbs_elements_dao.dart' as _i68;
import '../execution/data/wbs/wbs_grouping_service.dart' as _i69;
import '../execution/data/wbs/wbs_level_style_dao.dart' as _i70;
import '../execution/domain/activity_relation/datastore/activity_relations_datastore.dart'
    as _i82;
import '../execution/domain/activity_relation/repository/activity_relations_repository.dart'
    as _i84;
import '../execution/domain/blocking_reason/datastore/blocking_reason_datastore.dart'
    as _i147;
import '../execution/domain/blocking_reason/repository/blocking_reason_repository.dart'
    as _i149;
import '../execution/domain/comment/datastore/turnaround_comment_datastore.dart'
    as _i169;
import '../execution/domain/comment/repository/turnaround_comment_repository.dart'
    as _i171;
import '../execution/domain/done_for_the_day/datastore/done_for_the_day_datastore.dart'
    as _i125;
import '../execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart'
    as _i127;
import '../execution/domain/execution_search/datastore/execution_search_datastore.dart'
    as _i90;
import '../execution/domain/execution_search/repository/execution_search_repository.dart'
    as _i92;
import '../execution/domain/execution_sync/datastore/execution_sync_datastore.dart'
    as _i197;
import '../execution/domain/execution_sync/repository/execution_sync_repository.dart'
    as _i199;
import '../execution/domain/filter_config/datastore/filter_config_datastore.dart'
    as _i36;
import '../execution/domain/filter_config/repository/filter_config_repository.dart'
    as _i38;
import '../execution/domain/grouping/datastore/activity_grouping_datastore.dart'
    as _i80;
import '../execution/domain/grouping/repository/activity_grouping_repository.dart'
    as _i226;
import '../execution/domain/stack_filter/datastore/stack_filter_datastore.dart'
    as _i159;
import '../execution/domain/stack_filter/repository/stack_filter_repository.dart'
    as _i161;
import '../execution/domain/turnaround_activity/datastore/turnaround_activity_datastore.dart'
    as _i163;
import '../execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart'
    as _i207;
import '../execution/domain/wbs/datastore/wbs_datastore.dart' as _i177;
import '../execution/domain/wbs/repository/wbs_repository.dart' as _i179;
import '../execution/screens/activity_list/widgets/filter_pane.dart' as _i49;
import '../execution/screens/activity_list/widgets/sort_pane.dart' as _i53;
import '../execution/screens/execution_search/widgets/execution_search_bar.dart'
    as _i52;
import '../execution/screens/grouping/widgets/grouping_pane.dart' as _i50;
import '../planner/application/attachments/attachments_bloc.dart' as _i195;
import '../planner/application/field_definition/field_definition_bloc.dart'
    as _i220;
import '../planner/application/field_definition_types/field_type_boolean/field_type_boolean_cubit.dart'
    as _i23;
import '../planner/application/field_definition_types/field_type_contractor/field_type_contractor_bloc.dart'
    as _i221;
import '../planner/application/field_definition_types/field_type_date/field_type_date_cubit.dart'
    as _i24;
import '../planner/application/field_definition_types/field_type_lookup/field_type_lookup_cubit.dart'
    as _i25;
import '../planner/application/field_definition_types/field_type_planner/field_type_planner_bloc.dart'
    as _i222;
import '../planner/application/field_definition_types/field_type_primary_contractor/field_type_primary_contractor_bloc.dart'
    as _i223;
import '../planner/application/field_definition_types/field_type_work_type/field_type_work_type_bloc.dart'
    as _i224;
import '../planner/application/image_annotation/image_annotation_bloc.dart'
    as _i187;
import '../planner/application/planner_search/planner_search_bloc.dart'
    as _i109;
import '../planner/application/work_package_details/work_package_details_bloc.dart'
    as _i215;
import '../planner/application/workpackage_grouping_view/workpackage_grouping_view_bloc.dart'
    as _i193;
import '../planner/application/workpackage_list_screen/workpackage_list_screen_bloc.dart'
    as _i216;
import '../planner/application/workpackage_subgrouping_view/workpackage_subgrouping_view_bloc.dart'
    as _i76;
import '../planner/data/attachments/attachment_api_service.dart' as _i138;
import '../planner/data/attachments/attachments_dao.dart' as _i10;
import '../planner/data/attachments/datastore/attachment_datastore_impl.dart'
    as _i144;
import '../planner/data/attachments/repository/attachment_repository_impl.dart'
    as _i146;
import '../planner/data/field_definitions/contractor_options_dao.dart' as _i13;
import '../planner/data/field_definitions/datastore/field_definition_datastore_impl.dart'
    as _i95;
import '../planner/data/field_definitions/field_definition_value_dao.dart'
    as _i20;
import '../planner/data/field_definitions/field_definition_values_service.dart'
    as _i22;
import '../planner/data/field_definitions/mappers/field_definition_value_mapper.dart'
    as _i21;
import '../planner/data/field_definitions/repository/field_definition_repository_impl.dart'
    as _i202;
import '../planner/data/field_definitions/static_field_definition_value_dao.dart'
    as _i60;
import '../planner/data/field_definitions/user_options_dao.dart' as _i67;
import '../planner/data/field_definitions/work_type_options_dao.dart' as _i72;
import '../planner/data/planner_search/datastore/planner_search_datastore_impl.dart'
    as _i97;
import '../planner/data/planner_search/repository/planner_search_repository_impl.dart'
    as _i99;
import '../planner/data/planner_sync/attachment_sync_api_service.dart' as _i139;
import '../planner/data/planner_sync/datastore/planner_sync_datastore_impl.dart'
    as _i156;
import '../planner/data/planner_sync/planner_sync_api_service.dart' as _i130;
import '../planner/data/planner_sync/repository/planner_sync_repository_impl.dart'
    as _i158;
import '../planner/data/workpackage/datastore/models/workpackage_dynamic_columns.dart'
    as _i71;
import '../planner/data/workpackage/datastore/workpackage_datastore_impl.dart'
    as _i182;
import '../planner/data/workpackage/repository/workpackage_repository_impl.dart'
    as _i210;
import '../planner/data/workpackage/selected_workpackage.dart' as _i54;
import '../planner/data/workpackage/workpackage_api_service.dart' as _i136;
import '../planner/data/workpackage/workpackage_dao.dart' as _i73;
import '../planner/data/workpackagegrouping/datastore/workpackage_grouping_datastore_impl.dart'
    as _i184;
import '../planner/data/workpackagegrouping/repository/workpackage_grouping_repository_impl.dart'
    as _i186;
import '../planner/data/workpackagegrouping/workpackage_grouping_api_service.dart'
    as _i137;
import '../planner/data/workpackagegrouping/workpackage_grouping_dao.dart'
    as _i74;
import '../planner/domain/attachments/datastore/attachments_datastore.dart'
    as _i143;
import '../planner/domain/attachments/repository/attachments_repository.dart'
    as _i145;
import '../planner/domain/field_definitions/datastore/field_definitions_datastore.dart'
    as _i94;
import '../planner/domain/field_definitions/repository/field_definition_repository.dart'
    as _i201;
import '../planner/domain/planner_search/datastore/planner_search_datastore.dart'
    as _i96;
import '../planner/domain/planner_search/repository/planner_search_repository.dart'
    as _i98;
import '../planner/domain/planner_sync/datastore/planner_sync_datastore.dart'
    as _i155;
import '../planner/domain/planner_sync/repository/planner_sync_repository.dart'
    as _i157;
import '../planner/domain/workpackage/datastore/workpackage_datastore.dart'
    as _i181;
import '../planner/domain/workpackage/repository/workpackage_repository.dart'
    as _i209;
import '../planner/domain/workpackagegrouping/datastore/workpackage_grouping_datastore.dart'
    as _i183;
import '../planner/domain/workpackagegrouping/repository/workpackage_grouping_repository.dart'
    as _i185;
import '../planner/screens/workpackage_list/widgets/workpackage_filter_pane.dart'
    as _i75;
import 'modules/app_version/app_version_module.dart' as _i9;
import 'modules/core_modules.dart' as _i237;
import 'modules/done_for_the_day.dart/done_for_the_day_module.dart' as _i18;
import 'modules/grouping_sort/grouping_sort_module.dart' as _i26;
import 'modules/network/network_module.dart' as _i236;
import 'modules/persistence/persistence_module.dart' as _i46;

const String _prod = 'prod';
const String _dev = 'dev';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final networkModule = _$NetworkModule();
  final registerModule = _$RegisterModule();
  gh.factory<_i3.ActivitiesGroupingService>(
      () => _i3.ActivitiesGroupingService());
  gh.factory<_i4.ActivityFieldDAO>(() => _i4.ActivityFieldDAO());
  gh.factory<_i5.ActivityFieldMapper>(() => _i5.ActivityFieldMapper());
  gh.factory<_i6.AppConfigDAO>(() => _i6.AppConfigDAO());
  gh.lazySingleton<_i7.AppLinker>(() => _i7.AppLinker());
  gh.lazySingleton<_i8.AppLogsDAO>(() => _i8.AppLogsDAO());
  gh.singleton<_i9.AppVersionModule>(_i9.AppVersionModule());
  gh.factory<_i10.AttachmentsDAO>(() => _i10.AttachmentsDAO());
  gh.factory<_i11.BlockingReasonDAO>(() => _i11.BlockingReasonDAO());
  gh.factory<_i12.CommentDAO>(() => _i12.CommentDAO());
  gh.factory<_i13.ContractorOptionsDAO>(() => _i13.ContractorOptionsDAO());
  gh.lazySingleton<_i14.Dependencies>(
    () => _i15.ProductionDependenciesImpl(),
    registerFor: {_prod},
  );
  gh.lazySingleton<_i14.Dependencies>(
    () => _i16.DevelopmentDependenciesImpl(),
    registerFor: {_dev},
  );
  gh.lazySingleton<_i17.DoneForTheDayDAO>(() => _i17.DoneForTheDayDAO());
  gh.lazySingleton<_i18.DoneForTheDayModule>(() => _i18.DoneForTheDayModule());
  gh.lazySingleton<_i19.ExecutionFilterDAO>(() => _i19.ExecutionFilterDAO());
  gh.factory<_i20.FieldDefinitionValueDAO>(
      () => _i20.FieldDefinitionValueDAO());
  gh.factory<_i21.FieldDefinitionValueMapper>(
      () => _i21.FieldDefinitionValueMapper());
  gh.factory<_i22.FieldDefinitionValuesService>(
      () => _i22.FieldDefinitionValuesService());
  gh.factory<_i23.FieldTypeBooleanCubit>(
      () => _i23.FieldTypeBooleanCubit(get<_i21.FieldDefinitionValueMapper>()));
  gh.factory<_i24.FieldTypeDateCubit>(
      () => _i24.FieldTypeDateCubit(get<_i21.FieldDefinitionValueMapper>()));
  gh.factory<_i25.FieldTypeLookupCubit>(() => _i25.FieldTypeLookupCubit());
  gh.lazySingleton<_i26.GroupingSortModule>(() => _i26.GroupingSortModule());
  gh.factory<_i27.HttpFormatter>(() => networkModule.httpFormatter);
  gh.lazySingleton<_i28.IAppConfigDataStore>(
      () => _i29.AppConfigDataStoreImpl(get<_i6.AppConfigDAO>()));
  gh.lazySingleton<_i30.IAppConfigRepository>(
      () => _i31.AppConfigRepositoryImpl(get<_i28.IAppConfigDataStore>()));
  gh.lazySingleton<_i32.IAppLogsDataStore>(
      () => _i33.AppLogsDataStoreImpl(get<_i8.AppLogsDAO>()));
  gh.lazySingleton<_i34.IAppLogsRepository>(
      () => _i35.AppLogsRepositoryImpl(get<_i32.IAppLogsDataStore>()));
  gh.lazySingleton<_i36.IFilterConfigDataStore>(
      () => _i37.FilterConfigDataStoreImpl(get<_i19.ExecutionFilterDAO>()));
  gh.lazySingleton<_i38.IFilterConfigRepository>(() =>
      _i39.FilterConfigRepositoryImpl(get<_i36.IFilterConfigDataStore>()));
  gh.lazySingleton<_i40.INetworkInfo>(() => _i41.NetworkInfoImpl());
  gh.factory<_i42.LicenseDAO>(() => _i42.LicenseDAO());
  gh.factory<_i43.MultiLevelGroupingDao>(() => _i43.MultiLevelGroupingDao());
  gh.factory<_i44.MultiLevelSortingDao>(() => _i44.MultiLevelSortingDao());
  gh.factory<_i45.NetworkConnectionInterceptor>(
      () => _i45.NetworkConnectionInterceptor(get<_i40.INetworkInfo>()));
  gh.singleton<_i46.PersistenceModule>(_i46.PersistenceModule());
  gh.factory<_i47.QrCodeScannerBloc>(
      () => _i47.QrCodeScannerBloc(get<_i30.IAppConfigRepository>()));
  gh.singleton<_i48.ReportProblemCubit>(_i48.ReportProblemCubit());
  gh.lazySingleton<_i49.SelectedFilters>(() => _i49.SelectedFilters());
  gh.lazySingleton<_i50.SelectedGrouping>(() => _i50.SelectedGrouping());
  gh.lazySingleton<_i51.SelectedMultiLevelSort>(
      () => _i51.SelectedMultiLevelSort());
  gh.lazySingleton<_i52.SelectedSearchTerm>(() => _i52.SelectedSearchTerm());
  gh.lazySingleton<_i53.SelectedSort>(() => _i53.SelectedSort());
  gh.singleton<_i54.SelectedWorkPackage>(_i54.SelectedWorkPackage());
  gh.factory<_i55.ServerCredentialsBloc>(
      () => _i55.ServerCredentialsBloc(get<_i30.IAppConfigRepository>()));
  gh.lazySingleton<_i56.SessionDAO>(() => _i56.SessionDAO());
  gh.factory<_i57.SessionOpenIdClient>(() => _i57.SessionOpenIdClient());
  await gh.factoryAsync<_i58.SharedPreferences>(
    () => registerModule.prefs,
    preResolve: true,
  );
  gh.factory<_i59.StackFilterDAO>(() => _i59.StackFilterDAO());
  gh.factory<_i60.StaticFieldDefinitionValueDAO>(
      () => _i60.StaticFieldDefinitionValueDAO());
  gh.factory<_i61.SyncTickerDAO>(() => _i61.SyncTickerDAO());
  gh.lazySingleton<_i62.TurnAroundActivityDAO>(
      () => _i62.TurnAroundActivityDAO());
  gh.factory<_i63.TurnAroundEventDAO>(() => _i63.TurnAroundEventDAO());
  gh.factory<_i64.UpdatedCommentDAO>(() => _i64.UpdatedCommentDAO());
  gh.lazySingleton<_i65.UpdatedTurnAroundActivityDAO>(
      () => _i65.UpdatedTurnAroundActivityDAO());
  gh.factory<_i66.UserCallDAO>(() => _i66.UserCallDAO());
  gh.factory<_i67.UserOptionsDAO>(() => _i67.UserOptionsDAO());
  gh.factory<_i68.WbsElementsDao>(() => _i68.WbsElementsDao());
  gh.factory<_i69.WbsGroupingService>(() => _i69.WbsGroupingService());
  gh.factory<_i70.WbsLevelStyleDao>(() => _i70.WbsLevelStyleDao());
  gh.singleton<_i71.WorkPackageDynamicColumns>(
      _i71.WorkPackageDynamicColumns());
  gh.factory<_i72.WorkTypeOptionsDAO>(() => _i72.WorkTypeOptionsDAO());
  gh.factory<_i73.WorkpackageDAO>(() => _i73.WorkpackageDAO());
  gh.factory<_i74.WorkpackageGroupingDAO>(() => _i74.WorkpackageGroupingDAO());
  gh.lazySingleton<_i75.WorkpackageSelectedFilterOption>(
      () => _i75.WorkpackageSelectedFilterOption());
  gh.factory<_i76.WorkpackageSubgroupingViewBloc>(
      () => _i76.WorkpackageSubgroupingViewBloc());
  gh.factory<_i77.BaseUrlInterceptor>(
      () => _i77.BaseUrlInterceptor(get<_i30.IAppConfigRepository>()));
  gh.factory<_i78.ExceptionInterceptor>(
      () => _i78.ExceptionInterceptor(get<_i34.IAppLogsRepository>()));
  gh.factory<_i79.FilterConfigBloc>(
      () => _i79.FilterConfigBloc(get<_i38.IFilterConfigRepository>()));
  gh.lazySingleton<_i80.IActivityGroupingDataStore>(
      () => _i81.ActivityGroupingDataStoreImpl(
            get<_i4.ActivityFieldDAO>(),
            get<_i3.ActivitiesGroupingService>(),
            get<_i43.MultiLevelGroupingDao>(),
            get<_i44.MultiLevelSortingDao>(),
          ));
  gh.lazySingleton<_i82.IActivityRelationsDataStore>(() =>
      _i83.ActivityRelationsDataStoreImpl(get<_i62.TurnAroundActivityDAO>()));
  gh.lazySingleton<_i84.IActivityRelationsRepository>(() =>
      _i85.ActivityRelationsRepositoryImpl(
          get<_i82.IActivityRelationsDataStore>()));
  gh.lazySingleton<_i86.IDataLossDataStore>(() => _i87.DataLossDataStoreImpl(
        get<_i65.UpdatedTurnAroundActivityDAO>(),
        get<_i64.UpdatedCommentDAO>(),
        get<_i10.AttachmentsDAO>(),
        get<_i60.StaticFieldDefinitionValueDAO>(),
        get<_i20.FieldDefinitionValueDAO>(),
      ));
  gh.lazySingleton<_i88.IDataLossRepository>(
      () => _i89.DataLossRepositoryImpl(get<_i86.IDataLossDataStore>()));
  gh.lazySingleton<_i90.IExecutionSearchDataStore>(
      () => _i91.SearchDataStoreImpl(get<_i62.TurnAroundActivityDAO>()));
  gh.lazySingleton<_i92.IExecutionSearchRepository>(
      () => _i93.SearchRepositoryImpl(get<_i90.IExecutionSearchDataStore>()));
  gh.lazySingleton<_i94.IFieldDefinitionDataStore>(
      () => _i95.FieldDefinitionDataStoreImpl(
            get<_i20.FieldDefinitionValueDAO>(),
            get<_i67.UserOptionsDAO>(),
            get<_i13.ContractorOptionsDAO>(),
            get<_i72.WorkTypeOptionsDAO>(),
            get<_i73.WorkpackageDAO>(),
            get<_i60.StaticFieldDefinitionValueDAO>(),
          ));
  gh.lazySingleton<_i96.IPlannerSearchDataStore>(
      () => _i97.PlannerSearchDataStoreImpl(get<_i73.WorkpackageDAO>()));
  gh.lazySingleton<_i98.IPlannerSearchRepository>(() =>
      _i99.PlannerSearchRepositoryImpl(get<_i96.IPlannerSearchDataStore>()));
  gh.lazySingleton<_i100.ISessionDataStore>(() => _i101.SessionDataStoreImpl(
        get<_i57.SessionOpenIdClient>(),
        get<_i56.SessionDAO>(),
      ));
  gh.lazySingleton<_i102.ISessionRepository>(() => _i103.SessionRepositoryImpl(
        get<_i100.ISessionDataStore>(),
        get<_i34.IAppLogsRepository>(),
      ));
  gh.lazySingleton<_i104.ISyncTickerDataStore>(
      () => _i105.SyncTickerDataStoreImpl(get<_i61.SyncTickerDAO>()));
  gh.lazySingleton<_i106.ISyncTickerRepository>(
      () => _i107.SyncTickerRepositoryImpl(get<_i104.ISyncTickerDataStore>()));
  gh.factory<_i108.LoginBloc>(() => _i108.LoginBloc(
        get<_i102.ISessionRepository>(),
        get<_i30.IAppConfigRepository>(),
        get<_i40.INetworkInfo>(),
      ));
  gh.factory<_i109.PlannerSearchBloc>(
      () => _i109.PlannerSearchBloc(get<_i98.IPlannerSearchRepository>()));
  gh.factory<_i110.PredecessorsBloc>(
      () => _i110.PredecessorsBloc(get<_i84.IActivityRelationsRepository>()));
  gh.factory<_i111.ProfileDrawerCubit>(
      () => _i111.ProfileDrawerCubit(get<_i102.ISessionRepository>()));
  gh.singleton<_i112.SessionPreferenceManager>(
      _i112.SessionPreferenceManager(get<_i58.SharedPreferences>()));
  gh.factory<_i113.SplashBloc>(() => _i113.SplashBloc(
        get<_i30.IAppConfigRepository>(),
        get<_i102.ISessionRepository>(),
      ));
  gh.factory<_i114.SuccessorsBloc>(
      () => _i114.SuccessorsBloc(get<_i84.IActivityRelationsRepository>()));
  gh.factory<_i115.SyncTickerBloc>(
      () => _i115.SyncTickerBloc(get<_i106.ISyncTickerRepository>()));
  gh.factory<_i116.ApiHeadersInterceptor>(() => _i116.ApiHeadersInterceptor(
        get<_i102.ISessionRepository>(),
        get<_i30.IAppConfigRepository>(),
      ));
  gh.factory<_i117.Dio>(() => networkModule.getDio(
        get<_i77.BaseUrlInterceptor>(),
        get<_i27.HttpFormatter>(),
        get<_i116.ApiHeadersInterceptor>(),
        get<_i78.ExceptionInterceptor>(),
        get<_i45.NetworkConnectionInterceptor>(),
      ));
  gh.factory<_i118.DoneForTheDayApiService>(
      () => _i118.DoneForTheDayApiService(get<_i117.Dio>()));
  gh.factory<_i119.ExecutionSearchBloc>(
      () => _i119.ExecutionSearchBloc(get<_i92.IExecutionSearchRepository>()));
  gh.factory<_i120.ExecutionSyncApiService>(
      () => _i120.ExecutionSyncApiService(get<_i117.Dio>()));
  gh.lazySingleton<_i121.IAppSwitcherDataStore>(() =>
      _i122.AppSwitcherDataStoreImpl(get<_i112.SessionPreferenceManager>()));
  gh.lazySingleton<_i123.IAppSwitcherRepository>(() =>
      _i124.AppSwitcherRepositoryImpl(get<_i121.IAppSwitcherDataStore>()));
  gh.lazySingleton<_i125.IDoneForTheDayDataStore>(
      () => _i126.DoneForTheDayDataStoreImpl(
            get<_i17.DoneForTheDayDAO>(),
            get<_i118.DoneForTheDayApiService>(),
          ));
  gh.lazySingleton<_i127.IDoneForTheDayRepository>(() =>
      _i128.DoneForTheDayRepositoryImpl(get<_i125.IDoneForTheDayDataStore>()));
  gh.factory<_i129.LicenseApiService>(
      () => _i129.LicenseApiService(get<_i117.Dio>()));
  gh.factory<_i130.PlannerSyncApiService>(
      () => _i130.PlannerSyncApiService(get<_i117.Dio>()));
  gh.factory<_i131.StackFilterApiService>(
      () => _i131.StackFilterApiService(get<_i117.Dio>()));
  gh.factory<_i132.TurnAroundActivityApiService>(
      () => _i132.TurnAroundActivityApiService(get<_i117.Dio>()));
  gh.factory<_i133.TurnAroundEventApiService>(
      () => _i133.TurnAroundEventApiService(get<_i117.Dio>()));
  gh.factory<_i134.UserCallApiService>(
      () => _i134.UserCallApiService(get<_i117.Dio>()));
  gh.factory<_i135.WbsApiService>(() => _i135.WbsApiService(get<_i117.Dio>()));
  gh.factory<_i136.WorkpackageApiService>(
      () => _i136.WorkpackageApiService(get<_i117.Dio>()));
  gh.factory<_i137.WorkpackageGroupingApiService>(
      () => _i137.WorkpackageGroupingApiService(get<_i117.Dio>()));
  gh.factory<_i138.AttachmentApiService>(
      () => _i138.AttachmentApiService(get<_i117.Dio>()));
  gh.factory<_i139.AttachmentSyncApiService>(
      () => _i139.AttachmentSyncApiService(get<_i117.Dio>()));
  gh.factory<_i140.BlockingReasonApiService>(
      () => _i140.BlockingReasonApiService(get<_i117.Dio>()));
  gh.factory<_i141.CommentApiService>(
      () => _i141.CommentApiService(get<_i117.Dio>()));
  gh.lazySingleton<_i142.DoneForTheDayCubit>(
      () => _i142.DoneForTheDayCubit(get<_i127.IDoneForTheDayRepository>()));
  gh.lazySingleton<_i143.IAttachmentsDataStore>(
      () => _i144.AttachmentDataStoreImpl(
            get<_i138.AttachmentApiService>(),
            get<_i10.AttachmentsDAO>(),
          ));
  gh.lazySingleton<_i145.IAttachmentsRepository>(
      () => _i146.AttachmentsRepositoryImpl(
            get<_i143.IAttachmentsDataStore>(),
            get<_i106.ISyncTickerRepository>(),
          ));
  gh.lazySingleton<_i147.IBlockingReasonDataStore>(
      () => _i148.BlockingReasonDataStoreImpl(
            get<_i140.BlockingReasonApiService>(),
            get<_i11.BlockingReasonDAO>(),
          ));
  gh.lazySingleton<_i149.IBlockingReasonRepository>(() =>
      _i150.BlockingReasonRepositoryImpl(
          get<_i147.IBlockingReasonDataStore>()));
  gh.lazySingleton<_i151.ILicenseDataStore>(() => _i152.LicenseDataStoreImpl(
        get<_i129.LicenseApiService>(),
        get<_i42.LicenseDAO>(),
      ));
  gh.lazySingleton<_i153.ILicenseRepository>(
      () => _i154.LicenseRepositoryImpl(get<_i151.ILicenseDataStore>()));
  gh.lazySingleton<_i155.IPlannerSyncDataStore>(
      () => _i156.PlannerSyncDataStoreImpl(
            get<_i130.PlannerSyncApiService>(),
            get<_i94.IFieldDefinitionDataStore>(),
            get<_i139.AttachmentSyncApiService>(),
          ));
  gh.lazySingleton<_i157.IPlannerSyncRepository>(
      () => _i158.PlannerSyncRepositoryImpl(
            get<_i155.IPlannerSyncDataStore>(),
            get<_i145.IAttachmentsRepository>(),
            get<_i106.ISyncTickerRepository>(),
          ));
  gh.lazySingleton<_i159.IStackFilterDataStore>(
      () => _i160.StackFilterDataStoreImpl(
            get<_i131.StackFilterApiService>(),
            get<_i59.StackFilterDAO>(),
          ));
  gh.lazySingleton<_i161.IStackFilterRepository>(() =>
      _i162.StackFilterRepositoryImpl(get<_i159.IStackFilterDataStore>()));
  gh.lazySingleton<_i163.ITurnAroundActivityDataStore>(
      () => _i164.TurnAroundActivityDataStoreImpl(
            get<_i132.TurnAroundActivityApiService>(),
            get<_i62.TurnAroundActivityDAO>(),
            get<_i65.UpdatedTurnAroundActivityDAO>(),
            get<_i4.ActivityFieldDAO>(),
            get<_i36.IFilterConfigDataStore>(),
            get<_i159.IStackFilterDataStore>(),
          ));
  gh.lazySingleton<_i165.ITurnAroundEventDataStore>(
      () => _i166.TurnAroundEventDataStoreImpl(
            get<_i133.TurnAroundEventApiService>(),
            get<_i63.TurnAroundEventDAO>(),
            get<_i112.SessionPreferenceManager>(),
          ));
  gh.lazySingleton<_i167.ITurnAroundEventRepository>(
      () => _i168.TurnAroundEventRepositoryImpl(
            get<_i165.ITurnAroundEventDataStore>(),
            get<_i112.SessionPreferenceManager>(),
          ));
  gh.lazySingleton<_i169.ITurnaroundActivitiesCommentDataStore>(
      () => _i170.TurnaroundActivitiesCommentDataStoreImpl(
            get<_i141.CommentApiService>(),
            get<_i12.CommentDAO>(),
            get<_i64.UpdatedCommentDAO>(),
          ));
  gh.lazySingleton<_i171.ITurnaroundActivitiesCommentRepository>(
      () => _i172.TurnaroundActivitiesCommentRepositoryImpl(
            get<_i169.ITurnaroundActivitiesCommentDataStore>(),
            get<_i167.ITurnAroundEventRepository>(),
          ));
  gh.lazySingleton<_i173.IUserCallDataStore>(() => _i174.UserCallDataStoreImpl(
        get<_i134.UserCallApiService>(),
        get<_i66.UserCallDAO>(),
      ));
  gh.lazySingleton<_i175.IUserCallRepository>(
      () => _i176.UserCallRepositoryImpl(get<_i173.IUserCallDataStore>()));
  gh.lazySingleton<_i177.IWbsDataStore>(() => _i178.WbsDataStoreImpl(
        get<_i135.WbsApiService>(),
        get<_i70.WbsLevelStyleDao>(),
        get<_i69.WbsGroupingService>(),
        get<_i68.WbsElementsDao>(),
      ));
  gh.lazySingleton<_i179.IWbsRepository>(() => _i180.WbsRepositoryImpl(
        get<_i177.IWbsDataStore>(),
        get<_i69.WbsGroupingService>(),
      ));
  gh.lazySingleton<_i181.IWorkpackageDataStore>(
      () => _i182.WorkpackageDataStoreImpl(
            get<_i136.WorkpackageApiService>(),
            get<_i73.WorkpackageDAO>(),
          ));
  gh.lazySingleton<_i183.IWorkpackageGroupingDataStore>(
      () => _i184.WorkpackageGroupingDataStoreImpl(
            get<_i137.WorkpackageGroupingApiService>(),
            get<_i74.WorkpackageGroupingDAO>(),
          ));
  gh.lazySingleton<_i185.IWorkpackageGroupingRepository>(() =>
      _i186.WorkpackageGroupingRepositoryImpl(
          get<_i183.IWorkpackageGroupingDataStore>()));
  gh.factory<_i187.ImageAnnotationBloc>(
      () => _i187.ImageAnnotationBloc(get<_i145.IAttachmentsRepository>()));
  gh.factory<_i188.StackFilterBloc>(
      () => _i188.StackFilterBloc(get<_i161.IStackFilterRepository>()));
  gh.factory<_i189.TurnaroundEventBloc>(() => _i189.TurnaroundEventBloc(
        get<_i167.ITurnAroundEventRepository>(),
        get<_i88.IDataLossRepository>(),
      ));
  gh.factory<_i190.TurnaroundEventSelectorCubit>(() =>
      _i190.TurnaroundEventSelectorCubit(
          get<_i167.ITurnAroundEventRepository>()));
  gh.factory<_i191.UserCallBloc>(
      () => _i191.UserCallBloc(get<_i175.IUserCallRepository>()));
  gh.factory<_i192.WbsBloc>(() => _i192.WbsBloc(get<_i179.IWbsRepository>()));
  gh.factory<_i193.WorkpackageGroupingViewBloc>(() =>
      _i193.WorkpackageGroupingViewBloc(
          get<_i185.IWorkpackageGroupingRepository>()));
  gh.lazySingleton<_i194.AppSwitcherBloc>(() => _i194.AppSwitcherBloc(
        get<_i123.IAppSwitcherRepository>(),
        get<_i153.ILicenseRepository>(),
      ));
  gh.factory<_i195.AttachmentsBloc>(
      () => _i195.AttachmentsBloc(get<_i145.IAttachmentsRepository>()));
  gh.factory<_i196.BlockingReasonsBloc>(
      () => _i196.BlockingReasonsBloc(get<_i149.IBlockingReasonRepository>()));
  gh.lazySingleton<_i197.IExecutionSyncDataStore>(
      () => _i198.ExecutionSyncDataStoreImpl(
            get<_i120.ExecutionSyncApiService>(),
            get<_i163.ITurnAroundActivityDataStore>(),
            get<_i169.ITurnaroundActivitiesCommentDataStore>(),
          ));
  gh.lazySingleton<_i199.IExecutionSyncRepository>(
      () => _i200.ExecutionSyncRepositoryImpl(
            get<_i197.IExecutionSyncDataStore>(),
            get<_i167.ITurnAroundEventRepository>(),
            get<_i127.IDoneForTheDayRepository>(),
          ));
  gh.lazySingleton<_i201.IFieldDefinitionRepository>(
      () => _i202.FieldDefinitionRepositoryImpl(
            get<_i22.FieldDefinitionValuesService>(),
            get<_i94.IFieldDefinitionDataStore>(),
            get<_i185.IWorkpackageGroupingRepository>(),
            get<_i106.ISyncTickerRepository>(),
          ));
  gh.lazySingleton<_i203.ILogoutDataStore>(() => _i204.LogoutDataStoreImpl(
        get<_i163.ITurnAroundActivityDataStore>(),
        get<_i177.IWbsDataStore>(),
        get<_i169.ITurnaroundActivitiesCommentDataStore>(),
        get<_i197.IExecutionSyncDataStore>(),
        get<_i100.ISessionDataStore>(),
        get<_i165.ITurnAroundEventDataStore>(),
        get<_i112.SessionPreferenceManager>(),
        get<_i151.ILicenseDataStore>(),
        get<_i143.IAttachmentsDataStore>(),
        get<_i94.IFieldDefinitionDataStore>(),
        get<_i181.IWorkpackageDataStore>(),
        get<_i183.IWorkpackageGroupingDataStore>(),
        get<_i104.ISyncTickerDataStore>(),
        get<_i173.IUserCallDataStore>(),
        get<_i80.IActivityGroupingDataStore>(),
        get<_i36.IFilterConfigDataStore>(),
      ));
  gh.lazySingleton<_i205.ILogoutRepository>(() => _i206.LogoutRepositoryImpl(
        get<_i203.ILogoutDataStore>(),
        get<_i127.IDoneForTheDayRepository>(),
      ));
  gh.lazySingleton<_i207.ITurnAroundActivityRepository>(
      () => _i208.TurnAroundActivityRepositoryImpl(
            get<_i163.ITurnAroundActivityDataStore>(),
            get<_i167.ITurnAroundEventRepository>(),
            get<_i171.ITurnaroundActivitiesCommentRepository>(),
            get<_i179.IWbsRepository>(),
            get<_i199.IExecutionSyncRepository>(),
            get<_i149.IBlockingReasonRepository>(),
            get<_i5.ActivityFieldMapper>(),
            get<_i161.IStackFilterRepository>(),
          ));
  gh.lazySingleton<_i209.IWorkpackageRepository>(
      () => _i210.WorkpackageRepositoryImpl(
            get<_i181.IWorkpackageDataStore>(),
            get<_i185.IWorkpackageGroupingRepository>(),
            get<_i201.IFieldDefinitionRepository>(),
            get<_i157.IPlannerSyncRepository>(),
            get<_i145.IAttachmentsRepository>(),
            get<_i102.ISessionRepository>(),
          ));
  gh.lazySingleton<_i211.LogoutBloc>(() => _i211.LogoutBloc(
        get<_i30.IAppConfigRepository>(),
        get<_i205.ILogoutRepository>(),
        get<_i88.IDataLossRepository>(),
      ));
  gh.factory<_i212.SwipeToCompleteBloc>(() => _i212.SwipeToCompleteBloc(
        get<_i207.ITurnAroundActivityRepository>(),
        get<_i175.IUserCallRepository>(),
        get<_i167.ITurnAroundEventRepository>(),
      ));
  gh.factory<_i213.SyncChangesNotifierBloc>(() =>
      _i213.SyncChangesNotifierBloc(get<_i199.IExecutionSyncRepository>()));
  gh.factory<_i214.WbsActivityBloc>(
      () => _i214.WbsActivityBloc(get<_i207.ITurnAroundActivityRepository>()));
  gh.factory<_i215.WorkPackageDetailsBloc>(
      () => _i215.WorkPackageDetailsBloc(get<_i209.IWorkpackageRepository>()));
  gh.factory<_i216.WorkpackageListScreenBloc>(() =>
      _i216.WorkpackageListScreenBloc(get<_i209.IWorkpackageRepository>()));
  gh.factory<_i217.ActivityDetailsScreenBloc>(
      () => _i217.ActivityDetailsScreenBloc(
            get<_i207.ITurnAroundActivityRepository>(),
            get<_i167.ITurnAroundEventRepository>(),
            get<_i171.ITurnaroundActivitiesCommentRepository>(),
            get<_i102.ISessionRepository>(),
          ));
  gh.factory<_i218.CommentBloc>(() => _i218.CommentBloc(
        get<_i171.ITurnaroundActivitiesCommentRepository>(),
        get<_i102.ISessionRepository>(),
        get<_i207.ITurnAroundActivityRepository>(),
      ));
  gh.factory<_i219.Error_404Bloc>(
      () => _i219.Error_404Bloc(get<_i205.ILogoutRepository>()));
  gh.factory<_i220.FieldDefinitionBloc>(
      () => _i220.FieldDefinitionBloc(get<_i201.IFieldDefinitionRepository>()));
  gh.factory<_i221.FieldTypeContractorBloc>(() =>
      _i221.FieldTypeContractorBloc(get<_i201.IFieldDefinitionRepository>()));
  gh.factory<_i222.FieldTypePlannerBloc>(() =>
      _i222.FieldTypePlannerBloc(get<_i201.IFieldDefinitionRepository>()));
  gh.factory<_i223.FieldTypePrimaryContractorBloc>(() =>
      _i223.FieldTypePrimaryContractorBloc(
          get<_i201.IFieldDefinitionRepository>()));
  gh.factory<_i224.FieldTypeWorkTypeBloc>(() =>
      _i224.FieldTypeWorkTypeBloc(get<_i201.IFieldDefinitionRepository>()));
  gh.factory<_i225.GroupingActivityBloc>(() =>
      _i225.GroupingActivityBloc(get<_i207.ITurnAroundActivityRepository>()));
  gh.lazySingleton<_i226.IActivityGroupingRepository>(
      () => _i227.ActivityGroupingRepositoryImpl(
            get<_i80.IActivityGroupingDataStore>(),
            get<_i207.ITurnAroundActivityRepository>(),
          ));
  gh.lazySingleton<_i228.ITurnaroundDataRepository>(
      () => _i229.TurnaroundDataRepositoryImpl(
            get<_i207.ITurnAroundActivityRepository>(),
            get<_i209.IWorkpackageRepository>(),
            get<_i167.ITurnAroundEventRepository>(),
            get<_i153.ILicenseRepository>(),
            get<_i38.IFilterConfigRepository>(),
          ));
  gh.factory<_i230.MultiLevelGroupingBloc>(() =>
      _i230.MultiLevelGroupingBloc(get<_i226.IActivityGroupingRepository>()));
  gh.factory<_i231.MultiLevelGroupingSelectorCubit>(() =>
      _i231.MultiLevelGroupingSelectorCubit(
          get<_i226.IActivityGroupingRepository>()));
  gh.factory<_i232.SyncBloc>(
      () => _i232.SyncBloc(get<_i228.ITurnaroundDataRepository>()));
  gh.factory<_i233.TurnaroundDataCacheBloc>(() =>
      _i233.TurnaroundDataCacheBloc(get<_i228.ITurnaroundDataRepository>()));
  gh.factory<_i234.ActivityListScreenBloc>(() => _i234.ActivityListScreenBloc(
        get<_i207.ITurnAroundActivityRepository>(),
        get<_i226.IActivityGroupingRepository>(),
        get<_i179.IWbsRepository>(),
      ));
  gh.factory<_i235.GroupingScreenSelectorBloc>(() =>
      _i235.GroupingScreenSelectorBloc(
          get<_i226.IActivityGroupingRepository>()));
  return get;
}

class _$NetworkModule extends _i236.NetworkModule {}

class _$RegisterModule extends _i237.RegisterModule {}
