import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sto_mobile_v2/common/application/splash/splash_bloc.dart';
import 'package:sto_mobile_v2/config/constants/assets/icon_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final bloc = getIt<SplashBloc>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocListener<SplashBloc, SplashState>(
        listener: (context, state) {
          state.maybeWhen(
              configurationNeeded: () => jumpToAppConfigScreen(context: context),
              configuartionExisting: () => jumpToLoginScreen(context: context, isSessionRestoration: false),
              loggedIn: () => jumpToHomeScreen(context: context),
              orElse: () {});
        },
        child: Scaffold(
          body: Container(
            color: Theme.of(context).primaryColor,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  iconPrometheusLogo,
                  color: Colors.white,
                ),
                const SizedBox(height: 20),
                Text(
                  S.of(context).title,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0,
                    color: Colors.white,
                  ),
                ),
                const LoadingIndicator(
                    color: Colors.white,
                    textColor: Colors.white,
                    width: 100,
                    height: 100,
                    fontSize: 20,
                    strokeWidth: 6,
                    bottomMargin: 20),
              ],
            )),
          ),
        ),
      ),
    );
  }
}
