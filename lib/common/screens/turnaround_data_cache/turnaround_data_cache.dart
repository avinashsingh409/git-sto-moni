import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sto_mobile_v2/common/application/turnaround_data_cache/turnaround_data_cache_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_switcher.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/assets/icon_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class TurnAroundDataCache extends StatelessWidget {
  const TurnAroundDataCache({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<TurnaroundDataCacheBloc>();
    bloc.add(const TurnaroundDataCacheEvent.getTurnaroundData());
    return BlocProvider.value(
      value: bloc,
      child: Scaffold(
        backgroundColor: AppTheme.pgPrimaryRed,
        body: BlocBuilder<TurnaroundDataCacheBloc, TurnaroundDataCacheState>(
          builder: (context, state) {
            return SizedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    iconPrometheusLogo,
                    color: Colors.white,
                    height: 100,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  const Text(
                    'Fetching Data',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  const Text(
                    'Please wait while we get data from the server..',
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                  const SizedBox(
                    height: 34,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      state.maybeWhen(
                        orElse: () {
                          return const SizedBox(
                            child: Column(
                              children: [
                                Icon(
                                  Icons.done,
                                  color: Colors.green,
                                  size: 80,
                                ),
                                SizedBox(
                                  height: 34,
                                ),
                                AppSwitcher(
                                  isRescaleForAppSwitcher: false,
                                ),
                              ],
                            ),
                          );
                        },
                        executionAndPlannerLoading: () {
                          return const LoadingIndicator(
                            color: Colors.white,
                            textColor: Colors.white,
                          );
                        },
                        cacheFailure: (errorMsg) => Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.error,
                              color: Colors.red,
                              size: 80,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: RichText(
                                text: TextSpan(
                                  text: errorMsg,
                                  style: const TextStyle(color: Colors.white, fontSize: 14),
                                ),
                                maxLines: 3,
                              ),
                            ),
                            const SizedBox(
                              height: 34,
                            ),
                            Column(
                              children: [
                                ElevatedButton(
                                  onPressed: () async {
                                    bloc.add(
                                      const TurnaroundDataCacheEvent.getTurnaroundData(),
                                    );
                                  },
                                  child: const Text("Retry Getting from the Server"),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                ElevatedButton(
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                                  onPressed: () async {
                                    jumpToHomeScreen(context: context);
                                  },
                                  child: const Text("Proceed anyway"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
