import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/server_credentials/server_credentials_bloc.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';
import 'package:sto_mobile_v2/common/screens/widgets/string_field.dart';

class ServerCredentialsScreen extends StatefulWidget {
  const ServerCredentialsScreen({super.key});

  @override
  State<ServerCredentialsScreen> createState() => _ServerCredentialsScreenState();
}

class _ServerCredentialsScreenState extends State<ServerCredentialsScreen> {
  final bloc = getIt<ServerCredentialsBloc>();
  late TextEditingController baseUrlController;
  late TextEditingController authenticationUrlController;
  late TextEditingController clientIdController;
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    baseUrlController = TextEditingController();
    authenticationUrlController = TextEditingController();
    clientIdController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: PrometheusLogoScaffold(
        body: BlocListener<ServerCredentialsBloc, ServerCredentialsState>(
          listener: (context, state) {
            state.when(
                initial: () {},
                cacheSuccess: () {
                  jumpToLoginScreen(context: context, isSessionRestoration: false);
                },
                parseSuccessful: (appConfig) {
                  setState(() {
                    baseUrlController.text = appConfig!.baseUrl;
                    authenticationUrlController.text = appConfig.authenticationUrl;
                    clientIdController.text = appConfig.mobileClientId;
                  });
                  ScaffoldMessenger.of(context)
                    ..removeCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                          content: Text(
                            S.current.successfullyPasted,
                          ),
                          duration: const Duration(seconds: 2)),
                    );
                },
                error: (errorMsg) {
                  ScaffoldMessenger.of(context)
                    ..removeCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                          content: Text(
                            errorMsg,
                          ),
                          duration: const Duration(seconds: 2)),
                    );
                });
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const FormSpacer(),
                    ServerCredentialsTextField(
                      hintText: S.current.baseUrl,
                      controller: baseUrlController,
                      valueChangedCallBack: (_) {},
                    ),
                    const FormSpacer(),
                    ServerCredentialsTextField(
                      hintText: S.current.authenticationUrl,
                      controller: authenticationUrlController,
                      valueChangedCallBack: (_) {},
                    ),
                    const FormSpacer(),
                    ServerCredentialsTextField(
                      hintText: S.current.clientId,
                      controller: clientIdController,
                      valueChangedCallBack: (_) {},
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.010,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: ElevatedButton(
                          onPressed: () {
                            Clipboard.getData(Clipboard.kTextPlain).then((value) {
                              bloc.add(ServerCredentialsEvent.pasteServerConfigs(value: value));
                            });
                          },
                          child: Text(S.current.pasteFromClipboard)),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          onPressed: () => {Navigator.pop(context)},
                          style: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryBlue),
                            textStyle: MaterialStateProperty.all(
                              const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ),
                          child: Text(
                            S.current.goBack.toUpperCase(),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.02,
                        ),
                        ElevatedButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                bloc.add(ServerCredentialsEvent.saveServerConfigs(
                                    appConfig: AppConfig(
                                  authenticationUrl: authenticationUrlController.text,
                                  baseUrl: baseUrlController.text,
                                  mobileClientId: clientIdController.text,
                                )));
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                S.current.connect.toUpperCase(),
                                style: const TextStyle(fontSize: 16),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // baseUrlController.dispose();
    // baseUrlController.dispose();
    // clientIdController.dispose();
    super.dispose();
  }
}

class FormSpacer extends StatelessWidget {
  const FormSpacer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 8,
    );
  }
}
