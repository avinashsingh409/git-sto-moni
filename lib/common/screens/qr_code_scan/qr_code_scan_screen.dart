import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:sto_mobile_v2/common/application/qr_code/qr_code_scanner_bloc.dart';

import 'package:sto_mobile_v2/config/app_theme.dart';

import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class QRCodeScanScreen extends StatefulWidget {
  const QRCodeScanScreen({super.key});

  @override
  State<QRCodeScanScreen> createState() => _QRCodeScanScreenState();
}

class _QRCodeScanScreenState extends State<QRCodeScanScreen> {
  final bloc = getIt<QrCodeScannerBloc>();
  GlobalKey qrKey = GlobalKey(debugLabel: "QrCode");
  Barcode? result;
  late QRViewController controller;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).qrCodeScannerTitle),
        ),
        body: BlocListener<QrCodeScannerBloc, QrCodeScannerState>(
          listener: (context, state) {
            state.maybeWhen(
                cacheSuccess: () {
                  jumpToLoginScreen(context: context, isSessionRestoration: false);
                },
                error: (errorMsg) {
                  Navigator.of(context).pop();
                  _resumeCamera(controller);
                  return ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(errorMsg),
                      backgroundColor: AppTheme.pgPrimaryRed,
                    ),
                  );
                },
                orElse: () {});
          },
          child: Center(
            child: QRView(
              key: qrKey,
              formatsAllowed: const [BarcodeFormat.qrcode],
              overlay: QrScannerOverlayShape(overlayColor: Colors.black.withOpacity(0.6)),
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      controller.pauseCamera();
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(S.of(context).coreConfirmDialogTitle),
                content: Text("${S.of(context).qrCodeConfirmDialogContent}: ${scanData.code}?"),
                actions: [
                  OutlinedButton(
                      onPressed: () {
                        controller.resumeCamera();
                        Navigator.of(context).pop(false);
                      },
                      child: Text(S.of(context).cancel)),
                  ElevatedButton(
                      onPressed: () {
                        bloc.add(QrCodeScannerEvent.saveQRCodeConfigs(appConfigString: scanData.code));
                      },
                      child: Text(S.of(context).save))
                ],
              ));
    });
    _resumeCamera(controller);
  }

  void _resumeCamera(QRViewController controller) {
    if (Platform.isAndroid) {
      controller.pauseCamera();
      controller.resumeCamera();
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
