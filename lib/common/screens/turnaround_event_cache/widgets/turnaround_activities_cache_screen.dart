// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:sto_mobile_v2/config/app_theme.dart';
// import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
// import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
// import 'package:sto_mobile_v2/execution/application/turnaround_activity_cache/turnaround_activity_cache_bloc.dart';
// import 'package:sto_mobile_v2/injectable/injection.dart';
// import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
// import 'package:sto_mobile_v2/common/screens/widgets/pop_up_dialogue.dart';
// import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';

// class TurnAroundActivitiesCacheScreen extends StatefulWidget {
//   const TurnAroundActivitiesCacheScreen({super.key});

//   @override
//   State<TurnAroundActivitiesCacheScreen> createState() => _TurnAroundActivitiesCacheScreenState();
// }

// class _TurnAroundActivitiesCacheScreenState extends State<TurnAroundActivitiesCacheScreen> {
//   final bloc = getIt<TurnaroundActivityCacheBloc>();

//   @override
//   void initState() {
//     super.initState();
//     bloc.add(const TurnaroundActivityCacheEvent.getAndCacheTurnAroundActivities());
//   }

//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider(
//       create: (context) => bloc,
//       child: PrometheusLogoScaffold(
//         body: SizedBox(
//           child: Column(
//             children: [
//               BlocConsumer<TurnaroundActivityCacheBloc, TurnaroundActivityCacheState>(
//                 listener: (context, state) {
//                   state.maybeWhen(
//                       cacheNotEmpty: () {
//                         showDialog(
//                           context: context,
//                           builder: (context) => PopUpDialogue(
//                             isPopForChangingEvents: true,
//                             onPressed: () async {
//                               bloc.add(const TurnaroundActivityCacheEvent.forceGetAndCacheTurnAroundActivities());
//                             },
//                             popUpTitle: RichText(
//                               text: TextSpan(
//                                 text: S.current.sureYouWantToActionName(
//                                   S.current.logoutButton,
//                                 ),
//                                 style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
//                               ),
//                             ),
//                             popUpSubTitle: RichText(
//                               text: TextSpan(
//                                 text: S.current.allProgressLost,
//                                 style: const TextStyle(
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 15,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         );
//                       },
//                       orElse: () {});
//                 },
//                 builder: (context, state) {
//                   return state.when(
//                     initial: () {
//                       return const SizedBox();
//                     },
//                     cacheNotEmpty: () => const LoadingIndicator(),
//                     loading: () {
//                       return const LoadingIndicator();
//                     },
//                     success: () {
//                       return Column(
//                         children: const [
//                           DoneButton(),
//                         ],
//                       );
//                     },
//                     error: (errorMsg) => ErrorPane(errorMsg: errorMsg),
//                   );
//                 },
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class ErrorPane extends StatelessWidget {
//   final String errorMsg;
//   const ErrorPane({
//     Key? key,
//     required this.errorMsg,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Text(
//           errorMsg,
//           style: const TextStyle(
//             fontWeight: FontWeight.bold,
//             fontSize: 16,
//             color: AppTheme.pgAccentError,
//           ),
//         ),
//         const SizedBox(
//           height: 40,
//         ),
//         ElevatedButton(
//           onPressed: () async {
//             jumpToHomeScreen(context: context);
//           },
//           child: const Text('Sync Later'),
//         ),
//       ],
//     );
//   }
// }

// class DoneButton extends StatelessWidget {
//   const DoneButton({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//       onPressed: () {
//         jumpToHomeScreen(context: context);
//       },
//       style: ElevatedButton.styleFrom(
//         backgroundColor: AppTheme.pgPrimaryRed,
//         padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
//         textStyle: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//       ),
//       child: const Text('Done'),
//     );
//   }
// }
