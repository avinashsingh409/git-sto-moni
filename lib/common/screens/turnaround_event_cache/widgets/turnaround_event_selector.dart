import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/turnaround_event/turnaround_event_bloc.dart';
import 'package:sto_mobile_v2/common/application/turnaround_event_selector_cubit.dart/turnaround_event_selector_cubit.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/screens/widgets/data_loss_dialog.dart';

import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:sto_mobile_v2/common/screens/widgets/error_indicator.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';

class TurnAroundEventSelector extends StatefulWidget {
  final bool requiresOnlineSync;
  const TurnAroundEventSelector({super.key, required this.requiresOnlineSync});

  @override
  State<TurnAroundEventSelector> createState() => _TurnAroundEventSelectorState();
}

class _TurnAroundEventSelectorState extends State<TurnAroundEventSelector> {
  final bloc = getIt<TurnaroundEventBloc>();
  final turnAroundEventSelectorCubit = getIt<TurnaroundEventSelectorCubit>();

  @override
  void initState() {
    super.initState();
    bloc.add(TurnaroundEventEvent.getTurnAroundEvent(requiresOnlineSync: widget.requiresOnlineSync));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocConsumer<TurnaroundEventBloc, TurnaroundEventState>(
        listenWhen: (previous, current) => shouldListen(current),
        listener: (context, state) {
          state.maybeWhen(
            orElse: () {},
            safeEventSwitch: () => jumpToTurnAroundDataCacheScreen(context: context),
            unsafeEventSwitch: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DataLossDialog(
                    onCancel: () {
                      bloc.add(const TurnaroundEventEvent.cancelEventSwitch());
                    },
                    onConfirm: () async {
                      bloc.add(TurnaroundEventEvent.forceTurnaroundEventChange(turnaroundEvent: turnAroundEventSelectorCubit.turnAroundEvent ?? TurnAroundEventDTO(id: 99999)));                    
                    }
                  );
                }
              );
            }
          );
        },
        buildWhen: (previous, current) => !shouldListen(current),
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => const SizedBox(),
            loading: () => const LoadingIndicator(),
            success: (events) => Card(
              child: Column(
                children: [
                  Center(
                    child: Text(
                      S.of(context).selectStoEvent,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  TurnAroundEventListPane(
                    events: events,
                    turnaroundEventSelectorCubit: turnAroundEventSelectorCubit,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      if (turnAroundEventSelectorCubit.isTurnAroundSelected) {
                        bloc.add(TurnaroundEventEvent.setSelectedTurnAroundEvent(turnaroundEvent: turnAroundEventSelectorCubit.turnAroundEvent ?? TurnAroundEventDTO(id: 99999)));
                      }
                    },
                    child: Text(S.current.confirm),
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                ],
              ),
            ),
            error: (errorMsg) => Column(
              children: [
                ErrorIndicator(errorMessage: errorMsg),
                ElevatedButton(
                  onPressed: () async {
                    if (errorMsg == "No Cached Events") {
                      bloc.add(
                        const TurnaroundEventEvent.getTurnAroundEvent(requiresOnlineSync: true),
                      );
                    } else {
                      bloc.add(
                        TurnaroundEventEvent.getTurnAroundEvent(requiresOnlineSync: widget.requiresOnlineSync),
                      );
                    }
                  },
                  child: const Text("Retry Getting from the Server"),
                ),
                widget.requiresOnlineSync ? ElevatedButton(
                  onPressed: () async {
                    jumpToHomeScreen(context: context);
                  },
                  child: const Text('Sync Later'),
                ) : const SizedBox(),
              ],
            )
          );
        },
      ),
    );
  }
}

// decide when to listen vs. build
bool shouldListen(TurnaroundEventState current) {
  Type safeEventSwitch = const TurnaroundEventState.safeEventSwitch().runtimeType;
  Type unsafeEventSwitch = const TurnaroundEventState.unsafeEventSwitch().runtimeType;
  Type cancelEventSwitch = const TurnaroundEventState.canceledEventSwitch().runtimeType;
  return current.runtimeType == safeEventSwitch || current.runtimeType == unsafeEventSwitch || current.runtimeType == cancelEventSwitch;
}

class TurnAroundEventListPane extends StatelessWidget {
  final List<TurnAroundEventDTO> events;
  final TurnaroundEventSelectorCubit turnaroundEventSelectorCubit;
  const TurnAroundEventListPane({
    Key? key,
    required this.events,
    required this.turnaroundEventSelectorCubit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    turnaroundEventSelectorCubit.initializeSelector();
    return BlocProvider.value(
      value: turnaroundEventSelectorCubit,
      child: BlocBuilder<TurnaroundEventSelectorCubit, TurnaroundEventSelectorState>(
        builder: (context, state) {
          return state.when(
              initial: () => const SizedBox(),
              selectedEvent: (_) => const SizedBox(),
              loading: () => const Center(
                    child: LoadingIndicator(),
                  ),
              turnAroundEventChanged: (turnAroundEvent) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: DropdownSearch<TurnAroundEventDTO>(
                    // enabled: isConnected,
                    items: events,
                    selectedItem: turnAroundEvent,
                    itemAsString: (item) => item.description ?? "",
                    popupProps: PopupProps.menu(
                      showSearchBox: true,
                      searchFieldProps: TextFieldProps(
                        autofocus: false,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.search),
                          hintText: S.current.stoEventDropDownSearchBoxHintText,
                          border: const OutlineInputBorder(),
                        ),
                      ),
                    ),
                    onChanged: (changedTurnaroundEvent) {
                      if (changedTurnaroundEvent != null) {
                        turnaroundEventSelectorCubit.changeTurnAroundEvent(selectedTurnaroundEventEvent: changedTurnaroundEvent);
                      }
                    },
                  ),
                );
              });
        },
      ),
    );
  }
}
