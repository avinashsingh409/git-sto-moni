import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/common/screens/turnaround_event_cache/widgets/turnaround_event_selector.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';

class TurnAroundEventScreen extends StatelessWidget {
  const TurnAroundEventScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return PrometheusLogoScaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 24,
          ),
          const TurnAroundEventSelector(requiresOnlineSync: true),
          const SizedBox(
            height: 24,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.05,
              ),
            ],
          )
        ],
      ),
    );
  }
}
