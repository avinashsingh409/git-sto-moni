import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/common/screens/app_config/widgets/enter_server_credentials_pane.dart';
import 'package:sto_mobile_v2/common/screens/app_config/widgets/not_sure_drop_down_pane.dart';
import 'package:sto_mobile_v2/common/screens/app_config/widgets/scan_qr_pane.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';

class AppConfigScreen extends StatelessWidget {
  const AppConfigScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return PrometheusLogoScaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  S.of(context).welcomeMessage,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                S.of(context).welcomeExplanation,
                style: const TextStyle(color: AppTheme.pgPrimaryDarkGray),
              ),
              if (Platform.isAndroid || Platform.isIOS) const ScanQRCodePane(),
              if (Platform.isAndroid || Platform.isIOS) const NotSureDropDownPane(),
              const EnterServerCredentialsPane(),
            ],
          ),
        ),
      ),
    );
  }
}
