import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class EnterServerCredentialsPane extends StatelessWidget {
  const EnterServerCredentialsPane({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: ElevatedButton(
        onPressed: () {
          jumpToServerCredentialsScreen(context: context);
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          side: const BorderSide(color: AppTheme.pgPrimaryDarkGray),
          textStyle: const TextStyle(color: AppTheme.pgPrimaryBlue),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Row(
            children: [
              const Icon(
                Icons.storage_rounded,
                color: AppTheme.pgPrimaryBlue,
                size: 32,
              ),
              const SizedBox(width: 16),
              Text(
                S.of(context).serverConnectText,
                style: const TextStyle(
                  color: AppTheme.pgPrimaryDarkGray,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
