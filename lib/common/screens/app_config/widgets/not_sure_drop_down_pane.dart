import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/common/screens/widgets/expandable_text.dart';

class NotSureDropDownPane extends StatelessWidget {
  const NotSureDropDownPane({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpandableText(
      text: S.of(context).qrCodeExplanation,
      color: AppTheme.pgPrimaryDarkGray,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 8),
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: "${S.of(context).administrators}: ",
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: S.of(context).qrAdminMessage),
              ],
            ),
            style: const TextStyle(color: AppTheme.pgPrimaryDarkGray),
          ),
          const SizedBox(height: 12),
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: "${S.of(context).remainingUsers}: ",
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: S.of(context).qrRemainingUsersMessage),
              ],
            ),
            style: const TextStyle(color: AppTheme.pgPrimaryDarkGray),
          ),
        ],
      ),
    );
  }
}
