import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/login/login_bloc.dart';

import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/nav_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/common/screens/login/login_screen_style.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_version.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';

class LoginScreen extends StatefulWidget {
  final bool isSessionRestoration;
  const LoginScreen({super.key, required this.isSessionRestoration});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isButtonDisabled = false;

  final bloc = getIt<LoginBloc>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {
          state.when(
            initial: () {},
            success: () async {
              if (context.mounted) {
                widget.isSessionRestoration ? jumpToHomeScreen(context: context) : jumpToUserCallScreen(context: context);
              }
            },
            notConnected: () => ScaffoldMessenger.of(context)
              ..removeCurrentSnackBar()
              ..showSnackBar(
                const SnackBar(
                    backgroundColor: AppTheme.pgPrimaryRed,
                    content: Center(
                      child: Text(
                        "No Connection Established",
                      ),
                    ),
                    duration: Duration(seconds: 4)),
              ),
            error: () {},
          );
        },
        child: PrometheusLogoScaffold(
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                widget.isSessionRestoration
                    ? const Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              'Session Expired',
                              style: loginHeaderStyle,
                            ),
                          ),
                          Text(
                            "Please log in again to continue",
                            style: loginAgainMessageStyle,
                          )
                        ],
                      )
                    : Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              S.current.welcomeMessage,
                              style: loginHeaderStyle,
                            ),
                          ),
                          Text(
                            S.current.loginScreenMessage,
                            style: loginScreenMessageStyle,
                          ),
                        ],
                      ),
                const SizedBox(height: 20),
                _isButtonDisabled
                    ? const CircularProgressIndicator()
                    : Padding(
                        padding: EdgeInsets.symmetric(vertical: 16, horizontal: MediaQuery.of(context).size.width * 0.1),
                        child: ElevatedButton(
                          onPressed: () {
                            _isButtonDisabled ? null : {_disableButton(), bloc.add(const LoginEvent.login())};
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              side: const BorderSide(color: AppTheme.pgPrimaryGray),
                              textStyle: const TextStyle(color: AppTheme.pgPrimaryBlue)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.login,
                                  color: AppTheme.pgPrimaryBlue,
                                  size: 25,
                                ),
                                const SizedBox(width: 16),
                                Text(
                                  S.current.loginButton,
                                  style: loginBtnStyle,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                Flexible(
                  fit: FlexFit.loose,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        style: changeServerBtnStyle,
                        onPressed: () => Platform.isWindows
                            ? Navigator.pushNamed(context, cmnServerCredentialsScreen)
                            : Navigator.pushNamed(context, cmnAppConfigScreen),
                        child: Text(
                          S.current.changeServer.toUpperCase(),
                        ),
                      ),
                      // AppVersion(),
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: Center(child: AppVersion()),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _disableButton() {
    setState(() {
      _isButtonDisabled = true;
    });
    Future.delayed(const Duration(seconds: 4), () {
      setState(() {
        _isButtonDisabled = false;
      });
    });
  }
}
