import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AuthenticationView extends StatefulWidget {
  final String url;
  const AuthenticationView({super.key, required this.url});

  @override
  State<AuthenticationView> createState() => _AuthenticationViewState();
}

class _AuthenticationViewState extends State<AuthenticationView> {
  late final WebViewController controller;
  final WebViewCookieManager cookieManager = WebViewCookieManager();

  @override
  void initState() {
    super.initState();
    clearCookies();
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(
        Uri.parse(widget.url),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('STO Authentication Page'),
      ),
      body: WebViewWidget(
        controller: controller,
      ),
    );
  }

  Future<void> clearCookies() async {
    await cookieManager.clearCookies();
  }
}
