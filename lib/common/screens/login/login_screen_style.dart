import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

const TextStyle loginHeaderStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.bold,
);

const TextStyle loginBtnStyle = TextStyle(
  color: AppTheme.pgPrimaryDarkGray,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

const TextStyle loginScreenMessageStyle = TextStyle(color: AppTheme.pgPrimaryDarkGray);
const TextStyle loginAgainMessageStyle = TextStyle(color: AppTheme.pgPrimaryDarkGray, letterSpacing: 0);

ButtonStyle changeServerBtnStyle = ButtonStyle(foregroundColor: MaterialStateProperty.all(AppTheme.pgPrimaryBlue));
