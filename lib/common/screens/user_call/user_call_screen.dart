import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/user_call/user_call_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/loading_indicator.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_logo_scaffold.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class UserCallScreen extends StatelessWidget {
  const UserCallScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<UserCallBloc>();
    return PrometheusLogoScaffold(
      body: BlocProvider.value(
        value: bloc,
        child: BlocConsumer<UserCallBloc, UserCallState>(
          listener: (context, state) {
            state.maybeWhen(orElse: () {}, success: () => jumpToConfigureTurnAroundEventScreen(context: context), error: (errormsg) => jumpToErrorPage(context: context));
          },
          builder: (context, state) {
            return state.when(
                initial: () => const SizedBox(),
                loading: () => const Center(
                      child: LoadingIndicator(),
                    ),
                success: () => const SizedBox(),
                error: (errormsg) => Center(
                      child: Text(errormsg),
                    ));
          },
        ),
      ),
    );
  }
}
