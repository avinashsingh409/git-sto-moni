import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

class STOElevatedButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onPressed;
  const STOElevatedButton({
    super.key,
    required this.buttonName,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 120,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
            backgroundColor: AppTheme.pgPrimaryRed,
            side: const BorderSide(color: AppTheme.pgPrimaryGray),
            textStyle: const TextStyle(color: AppTheme.pgPrimaryBlue)),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Text(
            buttonName,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
