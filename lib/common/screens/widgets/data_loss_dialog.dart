import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class DataLossDialog extends StatefulWidget {
  final Function() onConfirm;
  final Function()? onCancel;
  const DataLossDialog({super.key, required this.onConfirm, this.onCancel});

  @override
  State<DataLossDialog> createState() => _DataLossDialogState();
}

class _DataLossDialogState extends State<DataLossDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Warning: Data Loss"),
      content: const Text("One or more Activities have been changed since last sync. Logging out or switching events before syncing will erase these changes."),
      actions: [
        TextButton(
          style: TextButton.styleFrom(
            foregroundColor: AppTheme.pgPrimaryDarkGray,
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          child: Text(S.of(context).cancel.toUpperCase()),
          onPressed: () {
            if (widget.onCancel != null) {
              widget.onCancel!();
            }
            Navigator.of(context).pop(false);
          }
        ),
        TextButton(
          style: TextButton.styleFrom(
            foregroundColor: AppTheme.pgPrimaryBlue,
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          child: Text(S.of(context).confirm.toUpperCase()),
          onPressed: () {
            widget.onConfirm();
            Navigator.of(context).pop(false);
          }
        )
      ]
    );
  }
}