import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sto_mobile_v2/config/constants/assets/icon_constants.dart';

const double _logoDimension = 140;

class PrometheusTextLogo extends StatelessWidget {
  const PrometheusTextLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: const TextStyle(color: Colors.white, fontFamily: "TrajanBold"),
      child: Column(
        children: [
          SvgPicture.asset(
            iconPrometheusLogo,
            color: Colors.white,
            width: _logoDimension,
            height: _logoDimension,
          ),
          const Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: "P",
                  style: TextStyle(fontSize: 44),
                ),
                TextSpan(
                  text: "rometheus",
                  style: TextStyle(fontSize: 36),
                ),
              ],
            ),
          ),
          const Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: "G",
                  style: TextStyle(fontSize: 44),
                ),
                TextSpan(
                  text: "roup",
                  style: TextStyle(fontSize: 36),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
