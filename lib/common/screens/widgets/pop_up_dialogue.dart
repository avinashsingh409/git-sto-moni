import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class PopUpDialogue extends StatelessWidget {
  final VoidCallback onPressed;
  final RichText popUpTitle;
  final RichText? popUpSubTitle;
  final bool destructiveAction;
  final bool isPopForChangingEvents;

  const PopUpDialogue(
      {super.key,
      required this.popUpTitle,
      required this.onPressed,
      this.popUpSubTitle,
      this.destructiveAction = false,
      this.isPopForChangingEvents = false});

  @override
  Widget build(BuildContext context) {
    final double popupDialogueWidth = MediaQuery.of(context).size.width * 0.65;
    final double popupDialogueHeight = MediaQuery.of(context).size.height * 0.2;
    final double fontResizeMultiplier = popupDialogueHeight * .0065;

    return AlertDialog(
      content: SizedBox(
        width: popupDialogueWidth,
        height: popupDialogueHeight,
        child: Flex(
          direction: Axis.vertical,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: popUpTitle,
            ),
            if (popUpSubTitle != null)
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: popUpSubTitle,
                ),
              ),
            Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  flex: 1,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      maximumSize: MaterialStateProperty.all(Size.fromWidth(popupDialogueWidth - 12)),
                      backgroundColor: MaterialStateProperty.all(Colors.white),
                    ),
                    onPressed: () => {isPopForChangingEvents ? jumpToHomeScreen(context: context) : Navigator.pop(context)},
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: S.current.popupCancel,
                        style: TextStyle(
                          fontSize: 16 * fontResizeMultiplier,
                          fontWeight: FontWeight.bold,
                          color: AppTheme.pgPrimaryDarkGray,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  flex: 1,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(destructiveAction ? AppTheme.pgPrimaryRed : AppTheme.pgPrimaryBlue),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      onPressed();
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: S.current.confirm,
                        style: TextStyle(
                          fontSize: 16 * fontResizeMultiplier,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
