import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sto_mobile_v2/common/application/app_switcher/app_switcher_bloc.dart';
import 'package:sto_mobile_v2/common/data/app_switcher/datastore/enums/app_features.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/assets/icon_constants.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class AppSwitcher extends StatefulWidget {
  final bool isRescaleForAppSwitcher;
  const AppSwitcher({super.key, required this.isRescaleForAppSwitcher});

  @override
  State<AppSwitcher> createState() => _AppSwitcherState();
}

class _AppSwitcherState extends State<AppSwitcher> {
  final bloc = getIt<AppSwitcherBloc>();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          'Please choose an App Module',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        const SizedBox(
          height: 24,
        ),
        IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              //Planner
              Center(
                  child: AppFeatureButton(
                isRescaleForAppSwitcher: widget.isRescaleForAppSwitcher,
                selectedAppFeature: AppFeatures.planner,
              )),
              const VerticalDivider(
                color: Colors.white,
                thickness: 1,
                width: 1,
              ),
              //Execution
              Center(
                  child: AppFeatureButton(
                isRescaleForAppSwitcher: widget.isRescaleForAppSwitcher,
                selectedAppFeature: AppFeatures.execution,
              )),
            ],
          ),
        ),
      ],
    );
  }
}

class AppFeatureButton extends StatelessWidget {
  final bool isRescaleForAppSwitcher;
  final AppFeatures selectedAppFeature;
  const AppFeatureButton({
    super.key,
    required this.isRescaleForAppSwitcher,
    required this.selectedAppFeature,
  });

  @override
  Widget build(BuildContext context) {
    final imageSize = MediaQuery.of(context).size.height * 0.2;
    final rescaledImageSize = MediaQuery.of(context).size.height * 0.12;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(
          height: 24,
        ),
        Text(
          selectedAppFeature.name.toUpperCase(),
          style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
        ),
        GestureDetector(
            onTap: () {
              getIt<AppSwitcherBloc>()
                  .add(AppSwitcherEvent.switchApp(selectedFeature: selectedAppFeature, isFromHome: isRescaleForAppSwitcher));
            },
            child: SvgPicture.asset(
              selectedAppFeature == AppFeatures.planner ? iconPlannerLogo : iconExecutionLogo,
              height: isRescaleForAppSwitcher ? rescaledImageSize : imageSize,
              width: isRescaleForAppSwitcher ? rescaledImageSize : imageSize,
              fit: BoxFit.cover,
            )),
      ],
    );
  }
}

class AppSwitcherCog extends StatelessWidget {
  const AppSwitcherCog({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        showDialog<void>(
          context: context,
          barrierDismissible: true, // user must tap button!
          builder: (BuildContext context) {
            return const AlertDialog(
              backgroundColor: AppTheme.pgPrimaryRed,
              title: Text(
                'App Switcher',
                style: TextStyle(color: Colors.white),
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: [
                    Text(
                      'Please select an app to switch to',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              actions: [AppSwitcher(isRescaleForAppSwitcher: true)],
            );
          },
        );
      },
      icon: const Icon(Icons.settings),
    );
  }
}
