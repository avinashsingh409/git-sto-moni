import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/injectable/modules/app_version/app_version_module.dart';

class AppVersion extends StatelessWidget {
  const AppVersion({Key? key}) : super(key: key);

//Kekw
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: AppVersionModule.appVersionNumber(),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) => Text(
        snapshot.hasData ? "${S.current.versionNumber} ${snapshot.data!}" : "${S.current.versionNumber} Loading ...",
        style: const TextStyle(color: AppTheme.pgPrimaryDarkGray),
      ),
    );
  }
}
