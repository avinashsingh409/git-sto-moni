import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

class CommentsTextField extends StringField {
  final ValueChanged<String>? valueChangedCallBack;
  CommentsTextField({
    Key? key,
    required TextEditingController controller,
    required String hintText,
    required this.valueChangedCallBack,
  }) : super(
          key: key,
          isPassword: false,
          controller: controller,
          inputDecoration: InputDecoration(
            hintText: hintText,
            errorStyle: const TextStyle(color: AppTheme.pgAccentError),
            hintStyle: TextStyle(color: Colors.grey[700]),
            filled: true,
            contentPadding: const EdgeInsets.fromLTRB(30, 20, 20, 20),
          ),
          hintText: hintText,
          validator: MultiValidator(
            [
              RequiredValidator(errorText: "Required"),
            ],
          ),
          onChanged: valueChangedCallBack,
        );
}

class ServerCredentialsTextField extends StringField {
  final ValueChanged<String>? valueChangedCallBack;
  ServerCredentialsTextField({
    Key? key,
    required TextEditingController controller,
    required String hintText,
    required this.valueChangedCallBack,
  }) : super(
          key: key,
          isPassword: false,
          controller: controller,
          inputDecoration: InputDecoration(
            hintText: hintText,
            errorStyle: const TextStyle(color: AppTheme.pgAccentError),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            hintStyle: TextStyle(color: Colors.grey[700]),
            filled: true,
            contentPadding: const EdgeInsets.fromLTRB(30, 20, 20, 20),
          ),
          hintText: hintText,
          validator: MultiValidator(
            [
              RequiredValidator(errorText: "Required"),
              MinLengthValidator(2, errorText: "Required"),
            ],
          ),
          onChanged: valueChangedCallBack,
        );
}

class FieldTypeTextField extends StringField {
  final ValueChanged<String>? valueChangedCallBack;
  final int? maxLength;
  final bool isReadOnly;
  FieldTypeTextField({
    Key? key,
    required this.maxLength,
    required TextEditingController controller,
    required String hintText,
    required this.valueChangedCallBack,
    required this.isReadOnly,
  }) : super(
          key: key,
          readOnly: isReadOnly,
          isPassword: false,
          controller: controller,
          // inputFormatters: [
          //   LengthLimitingTextInputFormatter(maxLength),
          // ],
          inputDecoration: InputDecoration(
            hintText: hintText,
            errorStyle: const TextStyle(color: AppTheme.pgAccentError),
            hintStyle: TextStyle(color: Colors.grey[700]),
            filled: true,
            suffixIcon: isReadOnly
                ? const Icon(
                    Icons.lock,
                    color: Colors.black54,
                  )
                : const SizedBox(),
            fillColor: Colors.white,
            contentPadding: const EdgeInsets.fromLTRB(4, 20, 0, 0),
          ),
          hintText: hintText,

          validator: MultiValidator(
            [
              RequiredValidator(errorText: "Required"),
              MinLengthValidator(2, errorText: "Required"),
            ],
          ),
          onChanged: valueChangedCallBack,
        );
}

class FieldTypeNumberField extends StringField {
  final ValueChanged<String>? valueChangedCallBack;
  final bool allowDecimals;
  final bool isReadOnly;
  FieldTypeNumberField({
    Key? key,
    required this.allowDecimals,
    required TextEditingController controller,
    required String hintText,
    required this.valueChangedCallBack,
    required this.isReadOnly,
  }) : super(
          key: key,
          isPassword: false,
          controller: controller,
          readOnly: isReadOnly,
          keyboardType: TextInputType.number,
          inputFormatters: [
            allowDecimals
                ? FilteringTextInputFormatter.allow(RegExp(r'(^\-?\d*\.?\d*)'))
                : FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
          ],
          inputDecoration: InputDecoration(
            suffixIcon: isReadOnly
                ? const Icon(
                    Icons.lock,
                    color: Colors.black54,
                  )
                : const SizedBox(),
            hintText: hintText,
            errorStyle: const TextStyle(color: AppTheme.pgAccentError),
            hintStyle: TextStyle(color: Colors.grey[700]),
            // filled: true,
            contentPadding: const EdgeInsets.fromLTRB(4, 20, 0, 0),
          ),
          hintText: hintText,
          validator: MultiValidator(
            [
              RequiredValidator(errorText: "Required"),
              MinLengthValidator(2, errorText: "Required"),
            ],
          ),
          onChanged: valueChangedCallBack,
        );
}

class StringField extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final bool isPassword;
  final InputDecoration inputDecoration;
  final ValueChanged<String>? onChanged;
  final TextInputType keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final bool readOnly;
  const StringField({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.validator,
    this.isPassword = false,
    this.keyboardType = TextInputType.text,
    this.inputFormatters,
    required this.inputDecoration,
    required this.onChanged,
    this.readOnly = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.8,
      child: TextFormField(
        obscureText: isPassword,
        readOnly: readOnly,
        keyboardType: keyboardType,
        validator: validator,
        autofocus: false,
        inputFormatters: inputFormatters,
        textInputAction: TextInputAction.done,
        style: const TextStyle(color: Colors.black),
        controller: controller,
        decoration: inputDecoration,
        onChanged: onChanged,
      ),
    );
  }
}
