import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sto_mobile_v2/common/application/error_404/error_404_bloc.dart';
import 'package:sto_mobile_v2/common/screens/widgets/app_switcher.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class AppSwitcherErrorPage extends StatelessWidget {
  const AppSwitcherErrorPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = getIt<Error_404Bloc>();
    return BlocProvider(
      create: (context) => bloc,
      child: Scaffold(
        body: Container(
          color: AppTheme.pgPrimaryRed,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppSwitcher(isRescaleForAppSwitcher: false),
                ],
              ),
              const SizedBox(
                height: 80,
              ),
              const Text(
                'Stuck on this Screen?',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text(
                  'Make sure you have the Right Licenses or Have the Proper Server Config Setup.',
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              ElevatedButton(
                onPressed: () async {
                  bloc.add(const Error_404Event.clearDataAndRestart());
                },
                child: const Text('Exit and Restart'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
