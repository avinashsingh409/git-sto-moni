import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/common/screens/widgets/logs_button.dart';
import 'package:sto_mobile_v2/common/screens/widgets/prometheus_text_logo.dart';

const double _logoHeight = 380;

class PrometheusLogoScaffold extends StatelessWidget {
  const PrometheusLogoScaffold({required this.body, Key? key}) : super(key: key);

  final Widget body;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Flex(
        direction: Axis.vertical,
        children: [
          Flexible(
            fit: FlexFit.loose,
            flex: 2,
            // width: screenSize.width,
            // height: _logoHeight,
            child: Stack(
              children: [
                CustomPaint(
                  painter: _CurvePainter(screenSize.width),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 40),
                  child: Center(child: PrometheusTextLogo()),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 42,
                      ),
                      LogsButton(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            fit: FlexFit.tight,
            flex: 2,
            child: body,
          )
        ],
      ),
    );
  }
}

class _CurvePainter extends CustomPainter {
  _CurvePainter(this._screenWidth);

  final double _screenWidth;

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = AppTheme.pgPrimaryRed
      ..style = PaintingStyle.fill;

    final path = Path()
      ..moveTo(0, _logoHeight)
      ..quadraticBezierTo(_screenWidth * 0.35, 300, _screenWidth * 0.6, 312)
      ..quadraticBezierTo(_screenWidth * 0.9, 320, _screenWidth, 268)
      ..lineTo(_screenWidth, 0)
      ..lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant _CurvePainter oldDelegate) {
    return oldDelegate._screenWidth != _screenWidth;
  }
}
