import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sto_mobile_v2/common/application/logout/logout_bloc.dart';
import 'package:sto_mobile_v2/common/application/profile_drawer/profile_drawer_cubit.dart';
import 'package:sto_mobile_v2/common/application/report_problem/report_problem_cubit.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/constants/assets/icon_constants.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class AppDrawer extends StatefulWidget {
  final LogoutBloc logoutBloc;
  const AppDrawer({
    super.key,
    required this.logoutBloc,
  });

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  bool _isButtonDisabled = false;
  @override
  Widget build(BuildContext context) {
    final cubit = getIt<ProfileDrawerCubit>();
    cubit.getDetails();
    return SizedBox(
      child: Drawer(
        child: BlocProvider.value(
          value: cubit,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                height: 48,
                color: AppTheme.pgPrimaryRed,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.2,
                color: AppTheme.pgPrimaryRed,
                child: SvgPicture.asset(
                  iconPrometheusLogo,
                  color: Colors.white,
                ),
              ),
              Container(
                height: 42,
                color: AppTheme.pgPrimaryRed,
              ),
              const SizedBox(
                height: 20,
              ),
              BlocBuilder<ProfileDrawerCubit, ProfileDrawerState>(
                builder: (context, state) {
                  return state.when(
                    initial: () => const SizedBox(),
                    profileLoaded: (userInfo) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const CircleAvatar(
                              backgroundColor: Colors.grey,
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Text(
                              "${userInfo.givenName ?? ''} ${userInfo.familyName ?? ''}",
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Text(
                              userInfo.email ?? '',
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
              ),
              const Divider(
                color: AppTheme.pgPrimaryBlack,
              ),
              ListTile(
                leading: const Icon(
                  Icons.file_copy_outlined,
                ),
                title: const Text(
                  'Configure Filters',
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                onTap: () {
                  jumpToFilterConfigScreen(context: context);
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.error,
                  color: AppTheme.pgAccentError,
                ),
                title: const Text(
                  'Report a problem',
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                onTap: () {
                  Navigator.pop(context);
                  getIt<ReportProblemCubit>().popUpModal();
                },
              ),
              _isButtonDisabled
                  ? const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: LinearProgressIndicator(),
                    )
                  : ListTile(
                      leading: const Icon(
                        Icons.logout,
                      ),
                      title: const Text(
                        'Logout',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      onTap: () {
                        _isButtonDisabled
                            ? null
                            : {
                                _disableButton(),
                                widget.logoutBloc.add(const LogoutEvent.prepareLogout()),
                              };
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }

  void _disableButton() {
    setState(() {
      _isButtonDisabled = true;
    });
    Future.delayed(const Duration(seconds: 3), () {
      if (mounted) {
        setState(() {
          _isButtonDisabled = false;
        });
      }
    });
  }
}
