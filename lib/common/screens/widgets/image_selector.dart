import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';

void showImageSelection({
  required BuildContext context,
  required Function(String) onSelection,
}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Center(
        child: Container(
          decoration: const BoxDecoration(
            color: AppTheme.pgPrimaryRed,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          height: 80,
          width: 250,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (!Platform.isWindows)
                TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).pop();
                    _showCameraPicker(context).then(
                      (imageString) {
                        if (imageString != null) {
                          onSelection(imageString);
                        }
                      },
                    );
                  },
                  icon: const Icon(
                    Icons.camera,
                    color: Colors.white,
                    size: 20,
                  ),
                  label: const Text(
                    'Camera',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                ),
              if (!Platform.isWindows)
                VerticalDivider(
                  thickness: 1,
                  color: Colors.white.withOpacity(0.5),
                ),
              TextButton.icon(
                onPressed: () {
                  Navigator.of(context).pop();
                  _showImagePicker(context).then(
                    (imageString) {
                      if (imageString != null) {
                        onSelection(imageString);
                      }
                    },
                  );
                },
                icon: const Icon(
                  Icons.folder,
                  color: Colors.white,
                  size: 20,
                ),
                label: const Text(
                  'Gallery',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}

Future<String?> _showImagePicker(BuildContext context) async {
  final ImagePicker imagePicker = ImagePicker();
  final PickedFile? pickedFile = await imagePicker.getImage(
    source: ImageSource.gallery,
    imageQuality: 25,
  );
  if (pickedFile != null) {
    final File file = File(pickedFile.path);
    List<int> imageBase64 = file.readAsBytesSync();
    String imageAsString = base64Encode(imageBase64);
    return imageAsString;
  }
  return null;
}

Future<String?> _showCameraPicker(BuildContext context) async {
  final ImagePicker imagePicker = ImagePicker();
  final PickedFile? pickedFile = await imagePicker.getImage(
    source: ImageSource.camera,
    imageQuality: 25,
  );
  if (pickedFile != null) {
    final File file = File(pickedFile.path);
    List<int> imageBase64 = file.readAsBytesSync();
    String imageAsString = base64Encode(imageBase64);
    return imageAsString;
  }
  return null;
}
