import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';
import 'package:sto_mobile_v2/common/screens/widgets/string_field.dart';

class CommentDialog extends StatefulWidget {
  final Function(String) onCommentSubmitted;
  const CommentDialog({super.key, required this.onCommentSubmitted});

  @override
  State<CommentDialog> createState() => _CommentDialogState();
}

class _CommentDialogState extends State<CommentDialog> {
  final TextEditingController _commentController = TextEditingController();
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.current.addComment),
      content: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(S.current.provideNegativeProgressReason),
            const SizedBox(
              height: 20,
            ),
            CommentsTextField(
              hintText: S.current.commentsHintText,
              controller: _commentController,
              valueChangedCallBack: (_) {},
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          style: TextButton.styleFrom(
            backgroundColor: AppTheme.pgPrimaryDarkGray,
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          child: Text(
            S.of(context).cancel.toUpperCase(),
          ),
          onPressed: () {
            Navigator.of(context).pop(false);
          },
        ),
        TextButton(
            style: TextButton.styleFrom(
              backgroundColor: AppTheme.pgPrimaryBlue,
              textStyle: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            child: Text(
              S.of(context).save.toUpperCase(),
            ),
            onPressed: () async {
              final navigator = Navigator.of(context);
              if (_formKey.currentState!.validate()) {
                await widget.onCommentSubmitted(_commentController.text.trim());
                navigator.pop();
              }
            }),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _commentController.dispose();
  }
}
