import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class ErrorIndicator extends StatelessWidget {
  const ErrorIndicator({
    required this.errorMessage,
    this.onTryAgain,
    Key? key,
  }) : super(key: key);

  final String errorMessage;
  final VoidCallback? onTryAgain;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const SizedBox(height: 32),
          Text(
            S.of(context).somethingWentWrong,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 16),
          Text(
            errorMessage,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 48),
          if (onTryAgain != null)
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              height: 50,
              child: ElevatedButton(
                onPressed: onTryAgain,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(Icons.refresh),
                    const SizedBox(width: 8),
                    Text(
                      S.of(context).tryAgain,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
