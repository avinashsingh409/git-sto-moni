import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/core/localization/generated/l10n.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({
    this.color = AppTheme.pgPrimaryRed,
    this.textColor = AppTheme.pgPrimaryDarkGray,
    this.width = 40,
    this.height = 40,
    this.fontSize = 20,
    this.strokeWidth = 4,
    this.bottomMargin = 20,
    this.loadingMessage = "",
    super.key,
  });

  final Color color;
  final Color textColor;
  final double width;
  final double height;
  final double fontSize;
  final double strokeWidth;
  final double bottomMargin;
  final String loadingMessage;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: width,
            height: height,
            margin: EdgeInsets.only(bottom: bottomMargin),
            child: CircularProgressIndicator(
              color: color,
              strokeWidth: strokeWidth,
            ),
          ),
          Text(
            (loadingMessage.isNotEmpty) ? loadingMessage : S.of(context).loadingData,
            style: TextStyle(
              color: textColor,
              fontSize: fontSize,
            ),
          ),
        ],
      ),
    );
  }
}
