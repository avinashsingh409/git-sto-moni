import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

void showErrorToast({required bool isBannerRed, required String errorText}) {
  ScaffoldMessenger.of(GlobalNavigationKey().context!)
    ..removeCurrentSnackBar()
    ..showSnackBar(SnackBar(
        backgroundColor: isBannerRed ? AppTheme.pgPrimaryRed : Colors.white,
        content: Center(
          child: Text(
            errorText,
            style: TextStyle(color: isBannerRed ? Colors.white : Colors.red, fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        duration: const Duration(seconds: 2)));
}
