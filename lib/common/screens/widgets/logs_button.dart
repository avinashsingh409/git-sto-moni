import 'package:flutter/material.dart';
import 'package:sto_mobile_v2/common/application/report_problem/report_problem_cubit.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

class LogsButton extends StatelessWidget {
  const LogsButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
        iconSize: 32,
        onPressed: () {
          getIt<ReportProblemCubit>().popUpModal();
        },
        icon: const Icon(
          Icons.receipt,
          color: Colors.white,
        ));
  }
}
