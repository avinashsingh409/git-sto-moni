import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  final String text;
  final Widget child;
  final Color? color;
  const ExpandableText({required this.text, required this.child, this.color, super.key});

  @override
  State<ExpandableText> createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText> {
  bool isOpen = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isOpen = !isOpen;
        });
      },
      child: Column(
        children: [
          Row(
            children: [
              Text(widget.text, style: TextStyle(color: widget.color)),
              Icon(
                isOpen ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                color: widget.color,
              )
            ],
          ),
          if (isOpen) widget.child,
        ],
      ),
    );
  }
}
