abstract class IDataLossDataStore {
  Future<bool> isDataChanged();
}