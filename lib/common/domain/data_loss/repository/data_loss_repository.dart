abstract class IDataLossRepository {
  Future<bool> isDataChanged();
}