import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/domain/exception/logout_exception.dart';

abstract class ILogoutRepository {
  Future<Either<Unit, LogoutException>> logout({required AppConfig appConfig});
  Future<void> clearData();
}
