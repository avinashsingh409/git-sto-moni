import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session.dart';
import 'package:sto_mobile_v2/common/domain/exception/session_exception.dart';

abstract class ISessionRepository {
  Future<Either<Session, SessionException>> login({required AppConfig appConfig});
  Either<Session, Unit> getSession();
  Future<void> insertSession({required Session session});
}
