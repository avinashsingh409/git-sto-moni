abstract class ILogoutDataStore {
  Future<void> clearData();
}
