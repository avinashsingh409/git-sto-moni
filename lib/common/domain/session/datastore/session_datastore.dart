import 'package:openid_client/openid_client_io.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';

abstract class ISessionDataStore {
  Future<TokenResponse> login({required AppConfig appConfig});
  SessionDTO? getSession();
  Future<void> insertSession({required SessionDTO sessionDTO});
  Future<void> deleteSession();
}
