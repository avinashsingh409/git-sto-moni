import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/domain/exception/user_call_exception.dart';

abstract class IUserCallRepository {
  Future<Either<Unit, UserCallException>> getAndCacheUserCall();
  bool getIsSwipeEnabled();
}
