import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_call_response.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_dto.dart';

abstract class IUserCallDataStore {
  Future<UserCallResponse?> getUserCall();
  Future<void> upsertUser({required UserDTO userDTO});
  bool getIsSwipeEnabled();
  Future<void> deleteCachedUser();
}
