import 'package:rxdart/subjects.dart';

abstract class ISyncTickerDataStore {
  BehaviorSubject<bool> isSyncPending();
  Future<int> updateStatus();
  Future<void> clearStatus();
}
