import 'package:rxdart/subjects.dart';

abstract class ISyncTickerRepository {
  BehaviorSubject<bool> isSyncPending();
  Future<int> updateStatus();
  Future<void> clearStatus();
}
