import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_data_exception.dart';

abstract class ITurnaroundDataRepository {
  Future<Either<Unit, TurnAroundDataException>> getAllTurnaroundData();
}
