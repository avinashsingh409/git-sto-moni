import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_config_exception.dart';

import 'package:dartz/dartz.dart';

abstract class IAppConfigRepository {
  Either<AppConfig, AppConfigException> getAppConfig();
  Future<void> insertAppConfig({required AppConfig appConfig});
  Future<void> deleteConfigs();
  Either<AppConfig, AppConfigException> decodeAppConfig({required String appConfigString});
}
