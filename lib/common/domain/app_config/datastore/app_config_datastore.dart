import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';

abstract class IAppConfigDataStore {
  AppConfigDTO? getAppConfig();
  Future<void> insertAppConfig({required AppConfigDTO appConfigDTO});
  Future<void> deleteConfigs();
}
