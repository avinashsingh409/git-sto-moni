import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';

abstract class ILicenseDataStore {
  Future<LicenseDTO?> checkServiceLicense();
  Future<int> cacheServiceLicense({required LicenseDTO license});
  Future<void> clearCachedLicense();
  Future<LicenseDTO?> getCachedLicense();
}
