import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/license_exception.dart';

abstract class ILicenseRepository {
  Future<Either<LicenseDTO, LicenseException>> getAndCacheServiceLicense();
  Future<LicenseDTO?> getCachedLicense();
}
