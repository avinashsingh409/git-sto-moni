import 'package:freezed_annotation/freezed_annotation.dart';
part 'license_exception.freezed.dart';

@freezed
abstract class LicenseException with _$LicenseException {
  const factory LicenseException.noDataFound() = _NoDataFound;
  const factory LicenseException.unexpectedError() = _UnexpectedError;
  const factory LicenseException.socketException() = _SocketException;
  const factory LicenseException.connectTimeOut() = _ConnectTimeOut;

  // static String getErrorMessage(LicenseException exceptions) {
  //   return exceptions.when(
  //     noDataFound: () {
  //       return "No Activities Found";
  //     },
  //     unexpectedError: () {
  //       return "Unexpected Error";
  //     },
  //   );
  // }
}
