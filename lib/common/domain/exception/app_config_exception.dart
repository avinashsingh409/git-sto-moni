import 'package:freezed_annotation/freezed_annotation.dart';
part 'app_config_exception.freezed.dart';

@freezed
abstract class AppConfigException with _$AppConfigException {
  const factory AppConfigException.notConfigured() = _NotConfigured;
  const factory AppConfigException.emptyClipBoard() = _EmptyClipBoard;
  const factory AppConfigException.decodeError() = _DecodeError;

  static String getErrorMessage(AppConfigException exceptions) {
    return exceptions.when(
      notConfigured: () {
        return "Not Configured";
      },
      decodeError: () {
        return "Invalid Config";
      },
      emptyClipBoard: () {
        return "Nothing in Clipboard";
      },
    );
  }
}
