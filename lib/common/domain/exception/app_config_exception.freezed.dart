// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_config_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppConfigException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notConfigured,
    required TResult Function() emptyClipBoard,
    required TResult Function() decodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? notConfigured,
    TResult? Function()? emptyClipBoard,
    TResult? Function()? decodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notConfigured,
    TResult Function()? emptyClipBoard,
    TResult Function()? decodeError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NotConfigured value) notConfigured,
    required TResult Function(_EmptyClipBoard value) emptyClipBoard,
    required TResult Function(_DecodeError value) decodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NotConfigured value)? notConfigured,
    TResult? Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult? Function(_DecodeError value)? decodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NotConfigured value)? notConfigured,
    TResult Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult Function(_DecodeError value)? decodeError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppConfigExceptionCopyWith<$Res> {
  factory $AppConfigExceptionCopyWith(
          AppConfigException value, $Res Function(AppConfigException) then) =
      _$AppConfigExceptionCopyWithImpl<$Res, AppConfigException>;
}

/// @nodoc
class _$AppConfigExceptionCopyWithImpl<$Res, $Val extends AppConfigException>
    implements $AppConfigExceptionCopyWith<$Res> {
  _$AppConfigExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_NotConfiguredCopyWith<$Res> {
  factory _$$_NotConfiguredCopyWith(
          _$_NotConfigured value, $Res Function(_$_NotConfigured) then) =
      __$$_NotConfiguredCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NotConfiguredCopyWithImpl<$Res>
    extends _$AppConfigExceptionCopyWithImpl<$Res, _$_NotConfigured>
    implements _$$_NotConfiguredCopyWith<$Res> {
  __$$_NotConfiguredCopyWithImpl(
      _$_NotConfigured _value, $Res Function(_$_NotConfigured) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NotConfigured implements _NotConfigured {
  const _$_NotConfigured();

  @override
  String toString() {
    return 'AppConfigException.notConfigured()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NotConfigured);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notConfigured,
    required TResult Function() emptyClipBoard,
    required TResult Function() decodeError,
  }) {
    return notConfigured();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? notConfigured,
    TResult? Function()? emptyClipBoard,
    TResult? Function()? decodeError,
  }) {
    return notConfigured?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notConfigured,
    TResult Function()? emptyClipBoard,
    TResult Function()? decodeError,
    required TResult orElse(),
  }) {
    if (notConfigured != null) {
      return notConfigured();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NotConfigured value) notConfigured,
    required TResult Function(_EmptyClipBoard value) emptyClipBoard,
    required TResult Function(_DecodeError value) decodeError,
  }) {
    return notConfigured(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NotConfigured value)? notConfigured,
    TResult? Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult? Function(_DecodeError value)? decodeError,
  }) {
    return notConfigured?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NotConfigured value)? notConfigured,
    TResult Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult Function(_DecodeError value)? decodeError,
    required TResult orElse(),
  }) {
    if (notConfigured != null) {
      return notConfigured(this);
    }
    return orElse();
  }
}

abstract class _NotConfigured implements AppConfigException {
  const factory _NotConfigured() = _$_NotConfigured;
}

/// @nodoc
abstract class _$$_EmptyClipBoardCopyWith<$Res> {
  factory _$$_EmptyClipBoardCopyWith(
          _$_EmptyClipBoard value, $Res Function(_$_EmptyClipBoard) then) =
      __$$_EmptyClipBoardCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_EmptyClipBoardCopyWithImpl<$Res>
    extends _$AppConfigExceptionCopyWithImpl<$Res, _$_EmptyClipBoard>
    implements _$$_EmptyClipBoardCopyWith<$Res> {
  __$$_EmptyClipBoardCopyWithImpl(
      _$_EmptyClipBoard _value, $Res Function(_$_EmptyClipBoard) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_EmptyClipBoard implements _EmptyClipBoard {
  const _$_EmptyClipBoard();

  @override
  String toString() {
    return 'AppConfigException.emptyClipBoard()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_EmptyClipBoard);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notConfigured,
    required TResult Function() emptyClipBoard,
    required TResult Function() decodeError,
  }) {
    return emptyClipBoard();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? notConfigured,
    TResult? Function()? emptyClipBoard,
    TResult? Function()? decodeError,
  }) {
    return emptyClipBoard?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notConfigured,
    TResult Function()? emptyClipBoard,
    TResult Function()? decodeError,
    required TResult orElse(),
  }) {
    if (emptyClipBoard != null) {
      return emptyClipBoard();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NotConfigured value) notConfigured,
    required TResult Function(_EmptyClipBoard value) emptyClipBoard,
    required TResult Function(_DecodeError value) decodeError,
  }) {
    return emptyClipBoard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NotConfigured value)? notConfigured,
    TResult? Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult? Function(_DecodeError value)? decodeError,
  }) {
    return emptyClipBoard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NotConfigured value)? notConfigured,
    TResult Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult Function(_DecodeError value)? decodeError,
    required TResult orElse(),
  }) {
    if (emptyClipBoard != null) {
      return emptyClipBoard(this);
    }
    return orElse();
  }
}

abstract class _EmptyClipBoard implements AppConfigException {
  const factory _EmptyClipBoard() = _$_EmptyClipBoard;
}

/// @nodoc
abstract class _$$_DecodeErrorCopyWith<$Res> {
  factory _$$_DecodeErrorCopyWith(
          _$_DecodeError value, $Res Function(_$_DecodeError) then) =
      __$$_DecodeErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_DecodeErrorCopyWithImpl<$Res>
    extends _$AppConfigExceptionCopyWithImpl<$Res, _$_DecodeError>
    implements _$$_DecodeErrorCopyWith<$Res> {
  __$$_DecodeErrorCopyWithImpl(
      _$_DecodeError _value, $Res Function(_$_DecodeError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_DecodeError implements _DecodeError {
  const _$_DecodeError();

  @override
  String toString() {
    return 'AppConfigException.decodeError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_DecodeError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() notConfigured,
    required TResult Function() emptyClipBoard,
    required TResult Function() decodeError,
  }) {
    return decodeError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? notConfigured,
    TResult? Function()? emptyClipBoard,
    TResult? Function()? decodeError,
  }) {
    return decodeError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? notConfigured,
    TResult Function()? emptyClipBoard,
    TResult Function()? decodeError,
    required TResult orElse(),
  }) {
    if (decodeError != null) {
      return decodeError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NotConfigured value) notConfigured,
    required TResult Function(_EmptyClipBoard value) emptyClipBoard,
    required TResult Function(_DecodeError value) decodeError,
  }) {
    return decodeError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NotConfigured value)? notConfigured,
    TResult? Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult? Function(_DecodeError value)? decodeError,
  }) {
    return decodeError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NotConfigured value)? notConfigured,
    TResult Function(_EmptyClipBoard value)? emptyClipBoard,
    TResult Function(_DecodeError value)? decodeError,
    required TResult orElse(),
  }) {
    if (decodeError != null) {
      return decodeError(this);
    }
    return orElse();
  }
}

abstract class _DecodeError implements AppConfigException {
  const factory _DecodeError() = _$_DecodeError;
}
