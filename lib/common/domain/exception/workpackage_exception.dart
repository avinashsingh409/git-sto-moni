import 'package:freezed_annotation/freezed_annotation.dart';
part 'workpackage_exception.freezed.dart';

@freezed
abstract class WorkpackagesException with _$WorkpackagesException {
  const factory WorkpackagesException.unauthorized() = _Unauthorized;
  const factory WorkpackagesException.unexpectedError() = _UnexpectedError;
  const factory WorkpackagesException.connectTimeOut() = _ConnectTimeOut;
  const factory WorkpackagesException.socketException() = _SocketException;
  const factory WorkpackagesException.noActivityOnline() = _NoActivityOnline;
  const factory WorkpackagesException.noCachedWorkpackages() = _NoCachedWorkpackages;

  static String getErrorMessage(WorkpackagesException exceptions) {
    return exceptions.when(
        unauthorized: () {
          return "Unauthorized Access";
        },
        connectTimeOut: () => "Unstable Internet Connection ",
        socketException: () => "No Internet Connection ",
        unexpectedError: () {
          return "Unexpected Error";
        },
        noActivityOnline: () {
          return "Couldn't find any Workpackages Online ";
        },
        noCachedWorkpackages: () {
          return "No Cached Workpackages";
        });
  }
}
