// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_linker_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppLinkerException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noWorkPackageFound,
    required TResult Function() noActivitiesFound,
    required TResult Function() noWBSFound,
    required TResult Function() notLicensed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noWorkPackageFound,
    TResult? Function()? noActivitiesFound,
    TResult? Function()? noWBSFound,
    TResult? Function()? notLicensed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noWorkPackageFound,
    TResult Function()? noActivitiesFound,
    TResult Function()? noWBSFound,
    TResult Function()? notLicensed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoWorkPackageFound value) noWorkPackageFound,
    required TResult Function(_NoActivitiesFound value) noActivitiesFound,
    required TResult Function(_NoWBSFound value) noWBSFound,
    required TResult Function(_NotLicensed value) notLicensed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult? Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult? Function(_NoWBSFound value)? noWBSFound,
    TResult? Function(_NotLicensed value)? notLicensed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult Function(_NoWBSFound value)? noWBSFound,
    TResult Function(_NotLicensed value)? notLicensed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppLinkerExceptionCopyWith<$Res> {
  factory $AppLinkerExceptionCopyWith(
          AppLinkerException value, $Res Function(AppLinkerException) then) =
      _$AppLinkerExceptionCopyWithImpl<$Res, AppLinkerException>;
}

/// @nodoc
class _$AppLinkerExceptionCopyWithImpl<$Res, $Val extends AppLinkerException>
    implements $AppLinkerExceptionCopyWith<$Res> {
  _$AppLinkerExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_NoWorkPackageFoundCopyWith<$Res> {
  factory _$$_NoWorkPackageFoundCopyWith(_$_NoWorkPackageFound value,
          $Res Function(_$_NoWorkPackageFound) then) =
      __$$_NoWorkPackageFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoWorkPackageFoundCopyWithImpl<$Res>
    extends _$AppLinkerExceptionCopyWithImpl<$Res, _$_NoWorkPackageFound>
    implements _$$_NoWorkPackageFoundCopyWith<$Res> {
  __$$_NoWorkPackageFoundCopyWithImpl(
      _$_NoWorkPackageFound _value, $Res Function(_$_NoWorkPackageFound) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoWorkPackageFound implements _NoWorkPackageFound {
  const _$_NoWorkPackageFound();

  @override
  String toString() {
    return 'AppLinkerException.noWorkPackageFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoWorkPackageFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noWorkPackageFound,
    required TResult Function() noActivitiesFound,
    required TResult Function() noWBSFound,
    required TResult Function() notLicensed,
  }) {
    return noWorkPackageFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noWorkPackageFound,
    TResult? Function()? noActivitiesFound,
    TResult? Function()? noWBSFound,
    TResult? Function()? notLicensed,
  }) {
    return noWorkPackageFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noWorkPackageFound,
    TResult Function()? noActivitiesFound,
    TResult Function()? noWBSFound,
    TResult Function()? notLicensed,
    required TResult orElse(),
  }) {
    if (noWorkPackageFound != null) {
      return noWorkPackageFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoWorkPackageFound value) noWorkPackageFound,
    required TResult Function(_NoActivitiesFound value) noActivitiesFound,
    required TResult Function(_NoWBSFound value) noWBSFound,
    required TResult Function(_NotLicensed value) notLicensed,
  }) {
    return noWorkPackageFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult? Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult? Function(_NoWBSFound value)? noWBSFound,
    TResult? Function(_NotLicensed value)? notLicensed,
  }) {
    return noWorkPackageFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult Function(_NoWBSFound value)? noWBSFound,
    TResult Function(_NotLicensed value)? notLicensed,
    required TResult orElse(),
  }) {
    if (noWorkPackageFound != null) {
      return noWorkPackageFound(this);
    }
    return orElse();
  }
}

abstract class _NoWorkPackageFound implements AppLinkerException {
  const factory _NoWorkPackageFound() = _$_NoWorkPackageFound;
}

/// @nodoc
abstract class _$$_NoActivitiesFoundCopyWith<$Res> {
  factory _$$_NoActivitiesFoundCopyWith(_$_NoActivitiesFound value,
          $Res Function(_$_NoActivitiesFound) then) =
      __$$_NoActivitiesFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoActivitiesFoundCopyWithImpl<$Res>
    extends _$AppLinkerExceptionCopyWithImpl<$Res, _$_NoActivitiesFound>
    implements _$$_NoActivitiesFoundCopyWith<$Res> {
  __$$_NoActivitiesFoundCopyWithImpl(
      _$_NoActivitiesFound _value, $Res Function(_$_NoActivitiesFound) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoActivitiesFound implements _NoActivitiesFound {
  const _$_NoActivitiesFound();

  @override
  String toString() {
    return 'AppLinkerException.noActivitiesFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoActivitiesFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noWorkPackageFound,
    required TResult Function() noActivitiesFound,
    required TResult Function() noWBSFound,
    required TResult Function() notLicensed,
  }) {
    return noActivitiesFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noWorkPackageFound,
    TResult? Function()? noActivitiesFound,
    TResult? Function()? noWBSFound,
    TResult? Function()? notLicensed,
  }) {
    return noActivitiesFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noWorkPackageFound,
    TResult Function()? noActivitiesFound,
    TResult Function()? noWBSFound,
    TResult Function()? notLicensed,
    required TResult orElse(),
  }) {
    if (noActivitiesFound != null) {
      return noActivitiesFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoWorkPackageFound value) noWorkPackageFound,
    required TResult Function(_NoActivitiesFound value) noActivitiesFound,
    required TResult Function(_NoWBSFound value) noWBSFound,
    required TResult Function(_NotLicensed value) notLicensed,
  }) {
    return noActivitiesFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult? Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult? Function(_NoWBSFound value)? noWBSFound,
    TResult? Function(_NotLicensed value)? notLicensed,
  }) {
    return noActivitiesFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult Function(_NoWBSFound value)? noWBSFound,
    TResult Function(_NotLicensed value)? notLicensed,
    required TResult orElse(),
  }) {
    if (noActivitiesFound != null) {
      return noActivitiesFound(this);
    }
    return orElse();
  }
}

abstract class _NoActivitiesFound implements AppLinkerException {
  const factory _NoActivitiesFound() = _$_NoActivitiesFound;
}

/// @nodoc
abstract class _$$_NoWBSFoundCopyWith<$Res> {
  factory _$$_NoWBSFoundCopyWith(
          _$_NoWBSFound value, $Res Function(_$_NoWBSFound) then) =
      __$$_NoWBSFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoWBSFoundCopyWithImpl<$Res>
    extends _$AppLinkerExceptionCopyWithImpl<$Res, _$_NoWBSFound>
    implements _$$_NoWBSFoundCopyWith<$Res> {
  __$$_NoWBSFoundCopyWithImpl(
      _$_NoWBSFound _value, $Res Function(_$_NoWBSFound) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoWBSFound implements _NoWBSFound {
  const _$_NoWBSFound();

  @override
  String toString() {
    return 'AppLinkerException.noWBSFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoWBSFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noWorkPackageFound,
    required TResult Function() noActivitiesFound,
    required TResult Function() noWBSFound,
    required TResult Function() notLicensed,
  }) {
    return noWBSFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noWorkPackageFound,
    TResult? Function()? noActivitiesFound,
    TResult? Function()? noWBSFound,
    TResult? Function()? notLicensed,
  }) {
    return noWBSFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noWorkPackageFound,
    TResult Function()? noActivitiesFound,
    TResult Function()? noWBSFound,
    TResult Function()? notLicensed,
    required TResult orElse(),
  }) {
    if (noWBSFound != null) {
      return noWBSFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoWorkPackageFound value) noWorkPackageFound,
    required TResult Function(_NoActivitiesFound value) noActivitiesFound,
    required TResult Function(_NoWBSFound value) noWBSFound,
    required TResult Function(_NotLicensed value) notLicensed,
  }) {
    return noWBSFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult? Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult? Function(_NoWBSFound value)? noWBSFound,
    TResult? Function(_NotLicensed value)? notLicensed,
  }) {
    return noWBSFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult Function(_NoWBSFound value)? noWBSFound,
    TResult Function(_NotLicensed value)? notLicensed,
    required TResult orElse(),
  }) {
    if (noWBSFound != null) {
      return noWBSFound(this);
    }
    return orElse();
  }
}

abstract class _NoWBSFound implements AppLinkerException {
  const factory _NoWBSFound() = _$_NoWBSFound;
}

/// @nodoc
abstract class _$$_NotLicensedCopyWith<$Res> {
  factory _$$_NotLicensedCopyWith(
          _$_NotLicensed value, $Res Function(_$_NotLicensed) then) =
      __$$_NotLicensedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NotLicensedCopyWithImpl<$Res>
    extends _$AppLinkerExceptionCopyWithImpl<$Res, _$_NotLicensed>
    implements _$$_NotLicensedCopyWith<$Res> {
  __$$_NotLicensedCopyWithImpl(
      _$_NotLicensed _value, $Res Function(_$_NotLicensed) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NotLicensed implements _NotLicensed {
  const _$_NotLicensed();

  @override
  String toString() {
    return 'AppLinkerException.notLicensed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NotLicensed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noWorkPackageFound,
    required TResult Function() noActivitiesFound,
    required TResult Function() noWBSFound,
    required TResult Function() notLicensed,
  }) {
    return notLicensed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noWorkPackageFound,
    TResult? Function()? noActivitiesFound,
    TResult? Function()? noWBSFound,
    TResult? Function()? notLicensed,
  }) {
    return notLicensed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noWorkPackageFound,
    TResult Function()? noActivitiesFound,
    TResult Function()? noWBSFound,
    TResult Function()? notLicensed,
    required TResult orElse(),
  }) {
    if (notLicensed != null) {
      return notLicensed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoWorkPackageFound value) noWorkPackageFound,
    required TResult Function(_NoActivitiesFound value) noActivitiesFound,
    required TResult Function(_NoWBSFound value) noWBSFound,
    required TResult Function(_NotLicensed value) notLicensed,
  }) {
    return notLicensed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult? Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult? Function(_NoWBSFound value)? noWBSFound,
    TResult? Function(_NotLicensed value)? notLicensed,
  }) {
    return notLicensed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoWorkPackageFound value)? noWorkPackageFound,
    TResult Function(_NoActivitiesFound value)? noActivitiesFound,
    TResult Function(_NoWBSFound value)? noWBSFound,
    TResult Function(_NotLicensed value)? notLicensed,
    required TResult orElse(),
  }) {
    if (notLicensed != null) {
      return notLicensed(this);
    }
    return orElse();
  }
}

abstract class _NotLicensed implements AppLinkerException {
  const factory _NotLicensed() = _$_NotLicensed;
}
