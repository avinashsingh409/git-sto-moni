import 'package:freezed_annotation/freezed_annotation.dart';

part 'turnaround_data_exception.freezed.dart';

@freezed
abstract class TurnAroundDataException with _$TurnAroundDataException {
  const factory TurnAroundDataException.unauthorized() = _Unauthorized;
  const factory TurnAroundDataException.unexpectedError() = _UnexpectedError;
  const factory TurnAroundDataException.connectTimeOut() = _ConnectTimeOut;
  const factory TurnAroundDataException.socketException() = _SocketException;
  const factory TurnAroundDataException.noP6ProjectAssigned() = _NoP6ProjectAssigned;
  const factory TurnAroundDataException.plannerDataFailure() = _PlannerDataFailure;
  const factory TurnAroundDataException.executionDataFailure() = _ExecutionDataFailure;

  static String getErrorMessage(TurnAroundDataException exceptions) {
    return exceptions.when(
      unauthorized: () {
        return "Unauthorized Access";
      },
      connectTimeOut: () => "Unstable Internet Connection ",
      socketException: () => "No Internet Connection ",
      unexpectedError: () => "Unexpected Error",
      plannerDataFailure: () => "Planner Data Failure, Please submit the error in the logs",
      executionDataFailure: () => "Execution Data Failure, Please submit the error in the logs",
      noP6ProjectAssigned: () =>
          "The selected project does not have a P6/MSP Project Assigned. Please Assign the Project in STO Admin, and Resync.",
    );
  }
}
