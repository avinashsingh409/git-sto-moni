// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'workpackage_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$WorkpackagesException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkpackagesExceptionCopyWith<$Res> {
  factory $WorkpackagesExceptionCopyWith(WorkpackagesException value,
          $Res Function(WorkpackagesException) then) =
      _$WorkpackagesExceptionCopyWithImpl<$Res, WorkpackagesException>;
}

/// @nodoc
class _$WorkpackagesExceptionCopyWithImpl<$Res,
        $Val extends WorkpackagesException>
    implements $WorkpackagesExceptionCopyWith<$Res> {
  _$WorkpackagesExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_UnauthorizedCopyWith<$Res> {
  factory _$$_UnauthorizedCopyWith(
          _$_Unauthorized value, $Res Function(_$_Unauthorized) then) =
      __$$_UnauthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnauthorizedCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_Unauthorized>
    implements _$$_UnauthorizedCopyWith<$Res> {
  __$$_UnauthorizedCopyWithImpl(
      _$_Unauthorized _value, $Res Function(_$_Unauthorized) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Unauthorized implements _Unauthorized {
  const _$_Unauthorized();

  @override
  String toString() {
    return 'WorkpackagesException.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Unauthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return unauthorized?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return unauthorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class _Unauthorized implements WorkpackagesException {
  const factory _Unauthorized() = _$_Unauthorized;
}

/// @nodoc
abstract class _$$_UnexpectedErrorCopyWith<$Res> {
  factory _$$_UnexpectedErrorCopyWith(
          _$_UnexpectedError value, $Res Function(_$_UnexpectedError) then) =
      __$$_UnexpectedErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnexpectedErrorCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_UnexpectedError>
    implements _$$_UnexpectedErrorCopyWith<$Res> {
  __$$_UnexpectedErrorCopyWithImpl(
      _$_UnexpectedError _value, $Res Function(_$_UnexpectedError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UnexpectedError implements _UnexpectedError {
  const _$_UnexpectedError();

  @override
  String toString() {
    return 'WorkpackagesException.unexpectedError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UnexpectedError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return unexpectedError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return unexpectedError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return unexpectedError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return unexpectedError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(this);
    }
    return orElse();
  }
}

abstract class _UnexpectedError implements WorkpackagesException {
  const factory _UnexpectedError() = _$_UnexpectedError;
}

/// @nodoc
abstract class _$$_ConnectTimeOutCopyWith<$Res> {
  factory _$$_ConnectTimeOutCopyWith(
          _$_ConnectTimeOut value, $Res Function(_$_ConnectTimeOut) then) =
      __$$_ConnectTimeOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ConnectTimeOutCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_ConnectTimeOut>
    implements _$$_ConnectTimeOutCopyWith<$Res> {
  __$$_ConnectTimeOutCopyWithImpl(
      _$_ConnectTimeOut _value, $Res Function(_$_ConnectTimeOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ConnectTimeOut implements _ConnectTimeOut {
  const _$_ConnectTimeOut();

  @override
  String toString() {
    return 'WorkpackagesException.connectTimeOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ConnectTimeOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return connectTimeOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return connectTimeOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return connectTimeOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return connectTimeOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut(this);
    }
    return orElse();
  }
}

abstract class _ConnectTimeOut implements WorkpackagesException {
  const factory _ConnectTimeOut() = _$_ConnectTimeOut;
}

/// @nodoc
abstract class _$$_SocketExceptionCopyWith<$Res> {
  factory _$$_SocketExceptionCopyWith(
          _$_SocketException value, $Res Function(_$_SocketException) then) =
      __$$_SocketExceptionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SocketExceptionCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_SocketException>
    implements _$$_SocketExceptionCopyWith<$Res> {
  __$$_SocketExceptionCopyWithImpl(
      _$_SocketException _value, $Res Function(_$_SocketException) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SocketException implements _SocketException {
  const _$_SocketException();

  @override
  String toString() {
    return 'WorkpackagesException.socketException()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SocketException);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return socketException();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return socketException?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return socketException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return socketException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException(this);
    }
    return orElse();
  }
}

abstract class _SocketException implements WorkpackagesException {
  const factory _SocketException() = _$_SocketException;
}

/// @nodoc
abstract class _$$_NoActivityOnlineCopyWith<$Res> {
  factory _$$_NoActivityOnlineCopyWith(
          _$_NoActivityOnline value, $Res Function(_$_NoActivityOnline) then) =
      __$$_NoActivityOnlineCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoActivityOnlineCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_NoActivityOnline>
    implements _$$_NoActivityOnlineCopyWith<$Res> {
  __$$_NoActivityOnlineCopyWithImpl(
      _$_NoActivityOnline _value, $Res Function(_$_NoActivityOnline) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoActivityOnline implements _NoActivityOnline {
  const _$_NoActivityOnline();

  @override
  String toString() {
    return 'WorkpackagesException.noActivityOnline()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoActivityOnline);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return noActivityOnline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return noActivityOnline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (noActivityOnline != null) {
      return noActivityOnline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return noActivityOnline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return noActivityOnline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (noActivityOnline != null) {
      return noActivityOnline(this);
    }
    return orElse();
  }
}

abstract class _NoActivityOnline implements WorkpackagesException {
  const factory _NoActivityOnline() = _$_NoActivityOnline;
}

/// @nodoc
abstract class _$$_NoCachedWorkpackagesCopyWith<$Res> {
  factory _$$_NoCachedWorkpackagesCopyWith(_$_NoCachedWorkpackages value,
          $Res Function(_$_NoCachedWorkpackages) then) =
      __$$_NoCachedWorkpackagesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoCachedWorkpackagesCopyWithImpl<$Res>
    extends _$WorkpackagesExceptionCopyWithImpl<$Res, _$_NoCachedWorkpackages>
    implements _$$_NoCachedWorkpackagesCopyWith<$Res> {
  __$$_NoCachedWorkpackagesCopyWithImpl(_$_NoCachedWorkpackages _value,
      $Res Function(_$_NoCachedWorkpackages) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoCachedWorkpackages implements _NoCachedWorkpackages {
  const _$_NoCachedWorkpackages();

  @override
  String toString() {
    return 'WorkpackagesException.noCachedWorkpackages()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoCachedWorkpackages);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noActivityOnline,
    required TResult Function() noCachedWorkpackages,
  }) {
    return noCachedWorkpackages();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noActivityOnline,
    TResult? Function()? noCachedWorkpackages,
  }) {
    return noCachedWorkpackages?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noActivityOnline,
    TResult Function()? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (noCachedWorkpackages != null) {
      return noCachedWorkpackages();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoActivityOnline value) noActivityOnline,
    required TResult Function(_NoCachedWorkpackages value) noCachedWorkpackages,
  }) {
    return noCachedWorkpackages(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoActivityOnline value)? noActivityOnline,
    TResult? Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
  }) {
    return noCachedWorkpackages?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoActivityOnline value)? noActivityOnline,
    TResult Function(_NoCachedWorkpackages value)? noCachedWorkpackages,
    required TResult orElse(),
  }) {
    if (noCachedWorkpackages != null) {
      return noCachedWorkpackages(this);
    }
    return orElse();
  }
}

abstract class _NoCachedWorkpackages implements WorkpackagesException {
  const factory _NoCachedWorkpackages() = _$_NoCachedWorkpackages;
}
