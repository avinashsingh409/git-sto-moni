import 'package:freezed_annotation/freezed_annotation.dart';
part 'user_call_exception.freezed.dart';

@freezed
abstract class UserCallException with _$UserCallException {
  const factory UserCallException.noDataFound() = _NoDataFound;
  const factory UserCallException.unexpectedError() = _UnexpectedError;
  const factory UserCallException.socketException() = _SocketException;
  const factory UserCallException.connectTimeOut() = _ConnectTimeOut;

  static String getErrorMessage(UserCallException exceptions) {
    return exceptions.when(
        noDataFound: () => 'No data',
        unexpectedError: () => 'Unexpected Exception',
        socketException: () => 'Socket Exception',
        connectTimeOut: () => 'ConnectionTimeout');
  }
}
