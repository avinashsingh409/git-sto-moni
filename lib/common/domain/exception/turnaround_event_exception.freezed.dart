// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_event_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnAroundEventException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnAroundEventExceptionCopyWith<$Res> {
  factory $TurnAroundEventExceptionCopyWith(TurnAroundEventException value,
          $Res Function(TurnAroundEventException) then) =
      _$TurnAroundEventExceptionCopyWithImpl<$Res, TurnAroundEventException>;
}

/// @nodoc
class _$TurnAroundEventExceptionCopyWithImpl<$Res,
        $Val extends TurnAroundEventException>
    implements $TurnAroundEventExceptionCopyWith<$Res> {
  _$TurnAroundEventExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_UnauthorizedCopyWith<$Res> {
  factory _$$_UnauthorizedCopyWith(
          _$_Unauthorized value, $Res Function(_$_Unauthorized) then) =
      __$$_UnauthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnauthorizedCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_Unauthorized>
    implements _$$_UnauthorizedCopyWith<$Res> {
  __$$_UnauthorizedCopyWithImpl(
      _$_Unauthorized _value, $Res Function(_$_Unauthorized) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Unauthorized implements _Unauthorized {
  const _$_Unauthorized();

  @override
  String toString() {
    return 'TurnAroundEventException.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Unauthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return unauthorized?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return unauthorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class _Unauthorized implements TurnAroundEventException {
  const factory _Unauthorized() = _$_Unauthorized;
}

/// @nodoc
abstract class _$$_UnexpectedErrorCopyWith<$Res> {
  factory _$$_UnexpectedErrorCopyWith(
          _$_UnexpectedError value, $Res Function(_$_UnexpectedError) then) =
      __$$_UnexpectedErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnexpectedErrorCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_UnexpectedError>
    implements _$$_UnexpectedErrorCopyWith<$Res> {
  __$$_UnexpectedErrorCopyWithImpl(
      _$_UnexpectedError _value, $Res Function(_$_UnexpectedError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UnexpectedError implements _UnexpectedError {
  const _$_UnexpectedError();

  @override
  String toString() {
    return 'TurnAroundEventException.unexpectedError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UnexpectedError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return unexpectedError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return unexpectedError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return unexpectedError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return unexpectedError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(this);
    }
    return orElse();
  }
}

abstract class _UnexpectedError implements TurnAroundEventException {
  const factory _UnexpectedError() = _$_UnexpectedError;
}

/// @nodoc
abstract class _$$_ConnectTimeOutCopyWith<$Res> {
  factory _$$_ConnectTimeOutCopyWith(
          _$_ConnectTimeOut value, $Res Function(_$_ConnectTimeOut) then) =
      __$$_ConnectTimeOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ConnectTimeOutCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_ConnectTimeOut>
    implements _$$_ConnectTimeOutCopyWith<$Res> {
  __$$_ConnectTimeOutCopyWithImpl(
      _$_ConnectTimeOut _value, $Res Function(_$_ConnectTimeOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ConnectTimeOut implements _ConnectTimeOut {
  const _$_ConnectTimeOut();

  @override
  String toString() {
    return 'TurnAroundEventException.connectTimeOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ConnectTimeOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return connectTimeOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return connectTimeOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return connectTimeOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return connectTimeOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut(this);
    }
    return orElse();
  }
}

abstract class _ConnectTimeOut implements TurnAroundEventException {
  const factory _ConnectTimeOut() = _$_ConnectTimeOut;
}

/// @nodoc
abstract class _$$_SocketExceptionCopyWith<$Res> {
  factory _$$_SocketExceptionCopyWith(
          _$_SocketException value, $Res Function(_$_SocketException) then) =
      __$$_SocketExceptionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SocketExceptionCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_SocketException>
    implements _$$_SocketExceptionCopyWith<$Res> {
  __$$_SocketExceptionCopyWithImpl(
      _$_SocketException _value, $Res Function(_$_SocketException) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SocketException implements _SocketException {
  const _$_SocketException();

  @override
  String toString() {
    return 'TurnAroundEventException.socketException()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SocketException);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return socketException();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return socketException?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return socketException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return socketException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException(this);
    }
    return orElse();
  }
}

abstract class _SocketException implements TurnAroundEventException {
  const factory _SocketException() = _$_SocketException;
}

/// @nodoc
abstract class _$$_NoEventsOnlineCopyWith<$Res> {
  factory _$$_NoEventsOnlineCopyWith(
          _$_NoEventsOnline value, $Res Function(_$_NoEventsOnline) then) =
      __$$_NoEventsOnlineCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoEventsOnlineCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_NoEventsOnline>
    implements _$$_NoEventsOnlineCopyWith<$Res> {
  __$$_NoEventsOnlineCopyWithImpl(
      _$_NoEventsOnline _value, $Res Function(_$_NoEventsOnline) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoEventsOnline implements _NoEventsOnline {
  const _$_NoEventsOnline();

  @override
  String toString() {
    return 'TurnAroundEventException.noEventsOnline()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoEventsOnline);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return noEventsOnline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return noEventsOnline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (noEventsOnline != null) {
      return noEventsOnline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return noEventsOnline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return noEventsOnline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (noEventsOnline != null) {
      return noEventsOnline(this);
    }
    return orElse();
  }
}

abstract class _NoEventsOnline implements TurnAroundEventException {
  const factory _NoEventsOnline() = _$_NoEventsOnline;
}

/// @nodoc
abstract class _$$_NoCachedEventsCopyWith<$Res> {
  factory _$$_NoCachedEventsCopyWith(
          _$_NoCachedEvents value, $Res Function(_$_NoCachedEvents) then) =
      __$$_NoCachedEventsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoCachedEventsCopyWithImpl<$Res>
    extends _$TurnAroundEventExceptionCopyWithImpl<$Res, _$_NoCachedEvents>
    implements _$$_NoCachedEventsCopyWith<$Res> {
  __$$_NoCachedEventsCopyWithImpl(
      _$_NoCachedEvents _value, $Res Function(_$_NoCachedEvents) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoCachedEvents implements _NoCachedEvents {
  const _$_NoCachedEvents();

  @override
  String toString() {
    return 'TurnAroundEventException.noCachedEvents()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoCachedEvents);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noEventsOnline,
    required TResult Function() noCachedEvents,
  }) {
    return noCachedEvents();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noEventsOnline,
    TResult? Function()? noCachedEvents,
  }) {
    return noCachedEvents?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noEventsOnline,
    TResult Function()? noCachedEvents,
    required TResult orElse(),
  }) {
    if (noCachedEvents != null) {
      return noCachedEvents();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoEventsOnline value) noEventsOnline,
    required TResult Function(_NoCachedEvents value) noCachedEvents,
  }) {
    return noCachedEvents(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoEventsOnline value)? noEventsOnline,
    TResult? Function(_NoCachedEvents value)? noCachedEvents,
  }) {
    return noCachedEvents?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoEventsOnline value)? noEventsOnline,
    TResult Function(_NoCachedEvents value)? noCachedEvents,
    required TResult orElse(),
  }) {
    if (noCachedEvents != null) {
      return noCachedEvents(this);
    }
    return orElse();
  }
}

abstract class _NoCachedEvents implements TurnAroundEventException {
  const factory _NoCachedEvents() = _$_NoCachedEvents;
}
