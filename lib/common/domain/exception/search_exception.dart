import 'package:freezed_annotation/freezed_annotation.dart';
part 'search_exception.freezed.dart';

@freezed
abstract class SearchException with _$SearchException {
  const factory SearchException.noDataFound() = _NoDataFound;
  const factory SearchException.unexpectedError() = _UnexpectedError;

  static String getErrorMessage(SearchException exceptions) {
    return exceptions.when(
      noDataFound: () {
        return "No Activities Found";
      },
      unexpectedError: () {
        return "Unexpected Error";
      },
    );
  }
}
