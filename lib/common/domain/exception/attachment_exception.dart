import 'package:freezed_annotation/freezed_annotation.dart';
part 'attachment_exception.freezed.dart';

@freezed
abstract class AttachmentException with _$AttachmentException {
  const factory AttachmentException.unauthorized() = _Unauthorized;
  const factory AttachmentException.unexpectedError() = _UnexpectedError;
  const factory AttachmentException.connectTimeOut() = _ConnectTimeOut;
  const factory AttachmentException.socketException() = _SocketException;
  const factory AttachmentException.noDataFound() = _NoDataFound;

  // static String getErrorMessage(AttachmentException exceptions) {
  //   return exceptions.when(
  //     noDataFound: () {
  //       return "No Activities Found";
  //     },
  //     unexpectedError: () {
  //       return "Unexpected Error";
  //     },
  //   );
  // }
}
