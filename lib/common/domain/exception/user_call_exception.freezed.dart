// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_call_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserCallException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noDataFound,
    required TResult Function() unexpectedError,
    required TResult Function() socketException,
    required TResult Function() connectTimeOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noDataFound,
    TResult? Function()? unexpectedError,
    TResult? Function()? socketException,
    TResult? Function()? connectTimeOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noDataFound,
    TResult Function()? unexpectedError,
    TResult Function()? socketException,
    TResult Function()? connectTimeOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoDataFound value) noDataFound,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoDataFound value)? noDataFound,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoDataFound value)? noDataFound,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCallExceptionCopyWith<$Res> {
  factory $UserCallExceptionCopyWith(
          UserCallException value, $Res Function(UserCallException) then) =
      _$UserCallExceptionCopyWithImpl<$Res, UserCallException>;
}

/// @nodoc
class _$UserCallExceptionCopyWithImpl<$Res, $Val extends UserCallException>
    implements $UserCallExceptionCopyWith<$Res> {
  _$UserCallExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_NoDataFoundCopyWith<$Res> {
  factory _$$_NoDataFoundCopyWith(
          _$_NoDataFound value, $Res Function(_$_NoDataFound) then) =
      __$$_NoDataFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoDataFoundCopyWithImpl<$Res>
    extends _$UserCallExceptionCopyWithImpl<$Res, _$_NoDataFound>
    implements _$$_NoDataFoundCopyWith<$Res> {
  __$$_NoDataFoundCopyWithImpl(
      _$_NoDataFound _value, $Res Function(_$_NoDataFound) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoDataFound implements _NoDataFound {
  const _$_NoDataFound();

  @override
  String toString() {
    return 'UserCallException.noDataFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoDataFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noDataFound,
    required TResult Function() unexpectedError,
    required TResult Function() socketException,
    required TResult Function() connectTimeOut,
  }) {
    return noDataFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noDataFound,
    TResult? Function()? unexpectedError,
    TResult? Function()? socketException,
    TResult? Function()? connectTimeOut,
  }) {
    return noDataFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noDataFound,
    TResult Function()? unexpectedError,
    TResult Function()? socketException,
    TResult Function()? connectTimeOut,
    required TResult orElse(),
  }) {
    if (noDataFound != null) {
      return noDataFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoDataFound value) noDataFound,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
  }) {
    return noDataFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoDataFound value)? noDataFound,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
  }) {
    return noDataFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoDataFound value)? noDataFound,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    required TResult orElse(),
  }) {
    if (noDataFound != null) {
      return noDataFound(this);
    }
    return orElse();
  }
}

abstract class _NoDataFound implements UserCallException {
  const factory _NoDataFound() = _$_NoDataFound;
}

/// @nodoc
abstract class _$$_UnexpectedErrorCopyWith<$Res> {
  factory _$$_UnexpectedErrorCopyWith(
          _$_UnexpectedError value, $Res Function(_$_UnexpectedError) then) =
      __$$_UnexpectedErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnexpectedErrorCopyWithImpl<$Res>
    extends _$UserCallExceptionCopyWithImpl<$Res, _$_UnexpectedError>
    implements _$$_UnexpectedErrorCopyWith<$Res> {
  __$$_UnexpectedErrorCopyWithImpl(
      _$_UnexpectedError _value, $Res Function(_$_UnexpectedError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UnexpectedError implements _UnexpectedError {
  const _$_UnexpectedError();

  @override
  String toString() {
    return 'UserCallException.unexpectedError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UnexpectedError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noDataFound,
    required TResult Function() unexpectedError,
    required TResult Function() socketException,
    required TResult Function() connectTimeOut,
  }) {
    return unexpectedError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noDataFound,
    TResult? Function()? unexpectedError,
    TResult? Function()? socketException,
    TResult? Function()? connectTimeOut,
  }) {
    return unexpectedError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noDataFound,
    TResult Function()? unexpectedError,
    TResult Function()? socketException,
    TResult Function()? connectTimeOut,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoDataFound value) noDataFound,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
  }) {
    return unexpectedError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoDataFound value)? noDataFound,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
  }) {
    return unexpectedError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoDataFound value)? noDataFound,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(this);
    }
    return orElse();
  }
}

abstract class _UnexpectedError implements UserCallException {
  const factory _UnexpectedError() = _$_UnexpectedError;
}

/// @nodoc
abstract class _$$_SocketExceptionCopyWith<$Res> {
  factory _$$_SocketExceptionCopyWith(
          _$_SocketException value, $Res Function(_$_SocketException) then) =
      __$$_SocketExceptionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SocketExceptionCopyWithImpl<$Res>
    extends _$UserCallExceptionCopyWithImpl<$Res, _$_SocketException>
    implements _$$_SocketExceptionCopyWith<$Res> {
  __$$_SocketExceptionCopyWithImpl(
      _$_SocketException _value, $Res Function(_$_SocketException) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SocketException implements _SocketException {
  const _$_SocketException();

  @override
  String toString() {
    return 'UserCallException.socketException()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SocketException);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noDataFound,
    required TResult Function() unexpectedError,
    required TResult Function() socketException,
    required TResult Function() connectTimeOut,
  }) {
    return socketException();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noDataFound,
    TResult? Function()? unexpectedError,
    TResult? Function()? socketException,
    TResult? Function()? connectTimeOut,
  }) {
    return socketException?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noDataFound,
    TResult Function()? unexpectedError,
    TResult Function()? socketException,
    TResult Function()? connectTimeOut,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoDataFound value) noDataFound,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
  }) {
    return socketException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoDataFound value)? noDataFound,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
  }) {
    return socketException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoDataFound value)? noDataFound,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException(this);
    }
    return orElse();
  }
}

abstract class _SocketException implements UserCallException {
  const factory _SocketException() = _$_SocketException;
}

/// @nodoc
abstract class _$$_ConnectTimeOutCopyWith<$Res> {
  factory _$$_ConnectTimeOutCopyWith(
          _$_ConnectTimeOut value, $Res Function(_$_ConnectTimeOut) then) =
      __$$_ConnectTimeOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ConnectTimeOutCopyWithImpl<$Res>
    extends _$UserCallExceptionCopyWithImpl<$Res, _$_ConnectTimeOut>
    implements _$$_ConnectTimeOutCopyWith<$Res> {
  __$$_ConnectTimeOutCopyWithImpl(
      _$_ConnectTimeOut _value, $Res Function(_$_ConnectTimeOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ConnectTimeOut implements _ConnectTimeOut {
  const _$_ConnectTimeOut();

  @override
  String toString() {
    return 'UserCallException.connectTimeOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ConnectTimeOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noDataFound,
    required TResult Function() unexpectedError,
    required TResult Function() socketException,
    required TResult Function() connectTimeOut,
  }) {
    return connectTimeOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noDataFound,
    TResult? Function()? unexpectedError,
    TResult? Function()? socketException,
    TResult? Function()? connectTimeOut,
  }) {
    return connectTimeOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noDataFound,
    TResult Function()? unexpectedError,
    TResult Function()? socketException,
    TResult Function()? connectTimeOut,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NoDataFound value) noDataFound,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
  }) {
    return connectTimeOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NoDataFound value)? noDataFound,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
  }) {
    return connectTimeOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NoDataFound value)? noDataFound,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut(this);
    }
    return orElse();
  }
}

abstract class _ConnectTimeOut implements UserCallException {
  const factory _ConnectTimeOut() = _$_ConnectTimeOut;
}
