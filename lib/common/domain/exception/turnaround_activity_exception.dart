import 'package:freezed_annotation/freezed_annotation.dart';
part 'turnaround_activity_exception.freezed.dart';

@freezed
abstract class TurnAroundActivityException with _$TurnAroundActivityException {
  const factory TurnAroundActivityException.unauthorized() = _Unauthorized;
  const factory TurnAroundActivityException.unexpectedError() = _UnexpectedError;
  const factory TurnAroundActivityException.connectTimeOut() = _ConnectTimeOut;
  const factory TurnAroundActivityException.socketException() = _SocketException;
  const factory TurnAroundActivityException.noActivityOnline() = _NoActivityOnline;
  const factory TurnAroundActivityException.noCachedActivities() = _NoCachedActivities;
  const factory TurnAroundActivityException.noP6ProjectAssigned() = _NoP6ProjectAssigned;

  static String getErrorMessage(TurnAroundActivityException exceptions) {
    return exceptions.when(
        unauthorized: () {
          return "Unauthorized Access";
        },
        connectTimeOut: () => "Unstable Internet Connection ",
        socketException: () => "No Internet Connection ",
        unexpectedError: () {
          return "Unexpected Error";
        },
        noActivityOnline: () {
          return "Couldn't find any Activities Online ";
        },
        noCachedActivities: () {
          return "No Cached Activities";
        },
        noP6ProjectAssigned: () {
          return "No P6 Project Assigned";
        });
  }
}
