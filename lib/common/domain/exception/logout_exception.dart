import 'package:freezed_annotation/freezed_annotation.dart';
part 'logout_exception.freezed.dart';

@freezed
abstract class LogoutException with _$LogoutException {
  const factory LogoutException.socketException() = _SocketException;
  const factory LogoutException.connectTimeOut() = _ConnectTimeOut;
  const factory LogoutException.unexpectedError() = _UnexpectedError;

  static String getErrorMessage(LogoutException exceptions) {
    return exceptions.when(socketException: () {
      return "No Internet Connection";
    }, connectTimeOut: () {
      return "Unstable Internet Connection";
    }, unexpectedError: () {
      return "Unexpected Error";
    });
  }
}
