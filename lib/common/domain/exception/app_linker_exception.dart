import 'package:freezed_annotation/freezed_annotation.dart';
part 'app_linker_exception.freezed.dart';

@freezed
abstract class AppLinkerException with _$AppLinkerException {
  const factory AppLinkerException.noWorkPackageFound() = _NoWorkPackageFound;
  const factory AppLinkerException.noActivitiesFound() = _NoActivitiesFound;
  const factory AppLinkerException.noWBSFound() = _NoWBSFound;
  const factory AppLinkerException.notLicensed() = _NotLicensed;

  static String getErrorTitle(AppLinkerException exception) {
    return exception.when(
      noWorkPackageFound: () => "Error Occurred",
      noActivitiesFound: () => "Work Package Empty",
      noWBSFound: () => "Work Package Empty",
      notLicensed: () => "Error Occurred",
    );
  }

  static String getErrorMessage(AppLinkerException exception) {
    return exception.when(
      noWorkPackageFound: () => "No Associated Work Package Found",
      noActivitiesFound: () => "No Associated Activities Found",
      noWBSFound: () => "No Associated WBS Found",
      notLicensed: () => "Access Denied: License Not Found",
    );
  }
}
