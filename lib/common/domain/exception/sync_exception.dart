import 'package:freezed_annotation/freezed_annotation.dart';
part 'sync_exception.freezed.dart';

@freezed
abstract class SyncException with _$SyncException {
  const factory SyncException.socketException() = _SocketException;
  const factory SyncException.connectTimeOut() = _ConnectTimeOut;
  const factory SyncException.unexpectedError() = _UnexpectedError;

  static String getErrorMessage(SyncException exceptions) {
    return exceptions.when(socketException: () {
      return "No Internet Connection";
    }, connectTimeOut: () {
      return "Unstable Internet Connection";
    }, unexpectedError: () {
      return "Unexpected Error";
    });
  }
}
