// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_data_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnAroundDataException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnAroundDataExceptionCopyWith<$Res> {
  factory $TurnAroundDataExceptionCopyWith(TurnAroundDataException value,
          $Res Function(TurnAroundDataException) then) =
      _$TurnAroundDataExceptionCopyWithImpl<$Res, TurnAroundDataException>;
}

/// @nodoc
class _$TurnAroundDataExceptionCopyWithImpl<$Res,
        $Val extends TurnAroundDataException>
    implements $TurnAroundDataExceptionCopyWith<$Res> {
  _$TurnAroundDataExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_UnauthorizedCopyWith<$Res> {
  factory _$$_UnauthorizedCopyWith(
          _$_Unauthorized value, $Res Function(_$_Unauthorized) then) =
      __$$_UnauthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnauthorizedCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_Unauthorized>
    implements _$$_UnauthorizedCopyWith<$Res> {
  __$$_UnauthorizedCopyWithImpl(
      _$_Unauthorized _value, $Res Function(_$_Unauthorized) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Unauthorized implements _Unauthorized {
  const _$_Unauthorized();

  @override
  String toString() {
    return 'TurnAroundDataException.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Unauthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return unauthorized?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return unauthorized?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class _Unauthorized implements TurnAroundDataException {
  const factory _Unauthorized() = _$_Unauthorized;
}

/// @nodoc
abstract class _$$_UnexpectedErrorCopyWith<$Res> {
  factory _$$_UnexpectedErrorCopyWith(
          _$_UnexpectedError value, $Res Function(_$_UnexpectedError) then) =
      __$$_UnexpectedErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnexpectedErrorCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_UnexpectedError>
    implements _$$_UnexpectedErrorCopyWith<$Res> {
  __$$_UnexpectedErrorCopyWithImpl(
      _$_UnexpectedError _value, $Res Function(_$_UnexpectedError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UnexpectedError implements _UnexpectedError {
  const _$_UnexpectedError();

  @override
  String toString() {
    return 'TurnAroundDataException.unexpectedError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UnexpectedError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return unexpectedError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return unexpectedError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return unexpectedError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return unexpectedError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(this);
    }
    return orElse();
  }
}

abstract class _UnexpectedError implements TurnAroundDataException {
  const factory _UnexpectedError() = _$_UnexpectedError;
}

/// @nodoc
abstract class _$$_ConnectTimeOutCopyWith<$Res> {
  factory _$$_ConnectTimeOutCopyWith(
          _$_ConnectTimeOut value, $Res Function(_$_ConnectTimeOut) then) =
      __$$_ConnectTimeOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ConnectTimeOutCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_ConnectTimeOut>
    implements _$$_ConnectTimeOutCopyWith<$Res> {
  __$$_ConnectTimeOutCopyWithImpl(
      _$_ConnectTimeOut _value, $Res Function(_$_ConnectTimeOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ConnectTimeOut implements _ConnectTimeOut {
  const _$_ConnectTimeOut();

  @override
  String toString() {
    return 'TurnAroundDataException.connectTimeOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ConnectTimeOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return connectTimeOut();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return connectTimeOut?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return connectTimeOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return connectTimeOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (connectTimeOut != null) {
      return connectTimeOut(this);
    }
    return orElse();
  }
}

abstract class _ConnectTimeOut implements TurnAroundDataException {
  const factory _ConnectTimeOut() = _$_ConnectTimeOut;
}

/// @nodoc
abstract class _$$_SocketExceptionCopyWith<$Res> {
  factory _$$_SocketExceptionCopyWith(
          _$_SocketException value, $Res Function(_$_SocketException) then) =
      __$$_SocketExceptionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SocketExceptionCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_SocketException>
    implements _$$_SocketExceptionCopyWith<$Res> {
  __$$_SocketExceptionCopyWithImpl(
      _$_SocketException _value, $Res Function(_$_SocketException) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SocketException implements _SocketException {
  const _$_SocketException();

  @override
  String toString() {
    return 'TurnAroundDataException.socketException()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SocketException);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return socketException();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return socketException?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return socketException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return socketException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (socketException != null) {
      return socketException(this);
    }
    return orElse();
  }
}

abstract class _SocketException implements TurnAroundDataException {
  const factory _SocketException() = _$_SocketException;
}

/// @nodoc
abstract class _$$_NoP6ProjectAssignedCopyWith<$Res> {
  factory _$$_NoP6ProjectAssignedCopyWith(_$_NoP6ProjectAssigned value,
          $Res Function(_$_NoP6ProjectAssigned) then) =
      __$$_NoP6ProjectAssignedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_NoP6ProjectAssignedCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_NoP6ProjectAssigned>
    implements _$$_NoP6ProjectAssignedCopyWith<$Res> {
  __$$_NoP6ProjectAssignedCopyWithImpl(_$_NoP6ProjectAssigned _value,
      $Res Function(_$_NoP6ProjectAssigned) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_NoP6ProjectAssigned implements _NoP6ProjectAssigned {
  const _$_NoP6ProjectAssigned();

  @override
  String toString() {
    return 'TurnAroundDataException.noP6ProjectAssigned()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_NoP6ProjectAssigned);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return noP6ProjectAssigned();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return noP6ProjectAssigned?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (noP6ProjectAssigned != null) {
      return noP6ProjectAssigned();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return noP6ProjectAssigned(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return noP6ProjectAssigned?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (noP6ProjectAssigned != null) {
      return noP6ProjectAssigned(this);
    }
    return orElse();
  }
}

abstract class _NoP6ProjectAssigned implements TurnAroundDataException {
  const factory _NoP6ProjectAssigned() = _$_NoP6ProjectAssigned;
}

/// @nodoc
abstract class _$$_PlannerDataFailureCopyWith<$Res> {
  factory _$$_PlannerDataFailureCopyWith(_$_PlannerDataFailure value,
          $Res Function(_$_PlannerDataFailure) then) =
      __$$_PlannerDataFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_PlannerDataFailureCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_PlannerDataFailure>
    implements _$$_PlannerDataFailureCopyWith<$Res> {
  __$$_PlannerDataFailureCopyWithImpl(
      _$_PlannerDataFailure _value, $Res Function(_$_PlannerDataFailure) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_PlannerDataFailure implements _PlannerDataFailure {
  const _$_PlannerDataFailure();

  @override
  String toString() {
    return 'TurnAroundDataException.plannerDataFailure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_PlannerDataFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return plannerDataFailure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return plannerDataFailure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (plannerDataFailure != null) {
      return plannerDataFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return plannerDataFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return plannerDataFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (plannerDataFailure != null) {
      return plannerDataFailure(this);
    }
    return orElse();
  }
}

abstract class _PlannerDataFailure implements TurnAroundDataException {
  const factory _PlannerDataFailure() = _$_PlannerDataFailure;
}

/// @nodoc
abstract class _$$_ExecutionDataFailureCopyWith<$Res> {
  factory _$$_ExecutionDataFailureCopyWith(_$_ExecutionDataFailure value,
          $Res Function(_$_ExecutionDataFailure) then) =
      __$$_ExecutionDataFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExecutionDataFailureCopyWithImpl<$Res>
    extends _$TurnAroundDataExceptionCopyWithImpl<$Res, _$_ExecutionDataFailure>
    implements _$$_ExecutionDataFailureCopyWith<$Res> {
  __$$_ExecutionDataFailureCopyWithImpl(_$_ExecutionDataFailure _value,
      $Res Function(_$_ExecutionDataFailure) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ExecutionDataFailure implements _ExecutionDataFailure {
  const _$_ExecutionDataFailure();

  @override
  String toString() {
    return 'TurnAroundDataException.executionDataFailure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ExecutionDataFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthorized,
    required TResult Function() unexpectedError,
    required TResult Function() connectTimeOut,
    required TResult Function() socketException,
    required TResult Function() noP6ProjectAssigned,
    required TResult Function() plannerDataFailure,
    required TResult Function() executionDataFailure,
  }) {
    return executionDataFailure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? unauthorized,
    TResult? Function()? unexpectedError,
    TResult? Function()? connectTimeOut,
    TResult? Function()? socketException,
    TResult? Function()? noP6ProjectAssigned,
    TResult? Function()? plannerDataFailure,
    TResult? Function()? executionDataFailure,
  }) {
    return executionDataFailure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthorized,
    TResult Function()? unexpectedError,
    TResult Function()? connectTimeOut,
    TResult Function()? socketException,
    TResult Function()? noP6ProjectAssigned,
    TResult Function()? plannerDataFailure,
    TResult Function()? executionDataFailure,
    required TResult orElse(),
  }) {
    if (executionDataFailure != null) {
      return executionDataFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unauthorized value) unauthorized,
    required TResult Function(_UnexpectedError value) unexpectedError,
    required TResult Function(_ConnectTimeOut value) connectTimeOut,
    required TResult Function(_SocketException value) socketException,
    required TResult Function(_NoP6ProjectAssigned value) noP6ProjectAssigned,
    required TResult Function(_PlannerDataFailure value) plannerDataFailure,
    required TResult Function(_ExecutionDataFailure value) executionDataFailure,
  }) {
    return executionDataFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Unauthorized value)? unauthorized,
    TResult? Function(_UnexpectedError value)? unexpectedError,
    TResult? Function(_ConnectTimeOut value)? connectTimeOut,
    TResult? Function(_SocketException value)? socketException,
    TResult? Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult? Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult? Function(_ExecutionDataFailure value)? executionDataFailure,
  }) {
    return executionDataFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unauthorized value)? unauthorized,
    TResult Function(_UnexpectedError value)? unexpectedError,
    TResult Function(_ConnectTimeOut value)? connectTimeOut,
    TResult Function(_SocketException value)? socketException,
    TResult Function(_NoP6ProjectAssigned value)? noP6ProjectAssigned,
    TResult Function(_PlannerDataFailure value)? plannerDataFailure,
    TResult Function(_ExecutionDataFailure value)? executionDataFailure,
    required TResult orElse(),
  }) {
    if (executionDataFailure != null) {
      return executionDataFailure(this);
    }
    return orElse();
  }
}

abstract class _ExecutionDataFailure implements TurnAroundDataException {
  const factory _ExecutionDataFailure() = _$_ExecutionDataFailure;
}
