import 'package:freezed_annotation/freezed_annotation.dart';
part 'turnaround_event_exception.freezed.dart';

@freezed
abstract class TurnAroundEventException with _$TurnAroundEventException {
  const factory TurnAroundEventException.unauthorized() = _Unauthorized;
  const factory TurnAroundEventException.unexpectedError() = _UnexpectedError;
  const factory TurnAroundEventException.connectTimeOut() = _ConnectTimeOut;
  const factory TurnAroundEventException.socketException() = _SocketException;
  const factory TurnAroundEventException.noEventsOnline() = _NoEventsOnline;
  const factory TurnAroundEventException.noCachedEvents() = _NoCachedEvents;

  static String getErrorMessage(TurnAroundEventException exceptions) {
    return exceptions.when(
        unauthorized: () {
          return "Unauthorized Access";
        },
        unexpectedError: () {
          return "Unexpected Error";
        },
        connectTimeOut: () => "Unstable Internet Connection ",
        socketException: () => "No Internet Connection ",
        noEventsOnline: () {
          return "Couldn't find any Events Online ";
        },
        noCachedEvents: () {
          return "No Cached Events";
        });
  }
}
