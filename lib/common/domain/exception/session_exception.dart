import 'package:freezed_annotation/freezed_annotation.dart';
part 'session_exception.freezed.dart';

@freezed
abstract class SessionException with _$SessionException {
  const factory SessionException.unexpectedError() = _UnexpectedError;

  static String getErrorMessage(SessionException exceptions) {
    return exceptions.when(
      unexpectedError: () {
        return "Unexpected Error";
      },
    );
  }
}
