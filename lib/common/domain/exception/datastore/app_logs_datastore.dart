import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';

abstract class IAppLogsDataStore {
  Future<List<AppLogsDTO>> getAllAppLogs();
  Future<int> upsertAppLog({
    required AppLogsDTO appLog,
  });
  Future<void> deleteAppLogs();
}
