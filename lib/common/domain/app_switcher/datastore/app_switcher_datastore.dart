abstract class IAppSwitcherDataStore {
  String? getSelectedApp();
  Future<void> saveSelectedApp({required String selectedAppFeature});
}
