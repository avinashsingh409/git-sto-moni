import 'package:sto_mobile_v2/common/data/app_switcher/datastore/enums/app_features.dart';

abstract class IAppSwitcherRepository {
  AppFeatures getSelectedApp();
  Future<void> saveSelectedApp({required AppFeatures appFeatures});
}
