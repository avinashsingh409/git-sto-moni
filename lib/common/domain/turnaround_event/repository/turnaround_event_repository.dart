import 'package:dartz/dartz.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_response.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_event_exception.dart';

abstract class ITurnAroundEventRepository {
  Future<Either<TurnAroundEventResponse, TurnAroundEventException>> getTurnAroundEvents();
  Future<void> insertAllActivities({required List<TurnAroundEventDTO>? turnAroundEvents});
  Future<Either<List<TurnAroundEventDTO>, Unit>> getCachedTurnAroundEvents();
  Future<void> deleteTurnAroudEvents();
  Future<void> saveSelectedTurnAroundEventId({required int turnAroundId});
  Either<int, Unit> getSelectedTurnAroundEventId();
  Future<TurnAroundEventDTO?> getSelectedTurnAroundEvent({required int id});
  Future<void> saveBackwardsCommentEnabled({required bool isBackwardsCommentEnabled});
  Either<bool, Unit> getBackwardsCommentEnabled();
  Future<void> savePercentageRange({required String? percentageRange});
  Either<String, Unit> getPercentageRange();
  Future<void> saveTurnaroundEventSwipeEnabled({required bool turnaroundEventSwipeEnabled});
  Either<bool, Unit> getTurnaroundEventSwipeEnabled();
}
