import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_response.dart';

abstract class ITurnAroundEventDataStore {
  Future<TurnAroundEventResponse> getTurnAroundEvents();
  Future<void> insertAllEvents({required List<TurnAroundEventDTO> turnAroundDTOs});
  Future<List<TurnAroundEventDTO>> getCachedTurnAroundEvents();
  Future<void> deleteTurnAroudEvents();
  Future<TurnAroundEventDTO?> getSelectedTurnAroundEvent({required int id});
  String saveSelectedTurnAroundEventId({required String turnAroundId});
  String? getSelectedTurnAroundEventId();
  bool saveBackwardsCommentEnabled({required bool isBackwardsCommentEnabled});
  bool? getBackwardsCommentEnabled();
  String? savePercentageRange({required String? percentageRange});
  String? getPercentageRange();
  bool saveTurnaroundEventSwipeEnabled({required bool turnaroundEventSwipeEnabled});
  bool? getTurnaroundEventSwipeEnabled();
}
