part of 'report_problem_cubit.dart';

@freezed
class ReportProblemState with _$ReportProblemState {
  const factory ReportProblemState.initial() = _Initial;
  const factory ReportProblemState.panelOpened() = _PanelOpened;
}
