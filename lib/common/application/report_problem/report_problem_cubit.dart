import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sto_mobile_v2/common/domain/exception/repository/app_logs_repository.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

import 'package:sto_mobile_v2/injectable/injection.dart';

part 'report_problem_state.dart';
part 'report_problem_cubit.freezed.dart';

@singleton
class ReportProblemCubit extends Cubit<ReportProblemState> {
  ReportProblemCubit() : super(const ReportProblemState.initial());

  void popUpModal() {
    showModalBottomSheet(
        isDismissible: false,
        context: GlobalNavigationKey().context!,
        builder: (BuildContext context) {
          return SizedBox(
            height: MediaQuery.of(context).copyWith().size.height * 0.3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Report a technical problem!",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                const SizedBox(
                  height: 8,
                ),
                const Text(
                  "Your feedback helps us improve STO Mobile",
                  style: TextStyle(fontSize: 14),
                ),
                const SizedBox(
                  height: 24,
                ),
                const Divider(),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.6,
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () async {
                          final IAppLogsRepository appLogsRepository = getIt<IAppLogsRepository>();
                          try {
                            final directory =
                                Platform.isAndroid ? await getExternalStorageDirectory() : await getApplicationSupportDirectory();
                            if (directory != null) {
                              final path = directory.path;
                              final File file = File('$path/STO_mobile_logs${DateTime.now()}.txt');
                              final logs = await appLogsRepository.getAllAppLogs();
                              if (logs.isNotEmpty && logs != []) {
                                final logsJson = jsonEncode(logs);
                                await file.writeAsString(logsJson);

                                Share.shareFiles([file.path]);
                              } else {
                                if (context.mounted) {
                                  _showErrorToast(
                                    context: context,
                                    message: "No logs to export",
                                  );
                                }
                              }
                              if (context.mounted) Navigator.of(context).pop();
                            }
                          } on Exception catch (_) {
                            if (context.mounted) {
                              _showErrorToast(
                                context: context,
                                message: "Error fetching logs, Please contact Support",
                              );
                            }
                          }
                        },
                        child: const Text('Export'),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text('Cancel'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  void _showErrorToast({required BuildContext context, required String message}) {
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          backgroundColor: AppTheme.pgPrimaryRed,
          content: Center(
            child: Text(
              message,
            ),
          ),
          duration: const Duration(seconds: 4)));
  }
}
