import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_config_exception.dart';

import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

part 'server_credentials_event.dart';
part 'server_credentials_state.dart';
part 'server_credentials_bloc.freezed.dart';

@injectable
class ServerCredentialsBloc extends Bloc<ServerCredentialsEvent, ServerCredentialsState> {
  final IAppConfigRepository appConfigRepository;
  ServerCredentialsBloc(this.appConfigRepository) : super(const _Initial()) {
    on<ServerCredentialsEvent>((event, emit) async {
      await event.when(
        saveServerConfigs: (appConfig) async {
          await appConfigRepository.deleteConfigs();
          await appConfigRepository.insertAppConfig(appConfig: appConfig);
          emit(const ServerCredentialsState.cacheSuccess());
        },
        pasteServerConfigs: (appConfigString) async {
          emit(const ServerCredentialsState.initial());
          if (appConfigString != null && appConfigString.text != null) {
            await appConfigRepository.decodeAppConfig(appConfigString: appConfigString.text!).fold(
              (config) async {
                emit(ServerCredentialsState.parseSuccessful(appConfig: config));
              },
              (exception) {
                emit(
                  ServerCredentialsState.error(
                    errorMsg: AppConfigException.getErrorMessage(exception),
                  ),
                );
              },
            );
          } else {
            emit(
              ServerCredentialsState.error(
                errorMsg: AppConfigException.getErrorMessage(const AppConfigException.emptyClipBoard()),
              ),
            );
          }
        },
      );
    });
  }
}
