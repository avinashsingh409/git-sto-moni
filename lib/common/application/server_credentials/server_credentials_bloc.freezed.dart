// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'server_credentials_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ServerCredentialsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ClipboardData? value) pasteServerConfigs,
    required TResult Function(AppConfig appConfig) saveServerConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ClipboardData? value)? pasteServerConfigs,
    TResult? Function(AppConfig appConfig)? saveServerConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ClipboardData? value)? pasteServerConfigs,
    TResult Function(AppConfig appConfig)? saveServerConfigs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PasteServerConfigs value) pasteServerConfigs,
    required TResult Function(_SaveServerConfigs value) saveServerConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult? Function(_SaveServerConfigs value)? saveServerConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult Function(_SaveServerConfigs value)? saveServerConfigs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerCredentialsEventCopyWith<$Res> {
  factory $ServerCredentialsEventCopyWith(ServerCredentialsEvent value,
          $Res Function(ServerCredentialsEvent) then) =
      _$ServerCredentialsEventCopyWithImpl<$Res, ServerCredentialsEvent>;
}

/// @nodoc
class _$ServerCredentialsEventCopyWithImpl<$Res,
        $Val extends ServerCredentialsEvent>
    implements $ServerCredentialsEventCopyWith<$Res> {
  _$ServerCredentialsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_PasteServerConfigsCopyWith<$Res> {
  factory _$$_PasteServerConfigsCopyWith(_$_PasteServerConfigs value,
          $Res Function(_$_PasteServerConfigs) then) =
      __$$_PasteServerConfigsCopyWithImpl<$Res>;
  @useResult
  $Res call({ClipboardData? value});
}

/// @nodoc
class __$$_PasteServerConfigsCopyWithImpl<$Res>
    extends _$ServerCredentialsEventCopyWithImpl<$Res, _$_PasteServerConfigs>
    implements _$$_PasteServerConfigsCopyWith<$Res> {
  __$$_PasteServerConfigsCopyWithImpl(
      _$_PasteServerConfigs _value, $Res Function(_$_PasteServerConfigs) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_$_PasteServerConfigs(
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as ClipboardData?,
    ));
  }
}

/// @nodoc

class _$_PasteServerConfigs implements _PasteServerConfigs {
  const _$_PasteServerConfigs({required this.value});

  @override
  final ClipboardData? value;

  @override
  String toString() {
    return 'ServerCredentialsEvent.pasteServerConfigs(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PasteServerConfigs &&
            (identical(other.value, value) || other.value == value));
  }

  @override
  int get hashCode => Object.hash(runtimeType, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PasteServerConfigsCopyWith<_$_PasteServerConfigs> get copyWith =>
      __$$_PasteServerConfigsCopyWithImpl<_$_PasteServerConfigs>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ClipboardData? value) pasteServerConfigs,
    required TResult Function(AppConfig appConfig) saveServerConfigs,
  }) {
    return pasteServerConfigs(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ClipboardData? value)? pasteServerConfigs,
    TResult? Function(AppConfig appConfig)? saveServerConfigs,
  }) {
    return pasteServerConfigs?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ClipboardData? value)? pasteServerConfigs,
    TResult Function(AppConfig appConfig)? saveServerConfigs,
    required TResult orElse(),
  }) {
    if (pasteServerConfigs != null) {
      return pasteServerConfigs(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PasteServerConfigs value) pasteServerConfigs,
    required TResult Function(_SaveServerConfigs value) saveServerConfigs,
  }) {
    return pasteServerConfigs(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult? Function(_SaveServerConfigs value)? saveServerConfigs,
  }) {
    return pasteServerConfigs?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult Function(_SaveServerConfigs value)? saveServerConfigs,
    required TResult orElse(),
  }) {
    if (pasteServerConfigs != null) {
      return pasteServerConfigs(this);
    }
    return orElse();
  }
}

abstract class _PasteServerConfigs implements ServerCredentialsEvent {
  const factory _PasteServerConfigs({required final ClipboardData? value}) =
      _$_PasteServerConfigs;

  ClipboardData? get value;
  @JsonKey(ignore: true)
  _$$_PasteServerConfigsCopyWith<_$_PasteServerConfigs> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SaveServerConfigsCopyWith<$Res> {
  factory _$$_SaveServerConfigsCopyWith(_$_SaveServerConfigs value,
          $Res Function(_$_SaveServerConfigs) then) =
      __$$_SaveServerConfigsCopyWithImpl<$Res>;
  @useResult
  $Res call({AppConfig appConfig});

  $AppConfigCopyWith<$Res> get appConfig;
}

/// @nodoc
class __$$_SaveServerConfigsCopyWithImpl<$Res>
    extends _$ServerCredentialsEventCopyWithImpl<$Res, _$_SaveServerConfigs>
    implements _$$_SaveServerConfigsCopyWith<$Res> {
  __$$_SaveServerConfigsCopyWithImpl(
      _$_SaveServerConfigs _value, $Res Function(_$_SaveServerConfigs) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appConfig = null,
  }) {
    return _then(_$_SaveServerConfigs(
      appConfig: null == appConfig
          ? _value.appConfig
          : appConfig // ignore: cast_nullable_to_non_nullable
              as AppConfig,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $AppConfigCopyWith<$Res> get appConfig {
    return $AppConfigCopyWith<$Res>(_value.appConfig, (value) {
      return _then(_value.copyWith(appConfig: value));
    });
  }
}

/// @nodoc

class _$_SaveServerConfigs implements _SaveServerConfigs {
  const _$_SaveServerConfigs({required this.appConfig});

  @override
  final AppConfig appConfig;

  @override
  String toString() {
    return 'ServerCredentialsEvent.saveServerConfigs(appConfig: $appConfig)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SaveServerConfigs &&
            (identical(other.appConfig, appConfig) ||
                other.appConfig == appConfig));
  }

  @override
  int get hashCode => Object.hash(runtimeType, appConfig);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SaveServerConfigsCopyWith<_$_SaveServerConfigs> get copyWith =>
      __$$_SaveServerConfigsCopyWithImpl<_$_SaveServerConfigs>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ClipboardData? value) pasteServerConfigs,
    required TResult Function(AppConfig appConfig) saveServerConfigs,
  }) {
    return saveServerConfigs(appConfig);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ClipboardData? value)? pasteServerConfigs,
    TResult? Function(AppConfig appConfig)? saveServerConfigs,
  }) {
    return saveServerConfigs?.call(appConfig);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ClipboardData? value)? pasteServerConfigs,
    TResult Function(AppConfig appConfig)? saveServerConfigs,
    required TResult orElse(),
  }) {
    if (saveServerConfigs != null) {
      return saveServerConfigs(appConfig);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PasteServerConfigs value) pasteServerConfigs,
    required TResult Function(_SaveServerConfigs value) saveServerConfigs,
  }) {
    return saveServerConfigs(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult? Function(_SaveServerConfigs value)? saveServerConfigs,
  }) {
    return saveServerConfigs?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PasteServerConfigs value)? pasteServerConfigs,
    TResult Function(_SaveServerConfigs value)? saveServerConfigs,
    required TResult orElse(),
  }) {
    if (saveServerConfigs != null) {
      return saveServerConfigs(this);
    }
    return orElse();
  }
}

abstract class _SaveServerConfigs implements ServerCredentialsEvent {
  const factory _SaveServerConfigs({required final AppConfig appConfig}) =
      _$_SaveServerConfigs;

  AppConfig get appConfig;
  @JsonKey(ignore: true)
  _$$_SaveServerConfigsCopyWith<_$_SaveServerConfigs> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ServerCredentialsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AppConfig? appConfig) parseSuccessful,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(AppConfig? appConfig)? parseSuccessful,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AppConfig? appConfig)? parseSuccessful,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ParseSuccessful value) parseSuccessful,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ParseSuccessful value)? parseSuccessful,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ParseSuccessful value)? parseSuccessful,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerCredentialsStateCopyWith<$Res> {
  factory $ServerCredentialsStateCopyWith(ServerCredentialsState value,
          $Res Function(ServerCredentialsState) then) =
      _$ServerCredentialsStateCopyWithImpl<$Res, ServerCredentialsState>;
}

/// @nodoc
class _$ServerCredentialsStateCopyWithImpl<$Res,
        $Val extends ServerCredentialsState>
    implements $ServerCredentialsStateCopyWith<$Res> {
  _$ServerCredentialsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ServerCredentialsStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ServerCredentialsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AppConfig? appConfig) parseSuccessful,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(AppConfig? appConfig)? parseSuccessful,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AppConfig? appConfig)? parseSuccessful,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ParseSuccessful value) parseSuccessful,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ParseSuccessful value)? parseSuccessful,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ParseSuccessful value)? parseSuccessful,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ServerCredentialsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_ParseSuccessfulCopyWith<$Res> {
  factory _$$_ParseSuccessfulCopyWith(
          _$_ParseSuccessful value, $Res Function(_$_ParseSuccessful) then) =
      __$$_ParseSuccessfulCopyWithImpl<$Res>;
  @useResult
  $Res call({AppConfig? appConfig});

  $AppConfigCopyWith<$Res>? get appConfig;
}

/// @nodoc
class __$$_ParseSuccessfulCopyWithImpl<$Res>
    extends _$ServerCredentialsStateCopyWithImpl<$Res, _$_ParseSuccessful>
    implements _$$_ParseSuccessfulCopyWith<$Res> {
  __$$_ParseSuccessfulCopyWithImpl(
      _$_ParseSuccessful _value, $Res Function(_$_ParseSuccessful) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appConfig = freezed,
  }) {
    return _then(_$_ParseSuccessful(
      appConfig: freezed == appConfig
          ? _value.appConfig
          : appConfig // ignore: cast_nullable_to_non_nullable
              as AppConfig?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $AppConfigCopyWith<$Res>? get appConfig {
    if (_value.appConfig == null) {
      return null;
    }

    return $AppConfigCopyWith<$Res>(_value.appConfig!, (value) {
      return _then(_value.copyWith(appConfig: value));
    });
  }
}

/// @nodoc

class _$_ParseSuccessful implements _ParseSuccessful {
  const _$_ParseSuccessful({required this.appConfig});

  @override
  final AppConfig? appConfig;

  @override
  String toString() {
    return 'ServerCredentialsState.parseSuccessful(appConfig: $appConfig)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ParseSuccessful &&
            (identical(other.appConfig, appConfig) ||
                other.appConfig == appConfig));
  }

  @override
  int get hashCode => Object.hash(runtimeType, appConfig);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ParseSuccessfulCopyWith<_$_ParseSuccessful> get copyWith =>
      __$$_ParseSuccessfulCopyWithImpl<_$_ParseSuccessful>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AppConfig? appConfig) parseSuccessful,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return parseSuccessful(appConfig);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(AppConfig? appConfig)? parseSuccessful,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return parseSuccessful?.call(appConfig);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AppConfig? appConfig)? parseSuccessful,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (parseSuccessful != null) {
      return parseSuccessful(appConfig);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ParseSuccessful value) parseSuccessful,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return parseSuccessful(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ParseSuccessful value)? parseSuccessful,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return parseSuccessful?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ParseSuccessful value)? parseSuccessful,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (parseSuccessful != null) {
      return parseSuccessful(this);
    }
    return orElse();
  }
}

abstract class _ParseSuccessful implements ServerCredentialsState {
  const factory _ParseSuccessful({required final AppConfig? appConfig}) =
      _$_ParseSuccessful;

  AppConfig? get appConfig;
  @JsonKey(ignore: true)
  _$$_ParseSuccessfulCopyWith<_$_ParseSuccessful> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CacheSuccessCopyWith<$Res> {
  factory _$$_CacheSuccessCopyWith(
          _$_CacheSuccess value, $Res Function(_$_CacheSuccess) then) =
      __$$_CacheSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CacheSuccessCopyWithImpl<$Res>
    extends _$ServerCredentialsStateCopyWithImpl<$Res, _$_CacheSuccess>
    implements _$$_CacheSuccessCopyWith<$Res> {
  __$$_CacheSuccessCopyWithImpl(
      _$_CacheSuccess _value, $Res Function(_$_CacheSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CacheSuccess implements _CacheSuccess {
  const _$_CacheSuccess();

  @override
  String toString() {
    return 'ServerCredentialsState.cacheSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CacheSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AppConfig? appConfig) parseSuccessful,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return cacheSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(AppConfig? appConfig)? parseSuccessful,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return cacheSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AppConfig? appConfig)? parseSuccessful,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ParseSuccessful value) parseSuccessful,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return cacheSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ParseSuccessful value)? parseSuccessful,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return cacheSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ParseSuccessful value)? parseSuccessful,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess(this);
    }
    return orElse();
  }
}

abstract class _CacheSuccess implements ServerCredentialsState {
  const factory _CacheSuccess() = _$_CacheSuccess;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$ServerCredentialsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_Error(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'ServerCredentialsState.error(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AppConfig? appConfig) parseSuccessful,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return error(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(AppConfig? appConfig)? parseSuccessful,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return error?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AppConfig? appConfig)? parseSuccessful,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ParseSuccessful value) parseSuccessful,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ParseSuccessful value)? parseSuccessful,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ParseSuccessful value)? parseSuccessful,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements ServerCredentialsState {
  const factory _Error({required final String errorMsg}) = _$_Error;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
