part of 'server_credentials_bloc.dart';

@freezed
class ServerCredentialsState with _$ServerCredentialsState {
  const factory ServerCredentialsState.initial() = _Initial;
  const factory ServerCredentialsState.parseSuccessful({required AppConfig? appConfig}) = _ParseSuccessful;
  const factory ServerCredentialsState.cacheSuccess() = _CacheSuccess;
  const factory ServerCredentialsState.error({required String errorMsg}) = _Error;
}
