part of 'server_credentials_bloc.dart';

@freezed
class ServerCredentialsEvent with _$ServerCredentialsEvent {
  const factory ServerCredentialsEvent.pasteServerConfigs({required ClipboardData? value}) = _PasteServerConfigs;
  const factory ServerCredentialsEvent.saveServerConfigs({required AppConfig appConfig}) = _SaveServerConfigs;
}
