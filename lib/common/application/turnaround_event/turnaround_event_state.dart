part of 'turnaround_event_bloc.dart';

@freezed
class TurnaroundEventState with _$TurnaroundEventState {
  const factory TurnaroundEventState.initial() = _Initial;
  const factory TurnaroundEventState.loading() = _Loading;
  const factory TurnaroundEventState.success({required List<TurnAroundEventDTO> turnAroundEvents}) = _Success;
  const factory TurnaroundEventState.error({required String errorMsg}) = _Error;
  const factory TurnaroundEventState.safeEventSwitch() = _SafeEventSwitch;
  const factory TurnaroundEventState.unsafeEventSwitch() = _UnsafeEventSwitch;
  const factory TurnaroundEventState.canceledEventSwitch() = _CanceledEventSwitch;
}
