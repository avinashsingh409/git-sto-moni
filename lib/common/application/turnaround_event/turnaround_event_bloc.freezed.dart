// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_event_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnaroundEventEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool requiresOnlineSync) getTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        setSelectedTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        forceTurnaroundEventChange,
    required TResult Function() cancelEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult? Function()? cancelEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult Function()? cancelEventSwitch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnAroundEvent value) getTurnAroundEvent,
    required TResult Function(_SetSelectedTurnAroundEventId value)
        setSelectedTurnAroundEvent,
    required TResult Function(_ForceTurnaroundEventChange value)
        forceTurnaroundEventChange,
    required TResult Function(_CancelEventSwitch value) cancelEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult? Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult? Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult? Function(_CancelEventSwitch value)? cancelEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult Function(_CancelEventSwitch value)? cancelEventSwitch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnaroundEventEventCopyWith<$Res> {
  factory $TurnaroundEventEventCopyWith(TurnaroundEventEvent value,
          $Res Function(TurnaroundEventEvent) then) =
      _$TurnaroundEventEventCopyWithImpl<$Res, TurnaroundEventEvent>;
}

/// @nodoc
class _$TurnaroundEventEventCopyWithImpl<$Res,
        $Val extends TurnaroundEventEvent>
    implements $TurnaroundEventEventCopyWith<$Res> {
  _$TurnaroundEventEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetTurnAroundEventCopyWith<$Res> {
  factory _$$_GetTurnAroundEventCopyWith(_$_GetTurnAroundEvent value,
          $Res Function(_$_GetTurnAroundEvent) then) =
      __$$_GetTurnAroundEventCopyWithImpl<$Res>;
  @useResult
  $Res call({bool requiresOnlineSync});
}

/// @nodoc
class __$$_GetTurnAroundEventCopyWithImpl<$Res>
    extends _$TurnaroundEventEventCopyWithImpl<$Res, _$_GetTurnAroundEvent>
    implements _$$_GetTurnAroundEventCopyWith<$Res> {
  __$$_GetTurnAroundEventCopyWithImpl(
      _$_GetTurnAroundEvent _value, $Res Function(_$_GetTurnAroundEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? requiresOnlineSync = null,
  }) {
    return _then(_$_GetTurnAroundEvent(
      requiresOnlineSync: null == requiresOnlineSync
          ? _value.requiresOnlineSync
          : requiresOnlineSync // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_GetTurnAroundEvent implements _GetTurnAroundEvent {
  const _$_GetTurnAroundEvent({required this.requiresOnlineSync});

  @override
  final bool requiresOnlineSync;

  @override
  String toString() {
    return 'TurnaroundEventEvent.getTurnAroundEvent(requiresOnlineSync: $requiresOnlineSync)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GetTurnAroundEvent &&
            (identical(other.requiresOnlineSync, requiresOnlineSync) ||
                other.requiresOnlineSync == requiresOnlineSync));
  }

  @override
  int get hashCode => Object.hash(runtimeType, requiresOnlineSync);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GetTurnAroundEventCopyWith<_$_GetTurnAroundEvent> get copyWith =>
      __$$_GetTurnAroundEventCopyWithImpl<_$_GetTurnAroundEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool requiresOnlineSync) getTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        setSelectedTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        forceTurnaroundEventChange,
    required TResult Function() cancelEventSwitch,
  }) {
    return getTurnAroundEvent(requiresOnlineSync);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult? Function()? cancelEventSwitch,
  }) {
    return getTurnAroundEvent?.call(requiresOnlineSync);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult Function()? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (getTurnAroundEvent != null) {
      return getTurnAroundEvent(requiresOnlineSync);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnAroundEvent value) getTurnAroundEvent,
    required TResult Function(_SetSelectedTurnAroundEventId value)
        setSelectedTurnAroundEvent,
    required TResult Function(_ForceTurnaroundEventChange value)
        forceTurnaroundEventChange,
    required TResult Function(_CancelEventSwitch value) cancelEventSwitch,
  }) {
    return getTurnAroundEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult? Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult? Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult? Function(_CancelEventSwitch value)? cancelEventSwitch,
  }) {
    return getTurnAroundEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult Function(_CancelEventSwitch value)? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (getTurnAroundEvent != null) {
      return getTurnAroundEvent(this);
    }
    return orElse();
  }
}

abstract class _GetTurnAroundEvent implements TurnaroundEventEvent {
  const factory _GetTurnAroundEvent({required final bool requiresOnlineSync}) =
      _$_GetTurnAroundEvent;

  bool get requiresOnlineSync;
  @JsonKey(ignore: true)
  _$$_GetTurnAroundEventCopyWith<_$_GetTurnAroundEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetSelectedTurnAroundEventIdCopyWith<$Res> {
  factory _$$_SetSelectedTurnAroundEventIdCopyWith(
          _$_SetSelectedTurnAroundEventId value,
          $Res Function(_$_SetSelectedTurnAroundEventId) then) =
      __$$_SetSelectedTurnAroundEventIdCopyWithImpl<$Res>;
  @useResult
  $Res call({TurnAroundEventDTO turnaroundEvent});
}

/// @nodoc
class __$$_SetSelectedTurnAroundEventIdCopyWithImpl<$Res>
    extends _$TurnaroundEventEventCopyWithImpl<$Res,
        _$_SetSelectedTurnAroundEventId>
    implements _$$_SetSelectedTurnAroundEventIdCopyWith<$Res> {
  __$$_SetSelectedTurnAroundEventIdCopyWithImpl(
      _$_SetSelectedTurnAroundEventId _value,
      $Res Function(_$_SetSelectedTurnAroundEventId) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? turnaroundEvent = null,
  }) {
    return _then(_$_SetSelectedTurnAroundEventId(
      turnaroundEvent: null == turnaroundEvent
          ? _value.turnaroundEvent
          : turnaroundEvent // ignore: cast_nullable_to_non_nullable
              as TurnAroundEventDTO,
    ));
  }
}

/// @nodoc

class _$_SetSelectedTurnAroundEventId implements _SetSelectedTurnAroundEventId {
  const _$_SetSelectedTurnAroundEventId({required this.turnaroundEvent});

  @override
  final TurnAroundEventDTO turnaroundEvent;

  @override
  String toString() {
    return 'TurnaroundEventEvent.setSelectedTurnAroundEvent(turnaroundEvent: $turnaroundEvent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetSelectedTurnAroundEventId &&
            (identical(other.turnaroundEvent, turnaroundEvent) ||
                other.turnaroundEvent == turnaroundEvent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, turnaroundEvent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetSelectedTurnAroundEventIdCopyWith<_$_SetSelectedTurnAroundEventId>
      get copyWith => __$$_SetSelectedTurnAroundEventIdCopyWithImpl<
          _$_SetSelectedTurnAroundEventId>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool requiresOnlineSync) getTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        setSelectedTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        forceTurnaroundEventChange,
    required TResult Function() cancelEventSwitch,
  }) {
    return setSelectedTurnAroundEvent(turnaroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult? Function()? cancelEventSwitch,
  }) {
    return setSelectedTurnAroundEvent?.call(turnaroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult Function()? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (setSelectedTurnAroundEvent != null) {
      return setSelectedTurnAroundEvent(turnaroundEvent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnAroundEvent value) getTurnAroundEvent,
    required TResult Function(_SetSelectedTurnAroundEventId value)
        setSelectedTurnAroundEvent,
    required TResult Function(_ForceTurnaroundEventChange value)
        forceTurnaroundEventChange,
    required TResult Function(_CancelEventSwitch value) cancelEventSwitch,
  }) {
    return setSelectedTurnAroundEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult? Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult? Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult? Function(_CancelEventSwitch value)? cancelEventSwitch,
  }) {
    return setSelectedTurnAroundEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult Function(_CancelEventSwitch value)? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (setSelectedTurnAroundEvent != null) {
      return setSelectedTurnAroundEvent(this);
    }
    return orElse();
  }
}

abstract class _SetSelectedTurnAroundEventId implements TurnaroundEventEvent {
  const factory _SetSelectedTurnAroundEventId(
          {required final TurnAroundEventDTO turnaroundEvent}) =
      _$_SetSelectedTurnAroundEventId;

  TurnAroundEventDTO get turnaroundEvent;
  @JsonKey(ignore: true)
  _$$_SetSelectedTurnAroundEventIdCopyWith<_$_SetSelectedTurnAroundEventId>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ForceTurnaroundEventChangeCopyWith<$Res> {
  factory _$$_ForceTurnaroundEventChangeCopyWith(
          _$_ForceTurnaroundEventChange value,
          $Res Function(_$_ForceTurnaroundEventChange) then) =
      __$$_ForceTurnaroundEventChangeCopyWithImpl<$Res>;
  @useResult
  $Res call({TurnAroundEventDTO turnaroundEvent});
}

/// @nodoc
class __$$_ForceTurnaroundEventChangeCopyWithImpl<$Res>
    extends _$TurnaroundEventEventCopyWithImpl<$Res,
        _$_ForceTurnaroundEventChange>
    implements _$$_ForceTurnaroundEventChangeCopyWith<$Res> {
  __$$_ForceTurnaroundEventChangeCopyWithImpl(
      _$_ForceTurnaroundEventChange _value,
      $Res Function(_$_ForceTurnaroundEventChange) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? turnaroundEvent = null,
  }) {
    return _then(_$_ForceTurnaroundEventChange(
      turnaroundEvent: null == turnaroundEvent
          ? _value.turnaroundEvent
          : turnaroundEvent // ignore: cast_nullable_to_non_nullable
              as TurnAroundEventDTO,
    ));
  }
}

/// @nodoc

class _$_ForceTurnaroundEventChange implements _ForceTurnaroundEventChange {
  const _$_ForceTurnaroundEventChange({required this.turnaroundEvent});

  @override
  final TurnAroundEventDTO turnaroundEvent;

  @override
  String toString() {
    return 'TurnaroundEventEvent.forceTurnaroundEventChange(turnaroundEvent: $turnaroundEvent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ForceTurnaroundEventChange &&
            (identical(other.turnaroundEvent, turnaroundEvent) ||
                other.turnaroundEvent == turnaroundEvent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, turnaroundEvent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ForceTurnaroundEventChangeCopyWith<_$_ForceTurnaroundEventChange>
      get copyWith => __$$_ForceTurnaroundEventChangeCopyWithImpl<
          _$_ForceTurnaroundEventChange>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool requiresOnlineSync) getTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        setSelectedTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        forceTurnaroundEventChange,
    required TResult Function() cancelEventSwitch,
  }) {
    return forceTurnaroundEventChange(turnaroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult? Function()? cancelEventSwitch,
  }) {
    return forceTurnaroundEventChange?.call(turnaroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult Function()? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (forceTurnaroundEventChange != null) {
      return forceTurnaroundEventChange(turnaroundEvent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnAroundEvent value) getTurnAroundEvent,
    required TResult Function(_SetSelectedTurnAroundEventId value)
        setSelectedTurnAroundEvent,
    required TResult Function(_ForceTurnaroundEventChange value)
        forceTurnaroundEventChange,
    required TResult Function(_CancelEventSwitch value) cancelEventSwitch,
  }) {
    return forceTurnaroundEventChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult? Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult? Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult? Function(_CancelEventSwitch value)? cancelEventSwitch,
  }) {
    return forceTurnaroundEventChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult Function(_CancelEventSwitch value)? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (forceTurnaroundEventChange != null) {
      return forceTurnaroundEventChange(this);
    }
    return orElse();
  }
}

abstract class _ForceTurnaroundEventChange implements TurnaroundEventEvent {
  const factory _ForceTurnaroundEventChange(
          {required final TurnAroundEventDTO turnaroundEvent}) =
      _$_ForceTurnaroundEventChange;

  TurnAroundEventDTO get turnaroundEvent;
  @JsonKey(ignore: true)
  _$$_ForceTurnaroundEventChangeCopyWith<_$_ForceTurnaroundEventChange>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CancelEventSwitchCopyWith<$Res> {
  factory _$$_CancelEventSwitchCopyWith(_$_CancelEventSwitch value,
          $Res Function(_$_CancelEventSwitch) then) =
      __$$_CancelEventSwitchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CancelEventSwitchCopyWithImpl<$Res>
    extends _$TurnaroundEventEventCopyWithImpl<$Res, _$_CancelEventSwitch>
    implements _$$_CancelEventSwitchCopyWith<$Res> {
  __$$_CancelEventSwitchCopyWithImpl(
      _$_CancelEventSwitch _value, $Res Function(_$_CancelEventSwitch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CancelEventSwitch implements _CancelEventSwitch {
  const _$_CancelEventSwitch();

  @override
  String toString() {
    return 'TurnaroundEventEvent.cancelEventSwitch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CancelEventSwitch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool requiresOnlineSync) getTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        setSelectedTurnAroundEvent,
    required TResult Function(TurnAroundEventDTO turnaroundEvent)
        forceTurnaroundEventChange,
    required TResult Function() cancelEventSwitch,
  }) {
    return cancelEventSwitch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult? Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult? Function()? cancelEventSwitch,
  }) {
    return cancelEventSwitch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool requiresOnlineSync)? getTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        setSelectedTurnAroundEvent,
    TResult Function(TurnAroundEventDTO turnaroundEvent)?
        forceTurnaroundEventChange,
    TResult Function()? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (cancelEventSwitch != null) {
      return cancelEventSwitch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnAroundEvent value) getTurnAroundEvent,
    required TResult Function(_SetSelectedTurnAroundEventId value)
        setSelectedTurnAroundEvent,
    required TResult Function(_ForceTurnaroundEventChange value)
        forceTurnaroundEventChange,
    required TResult Function(_CancelEventSwitch value) cancelEventSwitch,
  }) {
    return cancelEventSwitch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult? Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult? Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult? Function(_CancelEventSwitch value)? cancelEventSwitch,
  }) {
    return cancelEventSwitch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnAroundEvent value)? getTurnAroundEvent,
    TResult Function(_SetSelectedTurnAroundEventId value)?
        setSelectedTurnAroundEvent,
    TResult Function(_ForceTurnaroundEventChange value)?
        forceTurnaroundEventChange,
    TResult Function(_CancelEventSwitch value)? cancelEventSwitch,
    required TResult orElse(),
  }) {
    if (cancelEventSwitch != null) {
      return cancelEventSwitch(this);
    }
    return orElse();
  }
}

abstract class _CancelEventSwitch implements TurnaroundEventEvent {
  const factory _CancelEventSwitch() = _$_CancelEventSwitch;
}

/// @nodoc
mixin _$TurnaroundEventState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnaroundEventStateCopyWith<$Res> {
  factory $TurnaroundEventStateCopyWith(TurnaroundEventState value,
          $Res Function(TurnaroundEventState) then) =
      _$TurnaroundEventStateCopyWithImpl<$Res, TurnaroundEventState>;
}

/// @nodoc
class _$TurnaroundEventStateCopyWithImpl<$Res,
        $Val extends TurnaroundEventState>
    implements $TurnaroundEventStateCopyWith<$Res> {
  _$TurnaroundEventStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'TurnaroundEventState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements TurnaroundEventState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'TurnaroundEventState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements TurnaroundEventState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SuccessCopyWith<$Res> {
  factory _$$_SuccessCopyWith(
          _$_Success value, $Res Function(_$_Success) then) =
      __$$_SuccessCopyWithImpl<$Res>;
  @useResult
  $Res call({List<TurnAroundEventDTO> turnAroundEvents});
}

/// @nodoc
class __$$_SuccessCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_Success>
    implements _$$_SuccessCopyWith<$Res> {
  __$$_SuccessCopyWithImpl(_$_Success _value, $Res Function(_$_Success) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? turnAroundEvents = null,
  }) {
    return _then(_$_Success(
      turnAroundEvents: null == turnAroundEvents
          ? _value._turnAroundEvents
          : turnAroundEvents // ignore: cast_nullable_to_non_nullable
              as List<TurnAroundEventDTO>,
    ));
  }
}

/// @nodoc

class _$_Success implements _Success {
  const _$_Success({required final List<TurnAroundEventDTO> turnAroundEvents})
      : _turnAroundEvents = turnAroundEvents;

  final List<TurnAroundEventDTO> _turnAroundEvents;
  @override
  List<TurnAroundEventDTO> get turnAroundEvents {
    if (_turnAroundEvents is EqualUnmodifiableListView)
      return _turnAroundEvents;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_turnAroundEvents);
  }

  @override
  String toString() {
    return 'TurnaroundEventState.success(turnAroundEvents: $turnAroundEvents)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Success &&
            const DeepCollectionEquality()
                .equals(other._turnAroundEvents, _turnAroundEvents));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_turnAroundEvents));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      __$$_SuccessCopyWithImpl<_$_Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return success(turnAroundEvents);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return success?.call(turnAroundEvents);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(turnAroundEvents);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _Success implements TurnaroundEventState {
  const factory _Success(
      {required final List<TurnAroundEventDTO> turnAroundEvents}) = _$_Success;

  List<TurnAroundEventDTO> get turnAroundEvents;
  @JsonKey(ignore: true)
  _$$_SuccessCopyWith<_$_Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_Error(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'TurnaroundEventState.error(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return error(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return error?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements TurnaroundEventState {
  const factory _Error({required final String errorMsg}) = _$_Error;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SafeEventSwitchCopyWith<$Res> {
  factory _$$_SafeEventSwitchCopyWith(
          _$_SafeEventSwitch value, $Res Function(_$_SafeEventSwitch) then) =
      __$$_SafeEventSwitchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SafeEventSwitchCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_SafeEventSwitch>
    implements _$$_SafeEventSwitchCopyWith<$Res> {
  __$$_SafeEventSwitchCopyWithImpl(
      _$_SafeEventSwitch _value, $Res Function(_$_SafeEventSwitch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_SafeEventSwitch implements _SafeEventSwitch {
  const _$_SafeEventSwitch();

  @override
  String toString() {
    return 'TurnaroundEventState.safeEventSwitch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SafeEventSwitch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return safeEventSwitch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return safeEventSwitch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (safeEventSwitch != null) {
      return safeEventSwitch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return safeEventSwitch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return safeEventSwitch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (safeEventSwitch != null) {
      return safeEventSwitch(this);
    }
    return orElse();
  }
}

abstract class _SafeEventSwitch implements TurnaroundEventState {
  const factory _SafeEventSwitch() = _$_SafeEventSwitch;
}

/// @nodoc
abstract class _$$_UnsafeEventSwitchCopyWith<$Res> {
  factory _$$_UnsafeEventSwitchCopyWith(_$_UnsafeEventSwitch value,
          $Res Function(_$_UnsafeEventSwitch) then) =
      __$$_UnsafeEventSwitchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UnsafeEventSwitchCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_UnsafeEventSwitch>
    implements _$$_UnsafeEventSwitchCopyWith<$Res> {
  __$$_UnsafeEventSwitchCopyWithImpl(
      _$_UnsafeEventSwitch _value, $Res Function(_$_UnsafeEventSwitch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UnsafeEventSwitch implements _UnsafeEventSwitch {
  const _$_UnsafeEventSwitch();

  @override
  String toString() {
    return 'TurnaroundEventState.unsafeEventSwitch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UnsafeEventSwitch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return unsafeEventSwitch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return unsafeEventSwitch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (unsafeEventSwitch != null) {
      return unsafeEventSwitch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return unsafeEventSwitch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return unsafeEventSwitch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (unsafeEventSwitch != null) {
      return unsafeEventSwitch(this);
    }
    return orElse();
  }
}

abstract class _UnsafeEventSwitch implements TurnaroundEventState {
  const factory _UnsafeEventSwitch() = _$_UnsafeEventSwitch;
}

/// @nodoc
abstract class _$$_CanceledEventSwitchCopyWith<$Res> {
  factory _$$_CanceledEventSwitchCopyWith(_$_CanceledEventSwitch value,
          $Res Function(_$_CanceledEventSwitch) then) =
      __$$_CanceledEventSwitchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CanceledEventSwitchCopyWithImpl<$Res>
    extends _$TurnaroundEventStateCopyWithImpl<$Res, _$_CanceledEventSwitch>
    implements _$$_CanceledEventSwitchCopyWith<$Res> {
  __$$_CanceledEventSwitchCopyWithImpl(_$_CanceledEventSwitch _value,
      $Res Function(_$_CanceledEventSwitch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CanceledEventSwitch implements _CanceledEventSwitch {
  const _$_CanceledEventSwitch();

  @override
  String toString() {
    return 'TurnaroundEventState.canceledEventSwitch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CanceledEventSwitch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<TurnAroundEventDTO> turnAroundEvents)
        success,
    required TResult Function(String errorMsg) error,
    required TResult Function() safeEventSwitch,
    required TResult Function() unsafeEventSwitch,
    required TResult Function() canceledEventSwitch,
  }) {
    return canceledEventSwitch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult? Function(String errorMsg)? error,
    TResult? Function()? safeEventSwitch,
    TResult? Function()? unsafeEventSwitch,
    TResult? Function()? canceledEventSwitch,
  }) {
    return canceledEventSwitch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<TurnAroundEventDTO> turnAroundEvents)? success,
    TResult Function(String errorMsg)? error,
    TResult Function()? safeEventSwitch,
    TResult Function()? unsafeEventSwitch,
    TResult Function()? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (canceledEventSwitch != null) {
      return canceledEventSwitch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Success value) success,
    required TResult Function(_Error value) error,
    required TResult Function(_SafeEventSwitch value) safeEventSwitch,
    required TResult Function(_UnsafeEventSwitch value) unsafeEventSwitch,
    required TResult Function(_CanceledEventSwitch value) canceledEventSwitch,
  }) {
    return canceledEventSwitch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Success value)? success,
    TResult? Function(_Error value)? error,
    TResult? Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult? Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult? Function(_CanceledEventSwitch value)? canceledEventSwitch,
  }) {
    return canceledEventSwitch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Success value)? success,
    TResult Function(_Error value)? error,
    TResult Function(_SafeEventSwitch value)? safeEventSwitch,
    TResult Function(_UnsafeEventSwitch value)? unsafeEventSwitch,
    TResult Function(_CanceledEventSwitch value)? canceledEventSwitch,
    required TResult orElse(),
  }) {
    if (canceledEventSwitch != null) {
      return canceledEventSwitch(this);
    }
    return orElse();
  }
}

abstract class _CanceledEventSwitch implements TurnaroundEventState {
  const factory _CanceledEventSwitch() = _$_CanceledEventSwitch;
}
