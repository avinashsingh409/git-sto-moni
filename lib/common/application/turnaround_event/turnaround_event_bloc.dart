import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/domain/data_loss/repository/data_loss_repository.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_event_exception.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';

part 'turnaround_event_event.dart';
part 'turnaround_event_state.dart';
part 'turnaround_event_bloc.freezed.dart';

@injectable
class TurnaroundEventBloc extends Bloc<TurnaroundEventEvent, TurnaroundEventState> {
  final ITurnAroundEventRepository turnAroundEventRepository;
  final IDataLossRepository dataLossRepository;

  TurnaroundEventBloc(this.turnAroundEventRepository, this.dataLossRepository) : super(const _Initial()) {
    on<TurnaroundEventEvent>(
      (event, emit) async {
        await event.when(getTurnAroundEvent: (requiresOnlineSync) async {
          emit(const TurnaroundEventState.loading());
          if (requiresOnlineSync) {
            await _syncAndCacheEvents(emit);
          } else {
            await _getEventsFromCache(emit);
          }
        }, setSelectedTurnAroundEvent: (turnaroundEvent) async {
          final bool dataChanged = await dataLossRepository.isDataChanged();
          if (!dataChanged) {
            await _saveSelectedTurnaroundEvent(turnaroundEvent, emit);
          } else {
            emit(const TurnaroundEventState.unsafeEventSwitch());
          }
        }, forceTurnaroundEventChange: (turnaroundEvent) async {
          await _saveSelectedTurnaroundEvent(turnaroundEvent, emit);
        }, cancelEventSwitch: () {
          emit(const TurnaroundEventState.canceledEventSwitch());
        });
      },
    );
  }

  Future<void> _saveSelectedTurnaroundEvent(TurnAroundEventDTO turnaroundEvent, Emitter<TurnaroundEventState> emit) async {
    await turnAroundEventRepository.saveSelectedTurnAroundEventId(turnAroundId: turnaroundEvent.id ?? 0);
    await turnAroundEventRepository.saveBackwardsCommentEnabled(isBackwardsCommentEnabled: (turnaroundEvent.backwardsCommentsEnabled != null) ? turnaroundEvent.backwardsCommentsEnabled! : false);
    await turnAroundEventRepository.savePercentageRange(percentageRange: turnaroundEvent.isManualProgress == true
      ? "${turnaroundEvent.percentageRangeLowerBound}-${turnaroundEvent.percentageRangeUpperBound}" : "",);
    await turnAroundEventRepository.saveTurnaroundEventSwipeEnabled(turnaroundEventSwipeEnabled: turnaroundEvent.swipeAllowed ?? true);
    emit(const TurnaroundEventState.safeEventSwitch());
  }

  Future<void> _syncAndCacheEvents(Emitter<TurnaroundEventState> emit) async {
    //Fetching Turnaround Activities from API
    final turnAroundEvents = await turnAroundEventRepository.getTurnAroundEvents();
    await turnAroundEvents.fold((turnAroundEventResponse) async {
      final events = turnAroundEventResponse.events;
      if (events != null) {
        List<TurnAroundEventDTO> sortedEvents = List<TurnAroundEventDTO>.from(events)..sort((a, b) => (a.description ?? "").toLowerCase().compareTo((b.description ?? "").toLowerCase()));
        await _cacheTurnAroundEvents(sortedEvents, emit);
      } else {
        emit(TurnaroundEventState.error(
          errorMsg: TurnAroundEventException.getErrorMessage(
            const TurnAroundEventException.noEventsOnline(),
          ),
        ));
      }
      //Caching Turnaround activities
    }, (exception) {
      emit(TurnaroundEventState.error(errorMsg: TurnAroundEventException.getErrorMessage(exception)));
    });
  }

  Future<void> _cacheTurnAroundEvents(List<TurnAroundEventDTO> turnAroundEvents, Emitter<TurnaroundEventState> emit) async {
    await turnAroundEventRepository.deleteTurnAroudEvents();
    await turnAroundEventRepository.insertAllActivities(turnAroundEvents: turnAroundEvents);
    emit(TurnaroundEventState.success(turnAroundEvents: turnAroundEvents));
  }

  Future<void> _getEventsFromCache(Emitter<TurnaroundEventState> emit) async {
    //Fetching Turnaround Activities from Cache
    final events = await turnAroundEventRepository.getCachedTurnAroundEvents();
    events.fold((events) {
      events.sort((a, b) => (a.description ?? "").toLowerCase().compareTo((b.description ?? "").toLowerCase()));
      emit(TurnaroundEventState.success(turnAroundEvents: events));
    }, (_) {
      emit(TurnaroundEventState.error(
          errorMsg: TurnAroundEventException.getErrorMessage(const TurnAroundEventException.noCachedEvents())));
    });
  }
}
