part of 'turnaround_event_bloc.dart';

@freezed
class TurnaroundEventEvent with _$TurnaroundEventEvent {
  const factory TurnaroundEventEvent.getTurnAroundEvent({required bool requiresOnlineSync}) = _GetTurnAroundEvent;
  const factory TurnaroundEventEvent.setSelectedTurnAroundEvent({required TurnAroundEventDTO turnaroundEvent}) = _SetSelectedTurnAroundEventId;
  const factory TurnaroundEventEvent.forceTurnaroundEventChange({required TurnAroundEventDTO turnaroundEvent}) = _ForceTurnaroundEventChange;
  const factory TurnaroundEventEvent.cancelEventSwitch() = _CancelEventSwitch;
}
