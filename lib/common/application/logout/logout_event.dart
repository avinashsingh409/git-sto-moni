part of 'logout_bloc.dart';

@freezed
class LogoutEvent with _$LogoutEvent {
  const factory LogoutEvent.prepareLogout() = _PrepareLogout;
  const factory LogoutEvent.forceLogout() = _ForceLogout;
}
