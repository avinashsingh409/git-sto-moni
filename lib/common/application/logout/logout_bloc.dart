import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/domain/data_loss/repository/data_loss_repository.dart';
import 'package:sto_mobile_v2/common/domain/exception/logout_exception.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

import 'package:sto_mobile_v2/common/domain/session/repository/logout_repository.dart';

part 'logout_event.dart';
part 'logout_state.dart';
part 'logout_bloc.freezed.dart';

@lazySingleton
class LogoutBloc extends Bloc<LogoutEvent, LogoutState> {
  final IAppConfigRepository appConfigRepository;
  final ILogoutRepository logoutRepository;
  final IDataLossRepository dataLossRepository;

  LogoutBloc(this.appConfigRepository, this.logoutRepository, this.dataLossRepository) : super(const _Initial()) {
    on<LogoutEvent>((event, emit) async {
      await event.when(prepareLogout: () async {
        emit(const LogoutState.loading());
        final appConfig = appConfigRepository.getAppConfig();
        await appConfig.fold(
          (appConfig) async {
            final isDataChanged = await dataLossRepository.isDataChanged();
            if (isDataChanged) {
              emit(const LogoutState.cacheNotEmpty());
            } else {
              await _doLogout(appConfig, emit);
            }
          },
          (exception) async {
            emit(LogoutState.error(
              errMsg: LogoutException.getErrorMessage(const LogoutException.unexpectedError()),
            ));
          }
        );
      }, forceLogout: () async {
        final appConfig = appConfigRepository.getAppConfig();
        await appConfig.fold(
          (appConfig) async {
            await _doLogout(appConfig, emit);
          },
          (exception) {
            emit(LogoutState.error(
              errMsg: LogoutException.getErrorMessage(const LogoutException.unexpectedError()),
            ));
          },
        );
      });
    });
  }

  Future<void> _doLogout(AppConfig appConfig, Emitter<LogoutState> emit) async {
    final logout = await logoutRepository.logout(appConfig: appConfig);
    await logout.fold(
      (_) async {
        emit(const LogoutState.success());
      },
      (exception) {
        emit(
          LogoutState.error(
            errMsg: LogoutException.getErrorMessage(exception),
          ),
        );
      },
    );
  }
}
