// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'profile_drawer_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileDrawerState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(UserInformation userInformation) profileLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(UserInformation userInformation)? profileLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(UserInformation userInformation)? profileLoaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ProfileLoaded value) profileLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ProfileLoaded value)? profileLoaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ProfileLoaded value)? profileLoaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileDrawerStateCopyWith<$Res> {
  factory $ProfileDrawerStateCopyWith(
          ProfileDrawerState value, $Res Function(ProfileDrawerState) then) =
      _$ProfileDrawerStateCopyWithImpl<$Res, ProfileDrawerState>;
}

/// @nodoc
class _$ProfileDrawerStateCopyWithImpl<$Res, $Val extends ProfileDrawerState>
    implements $ProfileDrawerStateCopyWith<$Res> {
  _$ProfileDrawerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$ProfileDrawerStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ProfileDrawerState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(UserInformation userInformation) profileLoaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(UserInformation userInformation)? profileLoaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(UserInformation userInformation)? profileLoaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ProfileLoaded value) profileLoaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ProfileLoaded value)? profileLoaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ProfileLoaded value)? profileLoaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ProfileDrawerState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_ProfileLoadedCopyWith<$Res> {
  factory _$$_ProfileLoadedCopyWith(
          _$_ProfileLoaded value, $Res Function(_$_ProfileLoaded) then) =
      __$$_ProfileLoadedCopyWithImpl<$Res>;
  @useResult
  $Res call({UserInformation userInformation});

  $UserInformationCopyWith<$Res> get userInformation;
}

/// @nodoc
class __$$_ProfileLoadedCopyWithImpl<$Res>
    extends _$ProfileDrawerStateCopyWithImpl<$Res, _$_ProfileLoaded>
    implements _$$_ProfileLoadedCopyWith<$Res> {
  __$$_ProfileLoadedCopyWithImpl(
      _$_ProfileLoaded _value, $Res Function(_$_ProfileLoaded) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userInformation = null,
  }) {
    return _then(_$_ProfileLoaded(
      userInformation: null == userInformation
          ? _value.userInformation
          : userInformation // ignore: cast_nullable_to_non_nullable
              as UserInformation,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $UserInformationCopyWith<$Res> get userInformation {
    return $UserInformationCopyWith<$Res>(_value.userInformation, (value) {
      return _then(_value.copyWith(userInformation: value));
    });
  }
}

/// @nodoc

class _$_ProfileLoaded implements _ProfileLoaded {
  const _$_ProfileLoaded({required this.userInformation});

  @override
  final UserInformation userInformation;

  @override
  String toString() {
    return 'ProfileDrawerState.profileLoaded(userInformation: $userInformation)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProfileLoaded &&
            (identical(other.userInformation, userInformation) ||
                other.userInformation == userInformation));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userInformation);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProfileLoadedCopyWith<_$_ProfileLoaded> get copyWith =>
      __$$_ProfileLoadedCopyWithImpl<_$_ProfileLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(UserInformation userInformation) profileLoaded,
  }) {
    return profileLoaded(userInformation);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(UserInformation userInformation)? profileLoaded,
  }) {
    return profileLoaded?.call(userInformation);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(UserInformation userInformation)? profileLoaded,
    required TResult orElse(),
  }) {
    if (profileLoaded != null) {
      return profileLoaded(userInformation);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ProfileLoaded value) profileLoaded,
  }) {
    return profileLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ProfileLoaded value)? profileLoaded,
  }) {
    return profileLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ProfileLoaded value)? profileLoaded,
    required TResult orElse(),
  }) {
    if (profileLoaded != null) {
      return profileLoaded(this);
    }
    return orElse();
  }
}

abstract class _ProfileLoaded implements ProfileDrawerState {
  const factory _ProfileLoaded(
      {required final UserInformation userInformation}) = _$_ProfileLoaded;

  UserInformation get userInformation;
  @JsonKey(ignore: true)
  _$$_ProfileLoadedCopyWith<_$_ProfileLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}
