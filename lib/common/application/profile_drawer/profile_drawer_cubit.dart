import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';

part 'profile_drawer_state.dart';
part 'profile_drawer_cubit.freezed.dart';

@injectable
class ProfileDrawerCubit extends Cubit<ProfileDrawerState> {
  final ISessionRepository sessionRepository;
  ProfileDrawerCubit(this.sessionRepository) : super(const ProfileDrawerState.initial());

  void getDetails() {
    emit(const ProfileDrawerState.initial());
    final objSession = sessionRepository.getSession();
    objSession.fold((session) {
      emit(ProfileDrawerState.profileLoaded(userInformation: session.userInformation));
    }, (_) {});
  }
}
