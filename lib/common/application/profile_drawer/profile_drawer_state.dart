part of 'profile_drawer_cubit.dart';

@freezed
class ProfileDrawerState with _$ProfileDrawerState {
  const factory ProfileDrawerState.initial() = _Initial;
  const factory ProfileDrawerState.profileLoaded({required UserInformation userInformation}) = _ProfileLoaded;
}
