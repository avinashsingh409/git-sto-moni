import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';

part 'splash_event.dart';
part 'splash_state.dart';
part 'splash_bloc.freezed.dart';

@injectable
class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final IAppConfigRepository appConfigRepository;
  final ISessionRepository sessionRepository;
  SplashBloc(this.appConfigRepository, this.sessionRepository) : super(const _Initial()) {
    on<SplashEvent>((event, emit) {
      event.when(started: () {
        emit(const SplashState.loading());
        final session = sessionRepository.getSession();
        session.fold((session) {
          emit(const SplashState.loggedIn());
        }, (_) {
          final appConfig = appConfigRepository.getAppConfig();
          appConfig.fold(
            (appConfig) => emit(const SplashState.configuartionExisting()),
            (exception) => emit(
              const SplashState.configurationNeeded(),
            ),
          );
        });
      });
    });
    add(const SplashEvent.started());
  }
}
