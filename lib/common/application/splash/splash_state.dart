part of 'splash_bloc.dart';

@freezed
class SplashState with _$SplashState {
  const factory SplashState.initial() = _Initial;
  const factory SplashState.loading() = _Loading;
  const factory SplashState.loggedIn() = _LoggedIn;
  const factory SplashState.configurationNeeded() = _ConfigurationNeeded;
  const factory SplashState.configuartionExisting() = _ConfiguartionExisting;
}
