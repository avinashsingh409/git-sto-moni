// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_switcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppSwitcherEvent {
  AppFeatures get selectedFeature => throw _privateConstructorUsedError;
  bool get isFromHome => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AppFeatures selectedFeature, bool isFromHome)
        switchApp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AppFeatures selectedFeature, bool isFromHome)? switchApp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AppFeatures selectedFeature, bool isFromHome)? switchApp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SwitchApp value) switchApp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SwitchApp value)? switchApp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SwitchApp value)? switchApp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppSwitcherEventCopyWith<AppSwitcherEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppSwitcherEventCopyWith<$Res> {
  factory $AppSwitcherEventCopyWith(
          AppSwitcherEvent value, $Res Function(AppSwitcherEvent) then) =
      _$AppSwitcherEventCopyWithImpl<$Res, AppSwitcherEvent>;
  @useResult
  $Res call({AppFeatures selectedFeature, bool isFromHome});
}

/// @nodoc
class _$AppSwitcherEventCopyWithImpl<$Res, $Val extends AppSwitcherEvent>
    implements $AppSwitcherEventCopyWith<$Res> {
  _$AppSwitcherEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedFeature = null,
    Object? isFromHome = null,
  }) {
    return _then(_value.copyWith(
      selectedFeature: null == selectedFeature
          ? _value.selectedFeature
          : selectedFeature // ignore: cast_nullable_to_non_nullable
              as AppFeatures,
      isFromHome: null == isFromHome
          ? _value.isFromHome
          : isFromHome // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SwitchAppCopyWith<$Res>
    implements $AppSwitcherEventCopyWith<$Res> {
  factory _$$_SwitchAppCopyWith(
          _$_SwitchApp value, $Res Function(_$_SwitchApp) then) =
      __$$_SwitchAppCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AppFeatures selectedFeature, bool isFromHome});
}

/// @nodoc
class __$$_SwitchAppCopyWithImpl<$Res>
    extends _$AppSwitcherEventCopyWithImpl<$Res, _$_SwitchApp>
    implements _$$_SwitchAppCopyWith<$Res> {
  __$$_SwitchAppCopyWithImpl(
      _$_SwitchApp _value, $Res Function(_$_SwitchApp) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedFeature = null,
    Object? isFromHome = null,
  }) {
    return _then(_$_SwitchApp(
      selectedFeature: null == selectedFeature
          ? _value.selectedFeature
          : selectedFeature // ignore: cast_nullable_to_non_nullable
              as AppFeatures,
      isFromHome: null == isFromHome
          ? _value.isFromHome
          : isFromHome // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SwitchApp implements _SwitchApp {
  const _$_SwitchApp({required this.selectedFeature, required this.isFromHome});

  @override
  final AppFeatures selectedFeature;
  @override
  final bool isFromHome;

  @override
  String toString() {
    return 'AppSwitcherEvent.switchApp(selectedFeature: $selectedFeature, isFromHome: $isFromHome)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SwitchApp &&
            (identical(other.selectedFeature, selectedFeature) ||
                other.selectedFeature == selectedFeature) &&
            (identical(other.isFromHome, isFromHome) ||
                other.isFromHome == isFromHome));
  }

  @override
  int get hashCode => Object.hash(runtimeType, selectedFeature, isFromHome);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SwitchAppCopyWith<_$_SwitchApp> get copyWith =>
      __$$_SwitchAppCopyWithImpl<_$_SwitchApp>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AppFeatures selectedFeature, bool isFromHome)
        switchApp,
  }) {
    return switchApp(selectedFeature, isFromHome);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AppFeatures selectedFeature, bool isFromHome)? switchApp,
  }) {
    return switchApp?.call(selectedFeature, isFromHome);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AppFeatures selectedFeature, bool isFromHome)? switchApp,
    required TResult orElse(),
  }) {
    if (switchApp != null) {
      return switchApp(selectedFeature, isFromHome);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SwitchApp value) switchApp,
  }) {
    return switchApp(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SwitchApp value)? switchApp,
  }) {
    return switchApp?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SwitchApp value)? switchApp,
    required TResult orElse(),
  }) {
    if (switchApp != null) {
      return switchApp(this);
    }
    return orElse();
  }
}

abstract class _SwitchApp implements AppSwitcherEvent {
  const factory _SwitchApp(
      {required final AppFeatures selectedFeature,
      required final bool isFromHome}) = _$_SwitchApp;

  @override
  AppFeatures get selectedFeature;
  @override
  bool get isFromHome;
  @override
  @JsonKey(ignore: true)
  _$$_SwitchAppCopyWith<_$_SwitchApp> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AppSwitcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppSwitcherStateCopyWith<$Res> {
  factory $AppSwitcherStateCopyWith(
          AppSwitcherState value, $Res Function(AppSwitcherState) then) =
      _$AppSwitcherStateCopyWithImpl<$Res, AppSwitcherState>;
}

/// @nodoc
class _$AppSwitcherStateCopyWithImpl<$Res, $Val extends AppSwitcherState>
    implements $AppSwitcherStateCopyWith<$Res> {
  _$AppSwitcherStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$AppSwitcherStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'AppSwitcherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements AppSwitcherState {
  const factory _Initial() = _$_Initial;
}
