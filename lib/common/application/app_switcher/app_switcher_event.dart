part of 'app_switcher_bloc.dart';

@freezed
class AppSwitcherEvent with _$AppSwitcherEvent {
  const factory AppSwitcherEvent.switchApp({
    required AppFeatures selectedFeature,
    required bool isFromHome,
  }) = _SwitchApp;
}
