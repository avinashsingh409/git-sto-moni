part of 'app_switcher_bloc.dart';

@freezed
class AppSwitcherState with _$AppSwitcherState {
  const factory AppSwitcherState.initial() = _Initial;
}
