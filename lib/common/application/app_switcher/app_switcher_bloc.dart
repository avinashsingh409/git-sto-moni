import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_switcher/datastore/enums/app_features.dart';
import 'package:sto_mobile_v2/common/domain/app_switcher/repository/app_switcher_repository.dart';
import 'package:sto_mobile_v2/common/domain/license/repository/license_repository.dart';
import 'package:sto_mobile_v2/config/app_theme.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

part 'app_switcher_event.dart';
part 'app_switcher_state.dart';
part 'app_switcher_bloc.freezed.dart';

@lazySingleton
class AppSwitcherBloc extends Bloc<AppSwitcherEvent, AppSwitcherState> {
  final ILicenseRepository licenseRepository;
  final IAppSwitcherRepository appSwitcherRepository;
  AppSwitcherBloc(this.appSwitcherRepository, this.licenseRepository) : super(const _Initial()) {
    on<AppSwitcherEvent>((event, emit) async {
      await event.when(switchApp: (selectedFeature, isFromHome) async {
        final licenses = await licenseRepository.getCachedLicense();
        if (licenses != null) {
          switch (selectedFeature) {
            case AppFeatures.planner:
              if (licenses.planner != null && licenses.planner!) {
                await appSwitcherRepository
                    .saveSelectedApp(appFeatures: selectedFeature)
                    .then((_) => jumpToHomeScreen(context: GlobalNavigationKey().context!));
              } else {
                if (isFromHome) {
                  Navigator.of(GlobalNavigationKey().context!).pop();
                }
                _showErrorToast(isFromHome: isFromHome);
              }
              break;
            case AppFeatures.execution:
              if (licenses.execution != null && licenses.execution!) {
                await appSwitcherRepository
                    .saveSelectedApp(appFeatures: selectedFeature)
                    .then((_) => jumpToHomeScreen(context: GlobalNavigationKey().context!));
              } else {
                if (isFromHome) {
                  Navigator.of(GlobalNavigationKey().context!).pop();
                }
                _showErrorToast(isFromHome: isFromHome);
              }
              break;
            case AppFeatures.notSelected:
              break;
          }
        }
      });
    });
  }

  void _showErrorToast({required bool isFromHome}) {
    ScaffoldMessenger.of(GlobalNavigationKey().context!)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
          backgroundColor: isFromHome ? AppTheme.pgPrimaryRed : Colors.white,
          content: Center(
            child: Text(
              'Not Licensed',
              style: TextStyle(color: isFromHome ? Colors.white : Colors.red, fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          duration: const Duration(seconds: 2)));
  }
}
