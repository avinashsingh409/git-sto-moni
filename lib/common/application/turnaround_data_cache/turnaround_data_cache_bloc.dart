import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_data_exception.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_data/repository/turnaround_data_repository.dart';

part 'turnaround_data_cache_event.dart';
part 'turnaround_data_cache_state.dart';
part 'turnaround_data_cache_bloc.freezed.dart';

@injectable
class TurnaroundDataCacheBloc extends Bloc<TurnaroundDataCacheEvent, TurnaroundDataCacheState> {
  final ITurnaroundDataRepository turnaroundDataRepository;
  TurnaroundDataCacheBloc(this.turnaroundDataRepository) : super(const _Initial()) {
    on<TurnaroundDataCacheEvent>((event, emit) async {
      await event.when(getTurnaroundData: () async {
        emit(const TurnaroundDataCacheState.executionAndPlannerLoading());
        final turnaroundData = await turnaroundDataRepository.getAllTurnaroundData();
        await turnaroundData.fold((success) async {
          emit(const TurnaroundDataCacheState.cacheSuccess());
        }, (exception) async {
          emit(TurnaroundDataCacheState.cacheFailure(errorMsg: TurnAroundDataException.getErrorMessage(exception)));
        });
      });
    });
  }
}
