part of 'turnaround_data_cache_bloc.dart';

@freezed
class TurnaroundDataCacheState with _$TurnaroundDataCacheState {
  const factory TurnaroundDataCacheState.initial() = _Initial;
  const factory TurnaroundDataCacheState.executionAndPlannerLoading() = _ExecutionAndPlannerLoading;
  const factory TurnaroundDataCacheState.cacheSuccess() = _CacheSuccess;
  const factory TurnaroundDataCacheState.cacheFailure({required String errorMsg}) = _CacheFailure;
  const factory TurnaroundDataCacheState.executionDataError() = _ExecutionDataError;
  const factory TurnaroundDataCacheState.plannerDataError() = _PlannerDataError;
}
