part of 'turnaround_data_cache_bloc.dart';

@freezed
class TurnaroundDataCacheEvent with _$TurnaroundDataCacheEvent {
  const factory TurnaroundDataCacheEvent.getTurnaroundData() = _GetTurnaroundData;
}
