// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_data_cache_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnaroundDataCacheEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getTurnaroundData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getTurnaroundData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getTurnaroundData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnaroundData value) getTurnaroundData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnaroundData value)? getTurnaroundData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnaroundData value)? getTurnaroundData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnaroundDataCacheEventCopyWith<$Res> {
  factory $TurnaroundDataCacheEventCopyWith(TurnaroundDataCacheEvent value,
          $Res Function(TurnaroundDataCacheEvent) then) =
      _$TurnaroundDataCacheEventCopyWithImpl<$Res, TurnaroundDataCacheEvent>;
}

/// @nodoc
class _$TurnaroundDataCacheEventCopyWithImpl<$Res,
        $Val extends TurnaroundDataCacheEvent>
    implements $TurnaroundDataCacheEventCopyWith<$Res> {
  _$TurnaroundDataCacheEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_GetTurnaroundDataCopyWith<$Res> {
  factory _$$_GetTurnaroundDataCopyWith(_$_GetTurnaroundData value,
          $Res Function(_$_GetTurnaroundData) then) =
      __$$_GetTurnaroundDataCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GetTurnaroundDataCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheEventCopyWithImpl<$Res, _$_GetTurnaroundData>
    implements _$$_GetTurnaroundDataCopyWith<$Res> {
  __$$_GetTurnaroundDataCopyWithImpl(
      _$_GetTurnaroundData _value, $Res Function(_$_GetTurnaroundData) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GetTurnaroundData implements _GetTurnaroundData {
  const _$_GetTurnaroundData();

  @override
  String toString() {
    return 'TurnaroundDataCacheEvent.getTurnaroundData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GetTurnaroundData);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getTurnaroundData,
  }) {
    return getTurnaroundData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getTurnaroundData,
  }) {
    return getTurnaroundData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getTurnaroundData,
    required TResult orElse(),
  }) {
    if (getTurnaroundData != null) {
      return getTurnaroundData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetTurnaroundData value) getTurnaroundData,
  }) {
    return getTurnaroundData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetTurnaroundData value)? getTurnaroundData,
  }) {
    return getTurnaroundData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetTurnaroundData value)? getTurnaroundData,
    required TResult orElse(),
  }) {
    if (getTurnaroundData != null) {
      return getTurnaroundData(this);
    }
    return orElse();
  }
}

abstract class _GetTurnaroundData implements TurnaroundDataCacheEvent {
  const factory _GetTurnaroundData() = _$_GetTurnaroundData;
}

/// @nodoc
mixin _$TurnaroundDataCacheState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnaroundDataCacheStateCopyWith<$Res> {
  factory $TurnaroundDataCacheStateCopyWith(TurnaroundDataCacheState value,
          $Res Function(TurnaroundDataCacheState) then) =
      _$TurnaroundDataCacheStateCopyWithImpl<$Res, TurnaroundDataCacheState>;
}

/// @nodoc
class _$TurnaroundDataCacheStateCopyWithImpl<$Res,
        $Val extends TurnaroundDataCacheState>
    implements $TurnaroundDataCacheStateCopyWith<$Res> {
  _$TurnaroundDataCacheStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'TurnaroundDataCacheState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements TurnaroundDataCacheState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_ExecutionAndPlannerLoadingCopyWith<$Res> {
  factory _$$_ExecutionAndPlannerLoadingCopyWith(
          _$_ExecutionAndPlannerLoading value,
          $Res Function(_$_ExecutionAndPlannerLoading) then) =
      __$$_ExecutionAndPlannerLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExecutionAndPlannerLoadingCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res,
        _$_ExecutionAndPlannerLoading>
    implements _$$_ExecutionAndPlannerLoadingCopyWith<$Res> {
  __$$_ExecutionAndPlannerLoadingCopyWithImpl(
      _$_ExecutionAndPlannerLoading _value,
      $Res Function(_$_ExecutionAndPlannerLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ExecutionAndPlannerLoading implements _ExecutionAndPlannerLoading {
  const _$_ExecutionAndPlannerLoading();

  @override
  String toString() {
    return 'TurnaroundDataCacheState.executionAndPlannerLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExecutionAndPlannerLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return executionAndPlannerLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return executionAndPlannerLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (executionAndPlannerLoading != null) {
      return executionAndPlannerLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return executionAndPlannerLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return executionAndPlannerLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (executionAndPlannerLoading != null) {
      return executionAndPlannerLoading(this);
    }
    return orElse();
  }
}

abstract class _ExecutionAndPlannerLoading implements TurnaroundDataCacheState {
  const factory _ExecutionAndPlannerLoading() = _$_ExecutionAndPlannerLoading;
}

/// @nodoc
abstract class _$$_CacheSuccessCopyWith<$Res> {
  factory _$$_CacheSuccessCopyWith(
          _$_CacheSuccess value, $Res Function(_$_CacheSuccess) then) =
      __$$_CacheSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CacheSuccessCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res, _$_CacheSuccess>
    implements _$$_CacheSuccessCopyWith<$Res> {
  __$$_CacheSuccessCopyWithImpl(
      _$_CacheSuccess _value, $Res Function(_$_CacheSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CacheSuccess implements _CacheSuccess {
  const _$_CacheSuccess();

  @override
  String toString() {
    return 'TurnaroundDataCacheState.cacheSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CacheSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return cacheSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return cacheSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return cacheSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return cacheSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess(this);
    }
    return orElse();
  }
}

abstract class _CacheSuccess implements TurnaroundDataCacheState {
  const factory _CacheSuccess() = _$_CacheSuccess;
}

/// @nodoc
abstract class _$$_CacheFailureCopyWith<$Res> {
  factory _$$_CacheFailureCopyWith(
          _$_CacheFailure value, $Res Function(_$_CacheFailure) then) =
      __$$_CacheFailureCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_CacheFailureCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res, _$_CacheFailure>
    implements _$$_CacheFailureCopyWith<$Res> {
  __$$_CacheFailureCopyWithImpl(
      _$_CacheFailure _value, $Res Function(_$_CacheFailure) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_CacheFailure(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_CacheFailure implements _CacheFailure {
  const _$_CacheFailure({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'TurnaroundDataCacheState.cacheFailure(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CacheFailure &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CacheFailureCopyWith<_$_CacheFailure> get copyWith =>
      __$$_CacheFailureCopyWithImpl<_$_CacheFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return cacheFailure(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return cacheFailure?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (cacheFailure != null) {
      return cacheFailure(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return cacheFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return cacheFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (cacheFailure != null) {
      return cacheFailure(this);
    }
    return orElse();
  }
}

abstract class _CacheFailure implements TurnaroundDataCacheState {
  const factory _CacheFailure({required final String errorMsg}) =
      _$_CacheFailure;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_CacheFailureCopyWith<_$_CacheFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ExecutionDataErrorCopyWith<$Res> {
  factory _$$_ExecutionDataErrorCopyWith(_$_ExecutionDataError value,
          $Res Function(_$_ExecutionDataError) then) =
      __$$_ExecutionDataErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExecutionDataErrorCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res, _$_ExecutionDataError>
    implements _$$_ExecutionDataErrorCopyWith<$Res> {
  __$$_ExecutionDataErrorCopyWithImpl(
      _$_ExecutionDataError _value, $Res Function(_$_ExecutionDataError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ExecutionDataError implements _ExecutionDataError {
  const _$_ExecutionDataError();

  @override
  String toString() {
    return 'TurnaroundDataCacheState.executionDataError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ExecutionDataError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return executionDataError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return executionDataError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (executionDataError != null) {
      return executionDataError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return executionDataError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return executionDataError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (executionDataError != null) {
      return executionDataError(this);
    }
    return orElse();
  }
}

abstract class _ExecutionDataError implements TurnaroundDataCacheState {
  const factory _ExecutionDataError() = _$_ExecutionDataError;
}

/// @nodoc
abstract class _$$_PlannerDataErrorCopyWith<$Res> {
  factory _$$_PlannerDataErrorCopyWith(
          _$_PlannerDataError value, $Res Function(_$_PlannerDataError) then) =
      __$$_PlannerDataErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_PlannerDataErrorCopyWithImpl<$Res>
    extends _$TurnaroundDataCacheStateCopyWithImpl<$Res, _$_PlannerDataError>
    implements _$$_PlannerDataErrorCopyWith<$Res> {
  __$$_PlannerDataErrorCopyWithImpl(
      _$_PlannerDataError _value, $Res Function(_$_PlannerDataError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_PlannerDataError implements _PlannerDataError {
  const _$_PlannerDataError();

  @override
  String toString() {
    return 'TurnaroundDataCacheState.plannerDataError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_PlannerDataError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() executionAndPlannerLoading,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) cacheFailure,
    required TResult Function() executionDataError,
    required TResult Function() plannerDataError,
  }) {
    return plannerDataError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? executionAndPlannerLoading,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? cacheFailure,
    TResult? Function()? executionDataError,
    TResult? Function()? plannerDataError,
  }) {
    return plannerDataError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? executionAndPlannerLoading,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? cacheFailure,
    TResult Function()? executionDataError,
    TResult Function()? plannerDataError,
    required TResult orElse(),
  }) {
    if (plannerDataError != null) {
      return plannerDataError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_ExecutionAndPlannerLoading value)
        executionAndPlannerLoading,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_CacheFailure value) cacheFailure,
    required TResult Function(_ExecutionDataError value) executionDataError,
    required TResult Function(_PlannerDataError value) plannerDataError,
  }) {
    return plannerDataError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_CacheFailure value)? cacheFailure,
    TResult? Function(_ExecutionDataError value)? executionDataError,
    TResult? Function(_PlannerDataError value)? plannerDataError,
  }) {
    return plannerDataError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_ExecutionAndPlannerLoading value)?
        executionAndPlannerLoading,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_CacheFailure value)? cacheFailure,
    TResult Function(_ExecutionDataError value)? executionDataError,
    TResult Function(_PlannerDataError value)? plannerDataError,
    required TResult orElse(),
  }) {
    if (plannerDataError != null) {
      return plannerDataError(this);
    }
    return orElse();
  }
}

abstract class _PlannerDataError implements TurnaroundDataCacheState {
  const factory _PlannerDataError() = _$_PlannerDataError;
}
