// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'error_404_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Error_404Event {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDataAndRestart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? clearDataAndRestart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDataAndRestart,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ClearDataAndRestart value) clearDataAndRestart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ClearDataAndRestart value)? clearDataAndRestart,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ClearDataAndRestart value)? clearDataAndRestart,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Error_404EventCopyWith<$Res> {
  factory $Error_404EventCopyWith(
          Error_404Event value, $Res Function(Error_404Event) then) =
      _$Error_404EventCopyWithImpl<$Res, Error_404Event>;
}

/// @nodoc
class _$Error_404EventCopyWithImpl<$Res, $Val extends Error_404Event>
    implements $Error_404EventCopyWith<$Res> {
  _$Error_404EventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_ClearDataAndRestartCopyWith<$Res> {
  factory _$$_ClearDataAndRestartCopyWith(_$_ClearDataAndRestart value,
          $Res Function(_$_ClearDataAndRestart) then) =
      __$$_ClearDataAndRestartCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ClearDataAndRestartCopyWithImpl<$Res>
    extends _$Error_404EventCopyWithImpl<$Res, _$_ClearDataAndRestart>
    implements _$$_ClearDataAndRestartCopyWith<$Res> {
  __$$_ClearDataAndRestartCopyWithImpl(_$_ClearDataAndRestart _value,
      $Res Function(_$_ClearDataAndRestart) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ClearDataAndRestart implements _ClearDataAndRestart {
  const _$_ClearDataAndRestart();

  @override
  String toString() {
    return 'Error_404Event.clearDataAndRestart()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ClearDataAndRestart);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() clearDataAndRestart,
  }) {
    return clearDataAndRestart();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? clearDataAndRestart,
  }) {
    return clearDataAndRestart?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? clearDataAndRestart,
    required TResult orElse(),
  }) {
    if (clearDataAndRestart != null) {
      return clearDataAndRestart();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ClearDataAndRestart value) clearDataAndRestart,
  }) {
    return clearDataAndRestart(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ClearDataAndRestart value)? clearDataAndRestart,
  }) {
    return clearDataAndRestart?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ClearDataAndRestart value)? clearDataAndRestart,
    required TResult orElse(),
  }) {
    if (clearDataAndRestart != null) {
      return clearDataAndRestart(this);
    }
    return orElse();
  }
}

abstract class _ClearDataAndRestart implements Error_404Event {
  const factory _ClearDataAndRestart() = _$_ClearDataAndRestart;
}

/// @nodoc
mixin _$Error_404State {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Error_404StateCopyWith<$Res> {
  factory $Error_404StateCopyWith(
          Error_404State value, $Res Function(Error_404State) then) =
      _$Error_404StateCopyWithImpl<$Res, Error_404State>;
}

/// @nodoc
class _$Error_404StateCopyWithImpl<$Res, $Val extends Error_404State>
    implements $Error_404StateCopyWith<$Res> {
  _$Error_404StateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$Error_404StateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'Error_404State.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements Error_404State {
  const factory _Initial() = _$_Initial;
}
