// ignore_for_file: camel_case_types

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/logout_repository.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

part 'error_404_event.dart';
part 'error_404_state.dart';
part 'error_404_bloc.freezed.dart';

@injectable
class Error_404Bloc extends Bloc<Error_404Event, Error_404State> {
  final ILogoutRepository logoutRepository;
  Error_404Bloc(this.logoutRepository) : super(const _Initial()) {
    on<Error_404Event>((event, emit) async {
      await event.when(clearDataAndRestart: () async {
        await logoutRepository.clearData();
        jumpToLoginScreen(context: GlobalNavigationKey().context!, isSessionRestoration: false);
      });
    });
  }
}
