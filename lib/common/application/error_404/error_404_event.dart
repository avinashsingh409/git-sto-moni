// ignore_for_file: camel_case_types

part of 'error_404_bloc.dart';

@freezed
class Error_404Event with _$Error_404Event {
  const factory Error_404Event.clearDataAndRestart() = _ClearDataAndRestart;
}
