part of 'qr_code_scanner_bloc.dart';

@freezed
class QrCodeScannerState with _$QrCodeScannerState {
  const factory QrCodeScannerState.initial() = _Initial;
  const factory QrCodeScannerState.cacheSuccess() = _CacheSuccess;
  const factory QrCodeScannerState.error({required String errorMsg}) = _Error;
}
