part of 'qr_code_scanner_bloc.dart';

@freezed
class QrCodeScannerEvent with _$QrCodeScannerEvent {
  const factory QrCodeScannerEvent.saveQRCodeConfigs({required String? appConfigString}) = _SaveQRCodeConfigs;
}
