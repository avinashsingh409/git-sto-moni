import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_config_exception.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

part 'qr_code_scanner_event.dart';
part 'qr_code_scanner_state.dart';
part 'qr_code_scanner_bloc.freezed.dart';

@injectable
class QrCodeScannerBloc extends Bloc<QrCodeScannerEvent, QrCodeScannerState> {
  final IAppConfigRepository appConfigRepository;
  QrCodeScannerBloc(this.appConfigRepository) : super(const _Initial()) {
    on<QrCodeScannerEvent>((event, emit) async {
      await event.when(
        saveQRCodeConfigs: ((appConfigString) async {
          if (appConfigString != null) {
            await appConfigRepository.decodeAppConfig(appConfigString: appConfigString).fold((config) async {
              await appConfigRepository.deleteConfigs();
              await appConfigRepository.insertAppConfig(appConfig: config);
              emit(const QrCodeScannerState.cacheSuccess());
            }, (exception) {
              emit(
                QrCodeScannerState.error(
                  errorMsg: AppConfigException.getErrorMessage(exception),
                ),
              );
            });
          } else {
            emit(
              QrCodeScannerState.error(
                errorMsg: AppConfigException.getErrorMessage(
                  const AppConfigException.decodeError(),
                ),
              ),
            );
          }
        }),
      );
    });
  }
}
