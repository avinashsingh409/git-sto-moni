// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'qr_code_scanner_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$QrCodeScannerEvent {
  String? get appConfigString => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? appConfigString) saveQRCodeConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? appConfigString)? saveQRCodeConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? appConfigString)? saveQRCodeConfigs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SaveQRCodeConfigs value) saveQRCodeConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SaveQRCodeConfigs value)? saveQRCodeConfigs,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SaveQRCodeConfigs value)? saveQRCodeConfigs,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $QrCodeScannerEventCopyWith<QrCodeScannerEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QrCodeScannerEventCopyWith<$Res> {
  factory $QrCodeScannerEventCopyWith(
          QrCodeScannerEvent value, $Res Function(QrCodeScannerEvent) then) =
      _$QrCodeScannerEventCopyWithImpl<$Res, QrCodeScannerEvent>;
  @useResult
  $Res call({String? appConfigString});
}

/// @nodoc
class _$QrCodeScannerEventCopyWithImpl<$Res, $Val extends QrCodeScannerEvent>
    implements $QrCodeScannerEventCopyWith<$Res> {
  _$QrCodeScannerEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appConfigString = freezed,
  }) {
    return _then(_value.copyWith(
      appConfigString: freezed == appConfigString
          ? _value.appConfigString
          : appConfigString // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SaveQRCodeConfigsCopyWith<$Res>
    implements $QrCodeScannerEventCopyWith<$Res> {
  factory _$$_SaveQRCodeConfigsCopyWith(_$_SaveQRCodeConfigs value,
          $Res Function(_$_SaveQRCodeConfigs) then) =
      __$$_SaveQRCodeConfigsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? appConfigString});
}

/// @nodoc
class __$$_SaveQRCodeConfigsCopyWithImpl<$Res>
    extends _$QrCodeScannerEventCopyWithImpl<$Res, _$_SaveQRCodeConfigs>
    implements _$$_SaveQRCodeConfigsCopyWith<$Res> {
  __$$_SaveQRCodeConfigsCopyWithImpl(
      _$_SaveQRCodeConfigs _value, $Res Function(_$_SaveQRCodeConfigs) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appConfigString = freezed,
  }) {
    return _then(_$_SaveQRCodeConfigs(
      appConfigString: freezed == appConfigString
          ? _value.appConfigString
          : appConfigString // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_SaveQRCodeConfigs implements _SaveQRCodeConfigs {
  const _$_SaveQRCodeConfigs({required this.appConfigString});

  @override
  final String? appConfigString;

  @override
  String toString() {
    return 'QrCodeScannerEvent.saveQRCodeConfigs(appConfigString: $appConfigString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SaveQRCodeConfigs &&
            (identical(other.appConfigString, appConfigString) ||
                other.appConfigString == appConfigString));
  }

  @override
  int get hashCode => Object.hash(runtimeType, appConfigString);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SaveQRCodeConfigsCopyWith<_$_SaveQRCodeConfigs> get copyWith =>
      __$$_SaveQRCodeConfigsCopyWithImpl<_$_SaveQRCodeConfigs>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? appConfigString) saveQRCodeConfigs,
  }) {
    return saveQRCodeConfigs(appConfigString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? appConfigString)? saveQRCodeConfigs,
  }) {
    return saveQRCodeConfigs?.call(appConfigString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? appConfigString)? saveQRCodeConfigs,
    required TResult orElse(),
  }) {
    if (saveQRCodeConfigs != null) {
      return saveQRCodeConfigs(appConfigString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SaveQRCodeConfigs value) saveQRCodeConfigs,
  }) {
    return saveQRCodeConfigs(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SaveQRCodeConfigs value)? saveQRCodeConfigs,
  }) {
    return saveQRCodeConfigs?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SaveQRCodeConfigs value)? saveQRCodeConfigs,
    required TResult orElse(),
  }) {
    if (saveQRCodeConfigs != null) {
      return saveQRCodeConfigs(this);
    }
    return orElse();
  }
}

abstract class _SaveQRCodeConfigs implements QrCodeScannerEvent {
  const factory _SaveQRCodeConfigs({required final String? appConfigString}) =
      _$_SaveQRCodeConfigs;

  @override
  String? get appConfigString;
  @override
  @JsonKey(ignore: true)
  _$$_SaveQRCodeConfigsCopyWith<_$_SaveQRCodeConfigs> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$QrCodeScannerState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QrCodeScannerStateCopyWith<$Res> {
  factory $QrCodeScannerStateCopyWith(
          QrCodeScannerState value, $Res Function(QrCodeScannerState) then) =
      _$QrCodeScannerStateCopyWithImpl<$Res, QrCodeScannerState>;
}

/// @nodoc
class _$QrCodeScannerStateCopyWithImpl<$Res, $Val extends QrCodeScannerState>
    implements $QrCodeScannerStateCopyWith<$Res> {
  _$QrCodeScannerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$QrCodeScannerStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'QrCodeScannerState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements QrCodeScannerState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_CacheSuccessCopyWith<$Res> {
  factory _$$_CacheSuccessCopyWith(
          _$_CacheSuccess value, $Res Function(_$_CacheSuccess) then) =
      __$$_CacheSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CacheSuccessCopyWithImpl<$Res>
    extends _$QrCodeScannerStateCopyWithImpl<$Res, _$_CacheSuccess>
    implements _$$_CacheSuccessCopyWith<$Res> {
  __$$_CacheSuccessCopyWithImpl(
      _$_CacheSuccess _value, $Res Function(_$_CacheSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CacheSuccess implements _CacheSuccess {
  const _$_CacheSuccess();

  @override
  String toString() {
    return 'QrCodeScannerState.cacheSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CacheSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return cacheSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return cacheSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return cacheSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return cacheSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (cacheSuccess != null) {
      return cacheSuccess(this);
    }
    return orElse();
  }
}

abstract class _CacheSuccess implements QrCodeScannerState {
  const factory _CacheSuccess() = _$_CacheSuccess;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMsg});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$QrCodeScannerStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMsg = null,
  }) {
    return _then(_$_Error(
      errorMsg: null == errorMsg
          ? _value.errorMsg
          : errorMsg // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error({required this.errorMsg});

  @override
  final String errorMsg;

  @override
  String toString() {
    return 'QrCodeScannerState.error(errorMsg: $errorMsg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.errorMsg, errorMsg) ||
                other.errorMsg == errorMsg));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMsg);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() cacheSuccess,
    required TResult Function(String errorMsg) error,
  }) {
    return error(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? cacheSuccess,
    TResult? Function(String errorMsg)? error,
  }) {
    return error?.call(errorMsg);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? cacheSuccess,
    TResult Function(String errorMsg)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(errorMsg);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CacheSuccess value) cacheSuccess,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CacheSuccess value)? cacheSuccess,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CacheSuccess value)? cacheSuccess,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements QrCodeScannerState {
  const factory _Error({required final String errorMsg}) = _$_Error;

  String get errorMsg;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}
