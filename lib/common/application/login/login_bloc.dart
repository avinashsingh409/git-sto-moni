import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/network/network_info.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';
part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final ISessionRepository sessionRepository;
  final IAppConfigRepository appConfigRepository;
  final INetworkInfo networkInfo;
  LoginBloc(this.sessionRepository, this.appConfigRepository, this.networkInfo) : super(const _Initial()) {
    on<LoginEvent>((event, emit) async {
      await event.when(login: () async {
        emit(const LoginState.initial());
        final isConnected = await networkInfo.isConnected;
        if (isConnected) {
          final appConfig = appConfigRepository.getAppConfig();
          await appConfig.fold(
            (appConfig) async {
              final session = await sessionRepository.login(appConfig: appConfig);
              await session.fold(
                (session) async {
                  await sessionRepository.insertSession(session: session);
                  await Future.delayed(const Duration(seconds: 2));
                  emit(const LoginState.success());
                },
                (exception) {
                  emit(const LoginState.error());
                },
              );
            },
            (exception) {
              emit(const LoginState.error());
            },
          );
        } else {
          emit(const LoginState.notConnected());
        }
      });
    });
  }
}
