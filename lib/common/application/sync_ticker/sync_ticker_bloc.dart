import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/repository/sync_ticker_repository.dart';

part 'sync_ticker_event.dart';
part 'sync_ticker_state.dart';
part 'sync_ticker_bloc.freezed.dart';

@injectable
class SyncTickerBloc extends Bloc<SyncTickerEvent, SyncTickerState> {
  final ISyncTickerRepository syncTickerRepository;
  SyncTickerBloc(this.syncTickerRepository) : super(const _Initial()) {
    on<SyncTickerEvent>((event, emit) async {
      await event.when(getSyncStatus: () async {
        final statusBehaviorSubject = syncTickerRepository.isSyncPending();
        await emit.onEach(statusBehaviorSubject, onData: (isSyncPending) {
          if (isSyncPending) {
            emit(const SyncTickerState.syncPending());
          } else {
            emit(const SyncTickerState.noChanges());
          }
        });
      });
    });
    add(const SyncTickerEvent.getSyncStatus());
  }
}
