part of 'sync_ticker_bloc.dart';

@freezed
class SyncTickerEvent with _$SyncTickerEvent {
  const factory SyncTickerEvent.getSyncStatus() = _GetSyncStatus;
}
