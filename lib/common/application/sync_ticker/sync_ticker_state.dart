part of 'sync_ticker_bloc.dart';

@freezed
class SyncTickerState with _$SyncTickerState {
  const factory SyncTickerState.initial() = _Initial;
  const factory SyncTickerState.noChanges() = _NoChanges;
  const factory SyncTickerState.syncPending() = _SyncPending;
}
