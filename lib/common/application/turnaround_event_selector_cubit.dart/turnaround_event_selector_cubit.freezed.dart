// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_event_selector_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnaroundEventSelectorState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(TurnAroundEventDTO? turnAroundEvent)
        turnAroundEventChanged,
    required TResult Function(String event) selectedEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult? Function(String event)? selectedEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult Function(String event)? selectedEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_TurnAroundEventChanged value)
        turnAroundEventChanged,
    required TResult Function(_SelectedEvent value) selectedEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult? Function(_SelectedEvent value)? selectedEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult Function(_SelectedEvent value)? selectedEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnaroundEventSelectorStateCopyWith<$Res> {
  factory $TurnaroundEventSelectorStateCopyWith(
          TurnaroundEventSelectorState value,
          $Res Function(TurnaroundEventSelectorState) then) =
      _$TurnaroundEventSelectorStateCopyWithImpl<$Res,
          TurnaroundEventSelectorState>;
}

/// @nodoc
class _$TurnaroundEventSelectorStateCopyWithImpl<$Res,
        $Val extends TurnaroundEventSelectorState>
    implements $TurnaroundEventSelectorStateCopyWith<$Res> {
  _$TurnaroundEventSelectorStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$TurnaroundEventSelectorStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'TurnaroundEventSelectorState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(TurnAroundEventDTO? turnAroundEvent)
        turnAroundEventChanged,
    required TResult Function(String event) selectedEvent,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult? Function(String event)? selectedEvent,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult Function(String event)? selectedEvent,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_TurnAroundEventChanged value)
        turnAroundEventChanged,
    required TResult Function(_SelectedEvent value) selectedEvent,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult? Function(_SelectedEvent value)? selectedEvent,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult Function(_SelectedEvent value)? selectedEvent,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements TurnaroundEventSelectorState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$TurnaroundEventSelectorStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'TurnaroundEventSelectorState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(TurnAroundEventDTO? turnAroundEvent)
        turnAroundEventChanged,
    required TResult Function(String event) selectedEvent,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult? Function(String event)? selectedEvent,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult Function(String event)? selectedEvent,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_TurnAroundEventChanged value)
        turnAroundEventChanged,
    required TResult Function(_SelectedEvent value) selectedEvent,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult? Function(_SelectedEvent value)? selectedEvent,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult Function(_SelectedEvent value)? selectedEvent,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements TurnaroundEventSelectorState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_TurnAroundEventChangedCopyWith<$Res> {
  factory _$$_TurnAroundEventChangedCopyWith(_$_TurnAroundEventChanged value,
          $Res Function(_$_TurnAroundEventChanged) then) =
      __$$_TurnAroundEventChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({TurnAroundEventDTO? turnAroundEvent});
}

/// @nodoc
class __$$_TurnAroundEventChangedCopyWithImpl<$Res>
    extends _$TurnaroundEventSelectorStateCopyWithImpl<$Res,
        _$_TurnAroundEventChanged>
    implements _$$_TurnAroundEventChangedCopyWith<$Res> {
  __$$_TurnAroundEventChangedCopyWithImpl(_$_TurnAroundEventChanged _value,
      $Res Function(_$_TurnAroundEventChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? turnAroundEvent = freezed,
  }) {
    return _then(_$_TurnAroundEventChanged(
      turnAroundEvent: freezed == turnAroundEvent
          ? _value.turnAroundEvent
          : turnAroundEvent // ignore: cast_nullable_to_non_nullable
              as TurnAroundEventDTO?,
    ));
  }
}

/// @nodoc

class _$_TurnAroundEventChanged implements _TurnAroundEventChanged {
  const _$_TurnAroundEventChanged({required this.turnAroundEvent});

  @override
  final TurnAroundEventDTO? turnAroundEvent;

  @override
  String toString() {
    return 'TurnaroundEventSelectorState.turnAroundEventChanged(turnAroundEvent: $turnAroundEvent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TurnAroundEventChanged &&
            (identical(other.turnAroundEvent, turnAroundEvent) ||
                other.turnAroundEvent == turnAroundEvent));
  }

  @override
  int get hashCode => Object.hash(runtimeType, turnAroundEvent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TurnAroundEventChangedCopyWith<_$_TurnAroundEventChanged> get copyWith =>
      __$$_TurnAroundEventChangedCopyWithImpl<_$_TurnAroundEventChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(TurnAroundEventDTO? turnAroundEvent)
        turnAroundEventChanged,
    required TResult Function(String event) selectedEvent,
  }) {
    return turnAroundEventChanged(turnAroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult? Function(String event)? selectedEvent,
  }) {
    return turnAroundEventChanged?.call(turnAroundEvent);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult Function(String event)? selectedEvent,
    required TResult orElse(),
  }) {
    if (turnAroundEventChanged != null) {
      return turnAroundEventChanged(turnAroundEvent);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_TurnAroundEventChanged value)
        turnAroundEventChanged,
    required TResult Function(_SelectedEvent value) selectedEvent,
  }) {
    return turnAroundEventChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult? Function(_SelectedEvent value)? selectedEvent,
  }) {
    return turnAroundEventChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult Function(_SelectedEvent value)? selectedEvent,
    required TResult orElse(),
  }) {
    if (turnAroundEventChanged != null) {
      return turnAroundEventChanged(this);
    }
    return orElse();
  }
}

abstract class _TurnAroundEventChanged implements TurnaroundEventSelectorState {
  const factory _TurnAroundEventChanged(
          {required final TurnAroundEventDTO? turnAroundEvent}) =
      _$_TurnAroundEventChanged;

  TurnAroundEventDTO? get turnAroundEvent;
  @JsonKey(ignore: true)
  _$$_TurnAroundEventChangedCopyWith<_$_TurnAroundEventChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SelectedEventCopyWith<$Res> {
  factory _$$_SelectedEventCopyWith(
          _$_SelectedEvent value, $Res Function(_$_SelectedEvent) then) =
      __$$_SelectedEventCopyWithImpl<$Res>;
  @useResult
  $Res call({String event});
}

/// @nodoc
class __$$_SelectedEventCopyWithImpl<$Res>
    extends _$TurnaroundEventSelectorStateCopyWithImpl<$Res, _$_SelectedEvent>
    implements _$$_SelectedEventCopyWith<$Res> {
  __$$_SelectedEventCopyWithImpl(
      _$_SelectedEvent _value, $Res Function(_$_SelectedEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? event = null,
  }) {
    return _then(_$_SelectedEvent(
      event: null == event
          ? _value.event
          : event // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_SelectedEvent implements _SelectedEvent {
  const _$_SelectedEvent({required this.event});

  @override
  final String event;

  @override
  String toString() {
    return 'TurnaroundEventSelectorState.selectedEvent(event: $event)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SelectedEvent &&
            (identical(other.event, event) || other.event == event));
  }

  @override
  int get hashCode => Object.hash(runtimeType, event);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SelectedEventCopyWith<_$_SelectedEvent> get copyWith =>
      __$$_SelectedEventCopyWithImpl<_$_SelectedEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(TurnAroundEventDTO? turnAroundEvent)
        turnAroundEventChanged,
    required TResult Function(String event) selectedEvent,
  }) {
    return selectedEvent(event);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult? Function(String event)? selectedEvent,
  }) {
    return selectedEvent?.call(event);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(TurnAroundEventDTO? turnAroundEvent)?
        turnAroundEventChanged,
    TResult Function(String event)? selectedEvent,
    required TResult orElse(),
  }) {
    if (selectedEvent != null) {
      return selectedEvent(event);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_TurnAroundEventChanged value)
        turnAroundEventChanged,
    required TResult Function(_SelectedEvent value) selectedEvent,
  }) {
    return selectedEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult? Function(_SelectedEvent value)? selectedEvent,
  }) {
    return selectedEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_TurnAroundEventChanged value)? turnAroundEventChanged,
    TResult Function(_SelectedEvent value)? selectedEvent,
    required TResult orElse(),
  }) {
    if (selectedEvent != null) {
      return selectedEvent(this);
    }
    return orElse();
  }
}

abstract class _SelectedEvent implements TurnaroundEventSelectorState {
  const factory _SelectedEvent({required final String event}) =
      _$_SelectedEvent;

  String get event;
  @JsonKey(ignore: true)
  _$$_SelectedEventCopyWith<_$_SelectedEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
