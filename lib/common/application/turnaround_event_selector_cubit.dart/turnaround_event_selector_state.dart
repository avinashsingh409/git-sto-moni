part of 'turnaround_event_selector_cubit.dart';

@freezed
class TurnaroundEventSelectorState with _$TurnaroundEventSelectorState {
  const factory TurnaroundEventSelectorState.initial() = _Initial;
  const factory TurnaroundEventSelectorState.loading() = _Loading;
  const factory TurnaroundEventSelectorState.turnAroundEventChanged({required TurnAroundEventDTO? turnAroundEvent}) = _TurnAroundEventChanged;
  const factory TurnaroundEventSelectorState.selectedEvent({required String event}) = _SelectedEvent;
}
