import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';

import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';
part 'turnaround_event_selector_state.dart';
part 'turnaround_event_selector_cubit.freezed.dart';

@injectable
class TurnaroundEventSelectorCubit extends Cubit<TurnaroundEventSelectorState> {
  final ITurnAroundEventRepository turnAroundEventRepository;
  TurnAroundEventDTO? turnAroundEvent;
  bool isTurnAroundSelected = false;
  TurnaroundEventSelectorCubit(this.turnAroundEventRepository) : super(const TurnaroundEventSelectorState.initial());

  void initializeSelector() {
    emit(const TurnaroundEventSelectorState.loading());
    final selectedTurnAroundEventId = turnAroundEventRepository.getSelectedTurnAroundEventId();
    selectedTurnAroundEventId.fold(
      (selectedTurnAroundEventId) async {
        final event = await turnAroundEventRepository.getSelectedTurnAroundEvent(id: selectedTurnAroundEventId);
        if (event != null) {
          turnAroundEvent = TurnAroundEventDTO(
              id: selectedTurnAroundEventId,
              name: event.name ?? "",
              primaveraProjectId: event.primaveraProjectId ?? "",
              hostName: event.hostName ?? "",
              backwardsCommentsEnabled: event.backwardsCommentsEnabled ?? false,
              swipeAllowed: event.swipeAllowed ?? false,
              description: event.description ?? "",
              isManualProgress: event.isManualProgress,
              percentageRangeLowerBound: event.percentageRangeLowerBound,
              percentageRangeUpperBound: event.percentageRangeUpperBound,
              );
          emit(TurnaroundEventSelectorState.turnAroundEventChanged(turnAroundEvent: turnAroundEvent));
          isTurnAroundSelected = true;
        }
      },
      (_) {
        emit(TurnaroundEventSelectorState.turnAroundEventChanged(turnAroundEvent: turnAroundEvent));
      },
    );
  }

  void changeTurnAroundEvent({required TurnAroundEventDTO selectedTurnaroundEventEvent}) async {
    emit(const TurnaroundEventSelectorState.loading());
    turnAroundEvent = selectedTurnaroundEventEvent;
    isTurnAroundSelected = true;
    emit(TurnaroundEventSelectorState.turnAroundEventChanged(turnAroundEvent: turnAroundEvent));
  }

  void getCurrentSelectedTurnaroundEvent() async {
    emit(const TurnaroundEventSelectorState.loading());
    final selectedEventId = turnAroundEventRepository.getSelectedTurnAroundEventId();
    await selectedEventId.fold(
      (id) async {
        final event = await turnAroundEventRepository.getSelectedTurnAroundEvent(id: id);
        emit(TurnaroundEventSelectorState.selectedEvent(event: event?.name ?? ""));
      }, 
      (_) {
        emit(const TurnaroundEventSelectorState.selectedEvent(event: ""));
      }
    );
  }
}
