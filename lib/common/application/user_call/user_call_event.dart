part of 'user_call_bloc.dart';

@freezed
class UserCallEvent with _$UserCallEvent {
  const factory UserCallEvent.getUser() = _GetUser;
}
