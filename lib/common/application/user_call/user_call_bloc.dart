import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/user_call_exception.dart';
import 'package:sto_mobile_v2/common/domain/user_call/repository/user_call_repository.dart';

part 'user_call_event.dart';
part 'user_call_state.dart';
part 'user_call_bloc.freezed.dart';

@injectable
class UserCallBloc extends Bloc<UserCallEvent, UserCallState> {
  final IUserCallRepository userCallRepository;
  UserCallBloc(this.userCallRepository) : super(const _Initial()) {
    on<UserCallEvent>((event, emit) async {
      await event.when(getUser: () async {
        emit(const UserCallState.loading());
        final userResponse = await userCallRepository.getAndCacheUserCall();
        await userResponse.fold(
          (_) async => emit(const UserCallState.success()),
          (exception) async => emit(UserCallState.error(errmsg: UserCallException.getErrorMessage(exception))),
        );
      });
    });
    add(const UserCallEvent.getUser());
  }
}
