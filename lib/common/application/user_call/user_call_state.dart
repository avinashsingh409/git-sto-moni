part of 'user_call_bloc.dart';

@freezed
class UserCallState with _$UserCallState {
  const factory UserCallState.initial() = _Initial;
  const factory UserCallState.loading() = _Loading;
  const factory UserCallState.success() = _Success;
  const factory UserCallState.error({required String errmsg}) = _Error;
}
