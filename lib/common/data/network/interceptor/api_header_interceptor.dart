import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
// ignore: depend_on_referenced_packages
import 'package:http/http.dart' as http;
import 'package:openid_client/openid_client_io.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/refresh_token_request.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/logout_repository.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';

@injectable
class ApiHeadersInterceptor extends Interceptor {
  final ISessionRepository sessionRepository;
  final IAppConfigRepository appConfigRepository;

  ILogoutRepository get _logoutRepository => getIt<ILogoutRepository>();
  ISessionRepository get _sessionRepository => getIt<ISessionRepository>();

  ApiHeadersInterceptor(this.sessionRepository, this.appConfigRepository);

  @override
  Future onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final session = sessionRepository.getSession();
    await session.fold((session) async {
      final bool isTokenExpired = JwtDecoder.isExpired(session.accessToken!);
      if (isTokenExpired) {
        final appConfig = appConfigRepository.getAppConfig();
        await appConfig.fold(
          (appConfig) async {
            final String refreshUrl = "${appConfig.authenticationUrl}/protocol/openid-connect/token";
            try {
              final response = await http.post(Uri.parse(refreshUrl),
                  headers: {"Content-Type": "application/x-www-form-urlencoded"},
                  encoding: Encoding.getByName('utf-8'),
                  body: RefreshTokenRequest(
                          clientId: appConfig.mobileClientId,
                          grantType: 'refresh_token',
                          refreshToken: session.refreshToken ?? '')
                      .toJson());
              final tokenResponse = TokenResponse.fromJson(jsonDecode(response.body));
              final newSession = Session(
                  accessToken: tokenResponse.accessToken,
                  expiresAt: tokenResponse.expiresAt,
                  refreshToken: tokenResponse.refreshToken,
                  tokenType: tokenResponse.tokenType,
                  userInformation: session.userInformation);
              await _sessionRepository.insertSession(session: newSession);
              _addHeader(options: options, accessToken: newSession.accessToken!);
            } on HttpException {
              jumpToLoginScreen(context: GlobalNavigationKey().context!, isSessionRestoration: true);
            } on Exception catch (_) {}
          },
          (exception) async {
            await _logoutRepository.clearData();
            jumpToServerCredentialsScreen(context: GlobalNavigationKey().context!);
          },
        );
      } else {
        _addHeader(options: options, accessToken: session.accessToken!);
      }
    }, (_) async {});

    super.onRequest(options, handler);
  }

  void _addHeader({required RequestOptions options, required String accessToken}) {
    options.headers.addAll({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $accessToken',
    });
  }
}
