import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/repository/app_logs_repository.dart';

@injectable
class ExceptionInterceptor extends Interceptor {
  final IAppLogsRepository appLogsRepository;

  ExceptionInterceptor(this.appLogsRepository);

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    await appLogsRepository.upsertAppLog(
      appLog: AppLogsDTO(
        exceptionTime: DateTime.now().add(const Duration(milliseconds: 1)),
        exceptionString: err.toString(),
      ),
    );
    handler.next(err);
  }
}
