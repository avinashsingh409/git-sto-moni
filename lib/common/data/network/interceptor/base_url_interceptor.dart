import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

@injectable
class BaseUrlInterceptor extends Interceptor {
  final IAppConfigRepository appConfigRepository;

  BaseUrlInterceptor(this.appConfigRepository);
  @override
  Future onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final appConfig = appConfigRepository.getAppConfig();
    appConfig.fold(
      (appConfig) {
        options.baseUrl = appConfig.baseUrl;
      },
      (exception) {
        options.baseUrl = '';
      },
    );
    super.onRequest(options, handler);
  }
}
