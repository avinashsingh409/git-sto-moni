import 'dart:async';
import 'dart:io';

import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/repository/app_logs_repository.dart';
import 'package:sto_mobile_v2/common/domain/network/network_info.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

// ignore: depend_on_referenced_packages
import 'package:http/http.dart' as http;
import 'package:sto_mobile_v2/injectable/injection.dart';

@LazySingleton(as: INetworkInfo)
class NetworkInfoImpl implements INetworkInfo {
  IAppConfigRepository get _appConfigRepository => getIt<IAppConfigRepository>();
  //
  IAppLogsRepository get _appLogRepository => getIt<IAppLogsRepository>();

  @override
  Future<bool> get isConnected async {
    late bool connectionIdentity;
    final appConfig = _appConfigRepository.getAppConfig();
    await appConfig.fold((config) async {
      try {
        await http.get(Uri.parse("${config.baseUrl}/${ApiEndPoints.coreHealthCheck}")).timeout(
              const Duration(seconds: 5),
              onTimeout: () => Future.error(const SocketException('Connection Timeout')),
            );
        connectionIdentity = true;
      } on ConnectionException catch (err) {
        _appLogRepository.upsertAppLog(appLog: AppLogsDTO(exceptionString: err.toString(), exceptionTime: DateTime.now()));
        connectionIdentity = false;
      } on SocketException catch (err) {
        _appLogRepository.upsertAppLog(appLog: AppLogsDTO(exceptionString: err.toString(), exceptionTime: DateTime.now()));
        connectionIdentity = false;
      } on http.ClientException catch (err) {
        _appLogRepository.upsertAppLog(appLog: AppLogsDTO(exceptionString: err.toString(), exceptionTime: DateTime.now()));
      } on TimeoutException catch (err) {
        _appLogRepository.upsertAppLog(appLog: AppLogsDTO(exceptionString: err.toString(), exceptionTime: DateTime.now()));
        connectionIdentity = false;
      } on Exception catch (err) {
        _appLogRepository.upsertAppLog(appLog: AppLogsDTO(exceptionString: err.toString(), exceptionTime: DateTime.now()));
        connectionIdentity = false;
      }
    }, (_) async {
      connectionIdentity = false;
    });
    return connectionIdentity;
  }
}
