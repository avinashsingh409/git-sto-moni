import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';

import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class AppConfigDAO {
  final isar = PersistenceModule.isar;

  AppConfigDAO();

  /// Isar Collection of Configs
  late final IsarCollection<AppConfigDTO> _collection = isar.appConfigDTOs;

  /// Get Configs
  AppConfigDTO? getConfigs() {
    final appConfigDTO = _collection.getSync(0);
    return appConfigDTO;
  }

  // Upsert Configs
  Future<int> upsertSettings({required AppConfigDTO appConfigDTO}) async {
    return isar.writeTxn(() {
      return _collection.put(appConfigDTO);
    });
  }

  // Delete Configs
  Future<void> deleteConfigs() async {
    return isar.writeTxn(() async {
      return _collection.clear();
    });
  }
}
