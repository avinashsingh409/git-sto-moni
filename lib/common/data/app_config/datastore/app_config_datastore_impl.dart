import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/app_config_dao.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';

import 'package:sto_mobile_v2/common/domain/app_config/datastore/app_config_datastore.dart';

@LazySingleton(as: IAppConfigDataStore)
class AppConfigDataStoreImpl extends IAppConfigDataStore {
  final AppConfigDAO appConfigDao;

  AppConfigDataStoreImpl(this.appConfigDao);

  @override
  AppConfigDTO? getAppConfig() {
    return appConfigDao.getConfigs();
  }

  @override
  Future<void> insertAppConfig({required AppConfigDTO appConfigDTO}) {
    return appConfigDao.upsertSettings(appConfigDTO: appConfigDTO);
  }

  @override
  Future<void> deleteConfigs() {
    return appConfigDao.deleteConfigs();
  }
}
