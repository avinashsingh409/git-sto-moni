import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';

part 'app_config.freezed.dart';
part 'app_config.g.dart';

@freezed
abstract class AppConfig with _$AppConfig {
  const factory AppConfig({
    required String authenticationUrl,
    required String baseUrl,
    required String mobileClientId,
  }) = _AppConfig;

  factory AppConfig.fromJson(Map<String, dynamic> json) => _$AppConfigFromJson(json);

  factory AppConfig.fromDTO({required AppConfigDTO dto}) {
    final appConfig =
        AppConfig(authenticationUrl: dto.authenticationUrl, baseUrl: dto.baseUrl, mobileClientId: dto.mobileClientId);
    return appConfig;
  }
}
