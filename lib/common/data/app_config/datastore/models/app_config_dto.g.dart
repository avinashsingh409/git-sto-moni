// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_config_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetAppConfigDTOCollection on Isar {
  IsarCollection<AppConfigDTO> get appConfigDTOs => this.collection();
}

const AppConfigDTOSchema = CollectionSchema(
  name: r'AppConfigDTO',
  id: -472148424598839500,
  properties: {
    r'authenticationUrl': PropertySchema(
      id: 0,
      name: r'authenticationUrl',
      type: IsarType.string,
    ),
    r'baseUrl': PropertySchema(
      id: 1,
      name: r'baseUrl',
      type: IsarType.string,
    ),
    r'mobileClientId': PropertySchema(
      id: 2,
      name: r'mobileClientId',
      type: IsarType.string,
    )
  },
  estimateSize: _appConfigDTOEstimateSize,
  serialize: _appConfigDTOSerialize,
  deserialize: _appConfigDTODeserialize,
  deserializeProp: _appConfigDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _appConfigDTOGetId,
  getLinks: _appConfigDTOGetLinks,
  attach: _appConfigDTOAttach,
  version: '3.1.0+1',
);

int _appConfigDTOEstimateSize(
  AppConfigDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.authenticationUrl.length * 3;
  bytesCount += 3 + object.baseUrl.length * 3;
  bytesCount += 3 + object.mobileClientId.length * 3;
  return bytesCount;
}

void _appConfigDTOSerialize(
  AppConfigDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.authenticationUrl);
  writer.writeString(offsets[1], object.baseUrl);
  writer.writeString(offsets[2], object.mobileClientId);
}

AppConfigDTO _appConfigDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = AppConfigDTO(
    authenticationUrl: reader.readString(offsets[0]),
    baseUrl: reader.readString(offsets[1]),
    mobileClientId: reader.readString(offsets[2]),
  );
  object.id = id;
  return object;
}

P _appConfigDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readString(offset)) as P;
    case 1:
      return (reader.readString(offset)) as P;
    case 2:
      return (reader.readString(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _appConfigDTOGetId(AppConfigDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _appConfigDTOGetLinks(AppConfigDTO object) {
  return [];
}

void _appConfigDTOAttach(
    IsarCollection<dynamic> col, Id id, AppConfigDTO object) {
  object.id = id;
}

extension AppConfigDTOQueryWhereSort
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QWhere> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension AppConfigDTOQueryWhere
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QWhereClause> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AppConfigDTOQueryFilter
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QFilterCondition> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'authenticationUrl',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'authenticationUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'authenticationUrl',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'authenticationUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      authenticationUrlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'authenticationUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'baseUrl',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'baseUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'baseUrl',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'baseUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      baseUrlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'baseUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'mobileClientId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'mobileClientId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'mobileClientId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'mobileClientId',
        value: '',
      ));
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterFilterCondition>
      mobileClientIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'mobileClientId',
        value: '',
      ));
    });
  }
}

extension AppConfigDTOQueryObject
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QFilterCondition> {}

extension AppConfigDTOQueryLinks
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QFilterCondition> {}

extension AppConfigDTOQuerySortBy
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QSortBy> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      sortByAuthenticationUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authenticationUrl', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      sortByAuthenticationUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authenticationUrl', Sort.desc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> sortByBaseUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseUrl', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> sortByBaseUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseUrl', Sort.desc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      sortByMobileClientId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mobileClientId', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      sortByMobileClientIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mobileClientId', Sort.desc);
    });
  }
}

extension AppConfigDTOQuerySortThenBy
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QSortThenBy> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      thenByAuthenticationUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authenticationUrl', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      thenByAuthenticationUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'authenticationUrl', Sort.desc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> thenByBaseUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseUrl', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> thenByBaseUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'baseUrl', Sort.desc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      thenByMobileClientId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mobileClientId', Sort.asc);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QAfterSortBy>
      thenByMobileClientIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'mobileClientId', Sort.desc);
    });
  }
}

extension AppConfigDTOQueryWhereDistinct
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QDistinct> {
  QueryBuilder<AppConfigDTO, AppConfigDTO, QDistinct>
      distinctByAuthenticationUrl({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'authenticationUrl',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QDistinct> distinctByBaseUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'baseUrl', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AppConfigDTO, AppConfigDTO, QDistinct> distinctByMobileClientId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'mobileClientId',
          caseSensitive: caseSensitive);
    });
  }
}

extension AppConfigDTOQueryProperty
    on QueryBuilder<AppConfigDTO, AppConfigDTO, QQueryProperty> {
  QueryBuilder<AppConfigDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<AppConfigDTO, String, QQueryOperations>
      authenticationUrlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'authenticationUrl');
    });
  }

  QueryBuilder<AppConfigDTO, String, QQueryOperations> baseUrlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'baseUrl');
    });
  }

  QueryBuilder<AppConfigDTO, String, QQueryOperations>
      mobileClientIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'mobileClientId');
    });
  }
}
