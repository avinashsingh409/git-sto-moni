// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppConfig _$$_AppConfigFromJson(Map<String, dynamic> json) => _$_AppConfig(
      authenticationUrl: json['authenticationUrl'] as String,
      baseUrl: json['baseUrl'] as String,
      mobileClientId: json['mobileClientId'] as String,
    );

Map<String, dynamic> _$$_AppConfigToJson(_$_AppConfig instance) =>
    <String, dynamic>{
      'authenticationUrl': instance.authenticationUrl,
      'baseUrl': instance.baseUrl,
      'mobileClientId': instance.mobileClientId,
    };
