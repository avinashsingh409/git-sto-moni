import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';

part 'app_config_dto.g.dart';

@collection
class AppConfigDTO {
  final String authenticationUrl;
  final String baseUrl;
  final String mobileClientId;
  Id id = 0;

  AppConfigDTO({required this.authenticationUrl, required this.baseUrl, required this.mobileClientId});

  factory AppConfigDTO.fromModel({required AppConfig appConfig}) {
    final appConfigDTO = AppConfigDTO(
        authenticationUrl: appConfig.authenticationUrl, baseUrl: appConfig.baseUrl, mobileClientId: appConfig.mobileClientId);
    return appConfigDTO;
  }
}
