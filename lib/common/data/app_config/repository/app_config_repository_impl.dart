import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_config_exception.dart';
import 'package:sto_mobile_v2/common/domain/app_config/datastore/app_config_datastore.dart';
import 'package:sto_mobile_v2/common/domain/app_config/repository/app_config_repository.dart';

@LazySingleton(as: IAppConfigRepository)
class AppConfigRepositoryImpl extends IAppConfigRepository {
  final IAppConfigDataStore appConfigDataStore;

  AppConfigRepositoryImpl(this.appConfigDataStore);

  @override
  Either<AppConfig, AppConfigException> getAppConfig() {
    final appConfigDTO = appConfigDataStore.getAppConfig();
    if (appConfigDTO != null) {
      final appConfig = AppConfig.fromDTO(dto: appConfigDTO);
      return Left(appConfig);
    } else {
      return const Right(AppConfigException.notConfigured());
    }
  }

  @override
  Future<void> insertAppConfig({required AppConfig appConfig}) async {
    final appConfigDTO = AppConfigDTO.fromModel(appConfig: appConfig);
    await appConfigDataStore.insertAppConfig(appConfigDTO: appConfigDTO);
  }

  @override
  Future<void> deleteConfigs() async {
    await appConfigDataStore.deleteConfigs();
  }

  @override
  Either<AppConfig, AppConfigException> decodeAppConfig({required String appConfigString}) {
    try {
      final decodedAppConfigString = json.decode(appConfigString);
      final appConfig = AppConfig.fromJson(decodedAppConfigString);
      return Left(appConfig);
    } on Exception {
      return const Right(AppConfigException.decodeError());
    }
  }
}
