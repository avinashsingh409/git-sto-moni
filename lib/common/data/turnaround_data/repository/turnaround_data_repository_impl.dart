import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_activity_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_data_exception.dart';
import 'package:sto_mobile_v2/common/domain/license/repository/license_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_data/repository/turnaround_data_repository.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/repository/filter_config_repository.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/repository/workpackage_repository.dart';

@LazySingleton(as: ITurnaroundDataRepository)
class TurnaroundDataRepositoryImpl extends ITurnaroundDataRepository {
  final ILicenseRepository licenseRepository;
  final ITurnAroundActivityRepository turnAroundActivityRepository;
  final IWorkpackageRepository workpackageRepository;
  final ITurnAroundEventRepository turnAroundEventRepository;
  final IFilterConfigRepository filterConfigRepository;
  TurnaroundDataRepositoryImpl(this.turnAroundActivityRepository, this.workpackageRepository, this.turnAroundEventRepository,
      this.licenseRepository, this.filterConfigRepository);

  @override
  Future<Either<Unit, TurnAroundDataException>> getAllTurnaroundData() async {
    //Todo manage exception for api call failures.
    bool didPlannerFail = false;
    bool isP6ProjectFailure = false;
    bool didExecutionFail = false;
    final licenseCache = await licenseRepository.getAndCacheServiceLicense();
    await licenseCache.fold((license) async {
      final selectedTurnAroundEventId = turnAroundEventRepository.getSelectedTurnAroundEventId();
      await selectedTurnAroundEventId.fold((turnaroundEventId) async {
        final selectedTurnAroundEvent = await turnAroundEventRepository.getSelectedTurnAroundEvent(id: turnaroundEventId);

        //Checking license for planner
        if (license.planner != null && license.planner!) {
          final plannerDataStatus = await workpackageRepository.getAndCachePlannerData(turnaroundEventId: turnaroundEventId);
          plannerDataStatus.fold((_) {}, (exception) {
            didPlannerFail = true;
          });
        }
        //Checking license for execution
        if (license.execution != null && license.execution!) {
          final executionStatus = await turnAroundActivityRepository.getAndCacheExecutionData(
              turnAroundEventDTO: selectedTurnAroundEvent ?? TurnAroundEventDTO());

          executionStatus.fold((_) {}, (exception) {
            if (exception == const TurnAroundActivityException.noP6ProjectAssigned()) {
              isP6ProjectFailure = true;
            }
            didExecutionFail = true;
          });
        }
        //
      }, (turnaroundEventIdException) async {
        didPlannerFail = true;
      });

      //License api exception
    }, (licenseException) async {
      didPlannerFail = true;
    });

    if (didPlannerFail) {
      return const Right(TurnAroundDataException.plannerDataFailure());
    } else if (didExecutionFail && isP6ProjectFailure) {
      return const Right(TurnAroundDataException.noP6ProjectAssigned());
    } else if (didExecutionFail) {
      return const Right(TurnAroundDataException.executionDataFailure());
    } else {
      return const Left(unit);
    }
  }
}
