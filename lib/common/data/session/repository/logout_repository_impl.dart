import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/domain/exception/logout_exception.dart';
import 'package:sto_mobile_v2/common/domain/session/datastore/logout_datastore.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/logout_repository.dart';
import 'package:sto_mobile_v2/execution/domain/done_for_the_day/repository/done_for_the_day_repository.dart';

@LazySingleton(as: ILogoutRepository)
class LogoutRepositoryImpl extends ILogoutRepository {
  final ILogoutDataStore logoutDataStore;
  final IDoneForTheDayRepository doneForTheDayRepository;

  LogoutRepositoryImpl(this.logoutDataStore, this.doneForTheDayRepository);

  @override
  Future<Either<Unit, LogoutException>> logout({required AppConfig appConfig}) async {
    try {
      await logoutDataStore.clearData();
      doneForTheDayRepository.resetDoneForTheDay();
      return const Left(unit);
    } on Exception catch (_) {
      return const Right(LogoutException.unexpectedError());
    }
  }

  @override
  Future<void> clearData() {
    return logoutDataStore.clearData();
  }
}
