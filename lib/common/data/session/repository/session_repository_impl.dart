import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';
import 'package:sto_mobile_v2/common/domain/exception/repository/app_logs_repository.dart';
import 'package:sto_mobile_v2/common/domain/exception/session_exception.dart';

import 'package:sto_mobile_v2/common/domain/session/datastore/session_datastore.dart';
import 'package:sto_mobile_v2/common/domain/session/repository/session_repository.dart';

@LazySingleton(as: ISessionRepository)
class SessionRepositoryImpl extends ISessionRepository {
  final ISessionDataStore sessionDataStore;
  final IAppLogsRepository appLogsRepository;

  SessionRepositoryImpl(this.sessionDataStore, this.appLogsRepository);

  @override
  Future<Either<Session, SessionException>> login({required AppConfig appConfig}) async {
    try {
      final tokenResponse = await sessionDataStore.login(appConfig: appConfig);
      final session = Session(
        accessToken: tokenResponse.accessToken,
        tokenType: tokenResponse.tokenType,
        refreshToken: tokenResponse.refreshToken,
        expiresAt: tokenResponse.expiresAt,
        userInformation: UserInformation.fromJson(
          tokenResponse.idToken.claims.toJson(),
        ),
      );
      return Left(session);
    } on Exception catch (err) {
      await appLogsRepository.upsertAppLog(
        appLog: AppLogsDTO(
          exceptionTime: DateTime.now().add(const Duration(milliseconds: 1)),
          exceptionString: err.toString(),
        ),
      );
      return const Right(SessionException.unexpectedError());
    }
  }

  @override
  Future<void> insertSession({required Session session}) async {
    //Clearing session for duplicate session triggers
    await sessionDataStore.deleteSession();
    final sessionDTO = SessionDTO.fromModel(session: session);
    return await sessionDataStore.insertSession(sessionDTO: sessionDTO);
  }

  @override
  Either<Session, Unit> getSession() {
    final sessionDTO = sessionDataStore.getSession();
    if (sessionDTO != null) {
      final session = Session.fromDTO(dto: sessionDTO);
      return Left(session);
    } else {
      return const Right(unit);
    }
  }
}
