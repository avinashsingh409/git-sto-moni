import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';

import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class SessionDAO {
  final isar = PersistenceModule.isar;

  SessionDAO();

  /// Isar Collection of Session
  late final IsarCollection<SessionDTO> _sessionCollection = isar.sessionDTOs;

  /// Get Session
  SessionDTO? getSession() {
    final sessionDTO = _sessionCollection.getSync(0);
    return sessionDTO;
  }

  // Upsert Session
  Future<int> upsertSession({required SessionDTO sessionDTO}) async {
    return isar.writeTxn(() {
      return _sessionCollection.put(sessionDTO);
    });
  }

  // Delete Session
  Future<void> deleteSession() async {
    return isar.writeTxn(() async {
      return _sessionCollection.clear();
    });
  }
}
