// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_token_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RefreshTokenRequest _$$_RefreshTokenRequestFromJson(
        Map<String, dynamic> json) =>
    _$_RefreshTokenRequest(
      clientId: json['client_id'] as String,
      grantType: json['grant_type'] as String,
      refreshToken: json['refresh_token'] as String,
    );

Map<String, dynamic> _$$_RefreshTokenRequestToJson(
        _$_RefreshTokenRequest instance) =>
    <String, dynamic>{
      'client_id': instance.clientId,
      'grant_type': instance.grantType,
      'refresh_token': instance.refreshToken,
    };
