import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information_dto.dart';

part 'session_dto.g.dart';

@collection
class SessionDTO {
  Id id = 0;
  String? accessToken;
  String? tokenType;
  String? refreshToken;
  DateTime? expiresAt;
  UserInformationDTO? userInformation;

  SessionDTO({
    this.accessToken,
    this.tokenType,
    this.refreshToken,
    this.expiresAt,
    this.userInformation,
  });

  factory SessionDTO.fromModel({required Session session}) {
    final sessionDTO = SessionDTO(
      accessToken: session.accessToken,
      tokenType: session.tokenType,
      refreshToken: session.refreshToken,
      expiresAt: session.expiresAt,
      userInformation: UserInformationDTO.fromModel(session.userInformation),
    );
    return sessionDTO;
  }
}
