import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';

part 'session.freezed.dart';
part 'session.g.dart';

@freezed
abstract class Session with _$Session {
  const factory Session({
    required String? accessToken,
    required String? tokenType,
    required String? refreshToken,
    required DateTime? expiresAt,
    required UserInformation userInformation,
  }) = _Session;

  factory Session.fromJson(Map<String, dynamic> json) => _$SessionFromJson(json);

  factory Session.fromDTO({required SessionDTO dto}) {
    final session = Session(
      accessToken: dto.accessToken,
      tokenType: dto.tokenType,
      refreshToken: dto.refreshToken,
      expiresAt: dto.expiresAt,
      userInformation: dto.userInformation!.toModel(),
    );

    return session;
  }
}
