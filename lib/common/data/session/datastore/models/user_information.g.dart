// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_information.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserInformation _$$_UserInformationFromJson(Map<String, dynamic> json) =>
    _$_UserInformation(
      sub: json['sub'] as String?,
      email: json['email'] as String?,
      name: json['name'] as String?,
      preferredUsername: json['preferred_username'] as String?,
      givenName: json['given_name'] as String?,
      familyName: json['family_name'] as String?,
    );

Map<String, dynamic> _$$_UserInformationToJson(_$_UserInformation instance) =>
    <String, dynamic>{
      'sub': instance.sub,
      'email': instance.email,
      'name': instance.name,
      'preferred_username': instance.preferredUsername,
      'given_name': instance.givenName,
      'family_name': instance.familyName,
    };
