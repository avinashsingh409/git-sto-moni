// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information_dto.dart';

part 'user_information.freezed.dart';
part 'user_information.g.dart';

@freezed
abstract class UserInformation with _$UserInformation {
  const factory UserInformation({
    required String? sub,
    required String? email,
    required String? name,
    @JsonKey(name: "preferred_username") required String? preferredUsername,
    @JsonKey(name: "given_name") required String? givenName,
    @JsonKey(name: "family_name") required String? familyName,
  }) = _UserInformation;

  factory UserInformation.fromJson(Map<String, dynamic> json) => _$UserInformationFromJson(json);

  factory UserInformation.fromDTO({required UserInformationDTO dto}) {
    final userInformation = UserInformation(
        sub: dto.sub,
        email: dto.email,
        name: dto.name,
        preferredUsername: dto.preferredUsername,
        givenName: dto.givenName,
        familyName: dto.familyName);
    return userInformation;
  }
}
