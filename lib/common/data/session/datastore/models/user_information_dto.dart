import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/user_information.dart';

part 'user_information_dto.g.dart';

@embedded
@JsonSerializable()
class UserInformationDTO {
  String? sub;
  String? email;
  String? name;
  @JsonKey(name: "preferred_username")
  String? preferredUsername;
  @JsonKey(name: "given_name")
  String? givenName;
  @JsonKey(name: "family_name")
  String? familyName;

  UserInformationDTO({
    this.sub,
    this.email,
    this.name,
    this.preferredUsername,
    this.givenName,
    this.familyName,
  });

  factory UserInformationDTO.fromJson(Map<String, dynamic> json) => _$UserInformationDTOFromJson(json);

  /// Convert UserInformation to JSON
  Map<String, dynamic> toJson() => _$UserInformationDTOToJson(this);

  /// Convert from Userinformation to UserInformationDTO
  factory UserInformationDTO.fromModel(UserInformation userInformation) {
    return UserInformationDTO(
      sub: userInformation.sub,
      email: userInformation.email,
      name: userInformation.name,
      preferredUsername: userInformation.preferredUsername,
      givenName: userInformation.givenName,
      familyName: userInformation.familyName,
    );
  }

  UserInformation toModel() {
    return UserInformation(
        email: email ?? "",
        familyName: familyName ?? "",
        givenName: givenName ?? "",
        name: name ?? "",
        preferredUsername: preferredUsername ?? "",
        sub: sub ?? "");
  }
}
