// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_information.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserInformation _$UserInformationFromJson(Map<String, dynamic> json) {
  return _UserInformation.fromJson(json);
}

/// @nodoc
mixin _$UserInformation {
  String? get sub => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: "preferred_username")
  String? get preferredUsername => throw _privateConstructorUsedError;
  @JsonKey(name: "given_name")
  String? get givenName => throw _privateConstructorUsedError;
  @JsonKey(name: "family_name")
  String? get familyName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserInformationCopyWith<UserInformation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInformationCopyWith<$Res> {
  factory $UserInformationCopyWith(
          UserInformation value, $Res Function(UserInformation) then) =
      _$UserInformationCopyWithImpl<$Res, UserInformation>;
  @useResult
  $Res call(
      {String? sub,
      String? email,
      String? name,
      @JsonKey(name: "preferred_username") String? preferredUsername,
      @JsonKey(name: "given_name") String? givenName,
      @JsonKey(name: "family_name") String? familyName});
}

/// @nodoc
class _$UserInformationCopyWithImpl<$Res, $Val extends UserInformation>
    implements $UserInformationCopyWith<$Res> {
  _$UserInformationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sub = freezed,
    Object? email = freezed,
    Object? name = freezed,
    Object? preferredUsername = freezed,
    Object? givenName = freezed,
    Object? familyName = freezed,
  }) {
    return _then(_value.copyWith(
      sub: freezed == sub
          ? _value.sub
          : sub // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      preferredUsername: freezed == preferredUsername
          ? _value.preferredUsername
          : preferredUsername // ignore: cast_nullable_to_non_nullable
              as String?,
      givenName: freezed == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String?,
      familyName: freezed == familyName
          ? _value.familyName
          : familyName // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserInformationCopyWith<$Res>
    implements $UserInformationCopyWith<$Res> {
  factory _$$_UserInformationCopyWith(
          _$_UserInformation value, $Res Function(_$_UserInformation) then) =
      __$$_UserInformationCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? sub,
      String? email,
      String? name,
      @JsonKey(name: "preferred_username") String? preferredUsername,
      @JsonKey(name: "given_name") String? givenName,
      @JsonKey(name: "family_name") String? familyName});
}

/// @nodoc
class __$$_UserInformationCopyWithImpl<$Res>
    extends _$UserInformationCopyWithImpl<$Res, _$_UserInformation>
    implements _$$_UserInformationCopyWith<$Res> {
  __$$_UserInformationCopyWithImpl(
      _$_UserInformation _value, $Res Function(_$_UserInformation) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sub = freezed,
    Object? email = freezed,
    Object? name = freezed,
    Object? preferredUsername = freezed,
    Object? givenName = freezed,
    Object? familyName = freezed,
  }) {
    return _then(_$_UserInformation(
      sub: freezed == sub
          ? _value.sub
          : sub // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      preferredUsername: freezed == preferredUsername
          ? _value.preferredUsername
          : preferredUsername // ignore: cast_nullable_to_non_nullable
              as String?,
      givenName: freezed == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String?,
      familyName: freezed == familyName
          ? _value.familyName
          : familyName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserInformation implements _UserInformation {
  const _$_UserInformation(
      {required this.sub,
      required this.email,
      required this.name,
      @JsonKey(name: "preferred_username") required this.preferredUsername,
      @JsonKey(name: "given_name") required this.givenName,
      @JsonKey(name: "family_name") required this.familyName});

  factory _$_UserInformation.fromJson(Map<String, dynamic> json) =>
      _$$_UserInformationFromJson(json);

  @override
  final String? sub;
  @override
  final String? email;
  @override
  final String? name;
  @override
  @JsonKey(name: "preferred_username")
  final String? preferredUsername;
  @override
  @JsonKey(name: "given_name")
  final String? givenName;
  @override
  @JsonKey(name: "family_name")
  final String? familyName;

  @override
  String toString() {
    return 'UserInformation(sub: $sub, email: $email, name: $name, preferredUsername: $preferredUsername, givenName: $givenName, familyName: $familyName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserInformation &&
            (identical(other.sub, sub) || other.sub == sub) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.preferredUsername, preferredUsername) ||
                other.preferredUsername == preferredUsername) &&
            (identical(other.givenName, givenName) ||
                other.givenName == givenName) &&
            (identical(other.familyName, familyName) ||
                other.familyName == familyName));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, sub, email, name, preferredUsername, givenName, familyName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserInformationCopyWith<_$_UserInformation> get copyWith =>
      __$$_UserInformationCopyWithImpl<_$_UserInformation>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserInformationToJson(
      this,
    );
  }
}

abstract class _UserInformation implements UserInformation {
  const factory _UserInformation(
          {required final String? sub,
          required final String? email,
          required final String? name,
          @JsonKey(name: "preferred_username")
          required final String? preferredUsername,
          @JsonKey(name: "given_name") required final String? givenName,
          @JsonKey(name: "family_name") required final String? familyName}) =
      _$_UserInformation;

  factory _UserInformation.fromJson(Map<String, dynamic> json) =
      _$_UserInformation.fromJson;

  @override
  String? get sub;
  @override
  String? get email;
  @override
  String? get name;
  @override
  @JsonKey(name: "preferred_username")
  String? get preferredUsername;
  @override
  @JsonKey(name: "given_name")
  String? get givenName;
  @override
  @JsonKey(name: "family_name")
  String? get familyName;
  @override
  @JsonKey(ignore: true)
  _$$_UserInformationCopyWith<_$_UserInformation> get copyWith =>
      throw _privateConstructorUsedError;
}
