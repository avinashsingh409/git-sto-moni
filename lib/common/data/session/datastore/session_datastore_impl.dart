import 'package:injectable/injectable.dart';
// ignore: implementation_imports
import 'package:openid_client/src/model.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/common/data/session/datastore/models/session_dto.dart';
import 'package:sto_mobile_v2/common/data/session/session_dao.dart';
import 'package:sto_mobile_v2/common/data/session/session_openid_client.dart';

import 'package:sto_mobile_v2/common/domain/session/datastore/session_datastore.dart';

@LazySingleton(as: ISessionDataStore)
class SessionDataStoreImpl extends ISessionDataStore {
  final SessionOpenIdClient sessionOpenIdClient;
  final SessionDAO sessionDao;

  SessionDataStoreImpl(this.sessionOpenIdClient, this.sessionDao);

  @override
  Future<TokenResponse> login({required AppConfig appConfig}) {
    return sessionOpenIdClient.authenticate(appConfig: appConfig);
  }

  @override
  Future<void> insertSession({required SessionDTO sessionDTO}) {
    return sessionDao.upsertSession(sessionDTO: sessionDTO);
  }

  @override
  SessionDTO? getSession() {
    return sessionDao.getSession();
  }

  @override
  Future<void> deleteSession() {
    return sessionDao.deleteSession();
  }
}
