import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/managers/session_preference_manager.dart';
import 'package:sto_mobile_v2/common/domain/license/datastore/license_datastore.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/datastore/snyc_ticker_datastore.dart';
import 'package:sto_mobile_v2/common/domain/user_call/datastore/user_call_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/comment/datastore/turnaround_comment_datastore.dart';
import 'package:sto_mobile_v2/common/domain/session/datastore/logout_datastore.dart';
import 'package:sto_mobile_v2/common/domain/session/datastore/session_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/execution_sync/datastore/execution_sync_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/filter_config/datastore/filter_config_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/grouping/datastore/activity_grouping_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/datastore/turnaround_activity_datastore.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/datastore/turnaround_event_datastore.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/datastore/wbs_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/attachments/datastore/attachments_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/field_definitions/datastore/field_definitions_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/datastore/workpackage_datastore.dart';
import 'package:sto_mobile_v2/planner/domain/workpackagegrouping/datastore/workpackage_grouping_datastore.dart';

@LazySingleton(as: ILogoutDataStore)
class LogoutDataStoreImpl extends ILogoutDataStore {
  final ITurnAroundActivityDataStore turnAroundActivityDataStore;
  final IWbsDataStore wbsDataStore;
  final ITurnaroundActivitiesCommentDataStore commentDataStore;
  final IExecutionSyncDataStore syncDataStore;
  final ISessionDataStore sessionDataStore;
  final ITurnAroundEventDataStore turnAroundEventDataStore;
  final ILicenseDataStore licenseDataStore;
  final IFieldDefinitionDataStore fieldDefinitionDataStore;
  final IAttachmentsDataStore attachmentsDataStore;
  final IWorkpackageDataStore workpackageDataStore;
  final IWorkpackageGroupingDataStore workpackageGroupingDataStore;
  final SessionPreferenceManager sessionPreferenceManager;
  final ISyncTickerDataStore syncTickerDataStore;
  final IUserCallDataStore userCallDataStore;
  final IActivityGroupingDataStore activityGroupingDataStore;
  final IFilterConfigDataStore filterConfigDataStore;

  LogoutDataStoreImpl(
      this.turnAroundActivityDataStore,
      this.wbsDataStore,
      this.commentDataStore,
      this.syncDataStore,
      this.sessionDataStore,
      this.turnAroundEventDataStore,
      this.sessionPreferenceManager,
      this.licenseDataStore,
      this.attachmentsDataStore,
      this.fieldDefinitionDataStore,
      this.workpackageDataStore,
      this.workpackageGroupingDataStore,
      this.syncTickerDataStore,
      this.userCallDataStore,
      this.activityGroupingDataStore,
      this.filterConfigDataStore);

  @override
  Future<void> clearData() async {
    await sessionDataStore.deleteSession();
    await turnAroundEventDataStore.deleteTurnAroudEvents();
    await turnAroundActivityDataStore.deleteTurnAroudActivities();
    await wbsDataStore.deleteAllWbsItems();
    await activityGroupingDataStore.clearMultiLevelGrouping();
    await wbsDataStore.deleteAllWbsLevelStyles();
    await commentDataStore.deleteAllComments();
    await syncDataStore.clearCachedTransactions();
    await fieldDefinitionDataStore.clearWorkpackageCache();
    await fieldDefinitionDataStore.clearAllStaticFieldOptions();
    await workpackageDataStore.deleteAllWorkpackages();
    await workpackageGroupingDataStore.deleteAllWorkpackageGroupings();
    await attachmentsDataStore.deleteAllAttachments();
    await licenseDataStore.clearCachedLicense();
    await syncTickerDataStore.clearStatus();
    await userCallDataStore.deleteCachedUser();
    await filterConfigDataStore.resetFilters();
    await activityGroupingDataStore.clearSelectedMultiLevelSort();
    sessionPreferenceManager.clearDataConfig();
  }
}
