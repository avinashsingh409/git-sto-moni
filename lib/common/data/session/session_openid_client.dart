import 'dart:async';
import 'dart:io';
import 'package:injectable/injectable.dart';
import 'package:openid_client/openid_client_io.dart';
import 'package:sto_mobile_v2/common/data/app_config/datastore/models/app_config.dart';
import 'package:sto_mobile_v2/config/navigation/app_navigation.dart';
import 'package:sto_mobile_v2/config/navigation/global_navigation_key.dart';

import 'package:url_launcher/url_launcher.dart';
// ignore: depend_on_referenced_packages
import 'package:url_launcher_platform_interface/url_launcher_platform_interface.dart';

@injectable
class SessionOpenIdClient {
  Future<TokenResponse> authenticate({required AppConfig appConfig}) async {
    var clientId = appConfig.mobileClientId;
    var uri = Uri.parse(appConfig.authenticationUrl);
    var issuer = await Issuer.discover(uri);
    var client = Client(issuer, clientId);

    urlLauncher(String url) async {
      if (!Platform.isWindows) {
        jumpToAuthenticationScreen(context: GlobalNavigationKey().context!, url: url);
      } else {
        await _launchUrl(url);
      }
    }

    var authenticator = Authenticator(client, urlLancher: urlLauncher);
    // starts the authentication
    var credentials = await authenticator.authorize();
    // close the web view when finished
    if (!Platform.isWindows) {
      closeInAppWebView();
    }
    final token = await credentials.getTokenResponse();
    return token;
  }

  Future<void> _launchUrl(String url) async {
    if (await UrlLauncherPlatform.instance.canLaunch(url)) {
      await UrlLauncherPlatform.instance.launch(
        url,
        useWebView: (Platform.isAndroid || Platform.isIOS),
        useSafariVC: true,
        enableJavaScript: Platform.isIOS,
        enableDomStorage: false,
        universalLinksOnly: false,
        headers: <String, String>{},
      );
    } else {
      // throw Failure.of(format(S.current.couldNotLaunchUri, url));
    }
  }
}
