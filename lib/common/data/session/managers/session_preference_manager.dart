import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@singleton
class SessionPreferenceManager {
  final SharedPreferences sharedPreferences;

  SessionPreferenceManager(this.sharedPreferences);

  String? get selectedTurnAroundEvent => sharedPreferences.getString(selectedTurnAroundEventKey);
  set selectedTurnAroundEvent(String? selectedTurnAroundEvent) =>
      sharedPreferences.setString(selectedTurnAroundEventKey, selectedTurnAroundEvent ?? "");

  String? get selectedAppFeature => sharedPreferences.getString(selectedAppFeatureKey);
  set selectedAppFeature(String? selectedAppFeature) =>
      sharedPreferences.setString(selectedAppFeatureKey, selectedAppFeature ?? "");

  bool? get backwardsKeyEnabled => sharedPreferences.getBool(backwardsCommentEnabledKey);
  set backwardsKeyEnabled(bool? backwardsKeyEnabled) =>
      sharedPreferences.setBool(backwardsCommentEnabledKey, backwardsKeyEnabled ?? false);

  String? get percentageRange => sharedPreferences.getString(percentageRangeKey);
  set percentageRange(String? percentageRange) =>
      sharedPreferences.setString(percentageRangeKey, percentageRange ?? "");

  bool? get turnaroundEventSwipeEnabled => sharedPreferences.getBool(turnaroundEventSwipeEnabledKey);
  set turnaroundEventSwipeEnabled(bool? turnaroundEventSwipeEnabled) =>
      sharedPreferences.setBool(turnaroundEventSwipeEnabledKey, turnaroundEventSwipeEnabled ?? true);

  void clearDataConfig() {
    sharedPreferences.remove(selectedTurnAroundEventKey);
    sharedPreferences.remove(backwardsCommentEnabledKey);
    sharedPreferences.remove(selectedAppFeatureKey);
    sharedPreferences.remove(percentageRangeKey);
    sharedPreferences.remove(turnaroundEventSwipeEnabledKey);
  }
}

const String selectedTurnAroundEventKey = "key:session:selecetedTurnAroundEventKey";
const String backwardsCommentEnabledKey = "key:session:backwardsCommentEnabledKey";
const String selectedAppFeatureKey = "key:session:selectedAppFeatureKey";
const String percentageRangeKey = "key:session:percentageRange";
const String turnaroundEventSwipeEnabledKey = "key:session:turnaroundEventSwipeEnabledKey";
