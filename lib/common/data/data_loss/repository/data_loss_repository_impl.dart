import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/data_loss/datastore/data_loss_datastore.dart';
import 'package:sto_mobile_v2/common/domain/data_loss/repository/data_loss_repository.dart';

@LazySingleton(as: IDataLossRepository)
class DataLossRepositoryImpl extends IDataLossRepository {
  final IDataLossDataStore dataLossDataStore;

  DataLossRepositoryImpl(this.dataLossDataStore);

  @override
  Future<bool> isDataChanged() async {
    return await dataLossDataStore.isDataChanged();
  }
}