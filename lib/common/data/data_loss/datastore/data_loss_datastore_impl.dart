import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/data_loss/datastore/data_loss_datastore.dart';
import 'package:sto_mobile_v2/execution/data/comment/updated_comment_dao.dart';
import 'package:sto_mobile_v2/execution/data/turnaround_activity/updated_turnaround_activity_dao.dart';
import 'package:sto_mobile_v2/planner/data/attachments/attachments_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/field_definition_value_dao.dart';
import 'package:sto_mobile_v2/planner/data/field_definitions/static_field_definition_value_dao.dart';

@LazySingleton(as: IDataLossDataStore)
class DataLossDataStoreImpl extends IDataLossDataStore {
  final UpdatedTurnAroundActivityDAO updatedTurnAroundActivityDAO;
  final UpdatedCommentDAO updatedCommentDAO;
  final AttachmentsDAO attachmentsDAO;
  final StaticFieldDefinitionValueDAO staticFieldDefinitionValueDAO;
  final FieldDefinitionValueDAO fieldDefinitionValueDAO;

  DataLossDataStoreImpl(
    this.updatedTurnAroundActivityDAO, 
    this.updatedCommentDAO, 
    this.attachmentsDAO,
    this.staticFieldDefinitionValueDAO,
    this.fieldDefinitionValueDAO,
  );

  @override
  Future<bool> isDataChanged() async {
    // check updated activities
    if ((await updatedTurnAroundActivityDAO.getAllTurnAroundActivities()).isNotEmpty) {
      return true;
    }

    // check updated comments
    if ((await updatedCommentDAO.getAllUpdatedComments()).isNotEmpty) {
      return true;
    }

    // check updated attachments
    if ((await attachmentsDAO.getAllAttachments()).where((a) => a.isChanged == true).isNotEmpty) {
      return true;
    }

    // check updated static fields
    final staticFields = await staticFieldDefinitionValueDAO.getAllChangedStaticFieldDefinitionValues();
    if (staticFields != null && staticFields.isNotEmpty) {
      return true;
    }

    // check updated fields
    final fields = await fieldDefinitionValueDAO.getAllChangedFieldDefinitionValues();
    if (fields != null && fields.isNotEmpty) {
      return true;
    }

    // o/w, no data changed
    return false;
  }
}