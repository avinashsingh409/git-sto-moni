import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/common/data/license/license_api_service.dart';
import 'package:sto_mobile_v2/common/data/license/license_dao.dart';
import 'package:sto_mobile_v2/common/domain/license/datastore/license_datastore.dart';

@LazySingleton(as: ILicenseDataStore)
class LicenseDataStoreImpl extends ILicenseDataStore {
  final LicenseApiService licenseApiService;
  final LicenseDAO licenseDAO;

  LicenseDataStoreImpl(this.licenseApiService, this.licenseDAO);

  @override
  Future<int> cacheServiceLicense({required LicenseDTO license}) {
    return licenseDAO.upsertLicense(licenseEntry: license);
  }

  @override
  Future<LicenseDTO?> checkServiceLicense() {
    return licenseApiService.getLicenseFromService();
  }

  @override
  Future<LicenseDTO?> getCachedLicense() {
    return licenseDAO.getLicenseStatus();
  }

  @override
  Future<void> clearCachedLicense() {
    return licenseDAO.clearLicense();
  }
}
