// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'license_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetLicenseDTOCollection on Isar {
  IsarCollection<LicenseDTO> get licenseDTOs => this.collection();
}

const LicenseDTOSchema = CollectionSchema(
  name: r'LicenseDTO',
  id: 2932350684559007343,
  properties: {
    r'execution': PropertySchema(
      id: 0,
      name: r'execution',
      type: IsarType.bool,
    ),
    r'planner': PropertySchema(
      id: 1,
      name: r'planner',
      type: IsarType.bool,
    )
  },
  estimateSize: _licenseDTOEstimateSize,
  serialize: _licenseDTOSerialize,
  deserialize: _licenseDTODeserialize,
  deserializeProp: _licenseDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _licenseDTOGetId,
  getLinks: _licenseDTOGetLinks,
  attach: _licenseDTOAttach,
  version: '3.1.0+1',
);

int _licenseDTOEstimateSize(
  LicenseDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _licenseDTOSerialize(
  LicenseDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.execution);
  writer.writeBool(offsets[1], object.planner);
}

LicenseDTO _licenseDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = LicenseDTO(
    execution: reader.readBoolOrNull(offsets[0]),
    id: id,
    planner: reader.readBoolOrNull(offsets[1]),
  );
  return object;
}

P _licenseDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _licenseDTOGetId(LicenseDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _licenseDTOGetLinks(LicenseDTO object) {
  return [];
}

void _licenseDTOAttach(IsarCollection<dynamic> col, Id id, LicenseDTO object) {
  object.id = id;
}

extension LicenseDTOQueryWhereSort
    on QueryBuilder<LicenseDTO, LicenseDTO, QWhere> {
  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension LicenseDTOQueryWhere
    on QueryBuilder<LicenseDTO, LicenseDTO, QWhereClause> {
  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension LicenseDTOQueryFilter
    on QueryBuilder<LicenseDTO, LicenseDTO, QFilterCondition> {
  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition>
      executionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'execution',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition>
      executionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'execution',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> executionEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'execution',
        value: value,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> plannerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'planner',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition>
      plannerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'planner',
      ));
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterFilterCondition> plannerEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'planner',
        value: value,
      ));
    });
  }
}

extension LicenseDTOQueryObject
    on QueryBuilder<LicenseDTO, LicenseDTO, QFilterCondition> {}

extension LicenseDTOQueryLinks
    on QueryBuilder<LicenseDTO, LicenseDTO, QFilterCondition> {}

extension LicenseDTOQuerySortBy
    on QueryBuilder<LicenseDTO, LicenseDTO, QSortBy> {
  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> sortByExecution() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'execution', Sort.asc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> sortByExecutionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'execution', Sort.desc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> sortByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.asc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> sortByPlannerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.desc);
    });
  }
}

extension LicenseDTOQuerySortThenBy
    on QueryBuilder<LicenseDTO, LicenseDTO, QSortThenBy> {
  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenByExecution() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'execution', Sort.asc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenByExecutionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'execution', Sort.desc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.asc);
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QAfterSortBy> thenByPlannerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'planner', Sort.desc);
    });
  }
}

extension LicenseDTOQueryWhereDistinct
    on QueryBuilder<LicenseDTO, LicenseDTO, QDistinct> {
  QueryBuilder<LicenseDTO, LicenseDTO, QDistinct> distinctByExecution() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'execution');
    });
  }

  QueryBuilder<LicenseDTO, LicenseDTO, QDistinct> distinctByPlanner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'planner');
    });
  }
}

extension LicenseDTOQueryProperty
    on QueryBuilder<LicenseDTO, LicenseDTO, QQueryProperty> {
  QueryBuilder<LicenseDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<LicenseDTO, bool?, QQueryOperations> executionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'execution');
    });
  }

  QueryBuilder<LicenseDTO, bool?, QQueryOperations> plannerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'planner');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LicenseDTO _$LicenseDTOFromJson(Map<String, dynamic> json) => LicenseDTO(
      id: json['id'] as int?,
      planner: json['planner'] as bool?,
      execution: json['execution'] as bool?,
    );

Map<String, dynamic> _$LicenseDTOToJson(LicenseDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'planner': instance.planner,
      'execution': instance.execution,
    };
