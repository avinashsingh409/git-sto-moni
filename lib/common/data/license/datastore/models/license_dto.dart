import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'license_dto.g.dart';

@collection
@JsonSerializable()
class LicenseDTO {
  Id? id;
  bool? planner;
  bool? execution;

  LicenseDTO({
    this.id,
    this.planner,
    this.execution,
  });

  factory LicenseDTO.fromJson(Map<String, dynamic> json) => _$LicenseDTOFromJson(json);
}
