import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';

@injectable
class LicenseApiService {
  final Dio dio;

  LicenseApiService(this.dio);

  Future<LicenseDTO?> getLicenseFromService() async {
    final response = await dio.get(ApiEndPoints.license);
    final jsonData = response.data;
    if (jsonData != null) {
      final license = LicenseDTO.fromJson(jsonData);
      return license;
    }
    return null;
  }
}
