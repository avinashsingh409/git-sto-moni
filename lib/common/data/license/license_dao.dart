import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class LicenseDAO {
  final isar = PersistenceModule.isar;

  LicenseDAO();

  /// Isar Collection of License
  late IsarCollection<LicenseDTO> collection = isar.licenseDTOs;

  /// Clear license
  Future<void> clearLicense() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get license status
  Future<LicenseDTO?> getLicenseStatus() async {
    final license = collection.where().findFirst();
    return license;
  }

  /// Upsert license
  Future<int> upsertLicense({
    required LicenseDTO licenseEntry,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(licenseEntry);
    });
  }
}
