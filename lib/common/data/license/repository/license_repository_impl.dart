import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/license/datastore/models/license_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/license_exception.dart';
import 'package:sto_mobile_v2/common/domain/license/datastore/license_datastore.dart';
import 'package:sto_mobile_v2/common/domain/license/repository/license_repository.dart';

@LazySingleton(as: ILicenseRepository)
class LicenseRepositoryImpl extends ILicenseRepository {
  final ILicenseDataStore licenseDataStore;
  LicenseRepositoryImpl(this.licenseDataStore);

  @override
  Future<Either<LicenseDTO, LicenseException>> getAndCacheServiceLicense() async {
    await licenseDataStore.clearCachedLicense();
    try {
      final licenseFromResponse = await licenseDataStore.checkServiceLicense();
      if (licenseFromResponse != null) {
        await licenseDataStore.cacheServiceLicense(license: licenseFromResponse);
        return Left(licenseFromResponse);
      }
      return const Right(LicenseException.unexpectedError());
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(LicenseException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(LicenseException.socketException());
        }
      }
      return const Right(LicenseException.unexpectedError());
    } on ConnectionException catch (_) {
      return const Right(LicenseException.socketException());
    }
  }

  @override
  Future<LicenseDTO?> getCachedLicense() {
    return licenseDataStore.getCachedLicense();
  }
}
