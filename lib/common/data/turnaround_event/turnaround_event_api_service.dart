import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_response.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';

@injectable
class TurnAroundEventApiService {
  final Dio dio;

  TurnAroundEventApiService(this.dio);

  Future<TurnAroundEventResponse> getTurnAroundEvents() async {
    final response = await dio.get(
      ApiEndPoints.stoxTurnaroundEvents,
    );
    final jsonData = response.data;
    if (jsonData != null) {
      final List<TurnAroundEventDTO> events = List<TurnAroundEventDTO>.from(
        jsonData.map(
          (event) => TurnAroundEventDTO.fromJson(event),
        ),
      );
      return TurnAroundEventResponse(events: events);
    }
    return const TurnAroundEventResponse(events: []);
  }
}
