import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class TurnAroundEventDAO {
  final isar = PersistenceModule.isar;

  TurnAroundEventDAO();

  /// Isar Collection of TurnAroundEvents
  late IsarCollection<TurnAroundEventDTO> collection = isar.turnAroundEventDTOs;

  /// Delete All TurnAroundEvents
  Future<void> deleteTurnAroudEvents() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All TurnAroundEvents
  Future<List<TurnAroundEventDTO>> getAllTurnAroundEvents() async {
    final turnAroundEvents = collection.where().findAll();
    return turnAroundEvents;
  }

  // Get selected TurnAroundEvent
  Future<TurnAroundEventDTO?> getSelectedTurnAroundEvent({required int id}) async {
    final turnaroundEvent = collection.where().idEqualTo(id).findFirst();
    return turnaroundEvent;
  }

  /// Upsert All TurnAroundEvents
  Future<List<int>> upsertAllTurnAroundEvents({
    required List<TurnAroundEventDTO> turnAroundDTOs,
  }) async {
    return isar.writeTxn(() async {
      return collection.putAll(turnAroundDTOs);
    });
  }
}
