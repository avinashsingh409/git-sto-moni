import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/managers/session_preference_manager.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_response.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/turnaround_event_api_service.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/turnaround_event_dao.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';

import 'package:sto_mobile_v2/common/domain/turnaround_event/datastore/turnaround_event_datastore.dart';

@LazySingleton(as: ITurnAroundEventDataStore)
class TurnAroundEventDataStoreImpl extends ITurnAroundEventDataStore {
  final TurnAroundEventApiService apiService;
  final TurnAroundEventDAO turnAroundEventDAO;
  final SessionPreferenceManager sessionPreferenceManager;
  TurnAroundEventDataStoreImpl(this.apiService, this.turnAroundEventDAO, this.sessionPreferenceManager);

  @override
  Future<TurnAroundEventResponse> getTurnAroundEvents() {
    return apiService.getTurnAroundEvents().catchDioException();
  }

  @override
  Future<void> insertAllEvents({required List<TurnAroundEventDTO> turnAroundDTOs}) {
    return turnAroundEventDAO.upsertAllTurnAroundEvents(turnAroundDTOs: turnAroundDTOs);
  }

  @override
  Future<List<TurnAroundEventDTO>> getCachedTurnAroundEvents() {
    return turnAroundEventDAO.getAllTurnAroundEvents();
  }

  @override
  Future<void> deleteTurnAroudEvents() {
    return turnAroundEventDAO.deleteTurnAroudEvents();
  }

  @override
  Future<TurnAroundEventDTO?> getSelectedTurnAroundEvent({required int id}) {
    return turnAroundEventDAO.getSelectedTurnAroundEvent(id: id);
  }

  @override
  String saveSelectedTurnAroundEventId({required String turnAroundId}) {
    return sessionPreferenceManager.selectedTurnAroundEvent = turnAroundId;
  }

  @override
  String? getSelectedTurnAroundEventId() {
    return sessionPreferenceManager.selectedTurnAroundEvent;
  }

  @override
  bool? getBackwardsCommentEnabled() {
    return sessionPreferenceManager.backwardsKeyEnabled;
  }

  @override
  bool saveBackwardsCommentEnabled({required bool isBackwardsCommentEnabled}) {
    return sessionPreferenceManager.backwardsKeyEnabled = isBackwardsCommentEnabled;
  }

  @override
  String? getPercentageRange() {
    return sessionPreferenceManager.percentageRange;
  }

  @override
  String? savePercentageRange({required String? percentageRange}) {
    return sessionPreferenceManager.percentageRange = percentageRange;
  }

  @override
  bool? getTurnaroundEventSwipeEnabled() {
    return sessionPreferenceManager.turnaroundEventSwipeEnabled;
  }

  @override
  bool saveTurnaroundEventSwipeEnabled({required bool turnaroundEventSwipeEnabled}) {
    return sessionPreferenceManager.turnaroundEventSwipeEnabled = turnaroundEventSwipeEnabled;
  }
}
