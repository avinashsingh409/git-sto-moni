// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
part 'turnaround_association.freezed.dart';
part 'turnaround_association.g.dart';

@freezed
abstract class TurnAroundAssociation with _$TurnAroundAssociation {
  const factory TurnAroundAssociation({
    required int id,
    @JsonKey(name: "turnaroundEventId") required int turnaroundEventId,
    @JsonKey(name: "userId") required int userId,
    @JsonKey(name: "inProgress") required bool inProgress,
  }) = _TurnAroundAssociation;

  factory TurnAroundAssociation.fromJson(Map<String, dynamic> json) => _$TurnAroundAssociationFromJson(json);
}
