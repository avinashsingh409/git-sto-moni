// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'turnaround_association.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TurnAroundAssociation _$$_TurnAroundAssociationFromJson(
        Map<String, dynamic> json) =>
    _$_TurnAroundAssociation(
      id: json['id'] as int,
      turnaroundEventId: json['turnaroundEventId'] as int,
      userId: json['userId'] as int,
      inProgress: json['inProgress'] as bool,
    );

Map<String, dynamic> _$$_TurnAroundAssociationToJson(
        _$_TurnAroundAssociation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'turnaroundEventId': instance.turnaroundEventId,
      'userId': instance.userId,
      'inProgress': instance.inProgress,
    };
