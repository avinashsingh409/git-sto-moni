import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';

part 'turnaround_event_response.freezed.dart';

@freezed
abstract class TurnAroundEventResponse with _$TurnAroundEventResponse {
  const factory TurnAroundEventResponse({
    required List<TurnAroundEventDTO>? events,
  }) = _TurnAroundEvent;
}
