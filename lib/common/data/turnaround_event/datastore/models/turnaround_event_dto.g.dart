// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'turnaround_event_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetTurnAroundEventDTOCollection on Isar {
  IsarCollection<TurnAroundEventDTO> get turnAroundEventDTOs =>
      this.collection();
}

const TurnAroundEventDTOSchema = CollectionSchema(
  name: r'TurnAroundEventDTO',
  id: -5850178273005048593,
  properties: {
    r'backwardsCommentsEnabled': PropertySchema(
      id: 0,
      name: r'backwardsCommentsEnabled',
      type: IsarType.bool,
    ),
    r'description': PropertySchema(
      id: 1,
      name: r'description',
      type: IsarType.string,
    ),
    r'hostName': PropertySchema(
      id: 2,
      name: r'hostName',
      type: IsarType.string,
    ),
    r'isManualProgress': PropertySchema(
      id: 3,
      name: r'isManualProgress',
      type: IsarType.bool,
    ),
    r'name': PropertySchema(
      id: 4,
      name: r'name',
      type: IsarType.string,
    ),
    r'percentageRangeLowerBound': PropertySchema(
      id: 5,
      name: r'percentageRangeLowerBound',
      type: IsarType.long,
    ),
    r'percentageRangeUpperBound': PropertySchema(
      id: 6,
      name: r'percentageRangeUpperBound',
      type: IsarType.long,
    ),
    r'primaveraProjectId': PropertySchema(
      id: 7,
      name: r'primaveraProjectId',
      type: IsarType.string,
    ),
    r'swipeAllowed': PropertySchema(
      id: 8,
      name: r'swipeAllowed',
      type: IsarType.bool,
    )
  },
  estimateSize: _turnAroundEventDTOEstimateSize,
  serialize: _turnAroundEventDTOSerialize,
  deserialize: _turnAroundEventDTODeserialize,
  deserializeProp: _turnAroundEventDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _turnAroundEventDTOGetId,
  getLinks: _turnAroundEventDTOGetLinks,
  attach: _turnAroundEventDTOAttach,
  version: '3.1.0+1',
);

int _turnAroundEventDTOEstimateSize(
  TurnAroundEventDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.hostName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.primaveraProjectId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _turnAroundEventDTOSerialize(
  TurnAroundEventDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.backwardsCommentsEnabled);
  writer.writeString(offsets[1], object.description);
  writer.writeString(offsets[2], object.hostName);
  writer.writeBool(offsets[3], object.isManualProgress);
  writer.writeString(offsets[4], object.name);
  writer.writeLong(offsets[5], object.percentageRangeLowerBound);
  writer.writeLong(offsets[6], object.percentageRangeUpperBound);
  writer.writeString(offsets[7], object.primaveraProjectId);
  writer.writeBool(offsets[8], object.swipeAllowed);
}

TurnAroundEventDTO _turnAroundEventDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = TurnAroundEventDTO(
    backwardsCommentsEnabled: reader.readBoolOrNull(offsets[0]),
    description: reader.readStringOrNull(offsets[1]),
    hostName: reader.readStringOrNull(offsets[2]),
    id: id,
    isManualProgress: reader.readBoolOrNull(offsets[3]),
    name: reader.readStringOrNull(offsets[4]),
    percentageRangeLowerBound: reader.readLongOrNull(offsets[5]),
    percentageRangeUpperBound: reader.readLongOrNull(offsets[6]),
    primaveraProjectId: reader.readStringOrNull(offsets[7]),
    swipeAllowed: reader.readBoolOrNull(offsets[8]),
  );
  return object;
}

P _turnAroundEventDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readBoolOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _turnAroundEventDTOGetId(TurnAroundEventDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _turnAroundEventDTOGetLinks(
    TurnAroundEventDTO object) {
  return [];
}

void _turnAroundEventDTOAttach(
    IsarCollection<dynamic> col, Id id, TurnAroundEventDTO object) {
  object.id = id;
}

extension TurnAroundEventDTOQueryWhereSort
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QWhere> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension TurnAroundEventDTOQueryWhere
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QWhereClause> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension TurnAroundEventDTOQueryFilter
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QFilterCondition> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      backwardsCommentsEnabledIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'backwardsCommentsEnabled',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      backwardsCommentsEnabledIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'backwardsCommentsEnabled',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      backwardsCommentsEnabledEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'backwardsCommentsEnabled',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'hostName',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'hostName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'hostName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'hostName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      hostNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'hostName',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      isManualProgressIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isManualProgress',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      isManualProgressIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isManualProgress',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      isManualProgressEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isManualProgress',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'percentageRangeLowerBound',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'percentageRangeLowerBound',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentageRangeLowerBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'percentageRangeLowerBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'percentageRangeLowerBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeLowerBoundBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'percentageRangeLowerBound',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'percentageRangeUpperBound',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'percentageRangeUpperBound',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'percentageRangeUpperBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'percentageRangeUpperBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'percentageRangeUpperBound',
        value: value,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      percentageRangeUpperBoundBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'percentageRangeUpperBound',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'primaveraProjectId',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'primaveraProjectId',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'primaveraProjectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'primaveraProjectId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'primaveraProjectId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'primaveraProjectId',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      primaveraProjectIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'primaveraProjectId',
        value: '',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      swipeAllowedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'swipeAllowed',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      swipeAllowedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'swipeAllowed',
      ));
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterFilterCondition>
      swipeAllowedEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'swipeAllowed',
        value: value,
      ));
    });
  }
}

extension TurnAroundEventDTOQueryObject
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QFilterCondition> {}

extension TurnAroundEventDTOQueryLinks
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QFilterCondition> {}

extension TurnAroundEventDTOQuerySortBy
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QSortBy> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByBackwardsCommentsEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backwardsCommentsEnabled', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByBackwardsCommentsEnabledDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backwardsCommentsEnabled', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByIsManualProgress() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isManualProgress', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByIsManualProgressDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isManualProgress', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPercentageRangeLowerBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeLowerBound', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPercentageRangeLowerBoundDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeLowerBound', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPercentageRangeUpperBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeUpperBound', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPercentageRangeUpperBoundDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeUpperBound', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPrimaveraProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaveraProjectId', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortByPrimaveraProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaveraProjectId', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortBySwipeAllowed() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'swipeAllowed', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      sortBySwipeAllowedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'swipeAllowed', Sort.desc);
    });
  }
}

extension TurnAroundEventDTOQuerySortThenBy
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QSortThenBy> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByBackwardsCommentsEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backwardsCommentsEnabled', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByBackwardsCommentsEnabledDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'backwardsCommentsEnabled', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByHostName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByHostNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hostName', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByIsManualProgress() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isManualProgress', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByIsManualProgressDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isManualProgress', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPercentageRangeLowerBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeLowerBound', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPercentageRangeLowerBoundDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeLowerBound', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPercentageRangeUpperBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeUpperBound', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPercentageRangeUpperBoundDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'percentageRangeUpperBound', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPrimaveraProjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaveraProjectId', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenByPrimaveraProjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'primaveraProjectId', Sort.desc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenBySwipeAllowed() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'swipeAllowed', Sort.asc);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QAfterSortBy>
      thenBySwipeAllowedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'swipeAllowed', Sort.desc);
    });
  }
}

extension TurnAroundEventDTOQueryWhereDistinct
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct> {
  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByBackwardsCommentsEnabled() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'backwardsCommentsEnabled');
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByDescription({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'description', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByHostName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'hostName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByIsManualProgress() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isManualProgress');
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByPercentageRangeLowerBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'percentageRangeLowerBound');
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByPercentageRangeUpperBound() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'percentageRangeUpperBound');
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctByPrimaveraProjectId({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'primaveraProjectId',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QDistinct>
      distinctBySwipeAllowed() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'swipeAllowed');
    });
  }
}

extension TurnAroundEventDTOQueryProperty
    on QueryBuilder<TurnAroundEventDTO, TurnAroundEventDTO, QQueryProperty> {
  QueryBuilder<TurnAroundEventDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<TurnAroundEventDTO, bool?, QQueryOperations>
      backwardsCommentsEnabledProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'backwardsCommentsEnabled');
    });
  }

  QueryBuilder<TurnAroundEventDTO, String?, QQueryOperations>
      descriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'description');
    });
  }

  QueryBuilder<TurnAroundEventDTO, String?, QQueryOperations>
      hostNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'hostName');
    });
  }

  QueryBuilder<TurnAroundEventDTO, bool?, QQueryOperations>
      isManualProgressProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isManualProgress');
    });
  }

  QueryBuilder<TurnAroundEventDTO, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<TurnAroundEventDTO, int?, QQueryOperations>
      percentageRangeLowerBoundProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'percentageRangeLowerBound');
    });
  }

  QueryBuilder<TurnAroundEventDTO, int?, QQueryOperations>
      percentageRangeUpperBoundProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'percentageRangeUpperBound');
    });
  }

  QueryBuilder<TurnAroundEventDTO, String?, QQueryOperations>
      primaveraProjectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'primaveraProjectId');
    });
  }

  QueryBuilder<TurnAroundEventDTO, bool?, QQueryOperations>
      swipeAllowedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'swipeAllowed');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TurnAroundEventDTO _$TurnAroundEventDTOFromJson(Map<String, dynamic> json) =>
    TurnAroundEventDTO(
      id: json['id'] as int?,
      name: json['name'] as String?,
      primaveraProjectId: json['primaveraProjectId'] as String?,
      hostName: json['primaveraHostName'] as String?,
      backwardsCommentsEnabled: json['backwardsCommentsEnabled'] as bool?,
      swipeAllowed: json['swipeAllowed'] as bool?,
      description: json['description'] as String?,
      isManualProgress: json['isManualProgress'] as bool?,
      percentageRangeLowerBound: json['percentageRangeLowerBound'] as int?,
      percentageRangeUpperBound: json['percentageRangeUpperBound'] as int?,
    );

Map<String, dynamic> _$TurnAroundEventDTOToJson(TurnAroundEventDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'primaveraProjectId': instance.primaveraProjectId,
      'primaveraHostName': instance.hostName,
      'backwardsCommentsEnabled': instance.backwardsCommentsEnabled,
      'swipeAllowed': instance.swipeAllowed,
      'isManualProgress': instance.isManualProgress,
      'percentageRangeLowerBound': instance.percentageRangeLowerBound,
      'percentageRangeUpperBound': instance.percentageRangeUpperBound,
    };
