// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_association.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TurnAroundAssociation _$TurnAroundAssociationFromJson(
    Map<String, dynamic> json) {
  return _TurnAroundAssociation.fromJson(json);
}

/// @nodoc
mixin _$TurnAroundAssociation {
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: "turnaroundEventId")
  int get turnaroundEventId => throw _privateConstructorUsedError;
  @JsonKey(name: "userId")
  int get userId => throw _privateConstructorUsedError;
  @JsonKey(name: "inProgress")
  bool get inProgress => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TurnAroundAssociationCopyWith<TurnAroundAssociation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnAroundAssociationCopyWith<$Res> {
  factory $TurnAroundAssociationCopyWith(TurnAroundAssociation value,
          $Res Function(TurnAroundAssociation) then) =
      _$TurnAroundAssociationCopyWithImpl<$Res, TurnAroundAssociation>;
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: "turnaroundEventId") int turnaroundEventId,
      @JsonKey(name: "userId") int userId,
      @JsonKey(name: "inProgress") bool inProgress});
}

/// @nodoc
class _$TurnAroundAssociationCopyWithImpl<$Res,
        $Val extends TurnAroundAssociation>
    implements $TurnAroundAssociationCopyWith<$Res> {
  _$TurnAroundAssociationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? turnaroundEventId = null,
    Object? userId = null,
    Object? inProgress = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      turnaroundEventId: null == turnaroundEventId
          ? _value.turnaroundEventId
          : turnaroundEventId // ignore: cast_nullable_to_non_nullable
              as int,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      inProgress: null == inProgress
          ? _value.inProgress
          : inProgress // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TurnAroundAssociationCopyWith<$Res>
    implements $TurnAroundAssociationCopyWith<$Res> {
  factory _$$_TurnAroundAssociationCopyWith(_$_TurnAroundAssociation value,
          $Res Function(_$_TurnAroundAssociation) then) =
      __$$_TurnAroundAssociationCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      @JsonKey(name: "turnaroundEventId") int turnaroundEventId,
      @JsonKey(name: "userId") int userId,
      @JsonKey(name: "inProgress") bool inProgress});
}

/// @nodoc
class __$$_TurnAroundAssociationCopyWithImpl<$Res>
    extends _$TurnAroundAssociationCopyWithImpl<$Res, _$_TurnAroundAssociation>
    implements _$$_TurnAroundAssociationCopyWith<$Res> {
  __$$_TurnAroundAssociationCopyWithImpl(_$_TurnAroundAssociation _value,
      $Res Function(_$_TurnAroundAssociation) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? turnaroundEventId = null,
    Object? userId = null,
    Object? inProgress = null,
  }) {
    return _then(_$_TurnAroundAssociation(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      turnaroundEventId: null == turnaroundEventId
          ? _value.turnaroundEventId
          : turnaroundEventId // ignore: cast_nullable_to_non_nullable
              as int,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      inProgress: null == inProgress
          ? _value.inProgress
          : inProgress // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TurnAroundAssociation implements _TurnAroundAssociation {
  const _$_TurnAroundAssociation(
      {required this.id,
      @JsonKey(name: "turnaroundEventId") required this.turnaroundEventId,
      @JsonKey(name: "userId") required this.userId,
      @JsonKey(name: "inProgress") required this.inProgress});

  factory _$_TurnAroundAssociation.fromJson(Map<String, dynamic> json) =>
      _$$_TurnAroundAssociationFromJson(json);

  @override
  final int id;
  @override
  @JsonKey(name: "turnaroundEventId")
  final int turnaroundEventId;
  @override
  @JsonKey(name: "userId")
  final int userId;
  @override
  @JsonKey(name: "inProgress")
  final bool inProgress;

  @override
  String toString() {
    return 'TurnAroundAssociation(id: $id, turnaroundEventId: $turnaroundEventId, userId: $userId, inProgress: $inProgress)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TurnAroundAssociation &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.turnaroundEventId, turnaroundEventId) ||
                other.turnaroundEventId == turnaroundEventId) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.inProgress, inProgress) ||
                other.inProgress == inProgress));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, turnaroundEventId, userId, inProgress);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TurnAroundAssociationCopyWith<_$_TurnAroundAssociation> get copyWith =>
      __$$_TurnAroundAssociationCopyWithImpl<_$_TurnAroundAssociation>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TurnAroundAssociationToJson(
      this,
    );
  }
}

abstract class _TurnAroundAssociation implements TurnAroundAssociation {
  const factory _TurnAroundAssociation(
      {required final int id,
      @JsonKey(name: "turnaroundEventId") required final int turnaroundEventId,
      @JsonKey(name: "userId") required final int userId,
      @JsonKey(name: "inProgress")
      required final bool inProgress}) = _$_TurnAroundAssociation;

  factory _TurnAroundAssociation.fromJson(Map<String, dynamic> json) =
      _$_TurnAroundAssociation.fromJson;

  @override
  int get id;
  @override
  @JsonKey(name: "turnaroundEventId")
  int get turnaroundEventId;
  @override
  @JsonKey(name: "userId")
  int get userId;
  @override
  @JsonKey(name: "inProgress")
  bool get inProgress;
  @override
  @JsonKey(ignore: true)
  _$$_TurnAroundAssociationCopyWith<_$_TurnAroundAssociation> get copyWith =>
      throw _privateConstructorUsedError;
}
