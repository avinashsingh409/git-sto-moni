// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'turnaround_event_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TurnAroundEventResponse {
  List<TurnAroundEventDTO>? get events => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TurnAroundEventResponseCopyWith<TurnAroundEventResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TurnAroundEventResponseCopyWith<$Res> {
  factory $TurnAroundEventResponseCopyWith(TurnAroundEventResponse value,
          $Res Function(TurnAroundEventResponse) then) =
      _$TurnAroundEventResponseCopyWithImpl<$Res, TurnAroundEventResponse>;
  @useResult
  $Res call({List<TurnAroundEventDTO>? events});
}

/// @nodoc
class _$TurnAroundEventResponseCopyWithImpl<$Res,
        $Val extends TurnAroundEventResponse>
    implements $TurnAroundEventResponseCopyWith<$Res> {
  _$TurnAroundEventResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? events = freezed,
  }) {
    return _then(_value.copyWith(
      events: freezed == events
          ? _value.events
          : events // ignore: cast_nullable_to_non_nullable
              as List<TurnAroundEventDTO>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TurnAroundEventCopyWith<$Res>
    implements $TurnAroundEventResponseCopyWith<$Res> {
  factory _$$_TurnAroundEventCopyWith(
          _$_TurnAroundEvent value, $Res Function(_$_TurnAroundEvent) then) =
      __$$_TurnAroundEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<TurnAroundEventDTO>? events});
}

/// @nodoc
class __$$_TurnAroundEventCopyWithImpl<$Res>
    extends _$TurnAroundEventResponseCopyWithImpl<$Res, _$_TurnAroundEvent>
    implements _$$_TurnAroundEventCopyWith<$Res> {
  __$$_TurnAroundEventCopyWithImpl(
      _$_TurnAroundEvent _value, $Res Function(_$_TurnAroundEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? events = freezed,
  }) {
    return _then(_$_TurnAroundEvent(
      events: freezed == events
          ? _value._events
          : events // ignore: cast_nullable_to_non_nullable
              as List<TurnAroundEventDTO>?,
    ));
  }
}

/// @nodoc

class _$_TurnAroundEvent implements _TurnAroundEvent {
  const _$_TurnAroundEvent({required final List<TurnAroundEventDTO>? events})
      : _events = events;

  final List<TurnAroundEventDTO>? _events;
  @override
  List<TurnAroundEventDTO>? get events {
    final value = _events;
    if (value == null) return null;
    if (_events is EqualUnmodifiableListView) return _events;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'TurnAroundEventResponse(events: $events)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TurnAroundEvent &&
            const DeepCollectionEquality().equals(other._events, _events));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_events));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TurnAroundEventCopyWith<_$_TurnAroundEvent> get copyWith =>
      __$$_TurnAroundEventCopyWithImpl<_$_TurnAroundEvent>(this, _$identity);
}

abstract class _TurnAroundEvent implements TurnAroundEventResponse {
  const factory _TurnAroundEvent(
      {required final List<TurnAroundEventDTO>? events}) = _$_TurnAroundEvent;

  @override
  List<TurnAroundEventDTO>? get events;
  @override
  @JsonKey(ignore: true)
  _$$_TurnAroundEventCopyWith<_$_TurnAroundEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
