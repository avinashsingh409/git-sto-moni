import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'turnaround_event_dto.g.dart';

@collection
@JsonSerializable()
class TurnAroundEventDTO {
  Id? id;
  String? name;
  String? description;
  String? primaveraProjectId;
  @JsonKey(name: "primaveraHostName")
  String? hostName;
  bool? backwardsCommentsEnabled;
  bool? swipeAllowed;
  bool? isManualProgress;
  int? percentageRangeLowerBound;
  int? percentageRangeUpperBound;

  TurnAroundEventDTO({
    this.id,
    this.name,
    this.primaveraProjectId,
    this.hostName,
    this.backwardsCommentsEnabled,
    this.swipeAllowed,
    this.description,
    this.isManualProgress,
    this.percentageRangeLowerBound,
    this.percentageRangeUpperBound
  });

  factory TurnAroundEventDTO.fromJson(Map<String, dynamic> json) => _$TurnAroundEventDTOFromJson(json);
}
