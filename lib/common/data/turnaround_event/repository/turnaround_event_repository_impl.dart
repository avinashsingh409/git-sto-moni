import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/managers/session_preference_manager.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_dto.dart';
import 'package:sto_mobile_v2/common/data/turnaround_event/datastore/models/turnaround_event_response.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/turnaround_event_exception.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/datastore/turnaround_event_datastore.dart';
import 'package:sto_mobile_v2/common/domain/turnaround_event/repository/turnaround_event_repository.dart';

@LazySingleton(as: ITurnAroundEventRepository)
class TurnAroundEventRepositoryImpl extends ITurnAroundEventRepository {
  final ITurnAroundEventDataStore dataStore;
  final SessionPreferenceManager sessionPreferenceManager;
  TurnAroundEventRepositoryImpl(this.dataStore, this.sessionPreferenceManager);

  @override
  Future<Either<TurnAroundEventResponse, TurnAroundEventException>> getTurnAroundEvents() async {
    try {
      final events = await dataStore.getTurnAroundEvents();
      return Left(events);
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(TurnAroundEventException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(TurnAroundEventException.socketException());
        }
      }
      return const Right(TurnAroundEventException.unexpectedError());
    } on ConnectionException catch (_) {
      return const Right(TurnAroundEventException.socketException());
    }
  }

  @override
  Future<void> insertAllActivities({required List<TurnAroundEventDTO>? turnAroundEvents}) async {
    if (turnAroundEvents != null) {
      await dataStore.insertAllEvents(turnAroundDTOs: turnAroundEvents);
    }
  }

  @override
  Future<Either<List<TurnAroundEventDTO>, Unit>> getCachedTurnAroundEvents() async {
    final turnAroundEventDTOs = await dataStore.getCachedTurnAroundEvents();
    final List<TurnAroundEventDTO> events = [];
    if (turnAroundEventDTOs.isNotEmpty) {
      for (var element in turnAroundEventDTOs) {
        events.add(TurnAroundEventDTO(
          id: element.id ?? 0,
          name: element.name ?? "",
          backwardsCommentsEnabled: element.backwardsCommentsEnabled ?? true,
          hostName: element.hostName ?? "",
          primaveraProjectId: element.primaveraProjectId ?? "",
          swipeAllowed: element.swipeAllowed ?? false,
          description: element.description ?? "", 
          isManualProgress: element.isManualProgress ?? false, 
          percentageRangeLowerBound: element.percentageRangeLowerBound, 
          percentageRangeUpperBound: element.percentageRangeUpperBound,
        ));
      }
    } else {
      return const Right(unit);
    }
    return Left(events);
  }

  @override
  Future<void> deleteTurnAroudEvents() async {
    await dataStore.deleteTurnAroudEvents();
  }

  @override
  Future<void> saveSelectedTurnAroundEventId({required int turnAroundId}) async {
    dataStore.saveSelectedTurnAroundEventId(turnAroundId: turnAroundId.toString());
  }

  @override
  Either<int, Unit> getSelectedTurnAroundEventId() {
    final selectedTurnAroundEventId = dataStore.getSelectedTurnAroundEventId();
    if (selectedTurnAroundEventId != null) {
      return Left(int.parse(selectedTurnAroundEventId));
    } else {
      return const Right(unit);
    }
  }

  @override
  Future<TurnAroundEventDTO?> getSelectedTurnAroundEvent({required int id}) {
    return dataStore.getSelectedTurnAroundEvent(id: id);
  }

  @override
  Either<bool, Unit> getBackwardsCommentEnabled() {
    final isBackwardsCommentEnabled = dataStore.getBackwardsCommentEnabled();
    if (isBackwardsCommentEnabled != null) {
      return Left(isBackwardsCommentEnabled);
    } else {
      return const Right(unit);
    }
  }

  @override
  Future<void> saveBackwardsCommentEnabled({required bool isBackwardsCommentEnabled}) async {
    dataStore.saveBackwardsCommentEnabled(isBackwardsCommentEnabled: isBackwardsCommentEnabled);
  }

  @override
  Either<String, Unit> getPercentageRange() {
    final percentageRange = dataStore.getPercentageRange();
    if (percentageRange != null) {
      return Left(percentageRange);
    } else {
      return const Right(unit);
    }
  }
  
  @override
  Either<bool, Unit> getTurnaroundEventSwipeEnabled() {
    final turnaroundEventSwipeEnabled = dataStore.getTurnaroundEventSwipeEnabled();
    if (turnaroundEventSwipeEnabled != null) {
      return Left(turnaroundEventSwipeEnabled);
    } else {
      return const Right(unit);
    }
  }

  @override
  Future<void> savePercentageRange({required String? percentageRange}) async {
    dataStore.savePercentageRange(percentageRange: percentageRange);
  }

  @override
  Future<void> saveTurnaroundEventSwipeEnabled({required bool turnaroundEventSwipeEnabled}) async {
    dataStore.saveTurnaroundEventSwipeEnabled(turnaroundEventSwipeEnabled: turnaroundEventSwipeEnabled);
  }
}
