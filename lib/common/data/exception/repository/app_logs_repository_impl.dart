import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/datastore/app_logs_datastore.dart';
import 'package:sto_mobile_v2/common/domain/exception/repository/app_logs_repository.dart';

@LazySingleton(as: IAppLogsRepository)
class AppLogsRepositoryImpl extends IAppLogsRepository {
  final IAppLogsDataStore appLogsDataStore;
  AppLogsRepositoryImpl(this.appLogsDataStore);

  @override
  Future<void> deleteAppLogs() {
    return appLogsDataStore.deleteAppLogs();
  }

  @override
  Future<List<AppLogsDTO>> getAllAppLogs() {
    return appLogsDataStore.getAllAppLogs();
  }

  @override
  Future<int> upsertAppLog({
    required AppLogsDTO appLog,
  }) {
    return appLogsDataStore.upsertAppLog(appLog: appLog);
  }
}
