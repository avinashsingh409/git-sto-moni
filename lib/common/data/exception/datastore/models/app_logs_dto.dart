import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';
part 'app_logs_dto.g.dart';

@collection
@JsonSerializable()
class AppLogsDTO {
  Id? id;
  DateTime? exceptionTime;
  String? exceptionString;

  AppLogsDTO({
    this.id,
    this.exceptionTime,
    this.exceptionString,
  });

  /// Convert UserInformation to JSON
  Map<String, dynamic> toJson() => _$AppLogsDTOToJson(this);

  factory AppLogsDTO.fromJson(Map<String, dynamic> json) => _$AppLogsDTOFromJson(json);
}
