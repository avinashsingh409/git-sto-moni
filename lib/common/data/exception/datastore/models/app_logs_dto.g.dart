// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_logs_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetAppLogsDTOCollection on Isar {
  IsarCollection<AppLogsDTO> get appLogsDTOs => this.collection();
}

const AppLogsDTOSchema = CollectionSchema(
  name: r'AppLogsDTO',
  id: -3016864466015595514,
  properties: {
    r'exceptionString': PropertySchema(
      id: 0,
      name: r'exceptionString',
      type: IsarType.string,
    ),
    r'exceptionTime': PropertySchema(
      id: 1,
      name: r'exceptionTime',
      type: IsarType.dateTime,
    )
  },
  estimateSize: _appLogsDTOEstimateSize,
  serialize: _appLogsDTOSerialize,
  deserialize: _appLogsDTODeserialize,
  deserializeProp: _appLogsDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _appLogsDTOGetId,
  getLinks: _appLogsDTOGetLinks,
  attach: _appLogsDTOAttach,
  version: '3.1.0+1',
);

int _appLogsDTOEstimateSize(
  AppLogsDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.exceptionString;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _appLogsDTOSerialize(
  AppLogsDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.exceptionString);
  writer.writeDateTime(offsets[1], object.exceptionTime);
}

AppLogsDTO _appLogsDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = AppLogsDTO(
    exceptionString: reader.readStringOrNull(offsets[0]),
    exceptionTime: reader.readDateTimeOrNull(offsets[1]),
    id: id,
  );
  return object;
}

P _appLogsDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readDateTimeOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _appLogsDTOGetId(AppLogsDTO object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _appLogsDTOGetLinks(AppLogsDTO object) {
  return [];
}

void _appLogsDTOAttach(IsarCollection<dynamic> col, Id id, AppLogsDTO object) {
  object.id = id;
}

extension AppLogsDTOQueryWhereSort
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QWhere> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension AppLogsDTOQueryWhere
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QWhereClause> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AppLogsDTOQueryFilter
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QFilterCondition> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'exceptionString',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'exceptionString',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'exceptionString',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'exceptionString',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'exceptionString',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'exceptionString',
        value: '',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionStringIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'exceptionString',
        value: '',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'exceptionTime',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'exceptionTime',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'exceptionTime',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'exceptionTime',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'exceptionTime',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition>
      exceptionTimeBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'exceptionTime',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AppLogsDTOQueryObject
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QFilterCondition> {}

extension AppLogsDTOQueryLinks
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QFilterCondition> {}

extension AppLogsDTOQuerySortBy
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QSortBy> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> sortByExceptionString() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionString', Sort.asc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy>
      sortByExceptionStringDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionString', Sort.desc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> sortByExceptionTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionTime', Sort.asc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> sortByExceptionTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionTime', Sort.desc);
    });
  }
}

extension AppLogsDTOQuerySortThenBy
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QSortThenBy> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> thenByExceptionString() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionString', Sort.asc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy>
      thenByExceptionStringDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionString', Sort.desc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> thenByExceptionTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionTime', Sort.asc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> thenByExceptionTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'exceptionTime', Sort.desc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }
}

extension AppLogsDTOQueryWhereDistinct
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QDistinct> {
  QueryBuilder<AppLogsDTO, AppLogsDTO, QDistinct> distinctByExceptionString(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'exceptionString',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AppLogsDTO, AppLogsDTO, QDistinct> distinctByExceptionTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'exceptionTime');
    });
  }
}

extension AppLogsDTOQueryProperty
    on QueryBuilder<AppLogsDTO, AppLogsDTO, QQueryProperty> {
  QueryBuilder<AppLogsDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<AppLogsDTO, String?, QQueryOperations>
      exceptionStringProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'exceptionString');
    });
  }

  QueryBuilder<AppLogsDTO, DateTime?, QQueryOperations>
      exceptionTimeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'exceptionTime');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppLogsDTO _$AppLogsDTOFromJson(Map<String, dynamic> json) => AppLogsDTO(
      id: json['id'] as int?,
      exceptionTime: json['exceptionTime'] == null
          ? null
          : DateTime.parse(json['exceptionTime'] as String),
      exceptionString: json['exceptionString'] as String?,
    );

Map<String, dynamic> _$AppLogsDTOToJson(AppLogsDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'exceptionTime': instance.exceptionTime?.toIso8601String(),
      'exceptionString': instance.exceptionString,
    };
