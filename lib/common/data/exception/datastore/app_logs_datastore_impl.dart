import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/exception/app_logs_dao.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/datastore/app_logs_datastore.dart';

@LazySingleton(as: IAppLogsDataStore)
class AppLogsDataStoreImpl extends IAppLogsDataStore {
  final AppLogsDAO appLogsDao;
  AppLogsDataStoreImpl(this.appLogsDao);

  @override
  Future<void> deleteAppLogs() async {
    await appLogsDao.deleteAppLogs();
  }

  @override
  Future<List<AppLogsDTO>> getAllAppLogs() async {
    return await appLogsDao.getAllAppLogs();
  }

  @override
  Future<int> upsertAppLog({
    required AppLogsDTO appLog,
  }) async {
    return await appLogsDao.upsertAppLog(appLog: appLog);
  }
}
