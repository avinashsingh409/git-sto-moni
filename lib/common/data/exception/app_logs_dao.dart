import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/exception/datastore/models/app_logs_dto.dart';

import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@lazySingleton
class AppLogsDAO {
  final isar = PersistenceModule.isar;

  AppLogsDAO();

  /// Isar Collection of AppLogsDto
  late IsarCollection<AppLogsDTO> collection = isar.appLogsDTOs;

  /// Delete All appLogs
  Future<void> deleteAppLogs() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Get All applogs
  Future<List<AppLogsDTO>> getAllAppLogs() async {
    final appLogs = collection.where().findAll();
    return appLogs;
  }

  /// Upsert AppLogs
  Future<int> upsertAppLog({
    required AppLogsDTO appLog,
  }) async {
    return isar.writeTxn(() async {
      return collection.put(appLog);
    });
  }
}
