import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/datastore/snyc_ticker_datastore.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/repository/sync_ticker_repository.dart';

@LazySingleton(as: ISyncTickerRepository)
class SyncTickerRepositoryImpl extends ISyncTickerRepository {
  final ISyncTickerDataStore syncTickerDataStore;
  SyncTickerRepositoryImpl(this.syncTickerDataStore);

  @override
  BehaviorSubject<bool> isSyncPending() {
    return syncTickerDataStore.isSyncPending();
  }

  @override
  Future<int> updateStatus() {
    return syncTickerDataStore.updateStatus();
  }

  @override
  Future<void> clearStatus() {
    return syncTickerDataStore.clearStatus();
  }
}
