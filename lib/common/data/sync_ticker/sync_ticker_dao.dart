import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/data/sync_ticker/datastore/models/sync_ticker_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class SyncTickerDAO {
  final isar = PersistenceModule.isar;

  SyncTickerDAO();

  /// Isar Collection of License
  late IsarCollection<SyncTickerDTO> collection = isar.syncTickerDTOs;

  /// Clear license
  Future<void> clearSyncTicker() async {
    return isar.writeTxn(() async {
      return collection.clear();
    });
  }

  /// Watch All Activities
  BehaviorSubject<bool> getSyncStatus() {
    final bstream = BehaviorSubject<bool>();
    final query = collection.where().anyId().build();
    final stream = query.watch(fireImmediately: true);
    stream.listen((item) {
      if (item.isNotEmpty) {
        bstream.add(true);
      } else {
        bstream.add(false);
      }
    });
    return bstream;
  }

  /// Upsert license
  Future<int> updateStatus() async {
    return isar.writeTxn(() async {
      return collection.put(SyncTickerDTO(id: 1, isSyncPending: true));
    });
  }
}
