import 'package:isar/isar.dart';
part 'sync_ticker_dto.g.dart';

@collection
class SyncTickerDTO {
  Id id; //
  bool? isSyncPending;

  SyncTickerDTO({
    this.id = 1,
    this.isSyncPending = false,
  });
}
