// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_ticker_dto.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetSyncTickerDTOCollection on Isar {
  IsarCollection<SyncTickerDTO> get syncTickerDTOs => this.collection();
}

const SyncTickerDTOSchema = CollectionSchema(
  name: r'SyncTickerDTO',
  id: -1752580664392946338,
  properties: {
    r'isSyncPending': PropertySchema(
      id: 0,
      name: r'isSyncPending',
      type: IsarType.bool,
    )
  },
  estimateSize: _syncTickerDTOEstimateSize,
  serialize: _syncTickerDTOSerialize,
  deserialize: _syncTickerDTODeserialize,
  deserializeProp: _syncTickerDTODeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _syncTickerDTOGetId,
  getLinks: _syncTickerDTOGetLinks,
  attach: _syncTickerDTOAttach,
  version: '3.1.0+1',
);

int _syncTickerDTOEstimateSize(
  SyncTickerDTO object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _syncTickerDTOSerialize(
  SyncTickerDTO object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.isSyncPending);
}

SyncTickerDTO _syncTickerDTODeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = SyncTickerDTO(
    id: id,
    isSyncPending: reader.readBoolOrNull(offsets[0]),
  );
  return object;
}

P _syncTickerDTODeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _syncTickerDTOGetId(SyncTickerDTO object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _syncTickerDTOGetLinks(SyncTickerDTO object) {
  return [];
}

void _syncTickerDTOAttach(
    IsarCollection<dynamic> col, Id id, SyncTickerDTO object) {
  object.id = id;
}

extension SyncTickerDTOQueryWhereSort
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QWhere> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension SyncTickerDTOQueryWhere
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QWhereClause> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension SyncTickerDTOQueryFilter
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QFilterCondition> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition>
      isSyncPendingIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'isSyncPending',
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition>
      isSyncPendingIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'isSyncPending',
      ));
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterFilterCondition>
      isSyncPendingEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isSyncPending',
        value: value,
      ));
    });
  }
}

extension SyncTickerDTOQueryObject
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QFilterCondition> {}

extension SyncTickerDTOQueryLinks
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QFilterCondition> {}

extension SyncTickerDTOQuerySortBy
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QSortBy> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy>
      sortByIsSyncPending() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isSyncPending', Sort.asc);
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy>
      sortByIsSyncPendingDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isSyncPending', Sort.desc);
    });
  }
}

extension SyncTickerDTOQuerySortThenBy
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QSortThenBy> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy>
      thenByIsSyncPending() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isSyncPending', Sort.asc);
    });
  }

  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QAfterSortBy>
      thenByIsSyncPendingDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isSyncPending', Sort.desc);
    });
  }
}

extension SyncTickerDTOQueryWhereDistinct
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QDistinct> {
  QueryBuilder<SyncTickerDTO, SyncTickerDTO, QDistinct>
      distinctByIsSyncPending() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isSyncPending');
    });
  }
}

extension SyncTickerDTOQueryProperty
    on QueryBuilder<SyncTickerDTO, SyncTickerDTO, QQueryProperty> {
  QueryBuilder<SyncTickerDTO, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<SyncTickerDTO, bool?, QQueryOperations> isSyncPendingProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isSyncPending');
    });
  }
}
