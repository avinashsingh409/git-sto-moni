import 'package:injectable/injectable.dart';
import 'package:rxdart/subjects.dart';
import 'package:sto_mobile_v2/common/data/sync_ticker/sync_ticker_dao.dart';
import 'package:sto_mobile_v2/common/domain/sync_ticker/datastore/snyc_ticker_datastore.dart';

@LazySingleton(as: ISyncTickerDataStore)
class SyncTickerDataStoreImpl extends ISyncTickerDataStore {
  final SyncTickerDAO syncTickerDAO;

  SyncTickerDataStoreImpl(this.syncTickerDAO);
  @override
  BehaviorSubject<bool> isSyncPending() {
    return syncTickerDAO.getSyncStatus();
  }

  @override
  Future<int> updateStatus() {
    return syncTickerDAO.updateStatus();
  }

  @override
  Future<void> clearStatus() {
    return syncTickerDAO.clearSyncTicker();
  }
}
