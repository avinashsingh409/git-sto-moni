import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/session/managers/session_preference_manager.dart';
import 'package:sto_mobile_v2/common/domain/app_switcher/datastore/app_switcher_datastore.dart';

@LazySingleton(as: IAppSwitcherDataStore)
class AppSwitcherDataStoreImpl extends IAppSwitcherDataStore {
  final SessionPreferenceManager sessionPreferenceManager;
  AppSwitcherDataStoreImpl(this.sessionPreferenceManager);

  @override
  String? getSelectedApp() {
    return sessionPreferenceManager.selectedAppFeature;
  }

  @override
  Future<void> saveSelectedApp({required String selectedAppFeature}) async {
    sessionPreferenceManager.selectedAppFeature = selectedAppFeature;
  }
}
