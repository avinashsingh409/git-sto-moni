import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/app_switcher/datastore/enums/app_features.dart';
import 'package:sto_mobile_v2/common/domain/app_switcher/datastore/app_switcher_datastore.dart';
import 'package:sto_mobile_v2/common/domain/app_switcher/repository/app_switcher_repository.dart';

@LazySingleton(as: IAppSwitcherRepository)
class AppSwitcherRepositoryImpl extends IAppSwitcherRepository {
  final IAppSwitcherDataStore appSwitcherDataStore;
  AppSwitcherRepositoryImpl(this.appSwitcherDataStore);

  @override
  AppFeatures getSelectedApp() {
    final selectedAppFeature = appSwitcherDataStore.getSelectedApp();
    switch (selectedAppFeature) {
      case plannerEnumStringVal:
        return AppFeatures.planner;
      case executionEnumStringVal:
        return AppFeatures.execution;
      default:
        return AppFeatures.notSelected;
    }
  }

  @override
  Future<void> saveSelectedApp({required AppFeatures appFeatures}) {
    switch (appFeatures) {
      case AppFeatures.planner:
        return appSwitcherDataStore.saveSelectedApp(selectedAppFeature: AppFeatures.planner.name);
      case AppFeatures.execution:
        return appSwitcherDataStore.saveSelectedApp(selectedAppFeature: AppFeatures.execution.name);
      case AppFeatures.notSelected:
        return appSwitcherDataStore.saveSelectedApp(selectedAppFeature: AppFeatures.notSelected.name);
    }
  }
}

const String plannerEnumStringVal = "planner";
const String executionEnumStringVal = "execution";
