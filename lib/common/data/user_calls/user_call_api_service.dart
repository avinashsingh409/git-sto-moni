import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_call_response.dart';
import 'package:sto_mobile_v2/config/constants/api_endpoints.dart';

@injectable
class UserCallApiService {
  final Dio dio;

  UserCallApiService(this.dio);

  Future<UserCallResponse?> getUserCall() async {
    final response = await dio.get(ApiEndPoints.user);
    if (response.statusCode == 200) {
      final parsedResponse = UserCallResponse.fromJson(response.data);
      return parsedResponse;
    }
    return null;
  }
}
