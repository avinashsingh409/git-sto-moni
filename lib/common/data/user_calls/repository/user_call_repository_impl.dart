import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_dto.dart';
import 'package:sto_mobile_v2/common/domain/exception/connection_exception.dart';
import 'package:sto_mobile_v2/common/domain/exception/user_call_exception.dart';
import 'package:sto_mobile_v2/common/domain/user_call/datastore/user_call_datastore.dart';
import 'package:sto_mobile_v2/common/domain/user_call/repository/user_call_repository.dart';

@LazySingleton(as: IUserCallRepository)
class UserCallRepositoryImpl extends IUserCallRepository {
  final IUserCallDataStore userCallDataStore;
  UserCallRepositoryImpl(this.userCallDataStore);

  @override
  Future<Either<Unit, UserCallException>> getAndCacheUserCall() async {
    try {
      final userModel = await userCallDataStore.getUserCall();
      if (userModel != null) {
        await userCallDataStore.upsertUser(userDTO: UserDTO.fromModel(userResponse: userModel));
      } else {
        return const Right(UserCallException.unexpectedError());
      }
    } on DioError catch (error) {
      if (DioErrorType.connectTimeout == error.type) {
        return const Right(UserCallException.connectTimeOut());
      } else if (DioErrorType.other == error.type) {
        if (error.message.contains('SocketException')) {
          return const Right(UserCallException.socketException());
        }
        return const Right(UserCallException.unexpectedError());
      }
    } on ConnectionException catch (_) {
      return const Right(UserCallException.socketException());
    }
    return const Left(unit);
  }

  @override
  bool getIsSwipeEnabled() {
    return userCallDataStore.getIsSwipeEnabled();
  }
}
