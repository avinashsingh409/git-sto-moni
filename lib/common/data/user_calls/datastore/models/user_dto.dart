import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_call_response.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_settings.dart';
part 'user_dto.g.dart';

@collection
class UserDTO {
  Id id = 0;
  final String username;
  final String name;
  final String firstName;
  final String lastName;
  final String guid;
  final bool isSwipeEnabled;

  UserDTO({
    required this.username,
    required this.name,
    required this.firstName,
    required this.lastName,
    required this.guid,
    required this.isSwipeEnabled,
  });

  factory UserDTO.fromModel({required UserCallResponse userResponse}) {
    final sessionDTO = UserDTO(
      username: userResponse.username,
      name: userResponse.name,
      firstName: userResponse.firstName,
      lastName: userResponse.lastName,
      guid: userResponse.guid,
      isSwipeEnabled: getSwipedEnabledBool(userSettings: userResponse.userSettings),
    );
    return sessionDTO;
  }
}

bool getSwipedEnabledBool({required List<UserSettings>? userSettings}) {
  if (userSettings != null) {
    final swipeSetting = userSettings.where((element) => element.setting == 38).firstOrNull;
    if (swipeSetting != null && swipeSetting.value == 'true') {
      return false;
    }
  }
  return true;
}
