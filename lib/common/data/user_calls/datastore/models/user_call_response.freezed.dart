// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_call_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserCallResponse _$UserCallResponseFromJson(Map<String, dynamic> json) {
  return _UserCallResponse.fromJson(json);
}

/// @nodoc
mixin _$UserCallResponse {
  int get id => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get firstName => throw _privateConstructorUsedError;
  String get lastName => throw _privateConstructorUsedError;
  String get guid => throw _privateConstructorUsedError;
  List<UserSettings>? get userSettings => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserCallResponseCopyWith<UserCallResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCallResponseCopyWith<$Res> {
  factory $UserCallResponseCopyWith(
          UserCallResponse value, $Res Function(UserCallResponse) then) =
      _$UserCallResponseCopyWithImpl<$Res, UserCallResponse>;
  @useResult
  $Res call(
      {int id,
      String username,
      String name,
      String firstName,
      String lastName,
      String guid,
      List<UserSettings>? userSettings});
}

/// @nodoc
class _$UserCallResponseCopyWithImpl<$Res, $Val extends UserCallResponse>
    implements $UserCallResponseCopyWith<$Res> {
  _$UserCallResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? username = null,
    Object? name = null,
    Object? firstName = null,
    Object? lastName = null,
    Object? guid = null,
    Object? userSettings = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      guid: null == guid
          ? _value.guid
          : guid // ignore: cast_nullable_to_non_nullable
              as String,
      userSettings: freezed == userSettings
          ? _value.userSettings
          : userSettings // ignore: cast_nullable_to_non_nullable
              as List<UserSettings>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserCallResponseCopyWith<$Res>
    implements $UserCallResponseCopyWith<$Res> {
  factory _$$_UserCallResponseCopyWith(
          _$_UserCallResponse value, $Res Function(_$_UserCallResponse) then) =
      __$$_UserCallResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String username,
      String name,
      String firstName,
      String lastName,
      String guid,
      List<UserSettings>? userSettings});
}

/// @nodoc
class __$$_UserCallResponseCopyWithImpl<$Res>
    extends _$UserCallResponseCopyWithImpl<$Res, _$_UserCallResponse>
    implements _$$_UserCallResponseCopyWith<$Res> {
  __$$_UserCallResponseCopyWithImpl(
      _$_UserCallResponse _value, $Res Function(_$_UserCallResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? username = null,
    Object? name = null,
    Object? firstName = null,
    Object? lastName = null,
    Object? guid = null,
    Object? userSettings = freezed,
  }) {
    return _then(_$_UserCallResponse(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      guid: null == guid
          ? _value.guid
          : guid // ignore: cast_nullable_to_non_nullable
              as String,
      userSettings: freezed == userSettings
          ? _value._userSettings
          : userSettings // ignore: cast_nullable_to_non_nullable
              as List<UserSettings>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserCallResponse implements _UserCallResponse {
  const _$_UserCallResponse(
      {required this.id,
      required this.username,
      required this.name,
      required this.firstName,
      required this.lastName,
      required this.guid,
      required final List<UserSettings>? userSettings})
      : _userSettings = userSettings;

  factory _$_UserCallResponse.fromJson(Map<String, dynamic> json) =>
      _$$_UserCallResponseFromJson(json);

  @override
  final int id;
  @override
  final String username;
  @override
  final String name;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String guid;
  final List<UserSettings>? _userSettings;
  @override
  List<UserSettings>? get userSettings {
    final value = _userSettings;
    if (value == null) return null;
    if (_userSettings is EqualUnmodifiableListView) return _userSettings;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'UserCallResponse(id: $id, username: $username, name: $name, firstName: $firstName, lastName: $lastName, guid: $guid, userSettings: $userSettings)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserCallResponse &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.firstName, firstName) ||
                other.firstName == firstName) &&
            (identical(other.lastName, lastName) ||
                other.lastName == lastName) &&
            (identical(other.guid, guid) || other.guid == guid) &&
            const DeepCollectionEquality()
                .equals(other._userSettings, _userSettings));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, username, name, firstName,
      lastName, guid, const DeepCollectionEquality().hash(_userSettings));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserCallResponseCopyWith<_$_UserCallResponse> get copyWith =>
      __$$_UserCallResponseCopyWithImpl<_$_UserCallResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserCallResponseToJson(
      this,
    );
  }
}

abstract class _UserCallResponse implements UserCallResponse {
  const factory _UserCallResponse(
      {required final int id,
      required final String username,
      required final String name,
      required final String firstName,
      required final String lastName,
      required final String guid,
      required final List<UserSettings>? userSettings}) = _$_UserCallResponse;

  factory _UserCallResponse.fromJson(Map<String, dynamic> json) =
      _$_UserCallResponse.fromJson;

  @override
  int get id;
  @override
  String get username;
  @override
  String get name;
  @override
  String get firstName;
  @override
  String get lastName;
  @override
  String get guid;
  @override
  List<UserSettings>? get userSettings;
  @override
  @JsonKey(ignore: true)
  _$$_UserCallResponseCopyWith<_$_UserCallResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
