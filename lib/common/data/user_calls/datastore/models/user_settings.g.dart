// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserSettings _$$_UserSettingsFromJson(Map<String, dynamic> json) =>
    _$_UserSettings(
      id: json['id'] as int,
      userId: json['userId'] as int,
      setting: json['setting'] as int,
      value: json['value'] as String,
    );

Map<String, dynamic> _$$_UserSettingsToJson(_$_UserSettings instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'setting': instance.setting,
      'value': instance.value,
    };
