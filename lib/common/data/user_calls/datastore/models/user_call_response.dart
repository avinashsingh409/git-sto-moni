import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_settings.dart';

part 'user_call_response.freezed.dart';
part 'user_call_response.g.dart';

@freezed
abstract class UserCallResponse with _$UserCallResponse {
  const factory UserCallResponse({
    required int id,
    required String username,
    required String name,
    required String firstName,
    required String lastName,
    required String guid,
    required List<UserSettings>? userSettings,
  }) = _UserCallResponse;

  factory UserCallResponse.fromJson(Map<String, dynamic> json) => _$UserCallResponseFromJson(json);
}
