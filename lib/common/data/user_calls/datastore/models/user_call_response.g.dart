// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_call_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserCallResponse _$$_UserCallResponseFromJson(Map<String, dynamic> json) =>
    _$_UserCallResponse(
      id: json['id'] as int,
      username: json['username'] as String,
      name: json['name'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      guid: json['guid'] as String,
      userSettings: (json['userSettings'] as List<dynamic>?)
          ?.map((e) => UserSettings.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_UserCallResponseToJson(_$_UserCallResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'name': instance.name,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'guid': instance.guid,
      'userSettings': instance.userSettings,
    };
