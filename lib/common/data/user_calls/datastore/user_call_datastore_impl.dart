import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_call_response.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_dto.dart';
import 'package:sto_mobile_v2/common/data/user_calls/user_call_api_service.dart';
import 'package:sto_mobile_v2/common/data/user_calls/user_call_dao.dart';
import 'package:sto_mobile_v2/common/domain/exception/dio_error_extension.dart';
import 'package:sto_mobile_v2/common/domain/user_call/datastore/user_call_datastore.dart';

@LazySingleton(as: IUserCallDataStore)
class UserCallDataStoreImpl extends IUserCallDataStore {
  final UserCallApiService userCallApiService;
  final UserCallDAO userCallDAO;

  UserCallDataStoreImpl(this.userCallApiService, this.userCallDAO);

  @override
  Future<UserCallResponse?> getUserCall() {
    return userCallApiService.getUserCall().catchDioException();
  }

  @override
  Future<void> upsertUser({required UserDTO userDTO}) {
    return userCallDAO.upsertUser(userDTO: userDTO);
  }

  @override
  bool getIsSwipeEnabled() {
    return userCallDAO.getIsSwipeEnabled();
  }

  @override
  Future<void> deleteCachedUser() {
    return userCallDAO.deleteCachedUser();
  }
}
