import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:sto_mobile_v2/common/data/user_calls/datastore/models/user_dto.dart';
import 'package:sto_mobile_v2/injectable/modules/persistence/persistence_module.dart';

@injectable
class UserCallDAO {
  final isar = PersistenceModule.isar;

  UserCallDAO();

  /// Isar Collection of User
  late final IsarCollection<UserDTO> _userCollection = isar.userDTOs;

  /// Get User
  UserDTO? getCachedUser() {
    final userDTO = _userCollection.getSync(0);
    return userDTO;
  }

  bool getIsSwipeEnabled() {
    final userDTO = _userCollection.getSync(0);
    final isSwipeEnabled = userDTO?.isSwipeEnabled;
    if (isSwipeEnabled != null) {
      return isSwipeEnabled;
    } else {
      return true;
    }
  }

  // Upsert User
  Future<int> upsertUser({required UserDTO userDTO}) async {
    return isar.writeTxn(() {
      return _userCollection.put(userDTO);
    });
  }

  // Delete User
  Future<void> deleteCachedUser() async {
    return isar.writeTxn(() async {
      return _userCollection.clear();
    });
  }
}
