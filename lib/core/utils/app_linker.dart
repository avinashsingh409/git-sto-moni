
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sto_mobile_v2/common/domain/exception/app_linker_exception.dart';
import 'package:sto_mobile_v2/common/domain/license/repository/license_repository.dart';
import 'package:sto_mobile_v2/execution/data/wbs/datastore/models/dto/tree_node_dto.dart';
import 'package:sto_mobile_v2/execution/domain/turnaround_activity/repository/turnaround_activity_repository.dart';
import 'package:sto_mobile_v2/execution/domain/wbs/repository/wbs_repository.dart';
import 'package:sto_mobile_v2/injectable/injection.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/datastore/models/workpackage_dto.dart';
import 'package:sto_mobile_v2/planner/data/workpackage/selected_workpackage.dart';
import 'package:sto_mobile_v2/planner/domain/workpackage/repository/workpackage_repository.dart';

@lazySingleton
class AppLinker {
  AppLinker();

  ILicenseRepository get _licenseRepository => getIt<ILicenseRepository>();
  IWorkpackageRepository get _workPackageRepository => getIt<IWorkpackageRepository>();
  IWbsRepository get _wbsRepository => getIt<IWbsRepository>();
  ITurnAroundActivityRepository get _activityRepository => getIt<ITurnAroundActivityRepository>();
  
  // Given a work package ID, return the associated DTO or an exception
  Future<Either<WorkpackageDTO, AppLinkerException>> getLinkedWorkPackage(int workPackageId) async {
    final licenses = await _licenseRepository.getCachedLicense();
    if (licenses == null || !(licenses.planner ?? false)) {
      return const Right(AppLinkerException.notLicensed());
    }

    final workPackage = await _workPackageRepository.getSingleWorkPackage(workPackageId: workPackageId);
    if (workPackage == null) {
      return const Right(AppLinkerException.noWorkPackageFound());
    }
    SelectedWorkPackage().changeSelectedPackage(newDynamicColumns: workPackage.dynamicColumns, newWorkpackageId: workPackage.id, selectedWorkpackage: workPackage);
    return Left(workPackage);
  }

  // Given a WBS code, return the associated node or an exception
  Future<Either<TreeNodeDTO, AppLinkerException>> getLinkedWBS(int workPackageId) async {
    final licenses = await _licenseRepository.getCachedLicense();
    if (licenses == null || !(licenses.execution ?? false)) {
      return const Right(AppLinkerException.notLicensed());
    }

    final activity = await _activityRepository.getActivityByWorkPackageId(workPackageId: workPackageId);
    if (activity == null) {
      return const Right(AppLinkerException.noActivitiesFound());
    }

    final wbsCode = activity.wbsRoute?.elementAtOrNull(1);

    final wbsNode = await _wbsRepository.getWbsItemByCode(wbsCode: wbsCode ?? "");
    if (wbsNode == null) {
      return const Right(AppLinkerException.noWBSFound());
    }
    return Left(wbsNode);
  }
}
