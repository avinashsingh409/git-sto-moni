import 'package:intl/intl.dart';

// class to handle date conversion and formatting app-wide
class DateFormatter {

  // handle parsing of display dates, which must retain the "A" suffix if present
  static String handleDisplayDate({required String dateTimeString, bool long = true}) {
    RegExp regex = RegExp(r'(\d{4}-?\d\d-?\d\d(\s|T)\d\d:?\d\d:?\d\d)');
    if (dateTimeString.contains(regex)) {
      int len = dateTimeString.length;
      if (dateTimeString.substring(len - 2) == " A") {
        return "${mapAndReturnDate(dateTimeString: dateTimeString.substring(0, len - 2), long: long)} A";
      }
      return mapAndReturnDate(dateTimeString: dateTimeString, long: long);
    }
    return dateTimeString;
  }

  // convert the date to local time and return in the correct format
  static String mapAndReturnDate({required String dateTimeString, required bool long}) {
    try {
      DateTime parseDate = DateTime.parse(dateTimeString).toUtc();
      var outputDate = DateFormat("yy-MM-dd HH:mm:ss").parse(parseDate.toString());
      DateFormat dateFormat = long ? DateFormat('MM/dd/yyyy h:mm a') : DateFormat('MM/dd/yy h:mm');
      var formattedLocalTime = dateFormat.format(outputDate);
      return formattedLocalTime;
    } catch (_) {
      return '';
    }
  }
}
