// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(
        _current != null, 'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `STO Mobile`
  String get title {
    return Intl.message(
      'STO Mobile',
      name: 'title',
      desc: 'Title for the application',
      args: [],
    );
  }

  /// `STO Execution`
  String get stoxTitle {
    return Intl.message(
      'STO Execution',
      name: 'stoxTitle',
      desc: '',
      args: [],
    );
  }

  /// `STO Isolation`
  String get stoiTitle {
    return Intl.message(
      'STO Isolation',
      name: 'stoiTitle',
      desc: '',
      args: [],
    );
  }

  /// `Activities`
  String get activityListScreen {
    return Intl.message(
      'Activities',
      name: 'activityListScreen',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settingsScreen {
    return Intl.message(
      'Settings',
      name: 'settingsScreen',
      desc: 'Title for the Settings screen',
      args: [],
    );
  }

  /// `Log in`
  String get loginButton {
    return Intl.message(
      'Log in',
      name: 'loginButton',
      desc: '',
      args: [],
    );
  }

  /// `An error occurred when trying to log in. Please try again. If this error continues to occur, please contact your administrator.`
  String get loginError {
    return Intl.message(
      'An error occurred when trying to log in. Please try again. If this error continues to occur, please contact your administrator.',
      name: 'loginError',
      desc: 'Text for when user could not log in',
      args: [],
    );
  }

  /// `Log out`
  String get logoutButton {
    return Intl.message(
      'Log out',
      name: 'logoutButton',
      desc: 'button text of the log out button',
      args: [],
    );
  }

  /// `Not found`
  String get notFound {
    return Intl.message(
      'Not found',
      name: 'notFound',
      desc: 'Text that is shown when a route is requested that does not exist',
      args: [],
    );
  }

  /// `Progress`
  String get progress {
    return Intl.message(
      'Progress',
      name: 'progress',
      desc: 'The percentage of the work that has been completed for an activity',
      args: [],
    );
  }

  /// `Changes saved`
  String get changesSaved {
    return Intl.message(
      'Changes saved',
      name: 'changesSaved',
      desc: 'A message to indicate that user-made changes have been saved',
      args: [],
    );
  }

  /// `There are no activities to display`
  String get noActivities {
    return Intl.message(
      'There are no activities to display',
      name: 'noActivities',
      desc: 'A message to inform the user that the activity list is empty',
      args: [],
    );
  }

  /// `An error occurred while synchronizing data. Some changes may not have been synchronized to the server. Please try again. If this error continues to occur, please contact your administrator.`
  String get syncError {
    return Intl.message(
      'An error occurred while synchronizing data. Some changes may not have been synchronized to the server. Please try again. If this error continues to occur, please contact your administrator.',
      name: 'syncError',
      desc: '',
      args: [],
    );
  }

  /// `Could not connect to server.`
  String get errorCouldNotConnect {
    return Intl.message(
      'Could not connect to server.',
      name: 'errorCouldNotConnect',
      desc: '',
      args: [],
    );
  }

  /// `Start date`
  String get startDate {
    return Intl.message(
      'Start date',
      name: 'startDate',
      desc: 'Activity Details Planned Start Date',
      args: [],
    );
  }

  /// `End date`
  String get endDate {
    return Intl.message(
      'End date',
      name: 'endDate',
      desc: 'Activity Details Planned Start Date',
      args: [],
    );
  }

  /// `Unable to work`
  String get unableToWorkButton {
    return Intl.message(
      'Unable to work',
      name: 'unableToWorkButton',
      desc: 'Button text of the unable to work button on the activity details screen',
      args: [],
    );
  }

  /// `Details`
  String get activityDetails {
    return Intl.message(
      'Details',
      name: 'activityDetails',
      desc: 'caption of the tab menu item for details on the activity details screen',
      args: [],
    );
  }

  /// `Comments`
  String get activityComments {
    return Intl.message(
      'Comments',
      name: 'activityComments',
      desc: 'caption of the tab menu item for comments on the activity details screen',
      args: [],
    );
  }

  /// `Related`
  String get activityRelations {
    return Intl.message(
      'Related',
      name: 'activityRelations',
      desc: 'caption of the tab menu item for activities related to the activity in the details screen',
      args: [],
    );
  }

  /// `Most Recent`
  String get commentsMostRecent {
    return Intl.message(
      'Most Recent',
      name: 'commentsMostRecent',
      desc: 'caption of the sub tab menu item for the most recent comments on the activity details screen',
      args: [],
    );
  }

  /// `Your Comments`
  String get commentsYourComments {
    return Intl.message(
      'Your Comments',
      name: 'commentsYourComments',
      desc: 'caption of the sub tab menu item for the your comments on the activity details screen',
      args: [],
    );
  }

  /// `Post`
  String get activityPostButton {
    return Intl.message(
      'Post',
      name: 'activityPostButton',
      desc: 'Button text for the post button on the activityDetailsScreen',
      args: [],
    );
  }

  /// `Show more comments`
  String get commentsShowMore {
    return Intl.message(
      'Show more comments',
      name: 'commentsShowMore',
      desc: 'caption of the show more comments button',
      args: [],
    );
  }

  /// `Show fewer comments`
  String get commentsShowFewer {
    return Intl.message(
      'Show fewer comments',
      name: 'commentsShowFewer',
      desc: 'caption of the show less comments button',
      args: [],
    );
  }

  /// `Show more`
  String get commentWidgetShowMore {
    return Intl.message(
      'Show more',
      name: 'commentWidgetShowMore',
      desc: '',
      args: [],
    );
  }

  /// `Show less`
  String get commentWidgetShowLess {
    return Intl.message(
      'Show less',
      name: 'commentWidgetShowLess',
      desc: '',
      args: [],
    );
  }

  /// `Write your comment here...`
  String get commentsHintText {
    return Intl.message(
      'Write your comment here...',
      name: 'commentsHintText',
      desc: '',
      args: [],
    );
  }

  /// `Planned start date`
  String get plannedStartDate {
    return Intl.message(
      'Planned start date',
      name: 'plannedStartDate',
      desc: '',
      args: [],
    );
  }

  /// `Planned end date`
  String get plannedEndDate {
    return Intl.message(
      'Planned end date',
      name: 'plannedEndDate',
      desc: '',
      args: [],
    );
  }

  /// `Duration`
  String get duration {
    return Intl.message(
      'Duration',
      name: 'duration',
      desc: '',
      args: [],
    );
  }

  /// `Loading data...`
  String get loadingData {
    return Intl.message(
      'Loading data...',
      name: 'loadingData',
      desc: 'A message saying that data is being loaded',
      args: [],
    );
  }

  /// `Synchronize data`
  String get syncData {
    return Intl.message(
      'Synchronize data',
      name: 'syncData',
      desc: 'Title for the sync screen',
      args: [],
    );
  }

  /// `Sync`
  String get sync {
    return Intl.message(
      'Sync',
      name: 'sync',
      desc: 'Label for the sync menu button',
      args: [],
    );
  }

  /// `Last synced:`
  String get lastSynced {
    return Intl.message(
      'Last synced:',
      name: 'lastSynced',
      desc: 'Label for the last date and time at which the sync operation was performed',
      args: [],
    );
  }

  /// `Cannot connect. Please check your internet connection and contact your administrator.`
  String get needInternetToSync {
    return Intl.message(
      'Cannot connect. Please check your internet connection and contact your administrator.',
      name: 'needInternetToSync',
      desc: 'Warning that explains that the sync button is disabled when there is no internet connection',
      args: [],
    );
  }

  /// `Testing connection`
  String get testingInternetConnection {
    return Intl.message(
      'Testing connection',
      name: 'testingInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `Proceed to Activities`
  String get proceedToActivities {
    return Intl.message(
      'Proceed to Activities',
      name: 'proceedToActivities',
      desc: 'Message that prompts a user to proceed to the Activity List screen]',
      args: [],
    );
  }

  /// `days`
  String get durationDays {
    return Intl.message(
      'days',
      name: 'durationDays',
      desc: '',
      args: [],
    );
  }

  /// `Previous activity is not finished`
  String get activityNotFinished {
    return Intl.message(
      'Previous activity is not finished',
      name: 'activityNotFinished',
      desc: '',
      args: [],
    );
  }

  /// `Site is not safe`
  String get siteNotSafe {
    return Intl.message(
      'Site is not safe',
      name: 'siteNotSafe',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get submitButton {
    return Intl.message(
      'Submit',
      name: 'submitButton',
      desc: '',
      args: [],
    );
  }

  /// `Please select a reason why the work is blocked`
  String get selectReasonWarning {
    return Intl.message(
      'Please select a reason why the work is blocked',
      name: 'selectReasonWarning',
      desc: '',
      args: [],
    );
  }

  /// `Please write a short description in the description field before submitting`
  String get otherReasonWarning {
    return Intl.message(
      'Please write a short description in the description field before submitting',
      name: 'otherReasonWarning',
      desc: '',
      args: [],
    );
  }

  /// `Please specify...`
  String get unableToWorkHintText {
    return Intl.message(
      'Please specify...',
      name: 'unableToWorkHintText',
      desc: '',
      args: [],
    );
  }

  /// `Please add any additional information...`
  String get unableToWorkAdditionalInfoHintText {
    return Intl.message(
      'Please add any additional information...',
      name: 'unableToWorkAdditionalInfoHintText',
      desc: '',
      args: [],
    );
  }

  /// `Other reason...`
  String get unableToWorkOtherReasonText {
    return Intl.message(
      'Other reason...',
      name: 'unableToWorkOtherReasonText',
      desc: '',
      args: [],
    );
  }

  /// `Select reason(s): `
  String get unableToWorkHeaderQuestion {
    return Intl.message(
      'Select reason(s): ',
      name: 'unableToWorkHeaderQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Reason for Blockage`
  String get reasonForBlock {
    return Intl.message(
      'Reason for Blockage',
      name: 'reasonForBlock',
      desc: '',
      args: [],
    );
  }

  /// `Activity blocked`
  String get blockAlertText {
    return Intl.message(
      'Activity Blocked',
      name: 'blockAlertText',
      desc: '',
      args: [],
    );
  }

  /// `View/Unblock`
  String get blockAlertSeeMore {
    return Intl.message(
      'View/Unblock',
      name: 'blockAlertSeeMore',
      desc: '',
      args: [],
    );
  }

  /// `No comments here.`
  String get noComments {
    return Intl.message(
      'No comments here.',
      name: 'noComments',
      desc: '',
      args: [],
    );
  }

  /// `Reason`
  String get blockDetailsReason {
    return Intl.message(
      'Reason',
      name: 'blockDetailsReason',
      desc: '',
      args: [],
    );
  }

  /// `Submission`
  String get blockDetailsSubmission {
    return Intl.message(
      'Submission',
      name: 'blockDetailsSubmission',
      desc: '',
      args: [],
    );
  }

  /// `Able to work`
  String get blockDetailsAbleToWork {
    return Intl.message(
      'Able to work',
      name: 'blockDetailsAbleToWork',
      desc: '',
      args: [],
    );
  }

  /// `Custom percentage`
  String get chooseCustomPercentage {
    return Intl.message(
      'Custom percentage',
      name: 'chooseCustomPercentage',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Input chosen`
  String get nonValidInput {
    return Intl.message(
      'Invalid Input chosen',
      name: 'nonValidInput',
      desc: '',
      args: [],
    );
  }

  /// `No Input given`
  String get noInput {
    return Intl.message(
      'No Input given',
      name: 'noInput',
      desc: '',
      args: [],
    );
  }

  /// `QR Code Scanner`
  String get qrCodeScannerTitle {
    return Intl.message(
      'QR Code Scanner',
      name: 'qrCodeScannerTitle',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure?`
  String get coreConfirmDialogTitle {
    return Intl.message(
      'Are you sure?',
      name: 'coreConfirmDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure that you want to connect to`
  String get qrCodeConfirmDialogContent {
    return Intl.message(
      'Are you sure that you want to connect to',
      name: 'qrCodeConfirmDialogContent',
      desc: '',
      args: [],
    );
  }

  /// `Failed to scan QR Code`
  String get qrCodeScannerErrorTitle {
    return Intl.message(
      'Failed to scan QR Code',
      name: 'qrCodeScannerErrorTitle',
      desc: '',
      args: [],
    );
  }

  String get coreWarningDialogOkay {
    return Intl.message(
      'Okay',
      name: 'coreWarningDialogOkay',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Okay`
  String get qrCodeScannerErrorDesc {
    return Intl.message(
      'Invalid QR Code',
      name: 'invalidQRCode',
      desc: '',
      args: [],
    );
  }

  /// `No valid QR Code`
  String get qrCodeWarningDialogTitle {
    return Intl.message(
      'No valid QR Code',
      name: 'qrCodeWarningDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Please scan a valid QR Code`
  String get qrCodeWarningDialogContent {
    return Intl.message(
      'Please scan a valid QR Code',
      name: 'qrCodeWarningDialogContent',
      desc: '',
      args: [],
    );
  }

  /// `Type in...`
  String get customPercentageHintText {
    return Intl.message(
      'Type in...',
      name: 'customPercentageHintText',
      desc: '',
      args: [],
    );
  }

  /// `Set custom percentage`
  String get customPercentageDescriptiveText {
    return Intl.message(
      'Set custom percentage',
      name: 'customPercentageDescriptiveText',
      desc: '',
      args: [],
    );
  }

  /// `Select STO Event`
  String get selectStoEvent {
    return Intl.message(
      'Select STO Event',
      name: 'selectStoEvent',
      desc: '',
      args: [],
    );
  }

  /// `Clear selection`
  String get clearSelection {
    return Intl.message(
      'Clear selection',
      name: 'clearSelection',
      desc: '',
      args: [],
    );
  }

  /// `The sync did not finish successfully. Activities might not be up-to-date`
  String get syncNotSuccessful {
    return Intl.message(
      'The sync did not finish successfully. Activities might not be up-to-date',
      name: 'syncNotSuccessful',
      desc: '',
      args: [],
    );
  }

  /// `Currently syncing activities`
  String get isSyncing {
    return Intl.message(
      'Currently syncing activities',
      name: 'isSyncing',
      desc: '',
      args: [],
    );
  }

  /// `Do you want to delete the selected picture(s)?`
  String get warningDeleteSelectedPicturesContent {
    return Intl.message(
      'Do you want to delete the selected picture(s)?',
      name: 'warningDeleteSelectedPicturesContent',
      desc: '',
      args: [],
    );
  }

  /// `Error retrieving the Turnaround Events.`
  String get stoxErrorGettingTurnaroundEvents {
    return Intl.message(
      'Error retrieving the Turnaround Events.',
      name: 'stoxErrorGettingTurnaroundEvents',
      desc: '',
      args: [],
    );
  }

  /// `STO Isolation`
  String get coreStoIsolation {
    return Intl.message(
      'STO Isolation',
      name: 'coreStoIsolation',
      desc: '',
      args: [],
    );
  }

  /// `STO Execution`
  String get coreStoExecution {
    return Intl.message(
      'STO Execution',
      name: 'coreStoExecution',
      desc: '',
      args: [],
    );
  }

  /// `Your device cannot connect to the internet. Please check your internet settings. If this error continues to occur, please contact your administrator.`
  String get noInternetConnection {
    return Intl.message(
      'Your device cannot connect to the internet. Please check your internet settings. If this error continues to occur, please contact your administrator.',
      name: 'noInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `Open Camera`
  String get stoxOpenCamera {
    return Intl.message(
      'Open Camera',
      name: 'stoxOpenCamera',
      desc: '',
      args: [],
    );
  }

  /// `Open Album`
  String get stoxOpenAlbum {
    return Intl.message(
      'Open Album',
      name: 'stoxOpenAlbum',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get stoiSearchHint {
    return Intl.message(
      'Search',
      name: 'stoiSearchHint',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get stoiHomeTitle {
    return Intl.message(
      'Home',
      name: 'stoiHomeTitle',
      desc: '',
      args: [],
    );
  }

  /// `Search Isolation Points`
  String get stoiSearchIsoPoint {
    return Intl.message(
      'Search Isolation Points',
      name: 'stoiSearchIsoPoint',
      desc: '',
      args: [],
    );
  }

  /// `Search Isolation Lists`
  String get stoiSearchIsoList {
    return Intl.message(
      'Search Isolation Lists',
      name: 'stoiSearchIsoList',
      desc: '',
      args: [],
    );
  }

  /// `Scan Isolation Point`
  String get stoiScanIsoPoint {
    return Intl.message(
      'Scan Isolation Point',
      name: 'stoiScanIsoPoint',
      desc: '',
      args: [],
    );
  }

  /// `Favorites`
  String get favorites {
    return Intl.message(
      'Favorites',
      name: 'favorites',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get stoiSettingsTitle {
    return Intl.message(
      'Settings',
      name: 'stoiSettingsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Isolation Point`
  String get stoiIsolationPointLabel {
    return Intl.message(
      'Isolation Point',
      name: 'stoiIsolationPointLabel',
      desc: '',
      args: [],
    );
  }

  /// `Opened`
  String get stoiOpenStatusOpened {
    return Intl.message(
      'Opened',
      name: 'stoiOpenStatusOpened',
      desc: '',
      args: [],
    );
  }

  /// `Closed`
  String get stoiOpenStatusClosed {
    return Intl.message(
      'Closed',
      name: 'stoiOpenStatusClosed',
      desc: '',
      args: [],
    );
  }

  /// `Status`
  String get status {
    return Intl.message(
      'Status',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `Phase`
  String get phase {
    return Intl.message(
      'Phase',
      name: 'phase',
      desc: '',
      args: [],
    );
  }

  /// `Isolation Points Search`
  String get stoiIsolationPointSearchTitle {
    return Intl.message(
      'Isolation Points Search',
      name: 'stoiIsolationPointSearchTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sort by status`
  String get stoiIsolationPointSortByStatus {
    return Intl.message(
      'Sort by status',
      name: 'stoiIsolationPointSortByStatus',
      desc: '',
      args: [],
    );
  }

  /// `Results`
  String get stoiResults {
    return Intl.message(
      'Results',
      name: 'stoiResults',
      desc: '',
      args: [],
    );
  }

  /// `No items found`
  String get stoiNoResults {
    return Intl.message(
      'No items found',
      name: 'stoiNoResults',
      desc: '',
      args: [],
    );
  }

  /// `Not Released`
  String get stoiActivityStatusNotReleased {
    return Intl.message(
      'Not Released',
      name: 'stoiActivityStatusNotReleased',
      desc: '',
      args: [],
    );
  }

  /// ` Ready For Pre Quality Check`
  String get stoiActivityStatusReadyForPreQualityCheck {
    return Intl.message(
      ' Ready For Pre Quality Check',
      name: 'stoiActivityStatusReadyForPreQualityCheck',
      desc: '',
      args: [],
    );
  }

  /// `Ready To Start Work`
  String get stoiActivityStatusReadyToStartWork {
    return Intl.message(
      'Ready To Start Work',
      name: 'stoiActivityStatusReadyToStartWork',
      desc: '',
      args: [],
    );
  }

  /// `Confirmation Required`
  String get stoiActivityStatusConfirmationRequired {
    return Intl.message(
      'Confirmation Required',
      name: 'stoiActivityStatusConfirmationRequired',
      desc: '',
      args: [],
    );
  }

  /// `Executing Work`
  String get stoiActivityStatusExecutingWork {
    return Intl.message(
      'Executing Work',
      name: 'stoiActivityStatusExecutingWork',
      desc: '',
      args: [],
    );
  }

  /// `Waiting For Finish`
  String get stoiActivityStatusWaitingForFinish {
    return Intl.message(
      'Waiting For Finish',
      name: 'stoiActivityStatusWaitingForFinish',
      desc: '',
      args: [],
    );
  }

  /// `Ready For Visual Quality Check`
  String get stoiActivityStatusReadyForVisualQualityCheck {
    return Intl.message(
      'Ready For Visual Quality Check',
      name: 'stoiActivityStatusReadyForVisualQualityCheck',
      desc: '',
      args: [],
    );
  }

  /// `Ready For Post Quality Check`
  String get stoiActivityStatusReadyForPostQualityCheck {
    return Intl.message(
      'Ready For Post Quality Check',
      name: 'stoiActivityStatusReadyForPostQualityCheck',
      desc: '',
      args: [],
    );
  }

  /// `Ready For Final Quality Check`
  String get stoiActivityStatusReadyForFinalQualityCheck {
    return Intl.message(
      'Ready For Final Quality Check',
      name: 'stoiActivityStatusReadyForFinalQualityCheck',
      desc: '',
      args: [],
    );
  }

  /// `Ready`
  String get stoiActivityStatusReady {
    return Intl.message(
      'Ready',
      name: 'stoiActivityStatusReady',
      desc: '',
      args: [],
    );
  }

  /// `Not Applicable`
  String get stoiActivityStatusNotApplicable {
    return Intl.message(
      'Not Applicable',
      name: 'stoiActivityStatusNotApplicable',
      desc: '',
      args: [],
    );
  }

  /// `On hold`
  String get stoiActivityStatusOnHold {
    return Intl.message(
      'On hold',
      name: 'stoiActivityStatusOnHold',
      desc: '',
      args: [],
    );
  }

  /// `Rework`
  String get stoiActivityStatusRework {
    return Intl.message(
      'Rework',
      name: 'stoiActivityStatusRework',
      desc: '',
      args: [],
    );
  }

  /// `List Type`
  String get stoiIsolationListTypeLabel {
    return Intl.message(
      'List Type',
      name: 'stoiIsolationListTypeLabel',
      desc: '',
      args: [],
    );
  }

  /// `Contractor`
  String get contractor {
    return Intl.message(
      'Contractor',
      name: 'contractor',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get description {
    return Intl.message(
      'Description',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `List Name`
  String get stoiIsolationListNameLabel {
    return Intl.message(
      'List Name',
      name: 'stoiIsolationListNameLabel',
      desc: '',
      args: [],
    );
  }

  /// `Approval Date`
  String get stoiIsolationListApprovalDateLabel {
    return Intl.message(
      'Approval Date',
      name: 'stoiIsolationListApprovalDateLabel',
      desc: '',
      args: [],
    );
  }

  /// `Release Date`
  String get stoiIsolationListReleaseDateLabel {
    return Intl.message(
      'Release Date',
      name: 'stoiIsolationListReleaseDateLabel',
      desc: '',
      args: [],
    );
  }

  /// `Created`
  String get stoiIsolationListStatusCreated {
    return Intl.message(
      'Created',
      name: 'stoiIsolationListStatusCreated',
      desc: '',
      args: [],
    );
  }

  /// `Reviewed`
  String get stoiIsolationListStatusReviewed {
    return Intl.message(
      'Reviewed',
      name: 'stoiIsolationListStatusReviewed',
      desc: '',
      args: [],
    );
  }

  /// `Approved`
  String get stoiIsolationListStatusApproved {
    return Intl.message(
      'Approved',
      name: 'stoiIsolationListStatusApproved',
      desc: '',
      args: [],
    );
  }

  /// `Released`
  String get stoiIsolationListStatusReleased {
    return Intl.message(
      'Released',
      name: 'stoiIsolationListStatusReleased',
      desc: '',
      args: [],
    );
  }

  /// `Isolation List Search`
  String get stoiIsolationListSearchTitle {
    return Intl.message(
      'Isolation List Search',
      name: 'stoiIsolationListSearchTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sort by alphabet`
  String get stoiIsolationListSortByAlphabet {
    return Intl.message(
      'Sort by alphabet',
      name: 'stoiIsolationListSortByAlphabet',
      desc: '',
      args: [],
    );
  }

  /// `Shared Isolation`
  String get sharedIsolation {
    return Intl.message(
      'Shared Isolation',
      name: 'sharedIsolation',
      desc: '',
      args: [],
    );
  }

  /// `Potential Shared Isolation`
  String get potentialSharedIsolation {
    return Intl.message(
      'Potential Shared Isolation',
      name: 'potentialSharedIsolation',
      desc: '',
      args: [],
    );
  }

  /// `View Details`
  String get viewDetails {
    return Intl.message(
      'View Details',
      name: 'viewDetails',
      desc: '',
      args: [],
    );
  }

  /// `Current position`
  String get currentPosition {
    return Intl.message(
      'Current position',
      name: 'currentPosition',
      desc: '',
      args: [],
    );
  }

  /// `Activity`
  String get activity {
    return Intl.message(
      'Activity',
      name: 'activity',
      desc: '',
      args: [],
    );
  }

  /// `Type of Action`
  String get typeOfAction {
    return Intl.message(
      'Type of Action',
      name: 'typeOfAction',
      desc: '',
      args: [],
    );
  }

  /// `Current stage`
  String get currentStage {
    return Intl.message(
      'Current stage',
      name: 'currentStage',
      desc: '',
      args: [],
    );
  }

  /// `Up next`
  String get upNext {
    return Intl.message(
      'Up next',
      name: 'upNext',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get confirm {
    return Intl.message(
      'Confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Revert`
  String get revert {
    return Intl.message(
      'Revert',
      name: 'revert',
      desc: '',
      args: [],
    );
  }

  /// `Information`
  String get information {
    return Intl.message(
      'Information',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Hazards`
  String get hazards {
    return Intl.message(
      'Hazards',
      name: 'hazards',
      desc: '',
      args: [],
    );
  }

  /// `Normal Position`
  String get normalPosition {
    return Intl.message(
      'Normal Position',
      name: 'normalPosition',
      desc: '',
      args: [],
    );
  }

  /// `General`
  String get general {
    return Intl.message(
      'General',
      name: 'general',
      desc: '',
      args: [],
    );
  }

  /// `Reference ID`
  String get referenceId {
    return Intl.message(
      'Reference ID',
      name: 'referenceId',
      desc: '',
      args: [],
    );
  }

  /// `Isolation Type`
  String get isolationType {
    return Intl.message(
      'Isolation Type',
      name: 'isolationType',
      desc: '',
      args: [],
    );
  }

  /// `Size`
  String get size {
    return Intl.message(
      'Size',
      name: 'size',
      desc: '',
      args: [],
    );
  }

  /// `Diameter`
  String get diameter {
    return Intl.message(
      'Diameter',
      name: 'diameter',
      desc: '',
      args: [],
    );
  }

  /// `Pressure`
  String get pressure {
    return Intl.message(
      'Pressure',
      name: 'pressure',
      desc: '',
      args: [],
    );
  }

  /// `Extended`
  String get extended {
    return Intl.message(
      'Extended',
      name: 'extended',
      desc: '',
      args: [],
    );
  }

  /// `Pipe Specification`
  String get pipeSpecification {
    return Intl.message(
      'Pipe Specification',
      name: 'pipeSpecification',
      desc: '',
      args: [],
    );
  }

  /// `Connection`
  String get connection {
    return Intl.message(
      'Connection',
      name: 'connection',
      desc: '',
      args: [],
    );
  }

  /// `Nozzle Number`
  String get nozzleNumber {
    return Intl.message(
      'Nozzle Number',
      name: 'nozzleNumber',
      desc: '',
      args: [],
    );
  }

  /// `Location`
  String get location {
    return Intl.message(
      'Location',
      name: 'location',
      desc: '',
      args: [],
    );
  }

  /// `Elevation`
  String get elevation {
    return Intl.message(
      'Elevation',
      name: 'elevation',
      desc: '',
      args: [],
    );
  }

  /// `Grid`
  String get grid {
    return Intl.message(
      'Grid',
      name: 'grid',
      desc: '',
      args: [],
    );
  }

  /// `Exact Elevation`
  String get exactElevation {
    return Intl.message(
      'Exact Elevation',
      name: 'exactElevation',
      desc: '',
      args: [],
    );
  }

  /// `Isolation Data`
  String get isolationData {
    return Intl.message(
      'Isolation Data',
      name: 'isolationData',
      desc: '',
      args: [],
    );
  }

  /// `None`
  String get none {
    return Intl.message(
      'None',
      name: 'none',
      desc: '',
      args: [],
    );
  }

  /// `Open`
  String get open {
    return Intl.message(
      'Open',
      name: 'open',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Isolation List`
  String get isolationList {
    return Intl.message(
      'Isolation List',
      name: 'isolationList',
      desc: '',
      args: [],
    );
  }

  /// `Decisions`
  String get decisions {
    return Intl.message(
      'Decisions',
      name: 'decisions',
      desc: '',
      args: [],
    );
  }

  /// `Created By`
  String get createdBy {
    return Intl.message(
      'Created By',
      name: 'createdBy',
      desc: '',
      args: [],
    );
  }

  /// `On`
  String get on {
    return Intl.message(
      'On',
      name: 'on',
      desc: '',
      args: [],
    );
  }

  /// `Reviewed By`
  String get reviewedBy {
    return Intl.message(
      'Reviewed By',
      name: 'reviewedBy',
      desc: '',
      args: [],
    );
  }

  /// `Approved By`
  String get approvedBy {
    return Intl.message(
      'Approved By',
      name: 'approvedBy',
      desc: '',
      args: [],
    );
  }

  /// `Released By`
  String get releasedBy {
    return Intl.message(
      'Released By',
      name: 'releasedBy',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Order No.`
  String get orderNumber {
    return Intl.message(
      'Order No.',
      name: 'orderNumber',
      desc: '',
      args: [],
    );
  }

  /// `Isolation List Type`
  String get isolationListType {
    return Intl.message(
      'Isolation List Type',
      name: 'isolationListType',
      desc: '',
      args: [],
    );
  }

  /// `Type`
  String get type {
    return Intl.message(
      'Type',
      name: 'type',
      desc: '',
      args: [],
    );
  }

  /// `Site`
  String get site {
    return Intl.message(
      'Site',
      name: 'site',
      desc: '',
      args: [],
    );
  }

  /// `Label`
  String get label {
    return Intl.message(
      'Label',
      name: 'label',
      desc: '',
      args: [],
    );
  }

  /// `Plant Data Information`
  String get plantDataInformation {
    return Intl.message(
      'Plant Data Information',
      name: 'plantDataInformation',
      desc: '',
      args: [],
    );
  }

  /// `Process Area`
  String get processArea {
    return Intl.message(
      'Process Area',
      name: 'processArea',
      desc: '',
      args: [],
    );
  }

  /// `Unit`
  String get unit {
    return Intl.message(
      'Unit',
      name: 'unit',
      desc: '',
      args: [],
    );
  }

  /// `Additional Information`
  String get additionalInformation {
    return Intl.message(
      'Additional Information',
      name: 'additionalInformation',
      desc: '',
      args: [],
    );
  }

  /// `Try Again`
  String get tryAgain {
    return Intl.message(
      'Try Again',
      name: 'tryAgain',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong`
  String get somethingWentWrong {
    return Intl.message(
      'Something went wrong',
      name: 'somethingWentWrong',
      desc: '',
      args: [],
    );
  }

  /// `The application has encountered an unknown error. Please try again later.`
  String get defaultErrorMessage {
    return Intl.message(
      'The application has encountered an unknown error. Please try again later.',
      name: 'defaultErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `The server has encountered an error.`
  String get internalServerError {
    return Intl.message(
      'The server has encountered an error.',
      name: 'internalServerError',
      desc: '',
      args: [],
    );
  }

  /// `Isolation Points`
  String get isolationPoints {
    return Intl.message(
      'Isolation Points',
      name: 'isolationPoints',
      desc: '',
      args: [],
    );
  }

  /// `Go to Point`
  String get goToPoint {
    return Intl.message(
      'Go to Point',
      name: 'goToPoint',
      desc: '',
      args: [],
    );
  }

  /// `You are not authorised to perform this action. If this error continues to occur, or if it is a mistake, please contact your administrator.`
  String get unauthorisedOrForbiddenErrorMessage {
    return Intl.message(
      'You are not authorised to perform this action. If this error continues to occur, or if it is a mistake, please contact your administrator.',
      name: 'unauthorisedOrForbiddenErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `You are trying to retrieve or edit data that does not exist. If this error continues to occur, or if it is a mistake, please contact your administrator.`
  String get notFoundErrorMessage {
    return Intl.message(
      'You are trying to retrieve or edit data that does not exist. If this error continues to occur, or if it is a mistake, please contact your administrator.',
      name: 'notFoundErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `An error occurred while trying to read data from the device's internal storage. Please try again. If this error continues to occur, please contact your administrator.`
  String get readLocalDbErrorMessage {
    return Intl.message(
      'An error occurred while trying to read data from the device\'s internal storage. Please try again. If this error continues to occur, please contact your administrator.',
      name: 'readLocalDbErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `An error occurred while trying to write data to the device's internal storage. Please try again. If this error continues to occur, please contact your administrator.`
  String get writeLocalDbErrorMessage {
    return Intl.message(
      'An error occurred while trying to write data to the device\'s internal storage. Please try again. If this error continues to occur, please contact your administrator.',
      name: 'writeLocalDbErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `Unable to read from Hive box, not opened yet!`
  String get localDbNotOpenErrorMessage {
    return Intl.message(
      'Unable to read from Hive box, not opened yet!',
      name: 'localDbNotOpenErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `No function defined to determine key!`
  String get noFunctionDefinedToDetermineKey {
    return Intl.message(
      'No function defined to determine key!',
      name: 'noFunctionDefinedToDetermineKey',
      desc: '',
      args: [],
    );
  }

  /// `No app configuration found in local storage!`
  String get noAppConfigurationFound {
    return Intl.message(
      'No app configuration found in local storage!',
      name: 'noAppConfigurationFound',
      desc: '',
      args: [],
    );
  }

  /// `No token found in local storage!`
  String get noTokenFound {
    return Intl.message(
      'No token found in local storage!',
      name: 'noTokenFound',
      desc: '',
      args: [],
    );
  }

  /// `No user found in local storage!`
  String get noUserFound {
    return Intl.message(
      'No user found in local storage!',
      name: 'noUserFound',
      desc: '',
      args: [],
    );
  }

  /// `Could not open '{0}'`
  String get couldNotLaunchUri {
    return Intl.message(
      'Could not open \'{0}\'',
      name: 'couldNotLaunchUri',
      desc: '',
      args: [],
    );
  }

  /// `Failed to execute {0} request for '{1}`
  String get apiErrorMessage {
    return Intl.message(
      'Failed to execute {0} request for \'{1}',
      name: 'apiErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `Could not get user id!`
  String get couldNotGetUserId {
    return Intl.message(
      'Could not get user id!',
      name: 'couldNotGetUserId',
      desc: '',
      args: [],
    );
  }

  /// `Could not get user information!`
  String get couldNotGetUserInformation {
    return Intl.message(
      'Could not get user information!',
      name: 'couldNotGetUserInformation',
      desc: '',
      args: [],
    );
  }

  /// `Could not get user!`
  String get couldNotGetUser {
    return Intl.message(
      'Could not get user!',
      name: 'couldNotGetUser',
      desc: '',
      args: [],
    );
  }

  /// `Could not authenticate current session!`
  String get couldNotAuthenticate {
    return Intl.message(
      'Could not authenticate current session!',
      name: 'couldNotAuthenticate',
      desc: '',
      args: [],
    );
  }

  /// `Could not log out current session!`
  String get couldNotLogout {
    return Intl.message(
      'Could not log out current session!',
      name: 'couldNotLogout',
      desc: '',
      args: [],
    );
  }

  /// `Could not get default attachment!`
  String get couldNotGetDefaultAttachment {
    return Intl.message(
      'Could not get default attachment!',
      name: 'couldNotGetDefaultAttachment',
      desc: '',
      args: [],
    );
  }

  /// `Could not push outbox item to server!`
  String get couldNotPushOutboxItemToServer {
    return Intl.message(
      'Could not push outbox item to server!',
      name: 'couldNotPushOutboxItemToServer',
      desc: '',
      args: [],
    );
  }

  /// `Could not get turnaround events associated to the current user!`
  String get couldNotGetTurnaroundEventAssociations {
    return Intl.message(
      'Could not get turnaround events associated to the current user!',
      name: 'couldNotGetTurnaroundEventAssociations',
      desc: '',
      args: [],
    );
  }

  /// `Could not get turnaround events!`
  String get couldNotGetTurnaroundEvents {
    return Intl.message(
      'Could not get turnaround events!',
      name: 'couldNotGetTurnaroundEvents',
      desc: '',
      args: [],
    );
  }

  /// `A fatal error occurred, causing the application to close. If this error continues to occur, please contact your administrator.`
  String get fatalError {
    return Intl.message(
      'A fatal error occurred, causing the application to close. If this error continues to occur, please contact your administrator.',
      name: 'fatalError',
      desc: '',
      args: [],
    );
  }

  /// `Exit`
  String get exit {
    return Intl.message(
      'Exit',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `Shared Lists`
  String get sharedListsTitle {
    return Intl.message(
      'Shared Lists',
      name: 'sharedListsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Filter Shared Lists`
  String get sharedListsFiltersTitle {
    return Intl.message(
      'Filter Shared Lists',
      name: 'sharedListsFiltersTitle',
      desc: '',
      args: [],
    );
  }

  /// `Filter`
  String get sharedListsFilter {
    return Intl.message(
      'Filter',
      name: 'sharedListsFilter',
      desc: '',
      args: [],
    );
  }

  /// `Activities`
  String get sharedListsActivitiesTitle {
    return Intl.message(
      'Activities',
      name: 'sharedListsActivitiesTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sub-phase`
  String get subPhase {
    return Intl.message(
      'Sub-phase',
      name: 'subPhase',
      desc: '',
      args: [],
    );
  }

  /// `Remove from favorites?`
  String get removeFromFavorites {
    return Intl.message(
      'Remove from favorites?',
      name: 'removeFromFavorites',
      desc: '',
      args: [],
    );
  }

  /// `Add to favorites?`
  String get addToFavorites {
    return Intl.message(
      'Add to favorites?',
      name: 'addToFavorites',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `You currently have no favorites`
  String get noFavorites {
    return Intl.message(
      'You currently have no favorites',
      name: 'noFavorites',
      desc: '',
      args: [],
    );
  }

  /// `No AppConfig found`
  String get noAppConfig {
    return Intl.message(
      'No AppConfig found',
      name: 'noAppConfig',
      desc: '',
      args: [],
    );
  }

  /// `Tried to retrieve AppConfig from local DB, but received null. The application should initialize the AppConfig, but it has not.`
  String get noAppConfigFatalException {
    return Intl.message(
      'Tried to retrieve AppConfig from local DB, but received null. The application should initialize the AppConfig, but it has not.',
      name: 'noAppConfigFatalException',
      desc: '',
      args: [],
    );
  }

  /// `A conversion error occurred, thus the incoming data could not be processed. If this error continues to occur, please contact your administrator.`
  String get conversionError {
    return Intl.message(
      'A conversion error occurred, thus the incoming data could not be processed. If this error continues to occur, please contact your administrator.',
      name: 'conversionError',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to STO Mobile`
  String get welcomeMessage {
    return Intl.message(
      'Welcome to STO Mobile',
      name: 'welcomeMessage',
      desc: '',
      args: [],
    );
  }

  /// `To connect the device and log in, please scan the QR code by tapping the button below:`
  String get welcomeExplanation {
    return Intl.message(
      'To connect the device and log in, please scan the QR code by tapping the button below:',
      name: 'welcomeExplanation',
      desc: '',
      args: [],
    );
  }

  /// `Not sure where to find the QR code?`
  String get qrCodeExplanation {
    return Intl.message(
      'Not sure where to find the QR code?',
      name: 'qrCodeExplanation',
      desc: '',
      args: [],
    );
  }

  /// `Administrators`
  String get administrators {
    return Intl.message(
      'Administrators',
      name: 'administrators',
      desc: '',
      args: [],
    );
  }

  /// `You can access the QR code in the STO Admin section of the web application`
  String get qrAdminMessage {
    return Intl.message(
      'You can access the QR code in the STO Admin section of the web application',
      name: 'qrAdminMessage',
      desc: '',
      args: [],
    );
  }

  /// `Remaining users`
  String get remainingUsers {
    return Intl.message(
      'Remaining users',
      name: 'remainingUsers',
      desc: '',
      args: [],
    );
  }

  /// `Please contact your IT support`
  String get qrRemainingUsersMessage {
    return Intl.message(
      'Please contact your IT support',
      name: 'qrRemainingUsersMessage',
      desc: '',
      args: [],
    );
  }

  /// `Scan QR code`
  String get scanQrCode {
    return Intl.message(
      'Scan QR code',
      name: 'scanQrCode',
      desc: '',
      args: [],
    );
  }

  /// `Add Comment`
  String get addComment {
    return Intl.message(
      'Add Comment',
      name: 'addComment',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a reason for decreasing the progress percentage.`
  String get provideNegativeProgressReason {
    return Intl.message(
      'Please provide a reason for decreasing the progress percentage.',
      name: 'provideNegativeProgressReason',
      desc: '',
      args: [],
    );
  }

  /// `Select an Event`
  String get stoEventDropDownHintTxt {
    return Intl.message(
      'Select an Event',
      name: 'stoEventDropDownHintTxt',
      desc: '',
      args: [],
    );
  }

  /// `Search...`
  String get stoEventDropDownSearchBoxHintText {
    return Intl.message(
      'Search...',
      name: 'stoEventDropDownSearchBoxHintText',
      desc: '',
      args: [],
    );
  }

  /// `Select STO app to continue`
  String get appPreferenceSwitcherTitle {
    return Intl.message(
      'Select STO app to continue',
      name: 'appPreferenceSwitcherTitle',
      desc: '',
      args: [],
    );
  }

  /// `Selection will be default on sign in`
  String get appPreferenceSwitcherSubtitle {
    return Intl.message(
      'Selection will be default on sign in',
      name: 'appPreferenceSwitcherSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Switch to {appname}`
  String appPreferenceSwitcherPopupTitle(Object appname) {
    return Intl.message(
      'Switch to $appname',
      name: 'appPreferenceSwitcherPopupTitle',
      desc: '',
      args: [appname],
    );
  }

  /// `Are you sure you would like to switch and change your sign in preference app to {appname}?`
  String appPreferenceSwitcherPopupSubtitle(Object appname) {
    return Intl.message(
      'Are you sure you would like to switch and change your sign in preference app to $appname?',
      name: 'appPreferenceSwitcherPopupSubtitle',
      desc: '',
      args: [appname],
    );
  }

  /// `No app preference found, please contact your Administrator`
  String get appPreferenceError {
    return Intl.message(
      'No app preference found, please contact your Administrator',
      name: 'appPreferenceError',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get popupConfirm {
    return Intl.message(
      'Confirm',
      name: 'popupConfirm',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get popupCancel {
    return Intl.message(
      'Cancel',
      name: 'popupCancel',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to {actionName}?`
  String sureYouWantToActionName(Object actionName) {
    return Intl.message(
      'Are you sure you want to $actionName?',
      name: 'sureYouWantToActionName',
      desc: '',
      args: [actionName],
    );
  }

  /// `You are about to confirm that this stage is finished.`
  String get isolationPointConfirmDialogContent {
    return Intl.message(
      'You are about to confirm that this stage is finished.',
      name: 'isolationPointConfirmDialogContent',
      desc: '',
      args: [],
    );
  }

  /// `Please provide a reason for `
  String get isolationBlockedReasonTitleWithoutBlockType {
    return Intl.message(
      'Please provide a reason for ',
      name: 'isolationBlockedReasonTitleWithoutBlockType',
      desc: '',
      args: [],
    );
  }

  /// `Select all that apply`
  String get selectAllThatApply {
    return Intl.message(
      'Select all that apply',
      name: 'selectAllThatApply',
      desc: '',
      args: [],
    );
  }

  /// `Please add additional details ...`
  String get pleaseAddAdditionalDetails {
    return Intl.message(
      'Please add additional details ...',
      name: 'pleaseAddAdditionalDetails',
      desc: '',
      args: [],
    );
  }

  /// `Unable to Work`
  String get unableToWork {
    return Intl.message(
      'Unable to Work',
      name: 'unableToWork',
      desc: '',
      args: [],
    );
  }

  /// `Rework`
  String get rework {
    return Intl.message(
      'Rework',
      name: 'rework',
      desc: '',
      args: [],
    );
  }

  /// `An error occurred changing the progress on your work. If this error continues to occur, or if it is a mistake, please contact your administrator.`
  String get unableToChangeProgress {
    return Intl.message(
      'An error occurred changing the progress on your work. If this error continues to occur, or if it is a mistake, please contact your administrator.',
      name: 'unableToChangeProgress',
      desc: '',
      args: [],
    );
  }

  /// `The following reasons are given:`
  String get followingReasonsAreGiven {
    return Intl.message(
      'The following reasons are given:',
      name: 'followingReasonsAreGiven',
      desc: '',
      args: [],
    );
  }

  /// `Please enter at least {minimumAmountOfCharacters} characters here...`
  String enterMinimumAmountCharacters(Object minimumAmountOfCharacters) {
    return Intl.message(
      'Please enter at least $minimumAmountOfCharacters characters here...',
      name: 'enterMinimumAmountCharacters',
      desc: '',
      args: [minimumAmountOfCharacters],
    );
  }

  /// `Start work`
  String get startWork {
    return Intl.message(
      'Start work',
      name: 'startWork',
      desc: '',
      args: [],
    );
  }

  /// `Finish work`
  String get finishWork {
    return Intl.message(
      'Finish work',
      name: 'finishWork',
      desc: '',
      args: [],
    );
  }

  /// `Confirm check`
  String get confirmCheck {
    return Intl.message(
      'Confirm check',
      name: 'confirmCheck',
      desc: '',
      args: [],
    );
  }

  /// `Date Created`
  String get dateCreated {
    return Intl.message(
      'Date Created',
      name: 'dateCreated',
      desc: '',
      args: [],
    );
  }

  /// `Finish Date`
  String get finishDate {
    return Intl.message(
      'Finish Date',
      name: 'finishDate',
      desc: '',
      args: [],
    );
  }

  /// `Remaining Duration`
  String get remainingDuration {
    return Intl.message(
      'Remaining Duration',
      name: 'remainingDuration',
      desc: '',
      args: [],
    );
  }

  /// `Percent Complete`
  String get percentComplete {
    return Intl.message(
      'Percent Complete',
      name: 'percentComplete',
      desc: '',
      args: [],
    );
  }

  /// `Percent Complete`
  String get displayedPercentComplete {
    return Intl.message(
      'Percent Complete',
      name: 'displayedPercentComplete',
      desc: '',
      args: [],
    );
  }

  /// `Changed Percent Complete`
  String get changedPercentComplete {
    return Intl.message(
      'Changed Percent Complete',
      name: 'changedPercentComplete',
      desc: '',
      args: [],
    );
  }

  /// `Actual Start Date`
  String get actualStartDate {
    return Intl.message(
      'Actual Start Date',
      name: 'actualStartDate',
      desc: '',
      args: [],
    );
  }

  /// `Actual Finish Date`
  String get actualFinishDate {
    return Intl.message(
      'Actual Finish Date',
      name: 'actualFinishDate',
      desc: '',
      args: [],
    );
  }

  /// `Host Name`
  String get hostName {
    return Intl.message(
      'Host Name',
      name: 'hostName',
      desc: '',
      args: [],
    );
  }

  /// `Project Id`
  String get projectId {
    return Intl.message(
      'Project Id',
      name: 'projectId',
      desc: '',
      args: [],
    );
  }

  /// `Calendar Name`
  String get calendarName {
    return Intl.message(
      'Calendar Name',
      name: 'calendarName',
      desc: '',
      args: [],
    );
  }

  /// `Total Float`
  String get totalFloat {
    return Intl.message(
      'Total Float',
      name: 'totalFloat',
      desc: '',
      args: [],
    );
  }

  /// `Planned Duration`
  String get plannedDuration {
    return Intl.message(
      'Planned Duration',
      name: 'plannedDuration',
      desc: '',
      args: [],
    );
  }

  /// `ChangedActualStartDate`
  String get changedActualStartDate {
    return Intl.message(
      'ChangedActualStartDate',
      name: 'changedActualStartDate',
      desc: '',
      args: [],
    );
  }

  /// `Changed Actual Finish Date`
  String get changedActualFinishDate {
    return Intl.message(
      'Changed Actual Finish Date',
      name: 'changedActualFinishDate',
      desc: '',
      args: [],
    );
  }

  /// `Modified By Name`
  String get modifiedByName {
    return Intl.message(
      'Modified By Name',
      name: 'modifiedByName',
      desc: '',
      args: [],
    );
  }

  /// `Planned Finish Date`
  String get plannedFinishDate {
    return Intl.message(
      'Planned Finish Date',
      name: 'plannedFinishDate',
      desc: '',
      args: [],
    );
  }

  /// `Remaining Early Start Date`
  String get remainingEarlyStartDate {
    return Intl.message(
      'Remaining Early Start Date',
      name: 'remainingEarlyStartDate',
      desc: '',
      args: [],
    );
  }

  /// `Remaining Early Finish Date`
  String get remainingEarlyFinishDate {
    return Intl.message(
      'Remaining Early Finish Date',
      name: 'remainingEarlyFinishDate',
      desc: '',
      args: [],
    );
  }

  /// `Budgeted Labor Units`
  String get budgetedLaborUnits {
    return Intl.message(
      'Budgeted Labor Units',
      name: 'budgetedLaborUnits',
      desc: '',
      args: [],
    );
  }

  /// `Budgeted Non Labor Units`
  String get budgetedNonLaborUnits {
    return Intl.message(
      'Budgeted Non Labor Units',
      name: 'budgetedNonLaborUnits',
      desc: '',
      args: [],
    );
  }

  /// `Remaining Labor Units`
  String get remainingLaborUnits {
    return Intl.message(
      'Remaining Labor Units',
      name: 'remainingLaborUnits',
      desc: '',
      args: [],
    );
  }

  /// `Remaining Non Labor Units`
  String get remainingNonLaborUnits {
    return Intl.message(
      'Remaining Non Labor Units',
      name: 'remainingNonLaborUnits',
      desc: '',
      args: [],
    );
  }

  /// `Date Modified`
  String get dateModified {
    return Intl.message(
      'Date Modified',
      name: 'dateModified',
      desc: '',
      args: [],
    );
  }

  /// `Unable To Work DateTime`
  String get unableToWorkDateTime {
    return Intl.message(
      'Unable To Work DateTime',
      name: 'unableToWorkDateTime',
      desc: '',
      args: [],
    );
  }

  /// `Rejected Percent Complete`
  String get rejectedPercentComplete {
    return Intl.message(
      'Rejected Percent Complete',
      name: 'rejectedPercentComplete',
      desc: '',
      args: [],
    );
  }

  /// `Rejected Start Date`
  String get rejectedStartDate {
    return Intl.message(
      'Rejected Start Date',
      name: 'rejectedStartDate',
      desc: '',
      args: [],
    );
  }

  /// `Rejected Finish Date`
  String get rejectedFinishDate {
    return Intl.message(
      'Rejected Finish Date',
      name: 'rejectedFinishDate',
      desc: '',
      args: [],
    );
  }

  /// `Pre Approved`
  String get preApproved {
    return Intl.message(
      'Pre Approved',
      name: 'preApproved',
      desc: '',
      args: [],
    );
  }

  /// `Confirm work`
  String get confirmWork {
    return Intl.message(
      'Confirm work',
      name: 'confirmWork',
      desc: '',
      args: [],
    );
  }

  /// `The activity progress has been updated`
  String get activityProgressUpdated {
    return Intl.message(
      'The activity progress has been updated',
      name: 'activityProgressUpdated',
      desc: '',
      args: [],
    );
  }

  /// `This activity is already completed`
  String get activityAlreadyCompleted {
    return Intl.message(
      'This activity is already completed',
      name: 'activityAlreadyCompleted',
      desc: '',
      args: [],
    );
  }

  /// `Activities`
  String get activities {
    return Intl.message(
      'Activities',
      name: 'activities',
      desc: '',
      args: [],
    );
  }

  /// `Group`
  String get group {
    return Intl.message(
      'Group',
      name: 'group',
      desc: '',
      args: [],
    );
  }

  /// `Group Activities`
  String get groupActivities {
    return Intl.message(
      'Group Activities',
      name: 'groupActivities',
      desc: '',
      args: [],
    );
  }

  /// `Level 1`
  String get level1 {
    return Intl.message(
      'Level 1',
      name: 'level1',
      desc: '',
      args: [],
    );
  }

  /// `Level 2`
  String get level2 {
    return Intl.message(
      'Level 2',
      name: 'level2',
      desc: '',
      args: [],
    );
  }

  /// `Level 3`
  String get level3 {
    return Intl.message(
      'Level 3',
      name: 'level3',
      desc: '',
      args: [],
    );
  }

  /// `Group by`
  String get levelGroupField {
    return Intl.message(
      'Group by',
      name: 'levelGroupField',
      desc: '',
      args: [],
    );
  }

  /// `Sort by`
  String get levelSortField {
    return Intl.message(
      'Sort by',
      name: 'levelSortField',
      desc: '',
      args: [],
    );
  }

  /// `Sort direction `
  String get levelSortDirection {
    return Intl.message(
      'Sort direction ',
      name: 'levelSortDirection',
      desc: '',
      args: [],
    );
  }

  /// `Apply`
  String get apply {
    return Intl.message(
      'Apply',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `Please select variables for grouping levels in order.`
  String get setPreviousLevelsError {
    return Intl.message(
      'Please select variables for grouping levels in order.',
      name: 'setPreviousLevelsError',
      desc: '',
      args: [],
    );
  }

  /// `Undefined`
  String get undefined {
    return Intl.message(
      'Undefined',
      name: 'undefined',
      desc: '',
      args: [],
    );
  }

  /// `Activity not found!`
  String get activityNotFound {
    return Intl.message(
      'Activity not found!',
      name: 'activityNotFound',
      desc: '',
      args: [],
    );
  }

  /// `This activity is blocked`
  String get activityIsBlocked {
    return Intl.message(
      'This activity is blocked',
      name: 'activityIsBlocked',
      desc: '',
      args: [],
    );
  }

  /// `This activity can't be completed by swiping`
  String get activityCantCompleteSwipe {
    return Intl.message(
      'This activity can\'t be completed by swiping',
      name: 'activityCantCompleteSwipe',
      desc: '',
      args: [],
    );
  }

  /// `You're not allowed to complete this activity by swiping`
  String get activityNotAllowedToCompleteBySwipe {
    return Intl.message(
      'You\'re not allowed to complete this activity by swiping',
      name: 'activityNotAllowedToCompleteBySwipe',
      desc: '',
      args: [],
    );
  }

  /// `I'm finished for the day`
  String get doneForToday {
    return Intl.message(
      'I\'m finished for the day',
      name: 'doneForToday',
      desc: '',
      args: [],
    );
  }

  /// `Version number:`
  String get versionNumber {
    return Intl.message(
      'Version number:',
      name: 'versionNumber',
      desc: '',
      args: [],
    );
  }

  /// `Maximum amount of Activities reached. Please contact an Administrator, to limit the amount of synchronized activities.`
  String get tooManyActivitiesException {
    return Intl.message(
      'Maximum amount of Activities reached. Please contact an Administrator, to limit the amount of synchronized activities.',
      name: 'tooManyActivitiesException',
      desc: '',
      args: [],
    );
  }

  /// `Maximum amount of Comments reached. Please contact an Administrator, to limit the amount of synchronized activities.`
  String get tooManyCommentsException {
    return Intl.message(
      'Maximum amount of Comments reached. Please contact an Administrator, to limit the amount of synchronized activities.',
      name: 'tooManyCommentsException',
      desc: '',
      args: [],
    );
  }

  /// `Enter Server Credentials`
  String get serverConnectText {
    return Intl.message(
      'Enter Server Credentials',
      name: 'serverConnectText',
      desc: '',
      args: [],
    );
  }

  /// `Change Server`
  String get changeServer {
    return Intl.message(
      'Change Server',
      name: 'changeServer',
      desc: '',
      args: [],
    );
  }

  /// `Please log in to continue.`
  String get loginScreenMessage {
    return Intl.message(
      'Please log in to continue.',
      name: 'loginScreenMessage',
      desc: '',
      args: [],
    );
  }

  /// `Base URL`
  String get baseUrl {
    return Intl.message(
      'Base URL',
      name: 'baseUrl',
      desc: '',
      args: [],
    );
  }

  /// `Authentication URL`
  String get authenticationUrl {
    return Intl.message(
      'Authentication URL',
      name: 'authenticationUrl',
      desc: '',
      args: [],
    );
  }

  /// `Client ID`
  String get clientId {
    return Intl.message(
      'Client ID',
      name: 'clientId',
      desc: '',
      args: [],
    );
  }

  /// `connect`
  String get connect {
    return Intl.message(
      'connect',
      name: 'connect',
      desc: '',
      args: [],
    );
  }

  /// `go back`
  String get goBack {
    return Intl.message(
      'go back',
      name: 'goBack',
      desc: '',
      args: [],
    );
  }

  /// `Could not connect to server. please check used credentials.`
  String get couldNotConnectError {
    return Intl.message(
      'Could not connect to server. please check used credentials.',
      name: 'couldNotConnectError',
      desc: '',
      args: [],
    );
  }

  /// `Paste from clipboard`
  String get pasteFromClipboard {
    return Intl.message(
      'Paste from clipboard',
      name: 'pasteFromClipboard',
      desc: '',
      args: [],
    );
  }

  /// `The copied code is not in the right format, please select the appConfig from the copy to clipboard button in the web application.`
  String get copiedTextNotInRightFormat {
    return Intl.message(
      'The copied code is not in the right format, please select the appConfig from the copy to clipboard button in the web application.',
      name: 'copiedTextNotInRightFormat',
      desc: '',
      args: [],
    );
  }

  /// `Saved from Clipboard!`
  String get successfullyPasted {
    return Intl.message(
      'Saved from Clipboard!',
      name: 'successfullyPasted',
      desc: '',
      args: [],
    );
  }

  /// `Could not sync user settings to the server, please try again.`
  String get couldNotPublishUserSettingsException {
    return Intl.message(
      'Could not sync user settings to the server, please try again.',
      name: 'couldNotPublishUserSettingsException',
      desc: '',
      args: [],
    );
  }

  /// `change your selected turnaround event`
  String get changeSelectedTurnaroundEvent {
    return Intl.message(
      'change your selected turnaround event',
      name: 'changeSelectedTurnaroundEvent',
      desc: '',
      args: [],
    );
  }

  /// `All your current unsynced items will be lost`
  String get allProgressLost {
    return Intl.message(
      'All your current unsynced items will be lost',
      name: 'allProgressLost',
      desc: '',
      args: [],
    );
  }

  /// `Please select a turnaround event, before you can continue.`
  String get warningSelectTurnaroundEvent {
    return Intl.message(
      'Please select a turnaround event, before you can continue.',
      name: 'warningSelectTurnaroundEvent',
      desc: '',
      args: [],
    );
  }

  /// `No grouping found. Unable to group Activities.`
  String get noGroupingFound {
    return Intl.message(
      'No grouping found. Unable to group Activities.',
      name: 'noGroupingFound',
      desc: '',
      args: [],
    );
  }

  /// `Error occurred`
  String get errorTitle {
    return Intl.message(
      'Error occurred',
      name: 'errorTitle',
      desc: '',
      args: [],
    );
  }

  /// `Disconnected: No connection found.`
  String get noConnectionFound {
    return Intl.message(
      'Disconnected: No connection found.',
      name: 'noConnectionFound',
      desc: '',
      args: [],
    );
  }

  /// `Activity ready to start`
  String get readyToStart {
    return Intl.message(
      'Activity ready to start',
      name: 'readyToStart',
      desc: '',
      args: [],
    );
  }

  /// `Predecessor is blocked`
  String get predecessorBlocked {
    return Intl.message(
      'Predecessor is blocked',
      name: 'predecessorBlocked',
      desc: '',
      args: [],
    );
  }

  /// `Activity not ready to start`
  String get notReadyToStart {
    return Intl.message(
      'Activity not ready to start',
      name: 'notReadyToStart',
      desc: '',
      args: [],
    );
  }

  /// `Accept`
  String get accept {
    return Intl.message(
      'Accept',
      name: 'accept',
      desc: '',
      args: [],
    );
  }

  /// `Press '{accept}' to bypass this warning and record progress on this activity.`
  String bypassWarning(Object accept) {
    return Intl.message(
      'Press \'$accept\' to bypass this warning and record progress on this activity.',
      name: 'bypassWarning',
      desc: '',
      args: [accept],
    );
  }

  /// `Ascending`
  String get ascending {
    return Intl.message(
      'Ascending',
      name: 'ascending',
      desc: '',
      args: [],
    );
  }

  /// `Descending`
  String get descending {
    return Intl.message(
      'Descending',
      name: 'descending',
      desc: '',
      args: [],
    );
  }

  /// `Show WBS Hierarchy`
  String get showWbsHierarchy {
    return Intl.message(
      'Show WBS Hierarchy',
      name: 'showWbsHierarchy',
      desc: '',
      args: [],
    );
  }

  /// `Successor(s)`
  String get successors {
    return Intl.message(
      'Successor(s)',
      name: 'successors',
      desc: '',
      args: [],
    );
  }

  /// `Predecessor(s)`
  String get predecessors {
    return Intl.message(
      'Predecessor(s)',
      name: 'predecessors',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'nl'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
