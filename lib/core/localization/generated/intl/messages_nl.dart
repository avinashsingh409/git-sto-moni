// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a nl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'nl';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "activityComments": MessageLookupByLibrary.simpleMessage("Reacties"),
        "activityDetails": MessageLookupByLibrary.simpleMessage("Details"),
        "activityListScreen":
            MessageLookupByLibrary.simpleMessage("Activiteiten"),
        "activityNotFinished": MessageLookupByLibrary.simpleMessage(
            "De vorige activiteit is niet voltooid"),
        "activityPostButton": MessageLookupByLibrary.simpleMessage("Plaats"),
        "blockAlertSeeMore":
            MessageLookupByLibrary.simpleMessage("Meer bekijken"),
        "blockAlertText": MessageLookupByLibrary.simpleMessage(
            "Werk kan niet uitgevoerd worden"),
        "blockDetailsAbleToWork": MessageLookupByLibrary.simpleMessage(
            "Werk kan weer uitgevoerd worden"),
        "blockDetailsReason": MessageLookupByLibrary.simpleMessage("Oorzaak"),
        "blockDetailsSubmission":
            MessageLookupByLibrary.simpleMessage("Indiener"),
        "changesSaved":
            MessageLookupByLibrary.simpleMessage("Wijzigingen opgeslagen"),
        "chooseCustomPercentage":
            MessageLookupByLibrary.simpleMessage("Bevestig aangepaste waarde"),
        "commentWidgetShowLess":
            MessageLookupByLibrary.simpleMessage("Toon minder"),
        "commentWidgetShowMore":
            MessageLookupByLibrary.simpleMessage("Toon meer"),
        "commentsHintText":
            MessageLookupByLibrary.simpleMessage("Schrijf je reactie hier..."),
        "commentsMostRecent":
            MessageLookupByLibrary.simpleMessage("Meest recent"),
        "commentsShowMore":
            MessageLookupByLibrary.simpleMessage("Toon meer reacties"),
        "commentsYourComments":
            MessageLookupByLibrary.simpleMessage("Jouw reacties"),
        "duration": MessageLookupByLibrary.simpleMessage("duur"),
        "durationDays": MessageLookupByLibrary.simpleMessage("dagen"),
        "endDate": MessageLookupByLibrary.simpleMessage("Einddatum"),
        "loadingData": MessageLookupByLibrary.simpleMessage("Data laden..."),
        "loginButton": MessageLookupByLibrary.simpleMessage("Aanmelden"),
        "loginError":
            MessageLookupByLibrary.simpleMessage("Aanmelden niet gelukt"),
        "logoutButton": MessageLookupByLibrary.simpleMessage("Afmelden"),
        "needInternetToSync": MessageLookupByLibrary.simpleMessage(
            "Er is een internetverbinding nodig om de synchronisatie uit te voeren."),
        "noActivities": MessageLookupByLibrary.simpleMessage(
            "Er zijn geen activiteiten om weer te geven"),
        "noComments": MessageLookupByLibrary.simpleMessage("Geen reacties."),
        "noInput": MessageLookupByLibrary.simpleMessage("Geen invoer"),
        "nonValidInput":
            MessageLookupByLibrary.simpleMessage("Incorrecte invoer"),
        "notFound": MessageLookupByLibrary.simpleMessage("Niet gevonden"),
        "plannedEndDate":
            MessageLookupByLibrary.simpleMessage("Geplande einddatum"),
        "plannedStartDate":
            MessageLookupByLibrary.simpleMessage("Geplande startdatum"),
        "progress": MessageLookupByLibrary.simpleMessage("Voortgang"),
        "reasonForBlock":
            MessageLookupByLibrary.simpleMessage("Reden voor de belemmering"),
        "settingsScreen": MessageLookupByLibrary.simpleMessage("Instellingen"),
        "siteNotSafe":
            MessageLookupByLibrary.simpleMessage("Onveilige werkomgeving"),
        "startDate": MessageLookupByLibrary.simpleMessage("Startdatum"),
        "submitButton": MessageLookupByLibrary.simpleMessage("Verzend"),
        "sync": MessageLookupByLibrary.simpleMessage("Sync"),
        "syncData": MessageLookupByLibrary.simpleMessage("Synchroniseer data"),
        "title": MessageLookupByLibrary.simpleMessage("STO Execution"),
        "unableToWorkButton":
            MessageLookupByLibrary.simpleMessage("Kan werk niet uitvoeren!"),
        "unableToWorkHeaderQuestion": MessageLookupByLibrary.simpleMessage(
            "Waarom kan het werk niet voltooid worden?"),
        "unableToWorkHintText": MessageLookupByLibrary.simpleMessage(
            "Geef een andere reden op waarom het werk niet uitgevoerd kan worden... ")
      };
}
