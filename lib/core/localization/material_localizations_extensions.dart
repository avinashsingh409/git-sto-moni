import 'package:flutter/material.dart';

extension MaterialLocalizationsExtensions on MaterialLocalizations {
  formatTimeAndDate(DateTime dateTime) {
    return "${formatShortDate(dateTime)} ${formatTimeOfDay(TimeOfDay.fromDateTime(dateTime))} ";
  }

  formatDate(DateTime dateTime) {
    return "${formatShortDate(dateTime)}} ";
  }
}

DateTime formatDateTimeToDate(DateTime date) => DateTime(date.year, date.month, date.day);
